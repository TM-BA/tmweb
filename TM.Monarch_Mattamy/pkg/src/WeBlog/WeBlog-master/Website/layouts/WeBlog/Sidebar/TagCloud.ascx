﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogTagCloud.ascx.cs" Inherits="Sitecore.Modules.WeBlog.Layouts.BlogTagCloud" %>
<%@ Import Namespace="System.Collections.Generic" %>

<asp:Panel ID="PanelTagCloud" runat="server">
<ul class="rhs">
    <li><%= Sitecore.Modules.WeBlog.Globalization.Translator.Render("TAGCLOUD") %></li>
        <asp:Repeater runat="server" ID="TagList">
            <ItemTemplate>
            <li>
                <a href="<%# GetTagUrl(((KeyValuePair<string, int>)Container.DataItem).Key) %>">
                    <%# ((KeyValuePair<string, int>)Container.DataItem).Key %>
                </a>
            </li>
            </ItemTemplate>
        </asp:Repeater>
</ul>
</asp:Panel>