﻿using System.IO;
using System.Net.Mail;
using TM.Utils.Web;

namespace TM.Utils
{
    public class EmailHelper
    {
        public static void SendTextEmail(string originatorEmail, string recipientEmail, string subject, string body)
        {
            SendEmail(originatorEmail, recipientEmail, subject, body, false);
        }

        public static void SendEmail(string originatorEmail, string recipientEmail, string subject, string body,
                                     bool isBodyHTML = true, Stream attachmentStream = null,
                                     string attachmentName = null, string bccEmailAddress = null, bool isAsync = true)
        {
            using (var message = new MailMessage())
            {
                message.To.Add(recipientEmail);
                message.From = new MailAddress(originatorEmail);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isBodyHTML;
                if (!string.IsNullOrEmpty(bccEmailAddress))
                    message.Bcc.Add(new MailAddress(bccEmailAddress));


                if (attachmentStream != null)
                {
                    var attachment = new Attachment(attachmentStream, attachmentName, null);
                    message.Attachments.Add(attachment);
                }

                try
                {
                    var smtpClient = new SmtpClient(Config.Settings.PrimaryEmailServer);
                    smtpClient.Send(message);
                }
                catch
                {
                    var smtpClient = new SmtpClient(Config.Settings.SecondaryEmailServer);
                    smtpClient.Send(message);
                }
            }
        }

    }
}
