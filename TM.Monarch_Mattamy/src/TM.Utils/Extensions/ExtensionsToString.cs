using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace TM.Utils.Extensions
{
    using System.Text;

    public static class ExtensionsToString
    {
        public static string Between(this string source, string startString, string endString)
        {
            if (source.Contains(startString) && source.Contains(endString))
            {
                int startIndex = source.IndexOf(startString, System.StringComparison.Ordinal) + startString.Length;
                int endIndex = source.IndexOf(endString, startIndex, System.StringComparison.Ordinal);
                int substringLength = endIndex - startIndex;
                if (substringLength > 0)
                    return source.Substring(startIndex, substringLength);
            }

            return string.Empty;
        }


        /// <summary>
        /// Checks if a string is not null or empty
        /// </summary>
        /// <param name="value">A string instance</param>
        /// <returns>True if the string has a value</returns>
        public static bool IsNotEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Check if a string is null or empty
        /// </summary>
        /// <param name="value">A string instance</param>
        /// <returns>True if the string is null or empty, otherwise false</returns>
        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Returns true if a string is null (the string can, however, be empty)
        /// </summary>
        /// <param name="value">A string value</param>
        /// <returns>True if the string value is null, otherwise false</returns>
        public static bool IsNull(this string value)
        {
            return value == null;
        }

        /// <summary>
        /// Uses the string as a template and applies the specified arguments
        /// </summary>
        /// <param name="format">The format string</param>
        /// <param name="args">The arguments to pass to the format provider</param>
        /// <returns>The formatted string</returns>
        public static string FormatWith(this string format, params object[] args)
        {
            return format.IsEmpty() ? format : string.Format(format, args);
        }

        /// <summary>
        /// http://james.newtonking.com/archive/2008/03/29/formatwith-2-0-string-formatting-with-named-variables.aspx
        /// </summary>
        /// <param name="format"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string FormatWith(this string format, object source)
        {
            return FormatWith(format, null, source);
        }
        /// <summary>
        /// http://james.newtonking.com/archive/2008/03/29/formatwith-2-0-string-formatting-with-named-variables.aspx
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string FormatWith(this string format, IFormatProvider provider, object source)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            Regex r = new Regex(@"\{\{|\{([\w\.\[\]]+)((?:[,:][^}]+)?\})",
                                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            List<object> values = new List<object>();

            string rewrittenFormat = r.Replace(format, m =>
                                                           {
                                                               string propertyName = m.Groups[1].Value;

                                                               if (propertyName.Length == 0)

                                                                   return m.Value;

                                                               values.Add(propertyName == "0"
                                                                              ? source
                                                                              : DataBinder.Eval(source, propertyName));

                                                               return "{" + (values.Count - 1) + m.Groups[2].Value;
                                                           });

            return string.Format(provider, rewrittenFormat, values.ToArray());
        }

        /// <summary>
        /// Returns the UTF-8 encoded string from the specified byte array
        /// </summary>
        /// <param name="data">The byte array</param>
        /// <returns>The UTF-8 string</returns>
        public static string ToUtf8String(this byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public static string FormatPhone(string number)
        {
            if (!string.IsNullOrEmpty(number))
            {
                return Regex.Replace(number, @".*?(\d{3}).*?(\d{3}).*?(\d{4}).*?(( |x|ext|ext.|extension|Ext|Ext.|Extension|#){1}[ ]?([0-9]){1,7}){0,1}$", "$1-$2-$3 $4").Trim();
            }

            return string.Empty;
        }

        /// <summary>
        /// Simple currency formatting - $100,000
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static string FormatCurrency(this object price, string defaultValue = "0")
        {
            var priceNum = price.CastAs(0);
            if (priceNum == 0) return defaultValue;

            return priceNum.ToString("C0");
        }



        public static bool IsValidEmail(string email)
        {
            string emailRegEx = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

            Regex re = new Regex(emailRegEx);
            return
            (
                (re.Match(email).Success) &&
                (email.IndexOf(' ') == -1) &&
                (email.Length <= 50)
             );
        }

        public static bool IsValidAlphaNumericString(string password)
        {
            string alphaNumericRegEx = @"^[a-zA-Z0-9]+$";
            Regex re = new Regex(alphaNumericRegEx);
            return re.Match(password).Success;
        }

        public static bool IsValidZip(string postalCode)
        {
            // truncate any characters if postal code string is greater than five characters
            if (postalCode.Length > 5)
            {
                postalCode = postalCode.Substring(0, 5);
            }

            // perform regular expression validation
            Regex re = new Regex(@"\d{5}");

            if (re.Match(postalCode).Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

     
        public static string GetDelimitedString(string delimiter, ArrayList list)
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].ToString().Equals(""))
                {
                    if (s.Length > 0) s.Append(delimiter);
                    s.Append(list[i].ToString());
                }
            }
            return s.ToString();

        }

        public static string StripLeadingEndingChar(string stringToProcess, char chr)
        {
            if (stringToProcess.StartsWith(chr.ToString()))
                stringToProcess = stringToProcess.Remove(0, 1);

            if (stringToProcess.EndsWith(chr.ToString()))
                stringToProcess = stringToProcess.Remove(stringToProcess.Length - 1, 1);

            return stringToProcess;
        }

        public static string StripLeadingEndingChars(string stringToProcess, string chars)
        {
            if (stringToProcess.StartsWith(chars))
                stringToProcess = stringToProcess.Remove(0, chars.Length);

            if (stringToProcess.EndsWith(chars))
                stringToProcess = stringToProcess.Remove(stringToProcess.Length - chars.Length);

            return stringToProcess;
        }

        public static string AddTrailingSlash(string stringToProcess)
        {
            if (!stringToProcess.EndsWith("/") && stringToProcess.Length > 0)
                return stringToProcess += "/";
            else
                return string.Empty;
        }

        public static string AddLeadingSlash(string stringToProcess)
        {
            if (!stringToProcess.StartsWith("/") && stringToProcess.Length > 0)
                return stringToProcess = "/" + stringToProcess;
            else
                return string.Empty;
        }

        public static string ListToString(List<string> list, string separator)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in list)
            {
                if ((s != null) && (s.Trim() != String.Empty))
                {
                    sb.AppendFormat("{0}{1}", s, separator);
                }
            }

            return sb.ToString().TrimEnd(separator.ToCharArray());
        }

        public static object EmptyToNullString(string field)
        {
            if (field == string.Empty)
                return DBNull.Value;
            else
                return field;
        }


        /// <summary>
        /// Helper method to build name-value-pair string from a dictionary of name-value pairs
        /// </summary>
        /// <param name="fields">Dictionary of strings key-value-pairs</param>
        /// <returns>A string in the format {key1}={value1}&{key2}={value2}&etc.</returns>
        public static string NameValuePairString(Dictionary<string, string> fields)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in fields.Keys)
            {
                if (sb.Length > 0)
                {
                    sb.Append("&");
                }
                sb.AppendFormat("{0}={1}", key, fields[key]);
            }
            return sb.ToString();
        }


        /// <summary>
        /// Pretty prints the range.
        /// </summary>
        /// <example><code>PrettyPrintRange(1000, 2000, "c");</code></example>
        /// <param name="lowValue">The low value.</param>
        /// <param name="highValue">The high value.</param>
        /// <param name="forMatType">
        /// A standard numeric format string 
        /// <see cref="System.Globalization.NumberFormatInfo"/>
        /// <seealso cref="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconstandardnumericformatstringsoutputexample.asp"/>
        /// </param>
        /// <returns></returns>
        public static string PrettyPrintRange(double lowValue, double highValue, string forMatType)
        {
            string arg0Format = "{0:" + forMatType + "}";
            string arg1Format = "{1:" + forMatType + "}";
            if (lowValue == 0 & highValue == 0)
            {
                return string.Empty;
            }
            if ((lowValue > 0 & highValue == 0) | (lowValue == highValue))
            {
                return string.Format("From " + arg0Format, lowValue); // c -> currency, o -> arg index
            }
            else if (lowValue == 0 & highValue > 0)
            {
                return string.Format("Up to " + arg0Format, highValue);
            }
            else
            {
                return string.Format("From " + arg0Format + " - " + arg1Format, lowValue, highValue);
            }
        }

        /// <summary>
        /// Reverses a String.
        /// </summary>
        /// <param name="source">The string to reverse.</param>
        /// <returns>The reversed string.</returns>
        public static string Reverse(string source)
        {
            char[] charArray = new char[source.Length];
            int len = source.Length - 1;

            for (int i = 0; i <= len; i++)
                charArray[i] = source[len - i];

            return new string(charArray);
        }

        /// <summary>
        /// Finds a substring within another string and 
        /// returns specified number of characters before and after the keyword(including the keyword)
        /// </summary>
        /// <param name="sourceText">Source string</param>
        /// <param name="keyWord">Substring to find</param>
        /// <param name="before">Number of characters to include before substring</param>
        /// <param name="after">Number of characters to include after substring</param>
        /// <param name="highlightKeyword">Bolds keyword</param>
        /// <param name="hightLightFormatString">Format string that is used to wrap the keyword </param>
        /// <returns></returns>
        public static string HighlightSubstring(string sourceText, string keyWord, int before, int after, bool highlightKeyword, string hightLightFormatString)
        {
            string subString = string.Empty;
            if (sourceText.Contains(keyWord))//keyword present
            {
                try
                {
                    if (sourceText.IndexOf(keyWord) < keyWord.Length)
                        before = 0;
                    if (sourceText.IndexOf(keyWord) + keyWord.Length >= sourceText.Length)
                        after = 0;

                    int startIdx = sourceText.IndexOf(keyWord) - before;
                    int endIdx = keyWord.Length + after;
                    subString = sourceText.Substring(startIdx, endIdx);

                    if (highlightKeyword)
                        subString = subString.Replace(keyWord, string.Format(hightLightFormatString, keyWord));
                    return subString;
                }
                catch
                {
                    return sourceText;
                }
            }
            else
            {
                return sourceText;
            }
        }


        public static string Left(this string str, int length)
        {
            return str.Substring(0, Math.Min(length, str.Length));
        }

        public static string Right(this string str, int length)
        {
            return str.Substring(Math.Min(length, str.Length), str.Length);
        }


        public static string StripHtml(this string input)
        {

            var tagsExpression = new Regex(@"</?.+?>");
            return tagsExpression.Replace(input, string.Empty);
        }

        /// <summary>
        /// Used to Replace Names with their corresponding special Characters
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static string ParseString(this string p)
        {
            if (!string.IsNullOrEmpty(p))
            {
                return p.Replace("$", "")
                        .Replace("[", "")
                        .Replace("]", "")
                        .Replace("#", "")
                        .Replace("(", "")
                        .Replace(")", "")
                        .Replace(" ", "")
						.Replace(",", "");
            }
            return p;
        }

        public static List<string> StringList(this string p)
        {
            if (!string.IsNullOrEmpty(p))
            {
                string[] list = p.Split('-');
                return new List<string>(list);
            }
            return new List<string>();
        }

		public static string TrimUrlLastThreeText(this string str)
		{
			if (!string.IsNullOrWhiteSpace(str))
			{
				string[] ppcitems = str.Split('/');
				string newpath = string.Empty;

				for (int i = 0; i <= ppcitems.Length - 4; i++)
				{
					newpath += "/" + ppcitems[i];
				}
				return newpath.TrimStart('/');
			}
			return string.Empty;
		}

		public static string TrimUrlLastTwoText(this string str)
		{
			if (!string.IsNullOrWhiteSpace(str))
			{
				string[] ppcitems = str.Split('/');
				string newpath = string.Empty;

				for (int i = 0; i <= ppcitems.Length - 3; i++)
				{
					newpath += "/" + ppcitems[i];
				}
				return newpath.TrimStart('/');
			}
			return string.Empty;
		}

        public static string TrimUrlLastText(this string str)
        {
            if (!string.IsNullOrWhiteSpace(str))
            {
                string[] ppcitems = str.Split('/');
                string newpath = string.Empty;

                for (int i = 0; i <= ppcitems.Length - 2; i++)
                {
                    newpath += "/" + ppcitems[i];
                }
                return newpath;
            }
            return string.Empty;
        }

        public static string LastNSegments(this string url, int n)
	{
        if (!string.IsNullOrWhiteSpace(url))
        {
            string[] urlSegments = url.Split(new[]{'/'},StringSplitOptions.RemoveEmptyEntries);
            var newpath = new List<string>();

            for (int i = urlSegments.Length - n; urlSegments.Length>=n && i <= urlSegments.Length-1; i++)
            {
                newpath.Add(urlSegments[i]);
            }
            return string.Join("/", newpath);
        }
        return string.Empty;
	}


        public static string LastNthSegment(this string url, int n)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                string[] urlSegments = url.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                var length = urlSegments.Length;
		if(length>=n)
			return urlSegments[length-n];
            }
            return string.Empty;
        }

        public static string GetLastTextfromString(this string str, char chr)
        {
            if (!string.IsNullOrWhiteSpace(str))
            {
                int ps = str.LastIndexOf(chr);
                return str.Substring(ps + 1);
            }
            return string.Empty;
        }

        public static string GetQueryValue(this string qry)
        {
            string quryValue = string.Empty;
            if (!string.IsNullOrEmpty(qry))
            {
                string qury = string.Empty;
                qury = qry.Remove(0, 1);
                quryValue = qury.Split('=')[1];
            }
            return quryValue;
        }

		public static string GetDomainFromDeviceName(this string deviceName)
		{
			switch (deviceName.ToLower())
			{
				case "monarchgroup":
					return "monarchgroup";
				case "darlinghomes":
					return "darlinghomes";
				default:
					return "taylormorrison";
			}
		}


        public static string GetCurrentCompanyName(this string deviceName)
        {
            switch (deviceName.ToLower())
            {
                case "monarchgroup":
                    return "Monarch Group";
                case "darlinghomes":
                    return "Darling Homes";
                default:
                    return "Taylor Morrison";
            }
        }

        public static string EncodeJsString(this string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\'':
                        sb.Append("\\\'");
                        break;
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");

            return sb.ToString();
        }

        public static string GetCurrentCompanyNameAbbreviation(this string domain)
        {
			switch (domain.ToLower())
            {
                case "monarchgroup":
                    return "MG";
                case "darlinghomes":
                    return "DH";
                default:
                    return "TM";
            }
        }

        public static string TrimEnd(this string input, string suffixToRemove)
        {
            if (input != null && suffixToRemove != null
              && input.EndsWith(suffixToRemove))
            {
                return input.Substring(0, input.Length - suffixToRemove.Length);
            }
            return input;
        }

	public static string ToTitleCase(this string input)
	{
	   TextInfo ti = CultureInfo.CurrentCulture.TextInfo;
	    return ti.ToTitleCase(input);
	}

        public static string StringFromArray(this string[] array)
        {
            var items = string.Empty;
            if (array.Length > 0)
                items = array.Aggregate(items, (current, item) => current + (item + "|"));
            return items;
        }

        public static List<string> StringToList(this string p)
        {
            if (!string.IsNullOrEmpty(p))
            {
                p = p.TrimEnd('|');
                string[] list = p.Split('|');
                return new List<string>(list);
            }
            return new List<string>();
        }


        private static string OutExpression(object source, string expression)
        {
            string format = "";

            int colonIndex = expression.IndexOf(':');
            if (colonIndex > 0)
            {
                format = expression.Substring(colonIndex + 1);
                expression = expression.Substring(0, colonIndex);
            }

            try
            {
                if (String.IsNullOrEmpty(format))
                {
                    return (DataBinder.Eval(source, expression) ?? "").ToString();
                }
                return DataBinder.Eval(source, expression, "{0:" + format + "}")
                    ?? "";
            }
            catch (System.Web.HttpException)
            {
                throw new FormatException();
            }
        }

        public static string NamedFormat(this string format, object source)
        {
            if (format == null)
            {
                throw new ArgumentNullException("format");
            }

            StringBuilder result = new StringBuilder(format.Length * 2);

            using (var reader = new System.IO.StringReader(format))
            {
                StringBuilder expression = new StringBuilder();
                int @char = -1;

                State state = State.OutsideExpression;
                do
                {
                    switch (state)
                    {
                        case State.OutsideExpression:
                            @char = reader.Read();
                            switch (@char)
                            {
                                case -1:
                                    state = State.End;
                                    break;
                                case '{':
                                    state = State.OnOpenBracket;
                                    break;
                                case '}':
                                    state = State.OnCloseBracket;
                                    break;
                                default:
                                    result.Append((char)@char);
                                    break;
                            }
                            break;
                        case State.OnOpenBracket:
                            @char = reader.Read();
                            switch (@char)
                            {
                                case -1:
                                    throw new FormatException();
                                case '{':
                                    result.Append('{');
                                    state = State.OutsideExpression;
                                    break;
                                default:
                                    expression.Append((char)@char);
                                    state = State.InsideExpression;
                                    break;
                            }
                            break;
                        case State.InsideExpression:
                            @char = reader.Read();
                            switch (@char)
                            {
                                case -1:
                                    throw new FormatException();
                                case '}':
                                    result.Append(OutExpression(source, expression.ToString()));
                                    expression.Length = 0;
                                    state = State.OutsideExpression;
                                    break;
                                default:
                                    expression.Append((char)@char);
                                    break;
                            }
                            break;
                        case State.OnCloseBracket:
                            @char = reader.Read();
                            switch (@char)
                            {
                                case '}':
                                    result.Append('}');
                                    state = State.OutsideExpression;
                                    break;
                                default:
                                    throw new FormatException();
                            }
                            break;
                        default:
                            throw new InvalidOperationException("Invalid state.");
                    }
                } while (state != State.End);
            }

            return result.ToString();
        }

        private enum State
        {
            OutsideExpression,
            OnOpenBracket,
            InsideExpression,
            OnCloseBracket,
            End
     
        }

	
    }
}