
namespace TM.Utils.Extensions
{
	using System;
	using System.Collections.Generic;
	using System.Net;


	public static class ExtensionsToUri
	{
		/// <summary>
		///   Appends a path to an existing Uri
		/// </summary>
		/// <param name = "uri"></param>
		/// <param name = "path"></param>
		/// <returns></returns>
		public static Uri AppendPath(this Uri uri, string path)
		{
			string absolutePath = uri.AbsolutePath.TrimEnd('/') + "/" + path;
			return new UriBuilder(uri.Scheme, uri.Host, uri.Port, absolutePath, uri.Query).Uri;
		}

		public static IEnumerable<IPEndPoint> ResolveHostName(this Uri uri)
		{
			if (uri.HostNameType == UriHostNameType.Dns)
			{
				IPAddress[] addresses = Dns.GetHostAddresses(uri.DnsSafeHost);
				if (addresses.Length == 0)
					throw new ArgumentException("The host could not be resolved: " + uri.DnsSafeHost, "uri");

				foreach (IPAddress address in addresses)
				{
					var endpoint = new IPEndPoint(address, uri.Port);
					yield return endpoint;
				}
			}
			else if (uri.HostNameType == UriHostNameType.IPv4)
			{
				IPAddress address = IPAddress.Parse(uri.Host);
				if (address == null)
					throw new ArgumentException("The IP address is invalid: " + uri.Host, "uri");

				var endpoint = new IPEndPoint(address, uri.Port);
				yield return endpoint;
			}
			else
				throw new ArgumentException("Could not determine host name type: " + uri.Host, "uri");
		}

	      public static string LastSegment(this Uri uri)
	      {
	          return uri.Segments[uri.Segments.Length-1];

	      }

          public static string AppendQuery(this Uri uri, Dictionary<string, string> nameValues)
          {
              var queryToAppend = string.Empty;
              foreach (var key in nameValues.Keys)
              {
                  queryToAppend = key + "=" + nameValues[key] + "&";

              }

              queryToAppend = queryToAppend.Trim('&');

              var baseUri = new UriBuilder(uri);
              baseUri.Query = !string.IsNullOrEmpty(baseUri.Query) ?
                              baseUri.Query.Substring(1) + "&" + queryToAppend
                              : queryToAppend;

              return baseUri.Uri.ToString();
          }

          public static string AppendQuery(this string uri, Dictionary<string, string> nameValues)
          {

              return new Uri(uri).AppendQuery(nameValues);
          }

          public static string AppendQuery(this string uri, string name, string value)
          {

              return new Uri(uri).AppendQuery(name,value);
          }

          public static string AppendQuery(this Uri uri, string name, string value)
          {
              var queryToAppend = string.Empty;
            
              queryToAppend = name + "=" + value + "&";
              queryToAppend = queryToAppend.Trim('&');

              var baseUri = new UriBuilder(uri);
              baseUri.Query = !string.IsNullOrEmpty(baseUri.Query) ?
                              baseUri.Query.Substring(1) + "&" + queryToAppend
                              : queryToAppend;

              return baseUri.Uri.ToString();
          }

	}
}
