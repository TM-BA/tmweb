﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BHI.Configuration;
using System.Dynamic;

namespace TM.Utils.Web
{
    public class Config
    {
        //usage:
        //Config.Settings.AnyKey

        private static SettingsProxy _settings = new SettingsProxy();

        public static dynamic Settings
        {
            get { return _settings; }
        }

        private class SettingsProxy : DynamicObject
        {
            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                var setting = ConfigManager.Get<string>(binder.Name);
                if (setting != null)
                {
                    result = setting;
                    return true;
                }
                result = null;
                return false;
            }


        }    
    }


   
}
