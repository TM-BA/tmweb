﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ResourceCombiner.Core.API;
namespace TM.Utils.Web
{
    public class JsCssCombiner
    {

	  static WebExtensions _xtn = new WebExtensions();

	  public static string GetSet(string resourceSetName)
	  {
	     return  _xtn.GetCombinedLink(resourceSetName, null);
	  }

    }
}
