﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;   
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace TM.Utils.Web
{
	public class SerializerHelper
	{
		public static string JsonSerializer(object data)
		{
			if (data != null)
			{
				DataContractJsonSerializer serializer = new DataContractJsonSerializer(data.GetType());
				string serializedJson = string.Empty;
				using (MemoryStream memoryStream = new MemoryStream())
				{
					serializer.WriteObject(memoryStream, data);
					serializedJson = Encoding.Default.GetString(memoryStream.ToArray());
				}
				return serializedJson;
			}
			else
			{
				return string.Empty;
			}
		}

		public static XmlNode SerializeObjectToXmlNode(Object obj)
		{
			if (obj == null)
				throw new ArgumentNullException("Argument cannot be null");

			XmlNode resultNode = null;
			XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				try
				{
					xmlSerializer.Serialize(memoryStream, obj);
				}
				catch (InvalidOperationException)
				{
					return null;
				}
				memoryStream.Position = 0;
				XmlDocument doc = new XmlDocument();
				doc.Load(memoryStream);
				resultNode = doc.DocumentElement;
			}
			return resultNode;
		}


		public static Object DeSerializeXmlNodeToObject(XmlNode node, Type objectType)
		{
			if (node == null)
				throw new ArgumentNullException("Argument cannot be null");
			XmlSerializer xmlSerializer = new XmlSerializer(objectType);
			using (MemoryStream memoryStream = new MemoryStream())
			{
				XmlDocument doc = new XmlDocument();
				doc.AppendChild(doc.ImportNode(node, true));
				doc.Save(memoryStream);
				memoryStream.Position = 0;
				XmlReader reader = XmlReader.Create(memoryStream);
				try
				{
					return xmlSerializer.Deserialize(reader);
				}
				catch
				{
					return objectType.IsByRef ? null : Activator.CreateInstance(objectType);
				}
			}
		}
	}
}
