﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Utils.Web
{
   public class CacheKeys
   {
       public static string GetPlanQuickNavKey(string itemID)
       {
           return "PlanQuickNavKey_" + itemID;
       }

	   public static string GetConfigSitePathKey = "ConfigStateMarketingListingTree";
	   public static string GetStateMarketingListingTreeKey = "StateMarketingListingTree";
	   public static string AllCrossDivisionsKey = "AllCrossDivisionsKey";
	   public static string AllAvailabilitiesKey = "AllAvailabilitiesKey";
     
       public static string FindYourHomeAndLeftNavMenuCacheKey(string deviceName)
       {
           return "FindYourHomeAndLeftNavMenuCacheKey_" + deviceName;
       }

       public static string GetSavedItemsKey(string qualifiedUsername)
	   {
		   return "SavedItemsKey_" + qualifiedUsername;
	   }

	   public static string GetPlansSearchKey(string itemId)
	   {
		   return "PlansSearchKey_" + itemId;
	   }

	   public static string GetCommunityPromoKey(string itemId)
	   {
		   return "CommunityPromoKey_" + itemId;
	   }

	   public static string GetFavPlansSearchKey(string itemId)
	   {
		   return "PlansFavSearchKey_" + itemId;
	   }
	   public static string GetSubCommunitySearchKey(string itemId)
	   {
		   return "SubCommunitySearchKey_" + itemId;
	   }

	   public static string GetCommunitySearchKey(string searchPath)
	   {
		   return "CommunitySearchKey_" + searchPath;
	   }

	   public static string GetCommunitySearchGroupByDomainKey(string searchPath, string sortBy, string domain)
	   {
		   return string.Format("CommunitySearchGroupByDomainKey_{0}_{1}_{2}", searchPath, sortBy, domain);
	   }

	   public static string GetHomeforSaleSearchKey(string searchPath)
	   {
		   return "HomeforSaleSearchKey_" + searchPath;
	   }


       public static string GetBlogCacheKey(string domain)
       {
           return string.Format("{0}_Blog", domain);
       }

       public static string LeftNavMenuCacheKey(string deviceName)
       {
           return "LeftNavMenuCacheKey" + deviceName;
       }
   }
}
