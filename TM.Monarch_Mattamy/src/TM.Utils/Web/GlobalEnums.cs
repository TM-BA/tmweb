﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace TM.Utils.Web
{
	[Serializable]
	public class GlobalEnums
	{
		public enum SearchFacet
		{
			Multi,
			HomeForSale
		}

		public enum MapRenderingType
		{
			ComingSoon,
			NowSelling,
			CloseOut,
			DesignCenter,
			Closed,
			PreSelling,
			None
		}

		public enum FavoritesAction
		{
			Add,
			Remove
		}

		public enum FavoritesType
		{
			Community,
			Plan,
			HomeForsale
		}

		public enum FacetStatus
		{
			On,
			Off
		}

		public enum SearchStatus
		{
			InActive = 0,
			Active = 1			
		}

		public enum CommunityStatus
		{
			InActive = 0,
			Open = 1,			
			GrandOpening = 2,
			ComingSoon = 3,
			Closeout = 4,
			Closed = 5,
			PreSelling = 6
		}

		public enum SortBy
		{ 	
			CommunityName = 1,
			PriceLowtoHigh = 2,
			PriceHightoLow = 3,
			Location = 4,
			PlanName =5,
			SquareFootage = 6,
			Numberofbedrooms = 7,
			AvailabilityDate = 8,
			Stories = 9
		}

		public enum Domain
		{
			[Description("taylormorrison")]
			TaylorMorrison,
			[Description("monarchgroup")]
			MonarchGroup,
			[Description("darlinghomes")]
			DarlingHomes
		}

		public enum SearchFilterKeys
		{ 
			SearchUrl,
			DivisionArea,
			City,
			State,
			StartingPrice,
			EndingPrice,
			StartingSqFt,
			EndingSqFt,
			NumberofBedRooms,
			NumberofBathRooms,
			NumberofGarage,
			NumberofStories,
			Availability,
			SchoolDistrict,
			CommunityStatus,
			Sortby,
			BonusDenSolar
		}

		public enum StateProvinceAbbreviation
		{
			[Description("AL")] Alabama,
			[Description("AK")] Alaska,
			[Description("AZ")] Arizona,
			[Description("AR")] Arkansas,
			[Description("CA")] California,
			[Description("CO")] Colorado,
			[Description("CT")] Connecticut,
			[Description("DE")] Delaware,
			[Description("DC")] DistrictofColumbia,
			[Description("FL")] Florida,
			[Description("GA")] Georgia,
			[Description("HI")] Hawaii,
			[Description("ID")] Idaho,
			[Description("IL")] Illinois,
			[Description("IN")] Indiana,
			[Description("IA")] Iowa,
			[Description("KS")] Kansas,
			[Description("KY")] Kentucky,
			[Description("LA")] Louisiana,
			[Description("ME")] Maine,
			[Description("MD")] Maryland,
			[Description("MA")] Massachusetts,
			[Description("MI")] Michigan,
			[Description("MN")] Minnesota,
			[Description("MS")] Mississippi,
			[Description("MO")] Missouri,
			[Description("MT")] Montana,
			[Description("NE")] Nebraska,
			[Description("NV")] Nevada,
			[Description("NH")] NewHampshire,
			[Description("NJ")] NewJersey,
			[Description("NM")] NewMexico,
			[Description("NY")] NewYork,
			[Description("NC")] NorthCarolina,
			[Description("ND")] NorthDakota,
			[Description("OH")] Ohio,
			[Description("OK")] Oklahoma,
			[Description("OR")] Oregon,
			[Description("PA")] Pennsylvania,
			[Description("RI")] RhodeIsland,
			[Description("SC")] SouthCarolina,
			[Description("SD")] SouthDakota,
			[Description("TN")] Tennessee,
			[Description("TX")] Texas,
			[Description("UT")] Utah,
			[Description("VT")] Vermont,
			[Description("VA")] Virginia,
			[Description("WA")] Washington,
			[Description("WV")] WestVirginia,
			[Description("WI")] Wisconsin,
			[Description("WY")] Wyoming,
			[Description("AB")] Alberta,
			[Description("BC")] BritishColumbia,
			[Description("MB")] Manitoba,
			[Description("NB")] NewBrunswick,
			[Description("NL")] NewfoundlandandLabrador,
			[Description("NT")] NorthwestTerritories,
			[Description("NS")] NovaScotia,
			[Description("NU")] Nunavut,
			[Description("ON")] Ontario,
			[Description("PE")] PrinceEdwardIsland,
			[Description("QC")] Quebec,
			[Description("SK")] Saskatchewan,
			[Description("YT")] Yukon
		}

		public enum State
		{
			AL,
			AK,
			AS,
			AZ,
			AR,
			CA,
			CO,
			CT,
			DE,
			DC,
			FM,
			FL,
			GA,
			GU,
			HI,
			ID,
			IL,
			IN,
			IA,
			KS,
			KY,
			LA,
			ME,
			MH,
			MD,
			MA,
			MI,
			MN,
			MS,
			MO,
			MT,
			NE,
			NV,
			NH,
			NJ,
			NM,
			NY,
			NC,
			ND,
			MP,
			OH,
			OK,
			OR,
			PW,
			PA,
			PR,
			RI,
			SC,
			SD,
			TN,
			TX,
			UT,
			VT,
			VI,
			VA,
			WA,
			WV,
			WI,
			WY,
			AB,
			BC,
			MB,
			NB,
			NL,
			NS,
			NT,
			NU,
			ON,
			PE,
			QC,
			SK,
			YT,
			XX
		}
		
	}
}
