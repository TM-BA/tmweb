﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace TM.UrlRewriter.Utilities
{
   public class RewriterContants
    {
       private static NameValueCollection _rewriterConstants = new NameValueCollection();

       public static NameValueCollection Constants
       {
           get { return _rewriterConstants; }
       }
    }
}
