using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class LocalInterestItem : CustomItem
{

public static readonly string TemplateId = "{987D9883-EE0C-4F94-91C1-D46F6470C958}";


#region Boilerplate CustomItem Code

public LocalInterestItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator LocalInterestItem(Item innerItem)
{
	return innerItem != null ? new LocalInterestItem(innerItem) : null;
}

public static implicit operator Item(LocalInterestItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField LocalInterestsCategoryHeading
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{AAFF88AD-E5A8-4A6D-897A-1C9092EF8D1B}"]);
	}
}


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{DB6A89D1-BC23-42B8-81F2-3AC9DBDB49AE}"]);
	}
}


public CustomTextField LocalInterestsLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{3DB82C9D-BC51-49E7-80F0-34BE4ED563A8}"]);
	}
}


public CustomTextField LocalInterestsText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{87466E18-03E6-405A-94B5-FB4E59C95B47}"]);
	}
}


public CustomGeneralLinkField LocalInterestsTargetURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{B8749459-8A2C-42B6-BA22-9E1097030152}"]);
	}
}


#endregion //Field Instance Methods
}
}