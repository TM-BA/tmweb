using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;
namespace TM.Domain.Entities
{
public partial class HomeForSaleItem : CustomItem
{

public static readonly string TemplateId = "{7E05E34B-1262-4059-877D-335F1F7246FC}";

#region Inherited Base Templates

private readonly PlanBaseItem _PlanBaseItem;
public PlanBaseItem PlanBase { get { return _PlanBaseItem; } }
private readonly PlanBaseAdditionalItem _PlanBaseAdditionalItem;
public PlanBaseAdditionalItem PlanBaseAdditional { get { return _PlanBaseAdditionalItem; } }
private readonly PlanAndHomeForSaleOnlyBaseItem _PlanAndHomeForSaleOnlyBaseItem;
public PlanAndHomeForSaleOnlyBaseItem PlanAndHomeForSaleOnlyBase { get { return _PlanAndHomeForSaleOnlyBaseItem; } }
private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public HomeForSaleItem(Item innerItem) : base(innerItem)
{
	_PlanBaseItem = new PlanBaseItem(innerItem);
	_PlanBaseAdditionalItem = new PlanBaseAdditionalItem(innerItem);
	_PlanAndHomeForSaleOnlyBaseItem = new PlanAndHomeForSaleOnlyBaseItem(innerItem);
	_TMBaseItem = new TMBaseItem(innerItem);

}

public static implicit operator HomeForSaleItem(Item innerItem)
{
	return innerItem != null ? new HomeForSaleItem(innerItem) : null;
}

public static implicit operator Item(HomeForSaleItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField HomeforSaleLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{32F2A970-FB67-4F74-BFD2-1B20C68A6906}"]);
	}
}


public CustomLookupField MasterPlanID
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{C2AB616E-7631-45BE-8454-0B6DE852AC6C}"]);
	}
}


public CustomIntegerField Price
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{CB15C16C-F133-4661-AF37-0837F427B4DF}"]);
	}
}


public CustomLookupField HomeforSaleStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{651B4A85-2294-40FB-A1F0-384DB0FB4A6D}"]);
	}
}


public CustomLookupField StatusforAvailability
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{55C17A68-4A5E-4AE3-91A7-56BAE63D6579}"]);
	}
}


public CustomCheckboxField IsaModelHome
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{2A672058-3BA3-4B8A-BDDE-8DDFC4A30EF1}"]);
	}
}


public CustomTextField myTMNumber
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{13A1E321-9889-4BD6-8666-B2E0421A6DB0}"]);
	}
}


public CustomDateField AvailabilityDate
{
	get
	{
		return new CustomDateField(InnerItem, InnerItem.Fields["{E98A8262-FB5F-49CB-AD4E-FE2AA70611D5}"]);
	}
}


public CustomTextField LotNumber
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{0CEE027D-7999-4CEB-9F41-DF0DD9C5E1A8}"]);
	}
}


public CustomTextField StreetAddress1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{93CFD58A-4DF8-4FE2-AE17-7A8AEACC7248}"]);
	}
}


public CustomTextField StreetAddress2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{79CB1085-92CA-4222-B6B7-F2EE3876669E}"]);
	}
}


public CustomTextField MLSNumber
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{09317504-B070-4B22-9B05-5EF777C97F9D}"]);
	}
}


public CustomCheckboxField DisplayasFeaturedHomeinRWE
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{43249935-CFC3-4DBD-A292-04F06B0E75E8}"]);
	}
}


public CustomCheckboxField DisplayonHomesforSalePage
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{2A225A08-A2FB-4F81-8F8D-6E54B7CA3C26}"]);
	}
}


public CustomTextField RealtorIncentiveAmount
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{52EC7283-7578-4B39-B1EE-8891721C138D}"]);
	}
}


public CustomTextField RealtorBrokerCommissionRate
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{10E706D8-A660-4A4D-B5E6-6DC5D6A7A7BB}"]);
	}
}


public CustomCheckboxField DisplayonREPPage
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{C8E75EB2-EE25-4A05-8F46-346988E5E573}"]);
	}
}


#endregion //Field Instance Methods
}
}