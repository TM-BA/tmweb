using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class HomeFeaturesItem : CustomItem
{

public static readonly string TemplateId = "{E1131B83-D3CD-4BD4-96F0-AD0E1A4F5BA4}";


#region Boilerplate CustomItem Code

public HomeFeaturesItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HomeFeaturesItem(Item innerItem)
{
	return innerItem != null ? new HomeFeaturesItem(innerItem) : null;
}

public static implicit operator Item(HomeFeaturesItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField HomeFeaturesCategoryHeading
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{54AB8CB6-233F-445C-B34E-BBECD25E253D}"]);
	}
}


public CustomTextField HomeFeaturesText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{D3684C4A-C261-4D5D-A9DB-941C355B323C}"]);
	}
}


public CustomTextField HomeFeaturesLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{9D3F5B68-21EE-430D-A124-5825AB97A4CD}"]);
	}
}


#endregion //Field Instance Methods
}
}