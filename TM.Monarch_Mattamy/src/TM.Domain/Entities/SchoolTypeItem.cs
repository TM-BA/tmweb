using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class SchoolTypeItem : CustomItem
{

public static readonly string TemplateId = "{D54E2EA4-4E55-4EFD-97DA-F3F110CBF8C3}";


#region Boilerplate CustomItem Code

public SchoolTypeItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SchoolTypeItem(Item innerItem)
{
	return innerItem != null ? new SchoolTypeItem(innerItem) : null;
}

public static implicit operator Item(SchoolTypeItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Code
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{6BD3E7C6-92B2-48D2-BBFD-1D204FB89FBB}"]);
	}
}


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{3FCF441C-DB1F-464D-857C-A82509919FF3}"]);
	}
}


public CustomImageField Image
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{3BB9E49A-C7DE-41B7-9365-80F86EF5EF64}"]);
	}
}


public CustomIntegerField SortOrder
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{CA625910-9C5F-4F93-8E9C-D3270C5E5D64}"]);
	}
}


public CustomTextField CMSId
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{EF3D1438-8136-4F4E-9A06-0194CB594565}"]);
	}
}


#endregion //Field Instance Methods
}
}