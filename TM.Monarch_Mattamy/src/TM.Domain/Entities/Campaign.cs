﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class Campaign
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }

        public string SmallImageSrc { get; set; }

        public string LargeImageSrc { get; set; }

        public string TargetURL { get; set; }
    }
}
