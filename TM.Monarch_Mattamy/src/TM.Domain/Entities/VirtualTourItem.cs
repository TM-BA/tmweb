using Sitecore.Data.Items;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class VirtualTourItem : CustomItem
{

public static readonly string TemplateId = "{25E8955B-5664-4B21-B9D3-28E248FB09A5}";


#region Boilerplate CustomItem Code

public VirtualTourItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator VirtualTourItem(Item innerItem)
{
	return innerItem != null ? new VirtualTourItem(innerItem) : null;
}

public static implicit operator Item(VirtualTourItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField VirtualTourLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{827EE92B-31A8-44D9-B3DF-7D1226E955EC}"]);
	}
}


public CustomGeneralLinkField VirtualTourURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{C4F15803-7818-4A56-811C-28713B5A3B1B}"]);
	}
}


public CustomTextField InternalNote
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{43172FB2-D1C7-4F95-80CC-389A1B0C8CE6}"]);
	}
}


#endregion //Field Instance Methods
}
}