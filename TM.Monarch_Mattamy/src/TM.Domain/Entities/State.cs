﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class State
    {
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string abbreviation;

        public string Abbreviation
        {
            get { return abbreviation; }
            set { abbreviation = value; }
        }

        public State(string vName, string vAbbreviation)
        {
            name = vName;
            abbreviation = vAbbreviation;
        }
    }
}
