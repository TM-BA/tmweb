using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class SchoolInfoItem : CustomItem
{

public static readonly string TemplateId = "{1B4197AA-0BBA-481B-9413-C49B5031D684}";


#region Boilerplate CustomItem Code

public SchoolInfoItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator SchoolInfoItem(Item innerItem)
{
	return innerItem != null ? new SchoolInfoItem(innerItem) : null;
}

public static implicit operator Item(SchoolInfoItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField SchoolName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{1A6CA58A-BACE-4E9E-8C6E-F2F8009E81C7}"]);
	}
}


public CustomTextField SchoolLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{14E4B4F3-6E97-499E-AE22-1A29995C55D9}"]);
	}
}


public CustomTextField SchoolSourceID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{678EB6CF-738B-448D-99CA-7AE67FB821F5}"]);
	}
}


public CustomImageField SchoolImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{CFFC324E-9CDF-4857-B29A-6A84AA3502D1}"]);
	}
}


public CustomLookupField SchoolType
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{ED3515EC-4A28-4FE2-BC45-A88C79FF7AC3}"]);
	}
}


public CustomTextField SchoolGrades
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{1F641BCF-2782-46C4-BD20-9118926A62ED}"]);
	}
}


public CustomTextField SchoolPrincipal
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{28C39184-7F1E-4BB6-8386-87E3E2C0DBD9}"]);
	}
}


public CustomTextField SchoolDistance
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{07E116EC-9AE8-4568-94D1-66C8B8E54968}"]);
	}
}


public CustomTextField SchoolPhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{B4C06138-E01D-4076-B641-C7F61E89E358}"]);
	}
}


public CustomGeneralLinkField SchoolWebsiteURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{AD3E1ADE-15A4-4BD5-8AFC-1ADD1CDD3578}"]);
	}
}


#endregion //Field Instance Methods
}
}