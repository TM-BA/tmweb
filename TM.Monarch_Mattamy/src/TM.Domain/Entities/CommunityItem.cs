using System.Text;
using CustomItemGenerator.Fields.ListTypes;
using Sitecore.Data.Items;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.SimpleTypes;


namespace TM.Domain.Entities
{
public class CommunityItem : CustomItem
{

public static readonly string TemplateId = "{553E95A0-D2F3-4409-BF50-7A2C5A494D7C}";

#region Boilerplate CustomItem Code

public CommunityItem(Item innerItem):base(innerItem) 
{
	

}

public static implicit operator CommunityItem(Item innerItem)
{
	return innerItem != null ? new CommunityItem(innerItem) : null;
}

public static implicit operator Item(CommunityItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code

#region Field Instance Methods


//Could not find Field Type for Community Name


//Could not find Field Type for Home Features Intro


public CustomTextField RespondingEmailAddress
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{6A684FB1-F334-4D5A-8566-09B742C0B3B4}"]);
    }
}


public CustomLookupField SchoolDistrict
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{D0344129-2DA2-439E-B5B6-58EFA0A8AABC}"]);
    }
}


public CustomIntegerField SlideshowImageRotationSpeed
{
    get
    {
        return new CustomIntegerField(InnerItem, InnerItem.Fields["{087E4A05-8545-4EA9-B69F-B30FC468CE3C}"]);
    }
}


public CustomLookupField SundayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{AE0B022F-2F07-4B21-B04C-DFD7D9B9CE10}"]);
    }
}


public CustomLookupField SundayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{19E847EA-AB76-4E1F-8B1F-ECE7E5E60EDC}"]);
    }
}


public CustomLookupField MondayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{0E8EE858-1F8B-4727-8287-94C0A8B19385}"]);
    }
}


public CustomLookupField MondayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{345C4E5B-26AA-4F56-9419-093F0B8304D0}"]);
    }
}


public CustomLookupField TuesdayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{50C014A2-4776-4F47-A313-26B096B8479B}"]);
    }
}


public CustomCheckboxField AllowPinningofImagestoPinterest
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{A7BB76D1-A1AE-447D-A2FB-66BAA0A1A32A}"]);
    }
}


public CustomTextField CommunityLegacyID
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{7F7CBA09-1FCD-40F9-8E34-AE64CE547C32}"]);
    }
}


public CustomCheckboxField DisplayHomeFeaturesImages
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{D8B9F30A-A45C-4373-B2B9-777D39D64AB6}"]);
    }
}


public CustomTextField DrivingDirectionsText
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{4A0BEBAC-6B0E-4906-AC31-10ACA9485950}"]);
    }
}


public CustomTextField SchoolsDescription
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{FF0D9C98-B828-447A-96D5-ABAEF99C064B}"]);
    }
}


public CustomLookupField TuesdayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{ACF9EA07-8CF0-4528-86DB-B8E7E7350412}"]);
    }
}


public CustomCheckboxField AutomaticallyExpandLargeImagesonHover
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{5DFEF99E-2B78-43F7-A394-71085E1E6D1C}"]);
    }
}


public CustomLookupField CommunityStatus
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{68B4B141-4361-4238-9B5B-148CD10EA836}"]);
    }
}


//Could not find Field Type for Home Features Image


public CustomCheckboxField IsActiveAdult
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{CD04365D-BFFB-4F47-8117-A3854F720719}"]);
    }
}


public CustomCheckboxField ExpandImagesonClick
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{1B178941-714D-44DA-A682-6AF7390C5399}"]);
    }
}


public CustomDateField CommunityStatusChangeDate
{
    get
    {
        return new CustomDateField(InnerItem, InnerItem.Fields["{3F5E90FB-8E92-4993-9919-2F7CAD8FAAB1}"]);
    }
}


public CustomCheckboxField IsMasterPlanned
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{CD30259D-0C23-487F-B30E-76A0A3596FD6}"]);
    }
}


//Could not find Field Type for Slideshow Image


public CustomTextField CommunityEventDetails
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{E9AA367E-E1E2-4686-AB2E-BAB4B14AE8D0}"]);
    }
}


public CustomCheckboxField IsGated
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{04544622-7FF3-46A8-BA3A-C0EFF0017F8C}"]);
    }
}


public CustomLookupField StatusforSearch
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{1FD895BB-CC46-4B77-A68F-8D5E00A13E3E}"]);
    }
}


public CustomLookupField WednesdayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{5EA49C67-54D2-413A-ABF0-050043702ABE}"]);
    }
}


public CustomCheckboxField IsCondoOnly
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{C89166E9-A210-48D6-B5DB-6F779BA96DF4}"]);
    }
}


public CustomTextField SitePlanDescription
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{A20DFFAB-6840-4BCD-AAB4-E9B3DE0D5C14}"]);
    }
}


public CustomLookupField StatusforAssistance
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{27EE7735-96AE-434F-9539-4A8B1F82F6F3}"]);
    }
}


public CustomLookupField WednesdayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{4911F43B-832F-4035-828C-E0D8D3E7865B}"]);
    }
}


public CustomCheckboxField HasaPool
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{62C1CF9C-9048-4A63-A089-B00CBF53064C}"]);
    }
}


public CustomCheckboxField IsaHighTechHomeCommunity
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{F8D1CE49-335A-4AB4-A959-01BD20C72FD6}"]);
    }
}


public CustomImageField SitePlanImage
{
    get
    {
        return new CustomImageField(InnerItem, InnerItem.Fields["{01FB90B7-116E-4000-AA05-A2477A15C75C}"]);
    }
}


public CustomLookupField ThursdayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{9EA90D44-0256-4383-8718-B9E748BAF4C7}"]);
    }
}


public CustomGeneralLinkField FacebookURL
{
    get
    {
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{2BB916AD-D0C7-4B29-8F2B-11213EE413A5}"]);
    }
}


public CustomCheckboxField HasaPlayground
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{2788044F-B513-4AB1-93AE-EEFA85DE2888}"]);
    }
}


//Could not find Field Type for Price Override Text


public CustomLookupField ThursdayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{66DF801D-8856-495E-B06D-B6F03A75B790}"]);
    }
}


public CustomTextField CommunityDescription
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{A5ED4D7D-091E-497E-83DD-E8A446D2576D}"]);
    }
}


public CustomIntegerField CommunityLatitude
{
    get
    {
        return new CustomIntegerField(InnerItem, InnerItem.Fields["{E93CDA2F-225C-4772-A861-49CC5D19BE1B}"]);
    }
}


public CustomLookupField FridayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{2E530854-4C5A-435A-A6F0-C8E815EA3764}"]);
    }
}


public CustomCheckboxField HasaGolfCourse
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{69EF7AE1-7C73-4B00-B1D9-D1E184E3E412}"]);
    }
}


public CustomGeneralLinkField YouTubeURL
{
    get
    {
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{A82FB79F-839A-499C-8E45-9618DB4A8CD7}"]);
    }
}


public CustomTextField ColumnTwoHeading
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{FC42BEB5-F2E2-4776-A701-558E637BBED1}"]);
    }
}


public CustomTextField CommunityDescription2
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{D59D387A-2FB1-4456-AED0-FB0C002B1C8A}"]);
    }
}


public CustomLookupField FridayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{400D6277-E840-409C-8181-BE0E3E38BD04}"]);
    }
}


public CustomCheckboxField HasTennis
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{18B94D81-BDD1-46A0-9E80-7C915AD09C0D}"]);
    }
}


public CustomGeneralLinkField PinterestURL
{
    get
    {
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{B784B83F-9D72-4CA2-B05C-6CBCA5CF939A}"]);
    }
}


public CustomCheckboxField UseTwoColumnLayoutforDescription
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{47693779-BFEA-4275-8FFE-749230B864B9}"]);
    }
}


public CustomTextField CommunityPPCDescription
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{F7EBA09D-4099-4FD2-995C-858FD9BA8C47}"]);
    }
}


public CustomGeneralLinkField GooglePlusURL
{
    get
    {
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{9F93028B-AED3-4A41-B88B-42D8E319E62B}"]);
    }
}


public CustomCheckboxField HasSoccer
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{3CAE2797-9E67-420F-8256-BB38CC0507EB}"]);
    }
}


public CustomLookupField SaturdayOpen
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{7708D2D4-742B-473D-8D1C-8913437DEF5D}"]);
    }
}


public CustomGeneralLinkField BlogURL
{
    get
    {
        return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{3753FBF1-5C6D-4FA6-8831-1AAE9440B35B}"]);
    }
}


public CustomTextField ExtendedCommunityStatusInformation
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{0AE0C121-044F-472E-87C8-DA1CEF24DCC4}"]);
    }
}


public CustomCheckboxField HasVolleyball
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{33DE740B-0A91-424E-8AE3-C0CEE2DEA656}"]);
    }
}


public CustomLookupField SaturdayClose
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{BE39228C-5328-4C79-9A3F-8CD641F6FFE3}"]);
    }
}


public CustomTextField CommunityDisclaimerImage
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{C4CAF2C0-9BB3-4466-A86B-21A080714C8E}"]);
    }
}


public CustomCheckboxField HasBasketball
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{783A8E23-9622-4A23-B5AE-EFE817A514E4}"]);
    }
}


public CustomLookupField ImageSource
{
    get
    {
        return new CustomLookupField(InnerItem, InnerItem.Fields["{6CF4A315-DC3D-4204-93BF-AADCBA58A3BC}"]);
    }
}


public CustomCheckboxField UseOfficeHoursText
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{0E62F955-1561-46E6-99AF-A498AF32C760}"]);
    }
}


public CustomImageField CommunityLogo
{
    get
    {
        return new CustomImageField(InnerItem, InnerItem.Fields["{15F0C785-FE1E-4C7A-B55F-E3C41C010BBB}"]);
    }
}


public CustomCheckboxField HasBaseball
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{628C5681-9E5E-4FF6-B078-77FA5984E9B3}"]);
    }
}


public CustomTextField OfficeHoursText
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{8AB47205-F486-45FF-91EF-1C5872273B86}"]);
    }
}


public CustomFileField CommunityBrochure
{
    get
    {
        return new CustomFileField(InnerItem, InnerItem.Fields["{806ACD9D-28D4-40AC-8212-8DEE89DA14A6}"]);
    }
}


public CustomCheckboxField HasaView
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{3208B045-DB39-4AF4-B4F7-5A7FE28123A5}"]);
    }
}


public CustomCheckboxField HasaLake
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{03DFC729-6DD2-4A87-80CB-18204D541996}"]);
    }
}


public CustomTextField MLSAreaCode
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{ACD7319D-9A0C-47FD-9AF4-73F465E8E7CF}"]);
    }
}


public CustomCheckboxField HasaPond
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{A845B92C-1C4A-454C-8BAB-1000184A058F}"]);
    }
}


public CustomIntegerField PointsofInterestRadius
{
    get
    {
        return new CustomIntegerField(InnerItem, InnerItem.Fields["{59E4D55C-2ACB-4D52-ABA4-F1DA0BAFBFDA}"]);
    }
}


public CustomIntegerField CommunityLongitude
{
    get
    {
        return new CustomIntegerField(InnerItem, InnerItem.Fields["{F9EF22EC-E06A-423D-82D9-FEA6250F1B32}"]);
    }
}


public CustomFileField ConstructionUpdates
{
    get
    {
        return new CustomFileField(InnerItem, InnerItem.Fields["{0F81C0CE-4806-4582-9A1F-492B6C6967B8}"]);
    }
}


public CustomCheckboxField HasaMarina
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{FBA2638F-BB7C-4271-BD80-49E0FA7FB689}"]);
    }
}


public CustomCheckboxField HasaBeach
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{49DDFE6A-2473-4280-8904-52ED9DB51CB7}"]);
    }
}


public CustomTextField LeftSectionBackgroundTransparencyColor
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{679BD6B3-5C7B-4864-8EB0-BA32C239368C}"]);
    }
}


public CustomTextField StreetAddress1
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{8ABE4570-3889-421F-B058-E822F124A9EC}"]);
    }
}


public CustomCheckboxField IsWaterfront
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{999CBC8C-EBDA-455C-AD68-9E119E0F1401}"]);
    }
}


public CustomTextField LeftSectionBackgroundBoxColor
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{D61B6F61-C413-4BA4-ADB4-C420540E8F8C}"]);
    }
}


public CustomTextField StreetAddress2
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{4CC8A387-3A48-4B12-9A5E-0731F56F4272}"]);
    }
}


public CustomCheckboxField HasaPark
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{47968999-CD07-4FAC-9EA4-7CA39C60D837}"]);
    }
}


public CustomImageField LeftSectionTitleImage
{
    get
    {
        return new CustomImageField(InnerItem, InnerItem.Fields["{20AB91D8-2B6A-43E5-8953-D7A26EC73921}"]);
    }
}


public CustomCheckboxField HasTrails
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{D8CEBE77-C8BE-40DE-B2AB-779C3F9123C2}"]);
    }
}


public CustomTextField LeftSectionLinktoDivisionColor
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{49AD8239-C27B-4754-8455-CAFAFDDEB300}"]);
    }
}


public CustomImageField AlternateImage
{
    get
    {
        return new CustomImageField(InnerItem, InnerItem.Fields["{D38D09EC-2BBA-400E-AB04-005DA066D7F3}"]);
    }
}


public CustomCheckboxField HasaGreenbelt
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{91F69F77-7650-4300-A28C-5E4036CBBA61}"]);
    }
}


public CustomTextField ZiporPostalCode
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{2B751C9F-97A8-4600-9183-518030FCF62D}"]);
    }
}


public CustomCheckboxField HasaClubhouse
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{4F665248-CB4F-42CB-888A-30F65A2B8166}"]);
    }
}


public CustomTextField Phone
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{0191FACD-1F50-418D-B2BA-B9DF6629D345}"]);
    }
}


public CustomCheckboxField SendToBHI
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{A32F3D14-A1ED-4E22-8C90-8F61069F6216}"]);
    }
}


public CustomTextField Fax
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{9E553DBC-3DE9-490E-8A6D-964D73932078}"]);
    }
}


public CustomCheckboxField ShowFinanceLink
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{E9FA4CA2-37A2-42E1-9DC6-5082C9F34B96}"]);
    }
}


public CustomCheckboxField UseCoordinatesforRouteGeneration
{
    get
    {
        return new CustomCheckboxField(InnerItem, InnerItem.Fields["{5772BD01-CFC2-46E8-A7D0-3128E7BF0544}"]);
    }
}


#endregion //Field Instance Methods

#region Additional Fields

public CustomTextField CommunityName
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{19A3D680-967E-4AA4-B5AF-16BB85A0BFF8}"]);
    }
}


public CustomMultiListField  SlideShowImage
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{F0BAD41A-AFD3-4D19-9840-752F9A782F0C}"]);
    }
}

public CustomMultiListField HomeFeaturesImages
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{8670850B-AD12-42BB-8018-2B6A04677714}"]);
    }
}


public CustomMultiListField LocalInterestImages
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{7E7F7999-614C-44B8-A107-48AF1D4A84BA}"]);
    }
}


public CustomTextField ProjectIDs
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{8A4D356B-6781-45BB-AE44-BF81808C7060}"]);
    }
}


public CustomIntegerField SalesOfficeID
{
    get
    {
        return new CustomIntegerField(InnerItem, InnerItem.Fields["{6EDE3E8D-1301-4662-B55A-68F61449A8C2}"]);
    }
}

#endregion


public string GetOfficeHours()
    {
        if (this.UseOfficeHoursText)
            return this.OfficeHoursText;
        var officeHours = new StringBuilder();
        officeHours.AppendFormat("Sunday {0} ", GetTime(this.SundayOpen.Item, this.SundayClose.Item));
        officeHours.AppendFormat("Monday {0} ", GetTime(this.MondayOpen.Item, this.MondayClose.Item));
        officeHours.AppendFormat("Tueday {0} ", GetTime(this.TuesdayOpen.Item, this.TuesdayClose.Item));
        officeHours.AppendFormat("Wednesday {0} ", GetTime(this.WednesdayOpen.Item, this.WednesdayClose.Item));
        officeHours.AppendFormat("Thursday {0} ", GetTime(this.ThursdayOpen.Item, this.ThursdayClose.Item));
        officeHours.AppendFormat("Friday {0} ", GetTime(this.FridayOpen.Item, this.FridayClose.Item));
        officeHours.AppendFormat("Saturday {0} ", GetTime(this.SaturdayOpen.Item, this.SaturdayClose.Item));
        return officeHours.ToString();
    }

    private string GetTime(Item open, Item closed)
    {
        if (open == null|| closed==null || string.IsNullOrWhiteSpace(open.DisplayName) || string.IsNullOrEmpty(closed.DisplayName))
        {
            return "Closed";
        }
        return string.Format("{0} to {1}", open.DisplayName, closed.DisplayName);
    }
}
}