﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
   public class IFPInfo
    {
       public string Name { get; set; }
       public DateTime Date { get; set; }
       public string ID { get; set; }
       public string Url { get; set; }
       public string PlanID { get; set; }
    }
}
