using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class HomeFeaturesCategoryItem : CustomItem
{

public static readonly string TemplateId = "{A703DBB6-5DAE-4D8D-BDD9-B80CA6F8619E}";


#region Boilerplate CustomItem Code

public HomeFeaturesCategoryItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator HomeFeaturesCategoryItem(Item innerItem)
{
	return innerItem != null ? new HomeFeaturesCategoryItem(innerItem) : null;
}

public static implicit operator Item(HomeFeaturesCategoryItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{6BCE5553-A084-49E5-9755-C36FA942A92D}"]);
	}
}


public CustomTextField Code
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{C0B881F4-9E8A-48AF-AD08-D7DC6530F058}"]);
	}
}


public CustomTextField CMSId
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2E58018B-052B-4789-8C61-C271DF2852AD}"]);
	}
}


#endregion //Field Instance Methods
}
}