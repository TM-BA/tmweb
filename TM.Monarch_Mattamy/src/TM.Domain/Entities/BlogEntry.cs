﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class BlogEntry
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        private int commentsSummary;

        public int CommentsSummary
        {
            get { return commentsSummary; }
            set { commentsSummary = value; }
        }

        private string url;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }


    }
}
