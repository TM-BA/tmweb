using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class MasterPlanItem : CustomItem
{

public static readonly string TemplateId = "{11E21296-E52A-40BC-8CA5-2F00570E1800}";

#region Inherited Base Templates

private readonly PlanBaseItem _PlanBaseItem;
public PlanBaseItem PlanBase { get { return _PlanBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public MasterPlanItem(Item innerItem) : base(innerItem)
{
	_PlanBaseItem = new PlanBaseItem(innerItem);

}

public static implicit operator MasterPlanItem(Item innerItem)
{
	return innerItem != null ? new MasterPlanItem(innerItem) : null;
}

public static implicit operator Item(MasterPlanItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField PlanName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{78BFB7B7-BAF7-4CB2-972F-5BF2EA470F9E}"]);
	}
}


public CustomTextField PlanLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{3892C48D-4118-4344-847C-625BB1163136}"]);
	}
}


public CustomLookupField MasterPlanStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{A0465E38-EB02-4B30-A154-F32B4101BD95}"]);
	}
}


public CustomIntegerField PlanWidth
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{1AAADBB6-737F-4B2A-9A5D-76217BF4C07A}"]);
	}
}


public CustomLookupField VirtualToursMasterPlanID
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{44746F44-E5DC-442C-9523-0FB7E497A7F7}"]);
	}
}


public CustomIntegerField PlanDepth
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{1741FF9D-7E80-4AC4-9B4B-099553ABCC51}"]);
	}
}


public CustomLookupField BHIProductType
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{D04FD962-1314-4672-A193-C449A33D743C}"]);
	}
}


public CustomLookupField BHIProductDisplayType
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{DC02911C-D142-48F1-B68E-0EBDD6E8627A}"]);
	}
}


//Could not find Field Type for Plan Brochures


#endregion //Field Instance Methods
}
}