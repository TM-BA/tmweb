using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class TMBaseItem : CustomItem
{

public static readonly string TemplateId = "{252B9C98-4EA7-4F86-AA32-EEC865DC0F50}";


#region Boilerplate CustomItem Code

public TMBaseItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator TMBaseItem(Item innerItem)
{
	return innerItem != null ? new TMBaseItem(innerItem) : null;
}

public static implicit operator Item(TMBaseItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField TitleTemplate
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{FA08ABC2-5C1A-44BC-A628-C38D67C51B40}"]);
	}
}


public CustomTextField PageTitle
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{D9CE4A8F-D837-4551-8D47-8102A14F69DC}"]);
	}
}


public CustomTextField Keywords
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{86668977-F1DC-4AD4-B566-DD8681FAEADE}"]);
	}
}


public CustomTextField Description
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{F1B2966F-1F31-4FD4-AA41-AA50E9B021D5}"]);
	}
}


public CustomTextField Robots
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{77C0AF16-BDF1-4BEE-A62D-66E0B8CA2ED8}"]);
	}
}


public CustomTextField MetaAdditional
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{FAF7811F-D24B-4D7A-8B57-0E4494DEA0AF}"]);
	}
}


public CustomTextField HeadScriptsAndLinks
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{B30E2459-53B3-472A-BA31-64FA131BF882}"]);
	}
}


public CustomTextField AfterBeginOfBodyTag
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{E069827B-2176-4D38-8204-2934AACD4DB6}"]);
	}
}


public CustomTextField BeforeEndOfBodyTag
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{928C849F-46E2-4E9A-9825-0EC99CCF8A37}"]);
	}
}


public CustomTextField BeforeEndOfHtmlTag
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{41E4A3AC-BEE1-4E42-B117-5C1D4C2E1506}"]);
	}
}


public CustomCheckboxField IncludeInSitemap
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{BCCA7E6C-3F94-45E1-866B-1F0F567D53AD}"]);
	}
}


public CustomCheckboxField IncludeInSitemapXML
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{99F4FAB1-1540-40A3-ABBA-1817BA23D57D}"]);
	}
}


#endregion //Field Instance Methods
}
}