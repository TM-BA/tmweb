using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class PlanItem : CustomItem
{

public static readonly string TemplateId = "{8722CBBA-4A10-41FB-B6DD-6E56051EF666}";

#region Inherited Base Templates

private readonly PlanBaseItem _PlanBaseItem;
public PlanBaseItem PlanBase { get { return _PlanBaseItem; } }
private readonly PlanBaseAdditionalItem _PlanBaseAdditionalItem;
public PlanBaseAdditionalItem PlanBaseAdditional { get { return _PlanBaseAdditionalItem; } }
private readonly PlanAndHomeForSaleOnlyBaseItem _PlanAndHomeForSaleOnlyBaseItem;
public PlanAndHomeForSaleOnlyBaseItem PlanAndHomeForSaleOnlyBase { get { return _PlanAndHomeForSaleOnlyBaseItem; } }
private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public PlanItem(Item innerItem) : base(innerItem)
{
	_PlanBaseItem = new PlanBaseItem(innerItem);
	_PlanBaseAdditionalItem = new PlanBaseAdditionalItem(innerItem);
	_PlanAndHomeForSaleOnlyBaseItem = new PlanAndHomeForSaleOnlyBaseItem(innerItem);
	_TMBaseItem = new TMBaseItem(innerItem);

}

public static implicit operator PlanItem(Item innerItem)
{
	return innerItem != null ? new PlanItem(innerItem) : null;
}

public static implicit operator Item(PlanItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField PlanName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{83D8146D-EFEB-46B6-BDBA-C6143AAF401B}"]);
	}
}


public CustomTextField PlanLegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{EE1CF1B1-89B3-441D-AFC8-7C4FFC75FF2B}"]);
	}
}


public CustomLookupField MasterPlanID
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{5BCDCFDF-BF2A-4A83-8299-6BA4DEEFB9F0}"]);
	}
}


public CustomIntegerField PricedfromValue
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{655C6D93-AA58-4B55-BE17-2FDD82C6226F}"]);
	}
}


public CustomTextField PricedfromText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2EECD67E-895E-4482-BDE1-159FD9A8CBFA}"]);
	}
}


public CustomLookupField PlanStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{BF332405-E397-4F22-9543-BB4BE1D1FB0C}"]);
	}
}


public CustomLookupField StatusforAdvertising
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{F2519957-0B16-4EA0-ADCE-0F8C9AC498EE}"]);
	}
}


#endregion //Field Instance Methods



}
}