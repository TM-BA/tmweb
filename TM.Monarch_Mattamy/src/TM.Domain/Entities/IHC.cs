﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class IHC
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Registration { get; set; }

        public IHC(string ContactName, string ContactPhone, string ContactRegistration)
        {
            Name = ContactName;
            Phone = ContactPhone;
            Registration = ContactRegistration;
        }
    }
}
