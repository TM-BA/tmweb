namespace TM.Domain.Entities
{
    public class HomeAttributes
    {
        //see:HomeAttributeExtension
        public bool IsCurrentItemAPlan { get; set; }

        public int HalfBath { get; set; }

        public int Bathrooms { get; set; }

        public int Garage { get; set; }

        public int Stories { get; set; }

        public int Bedrooms { get; set; }

        public int SqFt { get; set; }

        public int Price { get; set; }

        public string Description { get; set; }

        public double BathCountForDisplay
        {
            get { return Bathrooms + (HalfBath > 0 ? 0.5 : 0); }
        }

        public string PriceText { get; set; }
    }
}