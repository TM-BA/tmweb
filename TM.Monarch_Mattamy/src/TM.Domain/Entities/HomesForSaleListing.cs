﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
   public  class HomesForSaleListing
    {
       public string Address { get; set; }
       public string Price { get; set; }

       public string Bed { get; set; }
       public double Bath { get; set; }
       public string Stories { get; set; }
       public string Garage { get; set; }
       public string Availability { get; set; }

       public string Link { get; set; }
    }
}
