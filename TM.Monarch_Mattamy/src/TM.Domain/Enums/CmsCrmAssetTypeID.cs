﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Enums
{
    public enum CmsCrmAssetTypeID
    {
        Elevationrendering =1,
        Elevationphoto,
        Floorplan,
        Interactivefloorplan,
        Interiorphoto,
        Exteriorphoto,
        Brochure,
        Virtualtour,
        Communityphoto,
        Communityincentiveimage,
        Communityfeaturedimage,
        Communitylogo,
        Communitybackdropphoto,
        Communitysiteplan,
        Communityinfospotimage,
        Communitylogowithgradient,
        Divisionvisual,
        Userphoto,
        Communityinfoleftphoto,
        Communityinforightphoto,
        Communitybrochure,
        CommunitySlideShowPhoto,
        NewCommunityPhotoSmall,
        NewCommunityPhotoLarge,
        HomeFeatureImagePool,
        LocalInterestImagePool,
        SchoolImagePool,
        ConstructionUpdateFile,
        IncentivePhoto,
        IncentivePDF

    }
}
