﻿<%@ Page Language="c#" Inherits="System.Web.UI.Page" CodePage="65001" Debug="true"%>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
  <title>Welcome to Sitecore</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="CODE_LANGUAGE" content="C#" />
  <meta name="vs_defaultClientScript" content="JavaScript" />
  <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
  <link href="/default.css" rel="stylesheet" />
</head>
<body> 
  <form id="mainform" method="post" runat="server">
    <div id="MainPanel">
      <sc:placeholder key="main" runat="server" /> 
    </div>
  </form>
</body>
</html>

<script runat="server" language="C#">
    private void Page_Load(object sender, EventArgs args)
    {        
        string path = Sitecore.Configuration.Settings.Media.FileFolder;        
        path = Sitecore.IO.FileUtil.MapPath(path);
        var files = System.IO.Directory.EnumerateFiles(path,"*.*",System.IO.SearchOption.AllDirectories);
		foreach(var file in files)
		{
		   if(IsFileNotBeingUsedAsMediaItem(file))
		   {
		       try
		       {
		           Sitecore.IO.FileUtil.Delete(file);
		       }
		       catch
		       {
		       }  
		   }
		}
		
	
	}
   
    private bool IsFileNotBeingUsedAsMediaItem(string file)
    {
        string name = Sitecore.Resources.Media.MediaPathManager.GetMediaName(file);
        if (name.IndexOf('{') >= 0)
        {
            string id = name.Substring(name.IndexOf('{'), name.IndexOf('}') + 1);
            Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");
            return (master.GetItem(Sitecore.Data.ID.Parse(id)) == null);
        }
        return false;
    }    
</script>
