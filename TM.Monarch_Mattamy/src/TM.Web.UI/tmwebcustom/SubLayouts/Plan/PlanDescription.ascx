﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlanDescription.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlanDescription" %>
<div class="plan-middle1">
    <hr/>
						<h1>
						 <%= PlanName %>
                        	</h1>
						<span><%= PlanInfo %></span>
						<p>
						    <sc:Text ID="scPlanDescription" runat="server" Field="Plan Description"/>
							</p>
							<p>
							<%= TourOptionsText %>
							</p>
						<ul class="plan2but" id="ulScheduleAnAppt" runat="server">
							<li><a href="<%= CommunityRequestInfo %>">Schedule an Appointment</a></li>
						</ul>
					</div>
                    
                    <!--end plan-middle1-->