﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashPageSocialMediaShare.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.SplashPageSocialMediaShare" %>
<span style="font-size: 16px; font-weight: bold; color: #19398a;"><sc:Text ID="txtTitle" runat="server" Field="Title"/></span>
<br/>
<br/>
    <div style="float: left; margin-right: 5px;">
        <asp:Literal runat="server" ID="litFacebook"></asp:Literal>
    </div>
    <asp:Literal runat="server" ID="litGoogle"></asp:Literal>  
    <asp:Literal runat="server" ID="litTwitter"></asp:Literal>     
    
    
<br/>
<br/>
<div><sc:Text ID="txtContent" runat="server" Field="Content"/></div>
<br/>
<span runat="server" Visible="False" id="spanNoDataSource">No datasource is associated with this Social Media Share Component</span>