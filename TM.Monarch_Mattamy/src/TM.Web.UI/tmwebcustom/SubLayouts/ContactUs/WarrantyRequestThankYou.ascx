﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WarrantyRequestThankYou.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.WarrantyRequestThankYou" %>
<div class="gr-bk">
  <p>Thank You </p>        
</div>
<div class="create-acc">

<div class="next-steps">
   <sc:fieldrenderer runat="server" id="ContentArea" fieldname="Content Area"></sc:fieldrenderer>
</div>

<div class="ways-connect">
<h1>More Ways To Connect</h1>
<asp:Literal ID="litSocialMedia" runat="server"></asp:Literal>
<asp:Literal ID="litIhc" runat="server"></asp:Literal>
<asp:Literal ID="litBlogLink" runat="server"></asp:Literal>
</div>
</div>