﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UniversalRFIThankYou.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.UniversalRFIThankYou" %>
<div class="gr-bk">
       	  <p>Thank You </p>        
        </div>
        <div class="create-acc">
<asp:Literal ID="litThankYouText" runat="server"></asp:Literal>
<asp:Literal ID="litAccountCreated" runat="server"></asp:Literal>

<div class="next-steps">
    <h1>Next Steps</h1>
    <asp:Panel ID="pnlCreateAccount" runat="server">
    <p>Provide a password to save communities and homes to your favorites.<br>
        * required information</p>

        <span>Password:</span>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="tm-account"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="newAccount" ControlToValidate="txtPassword" ErrorMessage="You need to provide a password for your account" runat="server" CssClass="error" display="Dynamic"></asp:RequiredFieldValidator>
        <span>Confirm Password:</span>
        <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" CssClass="tm-account"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="newAccount" ControlToValidate="txtPassword2" ErrorMessage="You need to provide a confirmation password for your account" runat="server" CssClass="error" display="Dynamic"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" ValidationGroup="newAccount" ControlToValidate="txtPassword2" ControlToCompare="txtPassword" ErrorMessage="Please confirm you password to continue" runat="server" CssClass="error" Display="Dynamic"></asp:CompareValidator>

    <asp:LinkButton ValidationGroup="newAccount" ToolTip="Create Account" runat="server" ID="lnkBtnCreateAccount" CssClass="create-acc-btn" />
    </asp:Panel>
    <div class="th-bt">
        <asp:Literal ID="litPrivacyPolicy" runat="server"></asp:Literal>
        <p id="pIntroText" runat="server">Want to make sure our emails get to your inbox? <br>
           Be sure to include <%= FromEmail %> in your email address book.</p>
        <div id="nxtStep"><asp:Literal ID="litNextSteps" runat="server"></asp:Literal></div>
        <div id="preQual"><asp:Literal ID="litPreQual" runat="server"></asp:Literal></div>
    </div>
</div>

<div class="ways-connect">
<h1>More Ways To Connect</h1>
<asp:Literal ID="litSocialMedia" runat="server"></asp:Literal>
<asp:Literal ID="litIhc" runat="server"></asp:Literal>
<asp:Literal ID="litBlogLink" runat="server"></asp:Literal>
</div>
</div>

<script>
    $j(document).ready(function () {
        $j("a.InterestItem").click(TM.Common.GAlogevent('Details', 'Click', '<%= InterestItemGALabel %>'));
        $j("a.create-acc-btn").click(TM.Common.GAlogevent('CreateAccount', 'Click', 'CreateAccount'));
        $j(".fb").click(TM.Common.GAlogevent('Social Media', 'Click', 'Facebook'));
        $j(".tw").click(TM.Common.GAlogevent('Social Media', 'Click', 'Twitter'));
        $j("#nxtStep a").click(TM.Common.GAlogevent('HomeBuying', 'Click', 'Homebuyin101'));
        $j("#preQual a").click(TM.Common.GAlogevent('Finance', 'Click', 'PrequalLink'));
    });
</script>