﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformationListing.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.ContactInformationListing" %>

<h1>Local Areas </h1>

<asp:Repeater ID="rptDivisionList" runat="server">
    <ItemTemplate>
        <div style="width: 100%;">
            <p>
                <a id="divisionName" target="_blank" href='<%#Eval("Url") %>' onclick="TM.Common.GAlogevent('Contact Us', 'Click', '<%# Eval("Name") %>');"><%#Eval("Name") %></a>
                <div>
                    <asp:Label ID="fullAddress" runat="server" Text='<%#Eval("FullAddress") %>'> </asp:Label></div>
                <div>
                    <asp:Label ID="cityState" runat="server" Text='<%#Eval("CityState") %>'> </asp:Label></div>
                <div>
                    <asp:Label ID="phoneNumber" runat="server" Text='<%#Eval("Phone") %>'> </asp:Label></div>
            </p>
        </div>
    </ItemTemplate>
</asp:Repeater>

<%--<div>
    <h1>Corporate</h1>
    <p>
        <div>
            <h1>
                <asp:Label ID="CompanyInfo" runat="server" Text=""></asp:Label></h1>
        </div>
        <div>
            <asp:Label ID="CompanyAddress" runat="server" Text=""></asp:Label></div>
        <div>
            <asp:Label ID="CompanyCityStatelbl" runat="server" Text=""></asp:Label></div>
        <div>
            <asp:Label ID="CompanyPhonelbl" runat="server" Text=""></asp:Label></div>
    </p>
</div>--%>
