﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogMenu.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.BlogMenu" %>

 <ul class="blog-mt">
        <li><asp:HyperLink ID="hlkHome" runat="server">HOME</asp:HyperLink></li>    
        <li> <asp:HyperLink ID="hlkArchive" runat="server">ARCHIVE</asp:HyperLink></li>
        <li> <asp:HyperLink ID="hlkContact" runat="server">CONTACT</asp:HyperLink></li>
        <li> <asp:HyperLink ID="hlkSubscribe" runat="server">SUBSCRIBE</asp:HyperLink></li>
        <li> <asp:HyperLink ID="hlkSite" runat="server"></asp:HyperLink></li>
    </ul>