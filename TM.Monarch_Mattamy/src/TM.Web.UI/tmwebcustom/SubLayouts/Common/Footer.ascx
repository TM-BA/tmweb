﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Footer" %>
<%@ Import Namespace="TM.Web.Custom.Constants" %>
<%@ Register TagPrefix="uc" TagName="SEODynamicTree" Src="SEODynamicTree.ascx" %>
<div class="footer-content-wrapper">
    <div class="fot-shad">
    </div>
    <div class="ft-shdw">
    </div>
    <div class="footerGrayBar">
        <ul>
            <li class="iconF1"></li>
            <li class="iconF2"></li>
            <li class="iconF3"></li>
            <li><a id="lnkYoutube" runat="server" class="iconTube" target="_blank"></a></li>
            <li><a id="lnkTwitter" runat="server" class="iconTwitter" target="_blank"></a></li>
            <li><a id="lnkFacebook" class="iconFb" target="_blank" runat="server"></a></li>
            <li><a id="lnkPinterest" runat="server" class="iconPinterest" target="_blank"></a>
            </li>
            <li><a id="lnkGooglePlus" runat="server" class="iconGoogle" target="_blank"></a>
            </li>
            <li><a id="lnkBlog" runat="server" class="iconY" target="_blank"></a></li>
        </ul>
    </div>
    <div class="f_right">
        <div class="foot-in">
            <a href="/General-Faqs" class="footer" style="padding-left: 6px;">FAQ</a> | <a href="/contact-us"
                class="footer">Contact Us</a> | <a href="/sitemap" class="footer">Sitemap</a>
            | <a href="<%=CurrentDevice.Contains("darling") ? "/careers/careers-with-darling-homes" : "/careers/careers-with-taylor-morrison" %>"
                class="footer">Careers</a> |
            <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
               { %>
            <a href="/realtors" class="footer">Realtors</a> |
            <% } %>
            <% if (SCCurrentDeviceType != TM.Domain.Enums.DeviceType.DarlingHomes)
               { %>
            <a href="<%=CurrentDevice.Contains("darling") ? "/our-partners/a-legacy-of-strength" : "/our-partners" %>"
                class="footer">Our Partners</a> |
            <% } %>
            <a href="/press-room" class="footer">Press Room</a> |
            <asp:HyperLink ID="hlkInvestors" runat="server" CssClass="footer" Target="_blank">Investor Relations</asp:HyperLink>
            | <a id="lnkBlogFooter" runat="server" target="_blank" class="footer" onclick="TM.Common.GAlogevent('Blog', 'Click', 'BlogLink');">
                Blog</a>
        </div>
        <div class="foot-in">
            <p style="font-size: 10px; padding-top: 8px;">
                Copyright © 2007 -
                <%=DateTime.Now.ToString("yyyy") %>
                Taylor Morrison, Inc. All rights reserved. <a href="/terms-of-use" class="footer-small">
                    Terms of Use</a> <a href="/Privacy-Policies" class="footer-small">Privacy Policy</a>
            </p>
            
        </div>
        <div class="foot-in logoContainer">
            <div class="logo-bottom">
                <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
                   { %>
                <a href="#">
                    <%-- <asp:Literal runat="server" ID="tm_logo"></asp:Literal> --%>
		    <sc:Image Field="Company Footer Logo"  runat="server"  CssClass="logoFooter1" DataSource="{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}"/>
                </a>
                <% }
                   else
                   { %>
                <a href="<%=(SCContextDB.GetItem(SCIDs.Home.TM_Home)[SCIDs.HomePage.CompanyWebSiteUrl])%>"
                    target="_blank">
                  <!--  <asp:Literal runat="server" ID="tm_logo2"></asp:Literal>-->
		      <sc:Image Field="Company Footer Logo"  runat="server"  CssClass="logoFooter1" DataSource="{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}"/>
                </a>
                <% } %>
             <%--   <span class="logoFooter2"></span><a href="http://www.monarchgroup.net" class="logoFooter3"
                    target="_blank"></a>--%>
                <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
                   { %>
                <a href="#" class="logoFooter4"></a>
                <% }
                   else
                   { %>
                <a href="<%=(SCContextDB.GetItem(SCIDs.Home.DarlingHome)[SCIDs.HomePage.CompanyWebSiteUrl])%>"
                    class="logoFooter4" target="_blank"></a>
                <% } %>
            </div>
            <!--end logo-bottom-->
        </div>
</div>
<div class="f_left">
    <uc:SEODynamicTree runat="server" ID="ucSEODynamicTree" />
</div>
<div style="float: left; clear: both; width: 100%;">
    <asp:Literal ID="litDisclaimer" runat="server"></asp:Literal>
</div>
</div>