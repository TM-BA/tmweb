﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Rotator.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Rotator" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%if (LoadScripts && !IsPrintMode)
  {%>
  
  <%= JsCssCombiner.GetSet("photorotatorjs")%>


<link href="/Styles/lib/slideshow.css" rel="stylesheet" type="text/css" />
<% } %>

<div class="slideshow <%=WrapperClass %>">
    <a Visible="false" ID="hypPrevious" runat="server" href="javascript:;" class="prev-arrow"></a>
		<ul class="carousel <%=ContainerClass %>">
			<asp:Literal ID="litSlideImages" runat="server"></asp:Literal>
		</ul>
    <a Visible="false" ID="hypNext" runat="server" href="javascript:;" class="next-arrow"></a>
	</div>
   
<%if (LoadScripts && !IsPrintMode)
  { %>				
	<script type="text/javascript">
	     NS("TM.Rotator");
	    $j(document).ready(function () {
	        // Create slideshow instances
	       var slides = $j('.slideshow').slides(<%= SlideOptions %>);
	        TM.Rotator.$slidesObj = slides.data('slides');
	        $j("a.expandImg").colorbox({ rel: 'expandImg' });
	    });
      
	</script>
	
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">    stLight.options({ publisher: "f3415b4a-0575-4cae-b53d-be5261aafa02", doNotHash: true, doNotCopy: true, hashAddressBar: false });</script>

  <% } %>
  <%else{%>
  <script type="text/javascript">
      NS("TM.Rotator");
      TM.Rotator.get_slideOptions = function() { return <%= SlideOptions %>; };
  </script>
  <% } %>
