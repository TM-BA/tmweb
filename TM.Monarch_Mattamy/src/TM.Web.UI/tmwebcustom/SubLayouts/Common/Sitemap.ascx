﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sitemap.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Sitemap" %>


<div class="plan-details-wrapper">


     <% if (HideHeader == false){ %>
     <div class="gr-bk">
       <p>Sitemap </p>        
     </div>
     <% } %>

    <div class="create-acc">

         <% if (HideHeader == false){ %>
        <p>Select from the following areas of our site. Need help? <a href="/Contact Us">Contact us</a></p>
         <% } %>
    <div class="spl"> 
        <asp:HyperLink ID="hlk_Header" runat="server"></asp:HyperLink>
    </div>
    <div>
        <div class="sp-lt">
    
    <asp:Repeater ID="rptLeftSection" runat="server">
        <ItemTemplate>
        <%# (Boolean.Parse(Eval("setUL").ToString())==true) ? "<ul class='sitemap'>" : "" %> 
                    <li style ='<%# Eval("CssClass") %>' >
                        <asp:HyperLink ID="hl_item" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Name") %></asp:HyperLink>
                     </li>
        <%# (Boolean.Parse(Eval("setUL").ToString()) == true) ? " </ul>" : ""%> 
      
        </ItemTemplate>
   </asp:Repeater> 
   
   </div>   

        <div class="sp-rt">
         
        <asp:Repeater ID="rptRightSection" runat="server">
        <ItemTemplate>
        <%# (Boolean.Parse(Eval("setUL").ToString())==true) ? "<ul class='sitemap'>" : "" %>
                     <li style ='<%# Eval("CssClass") %>'>
                        <asp:HyperLink ID="hl_item" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Name") %></asp:HyperLink>
                     </li>
        <%# (Boolean.Parse(Eval("setUL").ToString()) == true) ? " </ul>" : ""%>         
        </ItemTemplate>
        </asp:Repeater> 
    
   </div>       
    </div>
   </div>

</div>
