﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftColumn.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.LeftColumn" %>
    <script type="text/javascript">
        $j(window).load(function () {            
            $j("#leftColumn").css("min-height", $j("#rightColumn").height());
        });
    </script>
<div class="lt-col lt-col_mh" id="leftColumn">
	<sc:Placeholder runat="server" ID="phLeftAreaPromo" Key="LeftAreaPromo" />
	<tm:MenuStateProvinceList ID="menustprolist" runat="server" CssClass="ltste" Cacheable="true" VaryByDevice="true"/>
</div>
<div class="rt-col" id="rightColumn">
	<sc:Placeholder runat="server" ID="phStageCampaign" Key="StageCampaign" />
    <sc:Placeholder runat="server" ID="phAccordion" Key="Accordion" />
	<sc:Placeholder runat="server" ID="phBlogTeaser" Key="BlogTeaser" />
</div>
