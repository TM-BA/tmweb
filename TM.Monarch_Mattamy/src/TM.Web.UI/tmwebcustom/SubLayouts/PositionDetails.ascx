﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div><sc:fieldrenderer runat="server" id="fr_Title" fieldname="Title"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_Area" fieldname="Area"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_State" fieldname="State"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_DivisionCode" fieldname="Division code"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_Copy" fieldname="Copy"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_PostingStartDate" fieldname="Posting start date"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_PositionEndDate" fieldname="Position end date "></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_StreetAddress1" fieldname="Street Address 1"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_StreetAddress2" fieldname="Street Address 2"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_City" fieldname="City"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_State_Province" fieldname="State Province"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_Abbreviation" fieldname="Abbreviation"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_RelocationProvided" fieldname="Relocation Provided"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_TravelRequired" fieldname="Travel Required"></sc:fieldrenderer></div>
<div><sc:fieldrenderer runat="server" id="fr_JobType" fieldname="Job Type"></sc:fieldrenderer></div>

