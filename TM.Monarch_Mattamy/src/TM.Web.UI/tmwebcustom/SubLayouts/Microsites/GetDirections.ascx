﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GetDirections.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.GetDirections" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="../Search/SearchMap.ascx" %>
<script type="text/javascript" src="/javascript/search/searchMap.js"></script>
<script type="text/javascript" src="/javascript/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/javascript/lib/markerclusterer.js"></script>
<script type="text/javascript" src="/javascript/lib/jquery.colorbox-min.js"></script>
<div runat="server" ID="googleJS" Visible="False">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.5&key=<%=GoogleMapsApiKey %>&sensor=false"></script>    

</div>

<link href="/Styles/lib/colorbox.css" rel="stylesheet" type="text/css">

<div style="width: 50%; float: left" class="plan-middle3">
    <div><sc:Text ID="txtContent" runat="server" Field="Title Content"/></div>
    <input type="text" id="txtFromAddress" ClientIDMode="Static" placeholder="e.g. 123 House Drive, City, State, Zip" class="dd-sa" style="width: 90%" runat="server" />
        <ul class="dd-but" id="ulGetDirections">
            <li><a href="javascript:void(0);" onclick="calcRoute()">Get Directions</a></li>
        </ul>    
    <div class="dd-list">
        <div id="directions-panel"></div>
    </div>
    <div id="warnings_panel"></div>
</div>
<div id="mapContainer" style="width: 50%; float:right" class="plan-middle4" runat="server" clientidmode="Static">
    <ul class="dd-but" id="callsToAction" runat="server">
        <li><a href="#SendToForm" id="btnEmailDir">Email Directions</a></li>
        <li><asp:HyperLink ID="lnkPrintDirections" runat="server" Target="_blank" rel="nofollow" ClientIDMode="Static">Print Directions</asp:HyperLink></li>
    </ul>
    <div style='display:none;'>
        <div id="SendToForm" style='padding:10px; background:#fff;'>
        <p>Please enter your email address:</p>
            <asp:TextBox ID="toAddress" runat="server" style="width:100%" ValidationGroup="sendEmail"></asp:TextBox>
            <asp:RequiredFieldValidator id="isAddressEntered" ControlToValidate="txtFromAddress" Text="Please tell us where you want directions from on the previous screen." runat="server" ValidationGroup="sendEmail" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator id="isEmailValid" ValidationExpression=".+@.+" ControlToValidate="toAddress" Text="Your email seems odd, please check it?" runat="server" ValidationGroup="sendEmail" Display="Dynamic"></asp:RegularExpressionValidator>
            <ul class="dd-but">
                <li><asp:LinkButton ID="lnkbEmailDirections" runat="server" CausesValidation="true" ValidationGroup="sendEmail" OnClientClick="if(Page_ClientValidate('sendEmail')){$j('#SendToForm').appendTo($j('form'));}else{return false}" OnClick="lnkbEmailDirections_Click" >Send Directions</asp:LinkButton></li>
            </ul>
            <a href="#" onclick="$j.colorbox.close()" style="padding-top:17px;float:right;">Cancel</a>
        </div>
	</div>
    <uc:SearchMap ID="ucSearchMap" runat="server" />
</div>
<span style="display: none" id="spanNoDataSource">No datasource or neighborhoods are associated with this map control</span>
<script type="text/javascript">
    var directionDisplay;
    var directionsService;
    var communityJson = <%= CommunityJSON %>;
    var mapCanvas;
    $j(function () {
        
        $j("#btnEmailDir").colorbox({inline:true, width:'350px',height:'250px'});
        
        if (communityJson.length > 0) {
            if ($j('#mapContainer').length > 0) {
                $j('#mapContainer').show();
                $j('#spanNoDataSource').hide();
                initializeMicroSiteMap(communityJson);
            } else {
                $j('#txtFromAddress').hide();
                $j('#ulGetDirections').hide();
            }
            if ($j('#txtFromAddress').val() != "") {
                calcRoute();
            }
        } else {
            $j('#mapContainer').hide();
            $j('#spanNoDataSource').show();
        }

    });

    function initializeMicroSiteMap(communities) {
        
        // Create a map and center it on Manhattan.
        var myCommunity = new google.maps.LatLng(communityJson[0].CommunityLatitude, communityJson[0].CommunityLongitude);
        var myOptions = {
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: myCommunity
        };
        mapCanvas = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        //var dirMarkerOptions = {
        //icon : new google.maps.Symbol("CIRCLE")
        //}
        var communityMarker = new google.maps.Marker({
                position: myCommunity,
                map: mapCanvas,
                icon: "/images/icon-house.png",
                animation: google.maps.Animation.DROP

        });

        var contentString = makeFlyout(communityJson[0]);

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        google.maps.event.addListener(communityMarker, 'click', function() {
            infowindow.open(mapCanvas,communityMarker);
        });
        
    }

    function makeFlyout(community) {
            var content = "<div id=\"content\"><strong>" +
                community.CommunityName+"</strong><br/>"+
                community.StreetAddress1 + "<br/>" +
                community.City + ", " + community.StateProvinceAbbreviation + " " + community.ZipPostalCode + "<br/></div>" ;
            content += "";

            return content;
        };

    function calcRoute() {
        // Instantiate a directions service.
        directionsService = new google.maps.DirectionsService();
        var rendererOptions = {
            map: mapCanvas
            //,markerOptions : dirMarkerOptions
            //, suppressMarkers: true
        }
        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)
        directionsDisplay.setPanel(document.getElementById('directions-panel'));
            
        var start = '';
        

        // Retrieve the start and end locations and create
        // a DirectionsRequest using WALKING directions.
        start = document.getElementById("txtFromAddress").value;
        var end = new google.maps.LatLng(communityJson[0].CommunityLatitude, communityJson[0].CommunityLongitude);
        
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        // Route the directions and pass the response to a
        // function to create markers for each step.
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var warnings = document.getElementById("warnings_panel");
                warnings.innerHTML = "<b>" + response.routes[0].warnings + "</b>";
                directionsDisplay.setDirections(response);
                //showSteps(response);
            }
        });

        StoreAddress(start);
    }
    function StoreAddress(start){
        var printBtn = document.getElementById("lnkPrintDirections");
        if (printBtn) {
            var indexOfStartAddress = printBtn.href.indexOf("startAddress");
            if(indexOfStartAddress ==-1){
                printBtn.href +=  "&startAddress=" + start;
            } else {
                printBtn.href = printBtn.href.substring(0,indexOfStartAddress) + "startAddress=" + start;
            }
        }
        
    }
</script>
<style>
.mapWindow {
height: 432px;
width: 100%;
margin: 0;
border: 1px solid #dcdcdc;
}

.srchmap {
float: left;
margin: 0;
padding: 0;
width: 99.9%;
position: relative;
}
</style>