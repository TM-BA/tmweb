﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductMenu.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.ProductMenu" %>
<%@ Import Namespace="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders" %>

<script type="text/javascript">
    $j(function () {

    });
    function openChild(event, item) {
        var i = $j(item).find('span');
        i = $j(i[0]);
        
        if (i.hasClass('plus-sign')) {
            i.removeClass('plus-sign');
            i.addClass('minus-sign');
            $j($j(item).find('ol')[0]).show();
            
        } else {
            i.removeClass('minus-sign');
            i.addClass('plus-sign');
            $j(item).find('ol').hide();
            $j(item).find('span').removeClass('minus-sign');
            $j(item).find('span').addClass('plus-sign');
        }
        event.stopPropagation();
    }

    function openMain(event, item) {
        var i = $j(item);
        if (i.hasClass('expand')) {
            i.removeClass('expand');
            $j('.innerdivi').hide();
            $j('.innerdivi').find('span').removeClass('minus-sign');
            $j('.innerdivi').find('span').addClass('plus-sign');
            $j('.innerdivi').find('ol').hide();
        } else {
            i.addClass('expand');
            $j('.innerdivi').show();
        }
        event.stopPropagation();
    }
</script>


<div class="lt-col" style="width: 100%;">
	<section class="hm-lt">
	    <sc:Text ID="txtContent" runat="server" Field="Title Content"/>
    </section>
	
    <ul class="ltste" runat="server" id="ulPlansTitle"><li class="expand" onclick="openMain(event, this);" id="liPlansTitle"><a href="javascript:void(0);"><sc:Text ID="txtPlansTitle" runat="server" Field="Floorplans Home for Sale Title"/></a></li></ul>
	<asp:Repeater runat="server" ID="repCommunities">

        <HeaderTemplate>
            <ol class="innerdivi" style="display: block;">
        </HeaderTemplate>
        <ItemTemplate>
            <li onclick="openChild(event, this);">
                <span id="firstLevelItem" class="plus-sign" style='<%# renderTag(Eval("ChildList")) %>'> </span> <a href='<%#Eval("Url")%>' target="_blank" style='<%#Eval("Style")%>'><%#Eval("Name")%></a>
                <asp:Repeater runat="server" ID="repLevelOne" DataSource='<%#Eval("ChildList")%>'>
                    <HeaderTemplate>
                        <ol class="innerdivi2" id="firstLevelItemOL">
                    </HeaderTemplate>
                    <ItemTemplate>
                            <li onclick="openChild(event, this);" >
                                <span id="secondLevelItem" class="plus-sign" style='<%# renderTag(Eval("ChildList")) %>'> </span> <a href='<%#Eval("Url")%>' target="_blank" style='<%#Eval("Style")%>'><%#Eval("Name")%></a>
                                <asp:Repeater runat="server" ID="repLevelTwo" DataSource='<%#Eval("ChildList")%>'>
                                    <HeaderTemplate>
                                        <ol class="innerdivi2" id="secondLevelItemOL">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li onclick="openChild(event, this);">
                                            <span id="thirdLevelItem" class="plus-sign" style='<%# renderTag(Eval("ChildList")) %>'> </span> <a href='<%#Eval("Url")%>' target="_blank" style='<%#Eval("Style")%>'><%#Eval("Name")%></a>
                                            <asp:Repeater runat="server" ID="repLevelThree" DataSource='<%#Eval("ChildList")%>'>
                                                <HeaderTemplate>
                                                    <ol class="innerdivi3" id="thirdLevelItemOL">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <a href='<%#Eval("Url")%>' target="_blank" style='<%#Eval("Style")%>'><%#Eval("Name")%></a>
                                                    </li>   
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ol>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </li>   
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ol>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ol>
                    </FooterTemplate>
                </asp:Repeater>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Repeater runat="server" ID="repLinks">
    <ItemTemplate>
        <ul class="ltsteNoArrow" style='<%#Eval("Style")%>'> <li><a href='<%#Eval("Url")%>' target="_blank"><%#Eval("Name")%></a></li></ul>
    </ItemTemplate>
    </asp:Repeater>
					
</div>