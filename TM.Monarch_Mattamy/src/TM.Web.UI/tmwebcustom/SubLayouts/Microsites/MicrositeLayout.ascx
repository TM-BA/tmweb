﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MicrositeLayout.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.MicrositeLayout" %>
<div class="row">
	<div class="col-3">
		<sc:Placeholder runat="server" ID="phOneThird" Key="SplashPageOneThird1"/>
	</div>
	<div class="col-6">
		<sc:Placeholder runat="server" ID="phTwoThird" Key="SplashPageTwoThird1"/>
	</div>
</div>
<div class="row">
	<div class="col-6">
	    
		<sc:Placeholder runat="server" ID="Placeholder1" Key="SplashPageTwoThird2"/>
	</div>
	<div class="col-3">
		<sc:Placeholder runat="server" ID="Placeholder2" Key="SplashPageOneThird2"/>
	</div>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth" Key="SplashPageFullWidth1"/>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="Placeholder8" Key="mapComponent"/>
</div>
<div class="row">
	<div class="col-3">
		<sc:Placeholder runat="server" ID="Placeholder3" Key="SplashPageOneThird3"/>
	</div>
	<div class="col-6">
		<sc:Placeholder runat="server" ID="Placeholder4" Key="SplashPageTwoThird3"/>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<sc:Placeholder runat="server" ID="Placeholder5" Key="SplashPageTwoThird4"/>
	</div>
	<div class="col-3">
		<sc:Placeholder runat="server" ID="Placeholder6" Key="SplashPageOneThird4"/>
	</div>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="Placeholder7" Key="SplashPageFullWidth2"/>
</div>