﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedItems.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.SavedItems" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Import Namespace="TM.Web.Custom.WebControls" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="~/tmwebcustom/SubLayouts/Search/SearchMap.ascx" %>
<%=JsCssCombiner.GetSet("saveditemsjs") %>
<script>
	$j(document).ready(function () {
		var comfav = "<asp:Literal ID='litComFav' runat='server' />";
		var planfav = "<asp:Literal ID='litPlanFav' runat='server' />";
		var hfsfav = "<asp:Literal ID='litHfsFav' runat='server' />";
		$j(window).load(function () { GetSaveItemResults(comfav, planfav, hfsfav); });

		$j("#comSortby_taylormorrison").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#comSortby_taylormorrison").val(), "Community");
		});

		$j("#planSortby_taylormorrison").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#planSortby_taylormorrison").val(), "Plan");
		});

		$j("#homeforsaleSortby_taylormorrison").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#homeforsaleSortby_taylormorrison").val(), "HomeForSale");
		});

		$j("#comSortby_monarchgroup").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#comSortby_monarchgroup").val(), "Community");
		});

		$j("#planSortby_monarchgroup").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#planSortby_monarchgroup").val(), "Plan");
		});

		$j("#homeforsaleSortby_monarchgroup").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#homeforsaleSortby_monarchgroup").val(), "HomeForSale");
		});

		$j("#comSortby_darlinghomes").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#comSortby_darlinghomes").val(), "Community");
		});

		$j("#planSortby_darlinghomes").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#planSortby_darlinghomes").val(), "Plan");
		});

		$j("#homeforsaleSortby_darlinghomes").change(function () {
			SortSaveItemResults(comfav, planfav, hfsfav, $j("#homeforsaleSortby_darlinghomes").val(), "HomeForSale");
		});

		$j("a#findyournewhome").click(function () {
			$j("a#findyournewhome").last().addClass("selected");
			$j('#svgmap').show();
		});
		$j("a#svgclose").click(function () {
			$j('#svgmap').hide();
			$j("a#findyournewhome").removeClass("selected");
		});

		$j("a#msignupleadform").click(function () {
			$j('.UpdatesBoxModel').toggle();
		});
	});

	function SignUpModel(id) {
		$j('div#updatesBoxModel_' + id).toggle();
	}
</script>
<uc:SearchMap ID="ucSearchMap" runat="server" />

<script id="tmplCommunity" type="text/x-jquery-tmpl">
	<div id="Community_${CommunityID}">
	<div class="crossCard">
		<div class="si-img2">
			<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}"><img src="${CommunityCardImage}?h=90&w=158" alt="${CommunityName}" onError="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';"></a>
			{{if CommunityImageStatusFlag.length > 0 }}
				<div class="comstsflag"><img src="${CommunityImageStatusFlag}" alt="${CommunityStatus}"></div>
			{{/if}}
		</div>
		<div class="commName-c">
			<div class="redMarker-c">{{html MapIcon}}</div>
			<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}"  class="card_tcross">${Name}</a><br>
		</div>
		<div class="priceFromSpace-c">
			{{html NoLongerAvailableText}}
		</div>
		<div class="commLocation-c">
			<p>${StreetAddress1} ${StreetAddress2} ${City}, ${StateProvinceAbbreviation} ${ZipPostalCode}<br>${Phone}</p>
		</div>
		<div class="commDetails-c">
			<p>
				{{if SquareFootage == "" && (Bedrooms == "" || Bedrooms == "0 Bed") && (Bathrooms == "" || Bathrooms == "0 Bath") && (Stories == "" || Stories == "0 Story Home") && (Garage == "" || Garage =="0 Car Garage" )}}
					Details available soon.
				{{else}}
					{{if SquareFootage != ""}}${SquareFootage.formatMoney(0, ".", ",")}<br />{{/if}}
					{{if (Bedrooms == "" || Bedrooms == "0 Bed")}}{{else}}${Bedrooms}<br />{{/if}}
					{{if (Bathrooms == "" || Bathrooms == "0 Bath")}}{{else}}${Bathrooms}<br />{{/if}}
					{{if (Stories == "" || Stories == "0 Story Home")}}{{else}}${Stories}<br />{{/if}}
					{{if (Garage == "" || Garage =="0 Car Garage")}}{{else}}${Garage}{{/if}}
				{{/if}}
			</p>
		</div>
		<div class="commPromotions-c">
			<ul class="msibuttons">
				<li id="liveChat" class="chat-btn">
					{{html LiveChatButton}}
				</li>
				<li><a onclick="TM.Common.GAlogevent('Finance', 'Click', 'Finance')" {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/request-information/">Sign Up For Updates</a></li>
			</ul>
		</div>
		<div class="clearfix">
		</div>
		<ul class="si-bbr">
			<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}">View Details</a></li>
			<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/Request-Information">Request Info</a></li>
			<li><a href="javascript:RemoveFavorites('${CommunityID}', 'Community', 'Remove', '${Domain}')">Remove from Favorites</a></li>
		</ul>
	</div>
	</div>
</script>
<script id="tmplPlan" type="text/x-jquery-tmpl">
	<div id="Plan_${PlanID}">
		<div class="si-cc2">
				<div class="si-img2">
				<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${PlanPageUrl}>">
					<img src="${ElevationPlanImage}" onerror="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';">
				</a>
			</div>
			<div class="redMarker-si">
				{{html MapIcon}}
			</div>
			<p class="msi-t">
				<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${PlanPageUrl}">${PlanName}</a> at <a href="${CommunityDetailsURL}">${CommunityName}</a>
						asasasdasdasd
			        <a href="${PlanPageUrl}">${PlanName} personalized on </a> at <a href="${CommunityDetailsURL}">Remove</a>
				<a href="${PlanPageUrl}">${PlanName} personalized on </a> at <a href="${CommunityDetailsURL}">Remove</a>
				<a href="${PlanPageUrl}">${PlanName} personalized on </a> at <a href="${CommunityDetailsURL}">Remove</a>
			</p>
			<div class="si-details-c">
				{{if SquareFootage != ""}}
					${SquareFootage.formatMoney(0, ".", ",")} Sq. Ft.
				{{/if}}
				{{if NumberofBedrooms != ""}} |
					{{if NumberofBedrooms > 1}}
						${NumberofBedrooms} Beds
					{{else}}
						${NumberofBedrooms} Bed
					{{/if}}
				{{/if}}
				{{if NumberofBathrooms != ""}} |
					{{if NumberofBathrooms > 1}}
						${NumberofBathrooms}{{if NumberofHalfBathrooms > 0}} .5 Baths {{else}} Baths {{/if}}
					{{else}}
						${NumberofBathrooms}{{if NumberofHalfBathrooms > 0}} .5 Bath {{else}} Bath {{/if}}
					{{/if}}
				{{/if}}
				{{if NumberofGarages != ""}} |
						${NumberofGarages} Garage
				{{/if}}
				{{if NumberofStories != ""}} |
					{{if NumberofStories > 1}}
						${NumberofStories} Stories
					{{else}}
						${NumberofStories} Story
					{{/if}}
				{{/if}}
			</div>
			<div class="msi-btn">
				<ul class="msibuttons">
					<li id="liveChat" class="chat-btn">
						{{html LiveChatButton}}
					</li>
					<li><a onclick="TM.Common.GAlogevent('Finance', 'Click', 'Finance')" href="${FinanceYourDream}">Finance your Dream</a></li>
				</ul>
			</div>
			{{html NoLongerAvailableText}}
			</div>
			<ul class="si-bar">
				<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${PlanPageUrl}">View Details</a></li>
				<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${PlanPageUrl}/Request-Information">Request Info</a></li>
				<li><a href="javascript:RemoveFavorites('${PlanID}', 'Plan', 'Remove', '${Domain}')">Remove from Favorites</a></li>
			</ul>
	</div>
</script>
<script id="tmplHfs" type="text/x-jquery-tmpl">
	<div id="HomeForsale_${HomeforSaleID}">
		<div class="si-cc2">
		<div class="si-img2">
			<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${HomeforSaleDetailsURL}">
				<img src="${ElevationImages}" onerror="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';">
			</a>
		</div>
		<div class="redMarker-si">
			{{html MapIcon}}
		</div>
		<p class="msi-t">
			<a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}}>${HomeforSalePlanName} at ${StreetAddress1}, ${City}, ${State}</a> at <a href="${HomeforSaleCommunityDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}}>${HomeforSaleCommunityName}</a>

			  <a href="${PlanPageUrl}">${PlanName} personalized on 11/12/2013</a> at <a href="${CommunityDetailsURL}">Remove</a>
			  <a href="${PlanPageUrl}">${PlanName} personalized on 11/12/2013</a> at <a href="${CommunityDetailsURL}">Remove</a>
			  <a href="${PlanPageUrl}">${PlanName} personalized on 11/12/2013</a> at <a href="${CommunityDetailsURL}">Remove</a>
		</p>
		<div class="si-details-c">
			{{if SquareFootage != ""}}
				${SquareFootage.formatMoney(0, ".", ",")} Sq. Ft.
			{{/if}}
			{{if NumberofBedrooms != ""}} |
				{{if NumberofBedrooms > 1}}
					${NumberofBedrooms} Beds
				{{else}}
					${NumberofBedrooms} Bed
				{{/if}}
			{{/if}}
			{{if NumberofBathrooms != ""}} |
				{{if NumberofBathrooms > 1}}
					${NumberofBathrooms}{{if NumberofHalfBathrooms > 0}} .5 Baths {{else}} Baths {{/if}}
				{{else}}
					${NumberofBathrooms}{{if NumberofHalfBathrooms > 0}} .5 Bath {{else}} Bath {{/if}}
				{{/if}}
			{{/if}}
			{{if NumberofGarages != ""}} |
					${NumberofGarages} Garage
			{{/if}}
			{{if NumberofStories != ""}} |
				{{if NumberofStories > 1}}
					${NumberofStories} Stories
				{{else}}
					${NumberofStories} Story
				{{/if}}
			{{/if}}
		</div>
		<div class="msi-btn">
			<ul class="msibuttons">
				<li id="liveChat" class="chat-btn">
					{{html LiveChatButton}}
				</li>
				<li><a onclick="TM.Common.GAlogevent('Finance', 'Click', 'Finance')" href="${FinanceYourDream}">Finance your Dream</a></li>
			</ul>
		</div>
		{{html NoLongerAvailableText}}
		</div>
		<ul class="si-bar">
			<li><a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}}>
				View Details</a></li>
			<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${HomeforSaleDetailsURL}/Request-Information">Request Info</a></li>
			<li><a href="javascript:RemoveFavorites('${HomeforSaleID}', 'HomeForsale', 'Remove', '${Domain}')">Remove from Favorites</a></li>
		</ul>
	</div>
</script>
<div id="noComRecords" class="noComRecords" runat="server" visible="false">
	You have no saved homes for sale, plans, or communities in your account. <a id="findyournewhome" href="#">
		Search for homes</a>.
</div>
<asp:PlaceHolder runat="server" ID="phAllfavorites">
	<asp:Repeater ID="rptCompanyHeader" runat="server" OnItemDataBound="rptCompanyHeader_ItemDataBound">
		<ItemTemplate>
			<div class="msi">
				<h1>
					<asp:Literal runat="server" ID="litCompanyhder"></asp:Literal></h1>
			</div>
			<asp:PlaceHolder runat="server" ID="phComfavo">
				<div class="si-header">
					<div id="community_<%# Eval("Domain")%>">
					</div>
					<asp:Literal runat="server" ID="litCommunityLinks"></asp:Literal>
				</div>
				<div class="searcross">
					<div class="searcross1">
						Sort by:</div>
					<select id="comSortby_<%# Eval("Domain")%>" name="comSortby_<%# Eval("Domain")%>"
						class="si-f">
						<option value="1_<%# Eval("Domain")%>" selected="selected">Community name</option>
						<option value="2_<%# Eval("Domain")%>">Price low to high</option>
						<option value="3_<%# Eval("Domain")%>">Price high to low</option>
						<option value="4_<%# Eval("Domain")%>">Location</option>
					</select>
					<div class="searcross3">
						COMMUNITY</div>
					<div class="searcross4">
						LOCATION</div>
					<div class="searcross5">
						DETAILS</div>
					<div class="searcross6">
						NEXT STEPS</div>
				</div>
				<div id="favCommunitycard_<%# Eval("Domain")%>">
					<asp:Repeater ID="rptCommunityCard" runat="server">
						<ItemTemplate>
							<div id="Community_<%# Eval("CommunityID") %>">
								<div class="crossCard">
									<div class="si-img2">
										<a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
											href="<%# Eval("CommunityDetailsURL")%>">
											<img src="<%# Eval("CommunityCardImage")%>?h=90&w=158" alt="<%# Eval("CommunityName")%>"
												onerror="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';"></a>
										<%# Eval("CommunityImageStatusFlag") != ""  ? "<div class=\"comstsflag\"><img src=" + Eval("CommunityImageStatusFlag") + " alt=" + Eval("CommunityStatus") +"></div>" : string.Empty%>
									</div>
									<div class="commName-c">
										<div class="redMarker-c">
											<%# Eval("MapIcon")%></div>
										<a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
											href="<%# Eval("CommunityDetailsURL")%>" class="card_tcross">
											<%# Eval("Name")%></a><br>
									</div>
									<div class="priceFromSpace-c">
										<%#Eval("NoLongerAvailableText")%>
										<%--<span class="priceFrom-c"><%# Eval("CommunityPrice")%></span>--%>
									</div>
									<div class="commLocation-c">
										<p>
											<%# Eval("StreetAddress1")%>
											<%# Eval("StreetAddress2")%>
											<%# Eval("City")%>,
											<%# Eval("StateProvinceAbbreviation")%>
											<%# Eval("ZipPostalCode")%><br />
											<%# Eval("Phone")%>
										</p>
									</div>
									<div class="commDetails-c">
										<p>
											<%# (Eval("SquareFootage") == "" && (Eval("Bedrooms") == "" || Eval("Bedrooms") == "0 Bed") && (Eval("Bathrooms") == "" || Eval("Bathrooms") == "0 Bath") && (Eval("Stories") == "" || Eval("Stories") == "0 Story Home") && (Eval("Garage") == "" || Eval("Garage") == "0 Car Garage")) ? "Details available soon.  <a href=\"Contactus();\">Contact us</a> to find out more." : "" %>
											<%# Eval("SquareFootage") != "" ? string.Format("{0:n0}",Eval("SquareFootage"))  + "<br />" : string.Empty%>
											<%# Eval("Bedrooms") != "" ? Eval("Bedrooms") + "<br />" : string.Empty%>
											<%# Eval("Bathrooms") != "" ? Eval("Bathrooms") + "<br />" : string.Empty%>
											<%# Eval("Stories") != "" ? Eval("Stories") + "<br />" : string.Empty%>
											<%# Eval("Garage") != "" ? Eval("Garage") + "<br />" : string.Empty%>
										</p>
									</div>
									<div class="commPromotions-c">
										<ul class="msibuttons">
											<li id="liveChat" class="chat-btn">
												<%# Eval("LiveChatButton")%>
											</li>
											<li><a onclick="TM.Common.GAlogevent('Updates', 'Click', 'SignUpForUpdates')" <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %> href="<%# Eval("CommunityDetailsURL")%>/Request-Information">Sign Up For Updates</a></li>
										</ul>
									</div>
									<div class="clearfix">
									</div>
									<ul class="si-bbr">
										<li><a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
											href="<%# Eval("CommunityDetailsURL")%>">View Details</a></li>
										<li><a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %> href="<%# Eval("CommunityDetailsURL")%>/Request-Information">Request Info</a></li>
										<li><a href="javascript:RemoveFavorites('<%# Eval("CommunityID") %>', 'Community', 'Remove', '<%# Eval("Domain")%>')">
											Remove from Favorites</a></li>
									</ul>
								</div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="phPlanfavo">
				<div class="si-header">
					<div id="plan_<%# Eval("Domain")%>">
					</div>
					<asp:Literal runat="server" ID="litPlanLinks"></asp:Literal>
				</div>
				<div class="msigray">
					<span class="soby">Sort by:</span>
					<select id="planSortby_<%# Eval("Domain")%>" name="planSortby_<%# Eval("Domain")%>"
						class="si-f">
						<option selected="selected" value="2_<%# Eval("Domain")%>">Price low to high</option>
						<option value="3_<%# Eval("Domain")%>">Price high to low</option>
						<option value="5_<%# Eval("Domain")%>">Plan Name</option>
						<option value="7_<%# Eval("Domain")%>">Number of bedrooms</option>
						<option value="6_<%# Eval("Domain")%>">Square Footage</option>
						<option value="9_<%# Eval("Domain")%>">Stories</option>
					</select>
					<span class="deta-s">DETAILS</span> <span class="nex-s">NEXT STEPS</span>
				</div>
				<div id="favPlancard_<%# Eval("Domain")%>">
					<asp:Repeater ID="rptPlanCard" runat="server">
						<ItemTemplate>
							<div id="Plan_<%# Eval("PlanID") %>">
								<div class="si-cc2">
									<div class="si-img2">
										<a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>href="<%# Eval("PlanPageUrl")%>">
											<img src="<%# Eval("ElevationPlanImage") %>" onerror="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';">
										</a>
									</div>
									<div class="redMarker-si">
										<%# Eval("MapIcon")%>
									</div>
									<p class="msi-t">
										<a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
											href="<%# Eval("PlanPageUrl")%>">
											<%# Eval("PlanName")%></a> at <a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>href="<%# Eval("CommunityDetailsURL")%>">
												<%# Eval("CommunityName")%></a>

												<div class="ifpFavs">
												    <asp:Repeater ID="rptPlanIFPs" runat="server" DataSource="<%#((PlanInfo)Container.DataItem).IFPs %>" Visible="<%#((PlanInfo)Container.DataItem).IFPs.Count>0 %>">
												        <ItemTemplate>
												         <span id="span_<%# Eval("ID")%>">
												         <a href="<%# Eval("Url")%>" target="_blank">
													 <%# Eval("Name")%> personalized on <%# Eval("Date")%>
													 </a>
													 <a href="javascript:void();" onclick="removeIFP('<%# Eval("ID")%>');return false;" class="sslinf">remove</a>
													 <br>
													 </span>
													</ItemTemplate>
												    </asp:Repeater>
												</div>
									</p>
									<div class="si-details-c">
										<%# Eval("SquareFootage") != "" ? string.Format("{0:n0}", Eval("SquareFootage")) + " Sq. Ft." : string.Empty%>
										<%# Eval("NumberofBedrooms") != "" && (int)Eval("NumberofBedrooms") > 0 ? " | " + ((int)Eval("NumberofBedrooms") > 1 ? Eval("NumberofBedrooms") + " Beds" : Eval("NumberofBedrooms") + " Bed") : string.Empty%>
										<%# Eval("NumberofBathrooms") != "" && (int)Eval("NumberofBathrooms") > 0 ? " | " + ((int)Eval("NumberofBathrooms") > 1 ? Eval("NumberofBathrooms") + ((int)Eval("NumberofHalfBathrooms") > 0 ? ".5" : "") + " Baths" : Eval("NumberofBathrooms") + ((int)Eval("NumberofHalfBathrooms") > 0 ? ".5" : "") + " Bath") : string.Empty%>
										<%# Eval("NumberofGarages") != "" && (int)Eval("NumberofGarages") > 0 ? " | " + Eval("NumberofGarages") + " Garage" : string.Empty%>
										<%# Eval("NumberofStories") != "" && (int)Eval("NumberofStories") > 0 ? " | " + ((int)Eval("NumberofStories") > 1 ? Eval("NumberofStories") + " Stories" : Eval("NumberofStories") + " Story") : string.Empty%>
									</div>
									<div class="msi-btn">
										<ul class="msibuttons">
											<li id="liveChat" class="chat-btn">
												<%# Eval("LiveChatButton")%>
											</li>
											<li><a href="<%# Eval("FinanceYourDream")%>">Finance your Dream</a></li>
										</ul>
									</div>
									<%#Eval("NoLongerAvailableText")%>
									<%--<div class="si-price">
									<span>Priced from</span> <span>
										<%# Eval("PricedfromValue")%></span>
								</div>
								<div class="msi-call">
									<span>Call:
										<%# Eval("Phone")%></span>
								</div>--%>
								</div>
								<ul class="si-bar">
									<li><a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
										href="<%# Eval("PlanPageUrl")%>">View Details</a></li>
									<li><a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %> href="<%# Eval("PlanPageUrl")%>/Request-Information">Request Info</a></li>
									<li><a href="javascript:RemoveFavorites('<%# Eval("PlanID") %>', 'Plan', 'Remove', '<%# Eval("Domain")%>')">
										Remove from Favorites</a></li>
								</ul>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</asp:PlaceHolder>
			<asp:PlaceHolder runat="server" ID="phHomeforSalefavo">
				<div class="si-header">
					<div id="hfs_<%# Eval("Domain")%>">
					</div>
					<asp:Literal runat="server" ID="litHomeforSaleLinks"></asp:Literal>
				</div>
				<div class="msigray">
					<span class="soby">Sort by:</span>
					<select id="homeforsaleSortby_<%# Eval("Domain")%>" name="homeforsaleSortby_<%# Eval("Domain")%>"
						class="si-f">
						<option value="2_<%# Eval("Domain")%>">Price low to high</option>
						<option value="3_<%# Eval("Domain")%>">Price high to low</option>
						<option value="5_<%# Eval("Domain")%>">Plan Name</option>
						<option selected="selected" value="8_<%# Eval("Domain")%>">Availability</option>
						<option value="7_<%# Eval("Domain")%>">Number of bedrooms</option>
						<option value="6_<%# Eval("Domain")%>">Square Footage</option>
						<option value="9_<%# Eval("Domain")%>">Stories</option>
					</select>
					<span class="deta-s">DETAILS</span> <span class="nex-s">NEXT STEPS</span>
				</div>
				<div id="favHfsCard_<%# Eval("Domain")%>">
					<asp:Repeater ID="rptHomeforSaleCard" runat="server">
						<ItemTemplate>
							<div id="HomeForsale_<%# Eval("HomeforSaleID") %>">
								<div class="si-cc2">
									<div class="si-img2">
										<a href="<%# Eval("HomeforSaleDetailsURL")%> <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>">
											<img src="<%# Eval("ElevationImages") %>" onerror="this.onerror=null;this.src='/Images/img-not-available.jpg?h=90&w=158';">
										</a>
									</div>
									<div class="redMarker-si">
										<%# Eval("MapIcon")%>
									</div>
									<p class="msi-t">
										<a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
											href="<%# Eval("HomeforSaleDetailsURL") %>">
											<%# Eval("HomeforSalePlanName")%> at
											<%# Eval("StreetAddress1") %>,
											<%# Eval("City") %>,
											<%# Eval("State") %></a> at <a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>
												href="<%# Eval("HomeforSaleCommunityDetailsURL")%>">
												<%# Eval("HomeforSaleCommunityName")%></a>

												<div class="ifpFavs">
												    <asp:Repeater ID="rptHFSIFPs" runat="server" DataSource="<%#((HomeForSaleInfo)Container.DataItem).IFPs %>" Visible="<%#((HomeForSaleInfo)Container.DataItem).IFPs.Count>0 %>">
												        <ItemTemplate>
												         <span id="span_<%# Eval("ID")%>">
												         <a href="<%# Eval("Url")%>" target="_blank">
													 <%# Eval("Name")%> personalized on <%# Eval("Date")%>
													 </a>
													 <a href="javascript:void();" onclick="removeIFP('<%# Eval("ID")%>');return false;" class="sslinf">remove</a>
													 <br>
													 </span>
													</ItemTemplate>
												    </asp:Repeater>
												</div>
									</p>
									<div class="si-details-c">
                                    <%# AvalabilityText( Eval("HomeforSaleID").ToString() ) %><br />
										<%# Eval("SquareFootage") != "" ? string.Format("{0:n0}", Eval("SquareFootage")) + " Sq. Ft." : string.Empty%>
										<%# Eval("NumberofBedrooms") != "" && (int)Eval("NumberofBedrooms") > 0 ? " | " + ((int)Eval("NumberofBedrooms") > 1 ? Eval("NumberofBedrooms") + " Beds" : Eval("NumberofBedrooms") + " Bed") : string.Empty%>
										<%# Eval("NumberofBathrooms") != "" && (int)Eval("NumberofBathrooms") > 0 ? " | " + ((int)Eval("NumberofBathrooms") > 1 ? Eval("NumberofBathrooms") + ((int)Eval("NumberofHalfBathrooms") > 0 ? ".5" : "") + " Baths" : Eval("NumberofBathrooms") + ((int)Eval("NumberofHalfBathrooms") > 0 ? ".5" : "") + " Bath") : string.Empty%>
										<%# Eval("NumberofGarages") != "" && (int)Eval("NumberofGarages") > 0 ? " | " + Eval("NumberofGarages") + " Garage" : string.Empty%>
										<%# Eval("NumberofStories") != "" && (int)Eval("NumberofStories") > 0 ? " | " + ((int)Eval("NumberofStories") > 1 ? Eval("NumberofStories") + " Stories" : Eval("NumberofStories") + " Story") : string.Empty%>
									</div>
									<div class="msi-btn">
										<ul class="msibuttons">
											<li id="liveChat" class="chat-btn">
												<%# Eval("LiveChatButton")%>
											</li>
											<li><a href="<%# Eval("FinanceYourDream")%>">Finance your Dream</a></li>
										</ul>
									</div>
									<%#Eval("NoLongerAvailableText")%>
									<%--<div class="si-price">
									<span>
										<%# Eval("Availability")%></span> <span>
											<%# Eval("Price") %></span>
								</div>
								<div class="msi-call">
									<span>Call:
										<%# Eval("Phone")%></span>
								</div>--%>
								</div>
								<ul class="si-bar">
									<li><a href="<%# Eval("HomeforSaleDetailsURL")%> <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>">
										View Details</a></li>
									<li><a <%# (Boolean.Parse(Eval("IsCurrentDomain").ToString())) ? "" : "target='_blank'" %>" href="<%# Eval("HomeforSaleDetailsURL") %>/Request-Information">Request Info</a></li>
									<li><a href="javascript:RemoveFavorites('<%# Eval("HomeforSaleID") %>', 'HomeForsale', 'Remove', '<%# Eval("Domain")%>')">
										Remove from Favorites</a></li>
								</ul>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</asp:PlaceHolder>
		</ItemTemplate>
	</asp:Repeater>
</asp:PlaceHolder>