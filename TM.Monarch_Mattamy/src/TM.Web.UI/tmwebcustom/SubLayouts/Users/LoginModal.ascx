﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginModal.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.LoginModal" %>
<style>
	.create-acc {padding: 0 20px 10px;}
	.errbx {margin: 10px 0;padding: 7px;}
</style>
<div class="gr-bk">
	<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
			<img src="<%=MycompanyLogo %>"/>
		</div>
	<p>
		Account
	</p>
</div>
<div class="create-acc">
	<h1>
		Login to your <%=MycompanySmall%> account</h1>
	<p>
		Need a new account? Please <a href="/Account/Register">register now</a> to continue.</p> 
	
	<div id="usermodalerror" style="display:none; " class="errbx">We did not find a user with the email and password provided.  Please try again or request your password</div>
	<asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx"  EnableClientScript="True" ShowSummary="True" 
	DisplayMode="SingleParagraph" Enabled="True" HeaderText="We did not find a user with the email and password provided.  Please try again or request your password." ShowMessageBox="False"/>
	<span>Email Address *</span>
	<asp:TextBox runat="server" ID="txtUsername" CssClass="tm-account"></asp:TextBox>	
	<asp:RequiredFieldValidator runat="server" ID="requsername" ControlToValidate="txtUsername" EnableClientScript="True"></asp:RequiredFieldValidator>
	<span>Password *</span>
	<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="tm-account"></asp:TextBox>  
	<asp:RequiredFieldValidator runat="server" ID="reqPassword" ControlToValidate="txtPassword" EnableClientScript="True"></asp:RequiredFieldValidator>
	<div class="bl">
		<asp:HiddenField runat="server" ID="hdnId" />
		<asp:HiddenField runat="server" ID="hdnType" />
		<asp:HiddenField runat="server" ID="hdnAction" />
		<asp:HiddenField runat="server" ID="hdnSentto"/>
		<a href="javascript:ValidateLogin('<%= txtUsername.ClientID%>','<%= txtPassword.ClientID%>', '<%=hdnId.Value %>', '<%=hdnType.Value%>', '<%=hdnAction.Value%>', '<%=hdnSentto.Value %>');" class="button-arrow">Login</a>
		<a href="/Account/Forgot-Password">Forgot your password?</a>
	</div>
</div>
<script>
	function ValidateLogin(user, pwd, id, type, action, sentto) {
		if (LoginModal(user, pwd) == 1) {
			AddRemoveFavoritesbyModal(id, type, action, sentto);
		} 
	}
</script>
