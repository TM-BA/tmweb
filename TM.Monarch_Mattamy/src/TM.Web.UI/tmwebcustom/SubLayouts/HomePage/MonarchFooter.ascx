﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonarchFooter.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.MonarchFooter" %>
<%@ Import Namespace="TM.Web.Custom.Constants" %>
<div class="footer-content-wrapper">
    <div class="ft-shdw">
    </div>
    <div class="footerGrayBar">
        <ul>
            <li class="iconF1"></li>
            <li class="iconF2"></li>
            <li><a id="lnkYoutube" runat="server" class="iconTube" target="_blank"></a></li>
            <li><a id="lnkTwitter" runat="server" class="iconTwitter" target="_blank"></a></li>
            <li><a id="lnkFacebook" class="iconFb" target="_blank" runat="server"></a></li>
            <li><a id="lnkPinterest" runat="server" class="iconPinterest" target="_blank"></a>
            </li>
            <li><a id="lnkGooglePlus" runat="server" class="iconGoogle" target="_blank"></a>
            </li>
            <li><a id="lnkBlog" runat="server" class="iconY" target="_blank"></a></li>
        </ul>
    </div>
    <div class="f_right">
        <div class="foot-in">
            <a href="/contact-us" class="footer">Contact Us</a> | <a href="/sitemap" class="footer">
                Sitemap</a> | <a href="/careers/careers-with-monarch" class="footer">Careers</a>
            | <a href="/press-room" class="footer">Press Room</a> |
            <asp:HyperLink ID="hlkInvestors" runat="server" CssClass="footer" Target="_blank">Investor Relations</asp:HyperLink>
            | <a id="Blog" target="_blank" class="footer" runat="server" onclick="TM.Common.GAlogevent('Blog', 'Click', 'BlogLink');">
                Blog</a><br />
            <p style="font-size: 10px; padding-top: 8px;">
                Copyright © 2007 -
                <%=DateTime.Now.ToString("yyyy") %>
                Monarch (Mattamy) Limited. All rights reserved. <a href="/terms-of-use" class="footer-small">
                    Terms of Use</a> <a href="/Privacy-Policy" class="footer-small">Privacy Policy</a>
            </p>
        </div>
    </div>
    <div class="f_left">
        <%--<div class="logo95">
					</div>--%>
        <sc:Sublayout ID="scSEODynamicTree" Path="~/tmwebcustom/SubLayouts/Common/SEODynamicTree.ascx"
            runat="server" />
    </div>
    <div style="float: left; clear: both; width: 100%;">
        <asp:Literal ID="litDisclaimer" runat="server"></asp:Literal>
    </div>
</div>