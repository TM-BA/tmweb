﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompressCommunityHeader.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CompressCommunityHeader" %>
<%@ Import Namespace="TM.Web.Custom.Constants" %>
<%@ Import Namespace="TM.Web.Custom.SCHelpers" %>
<div class="dd-top">
	<div class="plan-top1">
		<h1><%=CommunityName %></h1>
        <span>
			<asp:DropDownList ID="drpNavigator" runat="server" onchange="changeDrpNav()"></asp:DropDownList>
		</span>
	</div>
	<!--end plan-top1-->
	<div class="plan-top2">
		<div class="s-center">
			<% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
            { %> <h1>Sales Center:</h1> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
            {  %>  <h1>Sales Centre:</h1> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
            {  %>  <h1>Sales Center:</h1> <% } %>
			<p class="address"><span class="clearRightText"><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>"><%=Address1 %></a></span>
                <span class="clearRightText"><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>"><%=Address2 %></a></span>
                <span><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>"><%= CityName %>, <%= State %> <%=Zip %></a></span></p>
		    <asp:HyperLink ID="lnkDrivingDirections" runat="server" NavigateUrl="">Driving Directions</asp:HyperLink> <br/>
			<a href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print" class="print" target="_blank"  rel="nofollow">Print Page</a>
		</div>
		<!--end s-center-->
		<div class="s-associate" id="sass" runat="server">
			<h1>Sales Contact:</h1>
			<p>
			<%--<span class="clearRightText"><%=IHCName %></span>--%>
                <span class="clearRightText ihc-phone"><a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a></span>
                <span class="clearRightText ihc-phone"><a href="tel:<%=IHCPhone%>"><%=IHCPhone%></a></span>
                
                <%--<span class="clearRightText"><%=IHCRegistration%></span>--%>
            </p>
		</div>
		<!--end s-associate-->
	</div>
	<!--end plan-top2-->
	<div class="plan-top3" id="col3" runat="server">
		<ul class="plan1but">
			<li><a href="<%=CurrentPage.SCVirtualItemUrl%>/request-information/" onclick="TM.Common.GAlogevent('Updates','Click','SignUpForUpdates')">Sign Up For Updates </a></li>
			<li id="liveChat" class="chat-btn"><sc:Placeholder ID="Placeholder1" runat="server" Key="LiveChat" /></li>
			
		</ul>
		
	</div>
	<!--end plan-top3-->
					
</div>

<script type="text/javascript">
    function changeDrpNav() {
        var selectObject = document.getElementById("<%=drpNavigator.ClientID %>");
        var selectedIndex = selectObject.selectedIndex;
        document.location.href = selectObject.options[selectedIndex].value;
        TM.Common.GAlogevent('Details', 'Click', selectObject.options[selectedIndex].text);
 
    }

</script>
