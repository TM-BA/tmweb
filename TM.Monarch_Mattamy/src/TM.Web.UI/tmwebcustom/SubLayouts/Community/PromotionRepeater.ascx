﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromotionRepeater.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.PromotionRepeater" %>
<script type="text/javascript">
	//Data for home cards
	<%= PromoRepeaterData %>
	$j(document).ready(function () {
    
	ko.applyBindings(new PromosModel(PromoInitialData), document.getElementById('promoRepeater'));

    $j("#promoRepeater a").click(function(){TM.Common.GAlogevent('Details', 'Click', 'PromotionURL')});

	});

    var PromosModel = function (Promos) {
		var self = this;
		self.Promos = ko.observableArray(ko.utils.arrayMap(Promos,
		function (promo) {
			return { 
				img: promo.img
				,url: promo.url
				,name: promo.name
				,content: promo.content
                ,target: promo.target
                ,hasURL : promo.hasURL
			};
		}
	));
	};
</script>
<asp:Label ID="lblNoResults" Text="Sorry No promotions are currently available" runat="server"></asp:Label>
<asp:Literal ID="litPrintHeader" runat="server"></asp:Literal>
<div id="promoRepeater" data-bind="foreach: Promos">
    <div class="promo_all">
        <div class="promoBlock">
            <div class="promoImg">
                <img data-bind="attr:{'src':img}" width="171" height="116"></div>
            <div class="promoText">
                <!-- ko template: hasURL ? 'hasURLTmplt' : 'hasntURLTmplt' -->
                <!-- /ko -->
                <script id="hasURLTmplt" type="text/html">
                     <a class="sc"  data-bind="text:name,attr:{href:url, target:target}"></a>
                </script>
                <script id="hasntURLTmplt" type="text/html">
                    <span><strong  data-bind="text:name"></strong></span>                    
                </script>
                <p data-bind="html:content">
                </p>
            </div>
        </div>
    </div>
</div>
</div>
