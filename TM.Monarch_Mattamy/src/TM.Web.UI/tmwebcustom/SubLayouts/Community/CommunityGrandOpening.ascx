﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityGrandOpening.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityGrandOpening" %>
    
<div class="lt-col-cd">
<section class="hm-lt_sr">
    <div class="banner-go">
        <div class="banner" style="float: right;">
            <sc:Placeholder ID="scImageRotator" runat="server" Key="Slides" />
        </div>
        <div class="across-lt" id="bannerLeftDiv" runat="server">
            <div class="go-logo">
                <sc:Image ID="Image1" Field="Community Logo" runat="server" />
            </div>
            <div class="go-date" id="goDate" runat="server">
                <h1>
                    Grand Opening</h1>
                <p>
                    <sc:Text ID="Text1" runat="server" Field="Extended Community Status Information" />
                </p>
            </div>
            
            <div id="callsToAction" class="callsToAction" runat="server">
                <ul class="ltnavbut">
                    <li><a href="<%=CurrentPage.SCVirtualItemUrl%>/request-information/">Ask a Question</a></li>
                </ul>
                <ul class="ltnavlnk">
                    <li class="fav"><a href="#" id="fav_<%=SCContextItem.ID.ToString().Substring(1,36) %>"
                        onclick="toggleCommunityFavorties(this,'<%=SCContextItem.ID %>');TM.Common.GAlogevent('Favorites','Click','AddToFavorites');">
                        <%=ComFavText%></a></li>
                    <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                        rel="nofollow">Print</a></li>
                </ul>
            </div>
            <div class="go-all-c ccs" id="goAllCom" runat="server">
                <asp:HyperLink ID="hypAreaCommunities" runat="server">View all <%= Division %> Area Communities</asp:HyperLink>
            </div>
        </div>
    </div>    
</section>
</div>

<div class="det-wrapper-bt">
    <div class="cd_left">
        <div class="geneInfo">
            <h1>
                GENERAL INFORMATION</h1>
        </div>
        <div class="geneInfoBox">
            <h2>
                Sales Center:
            </h2>
            <p class="address">
                <span class="clearRightText"><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>">
                    <%=Address1 %></a></span> <span class="clearRightText"><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>">
                        <%=Address2 %></a></span> <span><a href="https://www.google.com/maps?q=<%=Address1 %>,<%=Address2 %>,<%= CityName %>, <%= State %> <%=Zip %>">
                            <%= CityName %>,
                            <%= State %>
                            <%=Zip %></a></span><br />
                <span><a href="tel:<%=LocationPhone%>">
                    <%=LocationPhone%></a></span>
            </p>
            <asp:HyperLink CssClass="announ" ID="drivingDirections" runat="server">Driving Directions</asp:HyperLink>
            <div class="grayLineS">
            </div>
            <h2>
                Sales Team:
            </h2>
            <p>
                <%=MainIHCName %><br />
                <span class="ihc-phone">
                    <%=MainIHCPhone%></span><br />
                <%=MainIHCRegistration %>
            </p>
            <div class="grayLineS">
            </div>
            <sc:Placeholder ID="phSecondaryNavWidgets" Key="secNavWidgets" runat="server" />
        </div>
    </div>
    <!--end of cd_left-->
    <sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />
    <!--end cd_right-->
</div>
<asp:Literal ID="litStyle" runat="server"></asp:Literal>