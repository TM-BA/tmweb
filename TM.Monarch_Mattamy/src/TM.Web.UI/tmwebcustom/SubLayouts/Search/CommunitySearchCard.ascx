﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunitySearchCard.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Search.CommunitySearchCard" %>
<div class="search-results-wrapper">
	<div class="h-sear">
		<div class="h-sear1">
			Sort by:</div>
		<div class="h-sear2">
			<select id="searchsortby" name="sortBy" class="sb1" onchange="CommunitySort()">
				<option value="1" selected="selected">Community name</option>
				<option value="2">Price low to high</option>
				<option value="3">Price high to low</option>
				<option value="4">Location</option>
			</select></div>
		<div class="h-sear3">
			COMMUNITY</div>
		<div class="h-sear4">
			LOCATION</div>
		<div class="h-sear5">
			DETAILS</div>
		<div class="h-sear6">
			PROMOTIONS</div> 
	</div>
	<div id="CommunitySearchResult">
	</div>
	<script id="communitycard" type="text/x-jquery-tmpl">  
	{{each(companyname, communities) $data}}																					
		
		{{each communities}}				
		<div class="commCard">					
			<div class="commImg">
				<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}"><img src="${CommunityCardImage}?h=138&w=245&bc=white" alt="${CommunityName}" onError="this.onerror=null;this.src='/Images/img-not-available.jpg';"></a>
				{{if CommunityImageStatusFlag.length > 0 }}
				<div class="comstsflag"><img src="${CommunityImageStatusFlag}" alt="${CommunityStatus}"></div>
				{{/if}}
			</div>
			<div class="commName">			
				<div class="redMarker">{{html MapIcon}}</div>
					<a onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-CommunityName');" {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}" class="card_t1">${Name}</a>
			</div>
			<div class="priceFromSpace">
				<span class="priceFrom">${CommunityPrice}</span>
			</div>
			
			<div class="commLocation">
				<p><a href="https://www.google.com/maps?q=${StreetAddress1} ${StreetAddress2}, ${City}, ${StateProvinceAbbreviation} ${ZipPostalCode}">${StreetAddress1} ${StreetAddress2}<br> ${City}, ${StateProvinceAbbreviation} ${ZipPostalCode}</a><br><a href="tel:${Phone}">${Phone}</a></p>
			</div>
			<div class="commDetails">
				<p>	
					{{if SquareFootage == "" && (Bedrooms == "" || Bedrooms == "0 Bed") && (Bathrooms == "" || Bathrooms == "0 Bath") && (Stories == "" || Stories == "0 Story Home") && (Garage == "" || Garage =="0 Car Garage" )}}
						For more information <a href="${CommunityDetailsURL}/request-information/">Contact us</a>.
					{{else}}
						{{if SquareFootage != ""}}${SquareFootage}<br />{{/if}}
						{{if (Bedrooms == "" || Bedrooms == "0 Bed")}}{{else}}${Bedrooms}<br />{{/if}}
						{{if (Bathrooms == "" || Bathrooms == "0 Bath")}}{{else}}${Bathrooms}<br />{{/if}}

						{{if (Stories == "" || Stories == "0 Story Home")}}{{else}}${Stories}<br />{{/if}}
						{{if (Garage == "" || Garage =="0 Car Garage")}}{{else}}${Garage}{{/if}}
					{{/if}}
				</p>
			</div>
			<div class="commPromotions">
				<ul class="addFav-sr">
					<li id="fh_${CommunityID}">
						{{if IsFavorite == true}}							
							<a id="fav_${CommunityID}" class="selected" href="javascript:CommunityAddRemoveFavorites('<%=CurrentWebUser != null %>','${CommunityID}', 'Community', 'Remove')">Remove from Favorites</a>
						{{else}}
							<a id="fav_${CommunityID}" href="javascript:CommunityAddRemoveFavorites('<%=CurrentWebUser != null %>', '${CommunityID}', 'Community', 'Add')">Add to Favorites</a>
						{{/if}}
					</li>
				</ul>								
				<p>					
					<a onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-PromotionDetails');" href="${CommunityDetailsURL}/promotions">${PromoInfo.Title}</a>					
				</p>				
			</div>	
			<div class="clearfix">
			</div>	
            	
			<ul class="commCardGrayBar">				
				<li class="lb1">{{if HomeForSaleList.length > 0}}<a id="ico_${CommunityID}" onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-HomesForSale');" class="plusSign" href="javascript:ViewHomeForSales('${CommunityID}');">View Homes for Sale</a>{{/if}}</li>
				 <li class="lb2">{{if hasSitePlan == true }} <a {{if IsCurrentDomain == false }}target="_blank"{{/if}} onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-SitePlan');" href="${CommunityDetailsURL}/Site-Plan">Site Plan</a> {{/if}}  </li>
				<li class="lb3"><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-AvailablePlans');" href="${CommunityDetailsURL}/floor-plans">Available Plans</a></li>				
			</ul>			
			<div id="hfs_${CommunityID}" class="viewhfsbox">
				<div class="hfslist">
					{{if HomeForSaleList.length > 0}}
						<ul>
							<li>Address</li>
							<li>Plan Name</li>	
							<li>Price</li>
							<li>Bed</li>
							<li>Bath</li>								
							<li>Stories</li>
							<li>Garage</li>
							<li>Availability</li>
						</ul> 
						{{each HomeForSaleList}}								
						<ul>
							<li><a onclick="TM.Common.GAlogevent('Details', 'Click', 'HomeAddressLink');" {{if $value.IsCurrentDomain == false }}target="_blank"{{/if}} href="${$value.HomeforSaleDetailsURL}"> ${$value.HomeforSaleName.replace("Lot", "Homesite")} </a></li>
							<li>${$value.HomeforSalePlanName}</li>	
							<li>$${$value.Price.formatMoney(0, ".", ",")}</li>							
							<li>${$value.NumberofBedrooms}</li>								
							<li>${$value.NumberofBathrooms}</li>
							<li>${$value.NumberofStories}</li>
							<li>${$value.NumberofGarages}</li>
							<li>${$value.Availability}</li>
						</ul>
						{{/each}}										
					{{/if}} 								
				</div>	
				{{if HomeForSaleList.length > 0}}						
				<ul class="sched">
					<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/request-information/">Schedule an Appointment</a></li>
					<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} onclick="TM.Common.GAlogevent('Details', 'Click', 'CommunityCard-AllHomesForSale');" href="${CommunityDetailsURL}/homes-ready-now">View All Homes for Sale</a></li>
				</ul>
				{{/if}} 								
			</div>
		</div>
	   {{/each}}
	 {{/each}}
	</script>
	<script id="nocommunitycard" type="text/x-jquery-tmpl">    		
		<div class="commCard">					
			<p>No records found</p>
		</div>												  		
	</script>
</div>
