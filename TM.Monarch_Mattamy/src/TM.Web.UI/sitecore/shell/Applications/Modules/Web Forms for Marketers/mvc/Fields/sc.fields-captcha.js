﻿if (typeof ($scw) === "undefined") {
    window.$scw = jQuery.noConflict(true);
}

$scw.widget("wffm.captcha", {
    _create: function () {
        var self = this;

        $scw(this.element).find(".selector-field-captcha-play").click(function (event) {
            event.preventDefault();
            var container = $scw(self.element).find(".field-captcha-audio");
            container.empty();
            container.append('<embed id="player" src="' + self.options.handlerUrl + "&nocache=" + Date.now() + '" type="audio/x-wav" autostart="true" hidden="true"/>');
        });

        $scw(this.element).find(".selector-field-captcha-refresh").click(function (event) {
            event.preventDefault();
            window.location.reload();
        });
    },
});


