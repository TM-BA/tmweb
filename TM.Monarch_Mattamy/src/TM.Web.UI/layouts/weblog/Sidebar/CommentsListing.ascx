﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommentsListing.ascx.cs" Inherits="Sitecore.Modules.WeBlog.Layouts.CommentsListing" %>
<%@ Import Namespace="Sitecore.Modules.WeBlog.Items.WeBlog" %>
<%@ Import Namespace="Sitecore.Modules.WeBlog.Extensions" %>

<ul class="rhs">
<li><%= Sitecore.Modules.WeBlog.Globalization.Translator.Render("RECENT_COMMENTS") %></li>
<asp:Repeater ID="rptSteps" runat="server">
    <ItemTemplate>
         <li><a href='<%# GetEntryUrlForComment(Container.DataItem as CommentItem) %>'><%# (Container.DataItem as CommentItem).Comment.Text.MaxLength(50) %></a></li> 
    </ItemTemplate>
</asp:Repeater>
</ul>


 