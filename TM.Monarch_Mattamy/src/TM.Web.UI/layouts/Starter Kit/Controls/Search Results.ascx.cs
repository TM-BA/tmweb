using System;
using System.Web.UI;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Search;
using Sitecore.Web;
using Sitecore;
using Sitecore.Data;


namespace Sitecore.Starterkit
{
    public partial class LuceneSearchResults : System.Web.UI.UserControl
    {
        SearchManager searchMgr;
        private string baseURL;
        private Database database = Sitecore.Context.Item.Database;

        protected void Page_Load(object sender, EventArgs e)
        {
            lblSearchString.Enabled = true;
            string indexName = StringUtil.GetString(IndexName, SearchSettings.get("Search Index"));
            searchMgr = new SearchManager(indexName, database);
            // Decode the search string query string
            string searchStr = Server.UrlDecode(WebUtil.GetQueryString("searchStr"));

            // Category is provided if a visitor wants to see all the results for a given area of the site
            string categoryStr = Server.UrlDecode(WebUtil.GetQueryString("categoryStr"));
            baseURL = string.Format("{0}?searchStr={1}&categoryStr=", LinkManager.GetItemUrl(Sitecore.Context.Item), searchStr);

            // Be sure we have an empty search string at very least (avoid null)
            if (searchStr == null)
            {
                searchStr = string.Empty;
            }

            // If the visitor provided no criteria, don't bother searching
            if (searchStr == string.Empty)
            {
                lblSearchString.Text = SearchSettings.get("search criteria") + SearchSettings.get("search no criteria");
            }
            else
            {
                // Remind the visitor what they provided as search criteria
                lblSearchString.Text = SearchSettings.get("search criteria") + searchStr;

                // Perform the actual search
                searchMgr.Search(searchStr, database);

                // Display the search results
                DisplayResults(searchMgr.SearchResults, categoryStr);
            }

        }

        public string IndexName { get; set; }

        // Initially, this method will be called with a blank category name.
        // In that case, we display initial results for all areas of the site.
        // If the user wants more results for a given category, this method
        // will be called with the appropriate category name.
        private void DisplayResults(SearchResultCollection results, string categoryName)
        {
            pnResultsPanel.Controls.Clear();

            // Give an appropriate message if we didn't find anything
            if (results.Count == 0)
            {
                NoResults();
            }
            else
            {
                // The category name will be empty if this is an initial search.
                // In this case, we show up to "maxInitialResults" in each site section.
                if (categoryName == string.Empty)
                {
                    // Loop through all the search result categories
                    foreach (var category in results.Categories)
                    {
                        string maxResultsStr = SearchSettings.get("Search Max Initial Results");
                        int maxResults = int.Parse(maxResultsStr);

                        // See if we we have any hits in this category, don't do standard-items
                        if (category.Count > 0)
                        {
                            if ((category.Name != "Standard-Items") && (category.Name != "RSS-Feeds"))
                            {
                                // We must have results, so build the category title
                                string categoryTitleFormat = SearchSettings.get("Search Category Partial Title");
                                int resultsShown = Math.Min(category.Count, maxResults);
                                int totalResults = category.Count;
                                string categoryTitle =
                                    string.Format(categoryTitleFormat, resultsShown, totalResults, category.Name.Replace('-', ' '));
                                string titleDiv = string.Format("<div class='search-results-category'><div class='link'><a href='{1}{2}'>�</a></div><div class='title'>{0}</div></div>", categoryTitle, baseURL, category.Name);

                                LiteralControl catTitle = new LiteralControl(titleDiv);
                                pnResultsPanel.Controls.Add(catTitle);

                                // Now iterate over the number of results we will show initially
                                foreach (var result in category)
                                {
                                    Item hit = result.GetObject<Item>();
                                    if (hit != null)
                                    {
                                        string hitText = GenerateHitText(hit);
                                        LiteralControl hitControl = new LiteralControl(hitText);
                                        pnResultsPanel.Controls.Add(hitControl);
                                    }
                                }
                            }
                            else // This occurs when result is in Standard-items and we don't want to show...
                            {
                                NoResults();
                            }
                        }
                        else // Category count is zero
                        {
                            NoResults();
                        }
                    }
                }
                // enter here if category name is not Empty 
                // that is, a visitor wants to see all results for 1 area of the site
                else
                {

                    SearchResultCategoryCollection category = results.GetCategory(categoryName);

                    // We must have results, so build the category title, but not standard-items...
                    string categoryTitleFormat = SearchSettings.get("Search Category Full Title");
                    string categoryTitle =
                        string.Format(categoryTitleFormat, category.Count, category.Name.Replace('-', ' '));
                    string titleDiv =
                        string.Format("<div class='search-results-category'><div class='title'>{0}</div></div>", categoryTitle);

                    LiteralControl catTitle = new LiteralControl(titleDiv);
                    pnResultsPanel.Controls.Add(catTitle);

                    // Now iterate over all the results for this category
                    foreach (var result in category)
                    {
                        var hit = result.GetObject<Item>();
                        if (hit != null)
                        {
                            string hitText = GenerateHitText(hit);
                            LiteralControl hitControl = new LiteralControl(hitText);
                            pnResultsPanel.Controls.Add(hitControl);
                        }
                    }

                }
            }
        }

        private void NoResults()
        {
            string noResultsMsg = SearchSettings.get("Search No Results");
            LiteralControl noResults = new LiteralControl(string.Format("<p>{0}</p>", noResultsMsg));
            pnResultsPanel.Controls.Add(noResults);
        }

        // Generate the text to show for each item that matches 
        // the search criteria
        private static string GenerateHitText(Item hit)
        {
            // Set the name of the link to a non-blank value.
            // Try using one of the title fields if possible.
            string hitName;
            if (hit["menu title"] != string.Empty)
            {
                hitName = hit["menu title"];
            }
            else if (hit["breadcrumb title"] != string.Empty)
            {
                hitName = hit["breadcrumb title"];
            }
            else if (hit["banner title"] != string.Empty)
            {
                hitName = hit["banner title"];
            }
            else if (hit["title"] != string.Empty)
            {
                hitName = hit["title"];
            }
            else
            {
                hitName = hit.Name;
            }

            // Set the description to the start of one of the description
            // fields.
            string hitDescription;
            if (hit["menu abstract"] != string.Empty)
            {
                hitDescription = hit["overview abstract"];
            }
            else if (hit["text"] != string.Empty && hit["text"].Contains("<") == false)
            {
                hitDescription = hit["text"];
            }
            else
            {
                hitDescription = string.Empty;
            }

            // If the description is too long, shorten it.  If the description is
            // not blank, add a link break after it.
            if (hitDescription.Length > 150)
            {
                hitDescription = string.Format("{0}...<br/>", hitDescription.Substring(0, 150));
            }
            else if (hitDescription.Length > 0)
            {
                hitDescription += "<br/>";
            }

            DateField lastUpdatedField = hit.Fields["__updated"];
            string lblLastUpdated = SearchSettings.get("Search Last Updated");
            string lastUpdated = (lastUpdatedField != null
                                      ? lastUpdatedField.ToString()
                                      : "unknown");

            string hitText = string.Format("<div class='search-results-hit'><p><a href='{0}'>{1}</a><br/>{2}<strong>{3}</strong>{4}</p></div>", LinkManager.GetItemUrl(hit), hitName, hitDescription, lblLastUpdated, lastUpdated);
            return hitText;
        }
    }//class
}//namespace

/*
Sitecore Shared Source License

This License governs use of the accompanying Software, and your use of the Software 
constitutes acceptance of this license. 

Subject to the restrictions in this license, you may use this Software for any commercial or 
non-commercial purpose in Sitecore solutions only. You may also distribute this Software with 
books or other teaching materials, or publish the Software on websites, that are intended to 
teach the use of the Software in relation to Sitecore. 

You may not use or distribute this Software or any derivative works in any form for uses other 
than with Sitecore. 

You may modify this Software and distribute the modified Software as long as it relates to 
Sitecore, however, you may not grant rights to the Software or derivative works that are 
broader than those provided by this License. For example, you may not distribute modifications 
of the Software under terms that would permit uses not relating to Sitecore, or under terms 
that purport to require the Software or derivative works to be sublicensed to others. 

You may use any information in intangible form that you remember after accessing the Software. 
However, this right does not grant you a license to any of Sitecore's copyrights or patents 
for anything you might create using such information. 

In return, we simply require that you agree: 

1.	Not to remove any copyright or other notices from the Software.

2.	That if you distribute the Software in source or object form, you will include a verbatim 
    copy of this license. 
    
3.	That if you distribute derivative works of the Software in source code form you do so only 
    under a license that includes all of the provisions of this License, and if you distribute 
    derivative works of the Software solely in object form you do so only under a license that 
    complies with this License. 
    
4.	That if you have modified the Software or created derivative works, and distribute such 
    modifications or derivative works, you will cause the modified files to carry prominent 
    notices so that recipients know that they are not receiving the original Software. Such 
    notices must state: (i) that you have changed the Software; and (ii) the date of any changes. 
    
5.	THAT THE SOFTWARE COMES "AS IS", WITH NO WARRANTIES. THIS MEANS NO EXPRESS, IMPLIED OR 
    STATUTORY WARRANTY, INCLUDING WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY OR FITNESS 
    FOR A PARTICULAR PURPOSE OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT. ALSO, YOU MUST PASS 
    THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE SOFTWARE OR DERIVATIVE WORKS. 
    
6.	THAT SITECORE WILL NOT BE LIABLE FOR ANY DAMAGES RELATED TO THE SOFTWARE OR THIS LICENSE, 
    INCLUDING DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL OR INCIDENTAL DAMAGES, TO THE MAXIMUM 
    EXTENT THE LAW PERMITS, NO MATTER WHAT LEGAL THEORY IT IS BASED ON. ALSO, YOU MUST PASS 
    THIS LIMITATION OF LIABILITY ON WHENEVER YOU DISTRIBUTE THE SOFTWARE OR DERIVATIVE WORKS. 
    
7.	That if you sue anyone over patents that you think may apply to the Software or anyone's 
    use of the Software, your license to the Software ends automatically. 
    
8.	That your rights under the License end automatically if you breach it in any way. 

9.	Sitecore reserves all rights not expressly granted to you in this license. 

*/