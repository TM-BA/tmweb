<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search Box.ascx.cs"
    Inherits="Sitecore.Starterkit.LuceneSearchBox" %>
<%@ register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %> 

<asp:Panel ID="SearchPanel" runat="server" DefaultButton="btnSearch" class="search-panel">
<div class="search-box">
<table cellpadding="0px">
<tr>
<td valign="top"><asp:textbox onfocus="doClear(this)" runat="server" id="txtCriteria" width="130px" ontextchanged="txtCriteria_TextChanged" ForeColor="Silver"></asp:textbox></td>
<td><asp:imagebutton runat="server" id="btnSearch" imageurl="/images/search.gif" onclick="btnSearch_Click"></asp:imagebutton></td>
</tr>
</table>
</div>
</asp:Panel>