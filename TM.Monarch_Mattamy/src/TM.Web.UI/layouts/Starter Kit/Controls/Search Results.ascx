﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search Results.ascx.cs" 
Inherits="Sitecore.Starterkit.LuceneSearchResults" %>
<%@ register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %><p>
  <asp:label runat="server" id="lblSearchString" text="Label"></asp:label>
  <asp:panel runat="server" id="pnResultsPanel"></asp:panel>
</p>