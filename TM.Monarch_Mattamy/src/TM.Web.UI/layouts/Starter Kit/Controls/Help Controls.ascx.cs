using System;

namespace Sitecore.Starterkit
{
    public partial class Help_Controls : System.Web.UI.UserControl
    {
        private Data.Items.Item SiteRoot;
        private readonly string _showHelpImage = "/images/ShowHelp.gif";
        private readonly string _hideHelpImage = "/images/HideHelp.gif";
        private readonly string _showHelpLink = "Show Help";
        private readonly string _hideHelpLink = "Hide Help";
        private readonly string _showHelpButtons = "show help buttons";
        private readonly string _showHelpLinks = "show help links";

        protected void Page_Load(object sender, EventArgs e)
        {
            SiteRoot = GetSiteRoot(Sitecore.Context.Item);
            if (SiteRoot != null)
            {
                bool show_buttons = (SiteRoot[_showHelpButtons] == "1" ? true : false);

                if (Sitecore.Context.PageMode.IsPageEditorNavigating && show_buttons)
                {
                    pnButtons.Visible = true;

                    bool show_help = (SiteRoot[_showHelpLinks] == "1" ? true : false);
                    btnLeftHelpImage.ImageUrl = (show_help ? _hideHelpImage : _showHelpImage);
                    btnLeftHelpImage.AlternateText = (show_help ? _hideHelpLink : _showHelpLink);
                    btnHelpLink.Text = (show_help ? _hideHelpLink : _showHelpLink);
                }
                else
                {
                    pnButtons.Visible = false;
                }
            }
            else
            {
                pnButtons.Visible = false;
            }
        }

        private static Data.Items.Item GetSiteRoot(Data.Items.Item current)
        {
            if (current == null) return null;
            if (current.TemplateName.Equals("Site Root"))
                return current;
            else
            {
                return GetSiteRoot(current.Parent);
            }
        }
        
        private void updateHelp()
        {
            bool show_help = (SiteRoot[_showHelpLinks] == "1" ? true : false);

            using (new SecurityModel.SecurityDisabler())
            {
                SiteRoot.Editing.BeginEdit();
                SiteRoot[_showHelpLinks] = (show_help ? "" : "1");
                SiteRoot.Editing.EndEdit();
            }

            btnLeftHelpImage.ImageUrl = (show_help ? _hideHelpImage : _showHelpImage);
            btnLeftHelpImage.AlternateText = (show_help ? _hideHelpLink : _showHelpLink);
            btnHelpLink.Text = (show_help ? _hideHelpLink : _showHelpLink);
            Sitecore.Web.WebUtil.ReloadPage();
        }

        protected void btnHelpLink_Click(object sender, EventArgs e)
        {
            updateHelp();
        }

        protected void btnHelpImage_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            updateHelp();
        }
    }
}