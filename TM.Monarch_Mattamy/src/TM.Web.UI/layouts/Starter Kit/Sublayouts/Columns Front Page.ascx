﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="columns-C-and-R">
    <div id="content-col">
        <sc:Placeholder Key="column-content" ID="columncontent" runat="server"></sc:Placeholder>
    </div>
    <div id="right-col">
        <sc:Placeholder Key="column-right" ID="contentright" runat="server"></sc:Placeholder>
    </div>
</div>
