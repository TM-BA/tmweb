﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div class="content-spacing">
    <div style="float: left" class="image-left-div">
        <sc:Placeholder Key="content-image" runat="server"></sc:Placeholder>
    </div>
    <sc:Placeholder Key="content" runat="server"></sc:Placeholder>
</div>
