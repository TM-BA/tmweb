﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table width="100%" id="ctl1">
    <tbody>
        <tr>
            <td align="left" id="header-left">
                <sc:XslFile runat="server" RenderingID="B45EBAE-7B40-4CA4-B8EA-EF2BEF0708CA%7d" placeholder="content"
                    Path="/xsl/Starter Kit/Site Logo.xslt" ID="SiteLogo" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
                </sc:XslFile>
            </td>
            <td align="center" id="header-middle">
            </td>
            <td align="right" id="header-right">
                <sc:Sublayout runat="server" RenderingID="54B12B5-F043-47E9-BD20-E208F0CEBC0C%7d"
                    placeholder="content" Path="/layouts/Starter Kit/Controls/Search Box.ascx" ID="SearchBox">
                </sc:Sublayout>
                <sc:XslFile runat="server" RenderingID="B556B84-364A-4AB3-A64C-933787D19619%7d" placeholder="content"
                    Path="/xsl/Starter Kit/Toolbox.xslt" ID="Toolbox" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
                </sc:XslFile>
            </td>
        </tr>
    </tbody>
</table>
