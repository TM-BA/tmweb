﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<sc:Sublayout runat="server" RenderingID="{2BF352A5-6413-45DD-ABD3-2787356F555C}"
    Path="/layouts/Starter Kit/Sublayouts/Breadcrumb.ascx" ID="uxBreadcrumb" placeholder="content"
    Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem"></sc:Sublayout>
<div id="columns-C-and-R">
    <div id="content-col">
        <sc:Placeholder runat="server" Key="column-content" ID="columncontent" />
    </div>
    <div id="right-col">
        <sc:Placeholder runat="server" Key="column-right" ID="contentright" />
    </div>
</div>
