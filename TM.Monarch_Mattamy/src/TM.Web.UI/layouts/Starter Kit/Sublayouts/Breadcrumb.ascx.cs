﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Data.Items;
using Sitecore.Collections;

namespace Sitecore.Starterkit.layouts.Starter_Kit.Sublayouts
{
    public partial class Breadcrumb : System.Web.UI.UserControl
    {
        private static readonly string _siteRootTemplateName = "Site Root";
        private static readonly string _separatorPath = "/sitecore/content/Settings/Common Text/Breadcrumb Separator";
        private static readonly string _separatorField = "Text";
        private static readonly string _header = "<div id='breadcrumb'>";
        private static readonly string _footer = "</div>";
     
        private ItemList _breadcrumbItemList = new ItemList();
        private Item _currentItem = Sitecore.Context.Item;
    
        protected void Page_Load(object sender, EventArgs e) 
        {

            this.CreateCrumbs(_currentItem.Parent);
            _breadcrumbItemList.Add(_currentItem);


            Repeater repeater = new Repeater();
            repeater.ID = "Repeater1";
            this.Controls.Add(repeater);
            
            repeater.ItemTemplate = new CompiledTemplateBuilder(new BuildTemplateMethod(BuildItemTemplate));
            repeater.SeparatorTemplate = new CompiledTemplateBuilder(new BuildTemplateMethod(BuildSeparatorTemplate));
            repeater.HeaderTemplate = new CompiledTemplateBuilder(new BuildTemplateMethod(BuildHeaderTemplate));
            repeater.FooterTemplate = new CompiledTemplateBuilder(new BuildTemplateMethod(BuildFooterTemplate));

            repeater.DataSource = _breadcrumbItemList;
            repeater.DataBind();
 
        }


       
        // Recursive method that walks up the parent tree to find the
        // site root of the current item.
        private string GetSeperatorText()
        {
           if( !string.IsNullOrEmpty(Sitecore.Context.Database.GetItem(_separatorPath)[_separatorField]))
           {
               return string.Format("&#160;&#160;{0}&#160;&#160;", Sitecore.Context.Database.GetItem(_separatorPath)[_separatorField]);
           }
           else
           {
               return string.Empty;
           }
        }

        private string GetHeader()
        {
            return _header;
        }

        private string GetFooter()
        {
            return _footer;
        }
        

        // Builds Separator emplate
        protected void BuildSeparatorTemplate(Control container)
        {
            container.Controls.Add(new LiteralControl(GetSeperatorText()));
        }


        // Builds Header template
        protected void BuildHeaderTemplate(Control container)
        {
            container.Controls.Add(new LiteralControl(GetHeader()));

        }


        // Builds Footer template
        protected void BuildFooterTemplate(Control container)
        {
            container.Controls.Add(new LiteralControl(GetFooter()));
        }

        
        // Builds Item template with link
        protected void BuildItemTemplate(Control container)
        {
            HyperLink h = new HyperLink();
            Literal l = new Literal();
            container.Controls.Add(h);
            container.Controls.Add(l);
            h.ID = "HyperLink1";
            l.ID = "Literal1";
            h.DataBinding += new EventHandler(
                    delegate
                    {
                        RepeaterItem item = container as RepeaterItem;
                        Item crumb = item.DataItem as Item;
                        if (crumb.ID != _currentItem.ID)
                        {
                            h.Text = ShowTitle(crumb);
                            h.NavigateUrl = Sitecore.Links.LinkManager.GetItemUrl(crumb);
                        }
                        else
                        {
                            l.Text = ShowTitle(crumb);
                        }
                    }
                    );

        }
        // Returns Title or Item name
        // site root of the current item.
        public string ShowTitle(Item item)
        {
            if (item != null)
            {
				return string.IsNullOrEmpty(item["Breadcrumb Title"]) ? 
                        string.IsNullOrEmpty(item["Menu Title"]) ? 
                        string.IsNullOrEmpty(item["Title"]) ? 
                        item.Name : item["Title"] : item["Menu Title"] : item["Breadcrumb Title"];
            }
            else
            {
                return string.Empty;
            }
        }

        // Recursive method that walks up the parent tree to find the
        // site root of the current item.
        private void CreateCrumbs(Item current_item) {

            if ( current_item != null && (!current_item.TemplateName.ToLower().Equals(_siteRootTemplateName.ToLower())) ) {
                CreateCrumbs(current_item.Parent);
            }
            this._breadcrumbItemList.Add(current_item);

        }


   }
}
