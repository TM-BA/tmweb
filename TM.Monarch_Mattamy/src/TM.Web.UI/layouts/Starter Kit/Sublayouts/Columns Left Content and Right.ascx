﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %><sc:sublayout runat="server" renderingid="{2BF352A5-6413-45DD-ABD3-2787356F555C}" path="/layouts/Starter Kit/Sublayouts/Breadcrumb.ascx" id="uxBreadcrumb" placeholder="content" parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
    </sc:sublayout>
<!--

COLUMNS LEFT CONTENT AND RIGHT

  -->
<div id="columns-L-C-and-R">
    <!--

  LEFT COLUMN

    -->
    <div id="left-col">
        <sc:placeholder runat="server" key="column-left" id="columnleft"></sc:placeholder>
    </div>
    <!--

  CONTENT COLUMN

    -->
    <div id="content-col">
        <sc:placeholder runat="server" key="column-content" id="columncontent"></sc:placeholder>
    </div>
    <!--

  RIGHT COLUMN

    -->
    <div id="right-col">
        <sc:placeholder runat="server" key="column-right" id="columnright"></sc:placeholder>
    </div>
</div>