﻿<%@ Import Namespace="Sitecore.Analytics"%>
<%@ Page Language="c#" Inherits="System.Web.UI.Page" CodePage="65001"  %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<%@ Import Namespace="Sitecore.Configuration" %>
<%@ Import Namespace="Sitecore.Data" %>
<%@ Import Namespace="Sitecore.Modules.EmailCampaign" %>
<%@ Import Namespace="Sitecore.Modules.EmailCampaign.Core.Analytics" %>
<%@ Import Namespace="Sitecore.Modules.EmailCampaign.Messages" %>
<%@ Import Namespace="Sitecore.Web" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Web.UI" %>

<head runat="server">
  <title>Welcome to Sitecore</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="CODE_LANGUAGE" content="C#" />
  <meta name="vs_defaultClientScript" content="JavaScript" />
  <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
  <link href="/default.css" rel="stylesheet" />
  <sc:VisitorIdentification runat="server" />
</head>
<body> 
  <form id="mainform" method="post" runat="server">
    <div id="MainPanel">
      Unsubscribe from all
    </div>
    
  </form>
 </body>
</html>
<script language="c#" runat="server">
 protected void Page_Load(object sender, EventArgs e)
    {
	  this.Context.Response.Write("Hello");
      string str = this.Context.Request.QueryString[GlobalSettings.AutomationStateQueryKey];
	  
	  
      if (string.IsNullOrEmpty(str) || !ShortID.IsShortID(str))
        return;
      
	    Guid recipientId = new Guid(str);
        string messageId;
        Guid stateId;
        string recipient = AnalyticsHelper.GetRecipient(recipientId, out messageId, out stateId);
        
		if (string.IsNullOrEmpty(recipient) || !Sitecore.Data.ID.IsID(messageId))
          return;
		  
        Contact contactFromName = Sitecore.Modules.EmailCampaign.Factory.GetContactFromName(recipient);
        
		MessageItem message = Sitecore.Modules.EmailCampaign.Factory.GetMessage(messageId);

       
		
		if (contactFromName == null || message == null)
          return;
	  
	  this.Context.Response.Write(contactFromName.Name);
	  
	//  this.Context.Response.Write(message.TargetAudience==null);
	 // var url = ClientApi.UnsubscribeFromAll(str);
	 //  this.Context.Response.Write(url);
    }
</script>