<%@ Page language="c#" Codebehind="error.aspx.cs" AutoEventWireup="false" Inherits="Sitecore.Shell.ErrorPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>Whoops!</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body>
      <style>
          body{ font: normal 18px Arial, Helvetica, sans-serif, serif;
color: #666666;}
div.content
{
  position:relative;
  top:50px;
  width:650px;
  border:1px solid gray;
    line-height: 2em;
  -moz-border-radius: 5px;
  border-radius: 5px;
  padding: 50px;
  margin: 50px auto;
  -o-box-shadow: 10px 10px 5px #888;
  -icab-box-shadow: 10px 10px 5px #888;
  -khtml-box-shadow: 10px 10px 5px #888;
  -moz-box-shadow: 10px 10px 5px #888;
  -webkit-box-shadow: 10px 10px 5px #888;
  box-shadow: 10px 10px 5px #888;
}
      </style>
      <div class="content">
      <h1 style="color:#d31245">OH NO! So sorry..but there was a problem serving the requested page.  </h1> <br/>
      
      <strong>Now you're wondering, "what do I do now??". Well.. </strong>
       <ul>
           <li>
               You can go back and try refreshing (Ctrl+F5 or Command+R) the problem may be temporary
           </li>
	   <li>
	       You can go the home page. Click <a href="/">here</a>
	   </li>
	   <li>
	       <a href="mailto:webmaster@taylormorrison.com">Email us</a> for assistance.
	   </li>
       </ul>
      
       Rest assured our technical team has been notified of this error.
       </div>
    <form id="error" method="post" runat="server"><asp:Label id="Label1" runat="server" visible="false">Error:</asp:Label>&nbsp;<asp:Label id="errorLabel" runat="server">[msg]</asp:Label>
    </form>
  </body>
</html>
