﻿function PreventPeUnloadMessage(iframeId) {
    $('#' + iframeId).on('load', function () {
        (function DetachScBeforeUnloadRecursive(element) {
            $('iframe', element).each(function (index, iframe) {
                var contentWindow = iframe.contentWindow
                , scForm = contentWindow.scForm
                , beforeUnloadHandler = contentWindow.scBeforeUnload;
                
                if (scForm && beforeUnloadHandler) {
                    if (scForm.browser.isWebkit) {
                        scForm.browser.detachEvent(window.top, "onbeforeunload", beforeUnloadHandler);                       
                    }
                    else {
                        scForm.browser.detachEvent(contentWindow, "onbeforeunload", beforeUnloadHandler); 
                    }
                }
                DetachScBeforeUnloadRecursive($(iframe).contents());
            });
        })($(this).contents());
    });
}

function messageBodyPopupEditModeBtnClick() {
    $('a#CloseButton').bind('click', function() {
        $('#main_0_content').contents().find('#scWebEditRibbon').contents().find('#RibbonPanel #RibbonPane #Ribbon #Ribbon_Toolbar').find('div.chunk:first').find('.panel').children('a').click();
    });   
}