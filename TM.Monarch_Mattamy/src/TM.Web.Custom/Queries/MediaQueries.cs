﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Forms.Core.Data;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Queries
{
    public class ImageItem
    {
        public string[] Categories { get; set; }
        public string URL { get; set; }
        public string Caption { get; set; }

    }

    public class MediaQueries
    {
       
        private Database _DB = Sitecore.Context.Database;

      
        public string GetInspirationGalleryImages(string currentDivisionName)
        {
            var query =
            "fast:/sitecore/Media Library//*[@#Show In Gallery#='1']";
            var images = _DB.SelectItems(query);

            var company = Sitecore.Context.GetSiteName().ToLower();
            
            string gallery;

            var imageList = new List<ImageItem>();

            switch (company)
            {
                case CommonValues.DarlingHomesDomain:
                    gallery = "DH ";
                    break;
                case CommonValues.MonarchGroupDomain:
                    gallery = "MG ";
                    break;
                default:
                    gallery = "TM ";
                    break;

            }

            gallery = currentDivisionName.IsEmpty() || currentDivisionName == "InspirationGallery" ? gallery : currentDivisionName + " ";

            foreach (var image in images)
            {
                if (image.Fields[gallery + "Categories"].Value != string.Empty)
                {
                    var categoryList = image.Fields[gallery + "Categories"].Value.Split('|');
                    if (categoryList.Length > 0)
                    {
                        var categoryNames = new List<string>();
                        foreach (var category in categoryList)
                        {
                            var fieldItem = new FieldItem(_DB.GetItem(category));
                            categoryNames.Add(fieldItem.FieldDisplayName);
                        }
                        var imageObject = new ImageItem()
                                              {
                                                  Caption = image.Fields["Caption"].Value,
                                                  Categories = categoryNames.ToArray(),
                                                  URL = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image)
                                              };
                        imageList.Add(imageObject);
                    }
                }

            }
            return JsonConvert.SerializeObject(imageList);

        }

    }
}