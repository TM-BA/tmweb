﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Express;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using TM.Domain.Entities;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.WebControls;
using TM.Utils.Extensions;


namespace TM.Web.Custom.SCHelpers
{

	public class FavoritesHelper : BaseHelper
	{
		private static readonly SearchHelper _searchHelper = new SearchHelper();
		private static readonly Database _scContextDB = Context.Database;
		private readonly Cache _webCache = new Cache();
	    private List<IFPInfo> _userIFPs;

	    private Cache WebCache
		{
			get { return _webCache; }
		}

		private Item[] HytrateFavoritesCommunities(string path, Database db = null)
		{
			var qry =
				string.Format("{0}/*[{1}]", SCFastQueries.CrossCompanyProductTypeStateDiviCityCondition, path);
			return (db ?? _scContextDB).SelectItems(qry);
		}

		private Item[] HytrateFavoritesCommunitiesfromPlan(string path, Database db = null)
		{
			var qry =
				string.Format("{0}/*[@@templateid = '{1}' and {2}]/ancestor::*[@@templateid='{3}']",
				SCFastQueries.CrossCompanyProductTypeStateDiviCityCommunityCondition, SCIDs.TemplateIds.PlanPage, path, SCIDs.TemplateIds.CommunityPage);
			return (db ?? _scContextDB).SelectItems(qry);
		}

		private Item[] HytrateFavoritesCommunitiesfromHfs(string path, Database db = null)
		{
			var qry =
				string.Format("{0}/*[@@templateid = '{1}']/*[@@templateid = '{2}' and {3}]/ancestor::*[@@templateid='{4}']",
				SCFastQueries.CrossCompanyProductTypeStateDiviCityCommunityCondition, SCIDs.TemplateIds.PlanPage, SCIDs.TemplateIds.HomeForSalePage, path, SCIDs.TemplateIds.CommunityPage);
			return (db ?? _scContextDB).SelectItems(qry);
		}

		private Item[] HytrateMasterPlanIdforFavoritesPlans(ID dId, string mtrpId, Database db = null)
		{
			var qry =
				string.Format(
					"fast:/sitecore/content/*{0}/Home/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}' and @@id='{4}']/*[@@templateid='{5}']/*[@@templateid='{6}']/*[@@templateid = '{7}' and @Master Plan ID = '{8}']/ancestor::*[@@templateid='{6}']",
					SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, dId, SCIDs.TemplateIds.CityPage, SCIDs.TemplateIds.CommunityPage, SCIDs.TemplateIds.PlanPage, mtrpId);
			return (db ?? _scContextDB).SelectItems(qry);
		}
		

		public SaveItemResults SortFavoriteItems(string comPath, string planPath, string hfsPath, string sortbydomain, string type)
		{
			var saveItemResults = HydrateAllFavoriteItems(comPath, planPath, hfsPath);

			var sortby = sortbydomain.Split('_')[0];
			var domain = sortbydomain.Split('_')[1];

			if (type == "Community")
				saveItemResults.CommunityInfo = _searchHelper.CommunitySort(saveItemResults.CommunityInfo.Where(c => c.Domain == domain).ToList(), sortby).Distinct(new CommunityInfoComparer()).ToList() ;
			if (type == "Plan")
				saveItemResults.PlanInfo = _searchHelper.SortbyPlanInfo(saveItemResults.PlanInfo.Where(c => c.Domain == domain).ToList(), sortby).Distinct(new PlanInfoComparer()).ToList();
			if (type == "HomeForSale")
				saveItemResults.HomeForSaleInfo = _searchHelper.SortbyHomeForSaleInfo(saveItemResults.HomeForSaleInfo.Where(c => c.Domain == domain).ToList(), sortby).Distinct(new HomeForSaleInfoComparer()).ToList();

			return saveItemResults;
		}

		public SaveItemResults GetAllFavoriteItems(string comPath, string planPath, string hfsPath)
		{
			return HydrateAllFavoriteItems(comPath, planPath, hfsPath);
		}

		public SaveItemResults HydrateAllFavoriteItems(string comPath, string planPath, string hfsPath)
		{
		    var allCommunityItems = new List<Item>();
		    var comIds = GetIds(comPath);
		    var planIds = GetIds(planPath);
		    var hfsIds = GetIds(hfsPath);

		    var comfavPath = BoundFavIds(comIds);
		    if (!string.IsNullOrWhiteSpace(comfavPath))
		        allCommunityItems.AddRange(HytrateFavoritesCommunities(comfavPath));

		    var planfavPath = BoundFavIds(planIds);
		    if (!string.IsNullOrWhiteSpace(planfavPath))
		        allCommunityItems.AddRange(HytrateFavoritesCommunitiesfromPlan(planfavPath));

		    var hfsfavPath = BoundFavIds(hfsIds);
		    if (!string.IsNullOrWhiteSpace(hfsfavPath))
		        allCommunityItems.AddRange(HytrateFavoritesCommunitiesfromHfs(hfsfavPath));

		    var cInfos = _searchHelper.HydrateFavoritesCommunitiesByItems(allCommunityItems);

		    var mapcInfos = _searchHelper.FetchMapPins(cInfos, false);

		    var domain = Context.GetDeviceName().GetDomainFromDeviceName();

		    var compName = mapcInfos.GroupBy(n => new {n.CompanyName, n.Domain})
		        .Select(g => new CrossCompanyHeader
		                         {
		                             Company = g.Key.CompanyName,
		                             Domain = g.Key.Domain
		                         }).ToList();

		    var currentCompany = compName.Where(c => c.Domain == domain);
		    var otherCompany = compName.Where(c => c.Domain != domain).ToList();


		    var allCompanies = new List<CrossCompanyHeader>();
		    allCompanies.AddRange(currentCompany);
		    allCompanies.AddRange(otherCompany);

		    var allfavComInfo = comIds != null
		                            ? (from info in mapcInfos
		                               from id in comIds
		                               where info.CommunityID.ToString().ToLower() == id.ToLower().TrimStart('{').TrimEnd("}")
		                               select info)
		                            : new List<CommunityInfo>();
		    var newallfavComInfo = new List<CommunityInfo>();
		    foreach (var cinfo in allfavComInfo)
		    {
		        if (cinfo.CommunityStatus == GlobalEnums.CommunityStatus.Closed ||
		            cinfo.StatusforSearch == GlobalEnums.SearchStatus.InActive)
		        {
		            var dItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, new ID(cinfo.CommunityID));
		            if (dItem != null) // active divi
		            {
		                cinfo.NoLongerAvailableText = NoComCommunityMessageWithDivision(cinfo, dItem);
		                if (SCUtils.SearchStatus(dItem["Status for Search"]) == GlobalEnums.SearchStatus.InActive)
		                {
		                    cinfo.NoLongerAvailableText = NoComDivisionMessageWithState(cinfo);
		                    var sItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage,
		                                                                     new ID(cinfo.CommunityID));
		                    if (sItem != null)
		                    {
		                        cinfo.NoLongerAvailableText = SCUtils.SearchStatus(sItem["Status for Search"]) ==
		                                                      GlobalEnums.SearchStatus.InActive
		                                                          ? NoComStateMessage()
		                                                          : cinfo.NoLongerAvailableText;
		                    }
		                }
		            }
		        }
		        else
		        {
		            cinfo.NoLongerAvailableText = string.Format("<span class=\"priceFrom-c\">{0}</span>", cinfo.CommunityPrice);
		        }
		        cinfo.LiveChatButton = GetFavoriteLiveChat(cinfo.LiveChatSkill, cinfo.CommunityID.ToString(),
		                                                   cinfo.CommunityDetailsURL, "Request Information");
		        newallfavComInfo.Add(cinfo);
		    }

		    var allplans = mapcInfos.SelectMany(p => p.PlanList).Distinct(new PlanInfoComparer()).ToList();

		    var allfavPlanInfo = (planIds != null && allplans.Count > 0)
		                             ? (from info in allplans
		                                from id in planIds
		                                where info.PlanID.ToString().ToLower() == id.ToLower().TrimStart('{').TrimEnd("}")
		                                select info).ToList()
		                             : new List<PlanInfo>();

		    var allhfss = mapcInfos.SelectMany(p => p.HomeForSaleList).Distinct(new HomeForSaleInfoComparer()).ToList();
		    var newallfavPlanInfo = new List<PlanInfo>();
		    foreach (var pinfo in allfavPlanInfo)
		    {
		        var com = mapcInfos.FirstOrDefault(p => p.CommunityID == pinfo.PlanCommunityID);
		        var dItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, new ID(pinfo.PlanCommunityID));
		        pinfo.IFPs = GetIFPs(pinfo.PlanID);
		        pinfo.MapIcon = com != null ? com.MapIcon : string.Empty;
		        if (pinfo.PlanStatus == GlobalEnums.SearchStatus.InActive ||
		            pinfo.StatusforSearch == GlobalEnums.SearchStatus.InActive)
		        {
		            pinfo.NoLongerAvailableText = NoPlanMessageWithCommunity(pinfo, dItem, allhfss);
		            var cItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage,
		                                                             new ID(pinfo.PlanCommunityID));
		            if (cItem != null) // active community
		            {
		                if (SCUtils.CommunityCurrentStatus(cItem["Community Status"]) == GlobalEnums.CommunityStatus.Closed)
		                {
		                    if (dItem != null) // active divi
		                    {
		                        pinfo.NoLongerAvailableText = NoPlanCommunityMessageWithDivision(pinfo, dItem, cItem);
		                        if (SCUtils.SearchStatus(dItem["Status for Search"]) == GlobalEnums.SearchStatus.InActive)
		                        {
		                            pinfo.NoLongerAvailableText = NoPlanDivisionMessageWithState(pinfo);
		                            var sItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage,
		                                                                             new ID(pinfo.PlanCommunityID));
		                            if (sItem != null)
		                            {
		                                pinfo.NoLongerAvailableText = SCUtils.SearchStatus(sItem["Status for Search"]) ==
		                                                              GlobalEnums.SearchStatus.InActive
		                                                                  ? NoPlanStateMessage()
		                                                                  : pinfo.NoLongerAvailableText;
		                            }
		                        }
		                    }
		                }
		            }
		        }
		        else
		        {
		            pinfo.NoLongerAvailableText =
		                string.Format(
		                    "<div class=\"si-price\"><span>Priced from</span> <span>{0:c0}</span></div><div class=\"msi-call\"><span>Call:{1}</span></div>",
		                    pinfo.PricedfromValue, pinfo.Phone);
		        }
		        if (com != null && dItem != null)
		        {
		            pinfo.FinanceYourDream = GetFinanceCallAction(com.CommunityLegacyID, com.City, dItem["Legacy Division ID"]);
		        }
		        pinfo.LiveChatButton = GetFavoriteLiveChat(pinfo.LiveChatSkill, pinfo.PlanID.ToString(), pinfo.PlanPageUrl,
		                                                   "Request Information");
		        newallfavPlanInfo.Add(pinfo);
		    }


		    var newallfavHfsInfo = new List<HomeForSaleInfo>();
		    var allfavHfsInfo = hfsIds != null && allhfss.Count > 0
		                            ? (from info in allhfss
		                               from id in hfsIds
		                               where
		                                   info.HomeforSaleID.ToString().ToLower() == id.ToLower().TrimStart('{').TrimEnd("}")
		                               select info).ToList()
		                            : new List<HomeForSaleInfo>();
		    foreach (var hinfo in allfavHfsInfo)
		    {
		        var com = mapcInfos.FirstOrDefault(p => p.CommunityID == hinfo.HomeforSaleCommunityID);
		        var dItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage,
		                                                         new ID(hinfo.HomeforSaleCommunityID));
			hinfo.IFPs = GetIFPs(hinfo.HomeforSaleID);

		        hinfo.MapIcon = com != null ? com.MapIcon : string.Empty;
		        if (hinfo.HomeforSaleStatus == GlobalEnums.SearchStatus.InActive ||
		            hinfo.StatusforSearch == GlobalEnums.SearchStatus.InActive)
		        {
		            hinfo.NoLongerAvailableText = NoHfSMessageWithCommunity(hinfo);
		            var cItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage,
		                                                             new ID(hinfo.HomeforSaleCommunityID));
		            if (cItem != null) // active community
		            {
		                if (SCUtils.CommunityCurrentStatus(cItem["Community Status"]) == GlobalEnums.CommunityStatus.Closed)
		                {
		                    if (dItem != null) // active divi
		                    {
		                        hinfo.NoLongerAvailableText = NoHfSCommunityMessageWithDivision(hinfo, dItem, cItem);
		                        if (SCUtils.SearchStatus(dItem["Status for Search"]) == GlobalEnums.SearchStatus.InActive)
		                        {
		                            hinfo.NoLongerAvailableText = NoHfSDivisionMessageWithState(hinfo);
		                            var sItem = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage,
		                                                                             new ID(hinfo.HomeforSaleCommunityID));
		                            if (sItem != null)
		                            {
		                                hinfo.NoLongerAvailableText = SCUtils.SearchStatus(sItem["Status for Search"]) ==
		                                                              GlobalEnums.SearchStatus.InActive
		                                                                  ? NoHfSStateMessage()
		                                                                  : hinfo.NoLongerAvailableText;
		                            }
		                        }
		                    }
		                }
		            }
		        }
		        else
		        {
		            hinfo.NoLongerAvailableText =
		                string.Format(
		                    "<div class=\"si-price\"><span>Priced from</span> <span>{0:c0}</span></div><div class=\"msi-call\"><span>Call:{1}</span></div>",
		                    hinfo.Price, hinfo.Phone);
		        }
		        hinfo.FinanceYourDream = GetFinanceCallAction(com.CommunityLegacyID, com.City, dItem["Legacy Division ID"]);
		        hinfo.LiveChatButton = GetFavoriteLiveChat(hinfo.LiveChatSkill, hinfo.HomeforSaleID.ToString(),
		                                                   hinfo.HomeforSaleDetailsURL, "Find Out More");
		        newallfavHfsInfo.Add(hinfo);
		    }

		    var saveItemResults = new SaveItemResults
		                              {
		                                  Companies = allCompanies,
		                                  MapCommunityInfo = mapcInfos.Distinct(new CommunityInfoComparer()).ToList(),
		                                  CommunityInfo = newallfavComInfo.Distinct(new CommunityInfoComparer()).ToList(),
		                                  PlanInfo = newallfavPlanInfo.ToList(),
		                                  HomeForSaleInfo = newallfavHfsInfo.ToList()
		                              };

		    return _searchHelper.GetMapBounds(saveItemResults);
		}

		private string GetFinanceCallAction(string communityLegacyID,string cityName,string legacyDivisionID)
		{
			if (!string.IsNullOrWhiteSpace(CurrentHomeItem["Financing Call to Action"]))
			{
				return CurrentHomeItem["Financing Call to Action"] + "comid={0}&cityname={1}&divid={2}".FormatWith(communityLegacyID, cityName, legacyDivisionID);
			}
			return string.Empty; 
		}

		#region Community Messages
		private string NoComCommunityMessageWithDivision(CommunityInfo cinfo, Item dItem)
		{
			DateTime lastChanged = SCUtils.SCDateTimeToMsDateTime(cinfo.CommunityStatusChangeDate);
			return string.Format("This community closed {0}.  View other communities in the <a href=\"{1}\" {2}>{3}</a> area.",
			                     lastChanged.ToString("MMMM yyyy"), cinfo.CommunityDetailsURL.TrimUrlLastTwoText(),
			                     cinfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"", dItem.DisplayName);
		}

		private string NoComDivisionMessageWithState(CommunityInfo cinfo)
		{
			DateTime lastChanged = SCUtils.SCDateTimeToMsDateTime(cinfo.CommunityStatusChangeDate);
			return string.Format("This community closed {0}.  View other areas in <a href=\"{1}\" {2}>{3}</a>.",
								 lastChanged.ToString("MMMM yyyy"), cinfo.CommunityDetailsURL.TrimUrlLastThreeText(),
								 cinfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"", cinfo.StateProvince);
		}

		private string NoComStateMessage()
		{
			return "This community is no longer available.  <a href=\"/\">View other areas where we have homes for sale.</a>";
		}

		#endregion

		#region plan Messages
		private string NoPlanStateMessage()
		{
			return "<div class=\"sav-nolng\">This plan is no longer available.<br /><a href=\"/\">View other areas where we have homes for sale</a>.</div>";
		}

		private string NoPlanDivisionMessageWithState(PlanInfo pInfo)
		{
			return string.Format("<div class=\"sav-nolng\">This plan is no longer available.<br />View other plans in <a href=\"{0}\" {1}>{2}</a>.</div>",
											pInfo.CommunityDetailsURL.TrimUrlLastThreeText(), pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
											pInfo.State);
		}

		private string NoPlanCommunityMessageWithDivision(PlanInfo pInfo, Item dItem, Item cItem)
		{
			DateTime lastChanged = SCUtils.SCDateTimeToMsDateTime(cItem["Community Status Change Date"]);
			return string.Format("<div class=\"sav-nolng\">This plan is no longer available and the community closed {3}.<br />View other communities in the <a href=\"{0}\" {1}>{2} area</a>.</div>",
									   pInfo.CommunityDetailsURL.TrimUrlLastTwoText(),
									   pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
									   dItem.DisplayName, lastChanged.ToString("MMMM yyyy"));
		}


		private string NoPlanMessageWithCommunity(PlanInfo pInfo,Item dItem, List<HomeForSaleInfo> allHfs)
		{
			var comforMstPlns = string.Empty; 
			if (dItem != null)
			{
				var dId = dItem.ID;
				var mstpId = pInfo.MasterPlanID;
				if (!string.IsNullOrWhiteSpace(dId.ToString()) && !string.IsNullOrWhiteSpace(mstpId))
				{
					var cItems = HytrateMasterPlanIdforFavoritesPlans(dId, mstpId);
					
					Uri uri = HttpContext.Current.Request.Url;
					var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

					foreach (var cItem in cItems)
					{
						var crossSiteSpecification = cItem.GetCrossSiteSpecification(sitecoreUrlOptions, uri);
						var communityUrl = crossSiteSpecification.SiteSpecificUrl;
						var isCurrentDomain = communityUrl.Contains(uri.Authority);

						comforMstPlns += string.Format("<a href=\"{0}\" {1}>{2}</a>, ", communityUrl, isCurrentDomain ? string.Empty : "target=\"_blank\"", cItem.DisplayName);
					}
					if (!string.IsNullOrWhiteSpace(comforMstPlns))
					{
						return
							string.Format(
								"<div class=\"sav-nolng\">This home is no longer available.<br />View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.<br />This plan is also available in {3}</div>",
								pInfo.CommunityDetailsURL, pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"", pInfo.CommunityName,
								comforMstPlns.TrimEnd(", "));
					}

					if (allHfs.Any(hinfo => hinfo.HomeforSaleParentID == pInfo.PlanID))
					{
						return
							string.Format(
								"<div class=\"sav-nolng\">This home is no longer available.<br />View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.</div>",
								pInfo.CommunityDetailsURL, pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
								pInfo.CommunityName);
					}

					//foreach (var hinfo in allHfs)
					//{
					//    if (hinfo.HomeforSaleParentID == pInfo.PlanID)
					//    {
					//        return
					//            string.Format(
					//                "<div class=\"sav-nolng\">This home is no longer available.  View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.</div>",
					//                pInfo.CommunityDetailsURL, pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
					//                pInfo.CommunityName);
					//    }
					//}
				}
			}
			
			return
				string.Format(
					"<div class=\"sav-nolng\">This home is no longer available.<br />View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.</div>",
					pInfo.CommunityDetailsURL, pInfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
					pInfo.CommunityName);
			
		}
		#endregion

		#region Hfs Messages
		private string NoHfSStateMessage()
		{
			return "<div class=\"sav-nolng\">This home is no longer available.<br /><a href=\"/\">View other areas where we have homes for sale</a>.</div>";
		}

		private string NoHfSDivisionMessageWithState(HomeForSaleInfo hinfo)
		{
			return string.Format("<div class=\"sav-nolng\">This home is no longer available.<br />View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.</div>",
											hinfo.HomeforSaleCommunityDetailsURL.TrimUrlLastThreeText(), hinfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
											hinfo.State);
		}

		private string NoHfSCommunityMessageWithDivision(HomeForSaleInfo hinfo, Item dItem, Item cItem)
		{
			DateTime lastChanged = SCUtils.SCDateTimeToMsDateTime(cItem["Community Status Change Date"]);
			return string.Format("<div class=\"sav-nolng\">This home is no longer available and the community closed {3}.<br />View other communities in the <a href=\"{0}\" {1}>{2} area</a>.</div>",
									   hinfo.HomeforSaleCommunityDetailsURL.TrimUrlLastTwoText(),
									   hinfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
									   dItem.DisplayName, lastChanged.ToString("MMMM yyyy"));
		}


		private string NoHfSMessageWithCommunity(HomeForSaleInfo hinfo)
		{
			return string.Format("<div class=\"sav-nolng\">This home is no longer available.<br />View other homes for sale in <a href=\"{0}\" {1}>{2}</a>.</div>",
											hinfo.HomeforSaleCommunityDetailsURL, hinfo.IsCurrentDomain ? string.Empty : "target=\"_blank\"",
											hinfo.HomeforSaleCommunityName);
		}
		#endregion


		private List<string> GetIds(string favs)
		{
			return string.IsNullOrWhiteSpace(favs) ? null : favs.TrimStart('|').Split('|').Distinct().ToList();
		}

		private string BoundFavIds(IEnumerable<string> favs)
		{
			if (favs == null) return string.Empty; 
			var qryPath = string.Empty;

			foreach (var id in favs)
			{
				Guid newaid;
				if (Guid.TryParse(id, out newaid))
					qryPath += string.Format("@@id='{0}' or ", newaid);
			}

			//qryPath = favs.Aggregate(qryPath, (current, id) => current + string.Format("@@id='{0}' or ", id));
			return qryPath.TrimEnd(" or ");
		}

		//public string GetFavoriteLiveChat(string livePersonSkill, string id, string reqInfoUrl, string offPersonText)
		//{
		//    var serverName = HttpContext.Current.Request.Url.Host;
		//    id = id.Contains("{") ? id.TrimStart('{').TrimEnd('}') : id;
		//    var sb = new StringBuilder();
		//    sb.AppendFormat("<a id=\"chat-Button_{1}\" href=\"{0}/Request-Information\" onclick=\"TM.Common.GAlogevent('IHC', 'Click', 'StartLiveChat-IHCOffline');\">{2}</a>", reqInfoUrl, id, offPersonText);
		//    sb.AppendFormat("<div class=\"lv-chat-but\" id=\"pnLiveChat_{0}\" {1}>", id, string.IsNullOrWhiteSpace(livePersonSkill) ? "style=\"visibility:hidden;\"" : "");
		//    sb.AppendFormat("<a id=\"_lpChatBtn_{2}\" href='https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&byhref=1&imageUrl=http://{1}/~/media/images/livechat' " +
		//                    "target='chat70501840' style=\"position: absolute;\" onclick=\"lpButtonCTTUrl = 'https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&imageUrl=http://{1}/~/media/images/livechat&referrer='+escape(document.location); lpButtonCTTUrl = (typeof(lpAppendVisitorCookies) != 'undefined' ? lpAppendVisitorCookies(lpButtonCTTUrl) : lpButtonCTTUrl); lpButtonCTTUrl = ((typeof(lpMTag)!='undefined' && typeof(lpMTag.addFirstPartyCookies)!='undefined')?lpMTag.addFirstPartyCookies(lpButtonCTTUrl):lpButtonCTTUrl);window.open(lpButtonCTTUrl,'chat70501840','width=475,height=400,resizable=yes');return false;\"> <img id='chatimg_{2}' src='http://server.iad.liveperson.net/hc/70501840/?cmd=repstate&site=70501840&channel=web&&ver=1&SESSIONVAR!skill={0}&imageUrl=http://{1}/~/media/images/livechat' " +
		//                    "onload=\"LiveChatHeightTest(this, '{2}')\" name='hcIcon' alt='Chat Button' border=\"0\" \"></a>", livePersonSkill, serverName, id);
		//    sb.Append("</div>");
		//    return sb.ToString();
		//}


		public string GetFavoriteLiveChat(string livePersonSkill, string id, string reqInfoUrl, string offPersonText)
		{
			var serverName = HttpContext.Current.Request.Url.Host;
			id = id.Contains("{") ? id.TrimStart('{').TrimEnd('}') : id;
			var sb = new StringBuilder();
			sb.AppendFormat("<a id=\"chat-Button_{1}\" href=\"{0}/Request-Information\" onclick=\"TM.Common.GAlogevent('IHC', 'Click', 'StartLiveChat-IHCOffline', '');\">{2}</a>", reqInfoUrl, id, offPersonText);
			sb.AppendFormat("<div class=\"lv-chat-but\" id=\"pnLiveChat_{0}\" {1}>", id, string.IsNullOrWhiteSpace(livePersonSkill) ? "style=\"visibility:hidden;\"" : "");
			sb.AppendFormat("<a id=\"_lpChatBtn_{2}\" href='https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&byhref=1&imageUrl=http://{1}/~/media/images/livechat' " +
							"target='chat70501840' style=\"position: absolute;\" onclick=\"lpButtonCTTUrl = 'https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&imageUrl=http://{1}/~/media/images/livechat&referrer='+escape(document.location); lpButtonCTTUrl = (typeof(lpAppendVisitorCookies) != 'undefined' ? lpAppendVisitorCookies(lpButtonCTTUrl) : lpButtonCTTUrl); lpButtonCTTUrl = ((typeof(lpMTag)!='undefined' && typeof(lpMTag.addFirstPartyCookies)!='undefined')?lpMTag.addFirstPartyCookies(lpButtonCTTUrl):lpButtonCTTUrl);window.open(lpButtonCTTUrl,'chat70501840','width=475,height=400,resizable=yes');return false;\"></a>", livePersonSkill, serverName, id);
			sb.Append("</div>");
			return sb.ToString();
		}

		public string GetLiveChat(string livePersonSkill, string id, string reqInfoUrl)
		{
			var serverName = HttpContext.Current.Request.Url.Host;

			id = id.Contains("{") ? id.TrimStart('{').TrimEnd('}') : id;
			var sb = new StringBuilder();
			sb.AppendFormat("<a id=\"chat-Button_{1}\" href=\"{0}/Request-Information\">Request Information</a>", reqInfoUrl, id);
			sb.AppendFormat("<div id=\"pnLiveChat_{0}\">", id);
			sb.AppendFormat("<a id=\"_lpChatBtn_{2}\" href='https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&byhref=1&imageUrl=http://{1}/~/media/images/livechat' target='chat70501840' style=\"position: relative;\" onclick=\"lpButtonCTTUrl = 'https://server.iad.liveperson.net/hc/70501840/?cmd=file&file=visitorWantsToChat&site=70501840&SESSIONVAR!skill={0}&imageUrl=http://{1}/~/media/images/livechat&referrer='+escape(document.location); lpButtonCTTUrl = (typeof(lpAppendVisitorCookies) != 'undefined' ? lpAppendVisitorCookies(lpButtonCTTUrl) : lpButtonCTTUrl); lpButtonCTTUrl = ((typeof(lpMTag)!='undefined' && typeof(lpMTag.addFirstPartyCookies)!='undefined')?lpMTag.addFirstPartyCookies(lpButtonCTTUrl):lpButtonCTTUrl);window.open(lpButtonCTTUrl,'chat70501840','width=475,height=400,resizable=yes');return false;\"> <img src='http://server.iad.liveperson.net/hc/70501840/?cmd=repstate&site=70501840&channel=web&&ver=1&SESSIONVAR!skill={0}&imageUrl=http://{1}/~/media/images/livechat' name='hcIcon' alt='Chat Button' border=\"0\" onload=\"LiveChatHeightTest(this, '{2}')\"></a>",
				livePersonSkill, serverName, id);
			sb.Append("</div>");

			return sb.ToString();
		}

		private List<IFPInfo> GetIFPs(Guid planID)
		{
		    var shortID = ShortID.Encode(planID).ToLowerInvariant();
		    var ifps = new List<IFPInfo>();
		    ifps.AddRange(GetUserIFPs.Where(ifp => ifp.PlanID.ToLowerInvariant() == shortID));
	            
		    return ifps;
		}

	    private IEnumerable<IFPInfo> UserIFPs()
	    {
	        var ifps = new List<IFPInfo>();
	        if (Context.User.Name != "extranet\\Anonymous" && Context.User.IsAuthenticated)
	        {
	            var ifpInfo = Context.User.Profile.GetCustomProperty("IFPFavorites");


	            if (ifpInfo.IsNotEmpty())
	            {
	                ifps = ifpInfo.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries).Where(x =>!String.IsNullOrWhiteSpace(x)&&x!="|").Select(
	                    JsonConvert.DeserializeObject<IFPInfo>).ToList();
	               
	            }
	        }

	        if (Context.Item != null)
	        {
	            var ifpbuilderid = SCUtils.GetIFPBuilderID(Sitecore.Context.Item);
	            foreach (var ifpInfo in ifps)
	            {
	                ifpInfo.Url = ifpInfo.Url.AppendQuery("bid", ifpbuilderid);
	            }
	        }

	        return ifps;
	    }
	    
	    private IEnumerable<IFPInfo> GetUserIFPs
	    {
		get { return  UserIFPs(); }
	    }

	   public void RemoveIFPFromProfile(string id)
	   {
	       id = id.ToLowerInvariant();
	       var ifps = GetUserIFPs.ToList();
  	       ifps.RemoveAll(ifp =>  ifp.ID.ToLowerInvariant() == id);
	       string json = string.Empty;
	       foreach (var ifpInfo in ifps)
	       {
	            json += JsonConvert.SerializeObject(ifpInfo, Formatting.None)+"|";
	       }
	       using (new SecurityDisabler())
	       {
	           Context.User.Profile.SetCustomProperty("IFPFavorites", json);
	           Context.User.Profile.Save();
	       }
	   }
    }
	
}

