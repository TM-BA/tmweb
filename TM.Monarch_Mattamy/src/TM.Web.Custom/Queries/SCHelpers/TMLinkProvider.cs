﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.SCHelpers
{
    public static class TMLinkProvider
    {
        public static string GetItemUrl(Item item)
        {
            string ret = Sitecore.Links.LinkManager.GetItemUrl(item);
            //ret = ModifyUrl(item, ret);

            return ret;
        }
        public static string GetItemUrl(Item item,Sitecore.Links.UrlOptions options)
        {
            string ret = Sitecore.Links.LinkManager.GetItemUrl(item);
            //ret = ModifyUrl(item, ret);

            return ret;
        }

        public static string GetUrl (LinkField link)
        {
            string url;
            if (link.LinkType == "media")
            {
                MediaItem media = new MediaItem(link.TargetItem);
                url = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(media));
            }

            if (link.LinkType == "internal" && link.Url != "#")
            {
                url = string.Format("http://{0}", TMLinkProvider.GetRootPath(link.Url));
            }
            else
            {
                url = link.Url;
            }
            return url;
        }

        private static string GetRootPath(string link)
        {
            string domain = GetDomainFromPath(link);
            string linkWithoutDomain = GetLinkWithoutDomain(link);
            string hostname = string.Empty;
            foreach (Sitecore.Web.SiteInfo site in Sitecore.Configuration.Factory.GetSiteInfoList())
            {
                if (site.RootPath.IndexOf(domain, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    hostname = site.HostName;
                    break;
                }

            }
            return hostname + linkWithoutDomain;
        }
        private static string GetDomainFromPath(string link)
        {
            if (link.IndexOf(CommonValues.DarlingHomesDomain, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return CommonValues.DarlingHomesDomain;
            }
            if (link.IndexOf(CommonValues.TaylorMorrisonDomain, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return CommonValues.TaylorMorrisonDomain;
            }
            if (link.IndexOf(CommonValues.MonarchGroupDomain, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return CommonValues.MonarchGroupDomain;
            }
            if (link.IndexOf(CommonValues.Skyestone, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return CommonValues.Skyestone;
            }
            return string.Empty;
        }
        private static string GetLinkWithoutDomain(string link)
        {
            int index = link.IndexOf("/Home/", StringComparison.OrdinalIgnoreCase);
            if (index >= 0)
            {
                return link.Remove(0, index + 5).Replace(' ', '-');
            }
            return link;
        }
        //private static string ModifyUrl(Item item, string resolveUrl)
        //{
        //    return resolveUrl;
        //    switch (item.TemplateID.ToString())
        //    {
        //        case SCIDs.TemplateIds.SwitchCommunityPage:
        //            resolveUrl += "-Community";
        //            break;
        //        case SCIDs.TemplateIds.SwitchPlanPage:

        //            resolveUrl += "-Plan";
        //            break;
        //    }
        //    return resolveUrl;
        //}
    }
}