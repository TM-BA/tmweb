﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Search;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;
using TM.Web.Custom.Search.DynamicFields;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    //more:http://sitecoreblog.alexshyba.com/2010/11/sitecore-searcher-and-advanced-database.html
    public class PlanQueries
    {
        protected string FullTextQuery;
        private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";

        public PlanQueries()
            : this(Sitecore.Context.Database.Name)
        {
        }
	

        public PlanQueries(string db)
        {
            _ctxDB = db;
            _index = SearchManager.GetIndex(IndexName);
        }

        public Item GetPlanFromLegacyId(string legacyId)
        {
            var planQuery = new FieldSearchParam()
            {
                Database = _ctxDB,
                TemplateIds = SCIDs.TemplateIds.PlanPage.ToString(),
                FieldName = SCFieldNames.PlanPageFields.PlanLegacyID,
                FieldValue = legacyId
            };

            SkinnyItem plan;
            using (var runner = new QueryRunner(IndexName))
            {
                plan = runner.GetItems(planQuery).FirstOrDefault();
            }
            if (plan != null) return plan.GetItem();
            return null;
        }

        public IEnumerable<ID> GetHomesForSaleForRWEInDivision(ID divisionID)
        {
            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = divisionID.ToString(),
                TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString()
            };
            var refineMents = new List<MultiFieldSearchParam.Refinement>();

            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.DisplayAsAvailableHomeInRWE, "1"));
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.Active, SCIDs.StatusIds.Status.Active));
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.ActiveForSearch, SCIDs.StatusIds.Status.Active));

            conditions.Refinements = refineMents;
            var query = conditions.ProcessQuery(QueryOccurance.Must, _index);

            IEnumerable<SkinnyItem> allPlansMatchingCriteria;
            int totalResults;
            using (var runner = new QueryRunner(IndexName))
            {
                allPlansMatchingCriteria = runner.RunQuery(query, false, null, false, 0, 0, out totalResults);
            }

            if (totalResults == 0)
            {
                refineMents.RemoveAt(0);
                conditions.Refinements = refineMents;
                query = conditions.ProcessQuery(QueryOccurance.Must, _index);
                using (var runner = new QueryRunner(IndexName))
                {
                    allPlansMatchingCriteria = runner.RunQuery(query, false, null, false, 0, 0, out totalResults);
                }
            }

            var allHomesMatchingCriteria = allPlansMatchingCriteria.Select(c => c.ItemID).Distinct();

            return allHomesMatchingCriteria.Select(a => new ID(a));
        }

        public Item GetHomeForSaleFromLegacyId(string legacyId)
        {
            var hfsQuery = new FieldSearchParam()
            {
                Database = _ctxDB,
                TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString(),
                FieldName = SCFieldNames.HomeForSaleFields.HomeForSaleLegacyId,
                FieldValue = legacyId
            };

            SkinnyItem hfs;
            using (var runner = new QueryRunner(IndexName))
            {
                hfs = runner.GetItems(hfsQuery).FirstOrDefault();
            }
            if (hfs != null) return hfs.GetItem();
            return null;
        }

        public HomeTourOption IsAvailableAsModelHome(Item plan, Item currentDivision, Item currentCommunity,  out Item matchedInventory)
        {

          
            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = currentCommunity.ID.ToString(),
                TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString()
            };

            var refineMents = new List<MultiFieldSearchParam.Refinement>();

            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.IsModelHome, "1"));
            var isModelHome = new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.MasterPlanID,
                                                                   plan[SCIDs.PlanBaseFields.MasterPlanID]);
            refineMents.Add(isModelHome);
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.HomeForSaleStatus,
                                                                 SCIDs.GlobalSharedFieldValues.RecordStatus.Active.
                                                                     ToString()));

           

            conditions.Refinements = refineMents;

            var query = conditions.ProcessQuery(QueryOccurance.Must, _index);
            int totalResults = 0;
            List<SkinnyItem> searchResults;
            using (var runner = new QueryRunner(IndexName))
            {
                searchResults = runner.RunQuery(query, false, null, false, 0, 1, out totalResults);
            }

            var skinnyHome = searchResults.FirstOrDefault();
            matchedInventory = skinnyHome == null ? null : skinnyHome.GetItem();

            if (totalResults > 0) return HomeTourOption.AvailableAsModelHome;


            return HomeTourOption.None;
        }

        public HomeTourOption GetHomeTourOption(Item plan, Item currentDivision, Item currentCommunity, out Item nearByCommunity, out Item matchedInventory)
        {
           
            nearByCommunity = null;
            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = currentCommunity.ID.ToString(),
                TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString()
            };

            var refineMents = new List<MultiFieldSearchParam.Refinement>();

            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.IsModelHome, "1"));
            var isModelHome = new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.MasterPlanID,
                                                                   plan[SCIDs.PlanBaseFields.MasterPlanID]);
            refineMents.Add(isModelHome);
            
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.HomeForSaleStatus,
                                                                 SCIDs.GlobalSharedFieldValues.RecordStatus.Active.
                                                                     ToString()));


            conditions.Refinements = refineMents;

            var query = conditions.ProcessQuery(QueryOccurance.Must, _index);
            int totalResults = 0;
            List<SkinnyItem> searchResults;
            using (var runner = new QueryRunner(IndexName))
            {
                searchResults = runner.RunQuery(query, false, null, false, 0, 1, out totalResults);
            }

            var skinnyHome = searchResults.FirstOrDefault();
            matchedInventory = skinnyHome == null ? null : skinnyHome.GetItem();

            if (totalResults > 0) return HomeTourOption.AvailableAsModelHome;

            var communitiesWithin15Miles = SCUtils.CommunityWithinDistanceOf(currentCommunity, 15);

            communitiesWithin15Miles.Remove(currentCommunity.ID.ToString());

            conditions.LocationIds = string.Join("|", communitiesWithin15Miles.ToArray());

            query = conditions.ProcessQuery(QueryOccurance.Must, _index);

            totalResults = 0;

            using (var runner = new QueryRunner(IndexName))
            {
                searchResults = runner.RunQuery(query, false, null, false, 0, 1, out totalResults);
            }

            skinnyHome = searchResults.FirstOrDefault();
            matchedInventory = skinnyHome == null ? null : skinnyHome.GetItem();

            if (matchedInventory != null)
                nearByCommunity = matchedInventory.Parent.Parent;


            if (totalResults > 0) return HomeTourOption.AvailableAsModelHomeInNearByCommunity;

            conditions.LocationIds = currentCommunity.ID.ToString();

            refineMents.RemoveAll(r => r.Name == SCFieldNames.PlanPageFields.IsModelHome);

            refineMents.RemoveAll(r => r.Name == SCFieldNames.HomeForSaleFields.HomeForSaleStatus);

            //Sanjeevi
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.HomeForSaleFields.StatusforAvailability,
                                                  SCIDs.HomesForSalesFields.ReadyNow.ToString()));
          

            conditions.Refinements = refineMents;



            query = conditions.ProcessQuery(QueryOccurance.Must, _index);

            totalResults = 0;
            using (var runner = new QueryRunner(IndexName))
            {
                searchResults = runner.RunQuery(query, false, null, false, 0, 1, out totalResults);
            }

            skinnyHome = searchResults.FirstOrDefault();
            matchedInventory = skinnyHome == null ? null : skinnyHome.GetItem();
            if (totalResults > 0) return HomeTourOption.AvailableAsInventory;

            return HomeTourOption.None;

        }

	
       public IEnumerable<SkinnyItem> GetPlansInCommunityMatchingMasterPlan(ID communityID, string masterPlanID)
       {
           var conditions = new MultiFieldSearchParam()
           {
               Database = _ctxDB,
               LocationIds = communityID.ToString(),
               TemplateIds = SCIDs.TemplateIds.PlanPage.ToString(),
	    
           };
	     var refineMents = new List<MultiFieldSearchParam.Refinement>();

           refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.MasterPlanID,
                                                                   masterPlanID));
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.PlanStatus,
                                                                 SCIDs.GlobalSharedFieldValues.RecordStatus.Active.
                                                                     ToString()));
            conditions.Refinements = refineMents;

          
           IEnumerable<SkinnyItem> searchResults;
           var query = conditions.ProcessQuery(QueryOccurance.Must, _index);
           using (var runner = new QueryRunner(IndexName))
           {
              return runner.RunQuery(query);
           }

       }


	public IEnumerable<SkinnyItem> GetActiveHomesForSaleUnderPlan(ID plan)
	{
        var conditions = new FieldSearchParam()
        {
            Database = _ctxDB,
            LocationIds = plan.ToString(),
            TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString(),
	    FieldName = SCFieldNames.HomeForSaleFields.HomeForSaleStatus,
	    FieldValue = SCIDs.StatusIds.Status.Active

        };


	using(var runner = new QueryRunner(IndexName))
	{
	    return runner.GetItems(conditions);
	}
	}


    }
}
