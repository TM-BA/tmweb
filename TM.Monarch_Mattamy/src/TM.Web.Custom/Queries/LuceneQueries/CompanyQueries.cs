﻿using System.Collections.Generic;
using System.Linq;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Search;
using Sitecore.Shell.Applications.ContentEditor;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class CompanyQueries
    {

	  private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";

        public CompanyQueries()
            : this(Sitecore.Context.Database.Name)
        {
        }


        public CompanyQueries(string db)
        {
            _ctxDB = db;
            _index = SearchManager.GetIndex(IndexName);
        }
        public IEnumerable<Item> GetBlogSkinnyItems(ID companyID)
        {
            var conditions = new FieldSearchParam
            {
                Database = _ctxDB,
                LocationIds = companyID.ToString(),
                TemplateIds = SCIDs.Blog.TM.Entry.ToString()
              
            };


            using (var runner = new QueryRunner(IndexName))
            {
                return runner.GetItems(conditions).Select(i => i.GetItem());
            }
        }
    }
}
