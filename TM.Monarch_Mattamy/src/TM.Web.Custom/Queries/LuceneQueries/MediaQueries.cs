﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;
using Sitecore.Data;
using Sitecore.Forms.Core.Data;
using Sitecore.Search;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;


namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class Image
    {
        public string[] Categories { get; set; }
        public string URL { get; set; }
        public string Caption { get; set; }

    }

    public class MediaQueries
    {
        protected string FullTextQuery;
        private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";
        private Database _DB = Sitecore.Context.Database;

        public MediaQueries()
            : this(Sitecore.Context.Database.Name)
        {
        }
        public MediaQueries(string db)
        {
            _ctxDB = db;
            _index = SearchManager.GetIndex(IndexName);
        }


        public string GetInspirationGalleryImages(string currentDivisionName)
        {
            var query =
            "fast:/sitecore/Media Library//*[@#Show In Gallery#='1']";
            var images = _DB.SelectItems(query);

            var company = Sitecore.Context.GetSiteName().ToLower();
            
            string gallery;

            var imageList = new List<Image>();

            switch (company)
            {
                case CommonValues.DarlingHomesDomain:
                    gallery = "DH ";
                    break;
                case CommonValues.MonarchGroupDomain:
                    gallery = "MG ";
                    break;
                default:
                    gallery = "TM ";
                    break;

            }

            gallery = currentDivisionName.IsEmpty() || currentDivisionName == "InspirationGallery" ? gallery : currentDivisionName + " ";

            foreach (var image in images)
            {
                if (image.Fields[gallery + "Categories"].Value != string.Empty)
                {
                    var categoryList = image.Fields[gallery + "Categories"].Value.Split('|');
                    if (categoryList.Length > 0)
                    {
                        var categoryNames = new List<string>();
                        foreach (var category in categoryList)
                        {
                            var fieldItem = new FieldItem(_DB.GetItem(category));
                            categoryNames.Add(fieldItem.FieldDisplayName);
                        }
                        var imageObject = new Image()
                                              {
                                                  Caption = image.Fields["Caption"].Value,
                                                  Categories = categoryNames.ToArray(),
                                                  URL = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image)
                                              };
                        imageList.Add(imageObject);
                    }
                }

            }
            return JsonConvert.SerializeObject(imageList);

        }

        //public IEnumerable<ID> GetInspirationGalleryImages()
        //{
        //    var searchParam = new MultiFieldSearchParam
        //    {
        //        SearchBaseTemplates = false,
        //        Database = _ctxDB,
        //        Language = "en",
        //        FullTextQuery = FullTextQuery,
        //        LocationIds = SCIDs.TemplateIds.MediaLibrary,
        //        InnerCondition = QueryOccurance.Must

        //    };

        //    var refineMents = new List<MultiFieldSearchParam.Refinement>
        //                         {
        //                             new MultiFieldSearchParam.Refinement(SCFieldNames.ShowInGallery,
        //                                                                 "1"),
        //                            new MultiFieldSearchParam.Refinement(Sitecore.Search.BuiltinFields.LatestVersion,"1"),

        //                         };
        //    searchParam.Refinements = refineMents;
        //    var query = searchParam.ProcessQuery(QueryOccurance.Must, _index);

        //    IEnumerable<SkinnyItem> items;
        //    int totalResults;
        //    using (var runner = new QueryRunner(IndexName))
        //    {
        //        items = runner.RunQuery(query, false, null, false, 0, 0, out totalResults);
        //    }
        //    var allHomesMatchingCriteria = items.Select(c => c.ItemID).Distinct();
        //    return allHomesMatchingCriteria.Select(a => new ID(a));
        //}
    }
}