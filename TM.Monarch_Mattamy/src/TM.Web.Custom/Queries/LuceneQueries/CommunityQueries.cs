﻿using System.Collections.Generic;
using System.Linq;
using Lucene.Net.Search;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Search;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.DynamicFields;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;
using scSearchContrib.Searcher.Utilities;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class CommunityQueries
    {
        protected string FullTextQuery;
        private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";

	

        public CommunityQueries() : this(Sitecore.Context.Database.Name)
        {
            
        }

        public CommunityQueries(string contextDB)
        {
            _ctxDB = contextDB;
            _index = SearchManager.GetIndex(IndexName);
        }

        public IEnumerable<SkinnyItem> GetAllActiveCommunitesUnderCity(ID cityItemID)
        {
            var inactiveComms = new FieldSearchParam()
            {

                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.InActive.ToString(),
                LocationIds = cityItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };



            var closedComm = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.Closed.ToString(),
                LocationIds = cityItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };




            var searchableComms = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.StatusforSearch,
                FieldValue =
                    SCIDs.StatusIds.Status.Active,
                LocationIds = cityItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };

            var isLatestVersionParameter = new FieldSearchParam()
            {
                FieldName = Sitecore.Search.BuiltinFields.LatestVersion,
                FieldValue = "1",
		 LocationIds = cityItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString(),
                Condition = QueryOccurance.Must
            }; 

            var q1 = inactiveComms.ProcessQuery(QueryOccurance.Must, _index);
            var q2 = closedComm.ProcessQuery(QueryOccurance.Must, _index);
            var q3 = searchableComms.ProcessQuery(QueryOccurance.Must, _index);
            var q4 = isLatestVersionParameter.ProcessQuery(QueryOccurance.Must, _index);
            var bool1 = new BooleanClause(q1, BooleanClause.Occur.MUST_NOT);
            var bool2 = new BooleanClause(q2, BooleanClause.Occur.MUST_NOT);
            var bool3 = new BooleanClause(q3, BooleanClause.Occur.MUST);
            var bool4= new BooleanClause(q4, BooleanClause.Occur.MUST);

            var boolQ = new BooleanQuery();
            boolQ.Add(bool3);
            boolQ.Add(bool1);
            boolQ.Add(bool2);
            boolQ.Add(bool4);

            using (var runner = new QueryRunner(IndexName))
            {
               return runner.RunQuery(boolQ);
            }
	   
	   
            
        }

        public IEnumerable<ID> GetCommunitiesMatchingMasterPlanInDivision(ID divisionID, string masterPlanID)
        {
            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = divisionID.ToString(),
                TemplateIds = SCIDs.TemplateIds.PlanPage.ToString()
            };

            var refineMents = new List<MultiFieldSearchParam.Refinement>();

         
            var matchingMasterPlan = new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.MasterPlanID,
                                                                   masterPlanID);
            refineMents.Add(matchingMasterPlan);
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.PlanPageFields.PlanStatus,
                                                                 SCIDs.GlobalSharedFieldValues.RecordStatus.Active.
                                                                     ToString()));
            conditions.Refinements = refineMents;

            var query = conditions.ProcessQuery(QueryOccurance.Must, _index);

            IEnumerable<SkinnyItem> allPlansMatchingCriteria;
            using (var runner = new QueryRunner(IndexName))
            {
                allPlansMatchingCriteria = runner.RunQuery(query);
            }

           var allCommunitiesMatchingCriteria =  allPlansMatchingCriteria.Select(c => c.Fields[LuceneDynamicFieldNames.ParentID]).Distinct();

           return allCommunitiesMatchingCriteria.Select(a => new ID(a));
        }


        public IEnumerable<SkinnyItem> GetAllSearchableHomesForSaleUnderCommunity(ID community)
        {
            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = community.ToString(),
                TemplateIds = SCIDs.TemplateIds.HomeForSalePage.ToString(),
                Refinements = new List<MultiFieldSearchParam.Refinement>
	                      {
	                          new MultiFieldSearchParam.Refinement("Home for Sale Status", SCIDs.StatusIds.Status.Active ),
	                          new MultiFieldSearchParam.Refinement("Status for Search", SCIDs.StatusIds.Status.Active ),
	                          new MultiFieldSearchParam.Refinement("Display on Homes for Sale Page", "1" ),
	                        
	                      },
	       Condition = QueryOccurance.Must

            };

            using (var runner = new QueryRunner(IndexName))
            {
                return runner.GetItems(conditions);
            }
        }


        public Item GetCommunityFromLegacyId(string legacyId)
        {
            var communityQuery = new FieldSearchParam()
            {
                Database = _ctxDB,
                TemplateIds = SCIDs.TemplateIds.CommunityPage.ToString(),
                FieldName = SCFieldNames.CommunityFields.CommunityLegacyId,
                FieldValue = legacyId
            };

            SkinnyItem community;
            using (var runner = new QueryRunner(IndexName))
            {
                community = runner.GetItems(communityQuery).FirstOrDefault();
            }
            if (community != null) return community.GetItem();
            return null;
        }
    }
}