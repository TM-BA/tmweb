﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data.Query;
using TM.Domain;
using TM.Utils.Extensions;
using Sitecore.Data;
using TM.Utils.Web;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Queries
{
    public class SCFastQueries
    {
        /*
         Sitecore Fast Query only supports the following axes:
            parent::
            child::
            ancestor:: (when the FastQueryDescendantsDisabled setting is set to “false”)
            descendant:: (when the FastQueryDescendantsDisabled setting is set to “false”)
         Sitecore Fast Query only supports the following special attributes:
            @@id
            @@name
            @@key
            @@templateid
            @@templatename
            @@templatekey
            @@masterid
            @@parentid
         Sitecore Fast Query does not work correctly if you place the special attribute at the beginning of the condition.
         Sitecore Fast Query does take the context language into account when it checks the value of non-shared fields.
         * The query results therefore include all the items that have one or more language versions that match the query expression.
         Sitecore Fast Query does not support sorting.
                 */


        public SCFastQueries()
        {
            if (RefereshCache)
            {
                if (HttpContext.Current.Items["ItemCacheRefreshed"].CastAs<bool>(false))
                {
                    Sitecore.Context.Database.Caches.ItemCache.Clear();
                    HttpContext.Current.Items["ItemCacheRefreshed"] = true;
                }

            }
        }

        private  bool RefereshCache
        {
            get
            {
                var refereshCache = HttpContext.Current.Request.QueryString["clearcache"] != null;

                //handle ajax
                var uriReferrer = HttpContext.Current.Request.UrlReferrer;
                if (uriReferrer != null && uriReferrer.Query.Contains("clearcache"))
                {
                    refereshCache = true;
                }
                return refereshCache;
            }
        }

        public static string FindAllActivePromotionsUnder(string rootPath)
        {

            var escapedPath = EscapeFastQueryPath(rootPath);
            string SCTimeNow = TM.Web.Custom.SCHelpers.SCUtils.MSDateTimeToSCDateTime(DateTime.Now);

            var query = "fast:/{0}/*[@@templateid='{1}']/*[{2}]".FormatWith(escapedPath, SCIDs.Promos.Promotions,
                "@Disabled ='' and @Start Date < '" + SCTimeNow + "' and @End Date > '" + SCTimeNow + "'"
                );
            return query;
        }

        public static string FindItemsMatchingTemplateUnder(string rootPath, ID usingTemplateID)
        {

            var escapedPath = EscapeFastQueryPath(rootPath);

            var query = "fast:/{0}//*[@@templateid='{1}']".FormatWith(escapedPath, usingTemplateID.ToString());
            return query;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="usingTemplateID"></param>
        /// <param name="withTrue">@FieldName1='value' and @FieldName2='value'</param>
        /// <returns></returns>
        public static string FindItemsMatchingTemplateUnder(string rootPath, ID usingTemplateID, string withTrue)
        {

            var escapedPath = EscapeFastQueryPath(rootPath);
            var query = "fast:/{0}//*[@@templateid='{1}' and {2}]"
                .FormatWith(escapedPath, usingTemplateID.ToString(), withTrue);
            return query;
        }
        public static string FindItemsMatchingTemplateUnder(string rootPath, ID usingTemplateID, string withField, bool havingOrNotHaving, string fieldValue)
        {

            var escapedPath = EscapeFastQueryPath(rootPath);
            var query = "fast:/{0}//*[ @{2} {3} '{4}' and @@templateid='{1}']"
                .FormatWith(escapedPath, usingTemplateID.ToString(), withField, havingOrNotHaving ? "=" : "!=", fieldValue);
            return query;
        }

        public static string FindMatchingTemplates(string rootPath, ID usingTemplateID)
        {
            var escapedPath = EscapeFastQueryPath(rootPath);
            var query = "fast:{0}/descendant::*[@@templateid = '{1}']".FormatWith(rootPath, usingTemplateID);

            return query;
        }

        public static string AllActiveCommunitiesUnder(string rootPath)
        {
         
            return FindItemsMatchingTemplateUnder(rootPath,
                                                SCIDs.TemplateIds.CommunityPage,
                                                SCFieldNames.CommunityFields.CommunityStatus,
                                                false, 
                                               SCIDs.StatusIds.CommunityStatus.InActive.ToString());
        }



        public static string AllActiveStatesUnder(string rootPath)
        {
            var escapedPath = EscapeFastQueryPath(rootPath);
            var query = "fast:{0}/descendant::*[@@templateid = '{1}']".FormatWith(rootPath,SCIDs.TemplateIds.StatePage);

            return query;
        }


	/// <summary>
        /// e.g.fast:/sitecore/content/TaylorMorrison/Home/New Homes/Texas/Austin/Austin/Brodie Springs/../*/*[@@templateid='{8722CBBA-4A10-41FB-B6DD-6E56051EF666}' AND @Master Plan ID='{48AC50F2-20F2-4134-A40E-30A59EB9139B}' AND @Plan Status='{F1B5AF6A-35A0-4433-A275-E111066A7B20}' ]/..
	/// </summary>
	/// <param name="masterPlanID"></param>
	/// <param name="usingCurrentDivisionPath"></param>
	/// <returns></returns>
        public static string AllCommunitiesWithActivePlansBasedOn(string masterPlanID, string usingCurrentDivisionPath)
        {
            var escapedPath = EscapeFastQueryPath(usingCurrentDivisionPath);
            var planTemplateID = SCIDs.TemplateIds.PlanPage;
            var activePlanStatus = SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString();
            var query =
                "fast:/{0}//*[@@templateid='{1}' AND @Master Plan ID='{2}' AND @Plan Status='{3}']/.."
                .FormatWith(escapedPath,planTemplateID,masterPlanID,activePlanStatus);

            return query;
        }

        public static string AllActivePlansBasedOn(string masterPlanID, string underCommunity)
        {
            var escapedPath = EscapeFastQueryPath(underCommunity);
            var planTemplateID = SCIDs.TemplateIds.PlanPage;
            var activePlanStatus = SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString();
            var query =
                "fast:/{0}//*[@@templateid='{1}' AND @Master Plan ID='{2}' AND @Plan Status='{3}']"
                .FormatWith(escapedPath, planTemplateID, masterPlanID, activePlanStatus);

            return query;
        }


	public static string AllActiveDivisionsUnder(string path)
	{
	    var escapedPath = EscapeFastQueryPath(path);
	    var divisionTemplateID = SCIDs.TemplateIds.DivisionPage;
	    var query = "fast:/{0}//*[@@templateid='{1}' and @{2}='{3}']".FormatWith(
            escapedPath, divisionTemplateID, SCFieldNames.CommunityFields.StatusforSearch, SCIDs.StatusIds.Status.Active);

	    return query;
	}

    public static string AllDivisionsAvaialbleForAssistanceUnder(string path)
    {
        var escapedPath = EscapeFastQueryPath(path);
        var divisionTemplateID = SCIDs.TemplateIds.DivisionPage;
        var query = "fast:/{0}//*[@@templateid='{1}' and @{2}='{3}']".FormatWith(
            escapedPath, divisionTemplateID, SCFieldNames.CommunityFields.StatusforAssistance, SCIDs.StatusIds.Status.Active);

        return query;
    }


        public static string EscapeFastQueryPath(string path)
        {
            var splitPaths = path.Split('/');
            var escapedPaths = splitPaths.Select(splitPath => (splitPath.Contains("-") || splitPath.Contains(" and")) ? "#{0}#".FormatWith(splitPath) : splitPath).ToList();
            
            return String.Join("/", escapedPaths);
        }

        public static string GetMySiblings( Item item)
        {
           
            var mytemplateID = item.TemplateID.ToString();
            var myParentPath = EscapeFastQueryPath(item.Parent.Paths.FullPath); ;
            var query = "fast:/{0}/*[@@templateid='{1}']".FormatWith(myParentPath,mytemplateID);

            return query;
        }

        public static string CrossCompaniesCondition
        {
            get
            {
                return String.Format("[@@id='{0}' or @@id='{1}' or @@id='{2}']", SCIDs.CompanyIds.DarlingHomes, SCIDs.CompanyIds.MonarchGroup, SCIDs.CompanyIds.TaylorMorrison);
            }
        }

        public static string SameCompanyCondition
        {
            get
            {
                var company = Sitecore.Context.GetSiteName().ToLower();
                var companyId = SCIDs.CompanyIds.TaylorMorrison;
                switch (company)
                {
                    case CommonValues.DarlingHomesDomain:
                        companyId = SCIDs.CompanyIds.DarlingHomes;
                        break;
                    case CommonValues.MonarchGroupDomain:
                        companyId = SCIDs.CompanyIds.MonarchGroup;
                        break;
                }

                return String.Format("[@@id='{0}']", companyId);

            }
        }
		
        public static string ActiveCommunitiesCondition
        {
            get
            {
				return String.Format("[@@templateid='{0}' and @Community Status != '{1}' and @Community Status != '' and @Status for Search = '{2}']", SCIDs.CommunityFields.CommunityTemplateID, SCIDs.StatusIds.CommunityStatus.InActive, SCIDs.StatusIds.Status.Active);
            }
        }

        public static string ActiveCommunitiesConditionHTH
        {
            get
            {
                return String.Format("[@@templateid='{0}' and @Community Status != '{1}' and @Community Status != '' and @Status for Search = '{2}' and @Is a High Tech Home Community = '{3}']", SCIDs.CommunityFields.CommunityTemplateID, SCIDs.StatusIds.CommunityStatus.InActive, SCIDs.StatusIds.Status.Active, "1");
            }
        }


        

		public static string AllCommunitiesCondition
		{
			get
			{
				return String.Format("[@@templateid='{0}' and @Community Status != '{1}' and @Community Status != '']", SCIDs.CommunityFields.CommunityTemplateID, SCIDs.StatusIds.CommunityStatus.InActive);
			}
		}

		public static string ActiveCommunitiesConditionwithoutClosed
		{
			get
			{
				return String.Format("[@@templateid='{0}' and @Community Status != '{1}' @Community Status != '{2}' and @Community Status != '' and @Status for Search = '{3}']", SCIDs.CommunityFields.CommunityTemplateID, SCIDs.StatusIds.CommunityStatus.InActive, SCIDs.StatusIds.CommunityStatus.Closed,  SCIDs.StatusIds.Status.Active);
			}
		}
		
		public static string ActiveCommunitiesWithoutHighTechCondition
		{
			get
			{
				return String.Format("[@@templateid='{0}' and @Community Status != '{1}' and @Community Status != '' and @Status for Search = '{2}' and @Is a High Tech Home Community != '1']", SCIDs.CommunityFields.CommunityTemplateID, SCIDs.StatusIds.CommunityStatus.InActive, SCIDs.StatusIds.Status.Active);
			}
		}

	    public static string StateDivisionCityCondition
	    {
		    get
		    {
				return string.Format("/*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}']", SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
		    }

	    }

	    public static string ActiveSubCommunitiesCondition
        {
            get
            {
                return String.Format("[@@templateid='{0}' and @Subcommunity Status = '{1}']", SCIDs.CommunityFields.SubCommunityTemplateID, SCIDs.StatusIds.Status.Active);
            }
        }

		public static string ActiveDesignStudio
		{
			get
			{
				return String.Format("[@@templateid='{0}' and @Design Center Status = '{1}']", SCIDs.TemplateIds.DesignStudio, SCIDs.StatusIds.Status.Active);
			}
		}

        public static string ActivePlans
        {
            get
            {
                return String.Format("[@@templateid='{0}' and @Status for Search = '{1}']", SCIDs.TemplateIds.PlanPage, SCIDs.StatusIds.Status.Active);
				//return String.Format("[@@templateid='{0}']", SCIDs.TemplateIds.PlanPage, SCIDs.StatusIds.Status.Active);
            }
        }

        public static string JustActivePlans
        {
            get
            {
                return String.Format("[@@templateid='{0}' and @Plan Status = '{1}']", SCIDs.TemplateIds.PlanPage, SCIDs.StatusIds.Status.Active);
                //return String.Format("[@@templateid='{0}']", SCIDs.TemplateIds.PlanPage, SCIDs.StatusIds.Status.Active);
            }
        }

		public static string CommunityPromo
		{
			get
			{
				return String.Format("[@@templateid='{0}']/*[@@templateid='{1}' or @@templateid='{2}']", SCIDs.TemplateIds.PromoFolder, SCIDs.Promos.GenericCampaign, SCIDs.Promos.SpecificCampaign);
			}
		}

		public static string AllFavPlans
		{
			get
			{
				return String.Format("[@@templateid='{0}']", SCIDs.TemplateIds.PlanPage);
			}
		}

        public static string ActiveHomeForSales
        {
            get
            {
				return String.Format("[@@templateid='{0}' and @Status for Search = '{1}' and @Home for Sale Status = '{1}' and @Display on Homes for Sale Page = '1']", SCIDs.TemplateIds.HomeForSalePage, SCIDs.StatusIds.Status.Active);
            }
        }

		public static string AllHomeForSales
		{
			get
			{
				return String.Format("[@@templateid='{0}']", SCIDs.TemplateIds.HomeForSalePage);
			}
		}

		public static string CrossCompanyProductTypeStateDiviCityCondition
		{
			get
			{
				return string.Format("fast:/sitecore/content/*{0}/Home/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']/*[@@templateid='{4}']",
					CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage,SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
			}
		}

		public static string CrossCompanyProductTypeStateDiviCityCommunityCondition
		{
			get
			{
				return string.Format("fast:/sitecore/content/*{0}/Home/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']/*[@@templateid='{4}']/*[@@templateid='{5}']",
					CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage, SCIDs.TemplateIds.CommunityPage);
			}
		}

	

        public static string GetAllPlansUnderDivison(string divisionPath, PlanSearchCriteria planSearchCriteria)
        {
            var queryBuilder = new StringBuilder();
            queryBuilder.Append(divisionPath + "//*[@@templateid='{8722CBBA-4A10-41FB-B6DD-6E56051EF666}' AND ");

            var sqFtQuery = GetRangeQuery(planSearchCriteria.SqFtMin, planSearchCriteria.SqFtMax,
                                                "@Square Footage");


            var bedRoomQuery = GetRangeQuery(planSearchCriteria.BedMin, planSearchCriteria.BedMax,
                                             "@Number of Bedrooms");

            var bathRoomQuery = GetRangeQuery(planSearchCriteria.BathMin, planSearchCriteria.BathMax,
                                              "@Number of Bathrooms");

            var halfBathRoomQuery = GetRangeQuery(planSearchCriteria.HalfBathMin, planSearchCriteria.HalfBathMax,
                                                  "@Number of Half Bathrooms");

           
            if (sqFtQuery.IsNotEmpty())
                queryBuilder.Append(sqFtQuery + " AND ");

            if (bedRoomQuery.IsNotEmpty())
                queryBuilder.Append(bedRoomQuery + " AND ");
            
            if (bathRoomQuery.IsNotEmpty())
                queryBuilder.Append(bathRoomQuery + " AND ");

            if (halfBathRoomQuery.IsNotEmpty())
                queryBuilder.Append(halfBathRoomQuery + " AND ");

            if (planSearchCriteria.Story > 0)
                queryBuilder.AppendFormat("(@Number of Stories>={0})", planSearchCriteria.Story);


            return queryBuilder.ToString().TrimEnd(" AND ")+"]";

        }

	private static string GetRangeQuery(int min, int max, string fieldName)
	{
	    if(min<=0 && max <=0) return string.Empty;

	    if (min > 0 && max > 0)
	        return string.Format("({0}>={1} AND {0}<={2})", fieldName, min, max);

	    if(min > 0 && max <=0 )
	        return string.Format("({0}>={1})", fieldName, min);

	    if(min <= 0 && max > 0)
	        return string.Format("({0}<={1})", fieldName, max);

	    return string.Empty;
	}
    }
}
