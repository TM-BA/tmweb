﻿using System;
using System.Configuration;
using Sitecore.Links;
using TM.UrlRewriter;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.UrlRedirecting.Transforms
{
    public class GetDivisionUrlTransform : IRewriteTransform
    {
        public string ApplyTransform(string input)
        {
           var contentDB = TM.UrlRewriter.Utilities.RewriterContants.Constants.Get("db");
            var divisionItem =  new DivisionQueries(contentDB).GetDivisionFromLegacyId(input);
            return UrlTransformUtilities.GetUrl(divisionItem);
        }

        public string Name
        {
            get { return "getdivisionurl"; }
        }
    }
}