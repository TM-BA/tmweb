﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.UrlRedirecting.Transforms
{
    public class UrlTransformUtilities
    {
        public static string GetUrl(Sitecore.Data.Items.Item item)
        {
            if (item == null) return "/";
            var url = TMLinkProvider.GetItemUrl(item);
            var homeIndex = url.IndexOf("home/", StringComparison.InvariantCultureIgnoreCase);
            homeIndex = homeIndex == -1 ? 0 : homeIndex + 4;
            url = url.Substring(homeIndex, url.Length - homeIndex);
            return url;
        }
    }
}