﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.LuceneQueries;
using ListItemCollection = Sitecore.Form.Web.UI.Controls.ListItemCollection;
using SCContext = Sitecore.Context;

namespace TM.Web.Custom.Forms
{
    public class WFFMCommunityDropListStatusForAssistance : DropList
    {
        private DropDownList _divisionDropList;

        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof (BooleanField)), Localize]
        public string HideTitle
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value == "No"; }
        }


        protected override void InitItems(ListItemCollection items)
        {
            //load items in init so viewstate can be set
            base.InitItems(GetItems());
        }

        protected override void OnLoad(EventArgs e)
        {
            var prevSelectedValue = droplist.SelectedValue;
            droplist.Items.Clear();
            droplist.Items.AddRange(GetItems().ToArray());

           if (droplist.Items.FindByValue(prevSelectedValue) != null)
                droplist.SelectedValue = prevSelectedValue;

            base.OnLoad(e);
        }

        
        private ListItemCollection GetItems()
        {
            var WebSession = new TMSession();
            string siteName = SCContext.Site.Name;
            var divisionDDLID = WebSession[string.Format("WFFM{0}contactusareaddl", siteName)] as string;
            if (divisionDDLID.IsNotEmpty())
            {
                IEnumerable<Item> communities =
                    new DivisionQueries().GetAllCommunitiesAvaialableForAssitance(divisionDDLID);
                var sortedList = new List<ListItem>();

                foreach (Item community in communities)
                {
                    try
                    {
                        if (community != null)
                        {
                            string communityName = community[SCIDs.CommunityFields.CommunityName];
                            string communityID = community[SCIDs.CommunityFields.CommunityLegacyID];
                            if (communityName != null && communityName.IsNotEmpty())
                            {
                                sortedList.Add(new ListItem(communityName, communityID));
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Log.Error(
                            "Error enumerating available communities. Last community: " + community.Paths.FullPath,
                            exception, this);
                    }
                }
                sortedList.Sort((a, b) => string.Compare(a.Text, b.Text));
                return new ListItemCollection(sortedList);
            }

            return new ListItemCollection();
        }
    }
}
