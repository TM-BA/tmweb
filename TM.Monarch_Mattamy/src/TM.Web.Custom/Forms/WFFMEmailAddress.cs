﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.Forms
{
    public class WFFMEmailAddress : WFFMSingleLineTextBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (CurrentWebUser!=null && CurrentWebUser.Email.IsNotEmpty()) textbox.Text = CurrentWebUser.Email;
        }


        [VisualProperty("Default Value:", 100), DefaultValue(""), Localize]
        public override string Text
        {
            get
            {

                return textbox.Text;
            }
            set
            {
                if (CurrentWebUser != null && CurrentWebUser.Email.IsNotEmpty()) textbox.Text = CurrentWebUser.Email;
                else
		  textbox.Text = value; 
            }
        }


    }
}