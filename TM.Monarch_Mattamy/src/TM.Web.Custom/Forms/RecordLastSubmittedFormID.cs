﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Submit;
using TM.Utils.Web;

namespace TM.Web.Custom.Forms
{
    public class RecordLastSubmittedFormID: BaseCheckAction
    {
        public override void Execute(ID formid, IEnumerable<ControlResult> fields)
        {
            TMSession WebSession = new TMSession();
            WebSession["WFFMPostedFormID"] = formid.ToString();
            WebSession.Save();
        }
    }
}