﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Pipelines.FormSubmit;

namespace TM.Web.Custom.Forms.Pipelines
{
    public class FormSubmit
    {
	public void Process(SubmittedFormFailuresArgs failedArgs)
	{
           Assert.IsNotNull((object)failedArgs.FormID, "FormID");

           HttpContext.Current.Items["WFFMPostedFormID"] = failedArgs.FormID.ToString();
	}
    }
}