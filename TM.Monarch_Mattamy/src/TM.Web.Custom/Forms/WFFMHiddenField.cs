﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Sites;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Forms
{
    public class WFFMHiddenField : SingleLineText
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.textbox.Visible = false;
            base.generalPanel.Visible = false;
            title.Visible = false;
        }

        protected Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        protected Item SCContextItem
        {
            get { return Sitecore.Context.Item; }
        }

        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }

        protected Item CurrentHomeItem
        {
            get { return SCContextDB.GetItem(SCCurrentSite.StartPath); }
        }

        protected Item CurrentCommunity
        {
            get
            {
                var url = HttpContext.Current.Request.Url;

                if (url.ToString().ToLowerInvariant().Contains("request-information"))
                {
                    // Get the path to the Home item
                    var homePath = Sitecore.Context.Site.StartPath;
                    if (!homePath.EndsWith("/"))
                        homePath += "/";
                    var absPath =  new Regex("request-information", RegexOptions.IgnoreCase).Replace(url.AbsolutePath, string.Empty); 
                    var idx = absPath.LastIndexOf("/", System.StringComparison.Ordinal) + 1;
                    var path = absPath.Substring(0, idx);
                    // Get the path to the item, removing virtual path if any
                    var itemPath = MainUtil.DecodeName(path);
                    if (itemPath.StartsWith(Sitecore.Context.Site.VirtualFolder))
                        itemPath = itemPath.Remove(0, Sitecore.Context.Site.VirtualFolder.Length);

                    // Obtain the item
                    var fullPath = homePath + itemPath;
                    var item = Sitecore.Context.Site.Database.GetItem(fullPath);
                    return item;
                }
                else
                {
                    return SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, SCContextItem.ID,
                                                                CurrentHomeItem);
                }
            }
        }
    }
}