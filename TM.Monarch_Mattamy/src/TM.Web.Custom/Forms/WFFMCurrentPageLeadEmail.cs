﻿using System;
using System.ComponentModel;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Net;
using System.Text;
using System.Web;

namespace TM.Web.Custom.Forms
{
    public class WFFMCurrentPageLeadEmail : WFFMSingleLineTextBase
    {
        /*
         * Add additional field to template called "Lead Email In Addition to IHC" field that can be used to send email to Sales Office or Other email. If none specified use IHC email.
         */

	  protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (CurrentCommunity != null && !string.IsNullOrWhiteSpace(CurrentCommunity["Lead Email In Addition to IHC"]) ) 
                textbox.Text = CurrentCommunity["Lead Email In Addition to IHC"];
        }




        [VisualProperty("Default Value:", 100), DefaultValue(""), Localize]
        public override string Text
        {
            get
            {

                return textbox.Text;
            }
            set
            {
                if (CurrentCommunity != null && !string.IsNullOrWhiteSpace(CurrentCommunity["Lead Email In Addition to IHC"]))		                     textbox.Text = CurrentCommunity["Lead Email In Addition to IHC"];
                else
                    textbox.Text = value; ;
            }
        }
    }
 }
