﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Linq;
using DateUtil = Sitecore.DateUtil;

namespace TM.Web.Custom.Forms
{
    public class FormPostSaveAction : ISaveAction, ISubmit
    {
        private Item _currentDivision;

        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                string url = string.Empty;
                var formFieldNameValues = new NameValueCollection();
                foreach (AdaptedControlResult field in fields)
                {
                    var fieldItem = new FieldItem(Context.Database.GetItem(field.FieldID));

                    if (fieldItem.ClassName.Contains("FormPostURL"))
                    {
                        url = field.Value;
                    }
                    else if (fieldItem.ClassName.Contains("WFFMDivisionID"))
                    {
                        if(CurrentDivision!=null)
                            formFieldNameValues.Add("DivisionID",  CurrentDivision[SCIDs.DivisionFieldIDs.LegacyDivisionID]);
                    }
                    else if(fieldItem.ClassName.Contains("WFFMDivisionDropList"))
                    {
                        var div = string.IsNullOrEmpty(field.Value)
                                      ? CurrentDivision[SCIDs.DivisionFieldIDs.LegacyDivisionID]
                                      : field.Value;
                        formFieldNameValues.Add("DivisionID", div);
                    }
                    else if (fieldItem.ClassName.Contains("WFFMCompanyWorkflowID"))
                    {
                        var wId = field.Value;
                        formFieldNameValues.Add("WorkflowID", wId);
                    }
                    else
                    {
                        var fieldName = ExtensionsToString.Between(fieldItem.LocalizedParameters, "<FieldName>", "</FieldName>");
			fieldName = fieldName.IsEmpty() ? fieldItem.Title : fieldName;
                        
                        if(!string.IsNullOrEmpty(field.Value))
                        {
                            if(formFieldNameValues.Get(fieldName)!=null)
                            {
                                formFieldNameValues[fieldName] = field.Value;
                            }
                            else
                            {
                                formFieldNameValues.Add(fieldName, field.Value);
                            }
                        }
                    
                    }

                }
	    
                if(formid== SCIDs.WebForms.GeneralInformationRequest || formid == SCIDs.WebForms.UniversalRequestForm)
                {
	        
                   CollectOptionalFieldToQuestionsAndComments(formid,formFieldNameValues);
	        
                }

                 Post(formFieldNameValues, url);
		
                HttpContext.Current.Items["WFFMPostedFormID"] = formid.ToString();
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error while submitting form: "+formid,e,this);
            }
        }

        private void CollectOptionalFieldToQuestionsAndComments(ID formid, NameValueCollection formFieldNameValues)
        {
            var allKeys = formFieldNameValues.AllKeys;


            var optionalInfo = new StringBuilder();
            optionalInfo.AppendLine("--Additional Info --");
            if (formid == SCIDs.WebForms.GeneralInformationRequest)
            {
                optionalInfo.AppendLine("Phone: " + formFieldNameValues["Phone"]);
                formFieldNameValues.Remove("Phone");
                optionalInfo.AppendLine("Ext: " + formFieldNameValues["Ext"]);
                formFieldNameValues.Remove("Ext");
                optionalInfo.AppendLine("Address: " + formFieldNameValues["Address"]);
                formFieldNameValues.Remove("Address");
                optionalInfo.AppendLine("City: " + formFieldNameValues["City"]);
                formFieldNameValues.Remove("City");
                optionalInfo.AppendLine("State: " + formFieldNameValues["State"]);
                formFieldNameValues.Remove("State");

            }

            if (formid == SCIDs.WebForms.UniversalRequestForm)
            {

                optionalInfo.AppendLine("Planned move in date " +
                    DateUtil.ParseDateTime(formFieldNameValues["Planned MoveIn Date"], default(DateTime)).ToShortDateString());
                formFieldNameValues.Remove("Planned MoveIn Date");

                optionalInfo.AppendLine("Phone: " + formFieldNameValues["Phone"]);
                formFieldNameValues.Remove("Phone");
                optionalInfo.AppendLine("Ext: " + formFieldNameValues["Ext"]);
                formFieldNameValues.Remove("Ext");
                optionalInfo.AppendLine("Address: " + formFieldNameValues["Street Address"]);
                formFieldNameValues.Remove("Street Address");
                optionalInfo.AppendLine("Suite Apt: " + formFieldNameValues["SuiteApartmentUnit"]);
                formFieldNameValues.Remove("SuiteApartmentUnit");
                optionalInfo.AppendLine("City: " + formFieldNameValues["City"]);
                formFieldNameValues.Remove("City");
                optionalInfo.AppendLine("State: " + formFieldNameValues["StateProvince"]);
                formFieldNameValues.Remove("StateProvince");
            }

                var hasQuestionsField = allKeys.Contains("txtMessageText");

                if (hasQuestionsField)
                {
                    formFieldNameValues["txtMessageText"] = string.Format("{0} {1} {2}", formFieldNameValues["txtMessageText"] , Environment.NewLine ,optionalInfo);
                }
                else
                {
                    formFieldNameValues.Add("txtMessageText", optionalInfo.ToString());
                }


        }



      

        private void Post(NameValueCollection formFieldNameValues, string url)
        {
           if (url.IsEmpty() || formFieldNameValues == null)
            {
                return;
            }

           var dataStr = new StringBuilder();
           var ampersand = string.Empty;
           foreach (string key in formFieldNameValues.AllKeys)
           {
               dataStr.Append(ampersand);
               dataStr.Append(HttpUtility.UrlEncode(key));
               dataStr.Append("=");
               dataStr.Append(HttpUtility.UrlEncode(formFieldNameValues[key]));
               ampersand = "&";
           }

            using (var client = new WebClient())
            {
                var cookies = HttpContext.Current.Request.Cookies;
                var cookiestring = new List<string>();
                foreach (string cookieKey in cookies)
                {
                    if (cookieKey.StartsWith("cu"))
                    {
                        var cookie = cookies[cookieKey];
                        if (cookie != null)
                        {

                            cookiestring.Add(cookieKey + "=" + cookie.Value);
                            if (cookieKey == "cuvid")
                            {
                                dataStr.AppendFormat("&cd_visitorkey={0}", HttpUtility.UrlEncode(cookie.Value));
                            }
                        }

                    }
                }

                var uri = HttpContext.Current.Request.Url;
                var referrer = uri.GetLeftPart(UriPartial.Authority);
                string postData = dataStr.ToString();
                byte[] bytes = Encoding.UTF8.GetBytes(postData);

                client.Headers.Add(HttpRequestHeader.Cookie, string.Join(";", cookiestring));
                client.Headers.Add(HttpRequestHeader.Referer,referrer);
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                client.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore);

                var a= client.UploadData(url, "POST", bytes);
               
            }
        }

        public void Submit(ID formid, AdaptedResultList fields)
        {
            Execute(formid, fields, fields);
        }


        public Item CurrentDivision
        {
            get
            {
                return _currentDivision ??
                       (_currentDivision =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, Sitecore.Context.Item.ID,
                                                             Sitecore.Context.Database.GetItem(
                                                                 Sitecore.Context.Site.StartPath)));
            }
        }

    }
}