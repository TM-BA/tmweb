﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Sites;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using ListItemCollection = Sitecore.Form.Web.UI.Controls.ListItemCollection;

namespace TM.Web.Custom.Forms
{
    public class WFFMStatesDropList : DropList
    {
        private Item _currentDivision;
        public Item CurrentDivision
        {
            get
            {
                return _currentDivision ??
                       (_currentDivision =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, Sitecore.Context.Item.ID,
                                                              Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath)));
            }
        }
        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }
        public Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        public Item CurrentHomeItem
        {
            get
            {
                return SCContextDB.GetItem(SCCurrentSite.StartPath);
            }
        }

        protected string SCCurrentSitePath
        {
            get { return SCCurrentSite.StartPath; }
        }

        protected string SCCurrentHomePath
        {
            get { return CurrentHomeItem.Paths.FullPath; }
        }

        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(BooleanField)), Localize]
        public string HideTitle
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value == "No"; }
        }


        protected override void InitItems(ListItemCollection items)
        {
            base.KeepHiddenValue = false;


            
           Item states = SCContextDB.GetItem(SCIDs.GlobalSharedFieldValues.States);

           if (states != null)
           {

               var divisions = states.GetChildren().ToList();
               

               List<ListItem> sortedList = new List<ListItem>();
               foreach (var division in divisions)
               {
                   var divisionIdFld = division.Fields["Code"];
                   var divisionNameFld = division.Fields["Name"];
                   if (divisionIdFld != null && divisionIdFld.Value.IsNotEmpty())
                   {

                       sortedList.Add(new ListItem((divisionIdFld.Value), divisionIdFld.Value));
                   }

               }

               sortedList = (sortedList.Distinct().OrderBy(x => x.Text)).ToList();
               foreach (ListItem item in sortedList)
               {
                   items.Add(item);
               }


               base.InitItems(items);
           }

        }

    }
}