﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using Sitecore.Web.UI.XslControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Linq;
using DateUtil = Sitecore.DateUtil;


namespace TM.Web.Custom.Forms
{
    public class FormSaveActionRedirect : ISaveAction, ISubmit
    {

        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                var redirectField = string.Empty;

                foreach (AdaptedControlResult field in fields)
                {
                    var fieldItem = new FieldItem(Context.Database.GetItem(field.FieldID));
                    var localizedFielditem = ExtensionsToString.Between(fieldItem.LocalizedParameters, "<FieldName>", "</FieldName>");

                    if (localizedFielditem.ToLower() == "areainterest")
                        redirectField = "areainterest_" + field.Value;

                }
                foreach (AdaptedControlResult field in fields)
                {
                    if (!string.IsNullOrEmpty(redirectField) && field.FieldName.ToLower() == redirectField.ToLower())
                        HttpContext.Current.Response.Redirect(field.Value);
                }

            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error while submitting form: " + formid, e, this);
            }
        }

        public void Submit(ID formid, AdaptedResultList fields)
        {
            Execute(formid, fields, fields);
        }




    }
}