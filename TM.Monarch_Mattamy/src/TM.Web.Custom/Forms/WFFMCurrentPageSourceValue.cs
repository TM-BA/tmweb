﻿using System;
using System.ComponentModel;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Net;
using System.Text;
using System.Web;

namespace TM.Web.Custom.Forms
{
    public class WFFMCurrentPageSourceValue : WFFMSingleLineTextBase
    {
      
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (CurrentCommunity != null && !string.IsNullOrWhiteSpace(CurrentCommunity["Lead Form Source Value"])) textbox.Text = CurrentCommunity["Lead Form Source Value"];
        }




        [VisualProperty("Default Value:", 100), DefaultValue(""), Localize]
        public override string Text
        {
            get
            {

                return textbox.Text;
            }
            set
            {
                if (CurrentCommunity != null && !string.IsNullOrWhiteSpace(CurrentCommunity["Lead Form Source Value"])) textbox.Text = CurrentCommunity["Lead Form Source Value"];
                else
                    textbox.Text = value; ;
            }
        }
    }
}