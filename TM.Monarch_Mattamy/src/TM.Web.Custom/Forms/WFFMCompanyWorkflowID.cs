﻿using System;
using System.ComponentModel;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Net;
using System.Text;
using System.Web;

namespace TM.Web.Custom.Forms
{
    public class WFFMCompanyWorkflowID : WFFMSingleLineTextBase
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            var wfid = GetWorkflowID();
            textbox.Text = string.IsNullOrWhiteSpace(wfid)? textbox.Text : wfid;
        }




        [VisualProperty("Default Value:", 100), DefaultValue(""), Localize]
        public override string Text
        {
            get { return textbox.Text; }
            set
            {
                var wfid = GetWorkflowID();
                textbox.Text = string.IsNullOrWhiteSpace(wfid) ? value : wfid;
            }
        }


        private string GetWorkflowID()
        {
            if (CurrentItem != null)
            {
                if (CurrentItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
                {
                    if (CurrentProductItem != null && !string.IsNullOrWhiteSpace(CurrentProductItem["Community WorkflowID"]))
                    {
                        return CurrentProductItem["Community WorkflowID"];
                    }

                    if (CurrentHomeItem != null && !string.IsNullOrWhiteSpace(CurrentHomeItem["Community WorkflowID"]))
                    {
                        return CurrentHomeItem["Community WorkflowID"];
                    }
                }
                if (CurrentItem.TemplateID == SCIDs.TemplateIds.DivisionPage || CurrentItem.TemplateID == SCIDs.TemplateIds.CityPage)
                {
                    if (CurrentProductItem != null && !string.IsNullOrWhiteSpace(CurrentProductItem["Division WorkflowID"] ))
                    {
                        return CurrentProductItem["Division WorkflowID"];
                    }

                    if (CurrentHomeItem != null && !string.IsNullOrWhiteSpace(CurrentHomeItem["Division WorkflowID"]))
                    {
                        return CurrentHomeItem["Division WorkflowID"];
                    }
                }
                else if (CurrentHomeItem != null)
                {
                    return CurrentHomeItem["Company WorkflowID"];

                }

            }
            return null;
        }
    }
}