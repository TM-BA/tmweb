﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.DynamicFields;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class CommunityQueries : QueryBase
    {

        string _closedCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.Closed);
        string _inactive = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.InActive, true);
        string _inactiveCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.InActive);
        private string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active,true);

        public CommunityQueries(string dbName):base(dbName)
        {
            
        }

         public CommunityQueries()
        {

        }

        public IEnumerable<TMSearchResultItem> GetAllActiveCommunitesUnderCity(ID cityItemID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allActiveCommunities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage)
                    .Where(
                        i =>
                           i[SCFieldNames.CommunityFields.CommunityStatus] != _inactiveCommunity
                            && i[SCFieldNames.CommunityFields.CommunityStatus] != _closedCommunity
                            && i[SCFieldNames.CommunityFields.StatusforSearch] ==_active
                            && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                            && i.Paths.Contains(cityItemID));
                return allActiveCommunities.ToList();
            }
        }

        public IEnumerable<ID> GetCommunitiesMatchingMasterPlanInDivision(ID divisionID, string masterPlanID)
        {
            var masterPlan = IdHelper.NormalizeGuid(masterPlanID, true);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allPlansMatchingMasterPlanID = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage)
                    .Where(
                        i => i[SCFieldNames.PlanPageFields.MasterPlanID] == masterPlan
                             && i[SCFieldNames.PlanPageFields.PlanStatus] ==_active
                             && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                             && i.Paths.Contains(divisionID));

                return
                    allPlansMatchingMasterPlanID.Select(c => c.ParentID).ToList()
                        .Distinct() ;
            }
        }


        public IEnumerable<TMSearchResultItem> GetAllSearchableHomesForSaleUnderCommunity(ID community)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allHFSUnderCommunity = context.GetQueryable<TMSearchResultItem>()
                    .Where(
                        i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                             && i["Home for Sale Status"] ==_active
                             && i["Status for Search"] ==_active
                             && i["Display on Homes for Sale Page"] == "1"
                             && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                             && i.Paths.Contains(community));

                return allHFSUnderCommunity.ToList();
            }
        }


        public Item GetCommunityFromLegacyId(string legacyId)
        {
            legacyId = IdHelper.NormalizeGuid(legacyId, true);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem community = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage
                                && i[SCFieldNames.CommunityFields.CommunityLegacyId] == legacyId
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return community == null ? null : community.GetItem();
            }
        }
    }
}