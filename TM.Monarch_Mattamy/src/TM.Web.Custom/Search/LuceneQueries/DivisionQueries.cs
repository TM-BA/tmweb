﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.DynamicFields;
using QueryBase = TM.Web.Custom.Queries.LuceneQueries.QueryBase;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class DivisionQueries : QueryBase
    {
        private static string _activeStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true);
        private static string _inactiveStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.InActive);

        public DivisionQueries(string dbName):base(dbName)
        {
            
        }

        public DivisionQueries()
        {

        }
        public Item GetDivisionFromLegacyId(string legacyId)
        {
            var normalizedLegacyID = IdHelper.NormalizeGuid(legacyId, true);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem division = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.DivisionPage
                                && i[SCFieldNames.DivisionFields.LegacyDivisionID] == normalizedLegacyID
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return division == null ? null : division.GetItem();
            }
        }

        public IEnumerable<Item> GetAllCommunitiesAvaialableForAssitance(string divisionID)
        {
            var division = ID.Parse(divisionID);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var communities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage
                                && i[SCFieldNames.CommunityFields.StatusforAssistance] ==_activeStatus
                                && i.Paths.Contains(division)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return communities.Select(c => c.GetItem()).ToList();
            }


        }

        public IEnumerable<ID> GetAllActiveDivisionUnder(ID stateID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var communities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage
                                && i[SCFieldNames.CommunityFields.CommunityStatus] !=_inactiveStatus
                                && i[SCFieldNames.CommunityFields.StatusforSearch] ==_activeStatus
                                && i.Paths.Contains(stateID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return communities.ToList().Select(c => c.GrandParentID).Distinct();
            }

        }


        public IEnumerable<Item> GetAllActiveStatesAndDivisionsAcrossAllSites()
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var communities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage
                                && i[SCFieldNames.CommunityFields.CommunityStatus] !=_inactiveStatus
                                && i[SCFieldNames.CommunityFields.StatusforSearch] ==_activeStatus
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                var activeDivisionIDs =
                    communities.Select(c => c.GrandParentID).ToList().Distinct();
                var activeDivisionItems = activeDivisionIDs.Select(d => Sitecore.Context.Database.GetItem(d));

                var activeStates = activeDivisionItems.Select(s => s.Parent);
                var activeDivisionsAndStates = new List<Item>(activeDivisionItems);

                activeDivisionsAndStates.AddRange(activeStates);
                return activeDivisionsAndStates;
            }

        }

    }

}