﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Enums;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class PlanQueries : QueryBase
    {
        private string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active,true);

        public PlanQueries(string dbName):base(dbName)
        {
            
        }

        public PlanQueries()
        {

        }

        public Item GetPlanFromLegacyId(string legacyId)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem plan = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage
                                && i[SCFieldNames.PlanPageFields.PlanLegacyID] == IdHelper.NormalizeGuid(legacyId,true)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return plan == null ? null : plan.GetItem();
            }
        }

        public IEnumerable<ID> GetHomesForSaleForRWEInDivision(ID divisionID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var homeForSaleForRWE = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.DisplayAsAvailableHomeInRWE] == "1"
                                && i[SCFieldNames.HomeForSaleFields.Active] ==_active
                                && i[SCFieldNames.HomeForSaleFields.ActiveForSearch] ==_active
                                && i.Paths.Contains(divisionID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (homeForSaleForRWE.TotalSearchResults == 0)
                {
                    homeForSaleForRWE = context.GetQueryable<TMSearchResultItem>()
                        .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                    && i[SCFieldNames.HomeForSaleFields.Active] ==_active
                                    &&
                                    i[SCFieldNames.HomeForSaleFields.ActiveForSearch] ==_active
                                    && i.Paths.Contains(divisionID)
                                    && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();
                }

                return homeForSaleForRWE.Hits.Select(h => h.Document.ItemId).ToList();
            }
        }

        public Item GetHomeForSaleFromLegacyId(string legacyId)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem homeForSale = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyId] == IdHelper.NormalizeGuid(legacyId,true)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return homeForSale == null ? null : homeForSale.GetItem();
            }
        }

        public HomeTourOption IsAvailableAsModelHome(Item plan, Item currentDivision, Item currentCommunity,
            out Item matchedInventory)
        {
            matchedInventory = null;
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var availableAsModelHome = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                &&
                                i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHome.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHome.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsModelHome;
                }

                return HomeTourOption.None;
            }
        }

        public HomeTourOption GetHomeTourOption(Item plan, Item currentDivision, Item currentCommunity,
            out Item nearByCommunity, out Item matchedInventory)
        {
            nearByCommunity = null;
            matchedInventory = null;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var availableAsModelHome = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] ==IdHelper.NormalizeGuid( plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHome.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHome.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsModelHome;
                }

                var communitiesWithin15Miles = SCUtils.CommunityWithinDistanceOf(currentCommunity, 15);

                communitiesWithin15Miles.Remove(currentCommunity.ID.ToString());

                var availableAsModelHomeInNearbyCommunity = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] ==IdHelper.NormalizeGuid( plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHomeInNearbyCommunity.TotalSearchResults > 0)
                {
                    var results = availableAsModelHomeInNearbyCommunity.Hits;
                    if (
                        results.Any(
                            r => r.Document.Paths.Any(id => communitiesWithin15Miles.Any(cid => IdHelper.NormalizeGuid(cid, true) == IdHelper.NormalizeGuid(id)))))
                    {
                        var modelHome = availableAsModelHomeInNearbyCommunity.Hits.FirstOrDefault();
                        matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                        nearByCommunity = matchedInventory == null ? null : matchedInventory.Parent.Parent;
                        return HomeTourOption.AvailableAsModelHomeInNearByCommunity;
                    }
                }

                var homeReadyNow = IdHelper.NormalizeGuid(SCIDs.HomesForSalesFields.ReadyNow);
                var availableAsInventory = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                &&
                                i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                &&
                                i[SCFieldNames.HomeForSaleFields.StatusforAvailability] == homeReadyNow
                                
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsInventory.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHomeInNearbyCommunity.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsInventory;
                }
            }


            return HomeTourOption.None;
        }


        public IEnumerable<TMSearchResultItem> GetPlansInCommunityMatchingMasterPlan(ID communityID, string masterPlanID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var plansInCommunityMatchingMasterPlan = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(masterPlanID,true)
                                && i[SCFieldNames.PlanPageFields.PlanStatus] ==_active
                                && i.Paths.Contains(communityID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return plansInCommunityMatchingMasterPlan.ToList();
            }
        }


        public IEnumerable<TMSearchResultItem> GetActiveHomesForSaleUnderPlan(ID plan)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var homeForSale = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.Active] ==_active
                                && i[SCFieldNames.HomeForSaleFields.ActiveForSearch] ==_active
                                && i.Paths.Contains(plan)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return homeForSale.ToList();
            }
        }
    }
}