﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.ContentSearch;
namespace TM.Web.Custom.Search.DynamicFields
{
    public class LuceneParentIDField : IComputedIndexField
    {
        private string GetParent(Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            var parent = item.Parent;
            if (parent != null)
                return parent.ID.ToShortID().ToString();
            return null;
        }

        public object ComputeFieldValue(IIndexable indexable)
        {
            var indexItem = indexable as SitecoreIndexableItem;
            Assert.ArgumentNotNull(indexItem, "indexItem");
            var item = indexItem.Item;
            return GetParent(item);
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}