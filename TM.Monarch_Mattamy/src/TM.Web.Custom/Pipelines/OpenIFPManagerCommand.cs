﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Sites;
using Sitecore.Web.UI.Sheer;
using TM.Utils;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Pipelines
{
         /// <summary>
	/// Represents the OpenIFPManagerCommand command.
        /// 
        /// </summary>
        [Serializable]
        public class OpenIFPManagerCommand : Command
        {
            /// <summary>
            /// Executes the command in the specified context.
            /// 
            /// </summary>
            /// <param name="context">The context.</param>
            public override void Execute(CommandContext context)
            {
                var item = context.Items[0];
                var itemID = context.Items[0].ID.ToShortID().ToString();
                var user = Sitecore.Context.User.GetLocalName();
                var ifpbuilderid = SCUtils.GetIFPBuilderID(item);
                var token = CryptographyHelpers.EncryptString(user+":"+DateTime.Now.AddMinutes(30).ToUniversalTime().Ticks, "IFP_ROCKS!");

                var ifpUrl = item.Fields["IFP URL"].GetLinkFieldPath();

                if (!string.IsNullOrEmpty(ifpUrl))
                {
                    var ifpUri = new Uri(ifpUrl);
                    var queryParams = HttpUtility.ParseQueryString(ifpUri.Query);
                    if (queryParams.Count > 0)
                    {
                        var pid = queryParams["pid"];
                        token = HttpUtility.UrlEncode(token);
                        var webSiteUrl =
                            "http://ifp.taylormorrison.com/manager/TMAuthenticationProxy.ashx?pid=" + pid + "&token=" +
                            token + "&planid=" + itemID + "&bid=" + ifpbuilderid;
                        SheerResponse.Eval("window.open('" + webSiteUrl + "', '_blank')");
                    }
                    else
                    {
                        SheerResponse.Alert("IFP URL does not have product id info. Cannot open IFP manager", true);
                    }


                }
                else
                {
                    SheerResponse.Alert("IFP URL is not available. Cannot open IFP manager", true);
                }

            }

             

               /// <summary>
            /// Queries the state of the command.
            /// 
            /// </summary>
            /// <param name="context">The context.</param>
            /// <returns>
            /// The state of the command.
            /// </returns>
            public override CommandState QueryState(CommandContext context)
            {
                return CommandState.Enabled;
            }

         
        }
    
}