﻿using System.Web.UI;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using Assert = Sitecore.Diagnostics.Assert;
using SC = Sitecore;

namespace TM.Web.Custom.Pipelines
{
  

    public class EditorFormatter :
      SC.Shell.Applications.ContentEditor.EditorFormatter
    {
        public override void RenderField(
          Control parent,
          SC.Shell.Applications.ContentManager.Editor.Field field,
          bool readOnly)
        {
            base.RenderField(parent, field, readOnly || this.ShouldMakeReadOnly(field));
        }

        public new void RenderField(
          Control parent,
          SC.Shell.Applications.ContentManager.Editor.Field field,
          SC.Data.Items.Item fieldType,
          bool readOnly)
        {
            base.RenderField(
              parent,
              field,
              fieldType,
              readOnly || this.ShouldMakeReadOnly(field));
        }

        public override void RenderField(
          Control parent,
          SC.Shell.Applications.ContentManager.Editor.Field field,
          SC.Data.Items.Item fieldType,
          bool readOnly,
          string value)
        {
            base.RenderField(
              parent,
              field,
              fieldType,
              readOnly || this.ShouldMakeReadOnly(field),
              value);
        }

        private bool ShouldMakeReadOnly(
          SC.Shell.Applications.ContentManager.Editor.Field field)
        {
            Assert.ArgumentNotNull(field, "field");
            Assert.IsNotNull(
              SC.Context.ContentDatabase,
              "Sitecore.Context.ContentDatabase");
            var fieldItem = SC.Context.ContentDatabase.GetItem(field.ItemField.ID);
            var fieldTemplate = SC.Context.ContentDatabase.GetItem(field.TemplateField.ID);
            Assert.IsNotNull(fieldItem, "fieldItem " + field.TemplateField.ID);
            if (Arguments.Item == null) return false;
            return IsLegacyIDField(fieldItem) ||
                   new MasterPlanReadOnlyFieldsSetter(fieldItem, fieldTemplate, this.Arguments).IsFieldReadOnly();
        }

        private bool IsLegacyIDField(Item fieldItem)
        {
            return false;
            switch (fieldItem.Name.ToLowerInvariant())
            {
                case "community legacy id":
                case "plan legacy id":
                case "home for sale legacy id":
                case "legacyid":
                case "legacy division id":
                case "school legacy id":
                case "home features legacyid":
                case "local interests legacy id":
                    return true;
                default:
                    return false;
            }
        }
    }
}