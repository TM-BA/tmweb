﻿cd %1
pause 

for /f "delims=" %%a in ('dir "*.png" /b /s /a-d') do (
echo processing "%%a"
"D:\projects\EX2\TM\trunk\tools\optipng.exe" -nc -nb -o7 -full %%a
)
 
for /f "delims=" %%a in ('dir "*.jpg" /b /s /a-d') do (
echo processing "%%a"
"D:\projects\EX2\TM\trunk\tools\jpegtran.exe" -optimize  -copy none -outfile "%%a.tmp"  "%%a"
move /Y "%%a.tmp" "%%a" >nul
)
pause