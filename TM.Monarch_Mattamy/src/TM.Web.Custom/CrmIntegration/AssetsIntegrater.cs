﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.CrmIntegration
{
    public class AssetsIntegrater
    {
        private readonly Database _sourceDB;
        private bool _isCommunity;
        private bool _isPlan;
        private bool _isSpec;
        private string _legacyID;
        public AssetsIntegrater(Database sourceDB, Database targetDB)
        {
            _sourceDB = sourceDB;
        }

        public void Integrate(Item item,string legacyID)
        {
            var path = item.Paths.FullPath.ToLowerInvariant();
            var homePath = path.Substring(0, path.IndexOf("/home/", System.StringComparison.Ordinal) + 5);
            var homeItem = _sourceDB.GetItem(homePath);
            _legacyID = legacyID;
            Host = homeItem["Company Website URL"];

            if (item.Name != "__Standard Values")
            {
                if (item.TemplateID.ToString() == CommunityItem.TemplateId)
                {
                    _isCommunity = true;

                    IntegrateCommunity(item);
                }
                if (item.TemplateID.ToString() == PlanItem.TemplateId)
                {
                    _isPlan = true;
                    IntegratePlan(item);
                }
                if (item.TemplateID.ToString() == HomeForSaleItem.TemplateId)
                {
                    _isSpec = true;
                    IntegrateSpec(item);
                }
            }
        }

        protected string Host { get; set; }

        private void IntegrateSpec(Item item)
        {
            var hfsItem = new HomeForSaleItem(item);
            var hfsLegacyID = _legacyID.CastAs<int>();
            var exteriorImages = hfsItem.PlanBase.ElevationImages.ListItems.Select(l => (MediaItem) l);
            var interiorImages = hfsItem.PlanBase.InteriorImages.ListItems.Select(l => (MediaItem) l);
            var floorPlanImages = hfsItem.PlanBase.FloorPlanImages.ListItems.Select(l => (MediaItem) l);
            var allValidAssets = new List<KeyValuePair<int, string>>();
            MediaItem brochure = null;
            VirtualTourItem virtualTourItem = null;
            if (hfsItem.PlanBaseAdditional.VirtualTourStatus.Raw == SCIDs.StatusIds.Status.Active)
            {
                var virtualTourID = hfsItem.PlanBaseAdditional.VirtualToursMasterPlanID.Raw;
                if (ID.IsID(virtualTourID))
                {
                    virtualTourItem = new VirtualTourItem(_sourceDB.GetItem(virtualTourID));
                }
            }
            if (hfsItem.PlanBaseAdditional.PlanBrochureStatus.Raw == SCIDs.StatusIds.Status.Active)
            {
                var brochureID = hfsItem.PlanBaseAdditional.PlanBrochureMasterID.Raw;
                if (ID.IsID(brochureID))
                {
                    brochure = _sourceDB.GetItem(brochureID);
                }
            }

            using (var db = new CMSIntegrationEntities())
            {
                AddAssetsFromMediaItems(hfsLegacyID, db, exteriorImages, CmsCrmAssetTypeID.Elevationphoto,
                                        allValidAssets);
                AddAssetsFromMediaItems(hfsLegacyID, db, interiorImages, CmsCrmAssetTypeID.Interiorphoto,
                                        allValidAssets);
                AddAssetsFromMediaItems(hfsLegacyID, db, floorPlanImages, CmsCrmAssetTypeID.Floorplan,
                                        allValidAssets);
                if (virtualTourItem != null)
                {
                    AddAssetsFromMediaItems(hfsLegacyID, db, virtualTourItem.VirtualTourURL.Url,
                                            altText: virtualTourItem.InternalNote.Text,
                                            assetTypeID: CmsCrmAssetTypeID.Virtualtour,
                                            allValidAssets: allValidAssets);
                }

                if (brochure != null)
                {
                    AddAssetsFromMediaItems(hfsLegacyID, db, brochure, CmsCrmAssetTypeID.Brochure,
                                            allValidAssets);
                }


                db.SaveChanges();

             
               
            }

            RemoveAllValidAssetsAndDeleteRest(allValidAssets, hfsLegacyID);
        }


        private void IntegratePlan(Item item)
        {
            var planItem = new PlanItem(item);
            var planLegacyID = _legacyID.CastAs<int>();
            var allValidAssets = new  List<KeyValuePair<int, string>>();
            VirtualTourItem virtualTourItem = null;
            if (planItem.PlanBaseAdditional.VirtualTourStatus.Raw == SCIDs.StatusIds.Status.Active)
            {
                var virtualTourID = planItem.PlanBaseAdditional.VirtualToursMasterPlanID.Raw;
                if (ID.IsID(virtualTourID))
                {
                    virtualTourItem = new VirtualTourItem(_sourceDB.GetItem(virtualTourID));
                }
            }

            MediaItem brochure = null;
            if (planItem.PlanBaseAdditional.PlanBrochureStatus.Raw == SCIDs.StatusIds.Status.Active)
            {
                var brochureID = planItem.PlanBaseAdditional.PlanBrochureStatus.Raw;
                if (ID.IsID(brochureID))
                {
                    brochure = _sourceDB.GetItem(brochureID);
                }
            }


            var elevationImages = planItem.PlanBase.ElevationImages.ListItems.Select(l => (MediaItem) l);
            var interiorImages = planItem.PlanBase.InteriorImages.ListItems.Select(l => (MediaItem) l);
            var floorPlanImages = planItem.PlanBase.FloorPlanImages.ListItems.Select(l => (MediaItem) l);
            using (var db = new CMSIntegrationEntities())
            {
                AddAssetsFromMediaItems(planLegacyID, db, elevationImages, CmsCrmAssetTypeID.Elevationrendering,
                                        allValidAssets);
                AddAssetsFromMediaItems(planLegacyID, db, interiorImages, CmsCrmAssetTypeID.Interiorphoto,
                                        allValidAssets);
                AddAssetsFromMediaItems(planLegacyID, db, floorPlanImages, CmsCrmAssetTypeID.Floorplan,
                                        allValidAssets);
                if (brochure != null)
                {
                    AddAssetsFromMediaItems(planLegacyID, db, brochure, CmsCrmAssetTypeID.Brochure,
                                            allValidAssets);
                }

                if (virtualTourItem != null)
                {
                    AddAssetsFromMediaItems(planLegacyID, db, virtualTourItem.VirtualTourURL.Url,
                                            altText: virtualTourItem.InternalNote.Text,
                                            assetTypeID: CmsCrmAssetTypeID.Virtualtour,
                                            allValidAssets: allValidAssets);
                }

                db.SaveChanges();

                
            }
            RemoveAllValidAssetsAndDeleteRest(allValidAssets, planLegacyID);
        }

        private void IntegrateCommunity(Item item)
        {
            var communityItem = new CommunityItem(item);
            var allValidAssets = new List<KeyValuePair <int, string>>();
            var communityLegacyID = _legacyID.CastAs<int>();
            var slideShowImages = communityItem.SlideShowImage.ListItems.Select(l => (MediaItem) l).ToList();

            //slideshow image types
            using (var db = new CMSIntegrationEntities())
            {
                AddAssetsFromMediaItems(communityLegacyID, db, slideShowImages.FirstOrDefault(),
                                        CmsCrmAssetTypeID.Communityfeaturedimage, allValidAssets);
                
                AddAssetsFromMediaItems(communityLegacyID, db, slideShowImages, CmsCrmAssetTypeID.Communityphoto,
                                        allValidAssets);

                AddAssetsFromMediaItems(communityLegacyID, db, slideShowImages, CmsCrmAssetTypeID.NewCommunityPhotoLarge,
                                              allValidAssets);

                foreach (var slideShowImage in slideShowImages)
                {
                    var directUrl = slideShowImage.GetMediaUrl() + "?thn=1";

                    AddAssetsFromMediaItems(communityLegacyID, db, directUrl,
                                            CmsCrmAssetTypeID.NewCommunityPhotoSmall,
                                            allValidAssets);

                }


                AddAssetsFromMediaItems(communityLegacyID, db, communityItem.CommunityLogo.Field,
                                        CmsCrmAssetTypeID.Communitylogo, allValidAssets);
                AddAssetsFromMediaItems(communityLegacyID, db, communityItem.CommunityBrochure.MediaItem,
                                        CmsCrmAssetTypeID.Communitybrochure, allValidAssets);
                AddAssetsFromMediaItems(communityLegacyID, db, communityItem.ConstructionUpdates.MediaItem,
                                        CmsCrmAssetTypeID.ConstructionUpdateFile, allValidAssets);

                var homeFeaturesImages = communityItem.HomeFeaturesImages.ListItems.Select(l => (MediaItem) l);
                var localInterestsImages = communityItem.LocalInterestImages.ListItems.Select(l => (MediaItem) l);
                
                AddAssetsFromMediaItems(communityLegacyID, db, communityItem.SitePlanImage,
                                        CmsCrmAssetTypeID.Communitysiteplan, allValidAssets);
                AddAssetsFromMediaItems(communityLegacyID, db, homeFeaturesImages,
                                        CmsCrmAssetTypeID.HomeFeatureImagePool, allValidAssets);
                AddAssetsFromMediaItems(communityLegacyID, db, localInterestsImages,
                                        CmsCrmAssetTypeID.LocalInterestImagePool, allValidAssets);


                //school images
                var schoolFolders = communityItem.InnerItem.GetChildrenByTemplate(SCIDs.TemplateIds.SchoolsFolder);
                if (schoolFolders.Count > 0)
                {
                    var schoolFolder = schoolFolders[0];
                    var schools = schoolFolder.Children;
                    foreach (Item school in schools)
                    {
                        AddAssetsFromMediaItems(communityLegacyID, db, school.Fields[SCIDs.School.SchoolImage],
                                                CmsCrmAssetTypeID.SchoolImagePool,
                                                allValidAssets);
                    }
                }

                //teaser images

                List<Item> promoFolders = communityItem.InnerItem.GetChildrenByTemplate(SCIDs.TemplateIds.PromoFolder);
                if (promoFolders.Count > 0)
                {
                    var promoFolder = promoFolders[0];
                    var promos = promoFolder.Children;

                    foreach (Item promo in promos)
                    {
                        AddAssetsFromMediaItems(communityLegacyID, db, promo.Fields[SCIDs.PromoBase.TeaserImage],
                                                CmsCrmAssetTypeID.IncentivePhoto, allValidAssets);
                    }
                }


                db.SaveChanges();

		

            }
            RemoveAllValidAssetsAndDeleteRest(allValidAssets, communityLegacyID);
        }

        private void RemoveAllValidAssetsAndDeleteRest(List<KeyValuePair<int, string>> allValidAssets, int legacyID)
        {
            using (var db = new CMSIntegrationEntities())
            {
                List<TW_ContentAssets> allAssets = null;
                if (_isCommunity)
                {
                    allAssets = db.TW_ContentAssets.Where(
                        t => t.CommunityID == legacyID & t.CommunityPlanID == null & t.CommunitySpecID == null).ToList();
                }
                else if (_isPlan)
                {
                    allAssets = db.TW_ContentAssets.Where(
                        t => t.CommunityPlanID == legacyID & t.CommunitySpecID == null).ToList();
                }
                else if (_isSpec)
                {
                    allAssets = db.TW_ContentAssets.Where(
                        t => t.CommunitySpecID == legacyID).ToList();
                }

                if (allAssets != null)
                {
                    //remove all valid assets leaving us assets that need to be deleted.
                    var countOfAssetsToBeDeleted =
                        allAssets.RemoveAll(a => allValidAssets.Any(asset => asset.Key ==  a.AssetTypeID && asset.Value == a.DirectURL));

                    foreach (var asset in allAssets)
                    {
                        db.TW_ContentAssets.DeleteObject(asset);
                    }

                    if (allAssets.Count > 0)
                        db.SaveChanges();
                }
            }


        }

        private void AddAssetsFromMediaItems(int legacyID, CMSIntegrationEntities db,
                                             IEnumerable<MediaItem> mediaItems, CmsCrmAssetTypeID assetTypeID,
                                             List<KeyValuePair<int, string>> allValidAssets)
        {
            foreach (var image in mediaItems)
            {
                AddAssetsFromMediaItems(legacyID, db, image, assetTypeID, allValidAssets);
            }
        }


        private void AddAssetsFromMediaItems(int legacyID, CMSIntegrationEntities db,
                                             ImageField mediaField, CmsCrmAssetTypeID assetTypeID,
                                            List<KeyValuePair<int, string>> allValidAssets)
        {
            if (mediaField == null || mediaField.MediaItem.GetMediaUrl().IsEmpty()) return;

            var directUrl = Host + mediaField.MediaItem.GetMediaUrl();
            var assetTypeIDForDB = assetTypeID.CastAs<int>();
            allValidAssets.Add(new KeyValuePair<int, string>(assetTypeIDForDB, directUrl));
            var twAssets = new TW_ContentAssets();
            EntityState entityState = EntityState.Modified;
            if (_isCommunity)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t => t.CommunityID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isPlan)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunityPlanID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityPlanID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isSpec)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunitySpecID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunitySpecID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }

           

            twAssets.AssetTypeID = assetTypeIDForDB;        
            var caption = string.IsNullOrEmpty(mediaField.Alt) ? ((MediaItem)mediaField.MediaItem).Alt : mediaField.Alt;
            var oldCaption = twAssets.Caption;
            twAssets.Caption = caption;
            twAssets.DirectURL = directUrl;
            twAssets.ThumbnailUrl = twAssets.DirectURL + "?thn=1";

            if (entityState == EntityState.Added)
            {
                db.TW_ContentAssets.AddObject(twAssets);
            }
            else if (oldCaption == caption)
            {
                db.ObjectStateManager.ChangeObjectState(twAssets, EntityState.Unchanged);
            }
          
        }


        private void AddAssetsFromMediaItems(int legacyID, CMSIntegrationEntities db,
                                             MediaItem mediaItem, CmsCrmAssetTypeID assetTypeID, List<KeyValuePair<int, string>> allValidAssets)
        {
            if (mediaItem == null || mediaItem.GetMediaUrl().IsEmpty()) return;
            var assetTypeIDForDB = assetTypeID.CastAs<int>();
            var directUrl = Host + mediaItem.GetMediaUrl();
            allValidAssets.Add(new KeyValuePair<int, string>(assetTypeIDForDB, directUrl));
            var twAssets = new TW_ContentAssets();
            EntityState entityState = EntityState.Modified;
            if (_isCommunity)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t => t.CommunityID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isPlan)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunityPlanID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityPlanID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isSpec)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunitySpecID == legacyID & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrl);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunitySpecID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
        

            twAssets.AssetTypeID = assetTypeID.CastAs<int>();
            var oldCaption = twAssets.Caption;
            twAssets.Caption = mediaItem.Alt;

            twAssets.DirectURL = directUrl;
            twAssets.ThumbnailUrl = twAssets.DirectURL + "?thn=1";

            if (entityState == EntityState.Added)
            {
                db.TW_ContentAssets.AddObject(twAssets);
            }
            else if (oldCaption == mediaItem.Alt)
            {
                db.ObjectStateManager.ChangeObjectState(twAssets, EntityState.Unchanged);
            }
       
        }


        private void AddAssetsFromMediaItems(int legacyID, CMSIntegrationEntities db, string directUrl,
                                             CmsCrmAssetTypeID assetTypeID, List<KeyValuePair<int, string>> allValidAssets,
                                             string thumbnailUrl = null, string altText = null)
        {
            var assetTypeIDForDB = assetTypeID.CastAs<int>();
            var twAssets = new TW_ContentAssets();
            EntityState entityState = EntityState.Modified;
            var directUrlForDB = Host + directUrl;
            allValidAssets.Add(new KeyValuePair<int, string>(assetTypeIDForDB, directUrl));
            if (_isCommunity)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunityID == legacyID & t.CommunityPlanID == null &&
                        t.CommunitySpecID == null & t.AssetTypeID == assetTypeIDForDB & t.DirectURL == directUrlForDB);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isPlan)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunityPlanID == legacyID & t.AssetTypeID == assetTypeIDForDB &
                        t.DirectURL == directUrlForDB);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunityPlanID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }
            if (_isSpec)
            {
                var existingtwAsset =
                    db.TW_ContentAssets.FirstOrDefault(
                        t =>
                        t.CommunitySpecID == legacyID & t.AssetTypeID == assetTypeIDForDB &
                        t.DirectURL == directUrlForDB);
                if (existingtwAsset == null)
                {
                    entityState = EntityState.Added;
                    twAssets.CommunitySpecID = legacyID;
                }
                else
                    twAssets = existingtwAsset;
            }

         
            twAssets.AssetTypeID = assetTypeID.CastAs<int>();
            var oldCaption = twAssets.Caption;
            twAssets.Caption = altText;
            twAssets.DirectURL = directUrlForDB;
            twAssets.ThumbnailUrl = thumbnailUrl;

            if (entityState == EntityState.Added)
            {
                db.TW_ContentAssets.AddObject(twAssets);
            }
            else if (oldCaption != altText)
            {
                db.ObjectStateManager.ChangeObjectState(twAssets, EntityState.Unchanged);
            }
           
        }
    }
}