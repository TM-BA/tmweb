﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Entities.DataTemplate.Common;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.CrmIntegration
{
    public class DivisionIntegrater
    {
        private readonly Database _sourceDB;
        private readonly Database _targetDB;

        public DivisionIntegrater(Database sourceDB, Database targetDB)
        {
            _sourceDB = sourceDB;
            _targetDB = targetDB;
        }

        public void Integrate(Item division)
        {
            var currentDivision = new DivisionPageItem(division);
            var state = new StatePageItem(division.Parent);
            var company = new CompanyItem(state.InnerItem.Parent.Parent);
            using (var db = new CMSIntegrationEntities())
            {
                var divisionLegacyID = currentDivision.LegacyDivisionID.Text.CastAs<int>(0);
                //This code seems to be duplicated below
                //var divisionParamExist = from c in db.CRM_DivisionParams
                //                         where c.DivisionID == divisionLegacyID
                //                         select c;

                //var divisionParam = divisionParamExist.FirstOrDefault() ?? new CRM_DivisionParams();
                //divisionParam.IDCode = "NSS_REG_CODE";

                //var stateLookup = new LookupItem(_sourceDB.GetItem(state.StateName.Raw));
                //if (string.IsNullOrWhiteSpace(currentDivision.NSS_DB.Value))
                //{
                //    divisionParam.SValue = stateLookup.Code;
                //}
                //else
                //{
                //    divisionParam.SValue = currentDivision.NSS_DB.Value;
                //}

                var divisionExists = from c in db.TW_Divisions
                                     where c.DivisionID == divisionLegacyID
                                     select c;


                var existingDivision = divisionExists.FirstOrDefault();
                var twdivision = existingDivision ?? new TW_Divisions();
                if (string.IsNullOrWhiteSpace(currentDivision.CRMDivisionName))
                {
                    twdivision.Name = currentDivision.DivisionName;
                }
                else
                {
                    twdivision.Name = currentDivision.CRMDivisionName;
                }
                twdivision.IsActive = currentDivision.StatusforSearch.Raw == SCIDs.StatusIds.Status.Active;
                twdivision.IsUSDivision = company.InnerItem.ParentID != SCIDs.CompanyIds.MonarchGroup;
                twdivision.IsBuildHighRise = state.InnerItem.Parent.Name.ToLowerInvariant().Contains("condos");
                twdivision.IsBuildLowRise = !twdivision.IsBuildHighRise;
                twdivision.CommissionAmount = currentDivision.RealtorCommissionRate;
                twdivision.ContractorNumber = currentDivision.CompanyContractorNumber.Text;


            

                if (existingDivision == null)
                {
                    db.TW_Divisions.AddObject(twdivision);
                   
                }

                db.SaveChanges();

                var divAddressExists = from c in db.TW_OrgUnitAddresses
                                       where
                                           ((c.DivisionID == divisionLegacyID) &&
                                            (c.CommunityID == 0 || c.CommunityID == null))
                                       select c;
                var existingDivAddress = divAddressExists.FirstOrDefault();
                var twOrgUnitAddress = existingDivAddress ?? new TW_OrgUnitAddresses();
                
                //***
                divisionLegacyID = twdivision.DivisionID;
                twOrgUnitAddress.DivisionID = divisionLegacyID;
                twOrgUnitAddress.AddressTypeID = 1;
                twOrgUnitAddress.BrandID = company.LegacyCompanyID.Text.CastAs<int?>();
                twOrgUnitAddress.DivisionID = divisionLegacyID;
                twOrgUnitAddress.StreetAddress =
                    (currentDivision.StreetAddress1 + " " + currentDivision.StreetAddress2).Left(100);
                twOrgUnitAddress.City = currentDivision.City;

                var stateLookup = new LookupItem(_sourceDB.GetItem(state.StateName.Raw));
                twOrgUnitAddress.State = stateLookup.Code;
                twOrgUnitAddress.Zip = currentDivision.ZiporPostalCode;

                var divAPhoneExists = from c in db.TW_OrgUnitPhones
                                       where
                                           ((c.DivisionID == divisionLegacyID) &&
                                            (c.CommunityID == 0 || c.CommunityID == null))
                                       select c;
                var existingDivisionPhone = divAPhoneExists.FirstOrDefault();
                var twDivisionPhone= existingDivisionPhone ?? new TW_OrgUnitPhones();

                //***
                twDivisionPhone.DivisionID = divisionLegacyID;

                twDivisionPhone.PhoneTypeID = 1;
                twDivisionPhone.BrandID = company.LegacyCompanyID.Text.CastAs<int?>();
                twDivisionPhone.DivisionID = divisionLegacyID;
                twDivisionPhone.PhoneNumber = currentDivision.Phone;

                var divisionParamsExists = from c in db.CRM_DivisionParams
                                           where c.DivisionID == divisionLegacyID
                                           select c;
                var existingDivisionParams = divisionParamsExists.FirstOrDefault();

                var crmDivisionParams = existingDivisionParams ?? new CRM_DivisionParams();

                crmDivisionParams.DivisionID = divisionLegacyID;
                crmDivisionParams.IDCode = "NSS_REG_CODE";
                if (string.IsNullOrWhiteSpace(currentDivision.NSS_DB.Value))
                {
                    crmDivisionParams.SValue = stateLookup.Code;
                }
                else
                {
                    crmDivisionParams.SValue = currentDivision.NSS_DB.Value;
                }
                if (existingDivAddress == null)
                {
                    db.TW_OrgUnitAddresses.AddObject(twOrgUnitAddress);
                }
                if (existingDivisionPhone == null)
                {
                    db.TW_OrgUnitPhones.AddObject(twDivisionPhone);
                }

                if (existingDivisionParams == null)
                {
                    db.CRM_DivisionParams.AddObject(crmDivisionParams); // <-- Should this be divisionParam?
                }

                db.SaveChanges();

                if (existingDivision == null)
                {
                    var newcommunityLegacyID = twdivision.DivisionID;
                    using (new EventDisabler())
                    using (new EditContext(division))
                    {
                        var legacyID = currentDivision.LegacyDivisionID.Field.InnerField.ID;
                        division.Fields[legacyID].Value =
                            newcommunityLegacyID.ToString(CultureInfo.InvariantCulture);
                    }
                }
            }
        }

        public void ProcessDSOItems(Item sourceItem)
        {
            var division = new DivisionPageItem(sourceItem.Parent.Parent);
            if(sourceItem.TemplateID == new ID(HomeFeaturesCategoryItem.TemplateId))
            {
                var homeFeatureCategory = new HomeFeaturesCategoryItem(sourceItem);
                var twHomeFeatureCategory = new TW_HomeFeatureCategories
                                                {
                                                    Name = homeFeatureCategory.Name,
                                                    DivisionID = division.LegacyDivisionID.Text.CastAs<int>()
                                                };
                using (var db = new CMSIntegrationEntities())
                {
                    var existingCode = homeFeatureCategory.Code.Text.CastAs<int>(0);
                    var existing = existingCode==0?null:
                        db.TW_HomeFeatureCategories.FirstOrDefault(c => c.DivisionID == twHomeFeatureCategory.DivisionID && c.CategoryID == existingCode);
		    if(existing==null)
		    {
                     db.AddToTW_HomeFeatureCategories(twHomeFeatureCategory);
                   
		    }
		    else
		    {
		        existing.Name = twHomeFeatureCategory.Name;
		        twHomeFeatureCategory.CategoryID = existing.CategoryID;
		    }
		      db.SaveChanges();
                  
                }

                    using (new EventDisabler())
                    using (new EditContext(sourceItem))
		{
		    var categoryID = twHomeFeatureCategory.CategoryID;
		    var codeFieldID = homeFeatureCategory.Code.Field.InnerField.ID;
		    sourceItem[codeFieldID] = categoryID.CastAs<string>();
		}

            }

	    if(sourceItem.TemplateID == new ID(LocalInterestCategoryItem.TemplateId))
	    {
	        var localFeatureCategory = new LocalInterestCategoryItem(sourceItem);
	        var twLocalFeatureCategory = new TW_LocalInterestCategories
	                                         {
	                                             DivisionID = division.LegacyDivisionID.Text.CastAs<int>(),
	                                             Name = localFeatureCategory.Name
	                                         };
	        using (var db = new CMSIntegrationEntities())
	        {
	            var existingCode = localFeatureCategory.Code.Text.CastAs<int>(0);
                    var existing = existingCode==0?null:
                        db.TW_LocalInterestCategories.FirstOrDefault(c => c.DivisionID == twLocalFeatureCategory.DivisionID && c.CategoryID == existingCode);
                  if (existing == null)
                  {
                      db.AddToTW_LocalInterestCategories(twLocalFeatureCategory);
                    
                  }
                  else
                  {
                      twLocalFeatureCategory.CategoryID = existing.CategoryID;
                      existing.Name = twLocalFeatureCategory.Name;
                  }
		  db.SaveChanges();

                }

                    using (new EventDisabler())
		using(new EditContext(sourceItem))
		{
		    var categoryID = twLocalFeatureCategory.CategoryID;
		    var codeFieldID = localFeatureCategory.Code.Field.InnerField.ID;
		    sourceItem[codeFieldID] = categoryID.CastAs<string>();
		}
	    }
        }
    }
}