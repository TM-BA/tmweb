﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Entities.DataTemplate.Common;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.CrmIntegration
{
    public class CommunityIntegrater
    {
        private readonly Database _sourceDB;
        private readonly Database _targetDB;

        public CommunityIntegrater(Database sourceDB, Database targetDB)
        {
            _sourceDB = sourceDB;
            _targetDB = targetDB;
        }

        public string Integrate(Item sourceItem)
        {
            var currentCommunity = new CommunityItem(sourceItem);
            var communutyLegacyID = string.Empty;
            using (var db = new CMSIntegrationEntities())
            {
                var communityLegacyID = currentCommunity.CommunityLegacyID.Raw.CastAs<int>(0);
                var cityItem = sourceItem.Parent;
                var division = cityItem.Parent;
                var state = division.Parent;
                var company = state.Parent.Parent;
                var city = new CityPageItem(cityItem);

                var communityExists = from c in db.TW_Communities
                                      where c.CommunityID == communityLegacyID
                                      select c;
                var existingCommunity = communityExists.FirstOrDefault();
                var twCommunity = existingCommunity ?? new TW_Communities();
                twCommunity.DivisionID = division[SCIDs.DivisionFieldIDs.LegacyDivisionID].CastAs<int>();
                twCommunity.BrandID = company[SCIDs.CompanyFields.LegacyCompanyID].CastAs<int>();
                var status = _sourceDB.GetItem(currentCommunity.CommunityStatus.Raw);
                twCommunity.CommunityStatusID = status[SCIDs.StatusIds.CommunityStatus.Fields.Code].CastAs<int>();


                twCommunity.Name = currentCommunity.CommunityName;
                var schoolDistrict = currentCommunity.SchoolDistrict.Raw;
                if (schoolDistrict.IsNotEmpty())
                {
                    var schoolDistrictObj = _sourceDB.GetItem(schoolDistrict);
                    twCommunity.SchoolDistrictName = schoolDistrictObj[SCIDs.SchoolDistrict.SchoolDistrictName];
                    twCommunity.SchoolDistrictID = schoolDistrictObj[SCIDs.SchoolDistrict.SchoolDisrictID];
                }

                twCommunity.IsMasterPlanned = currentCommunity.IsMasterPlanned;
                twCommunity.IsActiveAdult = currentCommunity.IsActiveAdult;
                twCommunity.IsGated = currentCommunity.IsGated;
                twCommunity.IsCondosOnly = currentCommunity.IsCondoOnly;

                twCommunity.Description = (currentCommunity.CommunityDescription +
                                           currentCommunity.CommunityDescription2).Left(2000);
                twCommunity.DrivingDirections = currentCommunity.DrivingDirectionsText.Text.Left(1000);
                twCommunity.GeoLongitude = currentCommunity.CommunityLongitude.Raw.CastAs<decimal>();
                twCommunity.GeoLatitude = currentCommunity.CommunityLatitude.Raw.CastAs<decimal>();
                twCommunity.OfficeHours = currentCommunity.GetOfficeHours().Left(2000);




                if (existingCommunity == null)
                {
                    db.TW_Communities.AddObject(twCommunity);
                }

                db.SaveChanges();

                if (existingCommunity == null)
                {
                    var newcommunityLegacyID = twCommunity.CommunityID;
                    using (new EventDisabler())
                    using (new EditContext(sourceItem))
                    {
                        var legacyID = currentCommunity.CommunityLegacyID.Field.InnerField.ID;
                        sourceItem.Fields[legacyID].Value =
                            newcommunityLegacyID.ToString(CultureInfo.InvariantCulture);
			
                    }
                }

                var communityAddressExits = from c in db.TW_OrgUnitAddresses
                                            where c.CommunityID == communityLegacyID
                                            select c;
                var existingCommunityAddress = communityAddressExits.FirstOrDefault();
                var twOrgUnitAddress = existingCommunityAddress ?? new TW_OrgUnitAddresses();
                twOrgUnitAddress.AddressTypeID = 1;
                twOrgUnitAddress.BrandID = twCommunity.BrandID;
                twOrgUnitAddress.DivisionID = twCommunity.DivisionID;
                twOrgUnitAddress.CommunityID = twCommunity.CommunityID;
                twOrgUnitAddress.StreetAddress =
                    (currentCommunity.StreetAddress1 + " " + currentCommunity.StreetAddress2).Left(100);
                twOrgUnitAddress.City = city.CityName;
                twOrgUnitAddress.State = new LookupItem(_sourceDB.GetItem(state[SCIDs.StatePage.StateName])).Code;
                twOrgUnitAddress.Zip = currentCommunity.ZiporPostalCode;


                var communityPhoneRecordExists = from c in db.TW_OrgUnitPhones
                                                 where c.CommunityID == communityLegacyID
                                                 select c;
                var existingCommunityPhoneRecord = communityPhoneRecordExists.FirstOrDefault();
                var twOrgUnitPhones = existingCommunityPhoneRecord ?? new TW_OrgUnitPhones();
                twOrgUnitPhones.PhoneTypeID = 1;
                twOrgUnitPhones.BrandID = twCommunity.BrandID;
                twOrgUnitPhones.DivisionID = twCommunity.DivisionID;
                twOrgUnitPhones.CommunityID = twCommunity.CommunityID;
                twOrgUnitPhones.PhoneNumber = currentCommunity.Phone;


                var communityContactExist = from c in db.TW_ContactSettings
                                            where c.CommunityID == communityLegacyID
                                            select c;
                var existingCommunityContact = communityContactExist.FirstOrDefault();
                var twContactSettings = existingCommunityContact ?? new TW_ContactSettings();
                twContactSettings.ContactSettingTypeID = 6;
                twContactSettings.DivisionID = null;
                twContactSettings.CommunityID = twCommunity.CommunityID;
                twContactSettings.Email = currentCommunity.RespondingEmailAddress;
                if (!string.IsNullOrWhiteSpace(currentCommunity.SalesOfficeID.Raw))
                {
                    //update or insert salesoffice id
                    var salesOfficeIDFieldValue = currentCommunity.SalesOfficeID.Raw;
                    var soCommunityParams = from c in db.CRM_CommunityParams
                                            where c.CommunityID == communityLegacyID && c.IDCode == "NSS_SO_ID"
                                            select c;

                    var soExistingCommunityParam = soCommunityParams.FirstOrDefault();

                    var soCrmCommunityParam = soExistingCommunityParam ?? new CRM_CommunityParams();

                    soCrmCommunityParam.IDCode = "NSS_SO_ID";
                    soCrmCommunityParam.CommunityID = twCommunity.CommunityID;

                    soCrmCommunityParam.SValue = !string.IsNullOrWhiteSpace(salesOfficeIDFieldValue)
                             ? salesOfficeIDFieldValue
                             : soCrmCommunityParam.SValue;



                    if (existingCommunityAddress == null)
                    {
                        db.TW_OrgUnitAddresses.AddObject(twOrgUnitAddress);
                    }
                    if (existingCommunityPhoneRecord == null)
                    {
                        db.TW_OrgUnitPhones.AddObject(twOrgUnitPhones);
                    }
                    if (existingCommunityContact == null)
                    {
                        db.TW_ContactSettings.AddObject(twContactSettings);
                    }
                    if (soExistingCommunityParam == null)
                    {
                        db.CRM_CommunityParams.AddObject(soCrmCommunityParam);
                    }

                    db.SaveChanges();

                    //update or insert project id (subdivid)

                    var projectIDFieldValue = currentCommunity.ProjectIDs.Text.Trim();
                    var commParamsToAddForPids = new List<CRM_CommunityParams>();
                    if (!string.IsNullOrWhiteSpace(projectIDFieldValue))
                    {
                        var projectIDs = projectIDFieldValue.Split(',').ToList();


                        var allCommParamsWithPIDs = from c in db.CRM_CommunityParams
                                                    where c.CommunityID == communityLegacyID && c.IDCode == "NSS_SD_ID"
                                                    select c;

                        //projectids now has only new ids. delete SDID records that dont match 
                        var pidCommparamsToDelete = allCommParamsWithPIDs.Where(cp => !projectIDs.Contains(cp.SValue));
                        foreach (var pidCommparamToDelete in pidCommparamsToDelete)
                        {
                            db.CRM_CommunityParams.DeleteObject(pidCommparamToDelete);

                        }
                        db.SaveChanges();

                        allCommParamsWithPIDs = from c in db.CRM_CommunityParams
                                                where c.CommunityID == communityLegacyID && c.IDCode == "NSS_SD_ID"
                                                select c;

                        //remove existing SDID from currentcommunity projectids
                        foreach (var pidCommParam in allCommParamsWithPIDs)
                        {
                            projectIDs.Remove(pidCommParam.SValue);
                        }


                        //add new PIDs
                        commParamsToAddForPids = projectIDs.Select(pid => new CRM_CommunityParams()
                        {
                            IDCode = "NSS_SD_ID",
                            SValue = pid,
                            CommunityID = twCommunity.CommunityID
                        }).ToList();
                    }

                    commParamsToAddForPids.ForEach(newPID => db.CRM_CommunityParams.AddObject(newPID));
                }
                 db.SaveChanges();

                communutyLegacyID = twCommunity.CommunityID.ToString(CultureInfo.InvariantCulture);
            }

            return communutyLegacyID;
        }

        public void ProcessDSOItems(Item sourceItem)
        {
            if (sourceItem.TemplateID == new ID(SchoolTypeItem.TemplateId))
            {
                var schoolType = new SchoolTypeItem(sourceItem);
                var twSchoolType = new TW_SchoolTypes();
                twSchoolType.Name = schoolType.Name;
                using (var db = new CMSIntegrationEntities())
                {
                    var existingCode = schoolType.Code.Text.CastAs<int>(0);
                    var existing = existingCode == 0
                                       ? null
                                       : db.TW_SchoolTypes.FirstOrDefault(c => c.SchoolTypeID == existingCode);
                    if (existing == null)
                    {
                        db.AddToTW_SchoolTypes(twSchoolType);
                    }
                    else
                    {
                        twSchoolType.SchoolTypeID = existing.SchoolTypeID;
                        existing.Name = twSchoolType.Name;
                    }
                    db.SaveChanges();
                }

                using (new EventDisabler())
                using (new EditContext(sourceItem))
                {
                    var schoolTypeID = twSchoolType.SchoolTypeID;
                    var schoolTypeFieldID = schoolType.Code.Field.InnerField.ID;
                    sourceItem[schoolTypeFieldID] = schoolTypeID.CastAs<string>();
                }
            }


            if (sourceItem.TemplateID == new ID(HomeFeaturesItem.TemplateId))
            {
                var homeFeaturesItem = new HomeFeaturesItem(sourceItem);
                var twHomeFeature = new TW_HomeFeatures();
                var community = new CommunityItem(sourceItem.Parent.Parent);
                var division = new DivisionPageItem(community.InnerItem.Parent.Parent);
                var categoryItem = _sourceDB.GetItem(new ID(homeFeaturesItem.HomeFeaturesCategoryHeading.Raw));
                var category = new HomeFeaturesCategoryItem(categoryItem);

                twHomeFeature.DivisionID = division.LegacyDivisionID.Text.CastAs<int>();
                twHomeFeature.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                twHomeFeature.CategoryID = category.Code.Text.CastAs<int>();
                twHomeFeature.Description = homeFeaturesItem.HomeFeaturesText.Text;

                using (var db = new CMSIntegrationEntities())
                {
                    var existingCode = homeFeaturesItem.HomeFeaturesLegacyID.Text.CastAs<int>(0);
                    var existing = existingCode == 0
                                       ? null
                                       : db.TW_HomeFeatures.FirstOrDefault(c => c.HomeFeatureID == existingCode);
                    if (existing == null)
                    {
                        db.AddToTW_HomeFeatures(twHomeFeature);
                    }
                    else
                    {
                        existing.DivisionID = division.LegacyDivisionID.Text.CastAs<int>();
                        existing.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                        existing.CategoryID = category.Code.Text.CastAs<int>();
                        existing.Description = homeFeaturesItem.HomeFeaturesText.Text;
			twHomeFeature.HomeFeatureID = existing.HomeFeatureID;
                    }
                    db.SaveChanges();

		    
                }

                using (new EventDisabler())
		using(new EditContext(sourceItem))
		{
		    var legacyID = homeFeaturesItem.HomeFeaturesLegacyID.Field.InnerField.ID;
		    sourceItem[legacyID] = twHomeFeature.HomeFeatureID.CastAs<string>(string.Empty);
		}
            }

            if (sourceItem.TemplateID == new ID(LocalInterestItem.TemplateId))
            {
                var localInterestItem = new LocalInterestItem(sourceItem);
                var twLocalInterests = new TW_LocalInterests();
                var community = new CommunityItem(sourceItem.Parent.Parent);
                var division = new DivisionPageItem(community.InnerItem.Parent.Parent);
                var categoryItem = _sourceDB.GetItem(new ID(localInterestItem.LocalInterestsCategoryHeading.Raw));
                var category = new LocalInterestCategoryItem(categoryItem);

                twLocalInterests.DivisionID = division.LegacyDivisionID.Text.CastAs<int>();
                twLocalInterests.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                twLocalInterests.CategoryID = category.Code.Text.CastAs<int>();
                twLocalInterests.Description = localInterestItem.LocalInterestsText.Text;
                twLocalInterests.Title = localInterestItem.Name.Text;

                using (var db = new CMSIntegrationEntities())
                {
                    int existingCode = localInterestItem.LocalInterestsLegacyID.Text.CastAs<int>(0);
                    var existing = existingCode == 0
                                       ? null
                                       : db.TW_LocalInterests.FirstOrDefault(c => c.LocalInterestID == existingCode);

                    if (existing == null)
                    {
                        db.AddToTW_LocalInterests(twLocalInterests);
                    }
                    else
                    {
                        existing.DivisionID = division.LegacyDivisionID.Text.CastAs<int>();
                        existing.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                        existing.CategoryID = category.Code.Text.CastAs<int>();
                        existing.Description = localInterestItem.LocalInterestsText.Text;
                        existing.Title = localInterestItem.Name.Text;
			twLocalInterests.LocalInterestID = existing.LocalInterestID;
                    }
                    db.SaveChanges();
                   
                }

                using (new EventDisabler())
                using (new EditContext(sourceItem))
                {
                    sourceItem[localInterestItem.LocalInterestsLegacyID.Field.InnerField.ID] =
                        twLocalInterests.LocalInterestID.CastAs<string>(string.Empty);
                }
            }

            if (sourceItem.TemplateID == new ID(SchoolInfoItem.TemplateId))
            {
                var schoolInfoItem = new SchoolInfoItem(sourceItem);
                var twSchools = new TW_Schools();
                var community = new CommunityItem(sourceItem.Parent.Parent);

                twSchools.SchoolTypeID = new SchoolTypeItem(schoolInfoItem.SchoolType.Item).Code.Text.CastAs<int>(0);
                twSchools.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                twSchools.Name = schoolInfoItem.Name;
                twSchools.PrincipalName = schoolInfoItem.SchoolPrincipal;
                twSchools.DistanceFromCommunity = schoolInfoItem.SchoolDistance;
                twSchools.GradesOffered = schoolInfoItem.SchoolGrades;
                try
                {
                    twSchools.URL = schoolInfoItem.SchoolWebsiteURL.Url;
                }
                catch (Exception)
                {

                    twSchools.URL = schoolInfoItem.SchoolWebsiteURL.Raw;
                }
               
                twSchools.SchoolAuthorityID = schoolInfoItem.SchoolSourceID;

                using (var db = new CMSIntegrationEntities())
                {
                    int existingCode = schoolInfoItem.SchoolLegacyID.Text.CastAs<int>(0);
                    var existing = existingCode == 0
                                       ? null
                                       : db.TW_Schools.FirstOrDefault(c => c.SchoolID == existingCode);

                    if (existing == null)
                    {
                        db.AddToTW_Schools(twSchools);
                    }
                    else
                    {
                        existing.SchoolTypeID =
                            new SchoolTypeItem(schoolInfoItem.SchoolType.Item).Code.Text.CastAs<int>();
                        existing.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                        existing.Name = schoolInfoItem.Name;
                        existing.PrincipalName = schoolInfoItem.SchoolPrincipal;
                        existing.DistanceFromCommunity = schoolInfoItem.SchoolDistance;
                        existing.GradesOffered = schoolInfoItem.SchoolGrades;
                        try
                        {
                            existing.URL = schoolInfoItem.SchoolWebsiteURL.Url;
                        }
                        catch (Exception)
                        {

                            existing.URL = schoolInfoItem.SchoolWebsiteURL.Raw;
                        }
                        existing.SchoolAuthorityID = schoolInfoItem.SchoolSourceID;
			twSchools.SchoolID = existing.SchoolID;
                    }
                    db.SaveChanges();
                   
                }

                using (new EventDisabler())
		using(new EditContext(sourceItem))
		{
		    sourceItem[schoolInfoItem.SchoolLegacyID.Field.InnerField.ID] = twSchools.SchoolID.CastAs<string>(string.Empty);
		}
            }
        }
    }
}