﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Entities.DataTemplate.Common;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.CrmIntegration
{
    public class InventoryIntegrater
    {
        private readonly Database _sourceDB;
        private readonly Database _targetDB;

        public InventoryIntegrater(Database sourceDB, Database targetDB)
        {
            _sourceDB = sourceDB;
            _targetDB = targetDB;
        }

        public string Integrate(Item spec)
        {
            var currentHfs = new HomeForSaleItem(spec);

            var masterPlan = new MasterPlanItem(_sourceDB.GetItem(new ID(currentHfs.MasterPlanID.Raw)));
            var plan = new PlanItem(currentHfs.InnerItem.Parent);
            var community = new CommunityItem(plan.InnerItem.Parent);
            var legacyID = currentHfs.HomeforSaleLegacyID.Text.CastAs<int>(0);
            var statusID = currentHfs.HomeforSaleStatus.Raw == SCIDs.StatusIds.Status.Active ? 1 : 0;
            using (var db = new CMSIntegrationEntities())
            {
                var hfsExists = from s in db.TW_CommunitySpecs where s.CommunitySpecID == legacyID select s;
                var existingHfs = hfsExists.FirstOrDefault();
                var twCommunitySpec = existingHfs ?? new TW_CommunitySpecs();
                twCommunitySpec.Name = currentHfs.DisplayName;
                twCommunitySpec.CommunityPlanID = plan.PlanLegacyID.Text.CastAs<int?>();
                twCommunitySpec.HomeOfferingStatusID = statusID;
                twCommunitySpec.IsModel = currentHfs.IsaModelHome;
                twCommunitySpec.LotNumber = currentHfs.LotNumber;
                twCommunitySpec.StreetAddress = (currentHfs.StreetAddress1 + " " + currentHfs.StreetAddress2).Left(100);
                twCommunitySpec.Description = currentHfs.PlanBase.PlanDescription.Text.Left(1500);
                twCommunitySpec.AvailabilityText = currentHfs.StatusforAvailability.Item.Name.Left(50);
                twCommunitySpec.AvailabilityDate = currentHfs.AvailabilityDate;
                twCommunitySpec.MLSID = currentHfs.MLSNumber;
                twCommunitySpec.Price = currentHfs.Price;
                twCommunitySpec.SqFt = currentHfs.PlanBase.SquareFootage.Text.CastAs<int>();
                twCommunitySpec.BedroomCount = currentHfs.PlanBase.NumberofBedrooms.Rendered.CastAs<int>();
                twCommunitySpec.BathroomCount = currentHfs.PlanBase.NumberofBathrooms.Rendered.CastAs<int>();
                twCommunitySpec.HalfBathroomCount = currentHfs.PlanBase.NumberofHalfBathrooms.Rendered.CastAs<int>();
                twCommunitySpec.NumberOfStories = currentHfs.PlanBase.NumberofStories.Rendered.CastAs<int>();
                twCommunitySpec.GarageCapacity = currentHfs.PlanBase.NumberofGarages.Rendered.CastAs<int>();
                if (currentHfs.PlanBase.GarageEntryLocation.Rendered.IsNotEmpty())
                {
                    twCommunitySpec.GarageEntryCode = currentHfs.PlanBase.GarageEntryLocation.Item.DisplayName.Left(1);
                }
                if (currentHfs.PlanBase.MasterBedroomLocation.Rendered.IsNotEmpty())
                {
                    twCommunitySpec.MasterbedroomLocation = currentHfs.PlanBase.MasterBedroomLocation.Item.DisplayName.Left(1);
                }

		//http://bits.builderhomesite.com/production/default.asp?63765
                var isMonarch = spec.Paths.FullPath.ToLowerInvariant().Contains("monarch");
		twCommunitySpec.LivingAreasCount = isMonarch ? 1 : currentHfs.PlanBase.NumberofLivingAreas.Rendered.CastAs<int>();
                twCommunitySpec.DiningAreasCount = currentHfs.PlanBase.NumberofDiningAreas.Rendered.CastAs<int>();
                twCommunitySpec.Width = masterPlan.PlanWidth;
                twCommunitySpec.Depth = masterPlan.PlanDepth;
                twCommunitySpec.IsActive = currentHfs.HomeforSaleStatus.Raw == SCIDs.StatusIds.Status.Active;
                twCommunitySpec.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                var bhiProductTypeLookupItem = new LookupItem(_sourceDB.GetItem(new ID(masterPlan.BHIProductType.Raw)));

                twCommunitySpec.SpecBHITypeID = bhiProductTypeLookupItem.CMSId.Text.CastAs<int>();
                var bhiProductDisplayTypeLookupItem = new LookupItem(_sourceDB.GetItem(new ID(masterPlan.BHIProductDisplayType.Raw)));
                twCommunitySpec.SpeectBHIDisplayTypeID = bhiProductDisplayTypeLookupItem.CMSId.Text.CastAs<int>();

              
                twCommunitySpec.NumberOfBonusRooms = currentHfs.PlanBase.NumberofDens.Rendered.CastAs<int>(0) +
                                                     currentHfs.PlanBase.NumberofSolariums.Rendered.CastAs<int>(0) +
                                                     currentHfs.PlanBase.NumberofBonusRooms.Rendered.CastAs<int>(0);
                twCommunitySpec.IsREPInventory = currentHfs.DisplayonREPPage;
                twCommunitySpec.IsFeaturedHome = currentHfs.DisplayonHomesforSalePage;
                twCommunitySpec.HomeSignNumber = currentHfs.myTMNumber.Text.CastAs<int?>(null);
                twCommunitySpec.IncentiveAmount = currentHfs.RealtorIncentiveAmount.Text.CastAs<int?>(null);
                twCommunitySpec.CommisionAmount = currentHfs.RealtorBrokerCommissionRate.Text.CastAs<int?>(null);

		if(existingHfs==null)
		{
		    db.AddToTW_CommunitySpecs(twCommunitySpec);
		}

                db.SaveChanges();

                if (existingHfs == null)
                {
                    var newCommunitySpecID = twCommunitySpec.CommunitySpecID;
                    using (new EventDisabler())
                    using (new EditContext(spec))
                    {
                        var newlegacyID = currentHfs.HomeforSaleLegacyID.Field.InnerField.ID;
                        spec.Fields[newlegacyID].Value =
                            newCommunitySpecID.ToString(CultureInfo.InvariantCulture);
                        legacyID = newCommunitySpecID;
                    }
                }
            }

            return legacyID.ToString(CultureInfo.InvariantCulture);
        }
    }
}