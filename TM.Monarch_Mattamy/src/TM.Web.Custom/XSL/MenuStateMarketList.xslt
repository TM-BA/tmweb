﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:sql="http://www.sitecore.net/sql"
	xmlns:tm="TM.Web.Custom.XSL.XslExtensions"
  exclude-result-prefixes="sc sql tm">

	<!-- output directives -->
	<xsl:output method="html" indent="no" encoding="UTF-8"  />

	<!-- sitecore parameters -->
	<xsl:param name="lang" select="'en'"/>
	<xsl:param name="id" select="''"/>
	<xsl:param name="sc_item"/>
	<xsl:param name="sc_currentitem"/>

	<xsl:variable name="new-homes" select="/*/item[@key='content']/item[@key='home']/item[@key='new homes']" />
	<xsl:variable name="new-condos" select="/*/item[@key='content']/item[@key='home']/item[@key='new condos']" />
	<xsl:variable name="statename" select="NA" />
	<xsl:variable name="diviname" select="NA" />


	<!-- entry point -->
	<xsl:template match="*">
		<xsl:for-each select="$new homes/item">
			<xsl:variable name="statename" select="@name" />			
			<ul class="ddlist">
				<xsl:if test ="@template = 'statepage'">			
					<li>
						<a>
							<xsl:attribute name="href">
								<xsl:value-of select="tm:GetUrl(.)" />
							</xsl:attribute>
							<sc:html field="State Name" />
						</a>
					</li>
				</xsl:if>
				<xsl:for-each select="item">
					<xsl:variable name="diviname" select="@name" />

					<!--If any market under state page-->
					<xsl:if test ="@template = 'citypage'">
						<li>
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="tm:GetUrl(.)" />								
								</xsl:attribute>
								<sc:text field="Title" />
							</a>
						</li>
					</xsl:if>

					<xsl:for-each select="item[@template = 'citypage']">
						<li>
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="tm:GetUrl(.)" />
								</xsl:attribute>
								<sc:text field="Title" />
							</a>
						</li>
					</xsl:for-each>
				</xsl:for-each>
			</ul>
		</xsl:for-each>

		<!--<ul class="ddlist">
			<li><a href="#">TEXAS</a></li>
			<li><a href="#">Austin</a></li>
		</ul>-->


	</xsl:template>

</xsl:stylesheet>
