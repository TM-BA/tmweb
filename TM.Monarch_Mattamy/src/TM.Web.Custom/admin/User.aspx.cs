﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using TM.Domain;
using TM.Utils.Web;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;


namespace TM.Web.Custom.admin
{
	public partial class User : BasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var uri = HttpContext.Current.Request.Url;
			var qryString =  uri.Query.Remove(0, 1);

			var values = qryString.Split('&');

			var userName = values[0].Split('=')[1];
			var password = values[1].Split('=')[1];
			litStatus.Text = ValidateUser(userName, password).ToString();
		}

		public int ValidateUser(string userName, string password)
		{
			userName = HttpUtility.UrlDecode(userName); 
			var tmUser = new TMUser(WebUserExtensions.GetUsersCurrentDomain(), userName) { Password = password };
			
			CurrentWebUser = null;
			if (tmUser.Login(true))
			{
				CurrentWebUser = tmUser.Get(true);
				return 1;
			}
			return 0;
		}
		
	}
}