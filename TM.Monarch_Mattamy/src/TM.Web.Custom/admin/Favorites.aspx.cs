﻿using System;
using System.Collections.Specialized;
using System.Web;
using Sitecore.Data;
using Sitecore.Security.Authentication;
using TM.Domain;
using TM.Utils.Web;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.admin
{
    public partial class Favorites : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //new code
            NameValueCollection queryParams = Request.QueryString;
            if (queryParams.Count > 0)
            {
                string action = queryParams["action"];
                string id = queryParams["id"];
                if (!string.IsNullOrWhiteSpace(action) && !string.IsNullOrWhiteSpace(id))
                {
                    if (action.ToLowerInvariant() == "remove")
                    {
                        RemoveIFPFromProfile(id);
                    }
                }
            }


            Uri uri = HttpContext.Current.Request.Url;
            string qryString = uri.Query.Remove(0, 1);

            string[] values = qryString.Split('&');
            var myFavorites = new MyFavorites
                                  {
                                      Id = new ID(values[0].Split('=')[1].Replace("%7B", "{").Replace("%7D", "}")),
                                      Type =
                                          (GlobalEnums.FavoritesType)
                                          Enum.Parse(typeof (GlobalEnums.FavoritesType), values[1].Split('=')[1]),
                                      Action =
                                          (GlobalEnums.FavoritesAction)
                                          Enum.Parse(typeof (GlobalEnums.FavoritesAction), values[2].Split('=')[1])
                                  };

            switch (myFavorites.Action)
            {
                case GlobalEnums.FavoritesAction.Add:
                    AddToFavorite(myFavorites);
                    break;
                case GlobalEnums.FavoritesAction.Remove:
                    RemoveFromFavorite(myFavorites);
                    break;
            }
        }

        private void RemoveIFPFromProfile(string id)
        {
             TMUser tmUser = CurrentWebUser;
             if (tmUser != null)
             {
                 AuthenticationManager.SetActiveUser(tmUser.FullyQualifiedUserName);
             }
            new FavoritesHelper().RemoveIFPFromProfile(id);
        }

        public void AddToFavorite(MyFavorites myFavorites)
        {
            TMUser tmUser = CurrentWebUser;
            if (tmUser != null)
            {
                AuthenticationManager.SetActiveUser(tmUser.FullyQualifiedUserName);

                var upUser = new TMUser(TMUserDomain.TaylorMorrison, tmUser.UserName)
                                 {
                                     CommunityFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.Community)
                                             ? tmUser.CommunityFavorites + "|" + myFavorites.Id
                                             : tmUser.CommunityFavorites,
                                     PlanFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.Plan)
                                             ? tmUser.PlanFavorites + "|" + myFavorites.Id
                                             : tmUser.PlanFavorites,
                                     HomeforSaleFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.HomeForsale)
                                             ? tmUser.HomeforSaleFavorites + "|" + myFavorites.Id
                                             : tmUser.HomeforSaleFavorites
                                 };
                upUser.Update();
                CurrentWebUser = upUser;
            }
        }

        public void RemoveFromFavorite(MyFavorites myFavorites)
        {
            TMUser tmUser = CurrentWebUser;
            if (tmUser != null)
            {
                AuthenticationManager.SetActiveUser(tmUser.FullyQualifiedUserName);
                var favid = myFavorites.Id.ToString().ToLowerInvariant();
                var upUser = new TMUser(TMUserDomain.TaylorMorrison, tmUser.UserName)
                                 {
                                     CommunityFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.Community)
                                             ? tmUser.CommunityFavorites.ToLowerInvariant().Replace("|" + favid, "").Replace(favid, "")
                                             : tmUser.CommunityFavorites,
                                     PlanFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.Plan)
                                             ? tmUser.PlanFavorites.ToLowerInvariant().Replace("|" + favid, "").Replace(favid, "")
                                             : tmUser.PlanFavorites,
                                     HomeforSaleFavorites =
                                         (myFavorites.Type == GlobalEnums.FavoritesType.HomeForsale)
                                             ? tmUser.HomeforSaleFavorites.ToLowerInvariant().Replace("|" + favid, "").Replace(favid, "")
                                             : tmUser.HomeforSaleFavorites
                                 };
                upUser.Update();
                CurrentWebUser = upUser;
            }
        }
    }
}
