﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using TM.Domain;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.admin
{
    public partial class ImportSOSDIDs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnPublish.Visible = Page.IsPostBack;
        }

        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            FindContentRootAndRunImport();
        }

        private readonly Sitecore.Data.Database masterDB = Sitecore.Configuration.Factory.GetDatabase("master");
        private int _processedItemCount;
        private StringBuilder _messages;
        private List<Item> _availableItems;

        private void FindContentRootAndRunImport()
        {
            DataSource dataSource = masterDB.DataManager.DataSource;
	   

           /* string line;
            using (var file =new System.IO.StreamReader(Server.MapPath("/communityemail.txt")))
            {
                while ((line = file.ReadLine()) != null)
                {
                    var split = line.Split(',');
                    _ht.Add(split[0].Trim(), split[1].Trim());
                }
            }

            _communityCount = 0;*/
            _processedItemCount = 0;
            _messages =
                new StringBuilder("<b>Report:</b><br/>");
            _messages.AppendFormat("<table><tr><th>Item Path</th><th>Last Modified Date</th></tr>");
            var rootPath = txtRootPath.Text;
            _availableItems = masterDB.GetItem(SCIDs.Common.DataFields.HomeAvailabilityStatus).GetChildren().ToList<Item>();
            var root = rootPath.IsNotEmpty() ? masterDB.GetItem(rootPath).ID : Sitecore.ItemIDs.ContentRoot;
            ImportSOSDIDFromDB(root, dataSource);
            _messages.AppendFormat("</table>");
            _messages.AppendFormat("Total items processed: {0}", _processedItemCount);
            litReport.Text = _messages.ToString();
            CacheManager.ClearAllCaches();
        }

        public void ImportSOSDIDFromDB(ID itemId, DataSource dataSource)
        {
            IDList childIds = dataSource.GetChildIDs(itemId);
             

            foreach (ID childId in childIds)
            {
                ImportSOSDIDFromDB(childId, dataSource);
            }

            var item = masterDB.GetItem(itemId);

            var templateID = item.TemplateID;

            if (templateID == new ID(HomeForSaleItem.TemplateId))
            {
                var statusForAvailability = item["Status for Availability"];
                var isID = Sitecore.Data.ID.IsID(statusForAvailability);
		if(!isID)
		{
		    var matchedStatusForAvailabilityItem = _availableItems.FirstOrDefault(x => x["Name"] == statusForAvailability);

	                if(matchedStatusForAvailabilityItem!=null)
	                {
                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            var lastModifiedDate = item.Statistics.Updated.ToShortDateString();
                            var highLight = item.Statistics.Updated > new DateTime(2013, 11, 1);
                            using (new Sitecore.Data.Events.EventDisabler())
                            using (new Sitecore.Data.Items.EditContext(item))
                            {
                                item["Status for Availability"] = matchedStatusForAvailabilityItem.ID.ToString();
                                _messages.AppendFormat("<tr><td>{0}</td><td>{1}</td> </tr>", item.Paths.FullPath, highLight ? "<span style='color:red'>"+lastModifiedDate + "</span>": lastModifiedDate);
                                    _processedItemCount++;
                                hdnProcessedItems.Value = hdnProcessedItems.Value + "|" + item.ID;
                            }
                        }
	                }

		}
                

            }
        }

        private static void SetCommunitySOSDID(string soID, string sdIDs, Item item, CommunityItem community)
        {
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                using (new Sitecore.Data.Items.EditContext(item))
                {
                    using (new Sitecore.Data.Events.EventDisabler())
                    {
                        item[community.SalesOfficeID.Field.InnerField.ID] = soID;
                        item[community.ProjectIDs.Field.InnerField.ID] = sdIDs;
                    }
                }
            }
        }

        private static void GetSOSDIDs(int communityLegacyID, out string sdIDs, out string soID)
        {
            
            using (var db = new CMSIntegrationEntities())
            {
                soID = (from c in db.CRM_CommunityParams
                        where c.CommunityID == communityLegacyID && c.IDCode == "NSS_SO_ID"
                        select c.SValue).ToList().LastOrDefault();

                var sdIDsQuery = (from c in db.CRM_CommunityParams
                                  where c.CommunityID == communityLegacyID && c.IDCode == "NSS_SD_ID"
                                  select c.SValue).ToArray();

                sdIDs = string.Join(",", sdIDsQuery);
            }
        }

        protected void btnPublish_OnClick(object sender, EventArgs e)
        {
            var itemIDs = hdnProcessedItems.Value.Trim('|').Split(new[]{"|"},StringSplitOptions.RemoveEmptyEntries);

            foreach (var itemID in itemIDs)
            {
                var item = masterDB.GetItem(new ID(itemID));
                Sitecore.Publishing.PublishManager.AddToPublishQueue(item, ItemUpdateType.Saved);
            }
	    Database[] targetDBs = new Database[] { Sitecore.Configuration.Factory.GetDatabase("web") };
	    Language[] languages = new Language[] { LanguageManager.GetLanguage("en") };
	    Sitecore.Publishing.PublishManager.PublishIncremental(masterDB, targetDBs, languages);
	
	 

        }
    }
}