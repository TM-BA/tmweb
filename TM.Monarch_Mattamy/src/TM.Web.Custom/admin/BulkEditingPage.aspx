﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BulkEditingPage.aspx.cs" Inherits="TM.Web.Custom.admin.BulkEditingPage" %>
<%@ Import Namespace="TM.Utils.Extensions" %>
<%@Register tagPrefix="uc" tagName="homeForSale" src="~/tmwebcustom/SubLayouts/Community/homesForSale.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
     <style type="text/css">
        body,input,select
        {
            white-space:nowrap;
            font-family: verdana;
            font-size: 11px;
			
			
        }
		input[type="submit"]
		{
		  background-color:#b9c9fe;
		}
    .name
    {
        clear:left;
        white-space:normal;
    }
    .name{width:300px; float:left}
    .adStatus{width:150px; float:left;}
    .price,.sqft, .availDate, .lot, .tmNum, .saleStatus, .searchStatus,.priceTxt, .available
    {
        width:110px;
        float:left;
		color:#669;
    }
    .sqft,.saleStatus, .searchStatus,.availDate, .available
    {
        width:80px;
    }
    .lot, .tmNum
    {
        width:40px;
    }
    .price
    {
        width:50px;
    }
    .bed,.bath, .hBath
    {
        width:35px;
        float:left;
    }
    .featRWE, .salePage, .repPage
    {
        width:20px;
        float:left;
    }
    .available
    {
        width: 118px;
     }
      
          div.rowBreak {clear:both; float:left;width:100%;height:1px;border-top:solid 1px #ccc; padding: 6px 8px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label Text="Page: " ID="lblPageNum" runat="server"></asp:Label><asp:DropDownList ID="drpPageNum" runat="server" DataTextField="Page Number" AutoPostBack="true"></asp:DropDownList>
        <p><asp:CheckBox Enabled="true"  ID="chkIncludeInactive" runat="server" AutoPostBack="true" Text="Include Inactive Items" /> (dramatically slows this page down if checked)</p>
		<asp:Button ID="btnSubmit" runat="server" Text="Submit" /> 
        <div style="display:none;"><asp:Label Text="1" ID="lblCurrentPage" runat="server"></asp:Label><asp:CheckBox Enabled="true"  ID="chkCurrentInactive" runat="server" AutoPostBack="true" Text="Include Inactive Items" /></div>
        <asp:Literal ID="litHeading" runat="server"></asp:Literal>
    </form>
	<script type="text/javascript">
	    if (typeof jQuery == 'undefined') {
	        document.write(unescape("%3Cscript src='/Javascript/lib/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
	    }
        </script>

        <script type="text/javascript">
	    var $j = jQuery.noConflict();



	    var isUpdated = false;
        function scGetFrameValue() {
            if (isUpdated) {
                if (confirm('Do you want to save changes to <%=TypeText %> details?')) {
                    alert("submiting " + $j('#<%= btnSubmit.ClientID %>').length);
                    $j('#<%= btnSubmit.ClientID %>').click();
                    alert("form submitted");
                }
            }
        }

        $j(document).ready(function () {
            $j("input").change(function () {
                isUpdated = true;
            });
            $j("select").change(function () {
                isUpdated = true;
            });

        });
    </script>
</body>
</html>
<script language="c#" runat="server">
    
  protected void Page_PreRender(object sender, EventArgs e)
    {

        if (Page.IsPostBack)
        {
            var sb = new System.Text.StringBuilder();

            foreach (string key in Request.Form.Keys)
            {
                if (!key.Contains("__"))
                    sb.Append(string.Format("{0}{1}={2}",
                       sb.Length == 0 ? string.Empty : "|",
                       key,
                       Request.Form[key]));
            }

            var poststring = sb.ToString();

            Sitecore.Diagnostics.Log.Info("Grid Editor PostData: "+ poststring, this);
        }
    }

</script>
