﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore.Caching;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;






namespace TM.Web.Custom.admin
{
    public partial class BulkEditingPage : Page
    {
        private readonly Database _materDB = Factory.GetDatabase("master");
        private ID _searchType = Sitecore.Data.ID.Null;
        private bool _includeInactive;
        private int _pageNum = 1;
        private ID _pageContainerItemID = Sitecore.Data.ID.Null;

        public int NumPages = 1;
        public string TypeText = string.Empty;
        



        private const int PageSize = 25;
	private ID SearchType
        {
            get
            {
                if (_searchType == Sitecore.Data.ID.Null)
                {
                    if (Request["type"] != null)
                    {
                        _searchType = new ID(Request["type"]);
                    }
                    else
                    {
                        _searchType = SCIDs.TemplateIds.HomeForSalePage;
                    }
                }
                return _searchType;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _pageContainerItemID = Sitecore.Data.ID.IsID(Request["id"]) ? new ID(Request["id"]) : _pageContainerItemID;
            if (!Page.IsPostBack)
            {
                if (SearchType == SCIDs.TemplateIds.HomeForSalePage)
                {
                    GetInventoryValues();
                }
                else if (SearchType == SCIDs.TemplateIds.PlanPage)
                {
                    GetPlanValues();
                }
                else
                {
                    LogToScreen("Unrecognised type requested for Grid");
                }
            }
            if (SearchType == SCIDs.TemplateIds.HomeForSalePage)
            {
                TypeText = "Homes For Sale";
            }
            else if (SearchType == SCIDs.TemplateIds.PlanPage)
            {
                TypeText = "Plan";
            }
            else
            {
                LogToScreen("Unrecognised type requested for Grid");
            }
            drpPageNum.SelectedIndexChanged += btnSubmit_Click;
            chkIncludeInactive.CheckedChanged += btnSubmit_Click;
            btnSubmit.Click += btnSubmit_Click;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (SearchType == SCIDs.TemplateIds.HomeForSalePage)
            {
                _pageNum = lblCurrentPage.Text.CastAs(1);
                _includeInactive = chkCurrentInactive.Checked;

                InsertInventoryToSitecore();
                //we do this twice on purpose
                lblCurrentPage.Text = drpPageNum.SelectedValue;
                _pageNum = lblCurrentPage.Text.CastAs(1);
                _includeInactive = chkIncludeInactive.Checked;

                chkCurrentInactive.Checked = chkIncludeInactive.Checked;
                _includeInactive = chkCurrentInactive.Checked;
                GetInventoryValues();
            }
            else if (SearchType == SCIDs.TemplateIds.PlanPage)
            {
                _pageNum = lblCurrentPage.Text.CastAs(1);
                _includeInactive = chkCurrentInactive.Checked;


                InsertPlanToSitecore();
                //we do this twice on purpose
                lblCurrentPage.Text = drpPageNum.SelectedValue;
                _pageNum = lblCurrentPage.Text.CastAs(1);

                chkCurrentInactive.Checked = chkIncludeInactive.Checked;
                _includeInactive = chkCurrentInactive.Checked;
                GetPlanValues();
            }
        }

        protected void LogToScreen(string message)
        {
            Response.Write(message);
            Response.Flush();
        }

        protected void GetInventoryValues()
        {
            //Database MASTER_DB = Factory.GetDatabase("master")
            using (new SecurityDisabler())
            {
               
                using (new BulkUpdateContext())
                {
                    //getStates parent node
                    var homes = new List<Item>();
                    string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' {2}]".FormatWith(
                        Request["id"],
                        SearchType,
                        _includeInactive
                            ? string.Empty
                            : " and @Home for Sale Status='" + SCIDs.StatusIds.Status.Active + "'");

                    var homeForSaleItems = _materDB.SelectItems(query);
                   
                    homes.AddRange(homeForSaleItems);

                    if (_pageContainerItemID != Sitecore.Data.ID.Null)
                    {
                        var currentItem = _materDB.GetItem(_pageContainerItemID);
                        if (currentItem.TemplateID == SCIDs.TemplateIds.DivisionPage)
                        {

                            homes =
                                homes.OrderBy(h => h.Parent.Parent.GetItemName())
                                    .ThenBy(h => h["square footage"].CastAs<int>(0))
                                    .ToList();
                        }
                        else
                        {
                            homes = homes.OrderBy(h => h["square footage"].CastAs<int>(0)).ToList();
                        }

                    }

                   
		    

                    NumPages = homes.Count/PageSize;
                    drpPageNum.Items.Clear();
                    for (int i = 0; i <= NumPages; i++)
                    {
                        drpPageNum.Items.Add(new ListItem((i + 1).ToString()));
                    }

                    if (_pageNum > drpPageNum.Items.Count)
                        _pageNum = 1;

                    drpPageNum.SelectedValue = _pageNum.ToString();
                    drpPageNum.Visible = (drpPageNum.Items.Count > 1);
                    lblPageNum.Visible = drpPageNum.Visible;

                    List<Item> bedItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.BedroomCounts).GetChildren().ToList();
                    List<Item> bathItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.BathroomCounts).GetChildren().ToList();
                    List<Item> halfBathItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.HalfBathroomCounts).GetChildren().ToList();
                    List<Item> availableItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.HomeAvailabilityStatus).GetChildren().ToList();
                    var saleStatusItems = new List<Item>();
                    saleStatusItems.Add(_materDB.GetItem(SCIDs.StatusIds.Status.Active));
                    saleStatusItems.Add(_materDB.GetItem(SCIDs.StatusIds.Status.InActive));
                    litHeading.Text = @"
        <div class=""head"">
            <img src=""/Images/admin/title1.jpg"" />
        </div>
	        <div class=""rowBreak""></div>";
                    foreach (
                        Item home in
                            homes.Skip(PageSize*(_pageNum - 1))
                                .Take(PageSize))
                    {
                        var description = new Label();
                        description.ID = "lbl_" + home.ID.ToShortID();
                        description.Text = string.Format("<div class=\"name\">{0}:<i>{1}</i>:<b>{2}</b></div>"
                            , home.Parent.Parent.DisplayName.Replace("Community",string.Empty)
                            , home.Parent.DisplayName.Replace("Plan",string.Empty)
                            , home.DisplayName.Replace("Home Available Now at", string.Empty)
                            );
                        form1.Controls.Add(description);

                        var txtPrice = new TextBox();
                        txtPrice.ID = "price_" + home.ID.ToShortID();
                        txtPrice.Text = home.Fields["Price"].ToString();
                        txtPrice.CssClass = "price";
                        form1.Controls.Add(txtPrice);

                        var txtSqft = new TextBox();
                        txtSqft.ID = "sqft_" + home.ID.ToShortID();
                        txtSqft.Text = home.Fields["Square Footage"].ToString();
                        txtSqft.CssClass = "sqft";
                        form1.Controls.Add(txtSqft);

                        var drpBed = new DropDownList();
                        bedItems.ForEach(X => drpBed.Items.Add(new ListItem(X["Name"].ToString())));
                        drpBed.ID = "bed_" + home.ID.ToShortID();
                        drpBed.SelectedIndex =
                            drpBed.Items.IndexOf(new ListItem(home.Fields["Number of Bedrooms"].ToString()));
                        drpBed.CssClass = "bed input";
                        form1.Controls.Add(drpBed);

                        var drpBath = new DropDownList();
                        bathItems.ForEach(X => drpBath.Items.Add(new ListItem(X["Name"].ToString())));
                        drpBath.ID = "bath_" + home.ID.ToShortID();
                        drpBath.SelectedIndex =
                            drpBath.Items.IndexOf(new ListItem(home.Fields["Number of Bathrooms"].ToString()));
                        drpBath.CssClass = "bath input";
                        form1.Controls.Add(drpBath);

                        var drpHalfBath =
                            new DropDownList();
                        halfBathItems.ForEach(X => drpHalfBath.Items.Add(new ListItem(X["Name"].ToString())));
                        drpHalfBath.ID = "hBath_" + home.ID.ToShortID();
                        drpHalfBath.SelectedIndex =
                            drpHalfBath.Items.IndexOf(new ListItem(home.Fields["Number of Half Bathrooms"].ToString()));
                        drpHalfBath.CssClass = "hBath input";
                        form1.Controls.Add(drpHalfBath);

                        var homeSaleStatus =
                            new DropDownList();
                        saleStatusItems.ForEach(
                            X => homeSaleStatus.Items.Add(new ListItem(X["Name"].ToString(), X.ID.ToString())));
                        homeSaleStatus.ID = "saleStat_" + home.ID.ToShortID();
                        homeSaleStatus.SelectedValue = home.Fields["Home for Sale Status"].ToString();
                        homeSaleStatus.CssClass = "saleStatus input";
                        form1.Controls.Add(homeSaleStatus);

                        var txtAvailabilityDate = new TextBox();
                        txtAvailabilityDate.ID = "availDate_" + home.ID.ToShortID();
                        string dateTime = home.Fields["Availability Date"].ToString();
                        if (dateTime.Length > 7)
                            txtAvailabilityDate.Text = dateTime.Substring(4, 2) + "/" + dateTime.Substring(6, 2) + "/" +
                                                       dateTime.Substring(0, 4);
                        txtAvailabilityDate.CssClass = "availDate input";
                        form1.Controls.Add(txtAvailabilityDate);

                        var txtLotNumber = new TextBox();
                        txtLotNumber.ID = "lot_" + home.ID.ToShortID();
                        txtLotNumber.Text = home.Fields["Lot Number"].ToString();
                        txtLotNumber.CssClass = "lot input";
                        form1.Controls.Add(txtLotNumber);

                        var txtMyTMNumber = new TextBox();
                        txtMyTMNumber.ID = "TMNum_" + home.ID.ToShortID();
                        txtMyTMNumber.Text = home.Fields["myTM Number"].ToString();
                        txtMyTMNumber.CssClass = "tmNum input";
                        form1.Controls.Add(txtMyTMNumber);


                        var chkFeatured = new CheckBox();
                        chkFeatured.ID = "feat_" + home.ID.ToShortID();
                        chkFeatured.Checked = (home.Fields["Display as Available Home in RWE"].ToString() == "1")
                            ? true
                            : false;
                        chkFeatured.CssClass = "featRWE input";
                        form1.Controls.Add(chkFeatured);


                        var chkDisplayOnHome = new CheckBox();
                        chkDisplayOnHome.ID = "displayHome_" + home.ID.ToShortID();
                        chkDisplayOnHome.Checked = (home.Fields["Display on Homes for Sale Page"].ToString() == "1")
                            ? true
                            : false;
                        chkDisplayOnHome.CssClass = "salePage input";
                        form1.Controls.Add(chkDisplayOnHome);

                        var chkDisplayOnRepPage =
                            new CheckBox();
                        chkDisplayOnRepPage.ID = "displayRep_" + home.ID.ToShortID();
                        if (null != home.Fields["Display on REP Page"])
                            chkDisplayOnRepPage.Checked = (home.Fields["Display on REP Page"].ToString() == "1")
                                ? true
                                : false;
                        chkDisplayOnRepPage.CssClass = "repPage input";
                        form1.Controls.Add(chkDisplayOnRepPage);


                        var searchSaleStatus = new CheckBox();
                        searchSaleStatus.ID = "searchSaleStatus_" + home.ID.ToShortID();
                        if (null != home.Fields["Status For Search"])
                            searchSaleStatus.Checked = (home.Fields["Status For Search"].ToString() ==
                                                        SCIDs.StatusIds.Status.Active)
                                ? true
                                : false;
                        searchSaleStatus.CssClass = "searchStatus input";
                        form1.Controls.Add(searchSaleStatus);

                        var drpAvailable = new DropDownList();
                        availableItems.ForEach(
                            X => drpAvailable.Items.Add(new ListItem(X["Name"].ToString(), X.ID.ToString())));
                        drpAvailable.ID = "available_" + home.ID.ToShortID();
                        drpAvailable.SelectedValue = home.Fields["Status for Availability"].ToString();
                        drpAvailable.CssClass = "available input";
                        form1.Controls.Add(drpAvailable);

                        var clearDiv =
                            new HtmlGenericControl("div");
                        clearDiv.Attributes["class"] = "rowBreak";
                        form1.Controls.Add(clearDiv);
                    }
                }
            }
        }

        protected void InsertInventoryToSitecore()
        {
            if (Request["id"] != null)
            {
                using (new SecurityDisabler())
                {
                   
                    
                        //getStates parent node
                        var homes = new List<Item>();
                        string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' {2}]".FormatWith(
                            Request["id"],
                            SearchType,
                            _includeInactive
                                ? string.Empty
                                : " and @Home for Sale Status='" + SCIDs.StatusIds.Status.Active + "'");
                        homes.AddRange(_materDB.SelectItems(query));

                        IEnumerable<Item> orderedHomes =
                            homes.OrderBy(i => i.Parent.Parent.DisplayName)
                                .ThenBy(i => i.Parent.DisplayName)
                                .ThenBy(i => i.DisplayName)
                                .Skip(PageSize*(_pageNum - 1))
                                .Take(PageSize);
                        foreach (
                            Item home in
                                orderedHomes)
                        {
                           
                            using (new EditContext(home))
                            {
                                //Price
                                int outPrice;
                                string price = "price_" + home.ID.ToShortID();
                                if (int.TryParse(Request.Form[price], out outPrice))
                                    home["Price"] = outPrice.ToString();

                                //sqft
                                int outSqft;
                                string sqft = "sqft_" + home.ID.ToShortID();
                                if (int.TryParse(Request.Form[sqft], out outSqft))
                                    home["Square Footage"] = outSqft.ToString();

                                //Bed
                                string bed = "bed_" + home.ID.ToShortID();
                                home["Number of Bedrooms"] = Request.Form[bed];
                                //Available 
                                string available = "available_" + home.ID.ToShortID();
                                home["Status for Availability"] = Request.Form[available];
                                //bath
                                string bath = "bath_" + home.ID.ToShortID();
                                home["Number of Bathrooms"] = Request.Form[bath];
                                //hBath
                                string hBath = "hBath_" + home.ID.ToShortID();
                                home["Number of Half Bathrooms"] = Request.Form[hBath];
                                //saleStat
                                string saleStat = "saleStat_" + home.ID.ToShortID();
                                home["Home for Sale Status"] = Request.Form[saleStat];
                                //availDate
                                string availDate = "availDate_" + home.ID.ToShortID();
                                DateTime ParsedAvailDate;
                                home["Availability Date"] =
                                    (DateTime.TryParse(Request.Form[availDate], out ParsedAvailDate) &&
                                     ParsedAvailDate != DateTime.MinValue)
                                        ? String.Format("{0:d4}{1:d2}{2:d2}T{3:d2}{4:d2}{5:d2}",
                                            ParsedAvailDate.Year, ParsedAvailDate.Month,
                                            ParsedAvailDate.Day, ParsedAvailDate.Hour,
                                            ParsedAvailDate.Minute, ParsedAvailDate.Second)
                                        : "";

                                //lot
                                string lot = "lot_" + home.ID.ToShortID();
                                home["Lot Number"] = Request.Form[lot];
                                //TmNum
                                string TmNum = "TmNum_" + home.ID.ToShortID();
                                home["myTM Number"] = Request.Form[TmNum];
                                //feat
                                string feat = "feat_" + home.ID.ToShortID();
                                home["Display as Available Home in RWE"] = (Request.Form[feat] == "on") ? "1" : "";
                                //displayHome
                                string displayHome = "displayHome_" + home.ID.ToShortID();
                                home["Display on Homes for Sale Page"] = (Request.Form[displayHome] == "on") ? "1" : "";
                                //displayRep
                                string displayRep = "displayRep_" + home.ID.ToShortID();
                                home["Display on REP Page"] = (Request.Form[displayRep] == "on") ? "1" : "";
                                ////Price
                                string searchSaleStat = "searchSaleStatus_" + home.ID.ToShortID();
                                home["Status For Search"] = (Request.Form[searchSaleStat] == "on")
                                    ? SCIDs.StatusIds.Status.Active
                                    : SCIDs.StatusIds.Status.InActive;
                            }
                        }
                    }
                

                ClearCache();
            }
        }

        protected void GetPlanValues()
        {
            //Database MASTER_DB = Factory.GetDatabase("master")
            using (new SecurityDisabler())
            {
               
                using (new BulkUpdateContext())
                {
                    //getStates parent node
                    var plans = new List<Item>();
                    string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' {2}]".FormatWith(
                        Request["id"],
                        SearchType,
                        _includeInactive ? string.Empty : "and @Plan Status='" + SCIDs.StatusIds.Status.Active + "'");
                    plans.AddRange(_materDB.SelectItems(query));
                    if (_pageContainerItemID != Sitecore.Data.ID.Null)
                    {
                        var currentItem = _materDB.GetItem(_pageContainerItemID);
                        if (currentItem.TemplateID == SCIDs.TemplateIds.DivisionPage)
                        {

                            plans =
                                plans.OrderBy(p => p.Parent.GetItemName())
                                    .ThenBy(p => p["square footage"].CastAs<int>(0))
                                    .ToList();

                        }
                        else
                        {

                            plans = plans.OrderBy(p => p["square footage"].CastAs<int>(0)).ToList();
                        }

                    }

                  

                    NumPages = plans.Count/PageSize;
                    drpPageNum.Items.Clear();
                    for (int i = 0; i <= NumPages; i++)
                    {
                        drpPageNum.Items.Add(new ListItem((i + 1).ToString()));
                    }

                    if (_pageNum > drpPageNum.Items.Count)
                        _pageNum = 1;

                    drpPageNum.SelectedValue = _pageNum.ToString();
                    drpPageNum.Visible = (drpPageNum.Items.Count > 1);
                    lblPageNum.Visible = drpPageNum.Visible;


                    List<Item> bedItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.BedroomCounts).GetChildren().ToList();
                    List<Item> bathItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.BathroomCounts).GetChildren().ToList();
                    List<Item> halfBathItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.HalfBathroomCounts).GetChildren().ToList();
                    List<Item> saleStatusItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.RecordStatus).GetChildren().ToList();
                    List<Item> advertisingStatusItems =
                        _materDB.GetItem(SCIDs.Common.DataFields.AdvertisingStatus).GetChildren().ToList();

                    litHeading.Text =
                        @"
        <div class=""head"">
            <img src=""/images/admin/title2.jpg"" />
        </div>
	        <div class=""rowBreak""></div>";
                    foreach (
                        Item plan in
                            plans.Skip(PageSize*(_pageNum - 1))
                                .Take(
                                    PageSize))
                    {
                        var description = new Label();
                        description.ID = "name_" + plan.ID.ToShortID();
                        description.Text = string.Format("<div class=\"name\">{0}:<b>{1}</b></div>"
                            ,
                            string.IsNullOrWhiteSpace(plan.Parent["Community Name"])
                                ? plan.Parent.DisplayName
                                : plan.Parent["Community Name"]
                            ,
                            string.IsNullOrWhiteSpace(plan["Plan Name"])
                                ? plan.DisplayName
                                : plan["Plan Name"]
                            );
                        form1.Controls.Add(description);

                        var txtPrice = new TextBox();
                        txtPrice.ID = "price_" + plan.ID.ToShortID();
                        txtPrice.Text = plan.Fields["Priced from Value"].ToString();
                        txtPrice.CssClass = "price";
                        form1.Controls.Add(txtPrice);

                        var txtPriceText = new TextBox();
                        txtPriceText.ID = "pricetxt_" + plan.ID.ToShortID();
                        txtPriceText.Text = plan.Fields["Priced from Text"].ToString();
                        txtPriceText.CssClass = "priceTxt";
                        form1.Controls.Add(txtPriceText);

                        var lblSqft = new Label();
                        lblSqft.ID = "sqft_" + plan.ID.ToShortID();
                        lblSqft.Text =
                            "<div class=\"sqft\">{0}sqft.</div>".FormatWith(
                                plan.Fields["Square Footage"].Value.CastAs(0));
                        form1.Controls.Add(lblSqft);

                        var lblBed = new Label();
                        lblBed.ID = "bed_" + plan.ID.ToShortID();
                        lblBed.Text =
                            "<div class=\"bed\">{0}</div>".FormatWith(
                                plan.Fields["Number of Bedrooms"].Value.CastAs(0));
                        form1.Controls.Add(lblBed);


                        var lblBath = new Label();
                        lblBath.ID = "bath_" + plan.ID.ToShortID();
                        lblBath.Text =
                            "<div class=\"bath\">{0}</div>".FormatWith(
                                plan.Fields["Number of Bathrooms"].Value.CastAs(0));
                        form1.Controls.Add(lblBath);


                        var lblhBath = new Label();
                        lblhBath.ID = "hBath_" + plan.ID.ToShortID();
                        lblhBath.Text =
                            "<div class=\"hBath\">{0}</div>".FormatWith(
                                plan.Fields["Number of Half Bathrooms"].Value.CastAs(0));
                        form1.Controls.Add(lblhBath);

                        var AdvertisingStatus =
                            new DropDownList();
                        advertisingStatusItems.ForEach(
                            X => AdvertisingStatus.Items.Add(new ListItem(X["Name"].ToString(), X.ID.ToString())));
                        AdvertisingStatus.ID = "adStat_" + plan.ID.ToShortID();
                        AdvertisingStatus.SelectedValue = plan.Fields["Status for Advertising"].ToString();
                        AdvertisingStatus.CssClass = "adStatus input";
                        form1.Controls.Add(AdvertisingStatus);

                        var searchSaleStatus =
                            new DropDownList();
                        saleStatusItems.ForEach(
                            X => searchSaleStatus.Items.Add(new ListItem(X["Name"].ToString(), X.ID.ToString())));
                        searchSaleStatus.ID = "searchSaleStatus_" + plan.ID.ToShortID();
                        if (null != plan.Fields["Status For Search"])
                            searchSaleStatus.SelectedValue = plan.Fields["Status For Search"].ToString();
                        searchSaleStatus.CssClass = "searchStatus input";
                        form1.Controls.Add(searchSaleStatus);

                        var clearDiv =
                            new HtmlGenericControl("div");
                        clearDiv.Attributes["class"] = "rowBreak";
                        form1.Controls.Add(clearDiv);
                    }
                }
            }
        }

        protected void InsertPlanToSitecore()
        {
            if (Request["id"] != null)
            {
                using (new SecurityDisabler())
                {
                   
                        //getStates parent node
                        var plans = new List<Item>();
                        string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' {2}]".FormatWith(
                            Request["id"],
                            SearchType,
                            _includeInactive ? string.Empty : "and @Plan Status='" + SCIDs.StatusIds.Status.Active + "'");
                        plans.AddRange(_materDB.SelectItems(query));

                        IEnumerable<Item> orderedPlans =
                            plans.OrderBy(i => i.Parent.DisplayName)
                                .ThenBy(i => i.DisplayName)
                                .Skip(PageSize*(_pageNum - 1))
                                .Take(PageSize);
                        foreach (
                            Item plan in
                                orderedPlans)
                        {
                            using (new EditContext(plan))
                            {
                                //Price
                                int outPrice;
                                string price = "price_" + plan.ID.ToShortID();
                                if (int.TryParse(Request.Form[price], out outPrice))
                                    plan["Priced from Value"] = outPrice.ToString();

                                string priceTxt = "pricetxt_" + plan.ID.ToShortID();
                                plan["Priced from Text"] = Request.Form[priceTxt];

                                //sqft
                                //int outSqft;
                                //string sqft = "sqft_" + plan.ID.ToShortID().ToString();
                                //if (int.TryParse(Request.Form[sqft], out outSqft))
                                //    plan["Square Footage"] = outSqft.ToString();

                                //adStat
                                string adStat = "adStat_" + plan.ID.ToShortID();
                                plan["Status for Advertising"] = Request.Form[adStat];
                                //searchSaleStatus
                                string searchSaleStatus = "searchSaleStatus_" + plan.ID.ToShortID();
                                plan["Status For Search"] = Request.Form[searchSaleStatus];
                            }
                        }
                   
                }
                ClearCache();
            }
        }

        /// <summary>
        ///     Refreshes the screen.
        /// </summary>
        /// <param name="currentItem">The current item.</param>
        private void ClearCache()
        {
            //todo: selecting which cache to clear might speed up performance and overall site load time
            //I have to clear cache because the bulk update above does not reflect in the content editor
            //until the user logs out and back in.
            foreach (Cache cache in CacheManager.GetAllCaches())
                cache.Clear();
        }
    }
}