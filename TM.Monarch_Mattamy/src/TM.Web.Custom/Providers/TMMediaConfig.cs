﻿using System.Web;
using System.Xml;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;

namespace TM.Web.Custom.Providers
{
    /// <summary>
    /// original source from: https://github.com/getfishtank/Fishtank.MediaResolverPatch
    /// Intercept the media request handler and apply custom caching to certain media files
    /// based on their extension and configured in GlobalConfiguration.MediaExtensionsNotToClientCache
    /// **Modified 2014-06-16 - James Gusa
    /// This code base is taken from Sitecore.Kernel.DLL and altered so that media library requests aren't encoded. 
    /// 
    /// Sitecore Patch Note:
    /// When rendering media URLs, the system did not use the configuration in the encodeNameReplacements section to replace special characters in the URLs. 
    /// This has been fixed so that media URLs also use the encodeNameReplacements configuration. (323105, 314977)
    /// 
    /// </summary>
    
    public class TMMediaConfig : MediaConfig
    {
        public TMMediaConfig() : base()
        {
        }
        public TMMediaConfig(XmlNode configNode) : base(configNode)
        {
        }
        public override MediaRequest ConstructMediaRequestInstance(HttpRequest httpRequest)
        {
            Assert.ArgumentNotNull((object)httpRequest, "httpRequest");
            TMMediaRequest mediaRequest = new TMMediaRequest();
            if (mediaRequest == null)
                return (MediaRequest)null;
            mediaRequest.Initialize(httpRequest);
            return (MediaRequest)mediaRequest;
        }

    }

}
