﻿//-----------------------------------------------------------------------------------
// <copyright file="RenamingAction.cs" company="Sitecore Shared Source">
// Copyright (c) Sitecore.  All rights reserved.
// </copyright>
// <summary>
// Defines the Sitecore.Sharedsource.Rules.Actions.Naming.RenamingAction type.
// </summary>
// <license>
// http://sdn.sitecore.net/Resources/Shared%20Source/Shared%20Source%20License.aspx
// </license>
// <url>http://trac.sitecore.net/ItemNamingRules/</url>
//-----------------------------------------------------------------------------------

using Sitecore;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;

namespace TM.Web.Custom.SitecoreRules.Actions.Naming
{
  using System;

  /// <summary>
  /// Base class for rules engine actions that rename items.
  /// </summary>
  /// <typeparam name="T">Type providing rule context.</typeparam>
  public abstract class RenamingAction<T> : 
    Sitecore.Rules.Actions.RuleAction<T> 
    where T : Sitecore.Rules.RuleContext
  {
      /// <summary>
      /// Rename the item, unless it is a standard values item 
      /// or the start item for any of the managed Web sites.
      /// </summary>
      /// <param name="item">The item to rename.</param>
      /// <param name="newName">The new name for the item.</param>
      /// <param name="renameDisplayName">change display name too? </param>
      protected void RenameItem(
      Sitecore.Data.Items.Item item,
      string newName,bool renameDisplayName=false)
    {
      if (item.Template.StandardValues != null
        && item.ID == item.Template.StandardValues.ID)
      {
        return;
      }
      
      foreach (Sitecore.Web.SiteInfo site in Sitecore.Configuration.Factory.GetSiteInfoList())
      {
        if (String.Compare(site.RootPath + site.StartItem, item.Paths.FullPath, true) == 0)
        {
          return;
        }
      }

      if (Sitecore.Context.Job != null && Sitecore.Context.Job.Category == "publish")
      {
          return;
      } 


      using (new SecurityDisabler())
      using (new EventDisabler())
      using (new EditContext(item))
      {
          item.Name = newName;
          if (renameDisplayName)
              item.Appearance.DisplayName = newName;
      }
     
          RefreshScreen(item);
     
          
    }

      /// <summary>
      /// Refreshes the screen.
      /// </summary>
      /// <param name="currentItem">The current item.</param>
      private void RefreshScreen(Item currentItem)
      {
          if (Sitecore.Context.ClientPage != null)
          {
              if (currentItem.Parent != null)
              {
                  if (!currentItem.Editing.IsEditing)
                  {
                      if (Context.ContentDatabase != null)
                      {
                          Sitecore.Context.ContentDatabase.Caches.DataCache.RemoveItemInformation(currentItem.ID);
                          Sitecore.Context.ContentDatabase.Caches.ItemCache.RemoveItem(currentItem.ID);
                      }
                      var message = String.Format("item:refreshchildren(id={0})", currentItem.Parent.ID);
                       Sitecore.Context.ClientPage.SendMessage(this, message);
                   }
              }
          }
      }

      
  }
}
