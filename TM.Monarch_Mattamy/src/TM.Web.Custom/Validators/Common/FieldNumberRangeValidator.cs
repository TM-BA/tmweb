﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;
using Sitecore.Data.Validators;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
namespace TM.Web.Custom.Validators.Common
{
    [Serializable]
    public class FieldNumberRangeValidator : StandardValidator
    {
        public FieldNumberRangeValidator() { }
        public FieldNumberRangeValidator(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public override string Name { get { return ("Field Number"); } }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return (GetFailedResult(ValidatorResult.FatalError));
        }
        protected override ValidatorResult Evaluate()
        {
            float max = Sitecore.MainUtil.GetFloat(base.Parameters["max"], float.MaxValue);
            float min = Sitecore.MainUtil.GetFloat(base.Parameters["min"], float.MinValue);
            int oValue;

            if (int.TryParse(base.ControlValidationValue, out oValue))
            {
                if ((oValue < max) && (oValue > min))
                {
                    return ValidatorResult.Valid;
                }
                else
                {
                    base.Text = "Numbers in {0} need to be between {1} and {2}".FormatWith(base.GetFieldDisplayName(),min,max);

                    return ValidatorResult.CriticalError;
                }
            }
            return ValidatorResult.Valid;
        }
    }
}