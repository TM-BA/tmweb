﻿NS("TM.Scripts.PlanPages");

TM.Scripts.PlanPages = function() {
};

TM.Scripts.PlanPages.prototype = {

    loadImage: function (img, stop) {

        var $img = $j(img);
        var src = $img.attr("src");
        var li = $img.parent();
        src = src.substr(0, src.indexOf('?'));
        var mainImg = $j("#main-img");
        var imgTitle = $j("#imgTitle");
        var alt = $img.attr("alt");

        mainImg.attr("src", src);
        mainImg.attr("alt", alt);
        mainImg.attr("title", alt);
        imgTitle.html(alt);
        mainImg.fadeIn(800);

        var allLis = li.parent().children();
        $j.each(allLis, function (idx, val) {
            $j(val).removeClass("on");
        });
        li.addClass("on");
        var index = li.index();
       
        //Removes the click on next/previous buttons and change the arrows for disabled arrows
        if (this.$carousel.hasNext()) {
            
            var btn = $j(".next-arrow-disable");
            if (btn.length == 0) {
                btn = $j(".next-arrow");
            }
            btn.unbind("click");
            btn.bind("click", (function () {
                $j('.slides-next').click();
                return false;
            }));
            btn.removeClass('next-arrow-disable').addClass('next-arrow');
        } else {
            
            var btn = $j(".next-arrow");
            if (btn.length == 0) {
                btn = $j(".next-arrow-disable");
            }
            btn.unbind("click");
            btn.removeClass('next-arrow').addClass('next-arrow-disable');
        }

        if (this.$carousel.hasPrevious()) {
            
            var btn = $j(".prev-arrow-disable");
            if (btn.length == 0) {
                btn = $j(".prev-arrow");
            }
            btn.unbind("click");
            btn.bind("click", (function () {
                $j('.slides-prev').click();
                return false;
            }));

            btn.removeClass('prev-arrow-disable').addClass('prev-arrow');
        } else {
            
            var btn = $j(".prev-arrow");
            if (btn.length == 0) {
                btn = $j(".prev-arrow-disable");
            }
            btn.unbind("click");
            btn.removeClass('prev-arrow').addClass('prev-arrow-disable');
        }

        //fix to make the carousel index to point to the image that was clicked, this will fix the issue when clickin on next/previous to actually go to the next image after viewing an image
        if (index != null && stop) {
            if ($j(".prev-arrow").length > 0 || $j(".prev-arrow-disable").length > 0) {
                this.$carousel.to(index);
            }
        }

        if (stop) {
            this.$carousel.stop();
        }
    },


    onSlideTransitionComplete: function (thisCache) {
        var c = thisCache.$carousel;
        if (c != null && c.$items.length > 0) {
            var idx = (typeof c.current != 'undefined' && c.current != null) ? c.current : 0;
            var img = $j(c.$items[idx]).children()[0];
            thisCache.loadImage(img);
        }
    },

    $carousel: null,

    loadSlides: function () {
        if ($j("li.slide").length == 1) {
            $j("li.slide").hide();
            this.loadImage($j("li.slide").children()[0]);

        }
        else {
            var planRotatorOptions = TM.Rotator.get_slideOptions();
            var thisCache = this;
            planRotatorOptions.oncomplete = function () { thisCache.onSlideTransitionComplete(thisCache); };
            planRotatorOptions.animate = true;


            var slides = $j('.slideshow').slides(planRotatorOptions);

            if ($j("lide.slide").length != 1) {
                $j("li.slide").show();
            }

            this.$carousel = slides.data('slides');
        }
    },

    init: function () {

        var thisCache = this;

        $j(document).ready(function () {
            var href = window.document.location.href;
            var tab = href.substr(href.lastIndexOf('/') + 1);
            switch (tab) {
                case "floorplans":
                    $j("li.pd1").addClass("on");
                    break;
                case "photos":
                    $j("li.pd2").addClass("on");
                    break;
                case "locations":
                    $j("li.pd3").addClass("on");
                    break;
                case "options":
                    $j("li.pd4").addClass("on");
                    break;
                default:
                    $j("li.pd1").addClass("on");
                    break;
            }

            thisCache.loadSlides();

            //Stops the thumbnail slides to rotate
            thisCache.$carousel.stop();


        });

    }

};


var planScripts = new TM.Scripts.PlanPages();
planScripts.init();