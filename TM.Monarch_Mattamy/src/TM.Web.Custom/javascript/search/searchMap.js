﻿var infowindow;
var map;
NS("TM.SearchMap");

function ZoomMap(lat, lng) {
    var latLng = new google.maps.LatLng(lat, lng);
    map.setCenter(latLng);
  //  map.setZoom(15);

    $j('html, body').animate({
        scrollTop: $j("#searchmap").offset().top
    }, 'slow');
}


//Single Point

function InitializeDefaultMap(lat, lng, zoom) {
    var latLng = new google.maps.LatLng(lat, lng);
    var myOptions = {
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latLng,
        zoom: zoom
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}

//Bounds

function initializeMap(communities) {

    var tmmap = new TM.SearchMap(communities);
    tmmap.execute();
}

TM.SearchMap = function (communities) {

    this.reset();

    this._communities = communities;


    var myOptions = {
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: 9,
        scrollwheel: false

    };



    var mapElement = document.getElementById("map_canvas");
    map = new google.maps.Map(mapElement, myOptions);
    this.Defaultap = map;
    if (screen.width <= 579) {
        map.setOptions({ draggable: false });
    }
};



TM.SearchMap.prototype = {
    execute: function () {

        this.createMarkers();
        var styles = [{
            url: "/images/mappins/" + this._communities[0].CompanyNameAbbreviation + ".png",
            height: 55,
            width: 55,
            anchor: [32, 0],
            textColor: '#ffffff',
            textSize: 12
        }];
        var clusterOptions = { zoomOnClick: false, gridSize: 56, styles: styles, title: "Multiple communities available. Click to see more", minimumClusterSize: 20 };
        /*see markercluster.js */
        TM.SearchMap._markerClusterer = new MarkerClusterer(map, TM.SearchMap._markers, clusterOptions);
        TM.SearchMap._markerClusterer.fitMapToMarkers();
        this.attachEvents();

    },

    createMarkers: function () {
        var thisCache = this;
        var communities = this._communities;
        $j.each(communities, function (index, community) {
            thisCache.createMarker(community);
        });
    },


    createMarker: function (community) {

        var latlng = new google.maps.LatLng(community.CommunityLatitude, community.CommunityLongitude);
        var title = community.Name;
        var that = this;
        var newmarker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: (community.MapPinStatus != "DesignCenter") ? title : community.CommunityName,
            content: (community.MapPinStatus != "DesignCenter") ? that.makeFlyout(community.CommunityFlyouts, community.Name) : this.makeDesignCenterFlyout(community),
            icon: community.DynamicMapIcon,
            animation: google.maps.Animation.DROP
            // zIndex: Math.round(latlng.lat() * -100000) << 5
        });

        google.maps.event.addListener(newmarker, "click", function () {
            if (infowindow)
                infowindow.close();
            infowindow = new google.maps.InfoWindow(
                {
                    content: newmarker.content
                });
            infowindow.open(map, newmarker);
        });

        TM.SearchMap._markers.push(newmarker);


    },

    makeFlyout: function (flyout, name) {
        var obj = $j.parseJSON(flyout);
        var content = "<div id=\"content\">" +
            "<div>" +
                "<a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href='{0}' {3}><img src='{1}' alt='{2}'></a>" +
                    "</div>" +
                        "<a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href=\"{0}\" {3} class=\"hdr\">{2}</a>" +
                            "<div>{4}</div>" +
                                "</div>";
        content = String.format(content, obj.url, obj.imgsrc, name, obj.target, obj.facet);

        return content;

    },

    makeDesignCenterFlyout: function (community) {
        var content = "<div id=\"content\"><strong>" +
		     community.CommunityName+"</strong><br/>"+
		     community.StreetAddress1 + "<br/>" +
                     community.City + ", " + community.StateProvinceAbbreviation + " " + community.ZipPostalCode + "<br/>" +
                     community.Phone + "<br/><br/>" +
                     community.OfficeHoursDay
                         + "</div>" ;

        return content;
    },


    getInfoWindowContent: function (markers) {
        var contentDetails = '';
        for (var i = 0; i < markers.length; i++) {
            var marker = markers[i];
            if (typeof marker != "undefined") {
                var flyOut = $j(marker.content);
                var communityNumber = $j('<div>').append($j("<img>").attr("src", marker.icon).css({ "width": "19px", "height": "24px" }).clone()).html();
                // var a = (marker.MapPinStatus.toLowerCase() == "designcenter") ? "<b>" + marker.title + "</b>" : $j('<div>').append(flyOut.find("a.hdr").clone()).html();
                var a = "";
                a = $j('<div>').append(flyOut.find("a.hdr").css({ "vertical-align": "top" }).clone()).html();
                //design center;
                if (a === "") { a = "<span style='vertical-align:top'>" + marker.content + "</span>"; }
                var li = "<li style='list-style-type:none'>" + communityNumber + a + "</li>";
                contentDetails += li;
            }

        }
        return contentDetails;
    },

    attachEvents: function () {
        var that = this;
        // Listen for a cluster to be clicked
        google.maps.event.addListener(TM.SearchMap._markerClusterer, 'click', function (cluster) {
            var content = "<div style='width:290px'" + that.getInfoWindowContent(cluster.getMarkers()) + "</div>";
            // Convert lat/long from cluster object to a usable MVCObject
            var info = new google.maps.MVCObject;
            info.set('position', cluster.center_);
            if (infowindow)
                infowindow.close();
            infowindow = new google.maps.InfoWindow();
            infowindow.close();
            infowindow.setOptions({ maxWidth: 300 });
            infowindow.setContent(content);
            infowindow.open(map, info);

        });
    },

    reset: function () {
        TM.SearchMap._markers = [];

        if (TM.SearchMap._markerClusterer) {
            TM.SearchMap._markerClusterer.clearMarkers();
        }
    }
};




function GetCommunityURLs(isRealtor, communityDetailsUrl, communityId) {
    if (isRealtor)
        return "/Realtor Home for Sale Search/Realtor Home for Search Results?cid=" + communityId;
    else
        return communityDetailsUrl ;
}


