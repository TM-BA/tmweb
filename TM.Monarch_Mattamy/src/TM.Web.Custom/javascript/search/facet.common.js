﻿function SetSearchFacet(mode) {
	if (mode === undefined)
		mode = "Multi";
	Show(mode);
	SetDefaultValues();
}

function SetDefaultValues() {
	var conts = ["selectcity", "selectAvailability", "selectSchoolDistrict", "selectBonusRoomDen"];

	for (var value in conts) {
		$j("#" + conts[value]).html("");
		$j("#" + conts[value]).append($j("<option></option>").val("0").html("No preference"));
	}
}

function Show(mode) {
	if (mode == "Multi") {//multi mode
		MultiMode("none");
		HomeforSaleMode("block");
	}
	else { //home for sale mode
		MultiMode("block");
		HomeforSaleMode("none");
	}
}

function MultiMode(status) {
	$j("div#selectAreaBox").css("display", status);
}

function HomeforSaleMode(status) {
	$j("div#selectCommunityStatusBox").css("display", status);
}


//function SetSliderValues(minPr, maxPr, selminPr, selmaxPr, prTick, minSf, maxSf, selminSf, selmaxSf, sqftTick) {
//	$j("#sldrrange-StartingPrice").slider({
//		range: true,
//		min: minPr,
//		max: maxPr,
//		step: prTick,
//		animate: 'true',
//		values: [selminPr, selmaxPr],
//		stop: function (event, ui) {
//			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
//			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
//			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
//			BindSearchFacet();
//		}
//	});
//	$j("#startingprice").html("[ $" + $j("#sldrrange-StartingPrice").slider("values", 0).formatMoney(0, ".", ",") + " - $" + $j("#sldrrange-StartingPrice").slider("values", 1).formatMoney(0, ".", ",") + " ]");

//	if (minPr == 0 & maxPr == 0) {
//		$j("div#sdrpriceRange").css("display", "none");
//	}else{
//		$j("div#sdrpriceRange").css("display", "block");
//	}

//	$j("#sldrrange-SquareFeet").slider({
//		range: true,
//		min: minSf,
//		max: maxSf,
//		animate: 'true',
//		step: sqftTick,
//		values: [selminSf, selmaxSf],
//		stop: function (event, ui) {
//			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
//			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
//			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
//			BindSearchFacet();
//		}
//	});
//	$j("#squarefeet").html("[ " + $j("#sldrrange-SquareFeet").slider("values", 0).formatMoney(0, ".", ",") + " - " + $j("#sldrrange-SquareFeet").slider("values", 1).formatMoney(0, ".", ",") + " ]");
//	
//	if (minSf == 0 & maxSf == 0) {
//		$j("div#sdrSqftRange").css("display", "none");
//	}
//	else {
//		$j("div#sdrSqftRange").css("display", "block");
//	}
//}

function SetDropDownListByValue(parsedval, control) {
	$j("#" + control).html("");
	$j("#" + control).append($j("<option></option>").val("0").html("No preference"));
	if (parsedval != null && parsedval.length > 0) {
		$j.each(parsedval, function (i, cty) {
			if (cty.ItemStatus == "On") {
				if (cty.IsSelected == true)
					$j("#" + control).append($j("<option selected></option>").val(cty.Name).html(cty.Name));
				else
					$j("#" + control).append($j("<option class='available'></option>").val(cty.Name).html(cty.Name));
			} else if (cty.ItemStatus == "Off")
				$j("#" + control).append($j("<option disabled class='disabled'></option>").val(cty.Name).html(cty.Name));
		});
		UnLockControl(control);
	} else {
		LockControl(control);
	}
}


function SetDropDownListByNameValue(parsedval, control) {
	$j("#" + control).html("");
	$j("#" + control).append($j("<option></option>").val("0").html("No preference"));
	if (parsedval != null && parsedval.length > 0) {
		$j.each(parsedval, function (i, cty) {
			if (cty.ItemStatus == "On") {
				if (cty.IsSelected == true)
					$j("#" + control).append($j("<option selected></option>").val(cty.Value).html(cty.Name));
				else
					$j("#" + control).append($j("<option class='available'></option>").val(cty.Value).html(cty.Name));
			} else if (cty.ItemStatus == "Off")
				$j("#" + control).append($j("<option disabled class='disabled'></option>").val(cty.Value).html(cty.Name));
		});
		UnLockControl(control);
	} else {
		LockControl(control);
	}
}

function SetDropDownListById(parsedval, control) {
	$j("#" + control).html("");
	$j("#" + control).append($j("<option></option>").val("0").html("No preference"));
	if (parsedval != null && parsedval.length > 0) {
		$j.each(parsedval, function(i, cty) {
			if (cty.ItemStatus == "On") {
				if (cty.IsSelected == true)
					$j("#" + control).append($j("<option selected></option>").val("{" + cty.ID.Guid + "}").html(cty.Name));
				else
					$j("#" + control).append($j("<option class='available'></option>").val("{" + cty.ID.Guid + "}").html(cty.Name));
			} else if (cty.ItemStatus == "Off")
				$j("#" + control).append($j("<option disabled class='disabled'></option>").val("{" + cty.ID.Guid + "}").html(cty.Name));
		});
		UnLockControl(control);
	} else {
		LockControl(control);
	}
}

function UnLockControl(control) {
	$j("#" + control).removeAttr("disabled");
	$j("#" + control).removeAttr("style");
	ShowControl(control);
}

function LockControl(control) {
	$j("#" + control).attr("disabled", "");
	$j("#" + control).attr("style", "background:#dcdcdc; border:1px solid #999");
	HideControl(control);
}


function HideControl(control) {
	$j("div#" + control + "Box").css("display", "none");
}

function ShowControl(control) {
	$j("div#" + control + "Box").css("display", "block");
}


function SetMultiSelectLabels(parsedval, control) {
	$j("#" + control).html("");
	var i;
	if (parsedval != null && parsedval.length > 0) {
		for (i = 0; i <= parsedval.length - 1; ++i) {
			if (i == 6) {
				if (parsedval[i].ItemStatus == "On") {
					if (parsedval[i].IsSelected == true)
						$j("#" + control).append($j("<li><label class='available' onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "_plus'>" + parsedval[i].Value + "+</label></li>"));
					else
						$j("#" + control).append($j("<li><label onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "_plus'>" + parsedval[i].Value + "+</label></li>"));

				} else if (parsedval[i].ItemStatus == "Off") {
					$j("#" + control).append($j("<li><span class='readonly' id='" + control + "_" + parsedval[i].Value + "_plus'>" + parsedval[i].Value + "+</span></li>"));
				}
				break;
			}

			if (parsedval[i].ItemStatus == "On") {
				if (parsedval[i].IsSelected == true)
					$j("#" + control).append($j("<li><label class='available' onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</label></li>"));
				else
					$j("#" + control).append($j("<li><label onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</label></li>"));

			} else if (parsedval[i].ItemStatus == "Off") {
				$j("#" + control).append($j("<li><span class='readonly' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</span></li>"));
			}
		}
		ShowControl(control);
	} else {
		HideControl(control);
	}
}

function SetMultiSelectLabelswithHalf(parsedval, parsedHalfval, control) {
	$j("#" + control).html("");
	var i;
	var half = "½";
	if (parsedval != null && parsedval.length > 0) {
		for (i = 0; i <= parsedval.length -1; ++i) {
			if (parsedHalfval.length > 0 && i == 4) {
				if (parsedval[i].ItemStatus == "On") {
					if (parsedval[i].IsSelected == true) {
						$j("#" + control).append($j("<li><label class='available' onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "+</label></li>"));
					} else {
						$j("#" + control).append($j("<li><label onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "+</label></li>"));
					}
				} else if (parsedval[i].ItemStatus == "Off") {
					$j("#" + control).append($j("<li><span class='readonly' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "+</span></li>"));
				}
				break;
			}

			if (parsedval[i].ItemStatus == "On") {
				if (parsedval[i].IsSelected == true) {
					$j("#" + control).append($j("<li><label class='available' onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</label></li>"));
					if (parsedHalfval.length > 0) {
						$j("#" + control).append($j("<li><label class='available' onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "half" + "'>" + parsedval[i].Value + half + "</label></li>"));
					}
				} else {
					$j("#" + control).append($j("<li><label onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</label></li>"));
					if (parsedHalfval.length > 0) {
						$j("#" + control).append($j("<li><label onClick='" + control + "(this)' id='" + control + "_" + parsedval[i].Value + "half" + "'>" + parsedval[i].Value + half + "</label></li>"));
					}
				}
			} else if (parsedval[i].ItemStatus == "Off") {
				$j("#" + control).append($j("<li><span class='readonly' id='" + control + "_" + parsedval[i].Value + "'>" + parsedval[i].Value + "</span></li>"));
				if (parsedHalfval.length > 0) {
					$j("#" + control).append($j("<li><span class='readonly' id='" + control + "_" + parsedval[i].Value + "half" + "'>" + parsedval[i].Value + half + "</span></li>"));
				}
			}
		}
		ShowControl(control);
	} else {
		HideControl(control);
	}
}

function onFailSearch(request, status, error) {
	alert(error + ": 2" + request.responseText);
}
