﻿var parsedsfinfo = "";
var Beds = [];
var Baths = [];
var Garages = [];
var Stories = [];
var parsedMapResponse = "";
var parsedSearchFacets = "";
var parsedMapBounds = "";
var parsedCommunityinfo = "";
var parsedComminfo = "";
var isRealtor = true;

function GetRealtorCommunities() {
	InitializeDefaultMap("40.111689", "-95.888672", 4); //center of USA
}


function BoundFacetValues(diviId, isClearSession) {
	var searchInfo = { "searchLocation": diviId, "sortBy": 1, "favorites": "", "isRealtor": true, "isClearSession": isClearSession };
	$j('.mapprogress').show();
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetSearchResults",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedSearchResponse = JSON.parse(data.d);
			parsedMapBounds = parsedSearchResponse.MapBounds;
			parsedSearchFacets = parsedSearchResponse.SearchFacet;
			parsedCommunityinfo = parsedSearchResponse.CommunityInfo;

			if (parsedMapBounds != null) {
				if (parsedMapBounds.MinLatitude !== undefined && parsedMapBounds.MaxLongitude !== undefined && parsedMapBounds.MaxLatitude !== undefined && parsedMapBounds.MinLongitude !== undefined)
					initializeMap(parsedCommunityinfo, parsedMapBounds.MinLatitude, parsedMapBounds.MaxLongitude, parsedMapBounds.MaxLatitude, parsedMapBounds.MinLongitude, isRealtor);
				$j('.mapprogress').hide();

			}
			else {
				var defaultMap = parsedSearchResponse.DefaultMapPoint;
				InitializeDefaultMap(defaultMap.Latitude, defaultMap.Longitude, 4);
			}
			$j('.mapprogress').hide();
			BoundRealtorSearchFacet(parsedSearchFacets, isRealtor);
//			alert(parsedSearchFacets.Division);
//			if (parsedSearchFacets.Division != undefined && parsedSearchFacets.Division != "0" && parsedSearchFacets.Division != "")
//				$j("#realtorfacet").show();
//			else
//				$j("#realtorfacet").hide();

		}
	});
}

function BindRealtorSearchFacet() {
	var searchInfo = GetFacetValues();
	$j('.facetprogress').show();
	$j('.mapprogress').show();
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/RefineSearchByFacet",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedSearchResponse = JSON.parse(data.d);
			parsedMapBounds = parsedSearchResponse.MapBounds;
			parsedSearchFacets = parsedSearchResponse.SearchFacet;
			parsedCommunityinfo = parsedSearchResponse.CommunityInfo;

			$j("#realtorfacet").show();
			
			if (parsedMapBounds != null) {
				if (parsedMapBounds.MinLatitude !== undefined && parsedMapBounds.MaxLongitude !== undefined && parsedMapBounds.MaxLatitude !== undefined && parsedMapBounds.MinLongitude !== undefined)
					initializeMap(parsedCommunityinfo, parsedMapBounds.MinLatitude, parsedMapBounds.MaxLongitude, parsedMapBounds.MaxLatitude, parsedMapBounds.MinLongitude, isRealtor);
				$j('.mapprogress').hide();
			}
			else {
				var defaultMap = parsedSearchResponse.DefaultMapPoint;
				InitializeDefaultMap(defaultMap.Latitude, defaultMap.Longitude, 4);
			}
			$j('.mapprogress').hide();
			
			BoundRealtorSearchFacet(parsedSearchFacets, isRealtor);
			$j('.facetprogress').hide();
		}
	});
}

function SetSliderValues(minPr, maxPr, selminPr, selmaxPr, prTick, minSf, maxSf, selminSf, selmaxSf, sqftTick) {
	$j("#sldrrange-StartingPrice").slider({
		range: true,
		min: minPr,
		max: maxPr,
		step: prTick,
		animate: 'true',
		values: [selminPr, selmaxPr],
		slide: function (event, ui) {
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
			BindRealtorSearchFacet();
		}
	});
	$j("#startingprice").html("[ $" + $j("#sldrrange-StartingPrice").slider("values", 0).formatMoney(0, ".", ",") + " - $" + $j("#sldrrange-StartingPrice").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minPr == 0 & maxPr == 0) {
		$j("div#sdrpriceRange").css("display", "none");
	} else {
		$j("div#sdrpriceRange").css("display", "block");
	}

	$j("#sldrrange-SquareFeet").slider({
		range: true,
		min: minSf,
		max: maxSf,
		animate: 'true',
		step: sqftTick,
		values: [selminSf, selmaxSf],
		slide: function (event, ui) {
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
			BindRealtorSearchFacet();
		}
	});
	$j("#squarefeet").html("[ " + $j("#sldrrange-SquareFeet").slider("values", 0).formatMoney(0, ".", ",") + " - " + $j("#sldrrange-SquareFeet").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minSf == 0 & maxSf == 0) {
		$j("div#sdrSqftRange").css("display", "none");
	}
	else {
		$j("div#sdrSqftRange").css("display", "block");
	}
}


function BoundRealtorSearchFacet(parsedVal, isRealtor) {
	var minPrice = parsedVal.MinPrice;
	var maxPrice = parsedVal.MaxPrice;
	var selminPrice = parsedVal.SelectedMinPrice;
	var selmaxPrice = parsedVal.SelectedMaxPrice;
	var priceTick = parsedVal.PriceTick;
	var minSqft = parsedVal.MinSqFt;
	var maxSqft = parsedVal.MaxSqFt;
	var selminSqft = parsedVal.SelectedMinSqFt;
	var selmaxSqft = parsedVal.SelectedMaxSqFt;
	var sqftTick = parsedVal.SqFtTick;
//	var division = parsedVal.Division;

//	if (division != "") {
//		var hnddivi = $j("#hnddivision").val();
//		$j("#" + hnddivi).val(division);
//	}

	SetSliderValues(minPrice, maxPrice, selminPrice, selmaxPrice, priceTick, minSqft, maxSqft, selminSqft, selmaxSqft, sqftTick);

	//cities
	if (isRealtor == false) {
		SetDropDownListByValue(parsedVal.Cities, "selectcity");
	}
	SetDropDownListById(parsedVal.SchoolDistricts, "selectSchoolDistrict");
	SetDropDownListById(parsedVal.CommunityStatus, "selectCommunityStatus");
	SetDropDownListById(parsedVal.Availability, "selectAvailability");
	SetDropDownListByNameValue(parsedVal.BonusRoomsDensSolarium, "selectBonusRoomDen"); 

	SetMultiSelectLabels(parsedVal.NumberofGarages, "lblGarages");
	SetMultiSelectLabels(parsedVal.NumberofBedrooms, "lblBeds");
	//SetMultiSelectLabelswithHalf(parsedVal.NumberofBathrooms, parsedVal.NumberofHalfBathrooms, "lblBaths");
	SetMultiSelectLabels(parsedVal.NumberofBathrooms, "lblBaths");
	SetMultiSelectLabels(parsedVal.NumberofStories, "lblStories");
}


function lblBeds(ctrl) {
	$j(ctrl).addClass("selected");
	Beds.push($j(ctrl).text());
	BindRealtorSearchFacet();
}

function lblBaths(ctrl) {
	$j(ctrl).addClass("selected");
	Baths.push($j(ctrl).text());
	BindRealtorSearchFacet();
}

function lblGarages(ctrl) {
	$j(ctrl).addClass("selected");
	Garages.push($j(ctrl).text());
	BindRealtorSearchFacet();
}

function lblStories(ctrl) {
	$j(ctrl).addClass("selected");
	Stories.push($j(ctrl).text());
	BindRealtorSearchFacet();
}


function GetFacetValues() {
	var hndsearchPath = $j("#hndsearchPath").val();

	var hnddivi = $j("#hnddivision").val();
	var area = $j("#" + hnddivi).val();
	if (area == null)
		area = "";

	var city = $j("#selectcity").val();
	var searchsortby = $j("#searchsortby").val();
	if (searchsortby === undefined)
		searchsortby = "1";

	var price = $j("#startingprice").text();
	var sqft = $j("#squarefeet").text();
	var schooldt = $j("#selectSchoolDistrict").val();
	var comStatus = $j("#selectCommunityStatus").val();
	var availability = $j("#selectAvailability").val();
	var bonusDenSolar = $j("#selectBonusRoomDen").val();


	return { "isRealtor": true, "searchPath": hndsearchPath, "favi": "", "sortby": searchsortby, "area": area, "city": city, "stEdPrices": price, "stEdSqft": sqft, "schooldt": schooldt, "comStatus": comStatus, "beds": Beds, "baths": Baths, "garages": Garages, "stories": Stories, "availability": availability, "bonusDenSolar": bonusDenSolar };
}

function ClearSearchFacet(val) {
	BoundFacetValues(val, true);
}


function GetCitiesByDivision(val) {
	var searchInfo = { "areaId": val };
	$j('.facetprogress').show();
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetCitiesByDivision",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedval = JSON.parse(data.d);
			SetDropDownListById(parsedval, "selectcity");
			//BindSearchFacet();
			BoundFacetValues(val, false);
			$j("#realtorfacet").show();
			$j('.facetprogress').hide();
		}
	});
}

