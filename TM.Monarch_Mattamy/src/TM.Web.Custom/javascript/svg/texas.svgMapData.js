﻿var parsedSVGResponse = null;
var parsedTaMoUSStates = null;
var TaMoUSStates = null;
var parsedTaMoCANUSStateMarkets = null;
var TaMoCANUSStateMarkets = null;

function GetTexasSvgData() {
    $j.ajax({
        type: "POST",
        data: "",
        url: "/services/SearchMethods.asmx/GetTexasSvgData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: onSuccessSVG
    });
}


function onFailSVG(request, status, error) {
    //alert("Failed SVG Input " + error.Message);
}

// SVG Data
var isAdmin = false;

var start = function () {
    // storing original coordinates
    this.ox = this.attr("cx");
    this.oy = this.attr("cy");
    this.attr({ opacity: .5 });
},
        move = function (dx, dy) {
            // move will be called with dx and dy
            this.attr({ cx: this.ox + dx, cy: this.oy + dy });
        },
        up = function () {
            // restoring state
            var tX = this.attr("cx") - 26;
            var tY = this.attr("cy") - 27;
            //this.attr({ opacity: 1, title: "new X: " + this.attr("cx") + " \nnewY: " + this.attr("cy") });
            this.attr({ opacity: 1, title: "new X: " + tX + " \nnew Y: " + tY });
        };
var us;
var can;
var R;

function onSuccessSVG(data, status) {
    parsedSVGResponse = JSON.parse(data.d);
    parsedTaMoUSStates = parsedSVGResponse.ActiveSvgMapStateProAndAbbreviation;
    parsedTaMoCANUSStateMarkets = parsedSVGResponse.SvgMapCityDetails;

    TaMoUSStates = parsedTaMoUSStates;
    TaMoCANUSStateMarkets = parsedTaMoCANUSStateMarkets;

    SetActiveStates();
}

function SetActiveStates() {
    simple = (navigator.userAgent.indexOf("Mozilla/5.0") == -1);
    R = Raphael("paper", 420, 300);

    var attrOn = {
        fill: "45-#e5ddd6",
        stroke: "#786259",
        "stroke-width": 1,
        "stroke-linejoin": "round"
    };
    var attrOff = {
        fill: "#fff",
        stroke: "#fff",
        "stroke-width": 1,
        "stroke-linejoin": "round"
    };
    var attrBox = {
        fill: "#4672a3",
        stroke: "#fff",
        "stroke-width": 1,
        "stroke-linejoin": "round",
        "style": "border:1px solid #fff"
    };

    var attrGlow = {
        width: 3,
        fill: true,
        color: "#000"
    };

    us = {};
    var calloutBoxY = 0;
    var boxHeight = 30;
    var boxBottomPadding = 1;

    for (var state in usmap) {
        if (TaMoUSStates[state]) {
            us[state] = R.path(usmap[state]).attr({ title: TaMoUSStates[state] }).attr(attrOn);
            //TrigerMap(us[state][0], us[state], state, us, R);
            //enableTaMoCANUSStates(us[state], state, us, R);			
            //		} else {
            //			us[state] = R.path(usmap[state]).attr(attrOff);
        }
    }
};

function enableTaMoCANUSStates(st, stateName, us, R) {
    if (st[0][0] !== undefined) {
        TrigerMap(st[0][0], st, stateName, us, R);
        //addCANUSEvents(st[0][0], st, stateName, us, R);
    } else {
        TrigerMap(st[0], st, stateName, us, R);
        //addCANUSEvents(st[0], st, stateName, us, R);
    }
}

function TrigerMap(target, stateObj, stateCode, us, R) {
    var current = null;
    //target.style.cursor = "pointer";
    //stateObj.click(function () {
    var thisState = TaMoCANUSStateMarkets[stateCode];
    var j = thisState.length;
    if (!isAdmin && (j < 0)) {
        if (j == 1) {
            findHomes(thisState[0].h, thisState[0].tar);
        }
    } else {
        showState(stateCode, us, R);
    }
    //});
}

///SVG Map
var simple = false;
function addCANUSEvents(target, stateObj, stateCode, us, R) {
    var current = null;
    target.style.cursor = "pointer";
    stateObj.click(function () {
        var thisState = TaMoCANUSStateMarkets[stateCode];
        var j = thisState.length;
        if (!isAdmin && (j < 0)) {
            if (j == 1) {
                findHomes(thisState[0].h, thisState[0].tar);
            }
        } else {
            showState(stateCode, us, R);
        }
    });
}

function showState(stateCode, us, R) {
    var marketDots;
    //add a cover so nothing else is clickable
    var shade = R.rect(0, -10, 628, 450).attr({ opacity: "1", fill: "#fff", stroke: "#fff" });

    //clone our current state so we can manipulate it with out have to track our changes for a restore
    _selectedState = us[stateCode].clone();

    //compute scale factor
    var bounds = _selectedState.getBBox();
    var xMultiplier = 510 / bounds.width;
    var yMultiplier = 360 / bounds.height;
    var itemScale = (xMultiplier > yMultiplier) ? yMultiplier : xMultiplier;

    //compute translation so the state is centered over the us map
    _selectedState.scale(itemScale, itemScale);
    newbounds = _selectedState.getBBox();
    _selectedState.scale(1, 1);
    var dx = (285) - ((newbounds.x) + (newbounds.width / 2.0));
    var dy = (210) - ((newbounds.y) + (newbounds.height / 2.0));

    //alert(dx + " - " + dy + " - " +  itemScale);

    if (simple) {
        //simple way
        _selectedState.translate(dx, dy).scale(itemScale, itemScale);
        shade.attr({ opacity: "0" });

        //add markets to map
        marketDots = addMarket(stateCode, R);


    } else {
        //display the growing state to the user (attributes,delay,[scale easing]
        _selectedState.animate({ translation: [dx, dy], scale: [itemScale, itemScale] }, 110, ">", function () {
            //add markets to map					
            marketDots = addMarket(stateCode, R);
        });
        shade.animateWith(_selectedState, { opacity: "1" }, 110, ">");
    }

    R.safari();
}

function findHomes(event, isCurrentDomain) {
    //	var sessionValues = new Array();
    //	sessionValues['mapStateCode'] = stateCode;
    //	addWebSessionValues(sessionValues, function () { window.parent.location.href = event; });
    TM.Common.GAlogevent('Search', 'Click', 'OnMapFindYourNewHome', '');
    if (isCurrentDomain == "true")
        window.parent.location.href = event;
    else
        window.open(event);


}


function addMarket(stateCode, localR) {
    var marketAtom;
    var fontAttr = { font: 'Bold 13px/16px Arial', 'font-size': 13, 'text-anchor': 'start', fill: "#fff" };
    var dot;
    var triang;
    var mLabel;
    var halo;
    var stateMarkets = localR.set();
    var thisState = TaMoCANUSStateMarkets[stateCode];
    var posiLabel;

    if (thisState) {
        var j = thisState.length;
        for (var i = 0; i < j; i++) {
            marketAtom = localR.set();

            mLabel = localR.text(thisState[i].X + 4, thisState[i].Y, thisState[i].N).attr(fontAttr);

            var labelBounds = mLabel.getBBox();
            if (labelBounds.width == 0) {
                //fijlrt
                labelBounds.width = thisState[i].N.length * 9;
                labelBounds.height = 25;
            }

            //alert(labelBounds.width + "=" + labelBounds.height + "=" + thisState[i].N);

            halo = localR.rect(0, 0, 3, 3, 0);
            dot = localR.ellipse(thisState[i].X + (labelBounds.width / 2) + 1, thisState[i].Y + 27, 6, 2.5)
				.attr({ fill: "#836857", stroke: 0, "stroke": "#836857" });

            var xT = thisState[i].X + (labelBounds.width / 2) - 6;
            var yT = thisState[i].Y + 11;

            triang = localR.path('M ' + xT + ' ' + yT + ' l 7 12 l 7 -12 Z').attr({ fill: '#006a71', "stroke": "#006a71" });

            var dest = "javascript:findHomes('" + thisState[i].h + "', '" + thisState[i].tar + "');";


            halo.attr({ x: thisState[i].X - 3, y: thisState[i].Y - 14, width: labelBounds.width + 6, height: labelBounds.height, 'stroke-width': 0, fill: "#006a71", opacity: 1 });

            //halo.attr({ x: thisState[i].X - 1, y: thisState[i].Y - 16, width: bbox.width + 55, height: 25, 'stroke-width': 0, fill: "#006a71", opacity: 1 });


            mLabel = localR.text(thisState[i].X + 4, thisState[i].Y, thisState[i].N).attr(fontAttr)
                .attr({ href: dest })
                    .mouseover(function () {
                        this.prev.prev.attr({ opacity: 1 });
                    })
                    .mouseout(function () {
                        this.prev.prev.attr({ opacity: 1 });
                    });

            marketAtom.push(
                    halo, triang, dot, mLabel
                );


            if (isAdmin) {
                dot.attr({ fill: "#F00" });
                dot.drag(move, start, up);
            } else {
                marketAtom.attr({ href: dest });
                dot.attr({ href: dest });
                halo.attr({ href: dest });
            }

            stateMarkets.push(marketAtom);
        }
    }
    return stateMarkets;
}
