﻿var ddlCityState = "";

var isLoggedIn = 0;
var NS = function (NSstr) {
    var root = window;
    var ns = NSstr.split('.');
   //ie-8 doesn't support map
    for (var i = 0; i < ns.length; i++) {
        root = root[ns[i]] = root[ns[i]] || {};
    }
};

//first, checks if it isn't implemented yet
String.format = function() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
};

NS("TM.Common");

TM.Common.GAlogevent = function (Category, Action, Label, OptValue) {
    _gaq.push(['_trackEvent', Category, Action, Label, OptValue]);
};

Number.prototype.formatMoney = function (c, d, t) {
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


function ViewStateProvince(cval) {
	$j("#idi_" + cval).slideToggle("fast");
}

function UpdateCredentialText(status, hasFavorites) {
	if (status) {
		//if (hasFavorites)
	    ShowFaviLogoutText();
	    var cookies = document.cookie.split(";");
	    var cookiesString = "";
	    for (var i = 0; i < cookies.length; i++) {
	        var name = cookies[i].split("=")[0];
	        if (name != "IFPSSOAUTH") {
	            cookiesString += cookies[i];
	        }
	        else {
	            cookiesString += "IFPSSOAUTH" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	        }
	    }

	    document.cookies = cookiesString;


	
		//else
			//ShowLogoutText();
	} else {
		ShowLoginText();
	}
}

function ShowLogoutText() {
    $j("#logintext").html("<a href=\"/Account/logout\">Logout</a>");
}

function ShowFaviLogoutText() {
    $j("#logintext").html("<a onclick=\"TM.Common.GAlogevent('CreateAccount', 'Click', 'CreateAccountLink');\" href=\"/Account/My-Favorites\" class=\"fav-savitems\">Favorites</a>|<a href=\"/Account/logout\">Logout</a>");
}

function ShowLoginText() {
    $j("#logintext").html("<a onclick=\"TM.Common.GAlogevent('CreateAccount', 'Click', 'CreateAccountLink');\" href=\"/Account/Register\">Create Account</a>|<a onclick=\"TM.Common.GAlogevent('Login', 'Click', 'Login');\" href=\"/Account/Login\">Login</a>");
}

TM.Common.ShowLoginModal = function (query) {
	if ($j.colorbox === undefined) {
		document.location.href = "/Account/login.aspx";
	}

	//$j.colorbox({ width: 400, height: 383, scrolling: false, href: "/Account/loginmodal.aspx?sendto=" + sendto + "&iframe=true" });
	$j.colorbox({ width: 400, height: 383, scrolling: false, href: "/Account/loginmodal.aspx" + query + "&sendto=" + document.location.href + "&iframe=true" });
}

function CommunityAddRemoveFavorites(userIsLoggedIn, id, type, action) {
	TM.Common.GAlogevent('Favorites', 'Click', 'CommunityCard-AddtoFavorites');
	if (userIsLoggedIn == "True") {
		AddRemoveFavorites(id, type, action);
		CommunityChangeFavoriteText(id, userIsLoggedIn);
	} else {
		//TM.Common.ShowLoginModal(document.location.href);
		TM.Common.ShowLoginModal("?id=" + id + "&type=" + type + "&action=" + action);
	}
}

function CommunitySort() {
    TM.Common.GAlogevent('Sort', 'Click', $j("#searchsortby option:selected").text());
}

function SetGAEventbyText(id, label) {

    var value = $j("#" + id).text();
    TM.Common.GAlogevent(label, 'Click', value);
   
}

function SetGAEventbyVal(id, label) {
    
    var value = $j("#" + id).val();
    TM.Common.GAlogevent(label, 'Click', value);
    
}


function AddRemoveFavorites(id, type, action) {
	TM.Common.GAlogevent('Favorites', 'Click', 'AddtoFavorites');
	if (id === undefined)
		id = "";

	if (type === undefined)
		type = "1";

	var searchInfo = { "id": id, "type": type, "action": action };
	
	$j.ajax({
		type: "GET",
		data: searchInfo,
		url: "/admin/favorites.aspx",
		contentType: "application/json; charset=utf-8",
		dataType: "json"
	});

	
	//ChangeFavoriteText(id);
}

function AddRemoveFavoritesbyModal(id, type, action, sentto) {
	TM.Common.GAlogevent('Favorites', 'Click', 'AddtoFavorites');
	if (id === undefined)
		id = "";

	if (type === undefined)
		type = "1";

	var searchInfo = { "id": id, "type": type, "action": action };

	$j.ajax({
		type: "GET",
		data: searchInfo,
		url: "/admin/favorites.aspx",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		error: function (data) { //Thats weird success does not work even there are no erro
			window.location.href = sentto;
		}
	});
	
}


function RemoveFavorites(id, type, action, domain) {
	if (id === undefined)
		id = "";

	if (type === undefined)
		type = "1";

	var searchInfo = { "id": id, "type": type, "action": action };

	$j.ajax({
		type: "GET",
		data: searchInfo,
		url: "/admin/favorites.aspx",
		contentType: "application/json; charset=utf-8",
		dataType: "json"
	});

	RemoveFromCard(id, type, domain);
}

function RemoveFromCard(id, type, domain) {
	$j("#" + type + "_" + id).remove();
	var ct = parseInt($j("#" + domain + "_" + type + "_count").text());
	//$j("#" + domain + "_" + type + "_count").text(ct - 1);

	$j("[id*=" + domain + "_" + type + "_count]").text(ct - 1);
}

function onFail(request, status, error) {
	alert(error + ": 1 " + request.responseText);
}

function CommunityChangeFavoriteText(cVal, userIsLoggedIn) {
	if ($j("a#fav_" + cVal).html() == "Add to Favorites") {

		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:CommunityAddRemoveFavorites(" + userIsLoggedIn + ",'" + cVal + "', 'Community', 'Remove')\">Remove from Favorites</a>");
		$j("a#fav_" + cVal).addClass("selected");

	}
	else {
		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:CommunityAddRemoveFavorites(" + userIsLoggedIn + ",'" + cVal + "', 'Community', 'Add')\">Add to Favorites</a>");
		$j("a#fav_" + cVal).removeClass("selected");
	}
}

//function CommunityChangeFavoriteText(cVal, sendto, userIsLoggedIn) {
//	if ($j("a#fav_" + cVal).html() == "Add to Favorites") {

//		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:CommunityAddRemoveFavorites(" + userIsLoggedIn + "," + sendto + ",'" + cVal + "', 'Community', 'Remove')\">Remove to Favorites</a>");
//		$j("a#fav_" + cVal).addClass("selected");

//	}
//	else {
//		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:CommunityAddRemoveFavorites(" + userIsLoggedIn + "," + sendto + ",'" + cVal + "', 'Community', 'Add')\">Add to Favorites</a>");
//		$j("a#fav_" + cVal).removeClass("selected");
//	}
//}

function ChangeFavoriteText(cVal) {
	if ($j("a#fav_" + cVal).html() == "Add to Favorites") {

		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:AddRemoveFavorites('" + cVal + "', 'Community', 'Remove')\">Remove to Favorites</a>");
		$j("a#fav_" + cVal).addClass("selected");

	}
	else {
		$j("#fh_" + cVal).html("<a id=\"fav_" + cVal + "\" href=\"javascript:AddRemoveFavorites('" + cVal + "', 'Community', 'Add')\">Add to Favorites</a>");
		$j("a#fav_" + cVal).removeClass("selected");
	}
}
function printFriendly() { window.open(window.location + (window.location.href.indexOf('?') > 0 ? '&sc_device=print' : '?sc_device=print'), "_blank"); return false; }

function ViewHomeForSales(cVal) {
	$j("#hfs_" + cVal).slideToggle("normal", function () {
		$j(this).is(':visible') ? $j('#ico_' + cVal).removeClass("plusSign").addClass("plusSign on")
										   : $j('#ico_' + cVal).removeClass("plusSign on").addClass("plusSign");
	});
}

NS("TM.Common.Plan");
TM.Common.Plan.Sort = function Sort(a, b, key) {
    var keys = key.split('-');

    key = keys.shift();

    var reverseVal = key.lastIndexOf('Reverse');
    var isReverseSort = false;
    if (reverseVal != -1) {
        key = key.substr(0, reverseVal);
        isReverseSort = true;
    }

    var x, y;

    if (isReverseSort) {
        x = b[key];
        y = a[key];
    } else {
        x = a[key];
        y = b[key];
    }
    return ((x < y) ? -1 : ((x > y) ? 1 : (keys[0] ? TM.Common.Plan.Sort(a, b, keys.join()) : 0)));
};

function sitesearch(id) {
    var searchstr = $j(id).val();
    TM.Common.GAlogevent("Search", "Click", searchstr);
    window.location.href = "/searchresults?q=" + searchstr;

};

function stageCampaign(url,campaignId) {

    TM.Common.GAlogevent("Search", "Click", campaignId);
    window.location.href = url;

};


function LiveChatHeightTest(image, id) {
if ($j(image).height() < 10) {
   	$j("div#pnLiveChat_" + id).hide();
} else {
   	$j("div#pnLiveChat_" + id).hide();
   	$j("a#chat-Button" + "_" + id).attr({ "href": $j("a#_lpChatBtn_" + id).attr("href"), "target": $j("a#_lpChatBtn_" + id).attr("target") }).text("Chat Now");
}
}


//function LiveChatHeightTest(image, id) {
//	if ($j(image).height() < 10) {
//		$j("div#pnLiveChat_" + id).hide();
//		$j("a#chat-Button" + "_" + id).attr({ "href": $j("a#_lpChatBtn_" + id).attr("href"), "target": $j("a#_lpChatBtn_" + id).attr("target") }).text("Request-Information");
//	} else {
//		$j("div#pnLiveChat_" + id).hide();
//		$j("a#chat-Button" + "_" + id).attr({ "href": $j("a#_lpChatBtn_" + id).attr("href"), "target": $j("a#_lpChatBtn_" + id).attr("target") }).text("Chat Now");
//	}
//}


function LoginModal(user, pwd) {
	var username = $j("#" + user).val();
	var password = $j("#" + pwd).val();
	
	if (username === undefined)
		username = "";

	if (password === undefined)
		password = "";

	if (username == "" || password == "") {
		$j("#usermodalerror").show();
	} else {
		$j("#usermodalerror").hide();
		var userInfo = { "username": username, "password": password };

		var response = $j.ajax({
			type: "GET",
			data: userInfo,
			url: "/admin/User.aspx",
			contentType: "application/json; charset=utf-8",
			async: false
		}).responseText;

		if (response == 1) {
			$j("#usermodalerror").hide();
			ShowFaviLogoutText();
		} else {
			$j("#usermodalerror").show();
		}

		return response;
	}
}



