xcopy /d /Y /S /C .\bin\* ..\TM.Web.UI\bin\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\admin\* ..\TM.Web.UI\admin\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\javascript\* ..\TM.Web.UI\javascript\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\Services\* ..\TM.Web.UI\Services\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\layouts\* ..\TM.Web.UI\tmwebcustom\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\weblog\* ..\TM.Web.UI\layouts\weblog\
xcopy /exclude:buildExclusions.txt /d /Y /S /C .\UserControls\* ..\TM.Web.UI\tmwebcustom\Usercontrols\
xcopy /d /Y /S /C .\Styles\* ..\TM.Web.UI\Styles\
xcopy /d /Y /S /C .\Images\* ..\TM.Web.UI\Images\
xcopy /d /Y /S /C .\XSL\* ..\TM.Web.UI\XSL\
xcopy /d /Y /S /C .\global.asax ..\TM.Web.UI\
xcopy /d /Y /S /C .\BingSiteAuth.xml ..\TM.Web.UI\
xcopy /d /Y /S /C .\google806cdbc3d702ca71.html ..\TM.Web.UI\
xcopy /d /Y /S /C .\resourcecombiner.xml ..\TM.Web.UI\
xcopy /d /Y /S /C .\AppConfig\* ..\TM.Web.UI\App_Config\

REM uncomment following for releases
::optimizeimages ..\TM.Web.UI\Images
