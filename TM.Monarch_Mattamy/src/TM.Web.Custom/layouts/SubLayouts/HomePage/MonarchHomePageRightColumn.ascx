﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonarchHomePageRightColumn.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.MonarchHomePageRightColumn" %>
<div class="rt-col" id="rightColumn">
					<sc:Placeholder runat="server" ID="phStageCampaign" Key="StageCampaign" />
                    <sc:Placeholder runat="server" ID="phAccordion" Key="Accordion" />
					<sc:Placeholder runat="server" ID="phBlogTeaser" Key="BlogTeaser" />
				</div>