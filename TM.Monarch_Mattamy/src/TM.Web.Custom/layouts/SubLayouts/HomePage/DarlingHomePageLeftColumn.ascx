﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DarlingHomePageLeftColumn.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.DarlingHomePageLeftColumn" %>

<div class="lt-col hmbg" id="leftColumn">
    <section>
        <div class="lt-col-sp">
            <asp:Repeater runat="server" ID="rptHomes"  ItemType="Sitecore.Data.Items.Item">
                <ItemTemplate>
                    <p class="leftTitle_M">
                        <%#:GetItemName(Item) %>
                    </p>
                    <div class="grayLine">
                        &nbsp;
                    </div>
                    <div class="leftText_M">
                        <sc:Text runat="server" Field="Summary" DataSource="<%#:Item.ID %>" />
                    </div>
                    <ul class="SearHomes">
                        <li><a onclick="TM.Common.GAlogevent('Search', 'Click', '<%# GetItemName(Item) %>');" href="<%#GetItemUrl(Item)%>"><%#Item.Fields["Link Text"].Value %></a></li>
                    </ul>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <ul class="RecBlogBox">
            <li>
                <sc:Link ID="lnkFooterBlog" runat="server" Field="Blog URL">
                    Recent Blog Posts
                </sc:Link>
            </li>
        </ul>
    </section>
</div>
