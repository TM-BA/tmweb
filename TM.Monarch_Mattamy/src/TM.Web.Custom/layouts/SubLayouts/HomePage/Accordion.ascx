﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Accordion.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.Accordion" %>
<link href="/Styles/lib/accordion.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/javascript/lib/accordion.js"></script>
<style type="text/css">
    
    .accordionButton 
    {
        background-color: <%= BUTTON_BACKGROUND %>;
        color:  <%= BUTTON_FONT %>;
    }
    
    .accordionContent
    {
        background-color: <%= CONTENT_BACKGROUND %>;
        color:  <%= CONTENT_FONT %>;
    }
    
    .accordionContent p
    {        
        color:  <%= CONTENT_FONT %>;
    }
    .on
    {
         background-color: <%= BUTTON_WHEN_ON_BACKGROUND %>;
        color:  <%= BUTTON_WHEN_ON_FONT %>;
    }
    .over
    {
         background-color: <%= BUTTON_HOVER_BACKGROUND %>;
        color:  <%= BUTTON_HOVER_FONT %>;
    }
    
</style>
<div id="wrapper">
    
    <asp:Repeater runat="server" ID="rptAccordion">
        <ItemTemplate>
            <div class="accordionButton" onclick="TM.Common.GAlogevent('Accordion', 'Click', '<%#Eval("Title")%>');">
                <%#Eval("Title")%></div>
            <div class="accordionContent">
                <br />
                <%#Eval("Content")%><br />
                <br />
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
