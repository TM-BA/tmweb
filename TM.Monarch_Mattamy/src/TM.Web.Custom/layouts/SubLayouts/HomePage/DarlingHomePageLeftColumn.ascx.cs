﻿using System;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data;

namespace TM.Web.Custom.Layouts.SubLayouts.HomePage
{
    public partial class DarlingHomePageLeftColumn : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var productsQuery = SCFastQueries.FindItemsMatchingTemplateUnder(SCCurrentHomePath, SCIDs.TemplateIds.ProductTypePage);
            var products = SCContextDB.SelectItems(productsQuery);
            var divisions = new List<Item>();
            foreach (var product in products)
            {
                var states = product.Children;
                foreach (Item state in states)
                {
                    if (state.TemplateID == SCIDs.TemplateIds.StatePage)
                    {
                        foreach (Item division in state.Children)
                        {
                            if (division.TemplateID == SCIDs.TemplateIds.DivisionPage)
                                divisions.Add(division);
                        }
                    }
                }
            }

            rptHomes.DataSource = divisions;
            this.DataBind();
        }

        protected string GetItemName(Item item)
        {
            return item.GetItemName();
        }

        protected string GetItemUrl(Item item)
        {
            return item.GetItemUrl();
        }
    }
}