﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HTHDivisionPromotion.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.HighTechHomes.HTHDivisionPromotion" %>

<sc:placeholder runat="server" id="ContentArea" key="ContentArea">
</sc:placeholder>

<style>
.promo-form-wrap {
background-color: #EEEEEE;
border-radius: 12px 12px 12px 12px;
max-width: 280px;
padding: 30px;
position: absolute;
right: 30px;
top: 200px;
z-index: 100;
margin: 30px 16px 0;
}



.hth-wrapper h1.scfTitleBorder{
font-family: 'Gudea', Tahoma, Geneva, sans-serif;
font-weight: normal;
font-size: 19px;
color: #666666;
line-height: 25px;
margin: 0 0 12px;
}


.hth-wrapper input{
	width: 190px;
border: 1px solid #b6b5ac;
background-color: #fff;
height:19px;
padding:1 0 1 0;

}

.hth-wrapper select.scfDropList{
	width: 190px;
border: 1px solid #b6b5ac;
background-color: #fff;
height:20px;
padding:1 0 1 0;

}

.hth-wrapper .scfSubmitButton{
  margin-right: 20px;
  width: 110px !important;
  background:url("/~/media/high%20tech%20homes/hth-su.png") !important;
}

</style>


<div class="promo-form-wrap">
	
    <div class="hth-wrapper">
        <sc:placeholder runat="server" id="InterestList" key="InterestList">
        </sc:placeholder>
    </div>

</div>


<script type="text/javascript">
    $j(document).ready(function () {
        var form = $j(".hth-wrapper div.scfForm").length;
        if (!form) {
            $j("div.promo-form-wrap").hide();
        }
    });

    
</script>
