﻿using System;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone
{
    public partial class RequestInfo : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkCancel.NavigateUrl = SCCurrentHomeItem.GetItemUrl();
        }
    }
}