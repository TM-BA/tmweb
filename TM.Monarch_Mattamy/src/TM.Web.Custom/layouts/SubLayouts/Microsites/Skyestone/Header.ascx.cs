﻿using System;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone
{
    public partial class Header : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Phone"].Value))
            {
                PhoneLink.Text = SCCurrentHomeItem.Fields["Phone"].Value;
                PhoneLink.NavigateUrl = "tel:" + SCCurrentHomeItem.Fields["Phone"].Value
                    .Replace("(", string.Empty).Replace(")", string.Empty).Replace("-", string.Empty)
                    .Replace(" ", string.Empty);
            }
            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem.Fields["Logo"].Value))
            {

                Logo.Text = string.Format("<img src='{0}'></img>", SCCurrentHomeItem.Fields["Logo"].GetMediaUrl());

            }
            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem.Fields["Header Banner"].Value))
            {

                HeaderBanner.Text = string.Format("<img src='{0}'></img>", SCCurrentHomeItem.Fields["Header Banner"].GetMediaUrl());

            }
            GetUpdatesLink.NavigateUrl = TMLinkProvider.GetUrl(SCCurrentHomeItem.Fields["Get Updates"]);
            BlogLink.NavigateUrl = TMLinkProvider.GetUrl(SCCurrentHomeItem.Fields["Blog"]);

        }
    }
}