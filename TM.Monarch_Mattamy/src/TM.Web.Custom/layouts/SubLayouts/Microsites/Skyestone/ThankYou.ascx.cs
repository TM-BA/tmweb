﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone
{
    public partial class ThankYou : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LitThankYou.Text = SCContextItem["Thank You"];
        }
    }
}