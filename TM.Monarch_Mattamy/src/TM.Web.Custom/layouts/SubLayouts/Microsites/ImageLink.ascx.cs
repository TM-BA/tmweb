﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.SCHelpers;
namespace TM.Web.Custom.Layouts.SubLayouts.Microsites
{
    public partial class ImageLink : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Data.Fields.LinkField lnk = DataSource.Fields["Link"];
            var url = TMLinkProvider.GetUrl(lnk);
            LiteralImageLink.Text = string.Format("<a href='{0}' target='{2}'><img src='{1}'></img></a>", url, DataSource.Fields["Image"].GetMediaUrl(), lnk.Target);
        }
       
    }
}