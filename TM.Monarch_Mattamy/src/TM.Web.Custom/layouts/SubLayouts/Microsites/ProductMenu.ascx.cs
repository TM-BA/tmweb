﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Links;
using Sitecore.Pipelines.InsertRenderings.Processors;
using Sitecore.Shell.Applications.ContentEditor.FieldTypes;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class ProductMenu : ControlBase
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Item source = DataSource;
            if (source != null)
            {
                txtContent.DataSource = source.ID.ToString();
                txtContent.DataBind();

                txtPlansTitle.DataSource = source.ID.ToString();
                txtPlansTitle.DataBind();

                MultilistField communityList = source.Fields["Community Links"];
                var list = transformList(communityList.GetItems());
                if (list != null && list.Count > 0)
                {
                    this.repCommunities.DataSource = list;
                    repCommunities.DataBind();
                    this.repCommunities.Visible = true;
                    ulPlansTitle.Visible = true;
                }
                else
                {
                    this.repCommunities.Visible = false;
                    ulPlansTitle.Visible = false;
                }


                MultilistField productList = source.Fields["Product Links"];
                list = transformList(productList.GetItems());
                if (list != null && list.Count > 0)
                {
                    this.repLinks.DataSource = list;
                    repLinks.DataBind();
                    this.repLinks.Visible = true;
                }
                else
                {
                    this.repLinks.Visible = false;
                }
                
            }
            

        }

        private List<LinkStruct> transformList(IEnumerable<Item> list)
        {
            var returnList = new List<LinkStruct>();

            foreach (Item item in list)
            {
                var str = new LinkStruct();
                UrlOptions options = new UrlOptions();
                options.AlwaysIncludeServerUrl = false;
                options.AddAspxExtension = true;
                options.LowercaseUrls = true;
                options.SiteResolving = false;

                if (item.TemplateID.ToString() == SCIDs.TemplateIds.ProductLinks)
                {
                    str.Name = item.Fields["Link Name"].Value;
                    str.Style = item.Fields["Component Style"].Value;
                    str.Url = item.Fields["URL"].GetLinkFieldPath();
                }
                else if (item.TemplateID.ToString() == SCIDs.TemplateIds.CommunityLinks)
                {
                    str.Name = item.Fields["Link Name"].Value;
                    str.Style = item.Fields["Component Style"].Value;
                    
                    Item community = SCContextDB.GetItem(new Sitecore.Data.ID(item.Fields["Community"].Value));
                    if (community["Status for Search"] == SCIDs.StatusIds.Status.Active)
                    {
                        Uri uri = HttpContext.Current.Request.Url;
                        str.Url = community.GetSiteSpecificUrl(options, uri);
                        var planList = community.GetAllChildrenByTemplate(SCIDs.TemplateIds.PlanPage);

                        planList = (from p in planList
                            where p["Plan Status"] == SCIDs.StatusIds.Status.Active
                            select p).ToList();

                        var querySubcomm =
                            from plan in planList
                            group plan by plan["SubCommunity ID"]
                            into newGroup
                            orderby newGroup.Key
                            select newGroup;
                        var newList = new List<LinkStruct>();
                        foreach (var nameGroup in querySubcomm)
                        {


                            if (nameGroup.Key != "")
                            {
                                //Creates a new link via a SubCommunity
                                var strSubCommunity = new LinkStruct();
                                var subComItem = SCContextDB.GetItem(new Sitecore.Data.ID(nameGroup.Key));
                                strSubCommunity.Name = subComItem.DisplayName;
                                strSubCommunity.Style = "";
                                strSubCommunity.Url = "javascript:void(0)";
                                strSubCommunity.ChildList = transformList(nameGroup.ToList());
                                if (!String.IsNullOrEmpty(subComItem.DisplayName))
                                    newList.Add(strSubCommunity);
                            }
                            else
                            {
                                var x = newList.Union(transformList(nameGroup.ToList()));
                                newList = x.ToList();
                            }

                        }
                        str.ChildList = newList;
                    }
                    else
                    {
                        str.Name = "";
                    }

                }
                else if (item.TemplateID.ToString() == SCIDs.TemplateIds.PlanPage.ToString())
                {
                    str.Name = item.DisplayName;
                    str.Style = "";
                    
                    Uri uri = HttpContext.Current.Request.Url;
                    str.Url = item.GetSiteSpecificUrl(options, uri);
                    
                    var homeForSaleList = item.GetAllChildrenByTemplate(SCIDs.TemplateIds.HomeForSalePage);

                    homeForSaleList = (from h in homeForSaleList
                                       where h["Home for Sale Status"] == SCIDs.StatusIds.Status.Active
                                       select h).ToList();
                    
                    str.ChildList = transformList(homeForSaleList);
                }
                else if (item.TemplateID.ToString() == SCIDs.TemplateIds.HomeForSalePage.ToString())
                {
                    str.Name = item.DisplayName.Replace("Home Available Now at", string.Empty);
                    str.Style = "";
                    
                    Uri uri = HttpContext.Current.Request.Url;
                    str.Url = item.GetSiteSpecificUrl(options, uri);
                }

                if (!String.IsNullOrEmpty(str.Name))
                    returnList.Add(str);

            }

            return returnList;
        }

        public bool IsVisible(object list)
        {
            var s = list as List<LinkStruct>;
            bool res = s != null && s.Count > 0;
            return res;
        }

        public string renderTag(object list)
        {
            var s = list as List<LinkStruct>;
            var res = "visibility: hidden;";
            if (s != null && s.Count > 0)
            {
                res = "visibility: visible;";
            }
            
            return res;
        }

        
    }

    public struct LinkStruct
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string Style { get; set; }
        public List<LinkStruct> ChildList { get; set; }
    }

}