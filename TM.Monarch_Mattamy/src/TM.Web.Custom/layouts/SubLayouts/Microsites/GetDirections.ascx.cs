﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Pipelines.InsertRenderings.Processors;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class GetDirections : ControlBase
    {
        public string CommunityJSON { get; set; }
        public CommunityInfo MyCommunity { get; set; }
        public Item MyCommunityItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            var itemID = Session["GetDirectionsItemID"];
            var communityInfoList = new List<CommunityInfo>();
            if (itemID != null)
            {
                Item community = SCContextDB.GetItem(new Sitecore.Data.ID(itemID.ToString()));
                
                var searchHelper = new SearchHelper();
                var list = new List<Item>();
                list.Add(community);
                communityInfoList = searchHelper.HydrateCommunitiesByItems(list, "", "", false);
                MyCommunity = communityInfoList[0];
                MyCommunityItem = community;
            }

            CommunityJSON = communityInfoList.ToJSON();

            if (!IsPostBack)
            {
                lnkPrintDirections.NavigateUrl = Request.Url.GetLeftPart(UriPartial.Path).Contains('?') ? Request.Url.GetLeftPart(UriPartial.Path) + "&print=print" : Request.Url.GetLeftPart(UriPartial.Path) + "?print=print";
                if (Request["print"] != null)
                {
                    if (Request["print"] == "print")
                    {
                        mapContainer.Visible = false;
                        googleJS.Visible = true;
                    }
                }

                if (Request["startAddress"] != null)
                {
                    this.txtFromAddress.Value = Request["startAddress"];
                }
            }

            
        }

        public void lnkbEmailDirections_Click(object sender, EventArgs e)
        {
            //string body = DirectionsContent.InnerHtml;
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(@"<html><head></head><body>", Request.Url.Host);

            AddHeading(sb);
            AddSalesCenterAddress(sb);
            AddHours(sb);

            string destCommunity = MyCommunity.CommunityLatitude + "," + MyCommunity.CommunityLongitude;
            
            var directionSteps = TM.Utils.Web.GMapUtil.GetDirections(this.txtFromAddress.Value, destCommunity);
            foreach (TM.Utils.Web.DirectionSteps leg in directionSteps)
            {
                sb.AppendLine(@"<div class=""origin""><img src=""http://maps.gstatic.com/mapfiles/markers2/icon_greenA.png"" />{0}</div>".FormatWith(leg.OriginAddress));
                sb.AppendLine(@"<div></div>");
                foreach (TM.Utils.Web.DirectionStep step in leg.Steps)
                {
                    sb.AppendLine(@"<div class=""step""><span class=""index"">{0}.</span><span class=""instruction"">{1}</span><span class=""distance"">{2}</span></div>".FormatWith(
                         step.Index
                        , step.Description
                        , step.Distance));
                }
                sb.AppendLine(@"<div class=""origin""><img src=""http://maps.gstatic.com/mapfiles/markers2/icon_greenB.png"" />{0}</div>".FormatWith(leg.DestinationAddress));
            }
            sb.AppendLine(@"<p>Map data ©{0} Google</p>".FormatWith(DateTime.Now.Year));
            sb.AppendLine("</body></html>");
            string subject = "Driving Directions to {0}, {1}".FormatWith(
                MyCommunity.CommunityName,
                CurrentPage.CurrentHomeItem.GetItemName()
                );

            EmailHelper.SendEmail(FromEmail, toAddress.Text, subject, sb.ToString(), true, null, null, null, false);
            //Page.ClientScript.RegisterStartupScript(Page.GetType(), "initMap", "google.maps.event.addDomListener(window, \"load\", reloadMap);", true);
        }

        private void AddHeading(StringBuilder sb)
        {
            //sb.Append(@"<style type=""stylesheet"">body, h1, li { background-color:red; color:#19398A; }</style>");
            sb.Append(@"<h1 style=""color:#19398A; font-size:14px;"">Driving Directions</h1>");
            var logo = MyCommunity.CommunityLogo;
            sb.Append(@"<span class=""logo""><img src=""http://{2}{0}"" alt=""{1}"" /></span>".FormatWith(logo, MyCommunity.CompanyName, MyCommunity.HostName));
            sb.Append(@"<h2 style=""color:#D31245; font-size:14px; margin:0"">{0}</h2><div class=""phone"">{1}</div>".FormatWith(MyCommunity.CommunityName, MyCommunity.Phone));
        }


        private void AddSalesCenterAddress(StringBuilder sb)
        {
            sb.Append("<h1>Sales Center:</h1>");
            sb.Append(@"<div class=""SalesCenter""><span class=""address"">{0}</span><span=""address"">{1}</span><span=""address"">{2}, {3} {4}</span></div>".FormatWith(MyCommunity.StreetAddress1, MyCommunity.StreetAddress2, MyCommunity.CommunityCityName, MyCommunity.CommunityStateName, MyCommunity.ZipPostalCode));
        }


        private void AddHours(StringBuilder sb)
        {
            sb.Append(@"<div class=""da_ho""><h1>Sales Center Hours</h1>");
            
            if (string.IsNullOrWhiteSpace(MyCommunityItem["Use Office Hours Text"]))
            {
                sb.Append(@"<ul class=""saleshrs"">");

                sb.Append("<li><span>Sunday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Sunday Open"], MyCommunityItem["Sunday Close"])
                    ));
                sb.Append("<li><span>Monday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Monday Open"], MyCommunityItem["Monday Close"])
                    ));
                sb.Append("<li><span>Tuesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Tuesday Open"], MyCommunityItem["Tuesday Close"])
                    ));
                sb.Append("<li><span>Wednesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Wednesday Open"], MyCommunityItem["Wednesday Close"])
                    ));
                sb.Append("<li><span>Thursday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Thursday Open"], MyCommunityItem["Thursday Close"])
                    ));
                sb.Append("<li><span>Friday:</span><span>{0}</span></li>".FormatWith(
                    getTime(MyCommunityItem["Friday Open"], MyCommunityItem["Friday Close"])
                    ));
                sb.Append("<li><span>Saturday:</span><span>{0}</span></li>".FormatWith(
                     getTime(MyCommunityItem["Saturday Open"], MyCommunityItem["Saturday Close"])
                     ));

                sb.Append(@"</ul>");
            }
            else
            {
                sb.Append(MyCommunityItem["Office Hours Text"]);
            }
            sb.Append("</div>");
        }

        private string getTime(string open, string close)
        {
            string ret;
            if (string.IsNullOrWhiteSpace(open)
                || string.IsNullOrWhiteSpace(close)
                || (!Hours.ContainsKey(open))
                || (!Hours.ContainsKey(close)))
            {
                ret = "Closed";
            }
            else
            {
                if (Hours[open].DisplayName == Hours[close].DisplayName)
                {
                    ret = Hours[open].DisplayName;
                }
                else
                {
                    ret = "{0} to {1}".FormatWith(Hours[open].DisplayName, Hours[close].DisplayName);
                }
            }
            return ret;
        }

        private Dictionary<string, Item> _hours = new Dictionary<string, Item>();
        private Dictionary<string, Item> Hours
        {
            get
            {
                if (_hours.Count == 0)
                {

                    string query = SCFastQueries.FindItemsMatchingTemplateUnder("/sitecore/Content/Global Data/Shared Field Values/Valid Times", new Sitecore.Data.ID("{123D1034-5B8D-4224-8CFB-2D5A95368166}"));

                    Item[] allHours = CurrentPage.SCContextDB.SelectItems(query);

                    foreach (Item possibleOpen in allHours)
                    {
                        _hours[possibleOpen.ID.ToString()] = possibleOpen;
                    }
                }
                return _hours;
            }
        }

    }
}