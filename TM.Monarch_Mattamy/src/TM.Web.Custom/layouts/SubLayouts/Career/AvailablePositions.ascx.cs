﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using System.Collections.Generic;

namespace TM.Web.Custom.Layouts.SubLayouts.Carrer
{
    public partial class AvailablePositions : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateAvailablePositions(false);
                PopulateFilters();
            }
        }





        /// <summary>
        /// Populate available positions
        /// </summary>
        private void PopulateAvailablePositions(bool Filter)
        {

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.AvailablePositions.PositionDetails));
            
            List<Item> positions = new List<Item>();

            List<ListItem> cityStateFilter = new List<ListItem>();

            List<ListItem> departmentFilter = new List<ListItem>();

            string cityState = "";
            
            if (matchingTemplates != null)
            {

                if (matchingTemplates.Count() > 0)
                {

                    foreach(Item item in matchingTemplates)
                    {
                        if (item.Fields["Status"] != null)
                        { 
                             if (item.Fields["Status"].Value == SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString())
                             {
                                 if (DateTime.Now <= SCUtils.SCDateTimeToMsDateTime(item.Fields["PositionEndDate"].Value) || SCUtils.SCDateTimeToMsDateTime(item.Fields["PositionEndDate"].Value) == DateTime.MinValue)
                                 {
                                         if (item.Fields["Area"] != null)
                                         {
                                             if (!string.IsNullOrEmpty(item.Fields["Area"].Value))
                                             {
                                                 Item location = SCContextDB.GetItem(item.Fields["Area"].ToString());

                                                 cityState = location.GetItemName(true) + ", " + location.Parent.GetItemName(true);
                                             }
                                         }

                                         

                                        
                                     
                                         if (Filter)
                                         {


                                             if (ddlCityState.SelectedIndex > 0 && ddlDepartment.SelectedIndex > 0)
                                             {
                                                 if (cityState == ddlCityState.SelectedItem.Text && item.Fields["Department"].Value.Trim().Equals(ddlDepartment.SelectedItem.Text))
                                                 {
                                                     positions.Add(item);
                                                 }

                                             }
                                             else if (ddlCityState.SelectedIndex > 0 && ddlDepartment.SelectedIndex == 0)
                                             {
                                                 if (cityState == ddlCityState.SelectedItem.Text)
                                                 {
                                                     positions.Add(item);
                                                 }

                                                 
                                             }
                                             else if (ddlDepartment.SelectedIndex > 0 && ddlCityState.SelectedIndex == 0)
                                             {
                                                 if (item.Fields["Department"].Value.Trim().Equals(ddlDepartment.SelectedItem.Text))
                                                 {
                                                     positions.Add(item);
                                                 }

                                                 
                                             }

                                         }
                                         else
                                         {
                                             positions.Add(item);
                                         }


                                 }
                             }
                        
                        }
                    
                    }

                    if (positions.Count > 0)
                    {
                        rptAvailablePositions.DataSource = positions;
                        rptAvailablePositions.DataBind();
                        rptAvailablePositions.Visible = true;
                        lblNoResults.Visible = false;
                    }
                    else
                    {
                        rptAvailablePositions.Visible = false;
                        lblNoResults.Text = "Search criteria does not return any results.";
                        lblNoResults.Visible = true;
                    
                    }

                    
                }
            }
        
        
        }


        private void PopulateFilters()
        {

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.AvailablePositions.PositionDetails));

            List<ListItem> cityStateFilter = new List<ListItem>();

            List<ListItem> departmentFilter = new List<ListItem>();

            string cityState = "";

            if (matchingTemplates != null)
            {

                if (matchingTemplates.Count() > 0)
                {

                    foreach (Item item in matchingTemplates)
                    {
                        if (item.Fields["Status"] != null)
                        {
                            if (item.Fields["Status"].Value == SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString())
                            {
                                if (DateTime.Now <= SCUtils.SCDateTimeToMsDateTime(item.Fields["PositionEndDate"].Value) || SCUtils.SCDateTimeToMsDateTime(item.Fields["PositionEndDate"].Value) == DateTime.MinValue)
                                {

                                    if (item.Fields["Area"] != null)
                                    {
                                        if (!string.IsNullOrEmpty(item.Fields["Area"].Value))
                                        {
                                            Item location = SCContextDB.GetItem(item.Fields["Area"].ToString());

                                            cityState = location.GetItemName(true) + ", " + location.Parent.GetItemName(true);
                                        }
                                    }

                                    string divisionIdFld = item.ID.ToString();
                                    string divisionNameFld = cityState;

                                    cityStateFilter.Add(new ListItem(divisionNameFld, divisionNameFld));


                                    if (!string.IsNullOrEmpty(item.Fields["Department"].Value))
                                    {
                                        departmentFilter.Add(new ListItem(item.Fields["Department"].Value, item.Fields["Department"].Value));
                                    }
                                }
                            }
                        }

                      }
                  }
            }



                    cityStateFilter = (from d in cityStateFilter select d).Distinct().ToList();
                    ddlCityState.DataSource = (cityStateFilter.OrderBy(x => x.Text.Split(',')[1].TrimEnd()).ThenBy(y => y.Text.Split(',')[0])).ToList();
                    ddlCityState.DataBind();

                    ddlDepartment.DataSource = (departmentFilter.Distinct().OrderBy(x => x.Text)).ToList();
                    ddlDepartment.DataBind();

                    ddlCityState.Items.Insert(0, new ListItem("Please select a division to filter", "0"));
                    ddlDepartment.Items.Insert(0, new ListItem("Please select a department to filter", "0"));

                   
                }
            
  
        protected void rptAvailablePositions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    HyperLink relatedLink = (HyperLink)e.Item.FindControl("hlkTitle");
                    relatedLink.NavigateUrl =  SCUtils.GetItemUrl(currentItem).Replace(SCCurrentHomePath,"");
                    if (currentItem.Fields["Title"] != null)
                    {
                        relatedLink.Text = currentItem.Fields["Title"].ToString();
                    }

                    Label locationlbl = (Label)e.Item.FindControl("locationlbl");

                    if (currentItem.Fields["Area"] != null)
                    {
                        if (!string.IsNullOrEmpty(currentItem.Fields["Area"].Value))
                        {
                            Item location = SCContextDB.GetItem(currentItem.Fields["Area"].ToString());

                            locationlbl.Text = location.GetItemName(true) + ", " + location.Parent.GetItemName(true);
                        }
                    }

                    Label PostingStartDate = (Label)e.Item.FindControl("PostingStartDate");

                     if (currentItem.Fields["PostingStartDate"] != null)
                    {
                         DateTime date = SCUtils.SCDateTimeToMsDateTime(currentItem.Fields["PostingStartDate"].Value);
                         PostingStartDate.Text = String.Format("{0:MMMM}", date).ToString() + " " + date.Day.ToString() + ", " + date.Year;
                    }

                     
                    

                }
            }

        protected void ddlCityState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCityState.SelectedIndex > 0)
            {

                PopulateAvailablePositions(true);
                
            }
            else
            {
                PopulateAvailablePositions(false);
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedIndex > 0)
            {

                PopulateAvailablePositions(true);
                
            }
            else
            {
                PopulateAvailablePositions(false);
            }
        }
            
    }
}