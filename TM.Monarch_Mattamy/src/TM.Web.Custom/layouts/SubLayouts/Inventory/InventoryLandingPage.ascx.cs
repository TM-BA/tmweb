﻿using System;
using System.Web;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;

namespace TM.Web.Custom.Layouts.SubLayouts.Inventory
{
	public partial class InventoryLandingPage : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			liturl.Text = SCContextItem.Paths.FullPath.TrimStart('/').TrimUrlLastText();

			Uri uri = HttpContext.Current.Request.Url;
			string diviPath = uri.AbsolutePath.TrimStart('/').TrimUrlLastText();

			litdiviurl.Text = string.Format("<a href=\"{0}\">See All Communities, Plans and Homes For Sale in {1}</a>", diviPath, diviPath.GetLastTextfromString('/'));

			var searchHelper = new SearchHelper();
			var path = SCContextItem.Paths.FullPath;
			var diviitem = searchHelper.GetPpcDivision(path);

			//ihc card
			var ihcItem = searchHelper.GetIhcCardValues(diviitem);
			if (ihcItem != null)
				litihccard.Text = searchHelper.GetIHCCardDetails(ihcItem);

			//campaign
			var realtorCamp = searchHelper.GetCampaignValues(diviitem, "Inventory Landing Campaign");
			if (realtorCamp != null)
				litLandingPageCampaign.Text = searchHelper.GetCampaignDetails(realtorCamp);

			CurrentPage.CurrentLiveChatSkill = diviitem[SCFieldNames.LiveChatSkillCode];
			WebSession[SessionKeys.SpecificRfiLocationKey] = diviPath;			
		}
	}
}