﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PPCCommunityCard.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.PPCPages.PPCCommunityCard" %>

<div class="ppc-bottom">
	<div id="noRecords" runat="server" visible="false"  style="margin:20px 0 0">
			No communities are available.
	</div>
	<asp:PlaceHolder runat="server" ID="phAllcommunities">
	<div class="ppcsortby">
		<span>Sort by:</span>
			<asp:DropDownList ID="Searchsortby" runat="server" AutoPostBack="true" OnSelectedIndexChanged="onChange_Searchsortby"
				CssClass="sb3">
			</asp:DropDownList>		
	</div>
	<asp:Repeater ID="rptCommunityCardHeader" runat="server" OnItemDataBound="rptCommunityCardHeader_ItemDataBound">
		<ItemTemplate>
			<div class="h-ppc">
				<span><asp:Literal runat="server" ID="listCompanyhder" ></asp:Literal> is currently building in <%# Eval("NumberofCities")%> <%# Convert.ToInt32(Eval("NumberofCities")) > 1 ? "communities" : "community"%> in <%# Eval("City")%></span>
			</div>
			<asp:Repeater ID="rptCommunityCard" runat="server">
				
				<ItemTemplate>
					<div class="ppc_commCard">
						<div class="ppc-image">
							<a href="<%# Eval("CommunityDetailsURL")%>"><img src="<%# Eval("CommunityCardImage")%>?w=245&h=138" onError="this.onerror=null;this.src='/Images/img-not-available.jpg';" alt="<%# Eval("CommunityName")%>"/></a>
							<%#Eval("CommunityImageStatusFlag").ToString().Length > 0 ? CreateURL(Eval("CommunityImageStatusFlag").ToString(), Eval("CommunityStatus").ToString()) : string.Empty%> 										
						</div>					
						<div class="ppc-middle">
							<a href="<%# Eval("CommunityDetailsURL")%>">
								<%# Eval("Name")%></a> <span>
									<%# Eval("CommunityPrice")%></span>
							<p>
								<%# Eval("StreetAddress1")%> <%# Eval("StreetAddress2")%><br /><%# Eval("City")%>, <%# Eval("StateProvinceAbbreviation")%> <%# Eval("ZipPostalCode")%><br><%# Eval("Phone")%>
							</p>
						</div>
						<div class="ppc-right">
							<p>
								<%# Eval("SquareFootage") != "" ? Eval("SquareFootage") + "<br />" : string.Empty%>
								<%# Eval("Bedrooms") != "" ? Eval("Bedrooms") + "<br />" : string.Empty%>
								<%# Eval("Bathrooms") != "" ? Eval("Bathrooms") + "<br />" : string.Empty%>
								<%# Eval("Stories") != "" ? Eval("Stories") + "<br />" : string.Empty%>
								<%# Eval("Garage") != "" ? Eval("Garage") + "<br />" : string.Empty%>
							</p>
						</div>
						<div class="ppc-details">
							<p>
								<%# Eval("CommunityDescription")%>
							</p>
							<div class="addt-details">
								<a href="<%# Eval("CommunityDetailsURL")%>" class="button">Addtional Details<span></span></a>
							</div>
						</div>
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</ItemTemplate>
	</asp:Repeater>	
	</asp:PlaceHolder>	
</div>
