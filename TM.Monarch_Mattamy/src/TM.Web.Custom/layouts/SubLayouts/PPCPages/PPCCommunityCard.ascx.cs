﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.PPCPages
{
	public partial class PPCCommunityCard : ControlBase
	{
		public List<PPCCommunityInfo> hdrInfo;
		public List<CommunityInfo> cInfos;
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Searchsortby.Items.Clear();
				Searchsortby.Items.Insert(0, new ListItem("Community name", "1", true));
				Searchsortby.Items.Insert(1, new ListItem("Price low to high", "2"));
				Searchsortby.Items.Insert(2, new ListItem("Price high to low", "3"));
				Searchsortby.Items.Insert(3, new ListItem("Location", "4"));

				BoundData("1"); //Default Community Name
			}
			
		}

		protected void rptCommunityCardHeader_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rptr = (Repeater)e.Item.FindControl("rptCommunityCard");

				var ltr = (Literal)e.Item.FindControl("listCompanyhder");

				var headerinfo = (PPCCommunityInfo)e.Item.DataItem;
				ltr.Text = headerinfo.Company;  //cInfos.Select(h => h.CompanyName).FirstOrDefault();
				var info = cInfos.AsEnumerable().Where(x => x.City.Equals(headerinfo.City));
				rptr.DataSource = info; 
				rptr.DataBind();
			}
		}

		private void BoundData(string sortBy)
		{
			string ppcurl = SCContextItem.Paths.FullPath;

			hdrInfo = new List<PPCCommunityInfo>();
			cInfos = new List<CommunityInfo>();
			if (!string.IsNullOrEmpty(ppcurl))
			{
				var searchHelper = new SearchHelper();
				cInfos = searchHelper.GetAllActiveCommunitiesBySearch(ppcurl, sortBy);
				
				hdrInfo = cInfos.GroupBy(n => new {n.City, n.CompanyName})
				                .Select(g => new PPCCommunityInfo
					                {
						                City = g.Key.City,
										Company = g.Key.CompanyName,  
						                NumberofCities = g.Count()
					                }).ToList();
			}
			if (hdrInfo.Any())
			{
				noRecords.Visible = false;
				phAllcommunities.Visible = true; 
				rptCommunityCardHeader.DataSource = hdrInfo;
				rptCommunityCardHeader.DataBind();
			}
			else
			{
				noRecords.Visible = true;
				phAllcommunities.Visible = false;
			}
		}

		public string CreateURL(string imgURL, string alt)
		{
			if (!string.IsNullOrEmpty(imgURL))
			{
				return string.Format("<div class=\"comstsflag\"><img src=\"{0}\" alt=\"{1}\"/></div>", imgURL, alt);
			}

			return string.Empty;
		}

		public void onChange_Searchsortby(object sender, EventArgs e)
		{				  
			BoundData(Searchsortby.SelectedValue);
		}	

	}
}
