﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeeklyEmail.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.RealtorWeeklyEmail.WeeklyEmail" %>
<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head>
    <meta charset="utf-8" />
    <title><%= TITLE %></title>
    <!--	<link rel="stylesheet" type="text/css" href="style.css" />-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <style>
        body
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <td align="center" width="640">
                <a href="<%= URL %>" style="color: #0095D9;">To view this email as a web page click
                    here </a>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="355" rowspan="2">
                <sc:Image Field="LogoImage" runat="server" />
            </td>
            <td width="312" height="43">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                <%= COMPANY_NAME %> Weekly Inventory
            </td>
        </tr>
    </table>
    <table width="671" cellpadding="0" cellspacing="0" style="border: 2px solid <%= BASIC_COLOR %>;">
        <tr>
            <td colspan="2"  style="max-width:671px;">
                <sc:Image ID="Image2" Field="BannerImage" runat="server"  Width="671" Height="146" MaxWidth="671" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <% if (DISPLAY_FEATURED)
                   {%>
                <!--block starts here-->
                <table cellpadding="0" cellspacing="0">
                    <tr width="637">
                        <td>
                            &nbsp;
                        </td>
                        <td height="15" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr width="637">
                        <td width="10">
                            &nbsp;
                        </td>
                        <td width="200" height="33" style="background: <%= BASIC_COLOR %>;">
                            <p style="color: #FFFFFF; font-size: 13px; font-weight: bold; text-align: center;
                                font-family: Arial, Helvetica, sans-serif;">
                                FEATURED HOMES</p>
                        </td>
                        <td width="427">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <%
                   }
                %>
                <asp:Repeater runat="server" ID="rptFeaturedResults">
                    <ItemTemplate>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10">
                                    &nbsp;
                                </td>
                                <td style="border-bottom: 2px solid <%= BASIC_COLOR %>; border-top: 2px solid <%= BASIC_COLOR %>;
                                    width: 322px; height: 35px; font-weight: bold;">
                                    <%#Eval("PlanName")%>
                                </td>
                                <td style="border-bottom: 2px solid <%= BASIC_COLOR %>; border-top: 2px solid <%= BASIC_COLOR %>;
                                    width: 322px; height: 35px; text-align: right; font-weight: bold;">
                                    <%#Eval("CommunityName")%>, <%#Eval("City")%>, <%#Eval("State")%>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10">
                                    &nbsp;
                                </td>
                                <td width="172">
                                    <img src="<%#Eval("ElevationImageURL")%>" width="160" height="90" />
                                </td>
                                <td width="253">
                                    <a href="<%#Eval("HomeURL")%>" style="color: #0095D9; font-size: 18px; text-decoration: underline;">
                                        <%#Eval("Name")%></a> <span style="color: #666666;">(homesite
                                            <%#Eval("LotNumber")%>)</span>
                                    <p style="color: #666666; font-size: 12px; font-family: Arial, Helvetica, sans-serif">
                                        <%#Eval("SquareFeet")%>
                                        Sq. Ft. &nbsp; |&nbsp;
                                        <%#Eval("Bedrooms")%>
                                        Bed &nbsp; | &nbsp;
                                        <%#Eval("Baths")%>
                                        Bath&nbsp; | &nbsp;
                                        <%#Eval("Garage")%>
                                        Garage &nbsp; |&nbsp;
                                        <%#Eval("Stories")%>
                                        Stories
                                    </p>
                                    <span style="color: #19398A; float: left; font-size: 12px;"><%#Eval("StatusForAvailability")%> </span>
                                    <span style="color: #19398A; float: left; font-size: 16px; margin: -3px 0 0 10px;">$<%#Eval("Price")%>
                                    </span>
                                </td>
                                <td width="100">
                                    <table width="40" align="left" cellspacing="0" style="width: 100px; <%# Eval("RealtorAmount").ToString().Length == 0 ? "display:none": "" %>;">
                                        <tr style="background: <%= BASIC_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 16px; font-weight: bold; color: #fff;">$<%#Eval("RealtorAmount")%></span>
                                            </td>
                                        </tr>
                                        <tr style="background: <%= BASIC_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 11px; font-weight: bold; color: #fff;">Bonus </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="134">
                                    <table width="40" align="left"  cellspacing="0" style="width: 100px; 
                                        <%# Eval("RealtorRate").ToString().Length == 0 ? "display:none": "" %>;">
                                        <tr style="background: <%= BASIC_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 16px; font-weight: bold; color: #fff;">
                                                    <%#Eval("RealtorRate")%>%</span>
                                            </td>
                                        </tr>
                                        <tr style="background: <%= BASIC_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 11px; font-weight: bold; color: #fff;">CO-OP </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <p style="color: <%= BASIC_COLOR %>; float: left; font-size: 12px; margin: 7px 0 0;
                                        font-family: Arial, Helvetica, sans-serif;">
                                        <%# Eval("MLS").ToString().Length == 0 ? "" : "MLS: " + Eval("MLS") %>
                                    </p>
                                </td>
                                <td colspan="4">
                                    <a href="<%#Eval("HomeURL")%>">
                                        <img src="/Images/tm/view-home<%= IMAGE_SRC %>.png" width="139" height="35" border="0"></a>
                                    <a href="<%#Eval("PlanURL")%>">
                                        <img src="/Images/tm/view-plan<%= IMAGE_SRC %>.png" width="139" height="35" border="0"></a>
                                    <a href="<%#Eval("CommunityURL")%>">
                                        <img src="/Images/tm/view-com<%= IMAGE_SRC %>.png" width="185" height="35" border="0"></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="4">
                                     <a href="http://www.<%= URL_NAME %><%= REALTOR_URL %>/realtor-search/Realtor-Search-Results-in-<%#Eval("DivisionName")%>"
                                        style="float: right; color: <%= BASIC_COLOR %>; font-size: 14px; margin: 0 11px 15px;">
                                        Click Here for Advanced Search</a>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (DISPLAY_FEATURED)
                   {%>
                <table width="671" height="80" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2" rowspan="6" style="background: #EEEDEA">
                            <img src="<%= IHC_IMAGE %>" align="right">
                        </td>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td width="140" style="background: #EEEDEA">
                            <h1 style="color: #666666; float: left; font-size: 13px; margin: 0 0 2px; padding: 0;">
                                <%= IHC_NAME %></h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <span style="color: #666666; float: left; font-size: 11px;">
                                <% if (IHC_DRE != "DRE #") 
                                   {%>
                                   <%= IHC_DRE %>                                
                                <%} %>
                                <br>
                                Internet Home Consultant </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <p style="color: <%= BASIC_COLOR %>; float: left; font-size: 13px; margin: 1px 0 0;">
                                <%= IHC_PHONE %>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <% if (IHC_EMAIL != string.Empty)
                               {%>
                            <a href="mailto:<%= IHC_EMAIL %>" style="color: #0095D9; float: left; font-size: 12px;
                                margin: 0;">Contact Us</a>
                            <%
                               }%>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                        </td>
                    </tr>
                </table>
                <table width="671" border="0" cellpadding="0" cellspacing="0">
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td colspan="10">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td width="10">
                            &nbsp;
                        </td>
                        <td width="20">
                            <a href="<%= FACEBOOK %>">
                                <img src="/Images/tm/facebook.jpg" border="0"></a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="52">
                            <a href="<%= FACEBOOK %>" style="font-size: 11px; color: #fff; margin: 5px 0 0 5px; float: left;">
                                LIKE US </a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="20">
                            <a href="<%= TWITTER %>">
                                <img src="/Images/tm/twitter.jpg" border="0"></a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="74">
                            <a href="<%= TWITTER %>" style="font-size: 11px; color: #fff; margin: 5px 0 0 5px; float: left;">
                                FOLLOW US </a>
                        </td>
                        <td width="297">
                            &nbsp;
                        </td>
                        <td width="160">
                            <span style="font-size: 14px; font-weight: bold; color: #fff; float: right; margin: 4px 5px 0;">
                                <%= URL_NAME %> </span>
                        </td>
                    </tr>
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td colspan="10">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <%} if (DISPLAY_AVAILABLE)
                   {%>
                <table cellpadding="0" cellspacing="0">
                    <tr width="637">
                        <td>
                            &nbsp;
                        </td>
                        <td height="15" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr width="637">
                        <td width="10">
                            &nbsp;
                        </td>
                        <td width="200" height="33" style="background: <%= FEATURED_COLOR %>;">
                            <p style="color: #FFFFFF; font-size: 13px; font-weight: bold; text-align: center;
                                font-family: Arial, Helvetica, sans-serif;">
                                AVAILABLE HOMES</p>
                        </td>
                        <td width="427">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:Repeater runat="server" ID="rptAvailableHomes">
                   <ItemTemplate>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10">
                                    &nbsp;
                                </td>
                                <td style="border-bottom: 2px solid <%= FEATURED_COLOR %>; border-top: 2px solid <%= FEATURED_COLOR %>;
                                    width: 322px; height: 35px; font-weight: bold;">
                                    <%#Eval("PlanName")%>
                                </td>
                                <td style="border-bottom: 2px solid <%= FEATURED_COLOR %>; border-top: 2px solid <%= FEATURED_COLOR %>;
                                    width: 322px; height: 35px; text-align: right; font-weight: bold;">
                                    <%#Eval("CommunityName")%>, <%#Eval("City")%>, <%#Eval("State")%>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="10">
                                    &nbsp;
                                </td>
                                <td width="172">
                                    <img src="<%#Eval("ElevationImageURL")%>" width="160" height="90" />
                                </td>
                                <td width="253">
                                    <a href="<%#Eval("HomeURL")%>" style="color: #0095D9; font-size: 18px; text-decoration: underline;">
                                        <%#Eval("Name")%></a> <span style="color: #666666;">(homesite
                                            <%#Eval("LotNumber")%>)</span>
                                    <p style="color: #666666; font-size: 12px; font-family: Arial, Helvetica, sans-serif">
                                        <%#Eval("SquareFeet")%>
                                        Sq. Ft. &nbsp; |&nbsp;
                                        <%#Eval("Bedrooms")%>
                                        Bed &nbsp; | &nbsp;
                                        <%#Eval("Baths")%>
                                        Bath&nbsp; | &nbsp;
                                        <%#Eval("Garage")%>
                                        Garage &nbsp; |&nbsp;
                                        <%#Eval("Stories")%>
                                        Stories
                                    </p>
                                    <span style="color: #19398A; float: left; font-size: 12px;"><%# Eval("StatusForAvailability")%> </span><span
                                        style="color: #19398A; float: left; font-size: 16px; margin: -3px 0 0 10px;">$<%#Eval("Price")%>
                                    </span>
                                </td>
                                <td width="100">
                                    <table width="40" align="left" cellspacing="0" style="width: 100px; <%# Eval("RealtorAmount").ToString().Length == 0 ? "display:none": "" %>;">
                                        <tr style="background: <%= FEATURED_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 16px; font-weight: bold; color: #fff;">$<%#Eval("RealtorAmount")%></span>
                                            </td>
                                        </tr>
                                        <tr style="background: <%= FEATURED_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 11px; font-weight: bold; color: #fff;">Bonus </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="134">
                                    <table width="40" align="left"  cellspacing="0" style="width: 100px; 
                                        <%# Eval("RealtorRate").ToString().Length == 0 ? "display:none": "" %>;">
                                        <tr style="background: <%= FEATURED_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 16px; font-weight: bold; color: #fff;">
                                                    <%#Eval("RealtorRate")%>%</span>
                                            </td>
                                        </tr>
                                        <tr style="background: <%= FEATURED_COLOR %>;">
                                            <td align="center">
                                                <span style="font-size: 11px; font-weight: bold; color: #fff;">CO-OP </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <p style="color: <%= BASIC_COLOR %>; float: left; font-size: 12px; margin: 7px 0 0;
                                        font-family: Arial, Helvetica, sans-serif;">
                                        <%# Eval("MLS").ToString().Length == 0 ? "" : "MLS: " + Eval("MLS") %>
                                    </p>
                                </td>
                                <td colspan="4">
                                    <a href="<%#Eval("HomeURL")%>">
                                        <img src="/Images/tm/view-home<%= IMAGE_SRC %>.png" width="139" height="35" border="0"></a>
                                    <a href="<%#Eval("PlanURL")%>">
                                        <img src="/Images/tm/view-plan<%= IMAGE_SRC %>.png" width="139" height="35" border="0"></a>
                                    <a href="<%#Eval("CommunityURL")%>">
                                        <img src="/Images/tm/view-com<%= IMAGE_SRC %>.png" width="185" height="35" border="0"></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td colspan="4">
                                    <a href="http://www.<%= URL_NAME %><%= REALTOR_URL %>/realtor-search/Realtor-Search-Results-in-<%#Eval("DivisionName")%>"
                                        style="float: right;  color: <%= BASIC_COLOR %>; font-size: 14px; margin: 0 11px 15px;">
                                        Click Here for Advanced Search</a>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <table width="671" height="80" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2" rowspan="6" style="background: #EEEDEA">
                            <img src="<%= IHC_IMAGE %>" align="right">
                        </td>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td width="140" style="background: #EEEDEA">
                            <h1 style="color: #666666; float: left; font-size: 13px; margin: 0 0 2px; padding: 0;">
                                <%= IHC_NAME %></h1>
                        </td>
                    </tr>
                    <tr>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <span style="color: #666666; float: left; font-size: 11px;">
                               <% if (IHC_DRE != "DRE #") 
                                   {%>
                                   <%= IHC_DRE %>                                
                                <%} %>                          
                                <br>
                                Internet Home Consultant </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <p style="color: <%= BASIC_COLOR %>; float: left; font-size: 13px; margin: 1px 0 0;">
                                <%= IHC_PHONE %>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                            <% if (IHC_EMAIL != string.Empty)
                               {%>
                            <a href="mailto:<%= IHC_EMAIL %>" style="color: #0095D9; float: left; font-size: 12px;
                                margin: 0;">Contact Us</a>
                            <%
                               }%>
                        </td>
                    </tr>
                    <tr>
                        <td height="15" style="background: #EEEDEA">
                            &nbsp;
                        </td>
                        <td style="background: #EEEDEA">
                        </td>
                    </tr>
                </table>
                <table width="671" border="0" cellpadding="0" cellspacing="0">
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td colspan="10">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td width="10">
                            &nbsp;
                        </td>
                        <td width="20">
                            <a href="#">
                                <img src="/Images/tm/facebook.jpg" border="0"></a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="52">
                            <a href="#" style="font-size: 11px; color: #fff; margin: 5px 0 0 5px; float: left;">
                                LIKE US </a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="20">
                            <a href="#">
                                <img src="/Images/tm/twitter.jpg" border="0"></a>
                        </td>
                        <td width="5">
                            &nbsp;
                        </td>
                        <td width="74">
                            <a href="#" style="font-size: 11px; color: #fff; margin: 5px 0 0 5px; float: left;">
                                FOLLOW US </a>
                        </td>
                        <td width="297">
                            &nbsp;
                        </td>
                        <td width="160">
                            <span style="font-size: 14px; font-weight: bold; color: #fff; float: right; margin: 4px 5px 0;">
                                <%= URL_NAME %> </span>
                        </td>
                    </tr>
                    <tr style="background: <%= BASIC_COLOR %>;">
                        <td colspan="10">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <%
                   }%>
            </td>
        </tr>
    </table>
    <table style="padding: 0 0 0 19px; width: 651px;">
        <tr>
            <td>
                <asp:Literal ID="FooterText" runat="server"></asp:Literal></p> </div>
            </td>
        </tr>
    </table>
    <table>
        <tr>
          <td style="background:#f5f4f2; width:638px; height:40px;">
                <a href="#"><img src="/images/tm/iconF1.png" width="20" height="21" border="0"></a>
		<a href="#"><img src="/images/tm/iconF2.png" width="17" height="21" border="0"></a>
		<a href="#"><img src="/images/tm/iconF3.png" width="19" height="19" border="0"></a>
            </td>
        </tr>
    </table>
    <!--end of footer-shadow-->
</body>
</html>
