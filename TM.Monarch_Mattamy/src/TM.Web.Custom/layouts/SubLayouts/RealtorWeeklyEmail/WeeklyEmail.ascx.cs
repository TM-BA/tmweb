﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Data;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data.Items;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorWeeklyEmail
{
    public partial class WeeklyEmail : ControlBase
    {
        public string BASIC_COLOR = "#D31245";
        public string FEATURED_COLOR = "#1A3989";
        public string IMAGE_SRC = string.Empty;
        public bool DISPLAY_FEATURED = true;
        public bool DISPLAY_AVAILABLE = true;
        public const int MAX_COMMUNITY_NUMBER = 4;
        public string URL = string.Empty;
        public string IHC_IMAGE = string.Empty;
        public string IHC_NAME = string.Empty;
        public string IHC_PHONE = string.Empty;
        public string IHC_DRE = string.Empty;
        public string IHC_EMAIL = string.Empty;
        public string COMPANY_NAME = "Taylor Morrison";
        public string TITLE = "TaylorMorrison Home Builders and Real Estate for New Homes and Townhomes";
        public string URL_NAME = "taylormorrison.com";
        public string FACEBOOK = string.Empty;
        public string TWITTER = string.Empty;
        public string REALTOR_URL = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //var url = Sitecore.Context.Item.GetItemUrl();
            var domain = HttpContext.Current.Request.Url;
            URL = domain.AbsolutePath;
            FooterText.Text = Sitecore.Context.Item["FooterText"];
            REALTOR_URL = Sitecore.Context.Item.Parent.Parent.Parent.GetItemUrl();
            
            if (Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields["Facebook URL"].Value.IsNotEmpty())
                FACEBOOK = ((Sitecore.Data.Fields.LinkField) Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields["Facebook URL"]).Url;
            if (Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields["Twitter URL"].Value.IsNotEmpty())
                TWITTER = ((Sitecore.Data.Fields.LinkField) Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields["Twitter URL"]).Url;
            var featuredHomesSelected = Sitecore.Context.Item["FeaturedHomes"].Split('|');
            var featuredHomes = new List<Home>();
            var availableHomes = new List<Home>();
            if (featuredHomesSelected.Length > 0 && featuredHomesSelected[0] != string.Empty)
            {
                for (var i = 0; i != featuredHomesSelected.Length && i < MAX_COMMUNITY_NUMBER; i++)
                {
                    featuredHomes.Add(CreateHomeObject(new ID(featuredHomesSelected[i])));
                }
            }
            else
            {
                DISPLAY_FEATURED = false;
            }
            var plans = new PlanQueries().GetHomesForSaleForRWEInDivision(CurrentPage.CurrentDivision.ID);
            foreach (var plan in plans)
            {
                availableHomes.Add(CreateHomeObject(plan));
            }
            if (availableHomes.Count == 0)
            {
                DISPLAY_AVAILABLE = false;
            }

            //GetItem return community.Fields[SCIDs.CommunityFields.CommunityName].Value;
            var searchHelper = new SearchHelper();

            var ihcItem = searchHelper.GetIhcCardValues(Sitecore.Context.Item);
            IHC_IMAGE = "/images/tm/ihc_person.jpg";
            if (ihcItem != null)
            {
                
                if (!string.IsNullOrEmpty(ihcItem.Fields["Consultant Image"].Value))
                {

                    Sitecore.Data.Fields.ImageField imageField = ihcItem.Fields["Consultant Image"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        var image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                        IHC_IMAGE = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image)) + "?h=55";
                    }
                }

                IHC_NAME = ihcItem["First Name"] + " " + ihcItem["Last Name"];
                IHC_DRE ="DRE #" + ihcItem["Regulating Authority Designation"].Replace("\n", "<br />");
                IHC_PHONE = ihcItem["Phone"];
                IHC_EMAIL = ihcItem["Email Address"];
                // IHC1.Text = IHC2.Text = searchHelper.GetIHCCardDetailsForMailer(ihcItem);
            }
            //var company = CurrentPage.SCCurrentDeviceType;

            var company = Sitecore.Context.GetSiteName().ToLower();

            switch (company)
            {

                case CommonValues.DarlingHomesDomain:
                    BASIC_COLOR = "#513528";
                    FEATURED_COLOR = "#006A71";
                    IMAGE_SRC = "-dh";
                    COMPANY_NAME = "Darling Homes";
                    TITLE = "NewHomes by DarlingHomes";
                    URL_NAME = "darlinghomes.com";
                    break;
                case CommonValues.MonarchGroupDomain:
                    BASIC_COLOR = "#C02D2D";
                    FEATURED_COLOR = "#4D4D4D";
                    IMAGE_SRC = "-mg";
                    COMPANY_NAME = "Monarch Homes";
                    TITLE = "NewHomes by MonarchHomes";
                    URL_NAME = "monarchgroup.net";
                    break;
            }



            rptFeaturedResults.DataSource = featuredHomes;
            rptFeaturedResults.DataBind();
            rptAvailableHomes.DataSource = availableHomes;
            rptAvailableHomes.DataBind();

        }
        private Home CreateHomeObject(ID homeID)
        {
            var home = SCUtils.GetItem(homeID);
            var imageUrl = string.Empty;
            if (!string.IsNullOrWhiteSpace(home["Elevation Images"]))
            {
                string[] imgIds = home["Elevation Images"].Split('|');
                Item image = SCContextDB.GetItem(new ID(imgIds[0]));
                imageUrl = image != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(image) : Constants.PageUrls.NoImagePath;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(home.Parent["Elevation Images"]))
                {
                    string[] imgIds = home.Parent["Elevation Images"].Split('|');
                    Item image = SCContextDB.GetItem(new ID(imgIds[0]));
                   
                    imageUrl = image!=null? Sitecore.Resources.Media.MediaManager.GetMediaUrl(image) : Constants.PageUrls.NoImagePath;
                }

            }
            return new Home
             {
                 Name = home.GetItemName(),
                 SquareFeet = home.Fields[SCIDs.PlanBaseFields.SquareFootage].Value.CastAs<int>(0).ToString("n0"),
                 Address = home.Fields[SCIDs.HomesForSalesFields.StreetAddress1].Value,
                 State = home.Parent.Parent.Parent.Parent.Parent.GetItemName(),
                 City = home.Parent.Parent.Parent.GetItemName(),
                 Bedrooms = home.Fields[SCIDs.PlanBaseFields.NumberOfBedrooms].Value,
                 CommunityName = home.Parent.Parent.GetItemName(),
                 PlanName = home.Parent.GetItemName(),
                 Garage = home.Fields[SCIDs.PlanBaseFields.NumderOfGarages].Value,
                 CommunityURL = home.Parent.Parent.GetItemUrl(),
                 HomeURL = home.GetItemUrl(),
                 PlanURL = home.Parent.GetItemUrl(),
                 RealtorAmount = home.Fields[SCIDs.HomesForSalesFields.RealtorIncentiveAmount].Value,
                 RealtorRate = home.Fields[SCIDs.HomesForSalesFields.RealtorBrokerCommissionRate].Value,
                 MLS = home.Fields[SCIDs.HomesForSalesFields.MLS].Value,
                 Stories = home.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value,
                 LotNumber = home.Fields[SCIDs.HomesForSalesFields.LotNumber].Value,
                 //Hacer brete con los halfbathrooms
                 Baths = (home.Fields[SCIDs.PlanBaseFields.NumberOfBathrooms].Value.CastAs<int>(0) + home.Fields[SCIDs.PlanBaseFields.NumberOfHalfBathrooms].Value.CastAs<int>(0) * 0.5).ToString(),
                 Price = home.Fields[SCIDs.HomesForSalesFields.Price].Value.CastAs<int>(0).ToString("n0"),
                 DivisionName = home.Parent.Parent.Parent.Parent.GetItemName(),
                 ElevationImageURL = imageUrl,
                 StatusForAvailability = AvalabilityText(home)
             };

        }

        private string AvalabilityText(Item inventory)
        {
            string availabilityText;
            string statusForAvailability = inventory.Fields[SCIDs.HomesForSalesFields.StatusForAvailability].GetValue(string.Empty);

            if (Sitecore.Data.ID.IsID(statusForAvailability))
            {
                availabilityText = SCContextDB.GetItem(new ID(statusForAvailability)).Fields["Name"].GetValue(string.Empty);
            }
            else
            {
                availabilityText = GetAvailabilityFromDate(inventory);
            }

            return availabilityText;

        }

        private string GetAvailabilityFromDate(Item inventory)
        {
            
            var dateAvailable = DateUtil.ParseDateTime(inventory.Fields[SCIDs.HomesForSalesFields.AvaialabilityDate].Value,
                                                     default(DateTime));

            return dateAvailable == default(DateTime)? string.Empty :
                dateAvailable.CompareTo(DateTime.Now) < 0? "Available Now!": "Available in " + dateAvailable.ToString("MMMM");
        }

    }
    public class Home
    {
        public string Name { get; set; }
        public string SquareFeet { get; set; }
        public string MLS { get; set; }
        public string RealtorAmount { get; set; }
        public string RealtorRate { get; set; }
        public string Bedrooms { get; set; }
        public string Garage { get; set; }
        public string Stories { get; set; }
        public string HomeURL { get; set; }
        public string PlanURL { get; set; }
        public string CommunityURL { get; set; }
        public string PlanName { get; set; }
        public string CommunityName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string LotNumber { get; set; }
        public string Baths { get; set; }
        public string Price { get; set; }
        public string DivisionName { get; set; }
        public string ElevationImageURL { get; set; }
        public string StatusForAvailability { get; set; }

    }

}