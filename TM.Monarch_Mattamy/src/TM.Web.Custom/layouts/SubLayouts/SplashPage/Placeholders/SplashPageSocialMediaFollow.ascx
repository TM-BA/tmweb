﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashPageSocialMediaFollow.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.SplashPageSocialMediaFollow" %>
<ul class="rhs">
    <li>
        <sc:Text ID="txtTitle" runat="server" Field="Title"/>
    </li>
    <li runat="server" id="liFacebook" Visible="False">
        <a runat="server" id="hrefFacebook" href="#" target="_blank">Visit our <img src="/images/tm/iconFb.png"/> Facebook page.</a>
    </li>
    <li runat="server" id="liTwitter" Visible="False">
        <a runat="server" id="hrefTwitter" href="#" target="_blank">Visit our <img src="/images/tm/iconTwitter.png"/> Twitter page.</a>
    </li>
    <li runat="server" id="liYoutube" Visible="False">
        <a runat="server" id="hrefYoutube" href="#" target="_blank">Visit our <img src="/images/tm/iconTube.png"/> YouTube page.</a>
    </li>
    <li runat="server" id="liPinterest" Visible="False">
        <a runat="server" id="hrefPinterest" href="#" target="_blank">Visit our <img src="/images/tm/iconPinterest.png"/> Pinterest page.</a>
    </li>
    <li runat="server" id="liGoogle" Visible="False">
        <a runat="server" id="hrefGoogle" href="#" target="_blank">Visit our <img src="/images/tm/iconGoogle.png"/> Google+ page.</a>
    </li>
    <li runat="server" id="liBlog" Visible="False">
        <a runat="server" id="hrefBlog" href="#" target="_blank">Visit our <img src="/images/tm/iconY.png"/> Blog page.</a>
    </li>
</ul>
<div><sc:Text ID="txtContent" runat="server" Field="Content"/></div>
<span runat="server" Visible="False" id="spanNoDataSource">No datasource is associated with this Social Media Follow Component</span>