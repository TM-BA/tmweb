﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class SplashPageSocialMediaShare : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item source = DataSource;
            if (DataSource == null)
            {
                this.spanNoDataSource.Visible = true;
                return;
            }
            else
            {
                this.spanNoDataSource.Visible = false;
            }
            txtContent.DataSource = source.ID.ToString();
            txtContent.DataBind();

            txtTitle.DataSource = source.ID.ToString();
            txtTitle.DataBind();

            if (source.Fields["Show Facebook"] != null)
            {
                if (source.Fields["Show Facebook"].Value == "1")
                    this.litFacebook.Text = source.Fields["Facebook Script"].Value;
            }

            if (source.Fields["Show GooglePlus"] != null)
            {
                if (source.Fields["Show GooglePlus"].Value == "1")
                    this.litGoogle.Text = source.Fields["GooglePlus Script"].Value;
            }

            if (source.Fields["Show Twitter"] != null)
            {
                if (source.Fields["Show Twitter"].Value == "1")
                    this.litTwitter.Text = source.Fields["Twitter Script"].Value;
            }


            
            
            
        }
    }
}