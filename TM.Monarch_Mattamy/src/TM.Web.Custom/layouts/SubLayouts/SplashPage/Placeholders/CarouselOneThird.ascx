﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarouselOneThird.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.CarouselOneThird" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%if (LoadScripts && !IsPrintMode)
  {%>
  
  <%= JsCssCombiner.GetSet("photorotatorjs")%>


<link href="/Styles/lib/slideshow.css" rel="stylesheet" type="text/css" />
<% } %>
<div id="carouselOneThird" runat="server">
    <div class="slideshow" data-transition="crossfade" data-loop="true">
    <a Visible="false" ID="hypPrevious" runat="server" href="javascript:;" class="prev-arrow"></a>
		<ul class="carousel <%=ContainerClass %>">
			<asp:Literal ID="litSlideImages" runat="server"></asp:Literal>
		</ul>
    <a Visible="false" ID="hypNext" runat="server" href="javascript:;" class="next-arrow"></a>
	</div>
</div>
   
<%if (LoadScripts && !IsPrintMode)
  { %>				
	<script type="text/javascript">
	     NS("TM.Rotator");
	    $j(document).ready(function () {
	        // Create slideshow instances
	       var slides = $j('#<%= carouselOneThird.ClientID %> .slideshow').slides(<%= SlideOptions %>);
	        TM.Rotator.$slidesObj = slides.data('slides');
	        $j("a.expandImg").colorbox({ rel: 'expandImg' });
	    });
      
	</script>
	
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">        stLight.options({ publisher: "f3415b4a-0575-4cae-b53d-be5261aafa02", doNotHash: true, doNotCopy: true, hashAddressBar: false });</script>

  <% } %>
  <%else{%>
  <%--<script type="text/javascript">
      NS("TM.Rotator");
      TM.Rotator.get_slideOptions = function() { return <%= SlideOptions %>; };
  </script>--%>
  <% } %>
  <span runat="server" Visible="False" id="spanNoDataSource">No datasource is associated with this Carosuel Component</span>