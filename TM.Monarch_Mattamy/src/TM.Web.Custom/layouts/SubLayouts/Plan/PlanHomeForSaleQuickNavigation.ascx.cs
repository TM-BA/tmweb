﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Comparers;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanHomeForSaleQuickNavigation : ControlBase
    {
        protected bool IsCurrentItemAPlan;
        protected string ImageURL { get; set; }
        protected string PrevLink { get; set; }
        protected string NextLink { get; set; }
        protected int DisplayCurrentIndex { get; set; }

        protected int TotalNumberOfPlans { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
	    //HFS = home for sale
            var currentPlanHFS = SCContextItem;
            IsCurrentItemAPlan = currentPlanHFS.TemplateID == SCIDs.TemplateIds.PlanPage;
            var elevationImageFieldID = IsCurrentItemAPlan
                                            ? SCIDs.PlanFields.ElevationImages
                                            : SCIDs.HomesForSalesFields.ElevationImages;


            var images = (MultilistField)SCContextItem.Fields[elevationImageFieldID];
            var urlList = images.GetMediaUrl(0);
            if (urlList.Count != 0)
            {
                ImageURL = images.GetMediaUrl(0)[0];
            }
            else
            {
                 images = (MultilistField)SCContextItem.Fields[SCIDs.PlanBaseFields.Interior];
                 urlList = images.GetMediaUrl(0);
                 if (urlList.Count != 0)
                 {
                     ImageURL = images.GetMediaUrl(0)[0];
                 }
            }


            string cacheKeySuffix = IsCurrentItemAPlan
                                        ? CurrentPage.CurrentCommunity.ID.ToString()
                                        : currentPlanHFS.ParentID.ToString();

            var cacheKey = CacheKeys.GetPlanQuickNavKey(cacheKeySuffix);
            
	    var siblingIDs = WebCache.Get(cacheKey,GetQuickNavItems);

            
            var currentIndex = siblingIDs.FindIndex(s => s == SCContextItem.ID.ToString());
            TotalNumberOfPlans = siblingIDs.Count;
            currentIndex = currentIndex < 0 ? 0 : currentIndex;
            if (TotalNumberOfPlans > 0)
            {
                var prevIndex = currentIndex <= 0 ? TotalNumberOfPlans - 1 : currentIndex - 1;
                var nextIndex = currentIndex == TotalNumberOfPlans - 1 ? 0 : currentIndex + 1;

                PrevLink = SCContextDB.GetItem(siblingIDs[prevIndex]).GetItemUrl();
                NextLink = SCContextDB.GetItem(siblingIDs[nextIndex]).GetItemUrl();
                DisplayCurrentIndex = currentIndex + 1;
            }


            divQuickNav.Visible = TotalNumberOfPlans > 1;

        }



        public List<string> GetQuickNavItems()
	{

	    var allSiblingsOfSameTypeQuery = SCFastQueries.GetMySiblings(SCContextItem);
	    var allSiblings = SCContextDB.SelectItems(allSiblingsOfSameTypeQuery);

            var list = allSiblings.ToList();

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage)
            {
                var orderedList = list.Where(l => l[SCIDs.PlanFields.PlanStatus]==SCIDs.StatusIds.Status.Active).OrderBy(a => a.Fields[SCIDs.PlanFields.PricedFromValue].Value.CastAs<int>(0));
                return orderedList.Select(o => o.ID.ToString()).ToList();
            }
            else
            {
                var orderedList = list.Where(l => l[SCIDs.HomesForSalesFields.HomesForSaleSatus]==SCIDs.StatusIds.Status.Active).OrderBy(a => a.Parent[SCIDs.PlanFields.PlanName])
                    .ThenBy(b => b[SCIDs.HomesForSalesFields.Price].CastAs<int>(0))
                    .ThenBy(c => c[SCIDs.HomesForSalesFields.StreetAddress1]);


                return orderedList.Select(o => o.ID.ToString()).ToList();
            }


	}


       
    }

   
}