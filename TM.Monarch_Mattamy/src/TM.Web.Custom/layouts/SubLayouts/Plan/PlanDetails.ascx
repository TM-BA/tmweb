﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlanDetails.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlanDetails" %>
<%@ Import Namespace="TM.Utils.Extensions" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Import Namespace="TM.Web.Custom.SCHelpers" %>
<%if (!IsPrint)
  {%>
     <%=JsCssCombiner.GetSet("plandetailjs")%>
   
    <link href="/Styles/lib/slideshow.css" rel="stylesheet" type="text/css" />
    <div id="xhrPrompt" style="display: none;" >
        <p><strong>Loading...</strong></p>
    </div>
    <style>
        #xhrPrompt {
            position: fixed;
            top: 50%;
            left: 50%;
            margin-left: -145px;
            padding: 15px 50px;
            z-index: 100000000;
            border: 2px solid #000;
            background: #fff;
            border-radius: 4px;
        }

    </style>
 <% } %>
<%else{ %>
<div class="print-top">
<h1 class="pn2"><%= ItemName %></h1><img src="<%=CompanyLogo %>"/></div>
<% } %>			
<div class="plan-details-top">
    <sc:Placeholder runat="server" ID="phPlanCommunityHeader" Key="PlanCommunityHeader"/>
</div>
<!--end plan-details-top-->
<sc:Placeholder runat="server" ID="phPlanMenu" Key="PlanMenu"/>
			   
<div class="clearfix">
</div>
		
    


