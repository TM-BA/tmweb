﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlanMenu.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlanMenu" %>
<%@ Import Namespace="TM.Web.Custom.SCHelpers" %>
<ul class="planMenu">
    <li class="pd1" id="liFloorPlans" runat="server"><a onclick="TM.Common.GAlogevent('Details', 'Click', 'Tab-FloorPlan');"
        href="<%=CurrentPage.SCCurrentItemUrl%>/floorplans">FLOOR PLAN</a></li>
    <li class="pd2" id="liPhotos" runat="server"><a onclick="TM.Common.GAlogevent('Details', 'Click', 'Tab-Photos');"
        href="<%=CurrentPage.SCCurrentItemUrl%>/photos">PHOTOS</a> </li>
    <li class="pd3" id="liLocations" runat="server"><a onclick="TM.Common.GAlogevent('Details', 'Click', 'Tab-Locations');"
        href="<%=CurrentPage.SCCurrentItemUrl%>/locations">LOCATIONS</a> </li>
    <li class="pd4" id="liOptions" runat="server" ><a onclick="TM.Common.GAlogevent('Details', 'Click', 'Tab-Options');"
        href="<%=IFPUrl %>" target="_blank">OPTIONS</a> </li>
    <li class="pd5" id="liVirtualTour" runat="server"><a onclick="TM.Common.GAlogevent('Details', 'Click', 'Tab-VirtualTour');"
        runat="server" id="aVirtualTour" target="_blank">VIRTUAL TOUR</a></li>
</ul> 

