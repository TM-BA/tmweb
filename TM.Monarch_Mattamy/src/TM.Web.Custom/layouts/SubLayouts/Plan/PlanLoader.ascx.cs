﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanLoader : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var pageToLoad = Request.Url.LastSegment();
           
            LoadPage(pageToLoad);
        }

      
        private void LoadPage(string page)
	{
	
	    switch (page)
	    {
              case "photos":
		phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanPhotos.ascx"));
	            break;
            case "locations":
                phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/HomesForSale.ascx"));
		phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlansAlsoAvailable.ascx"));
	            break;
            case "floorplans":
            default:
                phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanPhotos.ascx"));
	            break;
            case "options":
                phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanTourOptions.ascx"));
                break;
           




	    }
	}
    }
}