﻿using System;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanDescription : ControlBase
    {

        protected string PlanName { get; set; }
        protected string PlanInfo { get; set; }
        protected string Description { get; set; }
        protected string TourOptionsText { get; set; }
      
        protected string CommunityRequestInfo
        {
            get { return SCContextItem.Parent.GetItemUrl() + "/request-information/"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var isCurrentItemAPlan = SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage;


            var homeAttributes = new HomeAttributes().GetHomeAttributes();
 
            var infoPrefix = isCurrentItemAPlan ? "From" : AvalabilityText();

            string stories = "";

              if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
              { 
                  stories = homeAttributes.Stories==1?"Story":"Stories";
              }else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
              {
                  stories = homeAttributes.Stories == 1 ? "Storey" : "Storeys";
              
              }else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
              {
                  stories = homeAttributes.Stories == 1 ? "Story" : "Stories";
              }

            var price = string.Empty;
	    if(isCurrentItemAPlan && !string.IsNullOrWhiteSpace(homeAttributes.PriceText))
	    {
	        price = homeAttributes.PriceText;
	        infoPrefix = string.Empty;
	    }
	    else
	    {
	        price = homeAttributes.Price.ToString("c0");
	    }
            
            string planInfo = "{0} {1:c0} | {2:n0} Sq. Ft. | {3:n0} Bedrooms | {4} Baths | {5:n0} Garage | {6:n0} {7}"
                                                  .FormatWith(infoPrefix,
                                                                   price,
                                                                 homeAttributes.SqFt,
                                                                 homeAttributes.Bedrooms,
                                                                 homeAttributes.BathCountForDisplay,
                                                                 homeAttributes.Garage,
                                                                 homeAttributes.Stories,
                                                                 stories
                                                                 );

            //clean empty values
            var regex = new System.Text.RegularExpressions.Regex(@"\| 0\.*0* \d*\w*");
            PlanInfo = regex.Replace(planInfo, string.Empty);

            Description = homeAttributes.Description;
            
            PlanName = isCurrentItemAPlan
                           ? SCContextItem[SCIDs.PlanFields.PlanName]
                           : GetInventoryName();


            if (isCurrentItemAPlan)
            {
              
                SetTourOptionsText();
            }
	    //if visibility has not already been set by SetTourOptionsText 
	    //todo:could refactor for more clarity
            if (ulScheduleAnAppt.Visible)
            {
                ulScheduleAnAppt.Visible = isCurrentItemAPlan;
            }
        }

       

        private void SetTourOptionsText()
	{
	    Item nearByCommunity;
	    Item home;
	    var homeTourOptions = new PlanQueries().GetHomeTourOption(SCContextItem, CurrentPage.CurrentDivision,
	                                                            CurrentPage.CurrentCommunity, out nearByCommunity,out home);
	    switch (homeTourOptions)
	    {
	            case HomeTourOption.AvailableAsModelHome:
                TourOptionsText = "The {0} is available to tour today in a model home"
                          .FormatWith(PlanName);
	            break;
		case HomeTourOption.AvailableAsModelHomeInNearByCommunity:
		 TourOptionsText = "A model home is available in <a href=\"{1}\\floor-plans\" target=\"_blank\">{0}</a>"
                            .FormatWith(
                                nearByCommunity.Fields[SCIDs.CommunityFields.CommunityName].Value,
                                nearByCommunity.GetItemUrl());

                        ulScheduleAnAppt.Visible = false;
		break;
		case HomeTourOption.AvailableAsInventory:
                        TourOptionsText = "The {0} is available to tour today in a home for sale."
                                           .FormatWith(PlanName);
	            break;
            default:
	            TourOptionsText = string.Empty;
	            break;
	    }

	}

        private string GetInventoryName()
        {
            var lotNumber = SCContextItem.DisplayName.Split('-');
            var lotNumberStr = string.Empty;
	    if(lotNumber.Length >1)
	    {
	        lotNumberStr = "(lot {0})".FormatWith(lotNumber[1]);
	    }
            var currentCity = CurrentPage.CurrentCity.GetItemName();
            var currentState = CurrentPage.CurrentState.GetItemName(true);
            var zipCode = CurrentPage.CurrentCommunity["Zip or Postal Code"];
            var address1 = SCContextItem[SCIDs.HomesForSalesFields.StreetAddress1];
	    if(address1.IsNotEmpty())
	    {
	        address1 = address1.Trim();
	    }

            var address2 = "{0}, {1} {2}".FormatWith(currentCity, currentState, zipCode);
            return ("{0} at {1}, {2} <span class='invLot'>{3}</span>").FormatWith(
                SCContextItem.Parent[SCIDs.PlanFields.PlanName],
                address1, 
                address2, 
                lotNumberStr);
        }

        private string AvalabilityText()
        {
            string availabilityText;
            var inventory = SCContextItem;
            var statusForAvailability = inventory.Fields[SCIDs.HomesForSalesFields.StatusForAvailability].GetValue(string.Empty);
	  
	    if(Sitecore.Data.ID.IsID(statusForAvailability))
	    {
		 availabilityText = SCContextDB.GetItem(new ID(statusForAvailability)).Fields["Name"].GetValue(string.Empty);
	    }
	    else
	    {
                 availabilityText = GetAvailabilityFromDate();
	    }

            return availabilityText!=string.Empty?"<span class='invavltxt'>{0}  </span>".FormatWith(availabilityText):string.Empty;

        }

        private string GetAvailabilityFromDate()
        {
            var inventory = SCContextItem;
            var dateAvailable = DateUtil.ParseDateTime(inventory.Fields[SCIDs.HomesForSalesFields.AvaialabilityDate].Value,
                                                     default(DateTime));

            return dateAvailable == default(DateTime)
                                   ? string.Empty
                                   : dateAvailable.CompareTo(DateTime.Now)<0
				   ? "Available Now!"
                                   : "Available in " + dateAvailable.ToString("MMMM");
        }


    }
}