﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts.Common;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanDetails : ControlBase
    {
        protected bool IsPrint;

	protected void Page_Load(object sender, EventArgs e)
	{
        IsPrint = Request["sc_device"].IsNotEmpty();
	}

        protected string ItemName
        {
            get
            {
                var isCurrentItemAPlan = SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage;
                return isCurrentItemAPlan
                           ? SCContextItem[SCIDs.PlanFields.PlanName]
                           : GetInventoryName();
            }
        }

        protected string CompanyLogo
        {
            get { return CurrentPage.CurrentHomeItem.Fields["Company Logo"].GetMediaUrl(); }
        }

        private string GetInventoryName()
        {
            var lotNumber = SCContextItem.DisplayName.Split('-');
            var lotNumberStr = string.Empty;
            if (lotNumber.Length > 1)
            {
                lotNumberStr = "(lot {0})".FormatWith(lotNumber[1]);
            }
            var currentCity = CurrentPage.CurrentCity.GetItemName();
            var currentState = CurrentPage.CurrentState.GetItemName(true);
            var zipCode = CurrentPage.CurrentCommunity["Zip or Postal Code"];
            var address1 = SCContextItem[SCIDs.HomesForSalesFields.StreetAddress1];
            if (address1.IsNotEmpty())
            {
                address1 = address1.Trim();
            }

            var address2 = "{0}, {1} {2}".FormatWith(currentCity, currentState, zipCode);
            return ("{0} at {1}, {2} <span class='invLot'>{3}</span>").FormatWith(
                SCContextItem.Parent[SCIDs.PlanFields.PlanName],
                address1,
                address2,
                lotNumberStr);
        }
    }
}