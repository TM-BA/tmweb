﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;
using ComFldIDs = TM.Web.Custom.Constants.SCIDs.CommunityFields;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlansAlsoAvialable : ControlBase
    {
        protected string PlanName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Item currentPlan = SCContextItem;
            string currentPlanPath = SCCurrentItemPath;
            if (SCContextItem.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
            {
                currentPlan = SCContextItem.Parent;
            }
            PlanName = currentPlan.Fields[SCIDs.PlanFields.PlanName].Value;
            var masterPlanID = currentPlan.Fields[SCIDs.PlanFields.MasterPlanID].Value;

            var plansAlsoAvailableCommunityIDs =
                new CommunityQueries().GetCommunitiesMatchingMasterPlanInDivision(CurrentPage.CurrentDivision.ID,
                                                                                  masterPlanID);

            var plansAlsoAvialableCommunityList = new List<PlansAlsoAvailableCommunity>();

            foreach (var community in plansAlsoAvailableCommunityIDs)
            {
                if (community == CurrentPage.CurrentCommunity.ID)
                    continue;

                var plansAlsoAvailableCommunity = new PlansAlsoAvailableCommunity();

                HydratePlansAlsoAvailable(plansAlsoAvailableCommunity, community, masterPlanID);

                plansAlsoAvialableCommunityList.Add(plansAlsoAvailableCommunity);
            }

            if (plansAlsoAvialableCommunityList.Any())
            {
                rptPlansAlsoAvailableCommunities.DataSource = plansAlsoAvialableCommunityList;
                rptPlansAlsoAvailableCommunities.DataBind();
            }
            else
            {
                var areHomesAvailable = HttpContext.Current.Items["IsHomesForSaleAsloAvailable"];
                if (areHomesAvailable != null)
                {
                    divNoPlanOrHfsMessage.Visible = true;
                }

                divViewPlanBox.Visible = false;

            }
        }

        private void HydratePlansAlsoAvailable(PlansAlsoAvailableCommunity plansAlsoAvailableCommunity, ID community, string masterPlanID)
        {
            var communityItem = SCContextDB.GetItem(community);

            if (communityItem == null || communityItem[SCIDs.CommunityFields.CommunityStatusField] == SCIDs.StatusIds.CommunityStatus.InActive.ToString())
                return;

            plansAlsoAvailableCommunity.CommunityLink = communityItem.GetItemUrl();
            plansAlsoAvailableCommunity.CommunityName = communityItem.Fields[ComFldIDs.CommunityName].Value;
            var commAddress = new string[]
                                  {
                                      communityItem.Fields[ComFldIDs.StreetAddress1].Value,
                                      communityItem.Fields[ComFldIDs.StreetAddress2].Value,
                                      communityItem.Fields[ComFldIDs.ZipOrPostalCode].Value
                                  };

            plansAlsoAvailableCommunity.CommunityAddress = string.Join(", ", commAddress.Where(c => !string.IsNullOrEmpty(c)));

            var plans = new PlanQueries().GetPlansInCommunityMatchingMasterPlan(community,
                                                                                                  masterPlanID);

            var forFields = new[]
                                {
                                    SCFieldNames.PlanPageFields.PricedFromValue, SCFieldNames.PlanBaseFields.NumberOfBedrooms, SCFieldNames.PlanBaseFields.NumberOfBathrooms
                                };

            const int defaultPrice = 0;
            const int defaultBedroomCount = 0;
            const int defaultBathroomCount = 0;
            var typeConvertorCollection = new Func<string, double>[]
                                              {
                                                  price => price.CastAs<float>(defaultPrice),
						  bedrooms => bedrooms.CastAs<int>(defaultBedroomCount),
						  bathrooms => bathrooms.CastAs<int>(defaultBathroomCount),
                                              };

            var ranges = SCUtils.GetRangeValuesFrom(plans, forFields, typeConvertorCollection);

            var priceMin = ranges[SCFieldNames.PlanPageFields.PricedFromValue + "min"];
            var bedMin = ranges[SCFieldNames.PlanBaseFields.NumberOfBedrooms + "min"];
            var bedMax = ranges[SCFieldNames.PlanBaseFields.NumberOfBedrooms + "max"];
            var bathMin = ranges[SCFieldNames.PlanBaseFields.NumberOfBathrooms + "min"];
            var bathMax = ranges[SCFieldNames.PlanBaseFields.NumberOfBathrooms + "max"];

            var priceRange = priceMin == defaultPrice ? string.Empty : priceMin.ToString("C0");

            /* Removed per ticket 62606
            var bedRoomsRange = bedMin == defaultBedroomCount?string.Empty: bedMin + "-" + bedMax;
            var bathRoomsRange = bathMin == defaultBathroomCount ? string.Empty : bathMin + "-" + bathMax;
            */

            plansAlsoAvailableCommunity.CommunityPriceRange = priceRange;

            plansAlsoAvailableCommunity.CommunityBedroomRange = bedMax.ToString();
            plansAlsoAvailableCommunity.CommunityBathroomRange = bathMax.ToString();
        }
    }
}