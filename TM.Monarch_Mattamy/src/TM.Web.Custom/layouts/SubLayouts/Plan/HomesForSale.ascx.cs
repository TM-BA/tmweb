﻿using System;
using System.Linq;
using System.Web;
using Sitecore;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;
using PlanIDs = TM.Web.Custom.Constants.SCIDs.PlanFields;
using HFSIDs = TM.Web.Custom.Constants.SCIDs.HomesForSalesFields;
using PlanbaseIDs = TM.Web.Custom.Constants.SCIDs.PlanBaseFields;
namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class HomesForSale : ControlBase
    {
        protected string PlanName { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var currentPlan = SCContextItem;
	    
            if (SCContextItem.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
            {
                currentPlan = SCContextItem.Parent;
            }
            PlanName = currentPlan.Fields[PlanIDs.PlanName].Value;

            var homesForSale = new PlanQueries().GetActiveHomesForSaleUnderPlan(currentPlan.ID);


            var homesForSaleListings = homesForSale.Select(ItemToHomesForListingMapper);
           

            if (homesForSaleListings.Any())
            {

                rptHomesForSale.DataSource = homesForSaleListings;
                rptHomesForSale.DataBind();
            }
            else
            {
                divViewPlanBox.Visible = false;
                HttpContext.Current.Items["IsHomesForSaleAsloAvailable"] =false;
            }


        }

        public HomesForSaleListing ItemToHomesForListingMapper(TMSearchResultItem skinnyHomesForSale)
        {
            var homesForSale = skinnyHomesForSale.GetItem();
            var homesForSaleListing = new HomesForSaleListing();
            homesForSaleListing.Link = homesForSale.GetItemUrl();
            homesForSaleListing.Address = homesForSale.Fields[HFSIDs.StreetAddress1].Value;
            homesForSaleListing.Price = homesForSale.Fields[HFSIDs.Price].Value.FormatCurrency(string.Empty);
            homesForSaleListing.Bed = homesForSale.Fields[PlanbaseIDs.NumberOfBedrooms].Value;
            homesForSaleListing.Bath = homesForSale.Fields[PlanbaseIDs.NumberOfBathrooms].Value.CastAs<int>(0) +
                                       homesForSale.Fields[PlanbaseIDs.NumberOfHalfBathrooms].Value.CastAs<int>(0) * 0.5;
            homesForSaleListing.Garage = homesForSale.Fields[PlanbaseIDs.NumderOfGarages].Value;

            homesForSaleListing.Stories = homesForSale.Fields[PlanbaseIDs.NumberOfStories].Value;
            var dateAvailable = DateUtil.ParseDateTime(homesForSale.Fields[HFSIDs.AvaialabilityDate].Value,
                                                       default(DateTime));

            homesForSaleListing.Availability = dateAvailable==default(DateTime)? string.Empty : dateAvailable.ToString("MMMM");

            return homesForSaleListing;
        }
    }
}