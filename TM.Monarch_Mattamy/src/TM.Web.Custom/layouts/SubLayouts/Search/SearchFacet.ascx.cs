﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using TM.Utils.Extensions;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;

namespace TM.Web.Custom.layouts.SubLayouts.Search
{
	public partial class SearchFacet : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string mode = SCContextItem.Fields["Mode"].Value;
			liturl.Text = mode;

			if (mode.ToLower() == "home for sale")
			{
				var searchHelper = new SearchHelper();
				var diviItems = searchHelper.GetAllCrossDivisions();
				foreach (var item in diviItems)
				{
					var listItem = new ListItem(item.Name, item.ID.ToString());

					listItem.Attributes.Add("optgroup", item.Domain.GetCurrentCompanyName());
					ddldivision.Items.Add(listItem);
				}
				ddldivision.Items.Insert(0, new ListItem("No preference", "0"));
			}
		}

	}
}
