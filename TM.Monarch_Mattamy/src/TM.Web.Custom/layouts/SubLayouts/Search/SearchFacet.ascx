﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchFacet.ascx.cs"
	Inherits="TM.Web.Custom.layouts.SubLayouts.Search.SearchFacet" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Register TagPrefix="tm" Namespace="TM.Web.Custom.WebControls" Assembly="TM.Web.Custom" %>

<link href="/Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<%=JsCssCombiner.GetSet("searchfacetjs") %>
<style>
	.lihdr
	{
		display: none;
	}
</style>
<script>
	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		$j(window).load(function () { SetSearchFacet(searchpath); });

		var hnddiv = $j("#hnddivision").val();

		$j("#" + hnddiv).change(function () {
			GetCitiesByDivision($j(this).val());
		});

		$j("#selectcity").change(function () { BindSearchFacet(); });
		$j("#selectBonusRoomDen").change(function () { BindSearchFacet(); });
		$j("#selectSchoolDistrict").change(function () { BindSearchFacet(); });
		$j("#selectCommunityStatus").change(function () { BindSearchFacet(); });
		$j("#selectAvailability").change(function () { BindSearchFacet(); });
	});
	
</script>
<div id="searchfacet" class="searchfact">
	<div class="facetprogress">
		<div>
			Loading...</div>
	</div>
	<input type="hidden" id="hnddivision" value="<%= ddldivision.ClientID%>" />
	<div id="selectAreaBox">
		<p>
			Select Area:</p>
		<div>
			<tm:ExtendedDropDownList runat="server" ID="ddldivision" />
		</div>
	</div>
	<div id="sdrpriceRange" class="range">
		<p>
			Starting Price:<span id="startingprice"></span>
		</p>
		<div id="sldrrange-StartingPrice">
		</div>
	</div>
	<div id="selectcityBox">
		<p>
			City:</p>
		<div>
			<select id="selectcity" onclick="SetGAEventbyVal('selectcity','Search');">
			</select>
		</div>
	</div>
	<div id="lblBedsBox">
		<p>
			Bed:</p>
		<div>
			<ul id="lblBeds">
			</ul>
		</div>
	</div>
	<div id="lblBathsBox">
		<p>
			Bath:</p>
		<div>
			<ul id="lblBaths">
			</ul>
		</div>
	</div>
	<div id="lblGaragesBox">
		<p>
			Garage:</p>
		<div>
			<ul id="lblGarages">
			</ul>
		</div>
	</div>
	<div id="lblStoriesBox">
		
       <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
       { %> <p>Stories:</p> <% } 
       else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
       {  %>  <p>Storeys:</p> <% } 
       else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
       {  %>  <p>Stories:</p> <% } %>
        
		<div>
			<ul id="lblStories">
			</ul>
		</div>
	</div>
	<div id="selectBonusRoomDenBox">
		<p>
			Additional Rooms:</p>
		<div>
			<select id="selectBonusRoomDen" class="bonden" onclick="SetGAEventbyVal('selectBonusRoomDen','Search');">
			</select></div>
	</div>
	<div id="sdrSqftRange" class="range">
		<p>
			Square Feet:<span id="squarefeet"></span>
		</p>
		<div id="sldrrange-SquareFeet" >
		</div>
	</div>
	<div id="selectSchoolDistrictBox">
		<p>
			School District:</p>
		<div>
			<select id="selectSchoolDistrict" onclick="SetGAEventbyVal('selectSchoolDistrict','Search');">
			</select></div>
	</div>
	<div id="selectCommunityStatusBox">
		<p>
			Community Status:</p>
		<div>
			<select id="selectCommunityStatus" onclick="SetGAEventbyVal('selectCommunityStatus','Search');"></select></div>
	</div>
	<div id="selectAvailabilityBox">
		<p>
			Availability:</p>
		<div>
			<select id="selectAvailability" onclick="SetGAEventbyVal('selectAvailability','Search');">
			</select></div>
	</div>
	<div>
		<a class="facetclear"  href="javascript:ClearSearchFacet();">clear selections</a>
	</div>
</div>
