﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Layouts;
using Sitecore.Data.Items;
using System.Collections;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.layouts.SubLayouts.Search
{
	public partial class MG_Search : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			liturl.Text = SCContextItem.Paths.FullPath;
			var tmUser = CurrentWebUser;
			litcomFav.Text = string.Empty;
			if (tmUser != null)
				litcomFav.Text = tmUser.CommunityFavorites;			
		}
	}
}