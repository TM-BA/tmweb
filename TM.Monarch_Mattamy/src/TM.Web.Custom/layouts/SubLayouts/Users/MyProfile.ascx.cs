﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using TM.Domain;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class MyProfile : ControlBase
	{
		public string Mycompany = string.Empty;
		public string MycompanySmall = string.Empty;
		public string MycompanyLogo = string.Empty;
		public string MycompanyEmail = string.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;
			var tmUser = CurrentWebUser;
			vlsErrorMessage.Enabled = false;
			if (tmUser == null)
			{
				ClientScriptManager cs = Page.ClientScript;
				cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(false, false);", true);
				Redirect();
			}
			else
			{
				tmUser = tmUser.Get(true);
				txtFirstName.Text = tmUser.FirstName;
				txtLastName.Text = tmUser.LastName;
				txtEmailAddress.Text = tmUser.Email;
				txtAddress1.Text = tmUser.AddressLine1;
				txtAddress2.Text = tmUser.AddressLine2;
				txtCity.Text = tmUser.City;
				txtPhone.Text = tmUser.Phone;
				txtPhoneExt.Text = tmUser.PhoneExtension;
				txtZip.Text = tmUser.Zipcode;
				ddlState.Value = tmUser.StateAbbreviation;
			}

			var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];
            linkPrivacyPolicy.NavigateUrl = "/Privacy-Policy";
			if (myTMLogo != null && myTMLogo.HasValue)
			{
				MycompanyLogo = myTMLogo.GetMediaUrl();
			}

			switch (SCCurrentDeviceName.ToLower())
			{
				case "monarchgroup":
					MycompanySmall = "myFavs";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
					break;
				case "darlinghomes":
					MycompanySmall = "myDH";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
					break;
				default:
					MycompanySmall = "myTM";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
					break;
			}

		}

		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(Sitecore.Context.Site.StartPath);
			}
		}

		private void Redirect()
		{
			Response.Redirect("/Account/Login");
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;
			string firstName = txtFirstName.Text.Trim();
			string lastName = txtLastName.Text.Trim();
			string password = txtPassword.Text;
			string email = txtEmailAddress.Text.Trim();
			string address1 = txtAddress1.Text.Trim();
			string address2 = txtAddress2.Text.Trim();
			string city = txtCity.Text.Trim();
			string state = ddlState.Value;
			string zip = txtZip.Text.Trim();
			string phone = txtPhone.Text.Trim();
			string phoneExt = txtPhoneExt.Text.Trim();

			var tmUser = CurrentWebUser;
			if (tmUser != null)
			{
				tmUser = tmUser.Get(true);

				var upUser = new TMUser(TMUserDomain.TaylorMorrison, tmUser.UserName)
					{
						FirstName = firstName,
						LastName = lastName,
						Email = email,
						AddressLine1 = address1,
						AddressLine2 = address2,
						City = city,
						StateAbbreviation = state,
						Zipcode = zip,
						Phone = phone,
						PhoneExtension = phoneExt,
						AreaOfInterest = tmUser.AreaOfInterest,
						CommunityFavorites = tmUser.CommunityFavorites,
						PlanFavorites = tmUser.PlanFavorites,
						HomeforSaleFavorites = tmUser.HomeforSaleFavorites
					};
				vlsErrorMessage.Enabled = true;


				try
				{
					if (!string.IsNullOrWhiteSpace(email))
					{
						var existTmUser = new TMUser(WebUserExtensions.GetUsersCurrentDomain(), email);
						if (existTmUser.Exists() && email != tmUser.Email)
						{
							vlsErrorMessage.Enabled = true;
							var alterr = new CustomValidator
								{
									ErrorMessage =
										string.Format("Email address already exists. Please try different one."),
									IsValid = false
								};
							Page.Validators.Add(alterr);
						}
						else
						{
							UpdateUser(tmUser, upUser, password);
						}
					}
					else
					{
						UpdateUser(tmUser, upUser, password);
					}

				}
				catch (Exception ex)
				{
					vlsErrorMessage.Enabled = true;
					var err = new CustomValidator
						{
							ErrorMessage = "An error has occured",
							IsValid = false
						};
					Page.Validators.Add(err);
					Log.Error("error in myprofile", ex, typeof(MyProfile));
				}
			}

		}

		private static bool ChangeUsername(TMUser oldUser, TMUser newUser)
		{
			using (var myConnection = new SqlConnection())
			{
				myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["core"].ConnectionString;

				var myCommand = new SqlCommand
					{
						Connection = myConnection,
						CommandText = "dbo.aspnet_Users_UpdateUsername",
						CommandType = CommandType.StoredProcedure
					};

				myCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = PageUrls.ApplicationName;
				myCommand.Parameters.Add("@OldUserName", SqlDbType.NVarChar).Value = oldUser.FullyQualifiedUserName;
				myCommand.Parameters.Add("@NewUserName", SqlDbType.NVarChar).Value = newUser.FullyQualifiedUserName;
				myCommand.Parameters.Add("@LoweredNewUserName", SqlDbType.NVarChar).Value = newUser.FullyQualifiedUserName;

				var retValParam = new SqlParameter("@ReturnValue", SqlDbType.Int) { Direction = ParameterDirection.ReturnValue };
				myCommand.Parameters.Add(retValParam);

				myConnection.Open();
				myCommand.ExecuteNonQuery();
				myConnection.Close();

				var returnValue = -1;
				if (retValParam.Value != null)
				{
					returnValue = Convert.ToInt32(retValParam.Value);
				}

				return returnValue == 0;
			}
		}


		private void UpdateUser(TMUser tmUser, TMUser upUser, string password)
		{
			var email = !string.IsNullOrWhiteSpace(upUser.Email) ? upUser.Email : tmUser.UserName;

			if (tmUser.UserName.ToLower() != email.ToLower())
			{
				var newformedUser = new TMUser(TMUserDomain.TaylorMorrison, email)
						{
							FirstName = upUser.FirstName,
							LastName = upUser.LastName,
							Email = upUser.Email,
							AddressLine1 = upUser.AddressLine1,
							AddressLine2 = upUser.AddressLine2,
							City = upUser.City,
							StateAbbreviation = upUser.StateAbbreviation,
							Zipcode = upUser.Zipcode,
							Phone = upUser.Phone,
							PhoneExtension = upUser.PhoneExtension,
							AreaOfInterest = upUser.AreaOfInterest,
							CommunityFavorites = upUser.CommunityFavorites,
							PlanFavorites = upUser.PlanFavorites,
  							HomeforSaleFavorites = upUser.HomeforSaleFavorites   
						};

				if (ChangeUsername(tmUser, newformedUser)) 
					upUser = newformedUser;
				
			}

			upUser.Update();

			if (!string.IsNullOrEmpty(password)) upUser.UpdatePassword(password);
			CurrentWebUser = upUser;

			vlsErrorMessage.Enabled = true;
			var err = new CustomValidator
			{
				ErrorMessage = SCFieldNames.UserTexts.Updated,
				IsValid = false
			};
			Page.Validators.Add(err);
		}
	}
}
