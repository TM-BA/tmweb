﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PasswordRequest.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.PasswordRequest" %>
<div class="gr-bk">
	<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
			<img src="<%=MycompanyLogo %>"/>
		</div>
	<p>
		Account
	</p>
</div>
<div class="create-acc">
	<h1>
		Request Your <%=MycompanySmall%> Account Password</h1>
	<p>
		Enter your email address and we'll send you your password. Already have your password?
		<a href="/Account/Login">Login</a></p>	
	 <asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx" EnableClientScript="True" ShowSummary="True" 
	 DisplayMode="SingleParagraph" Enabled="True" HeaderText="We did not find a user with this provided email. Please try again." ShowMessageBox="False"/>
	
	<span>Email Address *</span>
	<asp:TextBox runat="server" ID="txtUsername" CssClass="tm-account"></asp:TextBox>	
	<asp:RequiredFieldValidator runat="server" ID="requsername" ControlToValidate="txtUsername" EnableClientScript="True"></asp:RequiredFieldValidator>	
	<asp:LinkButton runat="server" CssClass="button" Text="Send Password<span></span>" ID="butLogin"  OnClick="OnClick"></asp:LinkButton>
</div>
