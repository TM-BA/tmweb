﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class LoginModal : ControlBase
	{
		public string Mycompany = string.Empty;
		public string MycompanySmall = string.Empty;
		public string MycompanyLogo = string.Empty;
		

		protected void Page_Load(object sender, EventArgs e)
		{
			var uri = HttpContext.Current.Request.Url;
			var qryString = uri.Query.Remove(0, 1);

			var values = qryString.Split('&');

			var id = values[0].Split('=')[1];
			var type = values[1].Split('=')[1];
			var action = values[2].Split('=')[1];
			var sentto = values[3].Split('=')[1];
			hdnId.Value = id;
			hdnType.Value = type;
			hdnAction.Value = action;
			hdnSentto.Value = sentto;

			if (!IsPostBack)
			{
				var sendtoUrlKey = SessionKeys.SendtoUrlKey;
				if (Request.UrlReferrer != null)
					WebSession[sendtoUrlKey] = Request.UrlReferrer.AbsoluteUri;
				else
					WebSession[sendtoUrlKey] = string.Empty;


				var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

				if (myTMLogo != null && myTMLogo.HasValue)
				{
					MycompanyLogo = myTMLogo.GetMediaUrl();
				}

				switch (SCCurrentDeviceName.ToLower())
				{
					case "monarchgroup":
						MycompanySmall = "myFavs";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
						break;
					case "darlinghomes":
						MycompanySmall = "myDH";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
						break;
					default:
						MycompanySmall = "myTM";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
						break;
				}
				

				ClientScriptManager cs = Page.ClientScript;
				if (CurrentWebUser == null)
				{
					cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(false, false);", true);
				}
				else
				{
					if (!String.IsNullOrWhiteSpace(CurrentWebUser.UserName))
					{
						if (!String.IsNullOrWhiteSpace(CurrentWebUser.CommunityFavorites) ||
						    !String.IsNullOrWhiteSpace(CurrentWebUser.PlanFavorites) ||
						    !String.IsNullOrWhiteSpace(CurrentWebUser.HomeforSaleFavorites))
							cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, true);", true);
						else
							cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, false);", true);
						Redirect();
					}
				}
			}
		}


		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(Sitecore.Context.Site.StartPath);
			}
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;
			string userName = txtUsername.Text.Trim();
			string password = txtPassword.Text;

			var tmUser = new TMUser(WebUserExtensions.GetUsersCurrentDomain(), userName) { Password = password };
			ClientScriptManager cs = Page.ClientScript;
			CurrentWebUser = null;
			if (tmUser.Login(true))
			{
				CurrentWebUser = tmUser.Get(true);
				if (!String.IsNullOrWhiteSpace(CurrentWebUser.CommunityFavorites) ||
							!String.IsNullOrWhiteSpace(CurrentWebUser.PlanFavorites) ||
							!String.IsNullOrWhiteSpace(CurrentWebUser.HomeforSaleFavorites))
					cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, true);", true);
				else
					cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true);", true);
				Redirect();
			}
			else
			{
				cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(false, false);", true);
				var err = new CustomValidator
				{	
					IsValid = false
				};
				Page.Validators.Add(err);
			}
		}

		private void Redirect()
		{
			if (Page.Request["sendto"] != null)
			{
				Response.Redirect(Page.Request["sendto"]);
			}
			else
			{
				var sendtoUrlKey = SessionKeys.SendtoUrlKey;
				var sendtourl = WebSession[sendtoUrlKey].CastAs<string>();
				sendtourl = sendtourl.ToLower().Contains("account/login") || sendtourl.ToLower().Contains("account/logout") ||
				            sendtourl.ToLower().Contains("account/register") ||
				            sendtourl.ToLower().Contains("account/forgot-password") ||
				            sendtourl.ToLower().Contains("account/reset-password")
					            ? "/"
					            : sendtourl;
				WebSession[sendtoUrlKey] = string.Empty;  
				Response.Redirect(!string.IsNullOrWhiteSpace(sendtourl) ? sendtourl : "/");
			}
		}
	}
}