﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using Sitecore.Data.Items;

namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class PasswordRequestEmail : ControlBase
	{
        protected string CurrentDevice
        {
            get { return SCContextSite.Device.ToLowerInvariant(); }
        }
		protected void Page_Load(object sender, EventArgs e)
		{

            litFindHomes.Text = @"Find a <a href=""/"">new home</a> or <a href=""/""> new condo</a> by {0}.".FormatWith(SCCurrentHomeItem["Company Name"]);

            Item TMHome = SCContextDB.GetItem(SCIDs.CompanyIds.TaylorMorrisonHomeItem);
            Item MGHome = SCContextDB.GetItem(SCIDs.CompanyIds.TaylorMorrisonHomeItem);
            Item DHHome = SCContextDB.GetItem(SCIDs.CompanyIds.TaylorMorrisonHomeItem);

            if( !CurrentDevice.Contains("taylor") && (TMHome != null))
                logo1.NavigateUrl = TMHome["Company Website URL"];
            else
                logo1.NavigateUrl =  "#";

            if (!CurrentDevice.Contains("taylor") && (TMHome != null))
                logo2.NavigateUrl = TMHome["Company Website URL"];
            else
                logo2.NavigateUrl = "#";

            if (!CurrentDevice.Contains("monarch") && (MGHome != null))
                logo3.NavigateUrl = MGHome["Company Website URL"];
            else
                logo3.NavigateUrl = "#";

            if (!CurrentDevice.Contains("darling") && (DHHome != null))
                logo4.NavigateUrl = DHHome["Company Website URL"];
            else
                logo4.NavigateUrl = "#";
		}

	}
}