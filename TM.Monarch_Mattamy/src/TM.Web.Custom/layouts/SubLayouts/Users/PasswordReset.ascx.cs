﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class PasswordReset : ControlBase
	{

		public string MycompanySmall = string.Empty;
		public string MycompanyLogo = string.Empty;
		
		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(Sitecore.Context.Site.StartPath);
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return; 
			if (GetUserByUrl() != null)
			{
				phPasswordReset.Visible = true;
			}
			else
			{
				vlsErrorMessage.HeaderText = string.Empty;
				phPasswordReset.Visible = false;
				var err = new CustomValidator
					{
						IsValid = false,
						ErrorMessage = SCFieldNames.UserTexts.InvalidPasswordRecovery
					};
				Page.Validators.Add(err);
			}

			var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

			if (myTMLogo != null && myTMLogo.HasValue)
			{
				MycompanyLogo = myTMLogo.GetMediaUrl();
			}

			switch (SCCurrentDeviceName.ToLower())
			{
				case "monarchgroup":
					MycompanySmall = "myFavs";
					break;
				case "darlinghomes":
					MycompanySmall = "myDH";
					break;
				default:
					MycompanySmall = "myTM";
					break;
			}
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;
			string password = txtPassword.Text.Trim();

			var tmUser = GetUserByUrl();

			if (tmUser != null)
			{
				tmUser.UpdatePassword(password);

				tmUser.PasswordRecoveryId = string.Empty;
				tmUser.Update();

				vlsErrorMessage.HeaderText = string.Empty; 
				var msg = new CustomValidator
				{
					ErrorMessage = SCFieldNames.UserTexts.PasswordResetConfirmation,
					IsValid = false
				};
				Page.Validators.Add(msg);
			}
			else
			{
				vlsErrorMessage.HeaderText = string.Empty;
				var err = new CustomValidator
				{
					ErrorMessage = SCFieldNames.UserTexts.UserNotExist,
					IsValid = false
				};
				Page.Validators.Add(err);
			}  
		}

		private TMUser GetUserByUrl()
		{
			Uri uri = HttpContext.Current.Request.Url;
			
			if (!string.IsNullOrEmpty(uri.Query))
			{				  
				string qury = uri.Query.Remove(0, 3);

				var decrpt = CryptographyHelpers.DecryptString(qury, Sitecore.Context.GetDeviceName().GetDomainFromDeviceName());

				string[] quryValues = decrpt.Split('|');

				if (quryValues.Length == 2)
				{
					var pwdId = quryValues[0];
					var userName = quryValues[1];

					var tmuser =  new TMUser(WebUserExtensions.GetUsersCurrentDomain(), userName).Get(true);

					return tmuser.PasswordRecoveryId == pwdId ? tmuser : null;	
				}
			}

			return null;
		}
	}
}