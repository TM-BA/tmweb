﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class PasswordRequest : ControlBase
	{
		
		public string MycompanySmall = string.Empty;
		public string MycompanyLogo = string.Empty;
		protected void Page_Load(object sender, EventArgs e)
		{
			var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

			if (myTMLogo != null && myTMLogo.HasValue)
			{
				MycompanyLogo = myTMLogo.GetMediaUrl();
			}

			switch (SCCurrentDeviceName.ToLower())
			{
				case "monarchgroup":
					MycompanySmall = "myFavs";
					break;
				case "darlinghomes":
					MycompanySmall = "myDH";
					break;
				default:
					MycompanySmall = "myTM";
					break;
			}
		}
		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(Sitecore.Context.Site.StartPath);
			}
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;

			string userName = txtUsername.Text.Trim();

			var tmUser = new TMUser(WebUserExtensions.GetUsersCurrentDomain(), userName).Get(true);
			
			if (tmUser.Exists())
			{  
				var pwdRecoveryId = Guid.NewGuid().ToString();
				tmUser.PasswordRecoveryId = pwdRecoveryId; 
				tmUser.Update();

				var pwdResetUrl = GetPasswordResetUrl(tmUser);
				var emailString = GetPasswordRequestEmailTemplate();
				var body = PasswordRequestEmailTemplate(tmUser, emailString, pwdResetUrl);

				var tmUserEmail = tmUser.Email ?? tmUser.UserName; 

				vlsErrorMessage.HeaderText = string.Empty;
				//TODO: Change sender the email and undo the comments.
				EmailHelper.SendEmail(FromEmail, tmUserEmail, SCFieldNames.UserTexts.PasswordReset, body);
				var err = new CustomValidator
				{
					ErrorMessage =SCFieldNames.UserTexts.PasswordRecoveryEmail, 
 					//ErrorMessage = pwdResetUrl,
					IsValid = false
				};
				Page.Validators.Add(err);
			}
			else
			{
				var err = new CustomValidator
				{	
					IsValid = false
				};
				Page.Validators.Add(err);
			}  
		}

		private string GetPasswordResetUrl(TMUser tmUser)
		{
			if (tmUser != null)
			{
				Uri uri = HttpContext.Current.Request.Url;

				string enstring = string.Format("{0}|{1}", tmUser.PasswordRecoveryId, tmUser.UserName);
				
				var encrpt = CryptographyHelpers.EncryptString(enstring, Sitecore.Context.GetDeviceName().GetDomainFromDeviceName());

				return string.Format("{0}/Account/Reset-Password?r={1}", GetCurrentAuthority(), encrpt);
			}
			return string.Empty;
		}

		private string GetPasswordRequestEmailTemplate()
		{
			// Declare stringbuilder to render control to
			StringBuilder sb = new StringBuilder();

			// Load the control
			UserControl ctrl = (UserControl)LoadControl("PasswordRequestEmail.ascx");

			// Do stuff with ctrl here

			// Render the control into the stringbuilder
			StringWriter sw = new StringWriter(sb);
			Html32TextWriter htw = new Html32TextWriter(sw);
			ctrl.RenderControl(htw);

			// Get full body text
			return sb.ToString();
		}

		private string PasswordRequestEmailTemplate(TMUser info, string emailString, string pwdResetUrl)
		{
			Uri uri = HttpContext.Current.Request.Url;
			string mycompany = string.Empty;
			string mycompanyname = string.Empty;
			string mycompanylogo = string.Empty;
			var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

			if (myTMLogo != null && myTMLogo.HasValue)
			{
				mycompanylogo = myTMLogo.GetMediaUrl();
			}

			switch (SCCurrentDeviceName.ToLower())
			{
				case "monarchgroup":
					mycompany = "myFavs";
					mycompanyname = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
					break;
				case "darlinghomes":
					mycompany = "myDH";
					mycompanyname = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
					break;
				default:
					mycompany = "myTM";
					mycompanyname = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
					break;
			}

			emailString = WebUtility.HtmlDecode(emailString);

			emailString = emailString.Replace("{0}", GetCurrentAuthority()).Replace("{1}", mycompanylogo).Replace("{2}", mycompany).Replace("{3}", info.FirstName).Replace("{4}", pwdResetUrl).Replace("{5}", mycompanyname);

			return emailString;
		}

		private string GetCurrentAuthority()
		{
			Uri uri = HttpContext.Current.Request.Url;
			return string.Format("{0}://{1}", uri.Scheme, uri.Authority);
		}
	}
}