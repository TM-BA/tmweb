﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeEmail.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.WelcomeEmail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<style type="text/css">
		body, div, p, table, ul, form, blockquote, h1, h2, h3
		{
			font-family: Arial, Helvetica, sans-serif;
			font-size: 11px;
			color: #1A3989;
			margin: 0;
			padding: 0;
		}
		td
		{
			font-size: 12px;
			height: auto;
			line-height: 20px;
			margin: 0;
			padding: 15px 0 10px 25px;
			float: left;
			color: #1A3989;
		}
		a
		{
			color: #1A3989;
			font-weight: bold;
			text-decoration: underline;
			border: none;
		}
		img
		{
			border: none;
		}
		a:hover
		{
			text-decoration: none;
		}
		
		.mylogobg
		{
			border-bottom:1px solid #dcdcdc; 
			float: left;
			height: 80px;
			line-height: 18px;
			margin: 0 0 10px;
			padding: 0 0 0 25px;
			width: 100%;
			background: #efefef; /* Old browsers */
			/* IE9 SVG, needs conditional override of 'filter' to 'none' */
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkY2RjZGMiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top,  #ffffff 0%, #dcdcdc 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#dcdcdc)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #ffffff 0%,#dcdcdc 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #ffffff 0%,#dcdcdc 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #ffffff 0%,#dcdcdc 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #ffffff 0%,#dcdcdc 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#dcdcdc',GradientType=0 ); /* IE6-8 */

		}
	</style>
</head>
<body>
	<table border="0" cellpadding="0" cellspacing="0" style="padding: 0; margin: 0 0 50px 0;
		width: 620px; float: left; background-color: #fff;">
		<tbody>
			<tr>
				<td class="mylogobg">
					<span style="float: left; margin: 35px 0 0">
						<img src="{0}{1}" alt="{2}" /></span><span style="float: left; margin: 35px 0 0 5px;
							font-weight: bold;">Account</span>
				</td>
			</tr>
			<tr>
				<td>
					Dear {3},<br />
					Welcome to {2}! You can now save home and communities in your profile for future
					visits.<br />
					Please use this information to sign in to your <a href="{0}/Account/Login">{2}
						account:</a>
				</td>
			</tr>
			<tr>
				<td style="padding: 15px 0 4px 40px;">
					Login: {4}
				</td>
			</tr>
			<tr>
				<td>
					Sincerely,
					<br />
					{5}
				</td>
			</tr>
			<tr>
				<td style="padding: 100px 0 10px; width: 100%;">
					<table style="width: 100%;">
						<tr>
							<td colspan="4" style="color: #838383; text-align: center; width: 100%;">
								Find a <a href="http://www.taylormorrison.com/new-homes" style="color: #838383;">new
									home</a> or <a href="http://www.monarchgroup.net/new-condos" style="color: #838383;">
										new condo</a> by {5}.
							</td>
						</tr>
						<tr>
							<td style="padding: 15px 0; width: 25%; text-align: center;">
								<a href="http://www.taylormorrison.com/">
									<img src="{0}/images/tm/logoFooter1.png" alt="taylormorrison" /></a>
							</td>
							<td style="padding: 15px 0; width: 25%; text-align: center;">
								<a href="http://www.monarchgroup.net/">
									<img src="{0}/images/tm/logoFooter2.png" alt="monarchgroup" /></a>
							</td>
							<td style="padding: 15px 0; width: 25%; text-align: center;">
								<a href="http://www.taylormorrison.com/">
									<img src="{0}/images/tm/logoFooter3.png" alt="monarchgroup" /></a>
							</td>
							<td style="padding: 15px 0; width: 25%; text-align: center;">
								<a href="http://www.taylormorrison.com/">
									<img src="{0}/images/dh/darling-logo.jpg" alt="darlinghomes" /></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
