﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeadForm.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.LeadForm" %>
<%@ Register src="GeneralInformation.ascx" tagname="GeneralInformation" tagprefix="uc1" %>
<%@ Register src="LookingForHome.ascx" tagname="LookingForHome" tagprefix="uc2" %>
<%@ Register src="WarrantyRequest.ascx" tagname="WarrantyRequest" tagprefix="uc3" %>

<div>
    <span>How may we help you ?</span>
   
    <asp:DropDownList ID="Support" runat="server" DataTextField="Name" 
        ontextchanged="Support_TextChanged" AutoPostBack="True" CssClass="c-u-inter"></asp:DropDownList>
        
</div>


<div class="forms-wrapper">

<asp:Panel ID="pGeneralInformation" runat="server" Visible="False">
    <uc1:GeneralInformation ID="GeneralInformation" runat="server"  />
</asp:Panel>

<asp:Panel ID="pLookingForHome" runat="server" Visible="False">
    <uc2:LookingForHome ID="LookingForHome" runat="server" />
</asp:Panel>

<asp:Panel ID="pWarrantyRequest" runat="server" Visible="False">
    <uc3:WarrantyRequest ID="WarrantyRequest" runat="server" />

</asp:Panel>

<asp:Panel ID="pFinancingOptions" runat="server" Visible="False">
   
   
<div class="req-info">
    <h1>Required Information</h1>


    <div class="ri-l">
        
        <p>Area of interest </p>

        <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="True" 
        ontextchanged="ddlArea_TextChanged" CssClass="rip-1">
        </asp:DropDownList>

        <p>Community of interest </p>

        <asp:DropDownList ID="ddlComunity" runat="server" CssClass="rip-1" 
            onselectedindexchanged="ddlComunity_SelectedIndexChanged">
        </asp:DropDownList>

        <asp:Button ID="btnContactInformation" runat="server" 
            Text="View Contact Information" CssClass="send-rbtn" 
            onclick="btnContactInformation_Click" />

         <p><asp:Label ID="hmc" runat="server" Text=""> </asp:Label></p>

        <asp:Repeater ID="rptContactInformation" runat="server" 
        onitemdatabound="rptContactInformation_ItemDataBound">
        <ItemTemplate>
        
            <div class="morg-1">
                 <div class="morg-ins">
                        <p> You may reach  </p> <asp:Label ID="name" runat="server" Text=""> </asp:Label> <p> by:</p>
                </div>
                <div class="morg-ins">
                    <p> Phone: </p><asp:Label ID="phone" runat="server" Text="" CssClass="pho-red"> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Mobile: </p><asp:Label ID="mobile" runat="server" Text=""> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Fax: </p><asp:Label ID="fax" runat="server" Text=""> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Email: </p> <asp:HyperLink ID="email" runat="server"></asp:HyperLink>
                </div> 
            </div>                            
        
        </ItemTemplate>
        </asp:Repeater>

       
        <div class="loan-app">
            <asp:HyperLink ID="hlkStarLoanApp" runat="server" target="_blank" Visible="false">Start your loan application</asp:HyperLink>
        </div>
    </div>
</div>


 

</asp:Panel>

</div>
