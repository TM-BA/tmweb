﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Domain;
using TM.Web.Custom.SCHelpers;
using System.Text;
using System.IO;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class UniversalRFIThankYou : ControlBase
    {
        private Sitecore.Data.Items.Item realContextItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            realContextItem = Sitecore.Context.Item;
            if (CurrentPage.WebSession["rfiID"] != null)
            {
                var proposedContextItem = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(CurrentPage.WebSession["rfiID"].ToString()));
                while (
                    (proposedContextItem.TemplateID == SCIDs.TemplateIds.UniversalThankyou)
                    || (proposedContextItem.TemplateID == SCIDs.TemplateIds.UniversalRFIPage)
                    || (proposedContextItem.TemplateID == SCIDs.TemplateIds.ContactUsPage))
                {
                    proposedContextItem = proposedContextItem.Parent;
                }
                Sitecore.Context.Item = proposedContextItem;
            }
            
        }

        public string InterestItemGALabel
        {
            get
            {
                //If you need a different GA Label Finish out this switchStatement
                switch (Sitecore.Context.Item.TemplateID.ToString())
                {
                    case SCIDs.TemplateIds.SwitchCommunityPage:
                        return "CommunityLink";
                }
                return "CommunityLink";

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] == null)
            {
                CurrentPage.Title = "Thank You - " + CurrentPage.PageTitle;
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Thank You", Sitecore.Context.Item));
            }

            if (CurrentPage.CurrentWebUser != null)
            {
                pnlCreateAccount.Visible = false;
            }

            litPrivacyPolicy.Text = @"<p>{0}</p>".FormatWith(realContextItem["New Account Privacy"]);
            if (CurrentPage.WebSession["rfiID"] != null)
            {
                litThankYouText.Text = @"<p>Thank you for your interest. We will keep you posted on the latest news, offers, events, home buying tips, and more.</p>";
            }
            else
            {
                litThankYouText.Text = @"<p>Thank you for your interest in <a href=""{0}"" class=""InterestItem"">{1}</a>. We will keep you posted on the latest news, offers, events, home buying tips, and more.</p>".FormatWith(
                SCContextItem.GetItemUrl(),
                SCContextItem.GetItemName()
                );
            }

            

            if(!string.IsNullOrWhiteSpace(CurrentPage.CurrentHomeItem["Financing Call to Action"]) ){
                litPreQual.Text = @"<p>Ready to get pre-qualified? <a href=""{0}"">Start the pre-qualification</a> process.</p>".FormatWith(
                    CurrentPage.CurrentHomeItem["Financing Call to Action"]
                    );
            }


            if(CurrentPage.FacebookUrl.IsNotEmpty() ){
                litSocialMedia.Text = @"<p>Visit our <a href=""{0}""><img src=""/Images/tm/icon_FB.jpg"" class=""tySocialIcn fb""/> Facebook</a>".FormatWith(
                    CurrentPage.FacebookUrl
                    );
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @" and <a href=""{0}""><img src=""/images/tm/iconTwitter.png""  class=""tySocialIcn tw"">Twitter</a> pages.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
                else
                    litSocialMedia.Text += " page.</p>";
            } else{
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @"<p>Visit our <a href=""{0}""><img src=""/images/tm/iconTwitter.png"" class=""tySocialIcn tw"">Twitter</a> page.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
            }


            if (CurrentPage.CurrentIHC != null)
                litIhc.Text = @"<p>Questions? Call Internet New Home Consultant<br/>{0} {1} {3} at <span class=""ihc-phone"">{2}.</span></p>".FormatWith( 
                    CurrentPage.CurrentIHC["First Name"], 
                        CurrentPage.CurrentIHC["Last Name"], 
                        CurrentPage.CurrentIHC["Phone"],
                        CurrentPage.CurrentIHC["Regulating Authority Designation"].IsNotEmpty()?"("+CurrentPage.CurrentIHC["Regulating Authority Designation"]+")":string.Empty
                         );

            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem["Blog Url"]))
                litBlogLink.Text = "<p>Have you seen our Blog. Visit <a href='{0}'>Our Blog</a> for the latest in home trends, technology, and more.</p>".FormatWith(CurrentPage.BlogURL);


            //Universal Thank You Next Steps
            litNextSteps.Text = realContextItem["Next Steps"];

            lnkBtnCreateAccount.Click += new EventHandler(btnCreateAccount_Click);
        }

        void btnCreateAccount_Click(object sender, EventArgs e)
        {
            pnlCreateAccount.Visible = false;
            var device = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

			var newUser = new TMUser(TMUserDomain.TaylorMorrison, WebSession["WFFMtxtEmail"].CastAs<string>())
				{
                    FirstName = WebSession["WFFMtxtFirstName"].CastAs<string>(),
                    LastName = WebSession["WFFMtxtLastName"].CastAs<string>(),
                    Email = WebSession["WFFMtxtEmail"].CastAs<string>(),
					Password = txtPassword.Text,
					AreaOfInterest = CurrentPage.CurrentDivision.DisplayName,
					Device = device
				};

			CurrentWebUser = null;


            if (newUser.Create(GetCreateAccountTemplate()))
            {
                litAccountCreated.Text = "<p>Account created successfully</p>";
            }
            else
            {
                litAccountCreated.Text = "<p>User account already exists. Please <a href=\"/Account/Login\">Login</a> here.</p>";
            }
        }

        private string GetCreateAccountTemplate()
        {
            // Declare stringbuilder to render control to
            StringBuilder sb = new StringBuilder();

            // Load the control
            UserControl ctrl = (UserControl)LoadControl("/tmwebcustom/SubLayouts/Users/WelcomeEmail.ascx");

            // Do stuff with ctrl here

            // Render the control into the stringbuilder
            StringWriter sw = new StringWriter(sb);
            Html32TextWriter htw = new Html32TextWriter(sw);
            ctrl.RenderControl(htw);

            // Get full body text
            return sb.ToString();
        }
    }
}