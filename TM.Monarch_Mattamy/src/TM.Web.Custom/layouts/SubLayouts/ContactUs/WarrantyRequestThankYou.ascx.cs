﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using Sitecore.Data.Items;
using TM.Web.Custom.Queries;
using SCID = Sitecore.Data.ID;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{

    public partial class WarrantyRequestThankYou : ControlBase 
    {

       

        protected void Page_Load(object sender, EventArgs e)
        {

            if (SCCurrentHomeItem.Fields["Facebook URL"].Value.IsNotEmpty())
            {
                litSocialMedia.Text = @"<p>Visit our <a href=""{0}""><img src=""/Images/tm/icon_FB.jpg"" class=""tySocialIcn fb""/> Facebook</a>".FormatWith(
                    SCCurrentHomeItem.Fields["Facebook URL"].GetLinkFieldPath()
                    );
                if (SCCurrentHomeItem.Fields["Twitter URL"].Value.IsNotEmpty())
                    litSocialMedia.Text += @" and <a href=""{0}""><img src=""/images/tm/iconTwitter.png""  class=""tySocialIcn tw"">Twitter</a> pages.</p>".FormatWith(
                        SCCurrentHomeItem.Fields["Twitter URL"].GetLinkFieldPath()
                        );
                else
                    litSocialMedia.Text += " page.</p>";
            }
            else
            {
                if (SCCurrentHomeItem.Fields["Twitter URL"].Value.IsNotEmpty())
                    litSocialMedia.Text += @"<p>Visit our <a href=""{0}""><img src=""/images/tm/iconTwitter.png"" class=""tySocialIcn tw"">Twitter</a> page.</p>".FormatWith(
                        SCCurrentHomeItem.Fields["Twitter URL"].GetLinkFieldPath()
                        );
            }

                if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem["Blog Url"]))
                    litBlogLink.Text = "<p>Have you seen our Blog. Visit <a href='{0}'>Our Blog</a> for the latest in home trends, technology, and more.</p>".FormatWith(CurrentPage.BlogURL);

                
        }
    }
}