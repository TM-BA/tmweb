﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Links;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class CreateAccount : ControlBase
    {
        string firstName;
        string lastName;
        string email;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (CurrentWebUser == null)
            {
                pnlAcccount.Visible = true;
                if (Session["FormParams"] != null)
                {
                    List<string> Params = (List<string>)Session["FormParams"];
                    firstName = Params[0];
                    lastName = Params[1];
                    email = Params[2];
                }

            }
        }

        protected void btnCreateAccount_Click(object sender, EventArgs e)
        {
           

            if (!Page.IsValid) return;

            if (!txtPassword.Text.Equals(txtConfirmPassword.Text)) return;

            string password = txtPassword.Text;
 

                var device = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

                var newUser = new TMUser(TMUserDomain.TaylorMorrison, email)
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    Password = password,
                    Device = device
                };

                CurrentWebUser = null;

                var emailString = GetCreateAccountTemplate();

                if (newUser.Create(emailString))
                {
                    CurrentWebUser = newUser;
                    ClientScriptManager cs = Page.ClientScript;
                    cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, false);", true);

                    ClientScriptManager csmgr = Page.ClientScript;
                    csmgr.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, false);", true);
                    pnlAcccount.Visible = false;
                }
                else
                {
                    vlsErrorMessage.Enabled = true;
                    var err = new CustomValidator
                    {
                        ErrorMessage =
                            string.Format("User account already exists. Please <a href=\"/Authentication/Login\">Login</a> here"),
                        IsValid = false
                    };
                    Page.Validators.Add(err);
                }
        }



        private string GetCreateAccountTemplate()
        {
            // Declare stringbuilder to render control to
            StringBuilder sb = new StringBuilder();

            // Load the control
            UserControl ctrl = (UserControl)LoadControl("../Users/WelcomeEmail.ascx");

            // Do stuff with ctrl here

            // Render the control into the stringbuilder
            StringWriter sw = new StringWriter(sb);
            Html32TextWriter htw = new Html32TextWriter(sw);
            ctrl.RenderControl(htw);

            // Get full body text
            return sb.ToString();
        }

        private string WelcomeEmail(TMUser info, string emailString)
        {
            string mycompany = string.Empty;
            string mycompanyname = string.Empty;
            string mycompanylogo = string.Empty;

            switch (info.Device.ToLower())
            {
                case "monarchgroup":
                    mycompany = "myMG";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
                    mycompanylogo = "/images/tm/mymg-smalllogo.png";
                    break;
                case "darlinghomes":
                    mycompany = "myDH";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
                    mycompanylogo = "/images/tm/mydh-smalllogo.png";
                    break;
                default:
                    mycompany = "myTM";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
                    mycompanylogo = "/images/tm/mytm-smalllogo.png";
                    break;
            }

            emailString = WebUtility.HtmlDecode(emailString);

            Uri uri = HttpContext.Current.Request.Url;
            emailString = emailString.Replace("{0}", GetCurrentAuthority()).Replace("{1}", mycompanylogo).Replace("{2}", mycompany).Replace("{3}", info.FirstName).Replace("{4}", info.Email).Replace("{5}", mycompanyname);

            return emailString;
        }

        private string GetCurrentAuthority()
        {
            Uri uri = HttpContext.Current.Request.Url;
            return string.Format("{0}://{1}", uri.Scheme, uri.Authority);
        }
    }
}
