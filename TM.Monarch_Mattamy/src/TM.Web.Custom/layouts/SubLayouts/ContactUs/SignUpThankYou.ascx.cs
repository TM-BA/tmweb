﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Domain;
using TM.Web.Custom.SCHelpers;
using System.Text;
using System.IO;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class SignUpThankYou : ControlBase
    {
        private Sitecore.Data.Items.Item realContextItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            realContextItem = Sitecore.Context.Item;
           
        }

        public string InterestItemGALabel
        {
            get
            {
                //If you need a different GA Label Finish out this switchStatement
                switch (Sitecore.Context.Item.TemplateID.ToString())
                {
                    case SCIDs.TemplateIds.SwitchCommunityPage:
                        return "CommunityLink";
                }
                return "CommunityLink";

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] == null)
            {
                CurrentPage.Title = "Thank You - " + CurrentPage.PageTitle;
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Thank You", Sitecore.Context.Item));
                
            }

            if (CurrentPage.CurrentWebUser != null)
            {
                pnlCreateAccount.Visible = false;
            }

            this.litTitle.Text = realContextItem["Title"];
            this.litContent.Text = realContextItem["Content"];
            this.litNextStep.Text = realContextItem["Next Step Content"];
            this.litWaysConnectContent.Text = realContextItem["Way Connect Content"];

            var showNextStep = realContextItem["Show Next Step"];
            var showSignup = realContextItem["Show Signup Password"];
            var showWayConnect = realContextItem["Show Way Connect"];
            var showWayConnectContent = realContextItem["Show Way Connect Content"];

            if (showNextStep == "1")
            {
                nextSteps.Visible = true;
            }
            else
            {
                nextSteps.Visible = false;
            }

            if (showSignup == "1")
            {
                createAccount.Visible = true;
            }
            else
            {
                createAccount.Visible = false;
            }

            if (showWayConnect == "1")
            {
                waystoconnect.Visible = true;
            }
            else
            {
                waystoconnect.Visible = false;
            }

            if (showWayConnectContent == "1")
            {
                waystoconnectContent.Visible = true;
            }
            else
            {
                waystoconnectContent.Visible = false;
            }

            if(CurrentPage.FacebookUrl.IsNotEmpty() ){
                litSocialMedia.Text = @"<p>Visit our <a href=""{0}""><img src=""/Images/tm/icon_FB.jpg"" class=""tySocialIcn fb""/> Facebook</a>".FormatWith(
                    CurrentPage.FacebookUrl
                    );
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @" and <a href=""{0}""><img src=""/images/tm/iconTwitter.png""  class=""tySocialIcn tw"">Twitter</a> pages.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
                else
                    litSocialMedia.Text += " page.</p>";
            } else{
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @"<p>Visit our <a href=""{0}""><img src=""/images/tm/iconTwitter.png"" class=""tySocialIcn tw"">Twitter</a> page.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
            }


            if (CurrentPage.CurrentIHC != null)
                litIhc.Text = @"<p>Questions? Call Internet New Home Consultant<br/>{0} {1} {3} at <span class=""ihc-phone"">{2}.</span></p>".FormatWith( 
                    CurrentPage.CurrentIHC["First Name"], 
                        CurrentPage.CurrentIHC["Last Name"], 
                        CurrentPage.CurrentIHC["Phone"],
                        CurrentPage.CurrentIHC["Regulating Authority Designation"].IsNotEmpty()?"("+CurrentPage.CurrentIHC["Regulating Authority Designation"]+")":string.Empty
                         );

            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem["Blog Url"]))
                litBlogLink.Text = "<p>Have you seen our Blog. Visit <a href='{0}'>Our Blog</a> for the latest in home trends, technology, and more.</p>".FormatWith(CurrentPage.BlogURL);

            lnkBtnCreateAccount.Click += new EventHandler(btnCreateAccount_Click);
        }

        void btnCreateAccount_Click(object sender, EventArgs e)
        {
            pnlCreateAccount.Visible = false;
            var device = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

			var newUser = new TMUser(TMUserDomain.TaylorMorrison, WebSession["WFFMtxtEmail"].CastAs<string>())
				{
                    FirstName = WebSession["WFFMtxtFirstName"].CastAs<string>(),
                    LastName = WebSession["WFFMtxtLastName"].CastAs<string>(),
                    Email = WebSession["WFFMtxtEmail"].CastAs<string>(),
					Password = txtPassword.Text,
					AreaOfInterest = CurrentPage.CurrentDivision.DisplayName,
					Device = device
				};

			CurrentWebUser = null;


            if (newUser.Create(GetCreateAccountTemplate()))
            {
                litAccountCreated.Text = "<p>Account created successfully</p>";
            }
            else
            {
                litAccountCreated.Text = "<p>User account already exists. Please <a href=\"/Account/Login\">Login</a> here.</p>";
            }
        }

        private string GetCreateAccountTemplate()
        {
            // Declare stringbuilder to render control to
            StringBuilder sb = new StringBuilder();

            // Load the control
            UserControl ctrl = (UserControl)LoadControl("/tmwebcustom/SubLayouts/Users/WelcomeEmail.ascx");

            // Do stuff with ctrl here

            // Render the control into the stringbuilder
            StringWriter sw = new StringWriter(sb);
            Html32TextWriter htw = new Html32TextWriter(sw);
            ctrl.RenderControl(htw);

            // Get full body text
            return sb.ToString();
        }
    }
}