﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WarrantyRequest.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.WarrantyRequest" %>

<div class="warranty-form">
<sc:fieldrenderer runat="server" id="ContentArea" fieldname="Warranty Request Introduction"></sc:fieldrenderer>
</div>
<sc:placeholder runat="server" id="WarrantyRequestWFFM" key="WarrantyRequest">
</sc:placeholder>