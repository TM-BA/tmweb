﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Queries;
using SCID = Sitecore.Data.ID;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;
using TM.Domain;
using Sitecore.Data.Items;
using TM.Utils.Web;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class LookingForHome : ContactUsBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateData();
        }

        private void PopulateData()
        {
            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.ContactUsForms.ThankYou));

            TMSession WebSession = new TMSession();
            WebSession["rfiID"] = matchingTemplates[0].ID;
            WebSession.Save();
        }

        
    }
}