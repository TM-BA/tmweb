﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class UniversalRFI : ControlBase
    {
        private string _longTitle;
        public string LongTitle
        {
            get
            {
                return _longTitle;
            }
            set
            {
                _longTitle = value;
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            Sitecore.Context.Item = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(CurrentPage.WebSession["rfiID"].ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentPage.Title = "Request Information - " + CurrentPage.PageTitle;
            var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
            breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Request Information", CurrentPage.CurrentCommunity));

            //This is a community
            if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.CommunityPage)
            {
                LongTitle = string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Community Name"])?CurrentPage.CurrentCommunity.DisplayName : CurrentPage.CurrentCommunity["Community Name"];
                litIntro.Text = "Get up to date pricing, availability, special incentives, answers to your questions, and more by filling out this form.";
                string[] items = CurrentPage.CurrentCommunity["Slideshow Image"].Split('|');
                
                //if(items.Length > 0)
                //imgMain.ImageUrl = SCContextDB.GetItem(new Sitecore.Data.ID( items[0] ) ).GetItemUrl()+"?bc=ffffff&w=160&h=92";
            }else  if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.PlanPage)
            {
                string planName = string.IsNullOrWhiteSpace(SCContextItem["Plan Name"]) ? SCContextItem.DisplayName : SCContextItem["Plan Name"];
                string commName = string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Community Name"]) ? CurrentPage.CurrentCommunity.DisplayName : CurrentPage.CurrentCommunity["Community Name"];
                LongTitle = "{0} at {1}".FormatWith(planName, commName);
                litIntro.Text = "Get up to date pricing, availability, special incentives, answers to your questions, and more by filling out this form.";
                string[] items = SCContextItem["Elevation Images"].Split('|');
                
                // if (items.Length > 0 && Sitecore.Data.ID.IsID(items[0]))
                 //   imgMain.ImageUrl = SCContextDB.GetItem(new Sitecore.Data.ID(items[0])).GetItemUrl() + "?bc=ffffff&&w=160&h=92";
            }
            else if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
            {
                string planName = string.IsNullOrWhiteSpace(SCContextItem.Parent["Plan Name"]) ? SCContextItem.Parent.DisplayName : SCContextItem.Parent["Plan Name"];
                string address = string.IsNullOrWhiteSpace(SCContextItem["Street Address 1"]) ? SCContextItem["Lot Number"] : SCContextItem["Street Address 1"];
                LongTitle = "{0} at {1}".FormatWith(planName, address);
                litIntro.Text = "Get up to date pricing, availability, special incentives, answers to your questions, and more by filling out this form.";
                string[] items = SCContextItem["Elevation Images"].Split('|');
               // if (items.Length > 0 && Sitecore.Data.ID.IsID(items[0]))
                   // imgMain.ImageUrl = SCContextDB.GetItem(new Sitecore.Data.ID(items[0])).GetItemUrl() + "?bc=ffffff&&w=160&h=92";
            }
            else
            {
                string companyName = string.IsNullOrWhiteSpace(CurrentPage.CurrentHomeItem["Company Name"]) ? "Us" : CurrentPage.CurrentHomeItem["Company Name"];
                LongTitle = "{0}".FormatWith(CurrentPage.CurrentHomeItem["Company Name"]);
                litIntro.Text = "Get up to date pricing, availability, special incentives, answers to your questions, and more by filling out this form.";
                //string[] items = SCContextItem["Elevation Images"].Split('|');
                //if (items.Length > 0)
                //    imgMain.ImageUrl = SCContextDB.GetItem(new Sitecore.Data.ID(items[0])).GetItemUrl() + "?bc=ffffff&w=350&h=183";
            }
            if (CurrentPage.CurrentIHC != null)
            {
                litIhcText.Text = @"You may also reach your internet home consultant, {0} {1}, at <span class=""ihc-phone"">{2}</span><br />{3}".FormatWith(
                    CurrentPage.CurrentIHC["First Name"],
                    CurrentPage.CurrentIHC["Last Name"],
                    CurrentPage.CurrentIHC["Phone"],
                     CurrentPage.CurrentIHC["Regulating Authority Designation"].Replace("\n","<br />")
                    );
            }
            lnkCancel.NavigateUrl = SCContextItem.GetItemUrl();

        }
    }
}
