﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Domain.Enums;
using TM.Web.Custom.Queries;
using SCID = Sitecore.Data.ID;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;
using TM.Domain;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class LeadForm : ControlBase
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopulateSupport();
                LoadSublayout();
                PopulateDivisions();
                SetLoanApplication();
            }

            Clear();
        }

        private void PopulateSupport()
        {

            Item contextItem = Sitecore.Context.Database.GetItem(SCIDs.SharedFieldValues.SupportOptions);

            Sitecore.Collections.ChildList supportOptions = contextItem.GetChildren();

            foreach (Item supportOption in supportOptions)
            {
                var supportOptionId = supportOption.ID.ToString();
                var supportOptionNameFld = supportOption.Fields["Name"].Value;
                if (supportOptionId != null && supportOptionNameFld.IsNotEmpty())
                {
                    if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup) 
                    {
                        if (SCIDs.SupportOptions.InterestedFinancingOptions != supportOption.ID)
                        {
                            Support.Items.Add(new ListItem(supportOptionNameFld, supportOptionId));
                        }
                    }else{
                     Support.Items.Add(new ListItem(supportOptionNameFld, supportOptionId));
                    }

                }

            }

        }

        
        protected void Support_TextChanged(object sender, EventArgs e)
        {
            LoadSublayout();
        }


        private void LoadSublayout()
        {
            Item generalInformation = Sitecore.Context.Database.GetItem(SCIDs.SupportOptions.GeneralInformation);
            Item LookingForHome = Sitecore.Context.Database.GetItem(SCIDs.SupportOptions.LookingForHome);
            Item InterestedFinancingOptions = Sitecore.Context.Database.GetItem(SCIDs.SupportOptions.InterestedFinancingOptions);
            Item WarrantyRequest = Sitecore.Context.Database.GetItem(SCIDs.SupportOptions.WarrantyRequest);
            Item Default = Sitecore.Context.Database.GetItem(SCIDs.SupportOptions.Default);
            if (Support.SelectedValue == generalInformation.ID.ToString())
            {
                pGeneralInformation.Visible = true;
                pLookingForHome.Visible = false;
                pWarrantyRequest.Visible = false;
                pFinancingOptions.Visible = false;
            }
            else if (Support.SelectedValue == LookingForHome.ID.ToString())
            {
                pLookingForHome.Visible = true;
                pGeneralInformation.Visible = false;
                pWarrantyRequest.Visible = false;
                pFinancingOptions.Visible = false;
            
            }else if (Support.SelectedValue == InterestedFinancingOptions.ID.ToString())
            {

                //pFinancingOptions.Visible = true;
                //pWarrantyRequest.Visible = false;
                //pLookingForHome.Visible = false;
                //pGeneralInformation.Visible = false;
                if (SCCurrentDeviceType == DeviceType.DarlingHomes)
                {
                    Response.Redirect("/finance-with-darling/find-a-mortgage-consultant");
                }
		else 
                Response.Redirect("/finance-your-home/find-a-mortgage-consultant");
            }

            else if (Support.SelectedValue == WarrantyRequest.ID.ToString())
            {
                pWarrantyRequest.Visible = true;
                pLookingForHome.Visible = false;
                pGeneralInformation.Visible = false;
                pFinancingOptions.Visible = false;

            }
            else if (Support.SelectedValue == Default.ID.ToString())
            {
                pLookingForHome.Visible = false;
                pGeneralInformation.Visible = false;
                pWarrantyRequest.Visible = false;
                pFinancingOptions.Visible = false;
            }

        }

        #region Financing Options


        private void Clear()
        {
            hmc.Text = "";
        }

        private void SetLoanApplication()
        {
            Sitecore.Data.Items.Item contextItem = SCContextDB.GetItem(SCCurrentHomePath);

            if (!string.IsNullOrEmpty(contextItem.Fields["Financing Call to Action"].Value))
            if (contextItem.Fields["Financing Call to Action"].Value != string.Empty)
            {
                hlkStarLoanApp.NavigateUrl = contextItem.Fields["Financing Call to Action"].Value;
            }
        }


        private void PopulateDivisions()
        {


            var query = SCFastQueries.AllActiveDivisionsUnder(SCCurrentHomePath);

            var divisions = SCContextDB.SelectItems(query);

            List<ListItem> items = new List<ListItem>();
            foreach (var division in divisions)
            {
                var divisionIdFld = division.ID.ToString();
                var divisionNameFld = division.Fields[SCIDs.DivisionFieldIDs.Divisionname];
                if (divisionIdFld != null && divisionIdFld.IsNotEmpty())
                {

                     items.Add(new ListItem(divisionNameFld.Value + ", " + division.Parent.GetItemName(true), divisionIdFld));
                }

            }

            items = (items.OrderBy(x => x.Text.Split(',')[1].TrimEnd()).ThenBy(y => y.Text.Split(',')[0])).ToList();
            foreach (var item in items)
            {
                ddlArea.Items.Add(item);
            }

            ddlArea.Items.Insert(0, new ListItem("Please select an area", "0"));
            ddlComunity.Items.Insert(0, new ListItem("Please select an area first", "0"));

            if (!Page.IsPostBack)
            {
                ddlArea.SelectedIndex = 0;

            }



        }

        private void PopulateCommunities(string divisionId)
        {

            Item currentDivision = Sitecore.Context.Database.GetItem(divisionId);

            Item[] communities = currentDivision.Database.SelectItems(SCFastQueries.AllActiveCommunitiesUnder(currentDivision.Paths.FullPath));

            ddlComunity.DataSource = "";
            ddlComunity.DataBind();

            foreach (Item community in communities.OrderBy(x => x.Fields["Community Name"].Value))
            {
                var communityIdFld = community.ID.ToString();
                var communityNameFld = community.Fields[SCIDs.CommunityFields.CommunityName];
                if (communityIdFld != null && communityIdFld.IsNotEmpty())
                {
                    ddlComunity.Items.Add(new ListItem(communityNameFld.Value, communityIdFld));
                }

            }

            ddlComunity.Items.Insert(0, new ListItem("Please select an area first", "0"));

        }

        protected void ddlComunity_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

     
        protected void rptContactInformation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        { 
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (currentItem != null)
                {

                    Label name = (Label)e.Item.FindControl("name");
                    string ihcName = "";
                    if (currentItem.Fields["First Name"].Value != string.Empty && currentItem.Fields["Last Name"].Value != string.Empty)
                    {
                        ihcName = ihcName + " " + currentItem.Fields["First Name"].Value + " " + currentItem.Fields["Last Name"].Value;
                    }

                    if (currentItem.Fields["Regulating Authority Designation"].Value != string.Empty)
                    {
                        ihcName = ihcName + " (" + currentItem.Fields["Regulating Authority Designation"].Value + ") ";
                    }

                    name.Text = ihcName;

                    Label phone = (Label)e.Item.FindControl("phone");

                    if (currentItem.Fields["Phone"].Value != string.Empty)
                    {
                        phone.Text = " " + currentItem.Fields["Phone"].Value;
                    }


                    Label mobile = (Label)e.Item.FindControl("mobile");

                    if (currentItem.Fields["Mobile"].Value != string.Empty)
                    {
                        mobile.Text = " " + currentItem.Fields["Mobile"].Value;
                    }


                    Label fax = (Label)e.Item.FindControl("fax");

                    if (currentItem.Fields["Fax"].Value != string.Empty)
                    {
                        fax.Text = " " + currentItem.Fields["Fax"].Value;
                    }


                    HyperLink email = (HyperLink)e.Item.FindControl("email");

                    if (currentItem.Fields["Email Address"].Value != string.Empty)
                    {
                        email.Text = " " + currentItem.Fields["Email Address"].Value;
                        email.NavigateUrl = "mailto:" + currentItem.Fields["Email Address"].Value;
                    }
                }
                
            }

        }

       

        protected void ddlArea_TextChanged(object sender, EventArgs e)
        {
            string divisionId = ddlArea.SelectedItem.Value;

            if (ddlArea.SelectedIndex > 0)
            {
                PopulateCommunities(divisionId);
            }
        }

        protected void btnContactInformation_Click(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex > 0)
            {
                PopulateContactInfo();
            }
            
        }

        private void PopulateContactInfo()
        {
            string communityId ="";
            Item currentCommunity;
            string ihc ="";
            string  contacts ="";

            if (ddlComunity.Items.Count > 0 && ddlComunity.SelectedIndex > 0)
            {
                communityId = ddlComunity.SelectedItem.Value;

                currentCommunity = Sitecore.Context.Database.GetItem(communityId);

                ihc = currentCommunity.Fields["IHC Card"].Value;
            }

            //Populate from division
            if (string.IsNullOrEmpty(ihc))
            {
                string divisionId = ddlArea.SelectedItem.Value;

                Item currentDivision = Sitecore.Context.Database.GetItem(divisionId);

                ihc = currentDivision.Fields["IHC Card"].Value;
            }

            List<Item> ihcList = new List<Item>();

            foreach (string ihcContact in ihc.Split('|').ToList())
            {
                Item item = Sitecore.Context.Database.GetItem(ihcContact);

                if (item != null)
                {

                    ihcList.Add(item);
                }
            }


            ihcList = ihcList.OrderBy(x => x.Fields["Last Name"].Value).ThenBy(n => n.Fields["First Name"].Value).ToList();


            foreach (Item item in ihcList)
            {
                if (item.Fields["First Name"].Value != string.Empty && item.Fields["Last Name"].Value != string.Empty)
                {
                    contacts += (item.Fields["First Name"].Value + " " + item.Fields["Last Name"].Value + ", ");
                }
            }


            if (ihcList.Count > 0)
            {

                if (ihcList.Count > 1)
                {

                    hmc.Text = @"Your dedicated home mortgage consultant are: <br/>" + (contacts.Trim().TrimEnd(',') + ".");
                }
                else if (ihcList.Count == 1)
                {

                    hmc.Text = @"Your dedicated home mortgage consultant is: <br/>" + (contacts.Trim().TrimEnd(',') + ".");
                }

              

                rptContactInformation.DataSource = ihcList.OrderBy(x => x.Fields["Last Name"].Value).ThenBy(n => n.Fields["First Name"].Value); ;
                rptContactInformation.DataBind();
                rptContactInformation.Visible = true;
                hlkStarLoanApp.Visible = true;
            }
            else
            {
                hmc.Text = "Sorry, we could not find a mortgage consultant available for this area.";
                rptContactInformation.Visible = false;
                hlkStarLoanApp.Visible = false;

            }

            

        }

        #endregion


    }
}
