﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class disclaimers : ControlBase
    {
        public string MyDisclaimerText
        {
            get
            {
                return CurrentPage.DisclaimerText;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["id"] != null)
            {
                Sitecore.Context.Item = SCContextDB.GetItem(new Sitecore.Data.ID(Request["id"]));
            }
        }
    }
}