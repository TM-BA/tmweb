﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;

namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class BlogMenu : ControlBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Item[] matchingBlogTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Blog));

            Item homeItem = Sitecore.Context.Database.GetItem(matchingBlogTemplates[0].Fields["Home Path"].Value);
            hlkHome.NavigateUrl = SCUtils.GetItemUrl(homeItem).Replace(SCCurrentHomePath, string.Empty);

            Item archiveItem = Sitecore.Context.Database.GetItem(matchingBlogTemplates[0].Fields["Archive Path"].Value);
            hlkArchive.NavigateUrl = SCUtils.GetItemUrl(archiveItem).Replace(SCCurrentHomePath, string.Empty);

            Item navigateItem = Sitecore.Context.Database.GetItem(matchingBlogTemplates[0].Fields["Contact Path"].Value);
            hlkContact.NavigateUrl = SCUtils.GetItemUrl(navigateItem).Replace(SCCurrentHomePath, string.Empty);

            Item subscribeItem = Sitecore.Context.Database.GetItem(matchingBlogTemplates[0].Fields["Subscribe Path"].Value);
            hlkSubscribe.NavigateUrl = SCUtils.GetItemUrl(subscribeItem).Replace(SCCurrentHomePath, string.Empty);


            Item companyItem = SCContextDB.GetItem(SCCurrentHomePath).Parent;
            hlkSite.Text = companyItem.DisplayName.ToUpper() + ".COM";

            string url = matchingBlogTemplates[0].Fields["Site URL"].Value;
            bool hasHttp = false;

            if (url != string.Empty)
            {
                hasHttp = url.ToLower().Contains("http://");
            }


            if (hasHttp)
            {
                hlkSite.NavigateUrl = url;
            }
            else
            {
                hlkSite.NavigateUrl = "http://" + url;
            }
        }
    }
}