﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;

namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class RecentPosts : ControlBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetRecentPosts();
        }

        private void GetRecentPosts()
        {
            Item[] recentPosts = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Entry));


            rptRecentPosts.DataSource = recentPosts.Where(x => SCUtils.SCDateTimeToMsDateTime(x.Fields["DateCreated"].Value) <=DateTime.Now)
						.OrderByDescending(x =>SCUtils.SCDateTimeToMsDateTime(x.Fields["DateCreated"].Value))
						.Take(5);
            rptRecentPosts.DataBind();
        }

        protected void rptRecentPosts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    HyperLink postLink = (HyperLink)e.Item.FindControl("postLink");
                    postLink.NavigateUrl = SCUtils.GetItemUrl(currentItem).Replace(SCCurrentHomePath,"");
                    postLink.Text = currentItem.Fields["Title"].ToString();

                  

                }
            }
        }
    }
}