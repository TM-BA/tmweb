﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchiveMain.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.ArchiveMain" %>

<div class="blog-banner">
   <asp:Image ID="blogBanner" runat="server" />
</div>


<div class="blog-lt">
   

    <sc:placeholder ID="phBlogMenu" key="phBlogMenu" runat="server" />

    <div class="barchive">
        <h1><sc:fieldrenderer runat="server" id="Title" fieldname="Title"></sc:fieldrenderer></h1>
    </div>

    <ul class="ularch">
        <asp:Repeater ID="rptCategories" runat="server" onitemdatabound="rptCategories_ItemDataBound">
            <ItemTemplate>
                <li>
                    <asp:HyperLink runat="server" ID="hlkCategory"><%#Eval("Name") %></asp:HyperLink>
                </li>
            </ItemTemplate>  
        </asp:Repeater>
    </ul>

    
        <asp:Repeater ID="rptEntriesbyCategory" runat="server" onitemdatabound="rptEntriesbyCategory_ItemDataBound">
        <ItemTemplate>
                <div class="busehs">
               <%--<asp:Label ID="lblCategoryTitle" runat="server" Text=""></asp:Label>--%>
               <a id="catTitle"  runat="server"></a>
                </div>

                <table class="arch-blog">
                    <tbody><tr>
	                    <td class="arch-hea">Date </td>
	                    <td class="arch-hea">Title </td>
	                 <%--   <td class="arch-hea">Comments</td>--%>
                    </tr>
                  
                    
                    <asp:Repeater ID="rptEntries" runat="server">
                        <ItemTemplate>
                           <tr>
                                <td><span><%#Eval("Date")%></span></td>
                                <td><asp:HyperLink runat="server" ID="hlkEntry" NavigateUrl='<%#Eval("Url") %>'><%#Eval("Name") %></asp:HyperLink></td>
                                </br>
                           <%--     <td><span><%#Eval("CommentsSummary")%></span></td>--%>
                           </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    

                  </tbody>
                 </table>
        </ItemTemplate>
        </asp:Repeater>
    

</div>
<div class="blog-rt">
  <sc:placeholder ID="phBlogSidebar" key="phBlogSidebar" runat="server" />
</div>