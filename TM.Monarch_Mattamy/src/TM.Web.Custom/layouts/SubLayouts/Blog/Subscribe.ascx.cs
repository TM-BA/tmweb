﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Domain.Entities;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class Subscribe : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item blog = SCUtils.GetNearestParentOfTemplateID(SCIDs.Blog.TM.Blog, SCContextItem.ID, SCCurrentHomeItem, SCContextDB);

            string facebookUrl = "";
            string twitterUrl = "";
            string rssURl = "";

            if (blog != null)
            {
                if (!string.IsNullOrEmpty(blog.Fields["Facebook URL"].Value))
                {
                    facebookUrl = ((Sitecore.Data.Fields.LinkField)blog.Fields["Facebook URL"]).Url;
                    lnkFacebook.NavigateUrl = facebookUrl;
                    lnkFacebook.Visible = true;
                    heading.Visible = true;
                }

                if (!string.IsNullOrEmpty(blog.Fields["Twitter URL"].Value))
                {
                    twitterUrl = ((Sitecore.Data.Fields.LinkField)blog.Fields["Twitter URL"]).Url;
                    lnkTwitter.NavigateUrl = twitterUrl;
                    lnkTwitter.Visible = true;
                    heading.Visible = true;
                }

                if (!string.IsNullOrEmpty(blog.Fields["Blog Rss URL"].Value))
                {
                    rssURl = blog.Fields["Blog Rss URL"].Value;
                    lnkRss.NavigateUrl = rssURl;
                    lnkRss.Visible = true;
                    heading.Visible = true;
                }
            }
        }
    }
}