﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Subscribe.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.Subscribe" %>
<h1 runat="server" id="heading" visible="false">
    Subscribe</h1>
<ul class="blog-icons">
    <li>
        <asp:HyperLink Visible="false" ID="lnkRss" runat="server" CssClass="blog-y" Target="_blank"></asp:HyperLink></li>
    <li>
        <asp:HyperLink Visible="false" ID="lnkTwitter" runat="server" CssClass="blog-twitter"
            Target="_blank"></asp:HyperLink></li>
    <li>
        <asp:HyperLink Visible="false" ID="lnkFacebook" runat="server" CssClass="blog-fb"
            Target="_blank"></asp:HyperLink></li>
</ul>