﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpecificCampaign.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Campaigns.SpecificCampaign" %>
<div class="banner-go">
	<div class="banner" style="float:right;">
        <sc:Placeholder ID="scImageRotator" runat="server" Key="Slideshow" />
	</div>
	<div class="across-lt" id="bannerLeftDiv" runat="server">
		<div class="go-logo">
			<sc:Image ID="Image1" Field="Campaign title image" runat="server"/>
		</div>
		<div class="go-date" id="goDate" runat="server">
			<h1><sc:Text ID="shortTitle" runat="server" Field="Campaign Title" /></h1>
			<p><sc:Text ID="Text1" runat="server" Field="Campaign title sub text" /></p>
		</div>
		<ul class="ltnavbut">
			<li><a href="<%=CurrentPage.SCCurrentItemUrl%>/request-information/">Ask a Question </a></li>
		</ul>
		<div class="go-all-c ccs" id="goAllCom" runat="server">
			<asp:HyperLink ID="hypAreaCommunities" runat="server">View all <%= CityName %> Area Communities</asp:HyperLink>
		</div>
	</div>
</div>
<div id="promoContentArea" runat="server" class="dt-lt" style="width:100%; margin:0;">
<h1 style="margin:10px"><sc:Text ID="Text2" runat="server" Field="Campaign title Long" /></h1>
</div>
<sc:Text ID="Text3" runat="server" Field="Campaign Content" />

<sc:placeholder runat="server" id="SiteTree" key="SiteTree">
</sc:placeholder>

    <asp:Literal ID="litStyle" runat="server"></asp:Literal>