﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Collections.Generic;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Campaigns
{
    public partial class GenericCampaign : CampaignBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

            if (isExpired(contextItem))
            {

                Response.Redirect(SCCurrentHomePath + SCUtils.GetItemUrl(contextItem.Parent));
            }
        }
    }
}