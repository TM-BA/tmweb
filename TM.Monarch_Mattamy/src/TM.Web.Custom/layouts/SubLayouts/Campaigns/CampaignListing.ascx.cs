﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;
using Image = System.Web.UI.WebControls.Image;

namespace TM.Web.Custom.Layouts.SubLayouts.Campaigns
{
    public partial class CampaignListing : ControlBase
    {
        private List<Item> itemList;
        private bool exitRecursive = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            populateCampaigns();

        }

        private void populateCampaigns()
        {
            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

            Sitecore.Data.Fields.ReferenceField ddlListingOrder = contextItem.Fields["Campaign Listing Order"];
            string order = ddlListingOrder.Value;

            if (order == "Ascending")
            {
                TraverseUp(contextItem);
            }
            else
            {
                TraverseDown(contextItem);
            }

            rptCampaignListing.DataSource = itemList;
            rptCampaignListing.DataBind();


        }

        private void TraverseUp(Item contextItem)
        {
            Item parentNode = Sitecore.Context.Database.GetItem(SCCurrentHomePath);

            Item stopNode = contextItem;
            itemList = new List<Item>();

            Traverse(parentNode, stopNode);


        }

        private void TraverseDown(Item contextItem)
        {
            Item parentNode = contextItem.Parent;
            itemList = new List<Item>();

            Traverse(parentNode);


        }

        private void Traverse(Item item, Item stopNode)
        {
            if (exitRecursive != true)
            {


                if (item.TemplateID.ToString() == SCIDs.Promos.Promotions.ToString() ||
                    item.TemplateID.ToString() == SCIDs.Promos.GenericCampaign.ToString() ||
                    item.TemplateID.ToString() == SCIDs.Promos.SpecificCampaign.ToString())
                {

                   if(CheckIfPromoIsValid(item))
                        itemList.Add(item);

                }


                Sitecore.Collections.ChildList childs = item.GetChildren();

                if (item.ID == stopNode.ID)
                {
                    Sitecore.Collections.ChildList lastChild = stopNode.GetChildren();
                    foreach (Item child in lastChild)
                    {
                        if (child.TemplateID.ToString() == SCIDs.Promos.Promotions.ToString() ||
                            child.TemplateID.ToString() == SCIDs.Promos.GenericCampaign.ToString() ||
                            child.TemplateID.ToString() == SCIDs.Promos.SpecificCampaign.ToString())
                        {
                           if(CheckIfPromoIsValid(item))
                                itemList.Add(child);
                        }
                    }

                    exitRecursive = true;
                }

                foreach (Item child in childs)
                {

                    Traverse(child, stopNode);

                }

            }
            else
            {
                return;
            }

        }

        private void Traverse(Item item)
        {

            if (item.TemplateID.ToString() == SCIDs.Promos.Promotions.ToString() ||
                item.TemplateID.ToString() == SCIDs.Promos.GenericCampaign.ToString() ||
                item.TemplateID.ToString() == SCIDs.Promos.SpecificCampaign.ToString())
            {
               if(CheckIfPromoIsValid(item))
                itemList.Add(item);
            }


            Sitecore.Collections.ChildList childs = item.GetChildren();

            foreach (Item child in childs)
            {
                Traverse(child);
            }

        }

        protected void rptCampaignListing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item) e.Item.DataItem;

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal relatedLink = (Literal) e.Item.FindControl("Headline");

                    Label content = (Label) e.Item.FindControl("ContentText");
                    content.Text = currentItem.Fields["Teaser Content"].ToString();

                    LinkField linkField = currentItem.Fields["External Link"];
                    relatedLink.Text = String.Format("<a class=\"sc\" onclick=\"TM.Common.GAlogevent('Campaign', 'Click', 'CampaignLink');\" href=\"{0}\">{1}</a>",
                                                    linkField.Url,content.Text);

                    string imageURL = string.Empty;
                    Sitecore.Data.Fields.ImageField imageField = currentItem.Fields["Teaser Image"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                        imageURL = Sitecore.StringUtil.EnsurePrefix('/',
                                                                    Sitecore.Resources.Media.MediaManager.GetMediaUrl(
                                                                        image));
                    }

                    Image campaignImage = (Image) e.Item.FindControl("Image");
                    campaignImage.ImageUrl = imageURL;

                }
            }
        }


        private bool CheckIfPromoIsValid(Item item)
        {
            var disabled = item[SCIDs.PromoBase.Disabled] == "1";
            var endDateFieldValue = item[SCIDs.PromoBase.EndDate];
            var endDate = !String.IsNullOrEmpty(endDateFieldValue)
                              ? DateUtil.IsoDateToDateTime(endDateFieldValue)
                              : DateTime.MaxValue;

            return (!disabled && endDate > DateTime.Now);
        }
    

}
}
