﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data.Items;


namespace TM.Web.Custom.Layouts.SubLayouts.Campaigns
{
    public class CampaignBase : ControlBase
    {
        DateTime endDate;

        public bool isExpired(Item contextItem)
        {

            bool expired = false;

            if (contextItem.Fields["End Date"] != null)
            {
                endDate = SCUtils.SCDateTimeToMsDateTime(contextItem.Fields["End Date"].ToString());

                if (endDate != DateTime.MinValue)
                {
                    if (endDate <= DateTime.Now)
                    {
                        expired = true ;
                    }

                }
            }

            bool disabled = false;

            if (contextItem.Fields["Campaign Disabled"] != null)
            {
                disabled = contextItem["Campaign Disabled"] == "1";

            }
          


            return expired||disabled;
        
        }
    }
}
