﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityClosed.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityClosed" %>
    
<div class="lt-col-cd">
    <section class="hm-lt_sr">
        <div class="go-logo" runat="server" id="logo" visible="false">
            <sc:Image ID="Image1" Field="Community Logo" runat="server" />
        </div>
        <h1 class="leftTitle_CD" runat="server" id="communityTitle" visible="false">
            <sc:Text ID="scName" runat="server" Field="Community Name" />
        </h1>
        <div class="navln">
        </div>
        <p class="leftsubTitle2_CD">
            <%= lastStatusChange%>
            <br>
        </p>
        <div id="callsToAction" class="callsToAction" runat="server">    
            <ul class="ltnavbut">
                <li><a href="<%=CurrentPage.SCVirtualItemUrl%>/request-information/">Ask A Question</a></li>
            </ul>
            <ul class="ltnavlnk">
                <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                    rel="nofollow">Print</a></li>
            </ul>
        </div>
    </section>
    <div class="anno-plan" id="goAllCom" runat="server">
        <asp:HyperLink ID="hypAreaCommunities" runat="server">View all <%= Division%> Area Communities</asp:HyperLink>
    </div>
</div>
<div class="rt-col">
    <div class="banner">
        <sc:Placeholder ID="scImageRotator" runat="server" Key="Slides" />
    </div>
</div>
<div class="det-wrapper-bt">
    <div class="cd_left">
        <div class="geneInfo">
            <h1 id="secondaryHeading" runat="server">
                SIMILAR COMMUNITIES</h1>
        </div>
        <div class="geneInfoBox">
            <span runat="server" id="spanSimilarCommunities">
                <h2>
                    The following nearby communities are similar to
                    <sc:Text ID="Text2" runat="server" Field="Community Name" />
                </h2>
                <div id="similarCommunityList" data-bind="foreach: SimilarCommunities">
                    <div class="grayLineS">
                    </div>
                    <a href="#" class="plan-nea" data-bind="text:name,attr:{href:url}"></a>
                    <p>
                        <span data-bind="text:priceRange"></span>
                        <br>
                        <span data-bind="text:sqftRange"></span>Sq. Ft.<br>
                        <span data-bind="text:bedRange"></span>Bedrooms<br>
                        <span data-bind="text:bathRange"></span>Bathrooms</p>
                </div>
            </span>
            <sc:Placeholder ID="leadForm" Key="RFIWidget" runat="server" />
            <sc:Placeholder ID="phSecondaryNavWidgets" Key="secNavWidgets" runat="server" />
        </div>
    </div>
    <!--end of cd_left-->
    <sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />
    <!--end cd_right-->
</div>
<script type="text/javascript">
<%=litSimilarCommunities %>
$j(document).ready(function () {
var SimilarComModel = function (SimilarCommunities) {
		var self = this;
		self.SimilarCommunities = ko.observableArray(ko.utils.arrayMap(SimilarCommunities,
		function (simCom) {
			return {
				url: simCom.url
				,name: simCom.name
                ,priceRange: simCom.pMin
                ,sqftRange: simCom.sMin != simCom.sMax ? simCom.sMin +' - '+simCom.sMax : simCom.sMax
                ,bedRange: simCom.bdMin!=simCom.bdMax?simCom.bdMin + ' - '+ simCom.bdMax:simCom.bdMax
                ,bathRange: simCom.bMin!=simCom.bMax?simCom.bMin + ' - '+ simCom.bMax:simCom.bMax
			};
		}
	));
	};

        if(window.similarComInitData){
	        ko.applyBindings(new SimilarComModel(similarComInitData), document.getElementById('similarCommunityList'));
        }
	});
</script>