﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityInfo.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityInfo" %>
<asp:Panel ID="pnlOneColumn" runat="server">
    <h1>About <sc:Text ID="Text1" runat="server" Field="Community Name" /></h1>
    <span class="justify">
    <sc:Text ID="CommunityDescription" runat="server" Field="Community Description" />
    </span>
</asp:Panel>
<asp:Panel ID="pnlTwoColumn" runat="server" Visible="false">
<div class="dt-lt cicl">
    <h1>About <sc:Text ID="Text4" runat="server" Field="Community Name" /></h1>
	<span class="justify">
	<sc:text ID="stCommDesc" field="Community Description" runat="server"></sc:text>
	</span>
</div>
<div class="dt-rt cicl">
    <h1><sc:Text ID="Text5" runat="server" Field="Column Two Heading" /></h1>
    <span class="justify">
    <asp:Literal ID="CommunityDesc" runat="server"></asp:Literal>
    </span>	
</div>
</asp:Panel>
<ul class="infobtb" id="nextSteps">
<%=ComInfoNavUL %>
</ul>
<div style="clear:both;"></div>
    <p id="financeDream" runat="server" style="padding-bottom:1.5em;" class="callToAction">
        Learn about options to <asp:HyperLink id="financeLink" Target="_blank" runat="server" class="cil">finance your dream home at <sc:Text ID="Text17" runat="server" Field="Community Name" /></asp:HyperLink>
    </p>
    <p id="contructionUpdates" runat="server" class="callToAction">
        Read the latest <asp:HyperLink ID="constructionLink" runat="server"><sc:Text ID="Text2" runat="server" Field="Community Name" /> construction updates</asp:HyperLink> and 
        <a href="<%=CurrentPage.SCCurrentItemUrl%>/request-information/">sign up to receive new updates about <sc:Text ID="Text3" runat="server" Field="Community Name" /></a>
    </p>

