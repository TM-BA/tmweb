﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CompressCommunityHeader : CommunityBase
    {
        private string _relativePath;
        public string RelativePath
        {
            get
            {
                if (_relativePath == null) {


                    _relativePath = SCUtils.GetItemUrl(CurrentPage.CurrentCommunity);

                }
                return _relativePath.ToLower();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                sass.Visible = (CurrentPage.CurrentIHC != null);
                if (Request.Path.EndsWith("driving-directions",StringComparison.CurrentCultureIgnoreCase))
                {
                    lnkDrivingDirections.Visible = false;
                }
                else
                {
                    lnkDrivingDirections.NavigateUrl = RelativePath + "/driving-directions";
                }
                if (Request["sc_device"] != null)
                {
                    drpNavigator.Visible = false;
                    col3.Visible = false;
                }
                else
                {
                    if ( (SCContextItem.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.PlanPage)
                        || (SCContextItem.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.HomeForSalePage))
                    {
                        drpNavigator.Items.Add("(Please Select)");
                    }

                    drpNavigator.Items.Add(new ListItem("Community Information", RelativePath));
                    if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Site Plan Description"]) ||
                       !string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Site Plan Image"]))
                    {
                        drpNavigator.Items.Add(new ListItem("Site Plan", RelativePath + "/site-plan"));
                    }
                    drpNavigator.Items.Add(new ListItem("Driving Directions", RelativePath + "/driving-directions"));

                    drpNavigator.Items.Add(new ListItem("Photos", RelativePath + "/photos"));
                    drpNavigator.Items.Add(new ListItem("Local Interests", RelativePath + "/local-interests"));
                    if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Community Brochure"]))
                    {
                        Sitecore.Data.Fields.FileField brochurePath = CurrentPage.CurrentCommunity.Fields["Community Brochure"];
                        drpNavigator.Items.Add(new ListItem("Community Brochure", brochurePath.Src));
                    }
                    drpNavigator.Items.Add(new ListItem("Home Features", RelativePath + "/home-features"));

                    if (!string.IsNullOrWhiteSpace(financeYourDreamsUrl))
                        drpNavigator.Items.Add(new ListItem("Finance Your Dream", financeYourDreamsUrl));

                    if (!string.IsNullOrWhiteSpace(ConstructionUpdatesUrl))
                        drpNavigator.Items.Add(new ListItem("Construction Updates", ConstructionUpdatesUrl));

                    drpNavigator.SelectedValue = Request.Path.ToLower();                    

                }
            }
        }
    }
}