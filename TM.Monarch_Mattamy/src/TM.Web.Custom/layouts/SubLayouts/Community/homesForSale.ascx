﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="homesForSale.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.homesForSale" %>
<script type="text/javascript">
    //Data for home cards
    <%= HomeForSaleJson %>
    var myHomesModel;
    var userIsLoggedIn = <%=UserIsLoggedIn %>;
   $j(document).ready(function () {

       if(typeof String.prototype.trim !== 'function') {
           String.prototype.trim = function() {
               return this.replace(/^\s+|\s+$/g, ''); 
           }   
       }

       var HomesModel = function (Homes) {
           var self = this;

           self.PreviousSubCommunity = "";
           self.Homes = ko.observableArray(ko.utils.arrayMap(Homes,
           function (home) {
        
        
               if(home.addr == null || home.addr == "undefined" ||home.addr == "")
               {
        
                   hfsfullname = home.name.trim();
               }else
               {
        
                   hfsfullname = home.name.trim() + " " + home.addr.trim();
               }

               return {
                   img: home.img,
                   id: home.id,
                   url: home.url,
                   purl: home.purl,
                   name: home.name,
                   addr: home.addr,
                   lot: home.lot,
                   avail: home.dateS,
                   availText: home.date,
                   price: '$' + home.price.formatMoney(0, '.', ','),
                   subCom: home.subCom,
                   sqft: home.sqft.formatMoney(0, '.', ','),
                   fullName: home.pname + " at " + hfsfullname,
                   bed: home.bed
                   ,pname:home.pname
                   , bath: home.bath
                   , garage: home.garage
                   ,story: home.story
                   ,isfav:ko.observable(home.isFav)
                   ,shouldShowSubCom : function (current){
                       if(self.PreviousSubCommunity != initialData[current].subCom){
                           self.PreviousSubCommunity = initialData[current].subCom;
                           return true;
                       }
                       return false;
                   }
                   ,toggleFav:function(){
                       if(userIsLoggedIn){
                           if(this.isfav())
                           {
                               this.isfav(!this.isfav());
                               AddRemoveFavorites(this.id, 'HomeForsale', 'Remove');
                           } 
                           else {
                               this.isfav(!this.isfav());
                               AddRemoveFavorites(this.id, 'HomeForsale', 'Add');
                                            
                           }
                       }
                       else 
                       {
                           //TM.Common.ShowLoginModal(document.location.href);
                           TM.Common.ShowLoginModal("?id=" + this.id + "&type=HomeForsale&action=Add");
                       }
                   }
               };
           }
       ));
           self.sortBy = function(sel){
               var key = sel.options[sel.selectedIndex].value;

               self.Homes().sort(function(a,b){ return TM.Common.Plan.Sort(a,b,key);});
               self.PreviousSubCommunity = "";
        
               self.Homes.valueHasMutated();
           }
       };
       myHomesModel = new HomesModel(initialData);
       ko.applyBindings(myHomesModel, document.getElementById('homeRepeater'));
   });
</script>
<style>
.sorted .series, .hidden{display:none;}
</style>
<div>
	<select name="sortList" class="sort" onchange="myHomesModel.sortBy(this)">
		<option value="subCom-avail">Sort List</option>
		<option value="subCom-price">Price low to high</option>
		<option value="subCom-priceReverse">Price high to low</option>
        <option value="subCom-pname">Plan Name</option>
        <option value="subCom-avail">Availability Date</option>
        <option value="subCom-bed">Bedrooms</option>
		<option value="subCom-sqft">Square Footage</option>
		
          <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
            { %> <option value="subCom-story">Stories</option> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
            {  %>  <option value="subCom-story">Storeys</option> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
            {  %>  <option value="subCom-story">Stories</option> <% } %>
	</select>
</div>
<asp:Literal ID="litPrintHeader" runat="server"></asp:Literal>
<div id="homeRepeater" data-bind="foreach: Homes">
	<div data-bind="attr:{'class' : shouldShowSubCom($index())?'series':'hidden' }">
		<h1 data-bind="text:subCom"></h1>
	</div>
    <div class="CD_commCard" style="clear:both;">
        <div class="CDimage">
            <a data-bind="attr:{href: url}"><img data-bind="    attr:{'src':img}" width="160" height="90"></a>
        </div>
        <div class="CDmiddleHs">
            <a data-bind="attr:{href: url}" class="CDhs" onclick="TM.Common.GAlogevent('Details','Click','HomeAddressLink')"><span data-bind="    text: fullName"></span></a>&nbsp;(homesite <span class="CDhs1" data-bind="    text: lot"></span>)
            <p class="CDinfoHs">
                <span data-bind="text: sqft"></span> Sq. Ft. &nbsp;  |&nbsp;   
                <span data-bind="text: bed"></span> Bed &nbsp;  | &nbsp;  
                <span data-bind="text: bath"></span> Bath&nbsp;   | &nbsp;  
                <span data-bind="text: garage"></span> Garage &nbsp; |&nbsp;   
                
                <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
                { %> <span data-bind="text: story"></span> Stories <% } 
                    else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
                {  %>  <span data-bind="text: story"></span> Storeys <% } 
                    else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
                {  %>  <span data-bind="text: story"></span> Stories <% } %>
            </p>
            <div class="pbt">
                <p class="CDprice1" data-bind="text:availText"></p>
                <p class="CDprice2" data-bind="text:price"></p>
            </div>
        </div>
        <div class="CDright callToAction">
            <ul class="addFav2">
		        <li><a href="#" data-bind="text : (isfav() ? 'Favorite' : 'Add to Favorites'),click:toggleFav"></a></li>
		    </ul>
            <div class="viewPlanBtnHs">
                <a data-bind="attr:{href: url}"class="button-arrow">View Home<span class="arrow-ri"></span></a>
            </div>
        </div>            
    </div>
</div>

<script>
    $j(document).ready(function () {
        $j(".CDimage a").click(function () { TM.Common.GAlogevent('Details', 'Click', 'HomesForSale') });
        $j("a.CDhs").click(function () { TM.Common.GAlogevent('Details', 'Click', 'HomeAddressLink')});
        $j(".addFav2 a").click(function () { TM.Common.GAlogevent('Favorites', 'Click', 'AddtoFavorites')});
        $j(".viewPlanBtnHs a").click(function () { TM.Common.GAlogevent('Search', 'Click', 'ViewHome') });

    });
</script>
