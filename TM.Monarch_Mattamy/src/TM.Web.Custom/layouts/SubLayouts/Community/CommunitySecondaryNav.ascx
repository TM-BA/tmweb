﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunitySecondaryNav.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunitySecondaryNav" %>

    <div class="cd_right">
        <ul class="commDetMenu" id="communitySubNav" runat="server">
            <li class="cdm" id="liCominfo" runat="server">
                <a href="<%=CurrentPage.SCCurrentItemUrl %>" id="cominfo">COMMUNITY INFORMATION</a>
                    <img src="/Images/arrow-comm.png" onclick="$j(this).siblings('ul').toggle()" />
                    <ul class="comInfo">
                        <%=ComInfoNavUL %>
                        <asp:Literal ID="litFinanceYourDream" runat="server"></asp:Literal>
                        <asp:Literal ID="litConstructionUpdates" runat="server"></asp:Literal>
                    </ul>
            </li>
            <li class="cdm" id="liEventDetails" runat="server"><a href="<%=CurrentPage.SCCurrentItemUrl %>/event-details" id="eventDetails">EVENT DETAILS</a></li>
            <li class="cdm" id="liHomesForSale" runat="server"><a href="<%=CurrentPage.SCCurrentItemUrl %>/homes-ready-now" id="homeforsale" >HOMES FOR SALE</a></li>
            <li class="cdm" id="liFloorPlans" runat="server"><a href="<%=CurrentPage.SCCurrentItemUrl %>/floor-plans" id="floorPlans" >FLOOR PLANS</a> </li>
            <li class="cdm" id="liSchools" runat="server"><a href="<%=CurrentPage.SCCurrentItemUrl %>/schools" id="school" >SCHOOLS</a> </li>
            <li class="cdm" id="liPromos" runat="server"><a href="<%=CurrentPage.SCCurrentItemUrl %>/promotions" id="promos" >PROMOTIONS </a></li>
        </ul>
        <div class="comSections commInfo" id="divCominfo" runat="server">
        <div id="About"></div>
            <sc:Placeholder id="commInfo" Key="CommunityInfo" runat="server" ></sc:Placeholder>
        </div>
        <!--end of commInfo-->
        <div class="comSections commEventDetails" id="divEventDetails" runat="server">
        <div id="Event-Deails"></div>
            <sc:Placeholder ID="phEventDetails" Key="EventDetails" runat="server" />
        </div>
        <div class="comSections commHomeForSale"  id="divHomesForSale" runat="server">
        <div id="Homes-Ready-Now"></div>
            <sc:Placeholder ID="phHomeForSale" Key="HomesForSale" runat="server" />
        </div>
        <div class="comSections commFloorPlan" id="divFloorPlans" runat="server">
        <div id="Floor-Plans"></div>
            <sc:Placeholder ID="phFloorPlans" Key="FloorPlans" runat="server" />
        </div>
        <div class="comSections commSchool" id="divSchools" runat="server">
        <div id="Schools"></div>
            <sc:Placeholder ID="phSchools" Key="Schools" runat="server" />
        </div>
        <div class="comSections commPromo"  id="divPromos" runat="server">
        <div id="Promotions"></div>
            <sc:Placeholder ID="phPromotions" Key="Promotions" runat="server" />
        </div>
    </div>
    <style type="text/css">
    .commDetMenu ul.comInfo
    {
        display: none;
    }

    </style>
    <script type="text/javascript">
        function showInfo(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commInfo").show();
            $j("#<%= liCominfo.ClientID%>").addClass("on");
            if(sendEvent)
            TM.Common.GAlogevent('Details', 'Click', 'Tab-CommunityInfo');
        }
        function showEventDetails(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commEventDetails").show();
            $j("#<%= liEventDetails.ClientID%>").addClass("on");
            if(sendEvent)
            TM.Common.GAlogevent('Details', 'Click', 'Tab-EventDetails');
        }
        function showHFS(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commHomeForSale").show();
            $j("#<%= liHomesForSale.ClientID%>").addClass("on");
            if (sendEvent)
                TM.Common.GAlogevent('Search', 'Click', 'Tab-HomeForSale');
        }
        function showFP(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commFloorPlan").show();
            $j("#<%= liFloorPlans.ClientID%>").addClass("on");
            if (sendEvent)
                TM.Common.GAlogevent('Details', 'Click', 'Tab-FloorPlans');
        }
        function showSch(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commSchool").show();
            $j("#<%= liSchools.ClientID%>").addClass("on");
            if (sendEvent)
                TM.Common.GAlogevent('Search', 'Click', 'Tab-Schools');
        }
        function showPromo(sendEvent) {
            $j(".commDetMenu li").removeClass("on");
            $j(".comSections").hide();
            $j(".commPromo").show();
            $j("#<%= liPromos.ClientID%>").addClass("on");
            if (sendEvent)
                TM.Common.GAlogevent('Search', 'Click', 'Tab-Promotions');
        }

        var tab = '<%=CurrentTab%>';
       
        switch (tab) {
            case "about":
                showInfo(false);
                break;
            case "event-details":
                showEventDetails(false);
                break;
            case "homes-ready-now":
                showHFS(false);
                break;
            case "schools":
                showSch(false);
                break;
            case "promotions":
                showPromo(false);
                break;
            case "floor-plans":
                showFP(false);
                break;
            default:
                //make sure we are not trying to print the page
                if (window.location.search.indexOf("sc_device=print") == -1) {
                    showInfo(false);
                }
                break;
        }
</script>