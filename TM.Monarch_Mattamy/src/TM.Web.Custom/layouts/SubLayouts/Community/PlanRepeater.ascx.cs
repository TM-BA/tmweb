﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Data.Events;
using TM.Domain.Enums;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.LuceneQueries;
using SCID = Sitecore.Data.ID;
using System.Text;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;
using Sitecore.Diagnostics;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class PlanRepeater : CommunityBase
    {
        public string PlansRepeaterData;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] != null)
            {
                litPrintHeader.Text = "<h1>Floor Plans</h1>";

            }
            else
            {
                CurrentPage.Title = "View floor plans at " + CurrentPage.PageTitle;
            }
            GetValues();
        }

        private SCID currentItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetValues();
            }
        }



        protected void GetValues()
        {
            using (new SecurityDisabler())
            {
                using (new BulkUpdateContext())
                {
                    using (new EventDisabler())
                    {
                        PlansRepeaterData = AdminMode(Plans);
                    }
                }
            }
        }
       

        private string AdminMode(IEnumerable<Item> Homes)
        {
            Homes = Homes.OrderBy(SortOrderOfSubCommunity)
                          .ThenBy(SubCommunityName)
                          .ThenBy(h => h["Priced from Value"].CastAs<int>(0))
                          .GroupBy(SubCommunityName)
                          .SelectMany(g => g);
                         

            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            sb.AppendLine("var PlanInitialData = [");

            foreach (Item home in Homes)
            {
                try{
                Item subCommunity = null;
                sb2.Append("{");
                if(!string.IsNullOrWhiteSpace( home["Elevation Images"])){
                    string[] imgIds = home["Elevation Images"].Split('|');
                    Item image = SCContextDB.GetItem(new SCID(imgIds[0]));
                    sb2.AppendFormat("img: '{0}',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                } else {
                    sb2.Append("img: '',");
                }

                if (!string.IsNullOrEmpty(home["SubCommunity ID"]))
                {
                    subCommunity = SCContextDB.GetItem(new SCID(home["SubCommunity ID"]));
                }
                    string subComName = "";
                    if (null != subCommunity)
                    {
                        subComName = subCommunity.DisplayName;
                    }
                    sb2.AppendFormat("subCom: '{0}',", subComName.Replace("'", "\\'"));


                    sb2.AppendFormat("url: '{0}',", SCUtils.GetItemUrl(home).Replace(".aspx", string.Empty));
                sb2.AppendFormat("id: '{0}',", home.ID.ToString());
                sb2.AppendFormat("name: '{0}',", (string.IsNullOrWhiteSpace(home["Plan Name"]) ? home.DisplayName : home["Plan Name"] ));
                sb2.AppendFormat("price: {0},", home["Priced from Value"].CastAs<int>(0));
                sb2.AppendFormat("pricet: '{0}',", home["Priced from Text"].Replace("'", "\\'"));
                sb2.AppendFormat("sqft: {0},", home["Square Footage"].CastAs<int>(0));
                sb2.AppendFormat("bed: {0},", home["Number of Bedrooms"].CastAs<int>(0));
                sb2.AppendFormat("bath: {0},", home["Number of Bathrooms"].CastAs<int>(0) + (home["Number of Half Bathrooms"].CastAs<int>(0) > 0 ? 0.5 : 0 ));
                sb2.AppendFormat("garage: {0},", home["Number of Garages"].CastAs<int>(0));
                sb2.AppendFormat("story: {0},", home["Number of Stories"].CastAs<int>(0));
                bool isClosedCommunity = (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.Closed);

                if (isClosedCommunity || string.IsNullOrWhiteSpace(home["Virtual Tours Master Plan ID"]) )
                {
                    sb2.Append("vtUrl:'',");
                }
                else
                {
                    SCID masterPlanVTItemID;
                    if (SCID.TryParse(home["Virtual Tours Master Plan ID"], out masterPlanVTItemID))
                    {
                        Item masterPlanVTItem = SCContextDB.GetItem(masterPlanVTItemID);
                        sb2.AppendFormat("vtUrl:'{0}',", ((masterPlanVTItem != null) && (masterPlanVTItem.Fields["Virtual Tour URL"] != null)) && (masterPlanVTItem.Fields["Virtual Tour URL"].Value.IsNotEmpty())
                            ? masterPlanVTItem.Fields["Virtual Tour URL"].GetLinkFieldPath()
                            : string.Empty);
                    }
                }

                //Get Model Home Info
                Item matchedInventory = null; 
             
                var homeType = new PlanQueries().IsAvailableAsModelHome(home, CurrentPage.CurrentDivision,
                                                                       CurrentPage.CurrentCommunity, 
                                                                       out matchedInventory); 
                 switch(homeType)
                 {
                     case HomeTourOption.AvailableAsModelHome:
			 var MHUrl = SCUtils.GetItemUrl(matchedInventory).Replace(".aspx", string.Empty);
			 sb2.AppendFormat("mhUrl:'{0}',", isClosedCommunity ? string.Empty : MHUrl);
                         break;
	          /*  case HomeTourOption.AvailableAsModelHomeInNearByCommunity:
			 var MHNear = SCUtils.GetItemUrl(matchedInventory).Replace(".aspx", string.Empty);
			 sb2.AppendFormat("mhNear:'{0}',", isClosedCommunity ? string.Empty : MHNear);
                         break;
		    case HomeTourOption.AvailableAsInventory:
			  var HFSUrl = SCUtils.GetItemUrl(matchedInventory).Replace(".aspx", string.Empty);
			  sb2.AppendFormat("HFSUrl:'{0}',", isClosedCommunity ? string.Empty : HFSUrl);
                         break;*/
                 }
               

                sb2.AppendFormat("showFav:{0},", isClosedCommunity ? "false" : "true");
                if ((CurrentPage.CurrentWebUser != null) 
                    && !string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.PlanFavorites) 
                    && CurrentPage.CurrentWebUser.PlanFavorites.Contains(home.ID.ToString()))
                {
                sb2.AppendFormat("isFav:{0}", "true");
                
                } else {
                sb2.AppendFormat("isFav:{0}", "false");
                }
                sb2.Append("},");
            }
            catch (Exception ex)
                {
                    sb2.Clear();
                    Log.Error(ex.Message, this);
                }
                finally
                {
                    sb.Append(sb2);
                    sb2.Clear();
                }
            }
            sb.Length = sb.Length - 1;
            sb.AppendLine("];");
            return sb.ToString();
        }

        private string SubCommunityName(Item plan)
        {
            string subCommunityID = plan["SubCommunity ID"];
            if (SCID.IsID(subCommunityID))
            {
                var subComm = new ID(subCommunityID).GetItemFromID();
                return subComm["Subcommunity Name"];
            }
            return string.Empty;
        }
	
        private int? SortOrderOfSubCommunity(Item plan)
        {
            if (SCID.IsID(plan["SubCommunity ID"]))
            {
                var subComm = new ID(plan["SubCommunity ID"]).GetItemFromID();
                if (subComm != null)
                {
                  
                    return subComm["__Sortorder"].CastAs<int>(0);
                   
                }

            }

            return null;
                                       
        }
    }
}
