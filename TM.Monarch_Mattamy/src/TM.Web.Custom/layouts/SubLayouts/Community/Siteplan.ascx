﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Siteplan.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.Siteplan" %>
<sc:Placeholder ID="header" Key="CompressedCommunityHeader" runat="server" />

    <div class="plan-middle3" id="sitePlan" runat="server">
        <h1>Site Plan</h1>
        <sc:Text id="scTextDescription" Field="Site Plan Description" runat="server" />
        <sc:Image ID="imgSitePlan" runat="server" Field="Site Plan Image" CssClass="sitePlanImg" />
    </div>
