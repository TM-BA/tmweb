﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Photos.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.Photos" %>
<sc:Placeholder ID="header" Key="CompressedCommunityHeader" runat="server" />
<div class="photos-sdshow communityPhotos">
<h1>Photos</h1>
    <div class="rotatorControl">
        <sc:PlaceHolder  runat="server" ID="photoRotator" Key="photos"/>
        <hr style="clear:both;"/>
    </div>
<asp:Panel ID="mainImage" runat="server">
    <div class="pho-main">
    <h3 id="imgTitle"></h3>
        <img id="mainImage" src="/images/ph-img.jpg" onmouseover="pause()" onmouseout="play()">
    </div>
</asp:Panel>
             
             <script>
             var slidePlay = true;
             function play(){
                slidePlay = true;
                 TM.Rotator.$slidesObj.play();
             }
             function pause(){
                slidePlay = false;
                 TM.Rotator.$slidesObj.pause();
             }
                 var nextImage = 0;

                TM.Rotator.$interval =   setInterval(function(){
                    if(slidePlay)
                    displayThisImage($j(".carousel .slide img")[nextImage]);
                    
                    $j(".slides-next").click();
                 }, <%= rotationDelay %>);

                 function displayThisImage(sender,stop) {
                     $j(".pho-thumb2 li").removeClass("on");
                     $j(sender).parent().addClass("on");
                     var img = document.getElementById("mainImage");
                     img.src = sender.src.substr(0, sender.src.indexOf('?')) + "?w=650&as=0";
                     document.getElementById("imgTitle").innerHTML = sender.alt;
                     nextImage= (parseInt($j(sender).attr("ni"))+1);
                     if(nextImage >= maxImages)
                        nextImage = 0;
                     if(stop) {
                          TM.Rotator.$slidesObj.stop();
                          TM.Rotator.$slidesObj.pause();
                          clearInterval( TM.Rotator.$interval);
                         clearInterval(TM.Rotator.$slidesObj.timeout);
                         slidePlay = false;
                     }
                         
                 }

                 displayThisImage(document.getElementById("firstSlide"));

                 var maxImages = $j(".carousel .slide img").length;

                 if (maxImages == 0) {
                     $j(".rotatorControl").hide();
                 }
             </script>
</div>
