﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityHours.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityHours" %>
<asp:Panel ID="pnlSalesCenterHours" CssClass="ComHours" runat="server">
 <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
{ %> <h2>Sales Center Hours:</h2> <% } 
else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
{  %>  <h2>Sales Centre Hours:</h2> <% } 
else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
{  %>  <h2>Sales Center Hours:</h2> <% } %>

<sc:Text ID="scUseHoursText" Field="Office Hours Text" runat="server" Visible="false" />
<asp:Literal ID="litDetailedHours" runat="server"></asp:Literal>
</asp:Panel>