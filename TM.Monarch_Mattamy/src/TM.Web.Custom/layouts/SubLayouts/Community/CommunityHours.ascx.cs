﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries;
using System.Text;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityHours : CommunityBase
    {

        private Dictionary<string, Item> _hours = new Dictionary<string, Item>();
        private Dictionary<string, Item> Hours
        {
            get
            {
                if (_hours.Count == 0)
                {

                    string query = SCFastQueries.FindItemsMatchingTemplateUnder("/sitecore/Content/Global Data/Shared Field Values/Valid Times", new Sitecore.Data.ID("{123D1034-5B8D-4224-8CFB-2D5A95368166}"));

                    Item[] allHours = CurrentPage.SCContextDB.SelectItems(query);

                    foreach(Item possibleOpen in allHours){
                        _hours[possibleOpen.ID.ToString()] = possibleOpen;
                    }
                }
                return _hours;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlSalesCenterHours.Visible = CurrentPage.ShowCommunityHours;
            if (string.IsNullOrWhiteSpace(SCContextItem["Use Office Hours Text"])) 
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(@"<div class=""><ul class=""saleshrs"">");

                sb.Append("<li><span>Sunday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Sunday Open"], CurrentPage.CurrentCommunity["Sunday Close"])
                    ));
                sb.Append("<li><span>Monday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Monday Open"], CurrentPage.CurrentCommunity["Monday Close"])
                    ));
                sb.Append("<li><span>Tuesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Tuesday Open"], CurrentPage.CurrentCommunity["Tuesday Close"])
                    ));
                sb.Append("<li><span>Wednesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Wednesday Open"], CurrentPage.CurrentCommunity["Wednesday Close"])
                    ));
                sb.Append("<li><span>Thursday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Thursday Open"], CurrentPage.CurrentCommunity["Thursday Close"])
                    ));
                sb.Append("<li><span>Friday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Friday Open"], CurrentPage.CurrentCommunity["Friday Close"])
                    )); 
                sb.Append("<li><span>Saturday:</span><span>{0}</span></li>".FormatWith(
                     getTime(CurrentPage.CurrentCommunity["Saturday Open"], CurrentPage.CurrentCommunity["Saturday Close"])
                     ));

                sb.Append(@"</ul></div>");
                litDetailedHours.Text = sb.ToString();
            }
            else
            {
                scUseHoursText.Visible = true;
                litDetailedHours.Visible = false;
            }

            if (IsPrintMode)
            {
                this.pnlSalesCenterHours.CssClass = "";
            }
        }

        private string getTime(string open, string close){
        string ret;
            if(string.IsNullOrWhiteSpace(open) 
                || string.IsNullOrWhiteSpace(close)
                || (!Hours.ContainsKey(open))
                || (!Hours.ContainsKey(close)))
            {
                ret = "Closed";
            } 
            else
            {
                if (Hours[open].DisplayName == Hours[close].DisplayName)
                {
                    ret = Hours[open].DisplayName;
                }
                else
                {
                    ret = "{0} to {1}".FormatWith(Hours[open].DisplayName, Hours[close].DisplayName);
                }
            }
            return ret;
        }
    }
}