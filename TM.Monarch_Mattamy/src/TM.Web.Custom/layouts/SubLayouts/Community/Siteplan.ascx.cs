﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class Siteplan : CommunityBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Request["sc_device"] == null)
             {
                
                     CurrentPage.Title = "Site plan for " + CurrentPage.PageTitle;
                     var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                     breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Site Plan", CurrentPage.CurrentCommunity));
                 
            }
            else
            {
                if (string.IsNullOrWhiteSpace(SCContextItem["Site Plan Image"]))
                    sitePlan.Visible = false;
            }
        }
    }
}