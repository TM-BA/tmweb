﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Utils.Web;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunitySecondaryNav : CommunityBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if ( ((myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.Closeout) || (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.Open) ) 
                && ( !string.IsNullOrWhiteSpace(SCContextItem["Show Finance Link"]) ))
            {
                litFinanceYourDream.Text = @"<li><a href=""{0}"">Finance Your Dream</a></li>".FormatWith(financeYourDreamsUrl);
            }

            string myConstructionUpdateUrl = ConstructionUpdatesUrl;
            if (!string.IsNullOrWhiteSpace(myConstructionUpdateUrl))
            {
                litConstructionUpdates.Text = @"<li><a href=""{0}"">Contruction Updates</a></li>".FormatWith(ConstructionUpdatesUrl);
            }

            switch (myCommunityStatus)
            {
                case GlobalEnums.CommunityStatus.Open:
                    liHomesForSale.Visible = HomesForSale.Count > 0;
                    divHomesForSale.Visible = liHomesForSale.Visible;
                    liFloorPlans.Visible = true;
                    divFloorPlans.Visible = liFloorPlans.Visible;
                    liSchools.Visible = Schools.Count > 0;
                    divSchools.Visible = liSchools.Visible;
                    liEventDetails.Visible = false;
                    divEventDetails.Visible = liEventDetails.Visible;
                    liPromos.Visible = Promos.Count > 0;
                    divPromos.Visible = liPromos.Visible;
                    break;
                case GlobalEnums.CommunityStatus.ComingSoon:
                case GlobalEnums.CommunityStatus.PreSelling:
                    liHomesForSale.Visible = false;
                    divHomesForSale.Visible = liHomesForSale.Visible;
                    liFloorPlans.Visible = Plans.Count > 0;
                    divFloorPlans.Visible = liFloorPlans.Visible;
                    liSchools.Visible = Schools.Count > 0;
                    divSchools.Visible = liSchools.Visible;
                    liEventDetails.Visible = false;
                    divEventDetails.Visible = liEventDetails.Visible;
                    liPromos.Visible = Promos.Count > 0;
                    divPromos.Visible = liPromos.Visible;
                    break;
                case GlobalEnums.CommunityStatus.GrandOpening:
                    liHomesForSale.Visible = false;
                    divHomesForSale.Visible = liHomesForSale.Visible;
                    liFloorPlans.Visible = true;
                    divFloorPlans.Visible = liFloorPlans.Visible;
                    liSchools.Visible = Schools.Count > 0;
                    divSchools.Visible = liSchools.Visible;
                    liEventDetails.Visible = true;
                    divEventDetails.Visible = liEventDetails.Visible;
                    liPromos.Visible = Promos.Count > 0;
                    divPromos.Visible = liPromos.Visible;
                    break;
                case GlobalEnums.CommunityStatus.Closeout:
                    liHomesForSale.Visible = HomesForSale.Count > 0;
                    divHomesForSale.Visible = liHomesForSale.Visible;
                    liFloorPlans.Visible = Plans.Count > 0;
                    divFloorPlans.Visible = liFloorPlans.Visible;
                    liSchools.Visible = Schools.Count > 0;
                    divSchools.Visible = liSchools.Visible;
                    liEventDetails.Visible = false;
                    divEventDetails.Visible = liEventDetails.Visible;
                    liPromos.Visible = Promos.Count > 0;
                    divPromos.Visible = liPromos.Visible;
                    break;
                case GlobalEnums.CommunityStatus.Closed:
                default:
                    liHomesForSale.Visible = false;
                    divHomesForSale.Visible = liHomesForSale.Visible;
                    liFloorPlans.Visible = true;
                    divFloorPlans.Visible = liFloorPlans.Visible;
                    liSchools.Visible = Schools.Count > 0;
                    divSchools.Visible = liSchools.Visible;
                    liEventDetails.Visible = false;
                    divEventDetails.Visible = liEventDetails.Visible;
                    liPromos.Visible = false;
                    divPromos.Visible = liPromos.Visible;
                    break;
            }

            var tab = Request.Url.LastSegment();

            switch (tab.ToLowerInvariant()) 
            {
                case "homes-ready-now":
                    phHomeForSale.Controls.Add(new Sublayout{Path ="/tmwebcustom/SubLayouts/Community/homesForSale.ascx"});
                    break;
                case "floor-plans":
                    phFloorPlans.Controls.Add(new Sublayout { Path = "/tmwebcustom/SubLayouts/Community/PlanRepeater.ascx" });
		    break;
                case "schools":
		     phSchools.Controls.Add(new Sublayout { Path = "/tmwebcustom/SubLayouts/Community/SchoolRepeater.ascx" });
                    break;
                case "promotions":
                    phPromotions.Controls.Add(new Sublayout() { Path = "/tmwebcustom/SubLayouts/Community/PromotionRepeater.ascx" });
                    break;
                case "event-details":
                    phPromotions.Controls.Add(new Sublayout() { Path = "/tmwebcustom/SubLayouts/Community/EventDetails.ascx" });
                    break;
            }

        }

        protected string CurrentTab
        {
            get { return Request.Url.LastSegment().ToLowerInvariant(); }
        }
    }
}