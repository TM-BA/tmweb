﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="homeFeatures.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.homeFeatures" %>
<sc:Placeholder ID="header" Key="CompressedCommunityHeader" runat="server" />
<div class="dd-bottom">
    <div class="plan-middle3 home-fe">
        <h1>
            Home Features</h1>
        <p>
            <sc:Text runat="server" ID="txtHomeFeaturesIntro" Field="Home Features Intro" />
        </p>
        <asp:Literal runat="server" ID="litContent"></asp:Literal>
    </div>
    <div class="home-feat">
        <asp:Literal ID="litImages" runat="server"></asp:Literal>
    </div>
    <div id="lastUpdated" class="clear">
        Last Updated:
        <%= LastUpdate.ToShortDateString() %>
    </div>
</div>