﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Configuration;
using TM.Web.Custom.Constants;
using SCID = Sitecore.Data.ID;
using System.Text;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class SchoolRepeater : CommunityBase
    {
        public string SchoolRepeaterData;
        private string _schoolReportUrl;
        public string SchoolReportUrl
        {
            get
            {
                if (_schoolReportUrl == null)
                {
                    _schoolReportUrl = SCCurrentHomeItem["School Report URL"];
                }
                return _schoolReportUrl;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] != null)
            {
                litPrintHeader.Text = "<h1>Schools</h1>";

            }
            else
            {
                CurrentPage.Title = "Schools around " + CurrentPage.PageTitle;
            }
            GetValues();
        }

        private SCID currentItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetValues();
            }
        }



        protected void GetValues()
        {
            using (new SecurityDisabler())
            {
                using (new BulkUpdateContext())
                {
                    using (new EventDisabler())
                    {
                        SchoolRepeaterData = AdminMode(Schools);
                    }
                }
            }
        }

        private string AdminMode(IEnumerable<Item> Schools)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("var SchoolInitialData = [");
            foreach (Item school in Schools)
            {
                sb.Append("{");
                if ((Request["sc_device"] == null) && (!string.IsNullOrWhiteSpace(school["School Image"])))
                {
                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)school.Fields["School Image"]);
                    sb.AppendFormat("img: '{0}',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem));
                } else {
                    sb.Append("img: '',");
                }
                Sitecore.Data.Fields.LinkField lnk = school.Fields["School Website URL"];
                string url;
                if(lnk.LinkType == "media"){
                    MediaItem media = new MediaItem(lnk.TargetItem);
                    url = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(media));
                } else {
                    url = lnk.Url;
                }
                sb.AppendFormat("url: '{0}',",url);
                sb.AppendFormat("name: '{0}',",school.DisplayName);
                sb.AppendFormat("phone: '{0}',", school["School Phone"]);
                sb.AppendFormat("grades: '{0}',", school["School Grades"]);
                sb.AppendFormat("prin: '{0}',", school["School Principal"]);
                sb.AppendFormat("dis: '{0}',", school["School Distance"]);
                sb.AppendFormat("nces:'{0}'", school["School Source ID"]);
                sb.Append("},");
            }
            sb.Length = sb.Length-1;
            sb.AppendLine("];");
            return sb.ToString();
        }
    }
}