﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DrivingDirections.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.DrivingDirections" %>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.5&key=<%=GoogleMapsApiKey %>&sensor=false"></script>
<sc:Placeholder ID="header" Key="DrivingDirectionsHeader" runat="server" />
<div runat="server" id="DirectionsContent">
<div class="plan-middle3">
    
    <asp:Literal runat="server" id="DrivingDirectionsHeader" visible="false"></asp:Literal>

    <h1 runat="server" id="DrivingDirectionsTitle">Driving Directions</h1>
    <p>
        <sc:Text ID="scTextDescription" Field="Driving Directions Text" runat="server" />
    </p>
    <div id="startAddress" runat="server">
        <span>Enter your starting address for turn by turn directions:</span>
        <input runat="server" id="FromAddress" type="text" placeholder="e.g. 123 House Drive, City, State, Zip" class="dd-sa" />
        <ul class="dd-but">
            <li><a href="javascript:calcRoute()" onclick="StoreAddress('<%= FromAddress.ClientID %>');TM.Common.GAlogevent('Directions','Click','GetDrivingDirections');">Get Directions</a></li>
        </ul>
    </div>
    <div class="dd-list">
        <div id="directions-panel"></div>
    </div>
    <div id="warnings_panel"></div>
</div>
</div>
<!--end plan-middle3-->
<div class="plan-middle4">
    <ul class="dd-mapbut" id="callsToAction" runat="server">
        <li><a href="#SendToForm" id="btnEmailDir" onclick="TM.Common.GAlogevent('Directions','Click','EmailDrivingDirections')">Email Directions</a></li>
        <li><asp:HyperLink ID="lnkPrintDirections" runat="server" Target="_blank" onclick="TM.Common.GAlogevent('Directions','Click','PrintDrivingDirections')"  rel="nofollow">Print Directions</asp:HyperLink></li>
    </ul>
    	<div style='display:none;'>
            <div id="SendToForm" style='padding:10px; background:#fff;'>
            <p>Please enter your email address:</p>
                <asp:TextBox ID="toAddress" runat="server" style="width:100%" ValidationGroup="sendEmail"></asp:TextBox>
                <asp:RequiredFieldValidator id="isAddressEntered" ControlToValidate="FromAddress" Text="Please tell us where you want directions from on the previous screen." runat="server" ValidationGroup="sendEmail" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator id="isEmailValid" ValidationExpression=".+@.+" ControlToValidate="toAddress" Text="Your email seems odd, please check it?" runat="server" ValidationGroup="sendEmail" Display="Dynamic"></asp:RegularExpressionValidator>
                <ul class="dd-but">
                    <li><asp:LinkButton ID="lnkbEmailDirections" runat="server" CausesValidation="true" ValidationGroup="sendEmail" OnClientClick="if(Page_ClientValidate('sendEmail')){$j('#SendToForm').appendTo($j('form'));}else{return false}" >Send Directions</asp:LinkButton></li>
                </ul>
                <a href="#" onclick="$j.colorbox.close()" style="padding-top:17px;float:right;">Cancel</a>
            </div>
		</div>

    <div class="dd-map">
        <div id="map_canvas" class="mapt">
        </div>
    </div>
    <!--end plan-middle4-->
</div>
<script>
        var mapCanvas;
        var directionDisplay;
        var directionsService;
        var stepDisplay;
        var myCommunity;
        var markerArray = [];

        function initialize() {
            // Instantiate a directions service.
            directionsService = new google.maps.DirectionsService();

            // Create a map and center it on Manhattan.
            myCommunity = new google.maps.LatLng(<%= CommunityLattitude %> , <%=  CommunityLongitude %> );
            var myOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: myCommunity
            }
            mapCanvas = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            //var dirMarkerOptions = {
            //icon : new google.maps.Symbol("CIRCLE")
            //}
            var communityMarker = new google.maps.Marker({
                    position: myCommunity,
                    map: mapCanvas,
                    icon: "/images/icon-house.png"
                });
                markerArray.push(communityMarker);
            // Create a renderer for directions and bind it to the map.
            var rendererOptions = {
                map: mapCanvas
                //,markerOptions : dirMarkerOptions
                //, suppressMarkers: true
            }
            directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)
            directionsDisplay.setPanel(document.getElementById('directions-panel'));
            // Instantiate an info window to hold step text.
            stepDisplay = new google.maps.InfoWindow();
        }

        function calcRoute(startAddress) {
            var start = startAddress;
            // First, remove any existing markers from the map.
            for (i = 0; i < markerArray.length; i++) {
                markerArray[i].setMap(null);
            }

            // Now, clear the array itself.
            markerArray = [];

            // Retrieve the start and end locations and create
            // a DirectionsRequest using WALKING directions.
            if(! start){
                start = document.getElementById("<%= FromAddress.ClientID %>").value;
            }
            var end = "<%= myCommunity %>";
            if (end.length == 0){
                end = myCommunity;
            }
            var request = {
                origin: start,
                destination: end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

            // Route the directions and pass the response to a
            // function to create markers for each step.
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    var warnings = document.getElementById("warnings_panel");
                    warnings.innerHTML = "<b>" + response.routes[0].warnings + "</b>";
                    directionsDisplay.setDirections(response);
                    //showSteps(response);
                }
            });
        }

        function showSteps(directionResult) {
            // For each step, place a marker, and add the text to the marker's
            // info window. Also attach the marker to an array so we
            // can keep track of it and remove it when calculating new
            // routes.
            var myRoute = directionResult.routes[0].legs[0];

            for (var i = 0; i < myRoute.steps.length; i++) {
                //var icon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=" + i + "|222277|ffffff";
                if (i == 0) {
                    icon = "https://chart.googleapis.com/chart?chst=d_map_xpin_icon&chld=pin_star|car-dealer|00FFFF|FF0000";
                    var marker = new google.maps.Marker({
                    position: myRoute.steps[i].start_point,
                    map: mapCanvas,
                    icon: icon
                });
                markerArray.push(marker);
                }
                
                //attachInstructionText(marker, myRoute.steps[i].instructions);
            }
            var marker = new google.maps.Marker({
                position: myRoute.steps[i - 1].end_point,
                map: mapCanvas,
                icon: "https://chart.googleapis.com/chart?chst=d_map_pin_icon&chld=flag|ADDE63"
            });
            markerArray.push(marker);

            //google.maps.event.trigger(markerArray[0], "click");
        }

        function attachInstructionText(marker, text) {
            google.maps.event.addListener(marker, 'click', function () {
                // Open an info window when the marker is clicked on,
                // containing the text of the step.
                stepDisplay.setContent(text);
                stepDisplay.open(mapCanvas, marker);
            });
        }
        function StoreAddress(startAddressField){
            var start = document.getElementById(startAddressField).value;
            var printBtn = document.getElementById("<%=lnkPrintDirections.ClientID %>");
            var indexOfStartAddress = printBtn.href.indexOf("startAddress");
            if(indexOfStartAddress ==-1){
                printBtn.href +=  "&startAddress=" + start;
            } else {
                printBtn.href = printBtn.href.substring(0,indexOfStartAddress) + "startAddress=" + start;
            }
        }
        function reloadMap(){
        initialize();
        calcRoute();
        }
</script>
