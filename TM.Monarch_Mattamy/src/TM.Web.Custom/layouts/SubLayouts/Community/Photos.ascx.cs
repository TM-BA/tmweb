﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts.Common;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class Photos : CommunityBase
    {
        private int delay = 4;
        public int rotationDelay
        {
            get
            {
                return delay * 1000;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] != null)
            {
                mainImage.Visible = false;
            }
            else
            {
                CurrentPage.Title = "View photos of " + CurrentPage.PageTitle;
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Community Photos", CurrentPage.CurrentCommunity));

                if (!string.IsNullOrWhiteSpace(SCContextItem["Slideshow Image Rotation Speed"]))
                {
                    int.TryParse(SCContextItem["Slideshow Image Rotation Speed"], out delay);

                }
            }
        }
    }
}