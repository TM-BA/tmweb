﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Sitecore.Data.Items;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class DrivingDirections : CommunityBase
    {
        public string myCommunity
        {
            get
            {
                if (string.IsNullOrEmpty(CurrentPage.CurrentCommunity["Use Coordinates for Route Generation"]))
                {
                    if (
                        !string.IsNullOrWhiteSpace(Address1) &&
                        (!string.IsNullOrWhiteSpace(CityName) && !string.IsNullOrWhiteSpace(State)
                        || !string.IsNullOrWhiteSpace(Zip)))
                    {

                        return string.Format("{0} {1}, {2}, {3} {4}", Address1, Address2, CityName, State, Zip);
                    }
                }
                return string.Empty;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentPage.Title = "Get directions to" + CurrentPage.PageTitle;
            //We are in printMode
            if (Request["sc_device"] != null)
            {
                if (Request["startAddress"] != null)
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "calcDirections", "$j(document).ready(function(){initialize(); calcRoute(\"" + Request["startAddress"] + "\");});", true);
                }
                callsToAction.Visible = false;
                startAddress.Visible = false;

                DrivingDirectionsHeader.Visible = true;
                StringBuilder header = new StringBuilder();
                AddPrintHeading(header);
                DrivingDirectionsHeader.Text = header.ToString();
                DrivingDirectionsTitle.Visible = false;
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "initMap", "google.maps.event.addDomListener(window, \"load\", initialize);  $j(document).ready(function(){$j(\"#btnEmailDir\").colorbox({inline:true, width:'350px',height:'250px'});});", true);
                    lnkPrintDirections.NavigateUrl = Request.Url.GetLeftPart(UriPartial.Path).Contains('?') ? Request.Url.GetLeftPart(UriPartial.Path) + "&sc_device=print" : Request.Url.GetLeftPart(UriPartial.Path) + "?sc_device=print";
                    if (CurrentPage.CurrentWebUser != null)
                    {
                        //http://davidcel.is/blog/2012/09/06/stop-validating-email-addresses-with-regex/
                        toAddress.Text = CurrentPage.CurrentWebUser.Email;
                        if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.AddressLine1) &&
                            !string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.City) &&
                            (!string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.StateAbbreviation) || !string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.Zipcode))
                            )
                        {
                            FromAddress.Value = "{0} {1}, {2}, {3}, {4}".FormatWith(
                                CurrentPage.CurrentWebUser.AddressLine1,
                                CurrentPage.CurrentWebUser.AddressLine2,
                                CurrentPage.CurrentWebUser.City,
                                CurrentPage.CurrentWebUser.StateAbbreviation,
                                CurrentPage.CurrentWebUser.Zipcode);
                        }
                    }
                }
             
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Driving Directions", CurrentPage.CurrentCommunity));
            }



            lnkbEmailDirections.Click += new EventHandler(lnkbEmailDirections_Click);
        }

        void lnkbEmailDirections_Click(object sender, EventArgs e)
        {
            //string body = DirectionsContent.InnerHtml;
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(@"<html><head></head><body>", Request.Url.Host);

            AddHeading(sb);
            AddSalesCenterAddress(sb);
            AddHours(sb);

            string destCommunity = myCommunity;
            if (destCommunity == string.Empty)
            {
                destCommunity = CommunityLattitude + "," + CommunityLongitude;
            }
            else
            {

            }
            var directionSteps = TM.Utils.Web.GMapUtil.GetDirections(FromAddress.Value, destCommunity);
            foreach (TM.Utils.Web.DirectionSteps leg in directionSteps)
            {
                sb.AppendLine(@"<div class=""origin""><img src=""http://maps.gstatic.com/mapfiles/markers2/icon_greenA.png"" />{0}</div>".FormatWith(leg.OriginAddress));
                sb.AppendLine(@"<div></div>");
                foreach (TM.Utils.Web.DirectionStep step in leg.Steps)
                {
                    sb.AppendLine(@"<div class=""step""><span class=""index"">{0}.</span><span class=""instruction"">{1}</span><span class=""distance"">{2}</span></div>".FormatWith(
                         step.Index
                        , step.Description
                        , step.Distance));
                }
                sb.AppendLine(@"<div class=""origin""><img src=""http://maps.gstatic.com/mapfiles/markers2/icon_greenB.png"" />{0}</div>".FormatWith(leg.DestinationAddress));
            }
            sb.AppendLine(@"<p>Map data ©{0} Google</p>".FormatWith(DateTime.Now.Year));
            sb.AppendLine("</body></html>");
            string subject = "Driving Directions to {0}, {1}".FormatWith(
                CommunityName,
                CurrentPage.CurrentHomeItem.GetItemName()
                );

            EmailHelper.SendEmail(FromEmail, toAddress.Text, subject, sb.ToString(), true, null, null, null, false);
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "initMap", "google.maps.event.addDomListener(window, \"load\", reloadMap);", true);
        }


        private void AddPrintHeading(StringBuilder sb)
        {
            sb.Append(@"<div class=""DrivingDirectionsHeader"">");
            sb.Append(@"<div class=""print-top-dir""><h1>Driving Directions</h1>");
            var logo = CurrentPage.CurrentHomeItem.Fields["Company Logo"];
            sb.Append(@"<img src=""http://{2}{0}"" alt=""{1}"" /></div>".FormatWith(logo.GetMediaUrl(), CurrentPage.CurrentHomeItem["Company Name"], Request.Url.Host));
            sb.Append(@"<div class=""CommunityTitle""><h2>{0}</h2></div><div class=""phone"">{1}</div>".FormatWith(CommunityName, SCContextItem["Phone"]));

            sb.Append("<h1>Sales Center:</h1>");
            sb.Append(@"<div class=""SalesCenter""><span class=""address"">{0}</span><span=""address"">{1}</span><span=""address"">{2}, {3} {4}</span></div>".FormatWith(Address1, Address2, CityName, State, Zip));
            sb.Append(@"</div>");
        }


        

        private void AddHeading(StringBuilder sb) {
            //sb.Append(@"<style type=""stylesheet"">body, h1, li { background-color:red; color:#19398A; }</style>");
            sb.Append(@"<h1 style=""color:#19398A; font-size:14px;"">Driving Directions</h1>");
            var logo =  CurrentPage.CurrentHomeItem.Fields["Company Logo"];
            sb.Append(@"<span class=""logo""><img src=""http://{2}{0}"" alt=""{1}"" /></span>".FormatWith(logo.GetMediaUrl(), CurrentPage.CurrentHomeItem["Company Name"], Request.Url.Host));
            sb.Append(@"<h2 style=""color:#D31245; font-size:14px; margin:0"">{0}</h2><div class=""phone"">{1}</div>".FormatWith(CommunityName,SCContextItem["Phone"]));
        }


        private void AddSalesCenterAddress(StringBuilder sb) {
            sb.Append("<h1>Sales Center:</h1>");
            sb.Append(@"<div class=""SalesCenter""><span class=""address"">{0}</span><span=""address"">{1}</span><span=""address"">{2}, {3} {4}</span></div>".FormatWith(Address1, Address2, CityName, State, Zip));
        }


        private void AddHours(StringBuilder sb){
            sb.Append(@"<div class=""da_ho""><h1>Sales Center Hours</h1>");
            if (string.IsNullOrWhiteSpace(SCContextItem["Use Office Hours Text"]))
            {
                sb.Append(@"<ul class=""saleshrs"">");

                sb.Append("<li><span>Sunday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Sunday Open"], CurrentPage.CurrentCommunity["Sunday Close"])
                    ));
                sb.Append("<li><span>Monday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Monday Open"], CurrentPage.CurrentCommunity["Monday Close"])
                    ));
                sb.Append("<li><span>Tuesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Tuesday Open"], CurrentPage.CurrentCommunity["Tuesday Close"])
                    ));
                sb.Append("<li><span>Wednesday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Wednesday Open"], CurrentPage.CurrentCommunity["Wednesday Close"])
                    ));
                sb.Append("<li><span>Thursday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Thursday Open"], CurrentPage.CurrentCommunity["Thursday Close"])
                    ));
                sb.Append("<li><span>Friday:</span><span>{0}</span></li>".FormatWith(
                    getTime(CurrentPage.CurrentCommunity["Friday Open"], CurrentPage.CurrentCommunity["Friday Close"])
                    ));
                sb.Append("<li><span>Saturday:</span><span>{0}</span></li>".FormatWith(
                     getTime(CurrentPage.CurrentCommunity["Saturday Open"], CurrentPage.CurrentCommunity["Saturday Close"])
                     ));

                sb.Append(@"</ul>");
            }
            else
            {
                sb.Append(CurrentPage.CurrentCommunity["Office Hours Text"]);
            }
            sb.Append("</div>");
        }


        private Dictionary<string, Item> _hours = new Dictionary<string, Item>();
        private Dictionary<string, Item> Hours
        {
            get
            {
                if (_hours.Count == 0)
                {

                    string query = SCFastQueries.FindItemsMatchingTemplateUnder("/sitecore/Content/Global Data/Shared Field Values/Valid Times", new Sitecore.Data.ID("{123D1034-5B8D-4224-8CFB-2D5A95368166}"));

                    Item[] allHours = CurrentPage.SCContextDB.SelectItems(query);

                    foreach (Item possibleOpen in allHours)
                    {
                        _hours[possibleOpen.ID.ToString()] = possibleOpen;
                    }
                }
                return _hours;
            }
        }
        private string getTime(string open, string close)
        {
            string ret;
            if (string.IsNullOrWhiteSpace(open)
                || string.IsNullOrWhiteSpace(close)
                || (!Hours.ContainsKey(open))
                || (!Hours.ContainsKey(close)))
            {
                ret = "Closed";
            }
            else
            {
                if (Hours[open].DisplayName == Hours[close].DisplayName)
                {
                    ret = Hours[open].DisplayName;
                }
                else
                {
                    ret = "{0} to {1}".FormatWith(Hours[open].DisplayName, Hours[close].DisplayName);
                }
            }
            return ret;
        }
    }
}