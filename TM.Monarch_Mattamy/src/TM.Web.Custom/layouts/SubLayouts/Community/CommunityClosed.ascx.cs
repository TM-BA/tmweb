﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;
using SCID = Sitecore.Data.ID;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityClosed : CommunityBase
    {
        public string lastStatusChange
        {
            get
            {
                var extendedStatus = SCContextItem["Extended Community Status Information"];

                DateTime lastChanged = SCUtils.SCDateTimeToMsDateTime(SCContextItem["Community Status Change Date"]);
                return string.IsNullOrWhiteSpace(extendedStatus) ? "Community completed in " + lastChanged.ToString("MMMM yyyy") : extendedStatus;
            }
        }

        private string _litSimilarCommunities = string.Empty;

        public string litSimilarCommunities
        {
            get { return _litSimilarCommunities; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SCContextItem["Community Logo"]))
            {
                communityTitle.Visible = true;
            }
            else
            {
                logo.Visible = true;
            }
            hypAreaCommunities.NavigateUrl = SCUtils.GetItemUrl(CurrentPage.CurrentDivision).Replace(".aspx", string.Empty);
            _litSimilarCommunities = InitializeSimilarCommunities();

            if (IsPrintMode)
            {
                goAllCom.Visible = false;
                Image1.Visible = false;
            }
        }

        private string InitializeSimilarCommunities()
        {
            List<SimilarCommunity> similarCommunities = SCUtils.getSimilarCommunities(SCContextItem, 20, .20f);
            if (similarCommunities.Count > 0)
            {
                bool matchedComs = false;
                StringBuilder sb = new StringBuilder("var similarComInitData = [");
                foreach (SimilarCommunity com in similarCommunities)
                {
                    Item communityItem = SCContextDB.GetItem(new SCID(com.comID));
                    if (communityItem.ID != SCContextItem.ID)
                    {
                        matchedComs = true;
                        sb.Append("{");
                        sb.AppendFormat("name:'{0}',", communityItem.Name);
                        sb.AppendFormat("url:'{0}',", SCUtils.GetItemUrl(communityItem));
                        sb.AppendFormat("pMin:\"{0}\",", string.IsNullOrWhiteSpace(communityItem["Price Override Text"]) ? "From $" + com.priceMin.ToString("#,#") : communityItem["Price Override Text"]);
                        sb.AppendFormat("sMin:'{0}',", com.sqftMin);
                        sb.AppendFormat("sMax:'{0}',", com.sqftMax);
                        sb.AppendFormat("bMin:'{0}',", com.bathMin);
                        sb.AppendFormat("bMax:'{0}',", com.bathMax);
                        sb.AppendFormat("bdMin:'{0}',", com.bedMin);
                        sb.AppendFormat("bdMax:'{0}'", com.bedMax);
                        sb.Append("},");
                    }
                }
                sb.Replace(',', ' ', sb.Length - 1, 1);
                sb.Append("];\n");
                if (matchedComs)
                    return sb.ToString();
            }
            secondaryHeading.InnerText = "GENERAL INFORMATION";
            spanSimilarCommunities.Visible = false;
            return string.Empty;
        }
    }
}