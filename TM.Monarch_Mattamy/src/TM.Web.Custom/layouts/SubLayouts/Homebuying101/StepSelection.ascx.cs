﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using System.Collections.Generic;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Homebuying101
{
    public partial class StepSelection : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateSteps();
        }

        int steps_counter;
        string[] stepsCss = new string[5] { "bubble_1", "bubble_2", "bubble_3", "bubble_4", "bubble_5" };

        /// <summary>
        /// Populate available positions
        /// </summary>
        private void PopulateSteps()
        {
          
            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

            if (contextItem.TemplateID == SCIDs.HomeBuying101.StepDetail)
            {
                contextItem = contextItem.Parent;
            }

            if (contextItem != null)
            {

                
                Sitecore.Collections.ChildList childItems = contextItem.GetChildren();
                List<Item> itemList = childItems.ToList();

                rptSteps.DataSource = itemList.Where(i => i.Fields["Status"].Value.Equals("Active")).Take(5);
                rptSteps.DataBind();
            }
           
        }

        protected void rptSteps_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

                    

                    HyperLink imgField = (HyperLink)e.Item.FindControl("StepImage");
                    if (imgField != null)
                    {
                        imgField.Text = "";
                        imgField.NavigateUrl = SCUtils.GetItemUrl(currentItem);

                        if (contextItem.ID == currentItem.ID)
                        {
                            imgField.CssClass = stepsCss[steps_counter] + " on";
                        }
                        else
                        {
                            imgField.CssClass = stepsCss[steps_counter];
                        }


                        steps_counter += 1;

                        Label stepLabel = new Label();
                        stepLabel.Text = currentItem.Fields["Label"].ToString();

                        imgField.Controls.Add(stepLabel);
                    }


                }
            }
        }

    }

        
}