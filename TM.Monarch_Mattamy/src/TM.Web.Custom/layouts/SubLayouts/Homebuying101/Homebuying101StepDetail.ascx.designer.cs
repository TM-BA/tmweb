﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace TM.Web.Custom.Layouts.SubLayouts.Homebuying101 {
    
    
    public partial class Homebuying101StepDetail {
        
        /// <summary>
        /// Control ContentArea.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder ContentArea;
        
        /// <summary>
        /// Control hlkLogo.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink hlkLogo;
        
        /// <summary>
        /// Control StepsSelection.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder StepsSelection;
        
        /// <summary>
        /// Control Glossary.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder Glossary;
        
        /// <summary>
        /// Control StepDetail.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder StepDetail;
        
        /// <summary>
        /// Control TweetReader.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder TweetReader;
    }
}
