﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Queries;

namespace TM.Web.Custom.Layouts.SubLayouts.Homebuying101
{
    public partial class RequestInformation : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateRequestInformation();
        }

        private void PopulateRequestInformation()
        {
          

            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

            AssociatedDocument.NavigateUrl = contextItem.Fields["AssociatedDocument"].ToString();

            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = contextItem.Fields["Image"];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }

            AssociatedDocument.ImageUrl = imageURL;

        }
    }
}