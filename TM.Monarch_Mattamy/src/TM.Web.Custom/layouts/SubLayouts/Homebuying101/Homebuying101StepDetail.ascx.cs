﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Homebuying101
{
    public partial class Homebuying101StepDetail : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;
            contextItem = contextItem.Parent;

            if(contextItem != null)
            {

                    string imageURL = "";
                    Sitecore.Data.Fields.ImageField imageField = contextItem.Fields["Step Detail Image"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                        imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                    }

                    hlkLogo.ImageUrl = imageURL;
                    hlkLogo.CssClass = "hb-logo";
                    hlkLogo.NavigateUrl = SCUtils.GetItemUrl(contextItem).Replace(SCCurrentHomePath,"");
                
            }

        }

    }
}