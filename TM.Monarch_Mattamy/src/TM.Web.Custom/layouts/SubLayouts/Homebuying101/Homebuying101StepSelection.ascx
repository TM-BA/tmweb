﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Homebuying101StepSelection.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Homebuying101.Homebuying101StepSelection" %>
    <div class="box-container"> 
        <div runat="server" id="logo" class="homebuying-logo-sm">
        <sc:Image ID="bannerImage" Field="Banner Image" runat="server" />
        </div>
        
        <div class="hb101-text">
            <sc:fieldrenderer runat="server" id="ContentArea" fieldname="Content Area"></sc:fieldrenderer>
        </div>
         
        <div class="arrows">
        </div>
                        
        <sc:placeholder runat="server" id="StepsSelection" key="StepsSelection">
    
        </sc:placeholder>
    </div>
    <sc:placeholder runat="server" id="Glossary" key="Glossary">
    
    </sc:placeholder>

    <div class="hb-steps">
    <sc:placeholder runat="server" id="TweetReader" key="TweetReader">
    
    </sc:placeholder>
    </div>
   

