﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeForSaleSearchFacet.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.HomeForSaleSearchFacet" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Register TagPrefix="tm" Namespace="TM.Web.Custom.WebControls" Assembly="TM.Web.Custom" %>

<link href="/Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<%= JsCssCombiner.GetSet("hfsfacetjs")%>
<style>
	.lihdr
	{
		display: none;
	}
	.facetprogress
	{
		top: 23px;
	}
</style>
<script>
	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		$j(window).load(function () { SetSearchFacet(searchpath); });

		var hnddiv = $j("#hnddivision").val();
		var hndrltr = $j("#hndRealtor").val();
		
		$j("#" + hnddiv).change(function () {
			GetLandingPageInfos($j(this).val(), hndrltr == 'true' ? "Realtor Inventory Page Campaign" : "Inventory Landing Campaign");
			GetCitiesByDivision($j(this).val());
		});

		$j("#selectcity").change(function () { RefineInventorySearchResults(); });
		$j("#selectBonusRoomDen").change(function () { RefineInventorySearchResults(); });
		$j("#selectSchoolDistrict").change(function () { RefineInventorySearchResults(); });
		//$j("#selectCommunityStatus").change(function () { RefineInventorySearchResults(); });
		$j("#selectAvailability").change(function () { RefineInventorySearchResults(); });

//		$j("#sldrrange-StartingPrice").draggable();
//		$j("#sldrrange-SquareFeet").draggable();

	});		
</script>
<div id="searchfacet" class="searchfact">
	<div class="facetprogress">
		<div>
			Loading...</div>
	</div>
	<input type="hidden" id="hnddivision" value="<%= ddldivision.ClientID%>" />
	<input type="hidden" id="hdnSelectedDiv" value="<%= SelectedArea %>"/>
	<div id="selectAreaBox">
		<p>
			Select Area:</p>
		<div>
			<tm:ExtendedDropDownList runat="server" ID="ddldivision" />
		</div>
	</div>
	<div id="sdrpriceRange" class="range">
		<p>
			Starting Price:<span id="startingprice"></span>
		</p>
		<div id="sldrrange-StartingPrice">
		</div>
	</div>
	<div id="selectcityBox">
		<p>
			City:</p>
		<div>
			<select id="selectcity">
			</select>
		</div>
	</div>
	<div id="lblBedsBox">
		<p>
			Bed:</p>
		<div>
			<ul id="lblBeds">
			</ul>
		</div>
	</div>
	<div id="lblBathsBox">
		<p>
			Bath:</p>
		<div>
			<ul id="lblBaths">
			</ul>
		</div>
	</div>
	<div id="lblGaragesBox">
		<p>
			Garage:</p>
		<div>
			<ul id="lblGarages">
			</ul>
		</div>
	</div>
	<div id="lblStoriesBox">
		
          <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
          { %> <p>Stories:</p> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
          {  %>  <p>Storeys:</p> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
          {  %>  <p>Stories:</p> <% } %>

		<div>
			<ul id="lblStories">
			</ul>
		</div>
	</div>
	<div id="selectBonusRoomDenBox">
		<p>
			Bonus room/Den:</p>
		<div>
			<select id="selectBonusRoomDen" class="bonden">
			</select></div>
	</div>
	<div id="sdrSqftRange" class="range">
		<p>
			Square Feet:<span id="squarefeet"></span>
		</p>
		<div id="sldrrange-SquareFeet">
		</div>
	</div>
	<div id="selectSchoolDistrictBox">
		<p>
			School District:</p>
		<div>
			<select id="selectSchoolDistrict">
			</select></div>
	</div>
	<div id="selectCommunityStatusBox">
		<p>
			Community Status:</p>
		<div>
			<select id="selectCommunityStatus">
			</select></div>
	</div>
	<div id="selectAvailabilityBox">
		<p>
			Availability:</p>
		<div>
			<select id="selectAvailability">
			</select></div>
	</div>
	<div>
		<a class="facetclear" href="javascript:ClearSearchFacet();">clear selections</a>
	</div>
</div>
