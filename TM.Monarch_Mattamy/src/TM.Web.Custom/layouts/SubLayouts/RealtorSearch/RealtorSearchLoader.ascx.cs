﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch
{
	public partial class RealtorSearchLoader : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var lastSegment = Request.Url.AbsolutePath.GetLastTextfromString('/');

			if (lastSegment.ToLower() != "realtor-search")
			{
				scphContent.Controls.Add(LoadControl("/tmwebcustom/SubLayouts/RealtorSearch/RealtorSearchResults.ascx"));
			}
			else
			{
				scphContent.Controls.Add(LoadControl("/tmwebcustom/SubLayouts/RealtorSearch/RealtorSearch.ascx"));
			}
		}
	}
}