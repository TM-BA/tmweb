﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch {
    
    
    public partial class RealtorSearchResults {
        
        /// <summary>
        /// Control liturl.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal liturl;
        
        /// <summary>
        /// Control ucSearchFact.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.HomeForSaleSearchFacet ucSearchFact;
        
        /// <summary>
        /// Control litLandingPageCampaign.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litLandingPageCampaign;
        
        /// <summary>
        /// Control phSignUpLeadForm.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder phSignUpLeadForm;
        
        /// <summary>
        /// Control LiveChat.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.UserControl LiveChat;
        
        /// <summary>
        /// Control litihccard.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litihccard;
        
        /// <summary>
        /// Control ucHomeForSaleSearchCard.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.HomeForSaleSearchCard ucHomeForSaleSearchCard;
    }
}
