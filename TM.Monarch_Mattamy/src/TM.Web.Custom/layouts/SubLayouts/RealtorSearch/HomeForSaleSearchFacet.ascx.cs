﻿using System;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch
{
    public partial class HomeForSaleSearchFacet : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string mode = SCContextItem.Fields["Mode"].Value;
            liturl.Text = mode;

            if (mode.ToLower() == "home for sale")
            {
                var searchHelper = new SearchHelper();

                var diviItems = searchHelper.GetAllCrossDivisions();
                var isRealtorSearch = (SCContextItem.TemplateID == SCIDs.TemplateIds.RealtorSearch);
                var realtorSearchArea = string.Empty;
                if (isRealtorSearch)
                {
                    realtorSearchArea = CurrentPage.Request.Url.ToString().GetLastTextfromString('-').ToLowerInvariant();
                }

                var currentDomainName = SCCurrentDeviceName.ToLowerInvariant();
                foreach (var item in diviItems)
                {
                    var listItem = new ListItem(item.Name, item.ID.ToString());
                    var itemDomainName = item.Domain.ToLowerInvariant();
                    listItem.Selected = isRealtorSearch & itemDomainName == currentDomainName & item.Name.ToLowerInvariant() == realtorSearchArea;
                    if (listItem.Selected) SelectedArea = listItem.Value;
                    listItem.Attributes.Add("optgroup", item.Domain.GetCurrentCompanyName());
                    ddldivision.Items.Add(listItem);

                }
                if (!isRealtorSearch)
                    ddldivision.Items.Insert(0, new ListItem("No preference", "0"));
            }
        }

        protected string SelectedArea { get; set; }
    }
}