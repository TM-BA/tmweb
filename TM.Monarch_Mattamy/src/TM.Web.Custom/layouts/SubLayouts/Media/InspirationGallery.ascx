﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InspirationGallery.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Media.InspirationGallery" %>
<link rel="stylesheet" type="text/css" href="/Styles/lib/bxslidder.css" />
<script type="text/javascript" src="/javascript/lib/jquery1.9.0.min.js"></script>
<script type="text/javascript" src="/javascript/lib/bxslidder.js"></script>
<style type="text/css">
    .bx-wrapper .bx-caption
    {
        margin-bottom: 10px;
    }
    #bx-pager
    {
        text-align: center;
    }
  
</style>
<div class="lt-col" style="<%= SpecialStyling %>">
    <section class="iss-lt">
        <div class="intimob">Inspiration Gallery</div>
        <div id="arrow1" class="arrow-up" onclick="toggleArrow();"></div>
        <a href="#" class="insh1 inpp" onclick="toggleArrow();"> Narrow your search </a>
		
						<div class="navln"></div>
                        <div class="categories" data-bind="foreach: categories">
   <div class="inscheck inspad">
        <input type="checkbox" data-bind="attr: {value:$data},click: $parent.categoryChanged" /><span
            data-bind="text: $data"></span>
    </div>
</div>
<a href="javascript:void(0);" class="inslink" onclick=" $('input:checkbox').removeAttr('checked'); vm.clean();"> Clear All </a>
</section>
</div>
<div class="rt-col">
    <div class="insgal">
        <div>
            <ul class="bxslider" style="width: auto; position: relative;" data-bind="foreach: selectedElements">
                <li>
                    <img data-bind="attr: {src:imageURL, title:caption}" />
                </li>
            </ul>
            <div class="gba1">
                <div id="bx-pager" class="carouselPager" data-bind="foreach: selectedElements">
                    <a href="javascript:void(0);" data-bind="attr: {onclick:'clicked('+$index()+', true)', class: 'carousel'+$index()}">
                        <img data-bind="attr: {src:imageURL}" width="125" height="75" /></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function toggleArrow()
    {
        $(".categories").slideToggle("slow");
        $("#arrow1").toggleClass("arrow-down");
    }
    
    var Element = function (categories, image, caption) {
        this.categories = categories;
        this.imageURL = image;
        this.caption = caption;
    };
    var ViewModel = function () {
        var self = this;
        self.elements = [];
        self.selectedElements = ko.observableArray([]);
        self.selectedCategories = [];
        self.categories = [];
        self.addElement = function (categories, image, caption) {
            var element = new Element(categories, image, caption);
            self.elements.push(element);
            $.each(categories, function (i, category) {
                self.addCategory(category);
            });
        };
        self.addCategory = function (category) {
            var index = self.categories.indexOf(category);
            if (index == -1) {
                self.categories.push(category);
            }
        };
        self.start = function () {
            $.each(self.elements, function (y, element) {
                
                    self.selectedElements.push(element);
                
            });
        };
        self.clean = function ()
        {
            self.selectedElements.removeAll();
            self.selectedCategories = [];
            self.start();
            slider.reloadSlider();
             carousel.reloadSlider();
              $(".carousel0").addClass("activeC");
        };
        self.categoryChanged = function (category) {

            var index = self.selectedCategories.indexOf(category);
            if (index > -1) {
                self.selectedCategories.splice(index, 1);
            } else {
                self.selectedCategories.push(category);
            }
            self.selectedElements.removeAll();


            if (self.selectedCategories.length == 0) {
                self.start();

            } else {
                $.each(self.selectedCategories, function (i, category) {
                    $.each(self.elements, function (y, element) {
                        if (jQuery.inArray(category, element.categories) > -1 && self.selectedElements.indexOf(element) == -1
                        ) {
                            self.selectedElements.push(element);
                        }

                    });
                });

            }
            slider.reloadSlider();

            carousel.reloadSlider();
             $(".carousel0").addClass("activeC");
            return true;

        };

    };
    var vm = new ViewModel();
    var jsFromServer = <%= JSon %>;
    for(i=0; i<jsFromServer.length; i++) {
        vm.addElement(jsFromServer[i].Categories, jsFromServer[i].URL, jsFromServer[i].Caption);
	 
    }
 

    vm.start();
    ko.applyBindings(vm);
    var slider = $('.bxslider').bxSlider({
        captions: true,
        adaptiveHeight: true,
        pager: false,
        controls:false,
        mode:'fade'      
        
    });
      var carousel = $('#bx-pager').bxSlider({
        slideWidth: 100,
        minSlides: 1,
        maxSlides: 5,
        moveSlides: 1,
        slideMargin: 10,
        pager: false,
        onSlidePrev :  function($slideElement, oldIndex, newIndex){ 
            clicked(newIndex, false);
        },
        onSlideNext : function($slideElement, oldIndex, newIndex){ 
            clicked(newIndex, false);
        }
    });
    $(".carousel0").addClass("activeC");
      function clicked (position, moveCarousel)
    {
        if (moveCarousel)
        {
            carousel.goToSlide(position);
        }
         slider.goToSlide(position);
         $(".carouselPager a").removeClass("activeC");
         $(".carousel"+position).addClass("activeC");
    }

     
    TM.Common.GAlogevent('InspirationGallery', 'Load', '')



</script>
