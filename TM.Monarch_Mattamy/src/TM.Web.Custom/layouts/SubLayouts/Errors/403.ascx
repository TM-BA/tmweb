﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="403.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Errors._403" %>

<%@ Register src="../Common/Sitemap.ascx" tagname="Sitemap" tagprefix="uc1" %>

<div class="plan-details-wrapper">
        
    <div class="gr-bk">
        <p>Our Apologies </p>        
    </div>
        
    <div class="create-acc">
    
    <sc:fieldrenderer runat="server" id="ErrorMessage" fieldname="Error Message"></sc:fieldrenderer>  
    
    <p>You may also search for a home in one of the following areas: </p>

    <sc:placeholder runat="server" id="DivisionListing" key="DivisionListing">    </sc:placeholder>
       
    <div class="error-or">Or select from the following areas of our site: </div>  
    
    <uc1:Sitemap ID="Sitemap1" runat="server" />

    </div><!--end create-acc-->
</div>




