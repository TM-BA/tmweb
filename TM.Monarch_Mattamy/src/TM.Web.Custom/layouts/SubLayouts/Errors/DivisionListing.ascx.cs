﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Errors
{
    public partial class DivisionListing : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateDivision();
        }

        private void PopulateDivision()
        {
            Item[] StatesList = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath,SCIDs.TemplateIds.StatePage));

            rptStates.DataSource = StatesList.OrderBy(x => x.Name);
            rptStates.DataBind();
        
        }

        protected void rptStates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
 
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            Item[] DivisionList = SCContextDB.SelectItems(SCFastQueries.FindItemsMatchingTemplateUnder(currentItem.Paths.FullPath, SCIDs.TemplateIds.DivisionPage));

           
                //Assign the current item to HyperLink control define on repeater
                if (currentItem != null)
                {

                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {

                        Panel pnlState = (Panel)e.Item.FindControl("pnlState");
                        if (DivisionList.Count() > 0)
                        {
                            Label lblState = (Label)e.Item.FindControl("lblState");
                            lblState.Text = currentItem.DisplayName;

                            Repeater repeater = (Repeater)e.Item.FindControl("rptDivisions");
                            repeater.DataSource = DivisionList;
                            repeater.DataBind();

                            pnlState.Visible = true;


                        }
                        else
                        {   
                            pnlState.Visible = false; }

                      

                    }
                }
            
            
        }


        protected void rptDivisions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                        HyperLink relatedLink = (HyperLink)e.Item.FindControl("hlkTitle");
                        relatedLink.NavigateUrl = SCUtils.GetItemUrl(currentItem);
                        if (currentItem.Fields["Division Name"] != null)
                        {

                            string divisionName = currentItem.Fields["Division Name"].Value;
                            int index = 0;

                            if(divisionName != string.Empty)
                            {
                                index = divisionName.ToLower().IndexOf("area");
                            }

                            if (index > 0)
                            {
                                relatedLink.Text = divisionName;
                            }
                            else
                            {
                                relatedLink.Text = divisionName + " Area "; ;
                            }

                            
                        }
                    

                }
            }


        }

    }
}