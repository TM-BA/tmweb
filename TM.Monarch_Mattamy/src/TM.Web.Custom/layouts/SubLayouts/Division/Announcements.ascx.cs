﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using System.Text;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Division
{
	public partial class Announcements : ControlBase
	{
		private string _announcementsValues = string.Empty; 
		private static readonly Database _scContextDB = Sitecore.Context.Database;
		protected void Page_Load(object sender, EventArgs e)
		{
			var currentDomain = SCCurrentDeviceName.ToLower();
            var domains = new List<string> { CommonValues.TaylorMorrisonDomain, CommonValues.MonarchGroupDomain, CommonValues.DarlingHomesDomain };

			//var crossDomains = domains.Where(d => !d.Contains(currentDomain)).OrderByDescending(o => o);
			List<string> crossDomains = new List<string>(); 
			crossDomains.Add(domains.FirstOrDefault(d => d.Contains(currentDomain)));
			crossDomains.AddRange(domains.Where(d => !d.Contains(currentDomain)));

			var isCurrentDomain = false;

			var values = SCContextItem.Fields["Announcements"].Value;
			if (!string.IsNullOrEmpty(values))
			{
				isCurrentDomain = true;
			}

            //Code removed due to ticket 65154, only display announcements that belong to the current domain
		    //string[] valuelist = values.TrimEnd('|').Split('|');
            //if (!string.IsNullOrEmpty(values) || valuelist.Length < 3)			
            //{				
            //    var path = SCContextItem.Paths.FullPath.TrimUrlLastText();
            //    values += "|" + CurrentParentsItem(path);

            //    valuelist = values.TrimEnd('|').Split('|');
            //    if (valuelist.Length < 3)
            //    {
            //        values += "|" + GetOtherCompaniesAnnouncementValues(crossDomains, currentDomain);
            //    }
            //    isCurrentDomain = false;
            //}

            //Only retrieve the announcements from the current domain
		    if (!string.IsNullOrEmpty(values))
		    {
		        values = values.Replace("}{", "}|{").Replace("||", "|");
		        values = !string.IsNullOrWhiteSpace(values) ? values.TrimEnd("|") : string.Empty;
		        var splitList = values.Split('|');

		        var newList = new List<string>(splitList.Distinct());

		        if (newList.Any())
		        {
                    divAnnouncements.Visible = false;
		            GetAnnouncementList(newList, isCurrentDomain);
		        }
		        else
		        {
                    divAnnouncements.Visible = false;
		        }

		    }
		    



		}

		private string GetOtherCompaniesAnnouncementValues(IEnumerable<string> crossDomains, string currentDomain)
		{
			string values = string.Empty;
			string path = string.Empty;
			foreach (var doma in crossDomains)
			{
				path = SCContextItem.Paths.FullPath.ToLower().Replace(currentDomain, doma);
				values += CurrentParentsItem(path);
			}

			return values;
		}

		private string CurrentParentsItem(string path)
		{
			if (path == "sitecore/content") return string.Empty; 
			var item = _scContextDB.GetItem(path);
			if (item != null)
				if (item.TemplateID == SCIDs.TemplateIds.HomeTemplateId)
					return string.Empty;
						
			string values = string.Empty;
			if (item != null)
			{
				values = item["Announcements"];
				_announcementsValues += !string.IsNullOrEmpty(values) ? values + "|" : string.Empty; 
				if (item.TemplateID != SCIDs.TemplateIds.ProductTypePage)
				{
					return CurrentParentsItem(path.TrimUrlLastText());
				}
			}
			else
			{
				return CurrentParentsItem(path.TrimUrlLastText());
			}

			return _announcementsValues; //!string.IsNullOrEmpty(values) ? values + "|" : string.Empty;
		}



		private void GetAnnouncementList(List<string> valuelist, bool isCurrentDomain)
		{
			litAnnouncements.Text = string.Empty;

            if (valuelist.Count > 0) //if (valuelist.Count > 1) Old code, was not displaying announcements with only 1 value, now it will display any announcement on the list
			{				
				var announcementList = valuelist.Select(info => Sitecore.Context.Database.GetItem(info)).ToList();
				litAnnouncements.Text = LoadAnnouncements(announcementList, isCurrentDomain);
                //if(!string.IsNullOrEmpty(litAnnouncements.Text))
                //{divAnnouncements.Visible = true;}
			}
		}

		private string LoadAnnouncements(IEnumerable<Item> info, bool isCurrentDomain)
		{
			var sb = new StringBuilder();

			sb.Append("<h1 class=\"announc-t\">Announcements:</h1>");
			sb.Append("<ul class=\"anno\">");
		    bool hasAnnouncements = false;
            foreach (var item in info.Take(3))
            {
                if (item == null) continue;
                var sdate = DateTime.MinValue;
                var edate = DateTime.MaxValue;

                bool CampaignDisabled = item["Campaign Disabled"].CastAs<bool>(false);

                if (!string.IsNullOrEmpty(item["Campaign start date"]))
                    sdate = Sitecore.DateUtil.IsoDateToDateTime(item["Campaign start date"]);

                if (!string.IsNullOrEmpty(item["Campaign end date"]))
                    edate = Sitecore.DateUtil.IsoDateToDateTime(item["Campaign end date"]);

                var opennewbrowser = item["Open in new browser"];
                var announcement = item["Announcement"];

                string targetUrl = string.Empty;

                var targetUrlItem = item.Fields["Target URL"];
                if (targetUrlItem != null && targetUrlItem.Value.IsNotEmpty())
                    targetUrl = targetUrlItem.GetLinkFieldPath();
                var targetattr = string.Empty;
                
                if (!CampaignDisabled)
                {
                    if (!string.IsNullOrEmpty(opennewbrowser))
                        if (opennewbrowser == "1")
                            targetattr = "target='_blank'";

                    if (!isCurrentDomain)
                        targetattr = "target='_blank'";

                    if (sdate <= DateTime.Now && edate >= DateTime.Now)
                    {
                        hasAnnouncements = true;
                        sb.AppendFormat("<li><a onclick=\"TM.Common.GAlogevent('Announcements', 'Click', 'Announcements', '');\" href=\"{0}\" {1}>{2}</a></li>", targetUrl, targetattr, announcement);
                    }
                }
            }
			sb.Append("</ul>");
		    if (hasAnnouncements)
		        divAnnouncements.Visible = true;
		    else
		        divAnnouncements.Visible = false;

		    return sb.ToString();
		}


	}

}