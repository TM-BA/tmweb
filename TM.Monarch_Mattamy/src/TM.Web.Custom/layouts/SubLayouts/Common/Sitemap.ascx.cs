﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Links;
using System.Text;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    

    public partial class Sitemap : ControlBase 
    {
        NavigationItem navItem;
        List<NavigationItem> itemList;
        List<NavigationItem> itemListRt;
        int Level = 0;
        string orientationNode = "L";
        bool firstIteration = true;
        private bool hideHeader = false;
        private bool skipNode = false;
        string liStyle = "color: #0095D9;font-size: 13px;text-decoration: underline;list-style: square inside none;margin: 0 0 0 15px;";

        public bool HideHeader
        {
            get { return hideHeader; }
            set { hideHeader = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            

            RenderSitemap();

          
        }

        private void RenderSitemap()
        {
            Item contextItem = Sitecore.Context.Database.GetItem(SCCurrentHomePath);

            itemList = new List<NavigationItem>();

            itemListRt = new List<NavigationItem>();

            Traverse(contextItem, (Level + 1));

         
            rptLeftSection.DataSource = itemList;
            rptLeftSection.DataBind();

           
            rptRightSection.DataSource = itemListRt;
            rptRightSection.DataBind();
        }

   
        private void Traverse(Item Item, int Level)
        {
            Sitecore.Collections.ChildList letterNode;
           
                
                if (Item.Fields["Include In Sitemap"] != null)
                    if (Item.Fields["Include In Sitemap"].Value == "1")
                    {
                        // Set root label
                        if (Level == 1)
                        {
                            hlk_Header.Text = Item.Fields["Company Name"].Value + " Home Page";
                            hlk_Header.NavigateUrl = SCUtils.GetItemUrl(Item).Replace(SCCurrentHomePath, "");


                        }else if(Level > 1)
                        {

                            if (firstIteration == false)
                                if (Level == 2)
                                {

                                    if (orientationNode == "L")
                                    { orientationNode = "R"; }
                                    else if (orientationNode == "R")
                                    { orientationNode = "L"; }

                                }


                            navItem = new NavigationItem();
                            navItem.Level = Level;
                            navItem.Name = Item.DisplayName;

                            if (Item.TemplateID.ToString() != SCIDs.TemplateIds.ContentFolders.ToString())
                            {
                                navItem.Url = SCUtils.GetItemUrl(Item).Replace(SCCurrentHomePath, "");
                            }

                            //if (Level == 3)
                            //{
                            //    navItem.CssClass = "sitemap_parent";
                            //}
                            //else if (Level == 4)
                            //{
                            //    navItem.CssClass = "sitemap_child";
                            //}


                            


                            navItem.CssClass = liStyle +  "padding-left:" + ((Level + 1) * 10).ToString() + "px;";

                            if (orientationNode == "L")
                            {
                                itemList.Add(navItem);
                            }
                            else if (orientationNode == "R")
                            {
                                itemListRt.Add(navItem);
                            }

                            firstIteration = false;

                            if (Level == 2)
                            { navItem.SetUL = true; }

                        }

                        letterNode = Item.GetChildren();

                        foreach (Item child in letterNode)
                        {
                            Traverse(child, (Level + 1));

                        }
                    }
                    

        }
    }
}