﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Sitecore;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class Accordion : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BuildNavLinks();
        }

        private void BuildNavLinks()
        {
            navBar.Text = "<div class=\"container\">";
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 1 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-1\">" + this.SCCurrentHomeItem["Section 1 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 2 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-2\">" + this.SCCurrentHomeItem["Section 2 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 3 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-3\">" + this.SCCurrentHomeItem["Section 3 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 4 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-4\">" + this.SCCurrentHomeItem["Section 4 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 5 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-5\">" + this.SCCurrentHomeItem["Section 5 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            if (!string.IsNullOrWhiteSpace(this.SCCurrentHomeItem["Section 6 Title"]))
            {
                navBar.Text += "<div class=\"child\"><a href=\"#tabs-6\">" + this.SCCurrentHomeItem["Section 6 Title"] + "</a><span class=\"arrow\"></span></div>";
            }
            navBar.Text += "</div>";
        }
    }
}