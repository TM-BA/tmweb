﻿using System;
using System.Linq;
using System.Text;
using Sitecore.Data.Items;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts.Community;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class SEODynamicTree : CommunityBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SCContextItem == null) return;
            litseodyntree.Text = SetSeoContent();
            /*try
            {
                litseodyntree.Text = SetSeoContent();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error when generating DynamicSEO links for: " + SCContextItem.Paths.FullPath, ex);
            }*/
        }

        private string SetSeoContent()
        {
            #region Home Node

            // Removed per ticket 63621
            //if (SCContextItem.TemplateID == SCIDs.TemplateIds.HomeTemplateId)
            //{
            //    var leadInText = "Find {ProductTypeList} by {CompanyName} <br/><br/> " +
            //                     "{Promotions}";

            //    var productsQuery = SCFastQueries.FindItemsMatchingTemplateUnder(SCCurrentHomePath,
            //                                                                     SCIDs.TemplateIds.ProductTypePage);
            //    var products = SCContextDB.SelectItems(productsQuery);
            //    var productTypeList = products.OrderBy(p => p.DisplayName)
            //        .Select(p => "<a href=\"{0}\" onlick=\"TM.Common.GAlogevent('Search', 'Click', 'FooterSearch')\" class=\"\">{1}</a>"
            //                         .FormatWith(p.GetItemUrl(), p.DisplayName))
            //        .ToArray();

            //    var productTypeListStr = string.Join(" or ", productTypeList);

            //    string companyName;
            //    switch (SCCurrentDeviceType.ToString().ToLower())
            //    {
            //        case "monarchgroup":
            //             companyName = "Monarch Group";
            //             break;
            //        case "darlinghomes":
            //             companyName =  "Darling Homes";
            //             break;
            //        default:
            //             companyName = "Taylor Morrison";
            //             break;
            //    }

            //    var promoLeadInText = string.Format("Learn more about special incentives and promotions available from{0}: ", companyName);
            //    string promotions = GetAllPromotionsLinksUnder(SCCurrentHomePath, promoLeadInText );

            //    var homePageSEOContent = new
            //                                 {
            //                                     ProductTypeList = productTypeListStr,
            //                                     CompanyName = companyName,
            //                                     Promotions = promotions
            //                                 };

            //    return leadInText.NamedFormat(homePageSEOContent);
            //}

            #endregion Home Node

            #region State Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.StatePage)
            {
                var leadInText = "Find {ProductType} in one of the following areas in {StateName}:<br/>" +
                                 "{DivisionList}<br/><br/>" +
                                 "{Promotions}";

                var state = SCContextItem;
                var product = SCContextItem.Parent;
                var activeDivisionIDs = new DivisionQueries().GetAllActiveDivisionUnder(state.ID);

                var activeDivisionItems = activeDivisionIDs.Select(a => SCContextDB.GetItem(a));

                var activeDivisions = activeDivisionItems.OrderBy(d => d.GetItemName()).Select(div => "<a href=\"{0}\" class=\"footer\">{1}</a>".FormatWith(div.GetItemUrl(), div.GetItemName())).ToArray();

                var activeDivisionsStr = string.Empty;
                activeDivisions.Each(division => activeDivisionsStr += "<span class=\"footerLinks\">{0}</span>".FormatWith(division));

                var promoLeadInText = string.Format("Learn more about special incentives and promotions available in {0}:", SCContextItem.GetItemName());
                var promotions = GetAllPromotionsLinksUnder(SCCurrentItemPath, promoLeadInText);
                var statePageSEOContent = new
                                              {
                                                  ProductType = product.Name,
                                                  StateName = SCContextItem.Name,
                                                  DivisionList = activeDivisionsStr,
                                                  Promotions = promotions
                                              };
                return leadInText.NamedFormat(statePageSEOContent);
            }

            #endregion State Node

            #region Division Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.DivisionPage)
            {
                var division = SCContextItem;
                var product = division.Parent;
                var activeCitiesIDs = new CityQueries().GetAllActiveCitiesUnderDivision(division.ID);

                if (activeCitiesIDs != null)
                {
                    var leadInText = "Find {ProductType} in one of the following cities in {DivisionName} area:<br/>" +
                                    "{CityList}<br/><br/>{Promotions}";

                    var activeCityItems = activeCitiesIDs.Select(a => SCContextDB.GetItem(a));
                    var activeCities = activeCityItems.OrderBy(d => d.GetItemName()).Select(div => "<a href=\"{0}\" onclick=\"TM.Common.GAlogevent('Search', 'Click', 'FooterSearch', '');\" class=\"footer\">{1}</a>".FormatWith(div.GetItemUrl(), div.GetItemName())).ToArray();

                    var activeCitiesStr = string.Empty;
                    activeCities.Each(city => activeCitiesStr += "<span class=\"footerLinks\">{0}</span>".FormatWith(city));
                    var divisionName = division.GetItemName();
                    var promoLeadInText = "Learn more about special incentives and promotions available in " + divisionName + " area:";
                    var promotions = GetAllPromotionsLinksUnder(SCCurrentItemPath, promoLeadInText);
                    var statePageSEOContent = new
                    {
                        ProductType = product.GetItemName(),
                        DivisionName = divisionName,
                        CityList = activeCitiesStr,
                        Promotions = promotions
                    };

                    return leadInText.NamedFormat(statePageSEOContent);
                }
            }

            #endregion Division Node

            #region City Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.CityPage)
            {
                var city = SCContextItem;
                var cityName = city.Name;
                var stateAbbrev = CurrentPage.CurrentState.GetItemName(true);
                var skinnyCommunities = new CommunityQueries().GetAllActiveCommunitesUnderCity(city.ID);

                if (skinnyCommunities != null)
                {
                    if (skinnyCommunities.Any())
                    {
                        var leadInText = "Select from one of the following communities in {City}, {StateAbbrev}:<br/>" +
                                           "{CommunityList}<br/><br/>" +
                                           "{Promotions}";
                        var communities = skinnyCommunities.Select(s => s.GetItem());
                        var activeComms = communities.OrderBy(d => d.GetItemName()).Select(div => "<a href=\"{0}\" onclick=\"TM.Common.GAlogevent('Search', 'Click', 'FooterSearch', '');\" class=\"footer\">{1}</a>".FormatWith(div.GetItemUrl(), div.GetItemName())).ToArray();

                        var activeCommStr = string.Empty;
                        activeComms.Each(com => activeCommStr += "<span class=\"footerLinks\">{0}</span>".FormatWith(com));

                        var promoLeadInText =
                            "Find out more about special incentives and promotions in {0}, {1}: ".FormatWith(
                                cityName, stateAbbrev);
                        var promotions = GetAllPromotionsLinksUnder(SCCurrentItemPath, promoLeadInText);

                        var citySEOContent = new
                                                 {
                                                     City = cityName,
                                                     StateAbbrev = stateAbbrev,
                                                     CommunityList = activeCommStr,
                                                     Promotions = promotions
                                                 };

                        return
                            leadInText.NamedFormat(citySEOContent);
                    }
                }
            }

            #endregion City Node

            #region Community Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
            {
                var leadInText = "Learn more about {CommunityName} in {City}, {StateAbbrev} {Zip}:<br/>" +
                                 "{CommunityMenuList}<br/><br/>" +
                                 "{Promotions}" +
                                 "{FloorPlans}";
                string leadInString = "Available Promotions:";
                var promotions = GetAllPromotionsLinksUnder(SCCurrentItemPath, leadInString);
                StringBuilder planText = new StringBuilder();
                if (Plans.Any())
                {
                    planText.Append("Available floor plans: <div class=\"fpList\">");
                    foreach (Item plan in Plans)
                    {
                        planText.AppendFormat("<span class=\"footerLinks\"><a href=\"{0}\" class=\"footer\">{1}</a></span>", plan.GetItemUrl(), plan.GetItemName());
                    }
                    planText.Append("</div>");
                }
                var communityPageSEOContent = new
                {
                    CommunityName = CurrentPage.CurrentCommunity.GetItemName(),
                    ProductType = CurrentPage.CurrentProduct.GetItemName(),
                    DivisionName = CurrentPage.CurrentDivision.GetItemName(),
                    City = CurrentPage.CurrentCity.GetItemName(),
                    StateAbbrev = CurrentPage.CurrentState.GetItemName(true),
                    Promotions = promotions,
                    Zip = Zip,
                    CommunityMenuList = "<ul>{0}</ul>".FormatWith(ComInfoNavUL),
                    FloorPlans = planText.ToString()
                };
                return leadInText.NamedFormat(communityPageSEOContent);
            }

            #endregion Community Node

            #region Plan Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage)
            {
                var leadInText = "Explore the {PlanName} floor plan with {Bed} bedrooms and {Bath} bathrooms priced from {Price} in the {CommunityName} community in {City}, {StateAbbr} {Zip}";
                var inventoryText = string.Empty;
                var similarCommunityText = string.Empty;
                var plan = SCContextItem;
                var inventoryItems = new PlanQueries().GetActiveHomesForSaleUnderPlan(plan.ID).Select(i => i.GetItem());

                var masterPlanID = SCContextItem.Fields[SCIDs.PlanFields.MasterPlanID].Value;
                var plansAlsoAvailableCommunityIDs = new CommunityQueries().GetCommunitiesMatchingMasterPlanInDivision(CurrentPage.CurrentDivision.ID, masterPlanID);

                var similarCommunities =from itemid in plansAlsoAvailableCommunityIDs
				        where itemid != CurrentCommunity.ID
                                        select SCUtils.GetItem(itemid);


// ReSharper disable PossibleMultipleEnumeration
// multiple enumeration of ienumerable is okay here. No unrepeatability in the enumerations
                if (inventoryItems.Any() || similarCommunities.Any())

                {
                    leadInText += ":<br/>PlanMenu<br/><br/>";
                    if (inventoryItems.Any())
                    {
                        leadInText += "Buy this home now at:<br/>{Inventory}<br/><br/>";
                        foreach (var hfs in inventoryItems)
                        {
                            inventoryText += string.Format("<span class=\"footerPlanLinks\"><a href=\"{0}\">{1}</a></span>", hfs.GetItemUrl(), hfs.DisplayName);
                        }
                    }
                    if (similarCommunities.Any())
                    {
                        leadInText += "This floor plan is also available nearby at the following communities:{SimilarCommunities}";
                        foreach (var simc in similarCommunities)
                        {
                            similarCommunityText += string.Format("<span class=\"footerPlanLinks\"><a href=\"{0}\">{1}</a></span>", simc.GetItemUrl(), simc.DisplayName);
                        }
                    }
                }
                var homeAttributes = new HomeAttributes().GetHomeAttributes();
                var price = homeAttributes.PriceText.IsNotEmpty()
                                ? homeAttributes.PriceText
                                : homeAttributes.Price.ToString("C0");
                var planPageSEOContent = new
                                             {
                                                 PlanName = SCContextItem[SCIDs.PlanFields.PlanName],
                                                 Bed = homeAttributes.Bedrooms,
                                                 Bath = homeAttributes.BathCountForDisplay,
                                                 Price = price,

                                                 CommunityName = CurrentPage.CurrentCommunity.GetItemName(),
                                                 City = CurrentPage.CurrentCity.GetItemName(),
                                                 StateAbbr = CurrentPage.CurrentState.GetItemName(true),
                                                 Zip,

                                                 Inventory = inventoryText,
                                                 SimilarCommunities = similarCommunityText
                                             };

                return leadInText.NamedFormat(planPageSEOContent);
            }

            #endregion Plan Node

            #region Home For Sale Node

            if (SCContextItem.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
            {
                var leadInText = "Find out more about the {PlanName} floor plan available now at {StreetAddress}, {City}, {StateAbbr} {Zip}";
            }

            #endregion Home For Sale Node

            return String.Empty;
        }

        private string GetAllPromotionsLinksUnder(string itemPath, string leadIn)
        {
            var allPromotionsQuery = SCFastQueries.FindAllActivePromotionsUnder(itemPath);

            var promotionItems = SCContextDB.SelectItems(allPromotionsQuery);
            if (promotionItems.Length > 0)
            {
                var promotions = new StringBuilder();
                promotions.AppendFormat("{0}<ul>".FormatWith(leadIn));
                foreach (var promotion in promotionItems)
                {
                    promotions.AppendFormat("<li><a href=\"{0}\" onlick=\"TM.Common.GAlogevent('Search', 'Click', 'FooterSearch')\" class=\"footer\">{1}</a></li>", promotion.GetItemUrl(),
                                         promotion["Title"]);
                }
                promotions.AppendFormat("</ul>");

                return promotions.ToString();
            }
            return string.Empty;
        }
    }
}
