﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Sitecore;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class ContentAreaTwoCollumn : ControlBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlContentWrapper.CssClass = TM.Web.Custom.SCHelpers.SCUtils.GetSCParameter("WrapperClass", ((Sitecore.Web.UI.WebControls.Sublayout)Parent)).CastAs<string>("content-pg");
        }
    }
}