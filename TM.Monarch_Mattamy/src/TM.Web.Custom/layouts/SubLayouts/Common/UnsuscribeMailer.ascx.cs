﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Resources.Media;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class UnsuscribeMailer : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;
            
            PopulateCampaigns();
        }

        private void PopulateCampaigns()
        {

            Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;


            // Populate primary campaign image 

            string primaryCampaignImageURL = string.Empty;
            bool primaryCampaignStatus = false;
            bool primaryCampaignExpirationStatus = false;
            string primaryCampaignTargetURL = "";

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryCampaignStatus].Value))
                if (currentItem.Fields[SCIDs.Navigation.primaryCampaignStatus].ToString() == "Active")
                {
                    primaryCampaignStatus = true;
                }

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryEndDate].Value))
            {
                primaryCampaignExpirationStatus = isExpired(currentItem.Fields[SCIDs.Navigation.primaryEndDate].Value);

            }


            if (primaryCampaignStatus == true && primaryCampaignExpirationStatus == false)
            {

                Sitecore.Data.Fields.ImageField imageField1 = currentItem.Fields[SCIDs.Navigation.primaryContent];
                if (imageField1 != null && imageField1.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField1.MediaItem);
                    primaryCampaignImageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                }

                if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryTargeURL].Value))
                {
                    primaryCampaignTargetURL = currentItem.Fields[SCIDs.Navigation.primaryTargeURL].Value;
                    primaryCampaignTargetURL = GetItemTargetURL(primaryCampaignTargetURL);
                }

                campaignImage1.PostBackUrl = primaryCampaignTargetURL.Replace(SCCurrentHomePath, "");
                campaignImage1.ImageUrl = primaryCampaignImageURL;
                campaignImage1.Visible = true;
            }
            else
            {
                campaignImage1.Visible = false;
            }



            // Populate seconday campaign image 

            string secondaryCampaignImageURL = string.Empty;
            bool secondaryCampaignStatus = false;
            bool secondaryCampaignExpirationStatus = false;
            string secondaryCampaignTargetURL = "";

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.secondaryCampaignStatus].Value))
                if (currentItem.Fields[SCIDs.Navigation.secondaryCampaignStatus].ToString() == "Active")
                {
                    secondaryCampaignStatus = true;
                }

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.secondaryEndDate].Value))
            {
                secondaryCampaignExpirationStatus = isExpired(currentItem.Fields[SCIDs.Navigation.secondaryEndDate].Value);
            }


            if (secondaryCampaignStatus == true && secondaryCampaignExpirationStatus == false)
            {

                Sitecore.Data.Fields.ImageField imageField2 = currentItem.Fields[SCIDs.Navigation.secondaryContent];
                if (imageField2 != null && imageField2.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField2.MediaItem);
                    secondaryCampaignImageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                }

                if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryTargeURL].Value))
                {
                    secondaryCampaignTargetURL = currentItem.Fields[SCIDs.Navigation.secondaryTargeURL].Value;
                    secondaryCampaignTargetURL = GetItemTargetURL(secondaryCampaignTargetURL);
                }

                campaignImage2.PostBackUrl = secondaryCampaignTargetURL.Replace(SCCurrentHomePath, "");
                campaignImage2.ImageUrl = secondaryCampaignImageURL;

                campaignImage2.Visible = true;
            }
            else
            {
                campaignImage2.Visible = false;
            }

        }


        public bool isExpired(string date)
        {
            DateTime endDate;
            bool expired = false;


            endDate = SCUtils.SCDateTimeToMsDateTime(date);

            if (endDate != DateTime.MinValue)
            {
                if (endDate <= DateTime.Now)
                {
                    expired = true;
                }

            }


            return expired;

        }

        private string GetItemTargetURL(string targetURL)
        {
            string res = targetURL;

            if (targetURL.Contains("sitecore") || !targetURL.Contains("http"))
            {
                var itemTarget = SCExtensions.GetItemFromPathNoFormat(targetURL);
                if (itemTarget != null)
                {
                    if (itemTarget.Paths.IsMediaItem)
                    {
                        res = MediaManager.GetMediaUrl(itemTarget);
                    }
                    else
                    {
                        res = SCUtils.GetItemUrl(itemTarget);
                    }
                }
            }

            return res;
        }
    }
}