﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Sitecore;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class Blogteaser : ControlBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var currentItem = base.SCContextItem;
            Sitecore.Data.Fields.LinkField blogRssUrlFld = currentItem.Fields[SCFieldNames.BlogRssURL];

            string blogRssURL = blogRssUrlFld.Url;

            try
            {
                var massagedData = WebCache.Get(CacheKeys.GetBlogCacheKey(SCContextSite.Name), ()=>GetRssData(blogRssURL));
                rptBlog.DataSource = massagedData;
                rptBlog.DataBind();
            }
            catch (Exception)
            {

                rptBlog.Visible = false;

            }
          
        }


        private static IEnumerable<RssData> GetRssData(string blogRssURL)
        {
            var xDoc = XDocument.Load(blogRssURL);

            var top2Items = xDoc.Descendants("item").Take(2);

            var massagedData = top2Items.Select(x => new RssData
                                                         {
                                                             Title = x.Descendants("title").Single().Value,
                                                             Description =
                                                                 x.Descendants("description").Single().Value.StripHtml()
                                                                     .Left(50) +
                                                                 "...",
                                                             Month =
                                                                 DateTime.Parse(x.Descendants("pubDate").Single().Value)
                                                                 .ToString("MMM"),
                                                             Date =
                                                                 DateTime.Parse(x.Descendants("pubDate").Single().Value)
                                                                 .Day.ToString(
                                                                     CultureInfo.InvariantCulture),
                                                             Link = x.Descendants("link").Single().Value
                                                         });
            return massagedData;
        }
    }

     
        [Serializable]
        public class RssData
        {
            public string Title { get; set; }
            public string Description { get; set; }
            public string Month { get; set; }
            public string Date { get; set; }
            public string Link { get; set; }
        }
}
