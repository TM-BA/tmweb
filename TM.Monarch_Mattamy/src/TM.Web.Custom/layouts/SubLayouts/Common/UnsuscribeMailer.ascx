﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnsuscribeMailer.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.UnsuscribeMailer" %>
<style>
    /*opt out page*/
    .bn-optout img {height: auto; max-width: 100%; width: 100%;}
    .tx-optout {float:left; width:60%;}
    .tx-optout h1 {font-weight:bold;}
    .form-optout {float:left; width:70%;}
    .op-sidebn {float:right; width:22%;}
    .op-sidebn img {  height: auto; max-width: 100%; width: 100%; padding:3px 0;}

    /*opt out page*/
    .tx-optout {float:left; width:100%;}
    .op-sidebn {float:right;}
    .op-sidebn img {  height: auto; max-width: 60%;}
    .form-optout {float:left;}
    
</style>
<div class="plan-details-wrapper">

    <div class="bn-optout">         
        <sc:Image runat="server" ID="scImageBannerHeader" Field="Banner Header Image" />
    </div>

    <div class="tx-optout">
        <div class="form-optout">
            <sc:placeholder runat="server" id="ContentArea" key="ContentArea"></sc:placeholder>
            <!--add form here--> 
            <sc:fieldrenderer runat="server" id="jsOptOut" fieldname="Company Specific Javascript"></sc:fieldrenderer>
        </div>
        <div class="op-sidebn">
                <asp:ImageButton ID="campaignImage1" runat="server" OnClientClick="window.document.forms[0].target='_blank';"   />
                <asp:ImageButton ID="campaignImage2" runat="server" OnClientClick="window.document.forms[0].target='_blank';"  /> 
            </div> 
    </div>
    
    

    
       
</div>

