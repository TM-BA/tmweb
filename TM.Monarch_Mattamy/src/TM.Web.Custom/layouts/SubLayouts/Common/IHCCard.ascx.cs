﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Sitecore.Data.Items;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
	public partial class IHCCard : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SCContextItem != null)
			{
				var searchHelper = new SearchHelper();
                var ihcItem = CurrentPage.CurrentIHC;
				if (ihcItem != null)
					litihccard.Text = searchHelper.GetIHCCardDetails(ihcItem);
			}
			else 
			litihccard.Text =string.Empty;
		}
	}
}