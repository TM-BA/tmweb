﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Glossary.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Glossary" %>


<asp:HiddenField ID="Terms" runat="server" />

<script type="text/javascript" >

//    $(document).ready(function () {

//        var availableTags = $("#<%= Terms.ClientID %>").val();
//        availableTags = availableTags.split(',');

//        alert("#<%= Terms.ClientID %>");

//        $("#<%= Terms.ClientID %>").autocomplete({
//        source: availableTags
//        });
//    });
  </script>  
    <div class="glos-index"> 
                	<span> Look-up a term: </span>
                      
                    <div class="glos-search">
					    <asp:TextBox ID="txtLookup" runat="server"></asp:TextBox>
                        <asp:ImageButton ID="btnSearch" runat="server" onclick="btnSearch_Click" 
                            ImageUrl="/Images/glos-search.jpg" Height="21px" Width="21px"  />
				    </div>
                    <span> GLOSSARY: </span>    

    <div class="glrymenu">
                 <ul>
                        <asp:Repeater ID="rptGlossary" runat="server">
            <ItemTemplate>

                <li>
                 <a href='<%# Eval("Url") %>' onclick="TM.Common.GAlogevent('HomebuyingGlossary', 'Click', '<%# Eval("Name") %>');"><%# Eval("Name") %></a>
                </li>
                
            </ItemTemplate>
            </asp:Repeater>   
                          
                 </ul>
          </div><!--end glrymenu-->
       </div>
