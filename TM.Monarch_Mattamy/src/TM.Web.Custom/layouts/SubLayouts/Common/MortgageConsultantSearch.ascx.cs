﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.LuceneQueries;
using SCID = Sitecore.Data.ID;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;
using TM.Domain;
using TM.Domain.Entities;


namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class MortgageConsultantSearch : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Clear();
            
            if (!Page.IsPostBack)
            {
                PopulateDivisions();
                SetLoanApplication();
            }
        }

        #region Financing Options


        private void Clear()
        {
            hmc.Text = "";
            errorMessageArea.Text = "";
            errorMessageCommunity.Text = "";
        }

      

        private void SetLoanApplication()
        {

            Sitecore.Data.Items.Item contextItem = SCContextDB.GetItem(SCCurrentHomePath);

            if (!string.IsNullOrEmpty(contextItem.Fields["Financing Call to Action"].Value))
            if (contextItem.Fields["Financing Call to Action"].Value != string.Empty)
            {
                hlkStarLoanApp.NavigateUrl = contextItem.Fields["Financing Call to Action"].Value;
            }
        }

        private void PopulateDivisions()
        {

            
            var query = SCFastQueries.AllActiveDivisionsUnder(SCCurrentHomePath);

            var divisions = SCContextDB.SelectItems(query);

            
            
            List<ListItem> items = new List<ListItem>();
            foreach (var division in divisions)
            {
                var divisionIdFld = division.ID.ToString();
                var divisionNameFld = division.Fields[SCIDs.DivisionFieldIDs.Divisionname];
                if (divisionIdFld != null && divisionIdFld.IsNotEmpty())
                {

                    items.Add(new ListItem(divisionNameFld.Value + ", " + division.Parent.GetItemName(true), divisionIdFld));
                }

            }

            items = (items.OrderBy(x => x.Text.Split(',')[1].TrimEnd()).ThenBy(y => y.Text.Split(',')[0])).ToList();
            foreach (var item in items)
            {
                ddlArea.Items.Add(item);
            }

            ddlArea.Items.Insert(0, new ListItem("Please select an area", "0"));
            ddlComunity.Items.Insert(0, new ListItem("Please select an area first", "0"));


            if (!Page.IsPostBack)
            {
                ddlArea.SelectedIndex = 0;
               
            }

            

        }

        private void PopulateCommunities(string divisionId)
        {

            Item currentDivision = Sitecore.Context.Database.GetItem(divisionId);

            Item[] communities = new CommunityQueries().GetAllActiveCommunitesUnderCity(currentDivision.ID).Select(i => i.GetItem()).ToArray();
               

            ddlComunity.DataSource = "";
            ddlComunity.DataBind();


            foreach (Item community in communities.OrderBy(x => x.Fields["Community Name"].Value))
            {
                var communityIdFld = community.ID.ToString();
                var communityNameFld = community.Fields[SCIDs.CommunityFields.CommunityName];
                if (communityIdFld != null && communityIdFld.IsNotEmpty())
                {
                    ddlComunity.Items.Add(new ListItem(communityNameFld.Value, communityIdFld));
                }

            }

            ddlComunity.Items.Insert(0, new ListItem("Please select an area first", "0"));

        }

        protected void ddlComunity_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlComunity.SelectedIndex == 0)
            {
                rptContactInformation.DataSource = "";
                rptContactInformation.DataBind();
                hlkStarLoanApp.Visible = false;
               
            }
        }


        protected void rptContactInformation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (currentItem != null)
                {

                    Label name = (Label)e.Item.FindControl("name");
                    string mloName = "";
                    if (currentItem.Fields["First Name"].Value != string.Empty && currentItem.Fields["Last Name"].Value != string.Empty)
                    {
                        mloName = " " + currentItem.Fields["First Name"].Value + " " + currentItem.Fields["Last Name"].Value;
                    }

                    if (currentItem.Fields["Regulating Authority Designation"].Value != string.Empty)
                    {
                        mloName = mloName + " (" + currentItem.Fields["Regulating Authority Designation"].Value + ") ";
                    }

                    name.Text = mloName;

                    Label phone = (Label)e.Item.FindControl("phone");

                    if (currentItem.Fields["Phone"].Value != string.Empty)
                    {
                        phone.Text = " " + currentItem.Fields["Phone"].Value;
                    }


                    Label mobile = (Label)e.Item.FindControl("mobile");

                    if (currentItem.Fields["Mobile"].Value != string.Empty)
                    {
                        mobile.Text = " " + currentItem.Fields["Mobile"].Value;
                    }


                    Label fax = (Label)e.Item.FindControl("fax");

                    if (currentItem.Fields["Fax"].Value != string.Empty)
                    {
                        fax.Text = " " + currentItem.Fields["Fax"].Value;
                    }


                    HyperLink email = (HyperLink)e.Item.FindControl("email");

                    if (currentItem.Fields["Email Address"].Value != string.Empty)
                    {
                        email.Text = " " + currentItem.Fields["Email Address"].Value;
                        email.NavigateUrl = "mailto:" + currentItem.Fields["Email Address"].Value;
                    }
                }

            }

        }



        protected void ddlArea_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void btnContactInformation_Click(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex > 0 && ddlComunity.SelectedIndex > 0)
            {
                PopulateContactInfo();
              
            }
            else {
                if (ddlArea.SelectedIndex == 0)
                {
                    errorMessageArea.Text = "Please select an area first";
                }
                if (ddlComunity.SelectedIndex == 0)
                {
                    errorMessageCommunity.Text = "Please select a community first";
                }
            }
            
            
            
        }

        private void PopulateContactInfo()
        {


            string communityId;
            Item currentCommunity;
            string mlo = "";


            if (ddlComunity.Items.Count > 0 && ddlComunity.SelectedIndex > 0)
            {

                communityId = ddlComunity.SelectedItem.Value;

                currentCommunity = Sitecore.Context.Database.GetItem(communityId);

                mlo = currentCommunity.Fields["MLO Card"].Value;
            }

            //Populate from division
            if (string.IsNullOrEmpty(mlo))
            {
                string divisionId = ddlArea.SelectedItem.Value;

                Item currentDivision = Sitecore.Context.Database.GetItem(divisionId);

                mlo = currentDivision.Fields["MLO Card"].Value;
            }
           
 
            string contacts = "";

            List<Item> mloList = new List<Item>();

            
            foreach (string mloContact in mlo.Split('|').ToList())
            {
                Item item = Sitecore.Context.Database.GetItem(mloContact);

                if (item != null)
                {
                   
                    mloList.Add(item);
                }
            }

            mloList = mloList.OrderBy(x => x.Fields["Last Name"].Value).ThenBy(n => n.Fields["First Name"].Value).ToList();

            foreach (Item item in mloList)
            {
                if (item.Fields["First Name"].Value != string.Empty && item.Fields["Last Name"].Value != string.Empty)
                {
                    contacts += (item.Fields["First Name"].Value + " " + item.Fields["Last Name"].Value + ", ");
                }
            }

            if (mloList.Count > 0)
            {
                if (mloList.Count > 1)
                {

                    hmc.Text = @"Your dedicated home mortgage consultant are: <br/>" + (contacts.Trim().TrimEnd(',') + ".");
                }
                else if(mloList.Count == 1)
                {

                    hmc.Text = @"Your dedicated home mortgage consultant is: <br/>" + (contacts.Trim().TrimEnd(',') + ".");
                }

                rptContactInformation.DataSource = mloList.OrderBy(x => x.Fields["Last Name"].Value).ThenBy(n => n.Fields["First Name"].Value);
                rptContactInformation.DataBind();
                rptContactInformation.Visible = true;
                hlkStarLoanApp.Visible = true;

            }
            else
            {
               hmc.Text = "Sorry, we could not find a mortgage consultant available for this area.";
              rptContactInformation.Visible = false;
              hlkStarLoanApp.Visible = false;
              
            }

        }

        #endregion

        protected void btnStarLoanApp_Click(object sender, EventArgs e)
        {

           
        }

        protected void lnkStarLoanApp_Click(object sender, EventArgs e)
        {
           
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex > 0)
            {
                string divisionId = ddlArea.SelectedItem.Value;

                PopulateCommunities(divisionId);
            }
            else
            {
                ddlComunity.Items.Clear();
                ddlComunity.Items.Insert(0, new ListItem("Please select an area first", "0"));
                rptContactInformation.DataSource = "";
                rptContactInformation.DataBind();
                hlkStarLoanApp.Visible = false;
                
            }
        }

       
    }
}
