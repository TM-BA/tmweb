﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class SiteSearchResults : ControlBase
    {
        protected string FullTextQuery;
        protected int Showing;
        protected int TotalResults;
        private string _ctxDB;
        private ISearchIndex _index;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FullTextQuery = Request.QueryString["q"];
                if(FullTextQuery.IsNotEmpty())
                {
                    List<Item> searchResults = new CompanyQueries().Search(FullTextQuery, out TotalResults,
                        SCCurrentHomeItem.ID);
                    Showing = searchResults.Count;

                    List<Item> results = searchResults;

                    if (!string.IsNullOrEmpty(Request.QueryString["ifl"]))
                    {
                        Item firstItem = results.FirstOrDefault();
                        Response.Redirect(firstItem.GetItemUrl());
                    }
                    else
                    {
                        RenderItemDetails(results);
                    }
                }
            }
        }


        protected virtual void RenderItemDetails(List<Item> items)
        {
            //lazy to create an object for this. will use keyvaluepair to hold this info
            var sr = new List<KeyValuePair<string, string>>();
            foreach (Item item in items.Where(item => item != null))
            {
                string header = "<span class='title'><a href='{0}'>{1}</a></span>".FormatWith(item.GetItemUrl(),
                    item.DisplayName);
                string desc = GetDescription(item);
                if (!string.IsNullOrEmpty(desc))
                {
                    desc =
                        "<div class='desc'>{0}</div>".FormatWith(desc.Substring(0, Math.Min(255, desc.Length)) + "...");
                }

                sr.Add(new KeyValuePair<string, string>(header, desc));
            }

            rptSearchResults.DataSource = sr;
            rptSearchResults.DataBind();
        }

        private string GetDescription(Item item)
        {
            if (item.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
                return item.Fields[SCIDs.PlanBaseFields.PlanDescription].GetValue(string.Empty);
            if (item.TemplateID == SCIDs.TemplateIds.PlanPage)
                return item.Fields[SCIDs.PlanBaseFields.PlanDescription].GetValue(string.Empty);
            if (item.TemplateID == SCIDs.TemplateIds.CommunityPage)
                return item.Fields[SCIDs.CommunityFields.CommunityDescription].GetValue(string.Empty);
            return string.Empty;
        }
    }
}