﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
	public partial class Favorites : ControlBase
	{

		protected void Page_Load(object sender, EventArgs e)
		{
			Uri uri = HttpContext.Current.Request.Url;
			string qryString = uri.Query.Remove(0, 1);

			//TODO: have to parse json object
			//qryString = qryString.ToJSON();
			//var myFavorites = qryString.FromJSON(); 

			string[] values = qryString.Split('&');
			var myFavorites = new MyFavorites()
				{
					Id = new ID(values[0].Split('=')[1]),
					Type = (GlobalEnums.FavoritesType)Enum.Parse(typeof(GlobalEnums.FavoritesType), values[1].Split('=')[1]),
					Action = (GlobalEnums.FavoritesAction)Enum.Parse(typeof(GlobalEnums.FavoritesAction), values[2].Split('=')[1])
				};

			switch (myFavorites.Action)
			{
				case GlobalEnums.FavoritesAction.Add:
					AddToFavorite(myFavorites);
					break;
				case GlobalEnums.FavoritesAction.Remove:
					RemoveFromFavorite(myFavorites);
					break;
			}
		}

		public void AddToFavorite(MyFavorites myFavorites)
		{
			var tmUser = CurrentWebUser;
			if (tmUser != null)
			{
				var upUser = new TMUser(TMUserDomain.TaylorMorrison, tmUser.UserName)
					{
						CommunityFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.Community) ? tmUser.CommunityFavorites + "|" + myFavorites.Id : tmUser.CommunityFavorites,
						PlanFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.Plan) ? tmUser.PlanFavorites + "|" + myFavorites.Id : tmUser.PlanFavorites,
						HomeforSaleFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.HomeForsale) ? tmUser.HomeforSaleFavorites + "|" + myFavorites.Id : tmUser.HomeforSaleFavorites
					};
				upUser.Update();
				CurrentWebUser = upUser;
			}  
		}

		public void RemoveFromFavorite(MyFavorites myFavorites)
		{
			var tmUser = CurrentWebUser;
			if (tmUser != null)
			{
				var upUser = new TMUser(TMUserDomain.TaylorMorrison, tmUser.UserName)
				{
					CommunityFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.Community) ? tmUser.CommunityFavorites.Replace("|" + myFavorites.Id, "") : tmUser.CommunityFavorites,
					PlanFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.Plan) ? tmUser.PlanFavorites.Replace("|" + myFavorites.Id, "") : tmUser.PlanFavorites,
					HomeforSaleFavorites = (myFavorites.Type == GlobalEnums.FavoritesType.HomeForsale) ? tmUser.HomeforSaleFavorites.Replace("|" + myFavorites.Id, "") : tmUser.HomeforSaleFavorites
				};
				upUser.Update();
				CurrentWebUser = upUser;
			}
		}
	}
}