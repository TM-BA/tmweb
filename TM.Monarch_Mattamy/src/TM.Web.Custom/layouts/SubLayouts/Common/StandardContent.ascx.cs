﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class StandardContent : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;

            if (currentItem.Fields["Show In Menu"].Value != "1")
            {
                pnl_Navigation.Visible = false;
                pnl_ContentArea.Visible = true;
                return;
            }
            else
            {
                pnl_Navigation.Visible = true;
                pnl_ContentArea.Visible = false;
            
            }
        }
    }
}