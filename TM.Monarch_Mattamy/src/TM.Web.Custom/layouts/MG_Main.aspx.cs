﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Resources.Media;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts
{
	public partial class MG_Main : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        protected void Page_Init(object sender, EventArgs e)
        {
            var webform = (SitecoreSimpleForm)wfmJoinYourInterestList.FormInstance;
            webform.FailedSubmit += new EventHandler<EventArgs>(webform_FailedSubmit);

        }

        void webform_FailedSubmit(object sender, EventArgs e)
        {
            JoinYourInterestFormPosted = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                if (PostedWFFMForm == SCIDs.WebForms.JoinOurInterestList.ToString())
                    JoinYourInterestFormPosted = true;
            }
            
            Page.Title = PageTitle;
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountMG) ?
                                            GoogleAnalytics.MonarchGroupAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountMG;

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            if(companyLogo!=null  && companyLogo.MediaItem!=null)
            {
               imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
               imgSiteLogo.AlternateText = companyLogo.Alt;
            }

            var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

            if (myTMLogo != null && myTMLogo.HasValue)
            {
                imgMyCompanyLogo.ImageUrl = myTMLogo.GetMediaUrl();

            }

            var contextItem = SCContextItem;
            if (contextItem.Fields["Tracking Code"] != null)
            {
                yahooPixels.Text = contextItem.Fields["Tracking Code"].Value;
            }
            else { yahooPixels.Visible = false; }

            LinkField youtubelink = CurrentHomeItem.Fields["YouTube URL"];

			litUserAuthentication.Text = WebUserExtensions.GetCredentialText(CurrentWebUser);
			//if url is new homes - redirect to home page
			var item = SCContextDB.GetItem(SCContextItem.Paths.FullPath);
			if (item.TemplateID == SCIDs.TemplateIds.ProductTypePage)
				Response.Redirect("/");
        }

	    protected bool JoinYourInterestFormPosted { get; set; }
    }
}