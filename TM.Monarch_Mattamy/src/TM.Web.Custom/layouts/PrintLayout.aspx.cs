﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts
{
    public partial class PrintLayout : BasePage
    {
        public string GoogleAnalyticsAccountNumber;

        public string PrintColors { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = PageTitle;

            //Gets the current Domain and depending on which domain we will get the default Google Analytics account number.

            var site = Sitecore.Context.GetSiteName().ToLower();

            switch (site)
            {
                case CommonValues.TaylorMorrisonDomain:
                    GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountTM) ?
                                            GoogleAnalytics.TaylorMorrisonAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountTM;
                    break;

                case CommonValues.DarlingHomesDomain:
                    GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountDH) ?
                                            GoogleAnalytics.DarlingHomesAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountDH;
                    break;

                case CommonValues.MonarchGroupDomain:
                    GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountMG) ?
                                            GoogleAnalytics.MonarchGroupAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountMG;
                    break;

                default:
                    GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountTM) ?
                                            GoogleAnalytics.TaylorMorrisonAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountTM;
                    break;
            }

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            //if(companyLogo!=null  && companyLogo.MediaItem!=null)
            //{
            //   imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
            //   imgSiteLogo.AlternateText = companyLogo.Alt;
            //}

            LinkField youtubelink = CurrentHomeItem.Fields["YouTube URL"];
            PrintColors = "<style>" + CurrentHomeItem["Site Color Theme"] + "</style>";

            ShowCommunityHours = true;
        }

        protected bool IsPlanorHFSPage = Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.PlanPage ||
                                         Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.HomeForSalePage;
    }
}