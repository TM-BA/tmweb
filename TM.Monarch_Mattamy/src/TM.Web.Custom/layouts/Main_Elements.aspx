﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main_Elements.aspx.cs" Inherits="TM.Web.Custom.Layouts.Main_Elements" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core" TagPrefix="wfm" %>

<%@ Register TagPrefix="uc" TagName="MainNav" Src="~/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/USCanadaSVGMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="LeftPromo" Src="~/tmwebcustom/SubLayouts/Common/HeaderSections/HomePageLeftPromo.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head>
	<meta charset="utf-8" />
	<title>
	Environment,  energy, efficiency - Elements by Taylor Morrison
    </title>
	<link rel="stylesheet" type="text/css" href="/Styles/tm_style.css" />
	<meta name="viewport" content="width=device-width">
	<sc:Placeholder ID="phHeadSection" runat="server" Key="HeadSection" />
	<link rel="shortcut icon" type="image/x-icon" href="/Images/tm/favicon.ico">
	<!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->
	<style>
		/*join interest list*/
		.join-wrapper
		{
			background: none repeat scroll 0 0 #FFFFFF;
			border: 1px solid #CECECE;
			display: none;
			height: 335px;
			padding: 20px;
			position: absolute;
			width:335px;
			z-index: 9999;
			top: 29px;
		}
		.join-wrapper h1
		{
			font-size: 19px;
			color: #d61042;
			line-height: 21px;
			margin: 0 0 12px;
		}
		.scfSingleLineTextBorder
		{
			margin: 0 0 7px;
		}
		.scfDropList
		{
			width: 169px;
			background-color: #DFDED8;
			margin: 0 0 7px;
			border: 1px solid #b6b5ac;
		}
		.jil
		{
			font-size: 10px;
			line-height: 12px;
		}
		.jil a
		{
			font-size: 10px;
			color: #2da5e0;
			text-decoration: underline;
			line-height: 12px;
		}
		.cl-bt
		{
			background-color: #acb5bb;
			width: 15px;
			height: 15px;
			float: right;
		}
		.cl-bt a
		{
			color: #fff;
			text-decoration: none;
			margin: 0 0 0 4px;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">

	    if (typeof jQuery == 'undefined') {
	        document.write(unescape("%3Cscript src='/Javascript/lib/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
	    }

	    // BEGIN Google Analiytics Tracking code
	    var _gaq = _gaq || [];
	    _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
		_gaq.push(['_setLocalRemoteServerMode']);
	    _gaq.push(['_trackPageview']);

	    (function () {
	        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	    })();
	    //END Google Analiytics Tracking code

	    var $j = jQuery.noConflict();

	    $j(document).ready(function () {
	        $j("li#findyournewhome").mouseover(function () {
	            $j('#svgmap').show();
	        }).mouseout(function () {
	            $j('#svgmap').hide();
	        });
	        
		$j("a#lijoininterest").click(function () {
				$j('#joinyourinterest').toggle();
			});
			$j("a#joininterest").click(function () {
				$j('#joinyourinterest').hide();
});
		      <%= JoinYourInterestFormPosted?@"$j('#joinyourinterest').show();":string.Empty %>
	    });


	</script>
	 <sc:VisitorIdentification runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <header>
		<div class="header">
			<div class="topheader">
				<ul>
					<sc:Sublayout runat="server" ID="subnowtrending" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx">
					</sc:Sublayout>
					<li class="top3"><a id="lijoininterest" href="javascript:;">JOIN OUR INTEREST LIST</a>
						<div id="joinyourinterest" class="join-wrapper">
						<wfm:FormRender runat="server" ID="wfmJoinYourInterestList" FormID="{E059951C-1C5A-464E-A876-4C06FFD10F26}"/>
					</div>
					</li>
					<li class="top4"><a href="#">
						<sc:Image runat="server" ID="scMyTmImg" Field="My Logo" />
					</a></li>
					<li class="top5"><a href="#">Create Account</a>|<a href="#">Login</a></li>
				</ul>
			</div>
		</div>
		<div class="hdline">
		</div>
		<div class="logomenu">
			<div class="lt-box">
				<a href="/" class="logo">
					<asp:Image runat="server" ID="imgSiteLogo" /></a>
			</div>
			<div class="rt-box">
				<nav>
					<ul>
						<li id="findyournewhome"><a href="/new-homes"><span>FIND</span><span>YOUR NEW HOME</span></a>
							<uc:SVGMap ID="svgMap" runat="server" /></li>
						<sc:Sublayout ID="Sublayout1" runat="server" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" />
					</ul>
					<div class="clearfix">
					</div>
				</nav>
			</div>
			<div class="breadcrumb">
				
				 
			</div>
		</div>		    	 
		<div class="clearfix">
		</div>
	</header>
    <div class="home-content">
		<article>
			<div class="home-content-wrapper">
			    <sc:Placeholder runat="server" ID="phContent" Key="Content" />
				<div class="clearfix">
				</div>
			</div>
		</article>
	</div>
    <footer>
		<article>
		    <sc:Sublayout ID="footer" runat="server" Path="/tmwebcustom/sublayouts/common/footer.ascx"/>
			<sc:Placeholder ID="phFooter" runat="server" Key="FootSection" />
		</article>
	</footer>
    </form>
</body>
</html>
