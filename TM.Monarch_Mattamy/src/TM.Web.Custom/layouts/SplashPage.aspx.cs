﻿using System;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Resources.Media;
using TM.Domain.Enums;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;
using Sitecore.Form.Core.Renderings;
namespace TM.Web.Custom.Layouts
{
    public partial class SplashPage : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        public string AdditionalJavaScripts;
        


        protected void Page_Init(object sender, EventArgs e)
        {

            var webform = (SitecoreSimpleForm)wfmJoinYourInterestList.FormInstance;
            webform.FailedSubmit += new EventHandler<EventArgs>(webform_FailedSubmit);

            Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;

            if (contextItem.Fields["Show Header"] != null)
            {
                var showHeader = contextItem.Fields["Show Header"].Value;
                if (String.IsNullOrEmpty(showHeader))
                {
                    this.headerSection.Visible = false;
                }
            }
            else
            {
                this.headerSection.Visible = false;
            }


            if (contextItem.Fields["Show Footer"] != null)
            {
                var showFooter = contextItem.Fields["Show Footer"].Value;
                if (String.IsNullOrEmpty(showFooter))
                {
                    this.footerSection.Visible = false;
                }
            }
            else
            {
                this.footerSection.Visible = false;
            }

            if (contextItem.Fields["Additional JavaScripts"] != null)
            {
                AdditionalJavaScripts = contextItem.Fields["Additional JavaScripts"].Value;
            }

            if (contextItem.Fields["StyleSheet File"] != null && contextItem["StyleSheet File"] != "")
            {
                var styleItemId = contextItem["StyleSheet File"];

                var item = SCContextDB.GetItem(styleItemId);

                var url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item);
                litStyleSheet.Text = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl(url) + "\" />";
                

            }
            
        }

        void webform_FailedSubmit(object sender, EventArgs e)
        {
            JoinYourInterestFormPosted = true;
        }

      

        protected void Page_Load(object sender, EventArgs e)
        {

            if (SCCurrentDeviceType == DeviceType.TaylorMorrison)
            {
                this.styleTM.Visible = true;
                this.tm.Attributes.Add("class", "tm");
            }
            else if (SCCurrentDeviceType == DeviceType.DarlingHomes)
            {
                this.styleDH.Visible = true;
                this.tm.Attributes.Add("class", "dh");
            }
            else if (SCCurrentDeviceType == DeviceType.MonarchGroup)
            {
                this.styleMG.Visible = true;
                this.tm.Attributes.Add("class", "mg");
            }


            if(Page.IsPostBack)
          {
             
            
              //todo: there are better ways to do this, by intercepting wffm pipeline
              if (PostedWFFMForm == SCIDs.WebForms.JoinOurInterestList.ToString())
                  JoinYourInterestFormPosted = true;
          }
      


            Page.Title = PageTitle;
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountTM) ?
                                            GoogleAnalytics.TaylorMorrisonAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountTM;

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            if(companyLogo!=null  && companyLogo.MediaItem!=null)
            {
               imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
               imgSiteLogo.AlternateText = companyLogo.Alt;
            }

            var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

	    if(myTMLogo!=null && myTMLogo.HasValue)
	    {
	        imgMyCompanyLogo.ImageUrl = myTMLogo.GetMediaUrl();
	       
	    }

        Sitecore.Data.Items.Item contextItem = Sitecore.Context.Item;
        if (contextItem.Fields["Tracking Code"] != null)
        {
            yahooPixels.Text = contextItem.Fields["Tracking Code"].Value;
        }
        else { yahooPixels.Visible = false; }
        
        litUserAuthentication.Text = WebUserExtensions.GetCredentialText(CurrentWebUser);

        //if url is new homes - redirect to home page
        if (SCContextItem.TemplateID == SCIDs.TemplateIds.ProductTypePage)
            Response.Redirect("/");
        }

      

        protected bool JoinYourInterestFormPosted { get; set; }
    }
}
