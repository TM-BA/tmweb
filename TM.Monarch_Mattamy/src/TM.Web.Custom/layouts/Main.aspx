﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="TM.Web.Custom.Layouts.Main" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Import Namespace="TM.Web.Custom.Constants.GoogleEventing" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core"
	TagPrefix="wfm" %>
<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/USCanadaSVGMap.ascx" %>
<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head runat="server">

	<meta charset="utf-8" />
	<title>Taylor Morrison Homepage</title>
	<link rel="stylesheet" type="text/css" href="/Styles/tm_style.css" media="handheld,screen" />
	<link rel="stylesheet" type="text/css" href="/Styles/print.css" media="print" />
	<link href="/Styles/lib/colorbox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/Styles/animate-custom.css" />
	<meta name="viewport" content="width=device-width">
	
	<sc:Placeholder ID="phHeadSection" runat="server" Key="HeadSection" />
	<!--[if gte IE 9]>
	  <style type="text/css">
  	    .gradient {
  	        filter: none;
  	    }
  	</style>
	<![endif]-->
	<%= JsCssCombiner.GetSet("mainjs") %>
	  
	<style>
		/*join interest list*/
		.join-wrapper
		{
			background: none repeat scroll 0 0 #FFFFFF;
			border: 1px solid #CECECE;
			display: none;
			height: auto;
			padding: 20px;
			position: absolute;
			width:335px;
			z-index: 9999;
			top: 29px;
			left: 610px;
		}
		
@-moz-document url-prefix() {       
.join-wrapper {left:583px;}
}
		
		
		.join-wrapper h1
		{
			font-size: 19px;
			color: #d61042;
			line-height: 21px;
			margin: 0 0 12px;
		}
		.scfSingleLineTextBorder
		{
			margin: 0 0 7px;
		}
		.scfDropList
		{
			width: 169px;
			background-color: #DFDED8;
			margin: 0 0 7px;
			border: 1px solid #b6b5ac;
		}
		.jil
		{
			font-size: 10px;
			line-height: 12px;
		}
		.jil a
		{
			font-size: 10px;
			color: #2da5e0;
			text-decoration: underline;
			line-height: 12px;
		}
		.cl-bt
		{
			background-color: #acb5bb;
			width: 15px;
			height: 15px;
			float: right;
		}
		.cl-bt a
		{
			color: #fff;
			text-decoration: none;
			margin: 0 0 0 4px;
		}
	</style>
</head>

<body>
 
	<%--<a href="<%= PrintFriendlyUrl %>" class="printLink">Oops, this page has not been formated
		for your printer, click here to print that instead.</a> <span id="slideImgExpander">
		</span>--%>
	<form id="form1" runat="server">
    
	<header>
		<div class="header">
			<div class="topheader">
				<ul>
					<sc:Sublayout runat="server" ID="subnowtrending" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx">
					</sc:Sublayout>

					<li class="top4"><a href="/Account/My-Profile" onclick="TM.Common.GAlogevent('CreateAccount','Click','CreateAccountLink')">
						<asp:Image runat="server" ID="imgMyCompanyLogo" />
					</a></li>
					<li class="top5" id="logintext">
						<asp:Literal runat="server" ID="litUserAuthentication"></asp:Literal></li>
                    <li class="top3"><a id="lijoininterest" href="javascript:;" onclick="TM.Common.GAlogevent('JoinInterestList','Click','InHeader')">JOIN OUR INTEREST LIST</a>
						<div id="joinyourinterest" class="join-wrapper">
							<wfm:FormRender runat="server" ID="wfmJoinYourInterestList" FormID="{E059951C-1C5A-464E-A876-4C06FFD10F26}" />
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="hdline">
		</div>
		<div class="logomenu">
			<div class="lt-box">
				<a href="/" class="logo">
					<asp:Image runat="server" ID="imgSiteLogo" /></a>
				<div class="searchbox">
					<input type="text" placeholder="Search Site, Enter myTM# or MLS#" id="siteSearchText" /><a
						href="#" onclick="sitesearch('#siteSearchText')"></a>
				</div>
			</div>
			<div class="rt-box">
				<div onclick='$j(".rt-box nav ul").toggle()' class="MobileMenuLink">
					Menu</div>
				<nav id="navtop-main">
					<ul>
						<li id="findyournewhome"><a href="#" onclick="TM.Common.GAlogevent('Search','Click','InHeaderFindYourNewHomes')"><span>FIND</span><span>YOUR HOME</span></a></li>
						<uc:SVGMap ID="svgMap" runat="server" />
						<sc:Sublayout ID="Sublayout1" runat="server" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" />
					</ul>
					<div class="clearfix">
					</div>
				</nav>
			</div>
			<div class="breadcrumb">
				<tm:Breadcrumb ID="bcBreadcrumb" runat="server" DividerText=">" />
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="clearfix">
		</div>
	</header>
	<div class="home-content">
		<article>
			<div class="home-content-wrapper">
				<sc:Placeholder runat="server" ID="phContent" Key="Content" />
				<div class="clearfix">
				</div>
			</div>
		</article>
	</div>
	<footer>
		<article>
			<sc:Sublayout ID="footer" runat="server" Path="/tmwebcustom/sublayouts/common/footer.ascx" />
			<sc:Placeholder ID="phFooter" runat="server" Key="FootSection" />
		</article>
	</footer>
   
	</form>
	<script type="text/javascript">
		$j(document).ready(function () {
			$j("li#findyournewhome").click(function () {
				$j("li#findyournewhome").last().addClass("selected");
				$j('#svgmap').show();				 
			});
			$j("a#svgclose").click(function () {
				$j('#svgmap').hide();
				$j("li#findyournewhome").removeClass("selected");
			});

			$j("a#lijoininterest").click(function () {
			    
				$j('#joinyourinterest').toggle();
			   <%=HomePageEvents.InHeader %>
			});
		    
		    <%= JoinYourInterestFormPosted?@"$j('#joinyourinterest').show();":string.Empty %>
		});
		
	</script>
    <sc:SubLayout id="clickdimensionscript" runat="server" path="/tmwebcustom/sublayouts/common/CDAndLivePersonScripts.ascx"/>

    <!-- Google Code Global Remarketing -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1030693709;
    var google_conversion_label = "G2QsCNPngAIQzca86wM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    <%-- 70289 --%>
    <script async src="http://i.simpli.fi/dpx.js?cid=8125&action=100&segment=taylormorrison&m=1"></script>

    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1030693709/?value=0&amp;label=G2QsCNPngAIQzca86wM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

    <script type="text/javascript">
        // BEGIN Google Analiytics Tracking code
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
        _gaq.push(['_setLocalRemoteServerMode']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        //END Google Analiytics Tracking code

        $j(document).ready(function () {
            $j('#siteSearchText').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    sitesearch('#siteSearchText');
                    return false;
                }
            });
        });
	</script>

    <script type="text/javascript">
        adroll_adv_id = "NEJ47QLMS5CI3EP5D5Q2YF";
        adroll_pix_id = "JUA2LYWVABBBLGG64ADBUR";
        (function () {
            var oldonload = window.onload;
            window.onload = function () {
                __adroll_loaded = true;
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                if (oldonload) { oldonload() }
            };
        } ());
    </script>
     <asp:literal runat="server" id="yahooPixels"></asp:literal>
     <sc:VisitorIdentification runat="server" />
</body>
</html>
