﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Web;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts
{
    public partial class SkyestoneMain : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        protected void Page_Load(object sender, EventArgs e)
        {
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountSkyestone) ?
                GoogleAnalytics.SkyestoneAccountNumber :
            Config.Settings.GoogleAnalyticsAccountSkyestone;
        }
    }
}