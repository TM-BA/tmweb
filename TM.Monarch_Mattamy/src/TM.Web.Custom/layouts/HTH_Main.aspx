﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HTH_Main.aspx.cs" Inherits="TM.Web.Custom.Layouts.HTM_Main" %>

<%@ Register TagPrefix="uc" TagName="MainNav" Src="~/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/USCanadaSVGMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="LeftPromo" Src="~/tmwebcustom/SubLayouts/Common/HeaderSections/HomePageLeftPromo.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head runat="server">
	<meta charset="utf-8" />
	<title>

    </title>
	<link rel="stylesheet" type="text/css" href="/Styles/tm_style.css" />
	<meta name="viewport" content="width=device-width">
	 <sc:VisitorIdentification runat="server" />
	<sc:Placeholder ID="phHeadSection" runat="server" Key="HeadSection" />
	<link rel="shortcut icon" type="image/x-icon" href="/Images/tm/favicon.ico">
	<!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">

	    if (typeof jQuery == 'undefined') {
	        document.write(unescape("%3Cscript src='/Javascript/lib/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
	    }

	    // BEGIN Google Analiytics Tracking code
	    var _gaq = _gaq || [];
	    _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
	    _gaq.push(['_setLocalRemoteServerMode']);
	    _gaq.push(['_trackPageview']);

	    (function () {
	        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	    })();
	    //END Google Analiytics Tracking code

	    var $j = jQuery.noConflict();

	    $j(document).ready(function () {
	        $j("li#findyournewhome").mouseover(function () {
	            $j('#svgmap').show();
	        }).mouseout(function () {
	            $j('#svgmap').hide();
	        });
	    });


	</script>
</head>
<body>
    <form id="form1" runat="server">
    <header>
		<div class="header">
			<div class="topheader">
				<ul>
					<sc:Sublayout runat="server" ID="subnowtrending" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx">
					</sc:Sublayout>
					<li class="top3">JOIN OUR INTEREST LIST</li>
					<li class="top4"><a href="#">
						<sc:Image runat="server" ID="scMyTmImg" Field="My Logo" />
					</a></li>
					<li class="top5"><a href="#">Create Account</a>|<a href="#">Login</a></li>
				</ul>
			</div>
		</div>
		<div class="hdline">
		</div>
		<div class="logomenu">
			<div class="lt-box">
				<a href="/" class="logo">
					<asp:Image runat="server" ID="imgSiteLogo" /></a>
			</div>
			<div class="rt-box">
				<nav id="navtop-main">
					<ul>
						<li id="findyournewhome"><a href="/new-homes"><span>FIND</span><span>YOUR NEW HOME</span></a>
							<uc:SVGMap ID="svgMap" runat="server" /></li>
						<sc:Sublayout ID="Sublayout1" runat="server" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" />
					</ul>
					<div class="clearfix">
					</div>
				</nav>
			</div>
			<div class="breadcrumb">
				
				 
			</div>
		</div>		    	 
		<div class="clearfix">
		</div>
	</header>
    <div class="home-content">
		<article>
			<div class="home-content-wrapper">
			    <sc:Placeholder runat="server" ID="phContent" Key="Content" />
				<div class="clearfix">
				</div>
			</div>
		</article>
	</div>
    <footer>
		<article>
		    <sc:Sublayout ID="footer" runat="server" Path="/tmwebcustom/sublayouts/common/footer.ascx"/>
			<sc:Placeholder ID="phFooter" runat="server" Key="FootSection" />
		</article>
	</footer>
    </form>
    <sc:SubLayout id="clickdimensionscript" runat="server" path="/tmwebcustom/sublayouts/common/ClickDimensionScript.ascx"/>
     <asp:literal runat="server" id="yahooPixels"></asp:literal>
</body>
</html>
