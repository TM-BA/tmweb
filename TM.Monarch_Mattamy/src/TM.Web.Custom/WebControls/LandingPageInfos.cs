﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	public class LandingPageInfos
	{
		public string CampaignDetails { get; set; }
		public string IhcCard { get; set; }
		public string LiveChat { get; set; }
	}
}