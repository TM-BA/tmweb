﻿using System;
using System.Collections.Generic;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class HomeforSaleSearchResults
	{
		public SearchFacet SearchFacet { get; set; }
		public List<HomeForSaleInfo> HomeForSaleInfo { get; set; }
	}
}
