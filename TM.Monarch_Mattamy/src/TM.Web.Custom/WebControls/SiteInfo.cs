﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public static class SiteInfo
	{
		public static string CurrentDomain { get; set; }
		public static string CurrentSiteHostName { get; set; }
		public static string CurrentCompanyName { get; set; }
		public static string CurrentCompanyAbbreviation { get; set; }
	}
}