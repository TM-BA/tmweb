﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class PPCCommunityInfo
	{			
		public String City { get; set; }
		public Int32 NumberofCities { get; set; }
		public String Company { get; set; }
	}


	[Serializable]
	public class CrossCompanyHeader
	{
		public String FavoriteType { get; set; }
		public String Company { get; set; }
		public String Domain { get; set; }
	}
}