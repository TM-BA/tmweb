﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class SearchFacet
	{
		//public List<HomeForSaleInfo> HomeForSaleInfo { get; set; }
		public string Division { get; set; }
		public List<NameandValue> Cities { get; set; }
		public List<NameandId> SchoolDistricts { get; set; }
		public List<NameandId> CommunityStatus { get; set; }
		public List<NameandId> Availability { get; set; }
		public List<NameandValue> BonusRoomsDensSolarium { get; set; }
		public double MinPrice { get; set; }
		public double MaxPrice { get; set; }
		public double SelectedMinPrice { get; set; }
		public double SelectedMaxPrice { get; set; }
		public double PriceTick { get; set; }
		public Int32 MinSqFt { get; set; }
		public Int32 MaxSqFt { get; set; }
		public Int32 SelectedMinSqFt { get; set; }
		public Int32 SelectedMaxSqFt { get; set; }
		public Int32 SqFtTick { get; set; }
		public List<MultiValueStatus> NumberofBedrooms { get; set; }
		public List<MultiNameValueStatus> NumberofBathrooms { get; set; }
		public List<MultiValueStatus> NumberofHalfBathrooms { get; set; }
		public List<MultiValueStatus> NumberofGarages { get; set; }
		public List<MultiValueStatus> NumberofStories { get; set; }
	}

	[Serializable]
	public class NameandValue
	{
		private string _itemStatus = GlobalEnums.FacetStatus.On.ToString();
		public string Name { get; set; }
		public string Value { get; set; }
		public bool IsSelected { get; set; }
		public string ParentName { get; set; }
		public string Domain { get; set; }
		public string ItemStatus
		{
			get { return _itemStatus; }
			set { _itemStatus = value; }
		}
	}

	[Serializable]
	public class NameandId
	{
		private string _itemStatus = GlobalEnums.FacetStatus.On.ToString();
		public ID ID { get; set; }
		public string Name { get; set; }
		public bool IsSelected { get; set; }
		public string ParentName { get; set; }
		public string Domain { get; set; }
		public string ItemStatus
		{
			get { return _itemStatus; }
			set { _itemStatus = value; }
		}
	}

	[Serializable]
	public class MultiValueStatus
	{
		private string _itemStatus = GlobalEnums.FacetStatus.On.ToString();
		public int Value { get; set; }
		public bool IsSelected { get; set; }
		public string ItemStatus
		{
			get { return _itemStatus; }
			set { _itemStatus = value; }
		}
	}

	[Serializable]
	public class MultiNameValueStatus
	{
		private string _itemStatus = GlobalEnums.FacetStatus.On.ToString();
		public string Value { get; set; }
		public bool IsSelected { get; set; }
		public string ItemStatus
		{
			get { return _itemStatus; }
			set { _itemStatus = value; }
		}
	}
}
