﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class PromoInfo
	{
		public string Title { get; set; }
		public bool OpenInNewBrowser { get; set; }
		public string ExternalLink { get; set; }
		public bool Disabled { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}