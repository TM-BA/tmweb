﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class SearchResults
	{
		public MapBounds MapBounds { get; set; }
		public List<CommunityInfo> CommunityInfo { get; set; }
		public List<CommunityInfo> MapCommunityInfo { get; set; }
		public SearchFacet SearchFacet { get; set; }
		//public List<ClusteredStateInfo> ClusteredStateInfo { get; set; }
		//public List<ClusteredDivisionInfo> ClusteredDivisionInfo { get; set; }
		public DefaultMapPoint DefaultMapPoint { get; set; }
		public List<HomeForSaleInfo> HomeForSaleInfo { get; set; }
	}

	[Serializable]
	public class SaveItemResults
	{
		public List<CrossCompanyHeader> Companies { get; set; }
		public MapBounds MapBounds { get; set; }
		public DefaultMapPoint DefaultMapPoint { get; set; }
		public List<CommunityInfo> MapCommunityInfo { get; set; }
		public List<CommunityInfo> CommunityInfo { get; set; }
		public List<PlanInfo> PlanInfo { get; set; }
		public List<HomeForSaleInfo> HomeForSaleInfo { get; set; }
	}
	
	[Serializable]
	public class MapBounds
	{
		public double MinLatitude { get; set; }
		public double MinLongitude { get; set; }
		public double MaxLatitude { get; set; }
		public double MaxLongitude { get; set; }
	}

	[Serializable]
	public class DefaultMapPoint
	{
		public double Latitude { get; set; }
		public double Longitude { get; set; }
	}
}
