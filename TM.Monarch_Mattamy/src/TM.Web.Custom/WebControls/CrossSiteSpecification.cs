﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class CrossSiteSpecification
	{
		public string SiteSpecificUrl { get; set; }
		public string Domain { get; set; }
		public string HostName { get; set; }
		public string CompanyName { get; set; }
		public string CompanyAbbreviation { get; set; }
	}
}