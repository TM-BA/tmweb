﻿using Sitecore.Data.Items;
using Sitecore.Shell.Applications.ContentEditor;

namespace TM.Web.Custom.WebControls
{
    public class QueryableTreeList : TreeList
    {
        
        
        
        public new string Source
        {
            get { return base.Source; }

            set
            {
                if (!value.StartsWith("query:"))
                {
                    base.Source = value;
                }
                else
                {
                    Item item = Sitecore.Context.ContentDatabase.Items[this.ItemID];
                    if (item != null)
                    {
                        Item itemQueried = item.Axes.SelectSingleItem(value.Substring("query:".Length));
                        base.Source = itemQueried != null ? itemQueried.Paths.FullPath : value;
                    }
                    else
                    {
                        base.Source = value;
                    }
                }
            }

        }





    }
}