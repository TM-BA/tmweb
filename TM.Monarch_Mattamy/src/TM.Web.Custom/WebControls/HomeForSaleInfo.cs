﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Domain.Entities;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class HomeForSaleInfo
	{
		private String _streetAddress1;

		public Guid HomeforSaleID { get; set; }
		public String HomeforSaleName { get; set; }
		public Guid HomeforSaleParentID { get; set; }
		public String HomeforSalePlanName { get; set; }
		public String HomeforSaleDetailsURL { get; set; }
		public String HomeforSaleLegacyID { get; set; }
		public String MasterPlanID { get; set; }
		public Double Price { get; set; }
		public GlobalEnums.SearchStatus HomeforSaleStatus { get; set; }
		public GlobalEnums.SearchStatus StatusforSearch { get; set; }
		public String StatusforAssistance { get; set; } //enum
		public String StatusforAvailability { get; set; } //enum
		public Boolean IsaModelHome { get; set; }
		public String myTMNumber { get; set; }
		public DateTime AvailabilityDate { get; set; }
		public string Availability { get; set; }
		public string AvailabilityItemId { get; set; }
		public String AvailabilityMonth { get; set; }		
		public String SubCommunityID { get; set; }
		public String LotNumber { get; set; }
		public String StreetAddress1
		{
			get { return _streetAddress1;}

			set { _streetAddress1 = string.IsNullOrEmpty(value) ? string.Empty : value; } 
		}
		public String StreetAddress2 { get; set; }
		public String City { get; set; }
		public String State { get; set; }
		public String Phone { get; set; }
		public String MLSNumber { get; set; }
		public String PlanDescription { get; set; }
		public int SquareFootage { get; set; }
		public int NumberofStories { get; set; }
		public int NumberofBedrooms { get; set; }
		public int NumberofBathrooms { get; set; }
		public int NumberofHalfBathrooms { get; set; }
		public int NumberofGarages { get; set; }		
		public int NumberofDens { get; set; }
		public int NumberofSolariums { get; set; }
		public int NumberofBonusRooms { get; set; }
		public int NumberofFamilyRooms { get; set; }
		public int NumberofLivingAreas { get; set; }
		public int NumberofDiningAreas { get; set; }
		public String MasterPlanElevationID { get; set; }
		public string ElevationStatus { get; set; } //enum
		public String ElevationNameOverride { get; set; }
		public String ElevationImages { get; set; }
		public int InteriorImageSortOrderOverride { get; set; }
		public Boolean DisplayasFeaturedHomeinRWE { get; set; }
		public string DisplayonHomesforSalePage { get; set; }
		public Boolean DisplayonREPPage { get; set; }
		public string RealtorIncentiveAmount { get; set; }
		public string RealtorBrokerCommissionRate { get; set; }
		public string HomeforSaleCommunityName { get; set; }
		public string HomeforSaleCommunityDetailsURL { get; set; }
		public bool IsCurrentDomain { get; set; }
		public string CompanyName { get; set; }
		public string Domain { get; set; }
		public string SchoolDistrict { get; set; }
		public string MapIcon { get; set; }
		public Guid HomeforSaleCommunityID { get; set; }
		public string NoLongerAvailableText { get; set; }
		public bool IsRealtor { get; set; }
		public string LiveChatSkill { get; set; }
		public string LiveChatButton { get; set; }
		public string FinanceYourDream { get; set; }

	    public List<IFPInfo> IFPs { get; set; }
	}

	/// <summary>
	/// Custom comparer for the HomeForSaleInfo class
	/// </summary>
	public class HomeForSaleInfoComparer : IEqualityComparer<HomeForSaleInfo>
	{
		public bool Equals(HomeForSaleInfo x, HomeForSaleInfo y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
				return false;
			return x.HomeforSaleID == y.HomeforSaleID && x.HomeforSalePlanName == y.HomeforSalePlanName;
		}

		public int GetHashCode(HomeForSaleInfo info)
		{
			if (ReferenceEquals(info, null)) return 0;
			var hashHomeforSaleName = info.HomeforSalePlanName == null ? 0 : info.HomeforSalePlanName.GetHashCode();
			var hashHomeforSaleCode = info.HomeforSaleID.GetHashCode();
			return hashHomeforSaleName ^ hashHomeforSaleCode;
		}
	}													  	
}

