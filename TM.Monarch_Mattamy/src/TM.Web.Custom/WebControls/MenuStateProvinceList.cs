﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web;
using System.Web.UI;
using System.Web.UI.Design;
using System.Xml;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using Sitecore.Data;
using Sitecore.Configuration;
using System.Collections.Generic;
using System.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.SCHelpers.Search;
using TM.Domain.Enums;

namespace TM.Web.Custom.WebControls
{
    /// <summary>MenuStateProvinceList control</summary>
    //[Designer(typeof(WebControlDesigner))]
    [ToolboxData("<{0}:MenuStateProvinceList runat=server></{0}:MenuStateProvinceList>")]
    public class MenuStateProvinceList : WebControl
    {
        private readonly Cache _webCache = new Cache();

        public Cache WebCache
        {
            get { return _webCache; }
        }

        private bool renderForSVGMap;

        public bool RenderForSVGMap
        {
            get { return renderForSVGMap; }
            set { renderForSVGMap = value; }
        }

        #region Fields

        string m_textField = "";

        #endregion

        #region Properties

        /// <summary>Name of field containing text</summary>
        [Category("Fields")]
        //[WebControlProperty]
        public string TextField
        {
            get
            {
                return m_textField;
            }
            set
            {
                m_textField = value;
            }
        }


        public bool RenderForLeftNav { get; set; }

        #endregion

        #region Protected methods

        /// <summary>Render control</summary>
        protected override void DoRender(HtmlTextWriter output)
        {

            if (RenderForSVGMap)
            {
                var MarketList = GetStateMarketList(false);
                var stateMarketList = MarketList["Current"];

                Uri uri = HttpContext.Current.Request.Url;
                RenderCurrentCompanyMarketList(output, stateMarketList);
                RenderAdditionalCompanyMarketList(output, MarketList);
            }
            else
            {
                RenderCurrentCompanyMarketList(output);

            }
        }

        private void RenderCurrentCompanyMarketList(HtmlTextWriter output)
        {
            var stateMarketList = GetStateMarketList();

            Uri uri = HttpContext.Current.Request.Url;

            //Canada provinces list
            if (ControlStyle.CssClass == "ddlist")
            {
                output.AddAttribute(HtmlTextWriterAttribute.Class, "svglstlt");
                output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>			 				
            }
            foreach (var stItem in stateMarketList.Where(s => s.Key == SCFieldNames.Canada))
            {
                var state = stItem.Key;
                var urlstate = state.Replace(" ", "_");
                List<Item> diviItems = stItem.Value;

                if (diviItems.Count > 0)
                {

                    output.AddAttribute(HtmlTextWriterAttribute.Class, ControlStyle.CssClass);
                    output.RenderBeginTag(HtmlTextWriterTag.Ul); //<ul>			 				

                    ////The link
                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                            string.Format("javascript:ViewStateProvince('{0}')", urlstate));
                    }
                    output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>

                    output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                               "TM.Common.GAlogevent('Search', 'Click', '" + state + "')");
                    output.RenderBeginTag(HtmlTextWriterTag.A);

                    output.Write(state);
                    output.RenderEndTag(); //</a>                        
                    output.RenderEndTag(); //</li>


                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.RenderEndTag(); //</ul>
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "innerdivi");
                        output.AddAttribute(HtmlTextWriterAttribute.Id, "idi_" + urlstate);
                        output.RenderBeginTag(HtmlTextWriterTag.Ol); //<ol>	
                    }

                    var newdiviList = GetdivisionList(diviItems);

                    foreach (var diviItem in newdiviList)
                    {
                        foreach (var item in diviItem.Value)
                        {
                            var name = item.Key;
                            var url = item.Value;

                            output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>
                            //The link
                            output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            //open in new browser
                            if (!url.Contains(uri.Authority))
                                output.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");

                            output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                                "TM.Common.GAlogevent('Search', 'Click', '" + name + "')");
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            output.Write(name);
                            output.RenderEndTag(); //</a>                        
                            output.RenderEndTag(); //</li>
                        }
                    }

                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.RenderEndTag(); //</ol>
                    }
                    else
                    {
                        output.RenderEndTag(); //</ul>
                    }
                }

            }

            if (ControlStyle.CssClass == "ddlist")
            {
                output.RenderEndTag(); //</div>			 				
            }

        }


        private void RenderCurrentCompanyMarketList(HtmlTextWriter output, Dictionary<string, List<Item>> stateMarketList)
        {
            Uri uri = HttpContext.Current.Request.Url;
            Item rootItem = Sitecore.Context.Database.Items[Sitecore.Context.Site.StartPath].Parent;

            string companyName;
            switch (rootItem.GetItemName().ToLower())
            {
                case "monarchgroup":
                    companyName = "Monarch Group";
                    break;
                case "darlinghomes":
                    companyName = "Darling Homes";
                    break;
                default:
                    companyName = "Taylor Morrison";
                    break;
            }


            output.AddAttribute(HtmlTextWriterAttribute.Class, "mod-tophea1");
            output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>
            output.Write(companyName);
            output.RenderEndTag();

            int columnBreaker = 0;

            //Canada provinces list
            if (ControlStyle.CssClass == "ddlist")
            {
                output.AddAttribute(HtmlTextWriterAttribute.Class, "svglstlt");
                output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>			 				
            }

            foreach (var stItem in stateMarketList.Where(s => s.Key == SCFieldNames.Canada))
            {

                var state = stItem.Key;
                var urlstate = state.Replace(" ", "_");
                List<Item> diviItems = stItem.Value;

                if (diviItems.Count > 0)
                {

                    output.AddAttribute(HtmlTextWriterAttribute.Class, ControlStyle.CssClass);
                    output.RenderBeginTag(HtmlTextWriterTag.Ul); //<ul>			 				

                    ////The link
                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                            string.Format("javascript:ViewStateProvince('{0}')", urlstate));
                    }
                    output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>


                    output.RenderBeginTag(HtmlTextWriterTag.A);

                    output.Write(state);
                    output.RenderEndTag(); //</a>                        
                    output.RenderEndTag(); //</li>


                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.RenderEndTag(); //</ul>
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "innerdivi");
                        output.AddAttribute(HtmlTextWriterAttribute.Id, "idi_" + urlstate);
                        output.RenderBeginTag(HtmlTextWriterTag.Ol); //<ol>	
                    }

                    var newdiviList = GetdivisionList(diviItems);

                    foreach (var diviItem in newdiviList)
                    {
                        foreach (var item in diviItem.Value)
                        {
                            var name = item.Key;
                            var url = item.Value;

                            output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>
                            //The link
                            output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                            //open in new browser
                            if (!url.Contains(uri.Authority))
                                output.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");

                            output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                                "TM.Common.GAlogevent('Search', 'Click', 'LeftofMapFindYourNewHome')");
                            output.RenderBeginTag(HtmlTextWriterTag.A);
                            output.Write(name);
                            output.RenderEndTag(); //</a>                        
                            output.RenderEndTag(); //</li>
                        }
                    }

                    if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                    {
                        output.RenderEndTag(); //</ol>
                    }
                    else
                    {
                        output.RenderEndTag(); //</ul>
                    }
                }
            }

            if (ControlStyle.CssClass == "ddlist")
            {
                output.RenderEndTag(); //</div>			 				
            }
        }


        private void RenderAdditionalCompanyMarketList(HtmlTextWriter output, Dictionary<string, Dictionary<string, List<Item>>> MarketList)
        {
            Uri uri = HttpContext.Current.Request.Url;
            Item rootItem = Sitecore.Context.Database.Items[Sitecore.Context.Site.StartPath].Parent;

            output.AddAttribute(HtmlTextWriterAttribute.Class, "mod-t1");
            output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>
            output.RenderEndTag();



            foreach (var market in MarketList.Where(s => s.Key != "Current"))
            {
                Dictionary<string, List<Item>> stateMarketList = MarketList[market.Key];

                string additionalMarkeTextt = "";

                if (market.Key == "MonarchGroup")
                {
                    additionalMarkeTextt = "Additional Homes by Monarch Group";
                }

                output.AddAttribute(HtmlTextWriterAttribute.Class, "mod-hea1");
                output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>
                output.Write(additionalMarkeTextt);
                output.RenderEndTag();


                int columnBreaker = 0;

                //Canada provinces list
                if (ControlStyle.CssClass == "ddlist")
                {
                    output.AddAttribute(HtmlTextWriterAttribute.Class, "svglstlt");
                    output.RenderBeginTag(HtmlTextWriterTag.Div); //<div>			 				
                }
                foreach (var stItem in stateMarketList.Where(s => s.Key == SCFieldNames.Canada))
                {

                    var state = stItem.Key;
                    var urlstate = state.Replace(" ", "_");
                    List<Item> diviItems = stItem.Value;

                    if (diviItems.Count > 0)
                    {

                        output.AddAttribute(HtmlTextWriterAttribute.Class, "ddlist-2");
                        output.RenderBeginTag(HtmlTextWriterTag.Ul); //<ul>			 				

                        ////The link
                        if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                                string.Format("javascript:ViewStateProvince('{0}')", urlstate));
                        }
                        output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>


                        output.RenderBeginTag(HtmlTextWriterTag.A);

                        output.Write(state);
                        output.RenderEndTag(); //</a>                        
                        output.RenderEndTag(); //</li>


                        if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                        {
                            output.RenderEndTag(); //</ul>
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "innerdivi");
                            output.AddAttribute(HtmlTextWriterAttribute.Id, "idi_" + urlstate);
                            output.RenderBeginTag(HtmlTextWriterTag.Ol); //<ol>	
                        }

                        var newdiviList = GetdivisionList(diviItems);

                        foreach (var diviItem in newdiviList)
                        {
                            foreach (var item in diviItem.Value)
                            {
                                var name = item.Key;
                                var url = item.Value;

                                output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>
                                //The link
                                output.AddAttribute(HtmlTextWriterAttribute.Href, url);
                                //open in new browser
                                if (!url.Contains(uri.Authority))
                                    output.AddAttribute(HtmlTextWriterAttribute.Target, "_blank");

                                output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                                                "TM.Common.GAlogevent('Search', 'Click', 'LeftofMapFindYourNewHome')");
                                output.RenderBeginTag(HtmlTextWriterTag.A);
                                output.Write(name);
                                output.RenderEndTag(); //</a>                        
                                output.RenderEndTag(); //</li>
                            }
                        }

                        if (ControlStyle.CssClass == "ltste" && diviItems.Count > 0)
                        {
                            output.RenderEndTag(); //</ol>
                        }
                        else
                        {
                            output.RenderEndTag(); //</ul>
                        }
                    }
                }

                if (ControlStyle.CssClass == "ddlist")
                {
                    output.RenderEndTag(); //</div>			 				
                }


            }

        }


        private static Dictionary<string, Dictionary<string, string>> GetdivisionList(IEnumerable<Item> diviItems)
        {
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };
            Uri uri = HttpContext.Current.Request.Url;

            var newDiviList = new Dictionary<string, Dictionary<string, string>>();
            foreach (var diviItem in diviItems)
            {
                var url = diviItem.GetSiteSpecificUrl(sitecoreUrlOptions, uri);
                var diviName = diviItem.DisplayName;

                if (newDiviList.ContainsKey(diviName))
                {
                    var existingDivi = newDiviList[diviName];
                    if (!existingDivi[diviName].Contains(uri.Authority))
                    {
                        var newdic = new Dictionary<string, string> { { diviName, url } };
                        newDiviList.Remove(diviName);
                        newDiviList.Add(diviName, newdic);
                    }
                    else if (url.Contains(uri.Authority))
                    {
                        //update existing key value
                        var exdic = new Dictionary<string, string> { { string.Format("{0} {1}", diviName, (existingDivi[diviName].Contains(SCFieldNames.NewCondos) ? SCFieldNames.Condos : SCFieldNames.SingleFamily)), existingDivi[diviName] } };
                        newDiviList.Remove(diviName);
                        newDiviList.Add(diviName, exdic);

                        //add new key value on the existing parent key
                        newDiviList[diviName].Add(
                            string.Format("{0} {1}", diviName,
                                          (url.Contains(SCFieldNames.NewCondos) ? SCFieldNames.Condos : SCFieldNames.SingleFamily)), url);
                    }
                }
                else
                {
                    newDiviList.Add(diviName, new Dictionary<string, string> { { diviName, url } });
                }
            }
            return newDiviList;
        }

        private Dictionary<string, List<Item>> GetStateMarketList()
        {
            //if cross company marketing list
            var items = RenderForLeftNav ? CompanySearch.GetCurrentCompanyStateMarketingList() : CompanySearch.GetCrossCompaniesStateMarketingList();


            var activeStateItems = items.Where(s => s.TemplateID == SCIDs.TemplateIds.StatePage).OrderBy(o => o.Name);
            var activedivisionItems = items.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name);

            var stateMarketList = new Dictionary<string, List<Item>>();

            var canPros = SearchHelper.CanadaProvinces();

            foreach (Item stItem in activeStateItems)
            {
                var diviItems = activedivisionItems.Where(d => d.ParentID.ToGuid() == stItem.ID.ToGuid()).ToList();

                if (diviItems.Count > 0)
                {
                    string stateName = stItem.DisplayName;

                    foreach (var canPro in canPros.Where(canPro => stateName.ToLower() == canPro.ToLower()))
                    {
                        stateName = SCFieldNames.Canada;
                    }

                    if (!stateMarketList.ContainsKey(stateName))
                    {
                        stateMarketList.Add(stateName, new List<Item>());
                        stateMarketList[stateName].AddRange(diviItems);
                    }
                    else
                    {
                        stateMarketList[stateName].AddRange(diviItems);
                    }

                }
            }

            return stateMarketList;
        }


        private Dictionary<string, Dictionary<string, List<Item>>> GetStateMarketList(bool crossCompanySearch)
        {
            var MarketList = new Dictionary<string, Dictionary<string, List<Item>>>();
            var stateMarketList = new Dictionary<string, List<Item>>();

            // Current Company Market List 
            var items = CompanySearch.GetCurrentCompanyStateMarketingList();
            var activeStateItems = items.Where(s => s.TemplateID == SCIDs.TemplateIds.StatePage).OrderBy(o => o.Name);
            var activedivisionItems = items.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name);

            var canPros = SearchHelper.CanadaProvinces();
            foreach (Item stItem in activeStateItems)
            {
                var diviItems = activedivisionItems.Where(d => d.ParentID.ToGuid() == stItem.ID.ToGuid()).ToList();

                if (diviItems.Count > 0)
                {
                    string stateName = stItem.DisplayName;

                    foreach (var canPro in canPros.Where(canPro => stateName.ToLower() == canPro.ToLower()))
                    {
                        stateName = SCFieldNames.Canada;
                    }


                    if (!stateMarketList.ContainsKey(stateName))
                    {
                        stateMarketList.Add(stateName, new List<Item>());
                        stateMarketList[stateName].AddRange(diviItems);
                    }
                    else
                    {
                        stateMarketList[stateName].AddRange(diviItems);
                    }

                }
            }


            MarketList.Add("Current", stateMarketList);

            // Aditional Market List
            Item rootItem = Sitecore.Context.Database.Items[Sitecore.Context.Site.StartPath].Parent;

            if (rootItem.ID == SCIDs.CompanyIds.TaylorMorrison)
            {
                MarketList.Add("DarlingHomes", GetAdditionalStateMarketList(SCIDs.CompanyIds.DarlingHomes));

                MarketList.Add("MonarchGroup", GetAdditionalStateMarketList(SCIDs.CompanyIds.MonarchGroup));


            }
            else if (rootItem.ID == SCIDs.CompanyIds.MonarchGroup)
            {
                MarketList.Add("TaylorMorrison", GetAdditionalStateMarketList(SCIDs.CompanyIds.TaylorMorrison));

                MarketList.Add("DarlingHomes", GetAdditionalStateMarketList(SCIDs.CompanyIds.DarlingHomes));
            }
            else if (rootItem.ID == SCIDs.CompanyIds.DarlingHomes)
            {

                MarketList.Add("TaylorMorrison", GetAdditionalStateMarketList(SCIDs.CompanyIds.TaylorMorrison));

                MarketList.Add("MonarchGroup", GetAdditionalStateMarketList(SCIDs.CompanyIds.MonarchGroup));


            }
            return MarketList;
        }

        private Dictionary<string, List<Item>> GetAdditionalStateMarketList(ID Company)
        {
            var stateMarketList = new Dictionary<string, List<Item>>();

            //Get additional items
            var items = CompanySearch.GetSelectedCompaniesStateMarketingList(Company);


            var activeStateItems = items.Where(s => s.TemplateID == SCIDs.TemplateIds.StatePage).OrderBy(o => o.Name);
            var activedivisionItems = items.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name);



            var canPros = SearchHelper.CanadaProvinces();

            foreach (Item stItem in activeStateItems)
            {
                var diviItems = activedivisionItems.Where(d => d.ParentID.ToGuid() == stItem.ID.ToGuid()).ToList();

                if (diviItems.Count > 0)
                {
                    string stateName = stItem.DisplayName;

                    foreach (var canPro in canPros.Where(canPro => stateName.ToLower() == canPro.ToLower()))
                    {
                        stateName = SCFieldNames.Canada;
                    }

                    if (!stateMarketList.ContainsKey(stateName))
                    {
                        stateMarketList.Add(stateName, new List<Item>());
                        stateMarketList[stateName].AddRange(diviItems);
                    }
                    else
                    {
                        stateMarketList[stateName].AddRange(diviItems);
                    }

                }
            }

            return stateMarketList;
        }


        #endregion
    }
}
