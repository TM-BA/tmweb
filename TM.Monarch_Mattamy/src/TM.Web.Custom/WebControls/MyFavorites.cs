﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	public class MyFavorites
	{
		//public MyFavorites(string json)
		//{
		//    JObject jObject = JObject.Parse(json);
		//    JToken jUser = jObject["user"];
		//    name = (string) jUser["name"];
		//    teamname = (string) jUser["teamname"];
		//    email = (string) jUser["email"];
		//    players = jUser["players"].ToArray();
		//}

		public ID Id { get; set; }
		public GlobalEnums.FavoritesType Type { get; set; }
		public GlobalEnums.FavoritesAction Action { get; set; }

		//public string Id { get; set; }
		//public string Type { get; set; }
		//public string Action { get; set; }
	}
}