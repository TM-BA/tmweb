﻿using System;
using System.Collections.Generic;
using TM.Utils.Web;
using Sitecore.Data.Items;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class CommunityInfo
	{
		public Guid CommunityID { get; set; }
		public Guid CommunityParentID { get; set; }
		public String Name { get; set; }
		public String CommunityName { get; set; }
		public String SubCommunityName { get; set; }
		public String CommunityLegacyID { get; set; }
		public GlobalEnums.CommunityStatus CommunityStatus {get; set;}
		public string CommunityStatusId { get; set; }
		public string CommunityStatusChangeDate { get; set; }
		public GlobalEnums.SearchStatus StatusforSearch { get; set; }
		public GlobalEnums.SearchStatus StatusforAssistance {get; set;}
		public String CommunityPrice { get; set; }
		public String PriceOverrideText { get; set; }
		public String CommunityDescription { get; set; }
		public String CommunityPPCDescription { get; set; }
		public String CommunityDisclaimerImage { get; set; }
		public String CommunityImageStatusFlag { get; set; }
		public String CommunityLogo { get; set; } 
		public String CommunityBrochure { get; set; }
		public Double CommunityLatitude { get; set; }
		public Double CommunityLongitude { get; set; }
		public int CommunityZoom { get; set; }
		public String StreetAddress1 { get; set; }
		public String StreetAddress2 { get; set; }
		public String City {get; set;}
		public String StateProvince{ get; set; }
		public String StateProvinceAbbreviation {get; set;}
		public String DivisionOf { get; set; }
		public String ZipPostalCode { get; set; }
		public String Phone { get; set; }
		public String IhcPhone { get; set; }
		public String Fax { get; set; }
		public String RespondingEmailAddress { get; set; }
		public String OfficeHoursDay {get; set;}
		public String OfficeHoursStartforDay { get; set; }
		public String OfficeHoursEndforDay { get; set; }
		public String AlternateImage { get; set; }
		public String SitePlanDescription { get; set; }
		public String SitePlanImage { get; set; }
        public bool hasSitePlan { get; set; }
		public List<SubcommunityInfo> SubcommunityList{get; set;}
		public List<HomeForSaleInfo> HomeForSaleList { get; set; }
		public List<PlanInfo> PlanList { get; set; }
		public string MapIcon { get; set; }
		public string DynamicMapIcon { get; set; }
		public string ClusteredMapIcon { get; set; }
		public string SingleClusteredMapIcon { get; set; }
		public string CommunityCardImage { get; set; }		
		public string CommunityDetailsURL { get; set; }
		public string SquareFootage { get; set; }
		public string Bedrooms { get; set; }
		public string Bathrooms { get; set; }
		public string Stories { get; set; }
		public string Garage { get; set; }
		public String SpecialIncentivesPromotions { get; set;}
		public bool IsCurrentDomain { get; set; }
		public string Domain { get; set; }
		public string CompanyName { get; set; }
		public string CompanyNameAbbreviation { get; set; }
		public string HostName { get; set; }
		public bool IsFavorite { get; set; }
		public string MapPinStatus { get; set; }
		public int MapCount { get; set; }
		public string StateClusteredCommunityFlyouts { get; set; }
		public string DivisionClusteredCommunityFlyouts { get; set; }
		public string CommunityFlyouts { get; set; }
		public string SchoolDistrict { get; set; }
		public List<int> BedRange { get; set; }
		public List<int> BathRange { get; set; }
		public List<int> GarageRange { get; set; }
		public List<int> StoryRange { get; set; }
		public List<int> HalfBathRange { get; set; }
		public List<int> BonusRoomRange { get; set; }
		public List<int> DenRange { get; set; }
		public List<int> SolariumRange { get; set; }
		public List<double> PriceRange { get; set; }
		public List<int> SqFtRange { get; set; }
		public double MinPrice { get; set; }
		public double MaxPrice { get; set; }
		public int MinSqFt { get; set; }
		public int MaxSqFt { get; set; }
		public int HalfBath { get; set; }
		public string NoLongerAvailableText { get; set; }
		public string LiveChatSkill { get; set; }
		public string LiveChatButton { get; set; }
		public PromoInfo PromoInfo { get; set; }
		public string CommunityDivisionName { get; set; }
		public string CommunityCityName { get; set; }
		public string CommunityStateName { get; set; }
        public int CurrentDevice { get; set; }
	}

	/// <summary>
	/// Custom comparer for the CommunityInfo class
	/// </summary>
	public class CommunityInfoComparer : IEqualityComparer<CommunityInfo>
	{
		public bool Equals(CommunityInfo x, CommunityInfo y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
				return false;
			return x.CommunityID == y.CommunityID && x.CompanyName == y.CompanyName;
		}

		public int GetHashCode(CommunityInfo info)
		{
			if (ReferenceEquals(info, null)) return 0;
			var hashCommunityName = info.CompanyName == null ? 0 : info.CompanyName.GetHashCode();
			var hashCommunityCode = info.CommunityID.GetHashCode();
			return hashCommunityName ^ hashCommunityCode;
		}
	}

	[Serializable]
	public class ClusteredCommunityInfo
	{
		public String StateProvince { get; set; }
		public List<ClusteredCommunityDetails> ClusteredCommunity { get; set; }
	}	

	[Serializable]
	public class ClusteredStateInfo
	{
		public String StateProvince{ get; set; }
		public String StateProvinceAbbreviation { get; set; }
		public List<ClusteredCommunityDetails> ClusteredCommunities { get; set; }
		public SearchCentroids SearchCentroids { get; set; }
		public string MapIcon { get; set; }
	}

	//[Serializable]
	//public class ClusteredDivisionInfo
	//{
	//    public String StateProvince { get; set; }
	//    public String StateProvinceAbbreviation { get; set; }
	//    public List<ClusteredCommunityDetails> ClusteredCommunities { get; set; }
	//    public SearchCentroids SearchCentroids { get; set; }
	//    public string MapIcon { get; set; }
	//}

	//[Serializable]
	//public class ClusteredCityInfo
	//{
	//    public String StateProvince{ get; set; }
	//    public String StateProvinceAbbreviation { get; set; }
	//    public List<ClusteredCommunityDetails> ClusteredCommunities { get; set; }
	//    public SearchCentroids SearchCentroids { get; set; }
	//    public string MapIcon { get; set; }
	//}

	[Serializable]
	public class SearchCentroids
	{
		public string Name { get; set; }
		public Double CentroidLatitude { get; set; }
		public Double CentroidLongitude { get; set; }
		public int CentroidZoom { get; set; }
	}

	[Serializable]
	public class ClusteredCommunityDetails
	{
		public Guid CommunityID { get; set; }
		public String CommunityName { get; set; }
		public String SubCommunityName { get; set; }
		public string CommunityDetailsURL { get; set; }		
	}
    public class CommunityLatLong
    {
        public string CommunityID { get; set; }
        public float Lattitude { get; set; }
        public float Longitude { get; set; }

        public CommunityLatLong(string comid, float lattitude, float longitude)
        {
            this.CommunityID = comid;
            this.Lattitude = lattitude;
            this.Longitude = longitude;
        }
    }
    public class SimilarCommunity
    {
        public SimilarCommunity()
        {
            this.comID = string.Empty;
            this.priceMin = float.MaxValue;
            this.priceMax = float.MinValue;
            this.sqftMin = int.MaxValue;
            this.sqftMax =int.MinValue;
            this.bedMin =int.MaxValue;
            this.bedMax =int.MinValue;
            this.bathMax = int.MinValue;
            this.bathMin = int.MaxValue;
            this.priceAvg = 0f;
        }
        public string comID {get;set;}
        public float priceMin { get; set; }
        public float priceMax { get; set; }
        public float priceAvg { get; set; }
        public int sqftMin { get; set; }
        public int sqftMax { get; set; }
        public int bedMin { get; set; }
        public int bedMax { get; set; }
        public int bathMax { get; set; }
        public int bathMin { get; set; }
    }
}