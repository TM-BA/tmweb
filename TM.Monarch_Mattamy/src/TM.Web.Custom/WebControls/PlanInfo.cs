﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Domain.Entities;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class PlanInfo
	{
		public Guid PlanID { get; set; }
		public Guid PlanParentID { get; set; }
		public String PlanName { get; set; }
		public String PlanPageUrl { get; set; }
		public String PlanLegacyID { get; set; }
		public String MasterPlanID { get; set; }
		public Double PricedfromValue { get; set; }
		public String PricedfromText { get; set; }
		public GlobalEnums.SearchStatus PlanStatus { get; set; }
		public GlobalEnums.SearchStatus StatusforSearch { get; set; }
		public string StatusforAssistance { get; set; } //enum
		public string StatusforAdvertising { get; set; }	//enum
		public String SubCommunityID { get; set; }
		public String PlanDescription { get; set; }
		public int SquareFootage { get; set; }
		public int NumberofStories { get; set; }
		public int NumberofBedrooms { get; set; }
		public int NumberofBathrooms { get; set; }
		public int NumberofHalfBathrooms { get; set; }
		public int NumberofGarages { get; set; }	
		public int NumberofDens { get; set; }
		public int NumberofSolariums { get; set; }
		public int NumberofBonusRooms { get; set; }
		public int NumberofFamilyRooms { get; set; }
		public int NumberofLivingAreas { get; set; }
		public int NumberofDiningAreas { get; set; }
		public Boolean HasaSunRoom { get; set; }
		public Boolean HasaStudy { get; set; }
		public Boolean HasaLoft { get; set; }
		public Boolean HasanOffice { get; set; }
		public Boolean HasaGameRoom { get; set; }
		public Boolean HasaMediaRoom { get; set; }
		public Boolean HasaGuestBedroom { get; set; }
		public Boolean HasaBonusRoom { get; set; }
		public Boolean HasaBasement { get; set; }
		public Boolean HasaFireplace { get; set; }
		public String MasterPlanElevationID { get; set; }
		public string ElevationStatus { get; set; } //enum
		public String ElevationNameOverride { get; set; }
		public String InteriorImageMasterPlanID { get; set; }
		public string InteriorImageStatus { get; set; } //enum
		public String InteriorImageCaptionOverride { get; set; }
		public int InteriorImageSortOrderOverride { get; set; }
		public String FloorPlanImageMasterFloorPlanID { get; set; }
		public String FloorPlanImageStatus { get; set; }
		public String FloorPlanImageCaptionOverride { get; set; }
		public int FloorPlanImageSortOrderOverride { get; set; }
		public String VirtualTourMasterPlanID { get; set; }
		public String VirtualTourStatus { get; set; }
		public String PlanBrochureMasterID { get; set; }
		public String PlanBrochureStatus { get; set; }
		public List<HomeForSaleInfo> HomeForSaleList { get; set; }
		public String CompanyName { get; set; }
		public String Domain { get; set; }
		public Guid PlanCommunityID { get; set; }
		public string CommunityName { get; set; }
		public string CommunityDetailsURL { get; set; }
		public bool IsCurrentDomain { get; set; }
		public string Phone { get; set; }
		public string ElevationPlanImage { get; set; }
		public string MapIcon { get; set; }
		public string NoLongerAvailableText { get; set; }
		public string State { get; set; }
		public string LiveChatSkill { get; set; }
		public string LiveChatButton { get; set; }
		public string FinanceYourDream { get; set; }
		public List<IFPInfo> IFPs { get; set; } 

	}
	
	/// <summary>
	/// Custom comparer for the PlanInfo class
	/// </summary>
	public class PlanInfoComparer : IEqualityComparer<PlanInfo>
	{
		public bool Equals(PlanInfo x, PlanInfo y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
				return false;
			return x.PlanID == y.PlanID && x.PlanName == y.PlanName;
		}

		public int GetHashCode(PlanInfo info)
		{
			if (ReferenceEquals(info, null)) return 0;
			var hashPlanName = info.PlanName == null ? 0 : info.PlanName.GetHashCode();
			var hashPlanCode = info.PlanID.GetHashCode();
			return hashPlanName ^ hashPlanCode;
		}
	}
	
}
