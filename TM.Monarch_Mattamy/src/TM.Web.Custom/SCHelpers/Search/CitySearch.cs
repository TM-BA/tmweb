﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;

namespace TM.Web.Custom.SCHelpers.Search
{
    public class CitySearch
    {
        public static List<Item> GetCitiesByDivision(string areaId, Database db = null)
        {
            string qry =
                String.Format(
                    "fast:/sitecore/content/*{0}/home/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']/*[@@templateid='{4}']/*{5}/ancestor::*[@@templateid='{4}' and @@parentid='{6}']",
                    SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage,
                    SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage,
                    SCFastQueries.ActiveCommunitiesCondition, areaId);
            return (db ?? SearchHelper.ContextDB).SelectItems(qry).ToList();
        }
    }
}