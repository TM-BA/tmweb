﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Sitecore;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Publishing;
using Sitecore.Resources.Media;
using Sitecore.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.SCHelpers
{
    public static class SCExtensions
    {
        #region Identity Tests --------------------------------------------------------------------

        public static bool IsStandardValues(this Item item)
        {
            if (item == null)
                return false;
            bool isStandardValue = false;

            if (item.Template.StandardValues != null)
                isStandardValue = (item.Template.StandardValues.ID == item.ID);

            return isStandardValue;
        }

        public static bool IsTemplate(this Item item)
        {
            return item.Database.Engines.TemplateEngine.IsTemplatePart(item);
        }

        #endregion Identity Tests --------------------------------------------------------------------

        #region Templates -------------------------------------------------------------------------

        public static bool IsDerivedFromTemplate(this Item item, ID templateId)
        {
            if (item == null)
                return false;

            if (templateId.IsNull)
                return false;

            TemplateItem templateItem = item.Database.Templates[templateId];

            bool returnValue = false;
            if (templateItem != null)
            {
                Template template = TemplateManager.GetTemplate(item);

                returnValue = template != null && template.ID == templateItem.ID || template.DescendsFrom(templateItem.ID);
            }

            return returnValue;
        }

        public static IEnumerable<Item> ChildrenDerivedFrom(this Item item, ID templateId)
        {
            ChildList children = item.GetChildren();
            List<Item> childrenDerivedFrom = new List<Item>();

            foreach (Item child in children)
            {
                if (child.IsDerivedFromTemplate(templateId))
                    childrenDerivedFrom.Add(child);
            }

            return childrenDerivedFrom;
        }

        #endregion Templates -------------------------------------------------------------------------

        #region Relations -------------------------------------------------------------------------

        public static IEnumerable<Item> GetReferrersAsItems(this Item item)
        {
            var links = Globals.LinkDatabase.GetReferrers(item);
            return links.Select(i => i.GetTargetItem()).Where(i => i != null);
        }

        public static IEnumerable<Item> GetAncestors(this Item item, Boolean descending = true)
        {
            return item.Axes.GetAncestors();
        }

        #endregion Relations -------------------------------------------------------------------------

        #region Publishing

        public static bool IsPublished(this Item pItem)
        {
            Database lWebDb = Factory.GetDatabase("web");
            if (pItem != null && lWebDb != null)
            {
                Item lWebItem = lWebDb.GetItem(pItem.ID, pItem.Language, pItem.Version);
                if (lWebItem == null || pItem.Statistics.Updated > lWebItem.Statistics.Updated)
                {
                    return false;
                }
            }
            return true;
        }

        public static Boolean IsValidForPublish(this Item item)
        {
            if (item == null)
                return false;

            CheckboxField hideVersion = item.Fields["__Hide version"];
            if (hideVersion.Checked)
                return false;

            DateTime validFrom = !String.IsNullOrEmpty(item["__Valid from"]) ? DateUtil.IsoDateToDateTime(item["__Valid from"]) : DateTime.MinValue;
            DateTime validTo = !String.IsNullOrEmpty(item["__Valid to"]) ? DateUtil.IsoDateToDateTime(item["__Valid to"]) : DateTime.MaxValue;

            return DateTime.Now >= validFrom && DateTime.Now <= validTo;
        }

        #endregion Publishing

        #region Languages -------------------------------------------------------------------------

        public static bool HasLanguage(this Item item, string languageName)
        {
            return ItemManager.GetVersions(item, LanguageManager.GetLanguage(languageName)).Count > 0;
        }

        public static bool HasLanguage(this Item item, Language language)
        {
            return ItemManager.GetVersions(item, language).Count > 0;
        }

        public static int LanguageVersionCount(this Item item, Language lang)
        {
            if (item == null)
                return 0;
            Item currentItem = item.Database.GetItem(item.ID, lang);
            return currentItem.Versions.Count > 0 ? currentItem.Versions.Count : 0;
        }

        #endregion Languages -------------------------------------------------------------------------

        /*   public static string ToString(this Item item)
        {
            if(item== null){
                return "NULL";
            }
            return item.DisplayName + " - " + item.Paths.FullPath;
        }*/

        public static Boolean IsInContextSite(this Item item)
        {
            if (item == null)
                return false;

            Assert.IsNotNull(Context.Site, "Sitecore.Context.Site required by the Item.IsInSite extension is null");
            Assert.IsNotNullOrEmpty(Context.Site.RootPath, "Sitecore.Context.Site.RootPath required by the Item.IsInSite extension is null or empty");
            Assert.IsNotNull(Context.Database, "Sitecore.Context.Database required by the Item.IsInSite extension is null");

            Item rootItem = Context.Site.Database.Items[Context.Site.RootPath];
            Assert.IsNotNull(rootItem, String.Format("Unable to retrieve the item at path {0} using the database {1}", Context.Site.RootPath, Context.Database.Name));

            Item currentItem = item;

            while (currentItem != null)
            {
                if (currentItem.ID.Guid.Equals(rootItem.ID.Guid))
                    return true;

                currentItem = currentItem.Parent;
            }

            return false;
        }

        /// <summary>
        /// Gets the list of child items based on TemplateName parameter
        /// </summary>
        public static List<Item> GetChildrenByTemplate(this Item item, string TemplateName)
        {
            if (item.HasChildren)
            {
                return item.Children.Where(x => x.TemplateName.Equals(TemplateName)).ToList();
            }
            return null;
        }

        /// <summary>
        /// Gets the list of child items based on TemplateID parameter
        /// </summary>
        public static List<Item> GetChildrenByTemplate(this Item item, ID TemplateID)
        {
            if (item.HasChildren)
            {
                return item.Children.Where(x => x.TemplateID.Equals(TemplateID)).ToList();
            }
            return new List<Item>();
        }

        /// <summary>
        /// Gets the list of child items based on TemplateID parameter
        /// </summary>
        public static List<Item> GetAllChildrenByTemplate(this Item item, ID templateID)
        {
            if (item.HasChildren)
            {
                //foreach (Item child in item.Children)
                //{
                //   return child.GetChildrenByTemplate(templateID);
                //}
                return item.GetChildrenByTemplate(templateID);
            }
            return new List<Item>();
        }

        /// <summary>
        /// Gets the list of child items based on TemplateID parameter
        /// </summary>
        public static List<Item> GetAllChildrenByTemplate(this Item item, string templateID)
        {
            return GetAllChildrenByTemplate(item, new ID(templateID));
        }

        /// <summary>
        /// Gets the (friendly) URL of an item
        /// </summary>
        public static string GetItemUrl(this Item item)
        {
            return SCUtils.GetItemUrl(item);
        }

        public static string GetItemName(this Item item)
        {
            return item.GetItemName(false);
        }

        /// <summary>
        /// Returns the best name available for an item type
        /// </summary>
        /// <param name="item">The item we want the name of</param>
        /// <param name="useShortName">Should we give you the shortest name possible</param>
        /// <returns></returns>
        public static string GetItemName(this Item item, bool useShortName)
        {
            if (item != null)
            {
                switch (item.TemplateID.ToString())
                {
                    case SCIDs.TemplateIds.SwitchCommunityPage:
                        return string.IsNullOrWhiteSpace(item["Community Name"]) ? item.DisplayName : item["Community Name"];
                    case SCIDs.TemplateIds.SwitchPlanPage:
                        return string.IsNullOrWhiteSpace(item["Plan Name"]) ? item.DisplayName : item["Plan Name"];
                    case SCIDs.TemplateIds.SwitchInventoryPage:
                        return string.IsNullOrWhiteSpace(item["Street Address 1"]) ? item.DisplayName : item["Street Address 1"];
                    case SCIDs.TemplateIds.SwitchSpecificCampaignPage:
                    case SCIDs.TemplateIds.SwitchGenericCampaignPage:
                        return string.IsNullOrWhiteSpace(item["Campaign Title"]) ? item.DisplayName : item["Campaign Title"];
                    case SCIDs.TemplateIds.SwitchSchoolPage:
                        return string.IsNullOrWhiteSpace(item["School Name"]) ? item.DisplayName : item["School Name"];
                    case SCIDs.TemplateIds.SwitchCityPage:
                        return string.IsNullOrWhiteSpace(item["City Name"]) ? item.DisplayName : item["City Name"];
                    case SCIDs.TemplateIds.SwitchDivisionPage:
                        return string.IsNullOrWhiteSpace(item["Division Name"]) ? item.DisplayName : item["Division Name"];
                    case SCIDs.TemplateIds.SwitchStatePage:
                        return (!string.IsNullOrWhiteSpace(item["State Name"])
                            && null != Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(item["State Name"])))
                            ? Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(item["State Name"])).GetItemName(useShortName) : item.DisplayName;
                    case SCIDs.TemplateIds.SwitchProductPage:
                        return string.IsNullOrWhiteSpace(item["Product Type Name"]) ? item.DisplayName : item["Product Type Name"];
                    case SCIDs.TemplateIds.SwitchHomePage:
                        return string.IsNullOrWhiteSpace(item["Company Name"]) ? item.DisplayName : item["Company Name"];
                    case SCIDs.TemplateIds.SwitchSubCommunity:
                        return string.IsNullOrWhiteSpace(item["Subcommunity Name"]) ? item.DisplayName : item["Subcommunity Name"];
                    case SCIDs.TemplateIds.SwitchLookupItem:
                        if (useShortName)
                        {
                            return string.IsNullOrWhiteSpace(item["Code"]) ? item.DisplayName : item["Code"];
                        }
                        return string.IsNullOrWhiteSpace(item["Name"]) ? item.DisplayName : item["Name"];
                }

                return item.DisplayName;
            }
            return string.Empty;
        }

        public static string GetSiteSpecificUrl(this Item item, UrlOptions options, Uri uri, XmlNode sites)
        {
            //var sites = Sitecore.Configuration.Factory.GetConfigNode(PageUrls.ConfigSitePath);
            XmlNodeList childNodes = sites.ChildNodes;

            string specificUrl = item.GetItemUrl(options);

            var currentDb = Sitecore.Context.Database.Name.ToLower();

            foreach (XmlNode childNode in childNodes.Cast<XmlNode>().Where(childNode => childNode.Attributes != null && (
                childNode.Attributes["name"].Value.ToLower() == "monarchgroup"
                || childNode.Attributes["name"].Value.ToLower() == "darlinghomes"
                || childNode.Attributes["name"].Value.ToLower() == "website"
                || childNode.Attributes["name"].Value.ToLower() == "taylormorrison"
                || childNode.Attributes["name"].Value.ToLower() == "mg"
                || childNode.Attributes["name"].Value.ToLower() == "dh"
                || childNode.Attributes["name"].Value.ToLower() == "tm")))
            {
                if (childNode.Attributes != null
                    && null != childNode.Attributes["rootPath"]
                    && specificUrl.Contains(childNode.Attributes["rootPath"].Value)
                    && childNode.Attributes["database"].Value == currentDb)
                    return string.Format("{0}://{1}", uri.Scheme,
                                         childNode.Attributes["hostName"].Value +
                                         specificUrl.Replace(childNode.Attributes["rootPath"].Value + "/home", string.Empty));
            }
            return string.Format("{0}://{1}{2}", uri.Scheme, uri.Authority, specificUrl);
        }

        public static string GetSiteSpecificUrl(this Item item, UrlOptions options, Uri uri)
        {
            var sites = Sitecore.Configuration.Factory.GetConfigNode(PageUrls.ConfigSitePath);
            return item.GetSiteSpecificUrl(options, uri, sites);
        }

        private static IEnumerable<XmlNode> _sites;

        private static IEnumerable<XmlNode> Sites
        {
            get { return _sites ?? (_sites = GetSites()); }
        }

        private static IEnumerable<XmlNode> GetSites()
        {
            var sitesRoot = Factory.GetConfigNode(PageUrls.ConfigSitePath);
            if (sitesRoot != null)
            {
                var childNodes = sitesRoot.ChildNodes;

                return childNodes.Cast<XmlNode>().Where<XmlNode>(
                      childNode =>
                      {
                          if (childNode.Attributes != null)
                          {
                              var attributeName = childNode.Attributes["name"].Value.ToLower();
                              return (attributeName == "monarchgroup" ||
                                      attributeName == "darlinghomes" ||
                                      attributeName == "website" ||
                          attributeName == "taylormorrison" ||
                                      attributeName == "mg" ||
                                      attributeName == "dh" ||
                                      attributeName == "tm");
                          }
                          return false;
                      });
            }

            return null;
        }

        public static CrossSiteSpecification GetCrossSiteSpecification(this Item item, UrlOptions options, Uri uri)
        {
            string specificUrl = item.GetItemUrl(options);
            string url = string.Format("{0}://{1}{2}", uri.Scheme, uri.Authority, specificUrl);

            string domain = Context.GetDeviceName();
            string hostName = uri.Authority;
            var currentDb = Context.Database.Name.ToLower();

            foreach (var site in Sites)
            {
                try
                {
                    XmlAttributeCollection siteAttributes = site.Attributes;
                    if (siteAttributes != null && siteAttributes["rootPath"].Value != null && specificUrl.Contains(siteAttributes["rootPath"].Value) && (null != siteAttributes["database"] && siteAttributes["database"].Value == currentDb))
                    {
                        url = string.Format("{0}://{1}", uri.Scheme,
                                            siteAttributes["hostName"].Value +
                                            specificUrl.Replace(siteAttributes["rootPath"].Value + "/home", string.Empty));
                        hostName = siteAttributes["hostName"].Value;
                        domain = siteAttributes["device"] != null ? siteAttributes["device"].Value : "defaultdevice";

                        break;
                    }
                }
                catch (NullReferenceException ex)
                {
                }
            }

            return new CrossSiteSpecification()
                {
                    SiteSpecificUrl = url,
                    Domain = domain.ToLower().GetDomainFromDeviceName(),
                    HostName = hostName,
                    CompanyName = domain.GetCurrentCompanyName(),
                    CompanyAbbreviation = domain.GetCurrentCompanyNameAbbreviation()
                };
        }

        /// <summary>
        /// Gets the (friendly) URL of an item
        /// </summary>
        public static string GetItemUrl(this Item item, Sitecore.Links.UrlOptions options)
        {
            return SCUtils.GetItemUrl(item);
        }

        public static string GetLinkFieldPath(this Field lnkField)
        {
            try
            {
                var lnk = (LinkField)lnkField;

                if (lnk == null) return string.Empty;

                string url = string.Empty;

                switch (lnk.LinkType)
                {
                    case "internal":
                        url = lnk.TargetItem.GetItemUrl();
                        break;

                    case "external":
                    case "mailto":
                    case "anchor":
                    case "javascript":
                        url = lnk.Url;
                        break;

                    case "media":
                        var media = new MediaItem(lnk.TargetItem);
                        url = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(media));
                        break;

                    case "":
                    default:
                        break;
                }

                return url;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetTextValue(this Field field, Item item)
        {
            return FieldRenderer.Render(item, field.Name);
        }

        #region Media Items

        public static string GetMediaUrl(this Item item)
        {
            //try as mediaitem
            try
            {
                var mediaItem = (MediaItem)item;
                return mediaItem.GetMediaUrl();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string GetMediaUrl(this MediaItem item)
        {
            if (item != null)
            {
                var src = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(item));
                return src;
            }

            return string.Empty;
        }

        public static List<string> GetMediaUrl(this MultilistField field, int index = -1)
        {
            var list = new List<string>();
            if (index == -1)
            {
                var items = field.GetItems();
                foreach (var item in items)
                {
                    list.Add(item.GetMediaUrl());
                }
            }
            else
            {
                if (field.Count > 0)
                {
                    var itemID = field.Items[index];
                    if (itemID.IsNotEmpty())
                    {
                        var item = Context.Database.GetItem(new ID(itemID));
                        list.Add(item.GetMediaUrl());
                    }
                }
            }

            return list;
        }

        public static string GetMediaUrl(this Field field)
        {
            try
            {
                var imageField = (ImageField)field;

                if (imageField != null && imageField.MediaItem != null)
                {
                    var src = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(imageField.MediaItem));
                    return src;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetValue(this Field field, string returnWhenNull)
        {
            return field.GetValue(() => returnWhenNull);
        }

        public static string GetValue(this Field field, Func<string> returnWhenNullCallBack)
        {
            if (field != null && field.HasValue)
                return field.Value;

            return returnWhenNullCallBack();
        }

        // if you have the full URL with protocol and host
        public static Item GetItemFromUrl(this string rawUrl)
        {
            string path = new Uri(rawUrl).AbsolutePath;
            return GetItemFromPath(path);
        }

        // if you have just the path after the hostname
        public static Item GetItemFromPath(string path)
        {
            // remove query string
            if (path.Contains("?"))
                path = path.Split('?')[0];

            path = path.Replace(".aspx", "").Replace('-', ' ');
            try
            {
                return Sitecore.Context.Database.GetItem(path);
            }
            catch (NullReferenceException)
            {
            }
            return null;
        }

        public static Item GetItemFromPathNoFormat(string path)
        {
            try
            {
                return Sitecore.Context.Database.GetItem(path);
            }
            catch (NullReferenceException)
            {
            }
            return null;
        }

        public static Item GetItemFromID(this ID idOfItem)
        {
            return Sitecore.Context.Database.GetItem(idOfItem);
        }
        #endregion Media Items
    }
}