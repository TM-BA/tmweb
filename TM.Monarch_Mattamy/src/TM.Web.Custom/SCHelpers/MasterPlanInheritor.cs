﻿using System.Collections.Generic;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Pipelines;

namespace TM.Web.Custom.SCHelpers
{
    public class MasterPlanInheritor
    {
        private readonly ID[] _attributeFields;
        private readonly Item _planThatInheritsMasterPlan;
        private Item _currentMasterPlan;
        private readonly ID[] _mediaFields;
        private bool _homeForSaleCreated;
        private Item _inventoryThatInheritsPlan;

        public MasterPlanInheritor(Item planThatInheritsMasterPlanOrHomeForSaleThatInheritsPlan, bool homeForSaleCreated = false)
        {
            if (homeForSaleCreated)
            {
                _planThatInheritsMasterPlan = planThatInheritsMasterPlanOrHomeForSaleThatInheritsPlan.Parent;
                _inventoryThatInheritsPlan = planThatInheritsMasterPlanOrHomeForSaleThatInheritsPlan;
            }
            else
            {
                _planThatInheritsMasterPlan = planThatInheritsMasterPlanOrHomeForSaleThatInheritsPlan;
            }

            _homeForSaleCreated = homeForSaleCreated;

            _attributeFields = new[]
                                   {
                                       SCIDs.PlanBaseFields.NumberOfStories,
                                       SCIDs.PlanBaseFields.NumberOfBedrooms,
                                       SCIDs.PlanBaseFields.NumberOfBathrooms,
                                       SCIDs.PlanBaseFields.NumberOfHalfBathrooms,
                                       SCIDs.PlanBaseFields.NumderOfGarages,
                                       SCIDs.PlanBaseFields.SquareFootage,
                                       SCIDs.PlanBaseFields.PlanDescription,
                                       SCIDs.PlanBaseFields.GarageEntryLocation,
				       SCIDs.PlanBaseFields.MasterBedroomLocation,
                                       SCIDs.PlanBaseFields.NumberOfDens,
                                       SCIDs.PlanBaseFields.NumberOfSolariums,
                                       SCIDs.PlanBaseFields.NumberOfBonusRooms,
                                       SCIDs.PlanBaseFields.NumberOfFamilyRooms,
                                       SCIDs.PlanBaseFields.NumberOfLivingAreas,
                                       SCIDs.PlanBaseFields.NumberOfDiningAreas,
                                       SCIDs.PlanBaseFields.HasaLivingRoom,
                                       SCIDs.PlanBaseFields.HasaDiningRoom,
                                       SCIDs.PlanBaseFields.HasaSunRoom,
                                       SCIDs.PlanBaseFields.HasaStudy,
                                       SCIDs.PlanBaseFields.HasaLoft,
                                       SCIDs.PlanBaseFields.HasanOffice,
                                       SCIDs.PlanBaseFields.HasaGameRoom,
                                       SCIDs.PlanBaseFields.HasaMediaRoom,
                                       SCIDs.PlanBaseFields.HasaGuestBedroom,
                                       SCIDs.PlanBaseFields.HasaBonusRoom,
                                       SCIDs.PlanBaseFields.HasaBasement,
                                       SCIDs.PlanBaseFields.HasaFireplace,
                                       SCIDs.PlanBaseFields.HasaMasterBedroom,
                                       SCIDs.PlanBaseFields.HasaFamilyRoom
				       
                                   };
            _mediaFields = new[]
                                      {
                                          SCIDs.PlanBaseFields.Interior,
                                          SCIDs.PlanBaseFields.FloorPlan,
                                           SCIDs.PlanBaseFields.VirtualToursMasterPlanID,
		                          SCIDs.PlanBaseFields.VirtualTourStatus,
					  SCIDs.PlanBaseFields.PlanBrochureMasterID,
		                          SCIDs.PlanBaseFields.PlanBrochureStatus
                                      };
        }

        public void Process(Database masterDB)
        {
            Field masterPlanIDField = _planThatInheritsMasterPlan.Fields["Master Plan ID"];

            if (masterPlanIDField != null)
            {
                var DB = _planThatInheritsMasterPlan.Database;
                var existingMasterPlanID = DB
                          .GetItem(_planThatInheritsMasterPlan.ID)
                          .Fields["Master Plan ID"]
                          .Value;

                _currentMasterPlan = DB.GetItem(masterPlanIDField.Value);

                if (_currentMasterPlan != null)
                {
                    var currentMasterPlanID = _currentMasterPlan.ID.ToString();

                    var masterPlanUpdated = existingMasterPlanID != currentMasterPlanID;

                    //home for sale created. inherit from its plan
                    if (_homeForSaleCreated)
                    {
                        SetInventoryFieldsFromPlan(_inventoryThatInheritsPlan, true);
                    }
                    //if master plan changed
                    else if (_currentMasterPlan != null && masterPlanUpdated)
                    {

                        SetPlanFieldsFromMaster();


                        SetAllInventoryFieldsFromPlan(true);
                    }
                    //only plan was updated no change in master plan
                    else
                    {
                        //removed per fb case: 62401 http://bits.builderhomesite.com/production/default.asp?62401#564036
                        // SetAllInventoryFieldsFromPlan(false);
                    }

                }

            }

        }

        private void SetPlanFieldsFromMaster()
        {
            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, _attributeFields);
            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, "Plan Name");
            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, "Image Source");
            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, "Plan Brochure Master ID");
            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, "Virtual Tours Master Plan ID");

            var activeStatus = SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString();

            if (_planThatInheritsMasterPlan.Fields["Plan Status"].HasValue && _planThatInheritsMasterPlan.Fields["Plan Status"].Value == activeStatus)
            {
                SetFieldValue(_planThatInheritsMasterPlan, SCIDs.PlanBaseFields.StatusForSearch, activeStatus);
            }
            //clear media fields add from master.
            var mediaFields = new[]  {
                                          SCIDs.PlanBaseFields.Interior,
                                          SCIDs.PlanBaseFields.FloorPlan,
                                          SCIDs.PlanBaseFields.ElevationImages
		                       
		                  
                                      };

            InheritFields(_currentMasterPlan, _planThatInheritsMasterPlan, mediaFields);
            var mastePlanVirtualTour = _currentMasterPlan.Fields["Virtual Tours"];
            if (mastePlanVirtualTour != null && mastePlanVirtualTour.HasValue)
            {
                var virtualTour = Context.ContentDatabase.GetItem(mastePlanVirtualTour.Value);
                var virtualTourName = virtualTour.DisplayName;
                SetFieldValue(_planThatInheritsMasterPlan, SCIDs.PlanBaseFields.VirtualToursMasterPlanID, virtualTourName);


            }


        }

        private void SetAllInventoryFieldsFromPlan(bool masterPlanUpdatedOrHomeForSaleCreated)
        {
            Item[] inventoryHomes = _planThatInheritsMasterPlan.GetChildren().ToArray();
            foreach (Item inventoryHome in inventoryHomes)
            {
                SetInventoryFieldsFromPlan(inventoryHome, masterPlanUpdatedOrHomeForSaleCreated);
            }
        }

        private void SetInventoryFieldsFromPlan(Item inventoryHome, bool masterPlanUpdatedOrHomeForSaleCreated)
        {
            if (masterPlanUpdatedOrHomeForSaleCreated)
            {
                InheritFields(_planThatInheritsMasterPlan, inventoryHome, "Master Plan ID");
                InheritFields(_planThatInheritsMasterPlan, inventoryHome, "Image Source");
                InheritFields(_planThatInheritsMasterPlan, inventoryHome, "Plan Brochure Master ID");
                InheritFields(_planThatInheritsMasterPlan, inventoryHome, "Virtual Tours Master Plan ID");
                InheritFields(_planThatInheritsMasterPlan, inventoryHome, _attributeFields);
            }

            InheritFields(_planThatInheritsMasterPlan, inventoryHome, _mediaFields);
        }


        private void InheritFields(Item fromItem, Item toItem, IEnumerable<ID> fields)
        {
            using (new EventDisabler())
            using (new SecurityDisabler())
            {
                using (new EditContext(toItem))
                {
                    foreach (ID field in fields)
                    {

                        SCItemEventHandler.InProcess.Add(toItem.ID);
                        toItem.Fields[field].Value = fromItem.Fields[field].Value;
                        SCItemEventHandler.InProcess.Remove(toItem.ID);
                    }
                   
                }
            }
        }


        private void InheritFields(Item fromItem, Item toItem, string field)
        {
            if (toItem.Fields[field] == null || fromItem.Fields[field] == null)
                return;

            using (new SecurityDisabler())
            {
                using (new EventDisabler())
                using (new EditContext(toItem))
                {
                    SCItemEventHandler.InProcess.Add(toItem.ID);
                    toItem.Fields[field].Value = fromItem.Fields[field].Value;
                }

                SCItemEventHandler.InProcess.Remove(toItem.ID);
            }
        }

        private void SetFieldValue(Item toItem, ID fieldName, string fieldValue)
        {
            if (toItem == null || toItem.Fields[fieldName] == null)
                return;

            using (new SecurityDisabler())
            {
                using (new EventDisabler())
                using (new EditContext(toItem))
                {
                    SCItemEventHandler.InProcess.Add(toItem.ID);
                    toItem.Fields[fieldName].Value = fieldValue;
                }

                SCItemEventHandler.InProcess.Remove(toItem.ID);
            }
        }
    }
}