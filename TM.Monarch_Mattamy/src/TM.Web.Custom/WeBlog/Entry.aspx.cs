﻿using Sitecore.Modules.WeBlog.Layouts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WeBlog
{
    public class Entry : BlogEntry
    {
        protected new void Page_Load(object sender, EventArgs e)
        {
            var item = Sitecore.Context.Item;
            if (item != null)
            {
                var title = item["Blog Title"];
                if (!String.IsNullOrEmpty(title))
                {
                    Page.Title = title == "$name" ? item.Name : title;
                }
            }
        }
    }
}