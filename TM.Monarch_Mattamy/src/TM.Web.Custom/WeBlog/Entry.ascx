﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Entry.ascx.cs" Inherits="TM.Web.Custom.WeBlog.Entry" %>
<%@ Register TagPrefix="gl" Namespace="Sitecore.Modules.WeBlog.Globalization" Assembly="Sitecore.Modules.WeBlog" %>

    <sc:placeholder ID="phBlogMenu" key="phBlogMenu" runat="server" />

    <h1><sc:Text ID="txtTitle" Field="Title" runat="server" /></h1>
    <p><sc:Text ID="txtContent" Field="Content" runat="server" /></p>

    <sc:Placeholder ID="Placeholder1" runat="server" key="phBlogTags" />

   <%-- <p><%=Sitecore.Modules.WeBlog.Globalization.Translator.Format("ENTRY_DETAILS", CurrentEntry.Created, CurrentEntry.CreatedBy.LocalName) %></p>
    

    <% if (ShowEntryIntroduction == CHECKBOX_TRUE) { %>
        <sc:Placeholder runat="server" key="phBlogBelowEntryTitle" />
    <% if(DoesFieldRequireWrapping("Introduction")) { %>
    <p>
    <% } %>
        <sc:Text ID="txtIntroduction" Field="Introduction" runat="server" />
    <% if(DoesFieldRequireWrapping("Introduction")) { %>
    </p>
    <% } %>
    <% } %>

    <% if(DoesFieldRequireWrapping("Content")) { %>
    <p>
    <% } %>
    
    <% if(DoesFieldRequireWrapping("Content")) { %>
    </p>
    <% } %>

     --%>
<%--
    <div class="blog-lt2">
        <sc:Placeholder ID="Placeholder2" runat="server" key="phBlogCommentList" /> 
    </div>

    <sc:Placeholder ID="Placeholder3" runat="server" key="phBlogSubmitComment" /> 

    </div>--%>
