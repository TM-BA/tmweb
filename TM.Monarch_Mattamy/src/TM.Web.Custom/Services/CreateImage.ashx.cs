﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Services
{
	/// <summary>
	/// Summary description for CreateImage
	/// </summary>
	public class CreateImage : IHttpHandler
	{
		//http://dev.tm.com/services/createimage.ashx?text=999&type=nowselling&domain=tm"
		public void ProcessRequest(HttpContext context)
		{
			var text = context.Request.QueryString["text"];
			var domain = context.Request.QueryString["domain"];
			var type = context.Request.QueryString["type"];

			var imageHelper = new ImageHelper();
			var imgPath = imageHelper.DrawText(text, domain, type); 
			context.Response.Redirect(imgPath);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}



		//private string DrawText(String text, string domain, string type)
		//{
		//    var fileName = type + text + ".png";
		//    const string webPath = "/images/mappins";
		//    var imgPath = webPath + "/" + fileName;
			
		//    if (!File.Exists(HttpContext.Current.Server.MapPath(imgPath)))
		//    {

		//        var textAlign = (text.Length == 1 ? 2 : (text.Length == 2 ? 1 : -2));
		//        var fontSize = textAlign == 3 ? 11 : 12;
		//        Font font = new Font("Arial", fontSize, FontStyle.Bold, GraphicsUnit.Pixel);
		//        Color textColor = Color.White;

		//        //first, create a dummy bitmap just to get a graphics object
		//        Image img = new Bitmap(1, 1);
		//        Graphics drawing = Graphics.FromImage(img);
		//        drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

		//        //free up the dummy image and old graphics object
		//        img.Dispose();
		//        drawing.Dispose();

		//        img = new Bitmap(26, 35);

		//        drawing = Graphics.FromImage(img);

		//        //create a brush for the text
		//        Brush textBrush = new SolidBrush(textColor);

		//        var backImageFilePath =
		//            HttpContext.Current.Server.MapPath(string.Format("/Images/mappins/{0}/{1}.png", domain, type));
		//        drawing.DrawImage(new Bitmap(backImageFilePath), new Rectangle(-1, -1, 26, 35));

		//        drawing.DrawString(text, font, textBrush, textAlign, 2);
		//        drawing.Save();

		//        textBrush.Dispose();
		//        drawing.Dispose();

		//        var path = HttpContext.Current.Server.MapPath(webPath);
		//        fileName = type + text + ".png";
		//        var fullPath = System.IO.Path.Combine(path, fileName);
		//        img.Save(fullPath, ImageFormat.Png);

		//        imgPath = webPath + "/" + fileName;
		//    }
		//    return imgPath;
		//}
	}
}