﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class HomeBuying101Events
    {
        public static GAEvent HomebuyingSearch = new GAEvent("Homebuying", "Click", "HomebuyingSearch");
        public static GAEvent HomebuyingGlossary = new GAEvent("Homebuying", "Click", "HomebuyingGlossary");
        public static GAEvent HomebuyingRequestInfo = new GAEvent("Homebuying", "Click", "HomebuyingRequestInfo");

        public static GAEvent HomebuyingRequestResourceVideos = new GAEvent("Homebuying", "Click",
                                                                            "HomebuyingRequestResourceVideos");

        public static GAEvent HomeBuying101 = new GAEvent("HomeBuying", "", "Click	HomeBuying101");
        public static GAEvent HomeBuying102 = new GAEvent("Homebuying", "Click", "HomeBuying102");

        public static GAEvent HomebuyingRequestInfoSubmit = new GAEvent("Homebuying", "Click",
                                                                        "HomebuyingRequestInfoSubmit");
    }
}