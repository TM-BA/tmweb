﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Reflection;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Constants.GoogleEventing
{

    public class GAEvent
    {
        public string Category;
        public string Action;
        public string Label;

        public GAEvent(string category, string action, string label)
        {
            Category = category;
            Action = action;
            Label = label;
        }

        public override string ToString()
        {
           return "TM.Common.GAlogevent('{0}','{1}','{2}')".FormatWith(Category, Action, Label);
        }
    }
}