﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class SearchResultsEvents
    {
        public static GAEvent AdvSearchClose = new GAEvent("Search", "Click", "AdvSearchClose");
        public static GAEvent AdvSearch_StartingPrice = new GAEvent("Search", "Click", "AdvSearch_StartingPrice");
        public static GAEvent AdvSearch_City = new GAEvent("Search", "Click", "AdvSearch_City");
        public static GAEvent AdvSearch_Beds = new GAEvent("Search", "Click", "AdvSearch_Beds");
        public static GAEvent AdvSearch_Baths = new GAEvent("Search", "Click", "AdvSearch_Baths");
        public static GAEvent AdvSearch_Garages = new GAEvent("Search", "Click", "AdvSearch_Garages");
        public static GAEvent AdvSearch_Stories = new GAEvent("Search", "Click", "AdvSearch_Stories");
        public static GAEvent AdvSearch_BonusRoom = new GAEvent("Search", "Click", "AdvSearch_BonusRoom");
        public static GAEvent AdvSearch_SqFt = new GAEvent("Search", "Click", "AdvSearch_SqFt");
        public static GAEvent AdvSearch_SchoolDistrict = new GAEvent("Search", "Click", "AdvSearch_SchoolDistrict");
        public static GAEvent AdvSearch_CommunityStatus = new GAEvent("Search", "Click", "AdvSearch_CommunityStatus");
        public static GAEvent AdvSearch_Availability = new GAEvent("Search", "Click", "AdvSearch_Availability");
        public static GAEvent SortByDivision = new GAEvent("Sort", "Click", "SortByDivision");
        public static GAEvent HomeAddressLink = new GAEvent("Details", "Click", "HomeAddressLink");
        public static GAEvent CommunityCard_AddtoFavorites = new GAEvent("Favorites", "Click", "CommunityCard_AddtoFavorites");
        public static GAEvent CommunityCard_CommunityName = new GAEvent("Details", "Click", "CommunityCard_CommunityName");
        public static GAEvent CommunityCard_HomesForSale = new GAEvent("Details", "Click", "CommunityCard_HomesForSale");
        public static GAEvent CommunityCard_SitePlan = new GAEvent("Details", "Click", "CommunityCard_SitePlan");
        public static GAEvent CommunityCard_AvailablePlans = new GAEvent("Details", "Click", "CommunityCard_AvailablePlans");
        public static GAEvent CommunityCard_AllHomesForSale = new GAEvent("Details", "Click", "CommunityCard_AllHomesForSale");
        public static GAEvent CommunityCard_PromotionDetails = new GAEvent("Details", "Click", "CommunityCard_PromotionDetails");
    }
}