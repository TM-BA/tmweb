﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class PPCLandingPageEvents
    {
        public static GAEvent SignUpForUpdatesButton = new GAEvent("ContactUs", "Click", "SignUpForUpdatesButton");
        public static GAEvent StartLiveChat = new GAEvent("ContactUs", "Click", "StartLiveChat");
        public static GAEvent CommunityLink = new GAEvent("Details", "Click", "CommunityLink");
        public static GAEvent AdditionalDetails = new GAEvent("Details", "Click", "AdditionalDetails");

    }
}