﻿using Sitecore.Data;

namespace TM.Web.Custom.Constants
{
    public class SCFieldNames
    {
        public const string LiveChatSkillCode = "Live Chat Skill";
        public const string InternetHomeConsultant = "IHC Card";

        //Home Page/Company Fields
        public const string BlogRssURL = "Blog Rss URL";

        public const string Canada = "Canada";
        public const string Condos = "Condos";
        public const string SingleFamily = "Single Family";

        public const string TaylorMorrison = "Taylor Morrison";
        public const string MonarchGroup = "Monarch Group";
        public const string DarlingHomes = "Darling Homes";

        public const string NewHomes = "new-homes";
        public const string NewCondos = "new-condos";


        public class NavigationItemFields
        {
            public const string DisplayText = "Display Text";
            public const string Link = "Link";
        }

        public class CommunityFields
        {
            public static string StatusforAssistance = "Status for Assistance";
            public static string CommunityLegacyId = "Community Legacy ID";
            public const string SlideShowImage = "SlideShow Image";
            public const string CommunityStatus = "Community Status";
            public const string StatusforSearch= "Status for Search";

        }

        public class DivisionFields
        {
            public static string LegacyDivisionID = "Legacy Division ID";

        }
        public class HomePageFields
        {
            public static string TabsDisplayInSeconds = "Tab Display in seconds";
            public static string YoutubeUrl = "YouTube URL";
            public static string FacebookUrl = "Facebook URL";
            public static string TwitterUrl = "Twitter URL";
            public static string PinterestUrl = "Pinterest URL";
            public static string GooglePlusUrl = "GooglePlus URL";
            public static string BlogUrl = "Blog URL";
        }

        public class UserTexts
        {
            public static string NewAccount = "Welcome to ";
            public static string PasswordReset = "Reset your password.";

            public static string PasswordRecoveryEmail =
                "Please check your email and follow the instruction to reset your password.";

            public static string InvalidPasswordRecovery = "Password recovery URL might have expired or invalid.";
            public static string PasswordResetConfirmation = "Your password is successfully updated.";
            public static string UserNotExist = "User does not exist.";
            public static string UserNotExistBig = "We did not find a user with this provided email. Please try again.";
            public static string EnterValidData = "Please provide a valid information.";
            public static string Updated = "Your changes have been successfully updated.";
        }

        public class PlanPageFields
        {
            public static string ElevationImages = "Elevation Images";
            public static string FloorPlan = "Floor Plan";
            public static string IsModelHome = "Is a Model Home";
            public static string MasterPlanID = "Master Plan ID";
            public static string PlanStatus = "Plan Status";
            public static string PricedFromValue = "Priced from Value";
            public static string PlanLegacyID = "Plan Legacy ID";
        }
	public class HomeForSaleFields
	{
	    public static string HomeForSaleStatus = "Home for Sale Status";
        public static string HomeForSaleLegacyId = "Home for Sale Legacy ID";
	    public static string DisplayAsAvailableHomeInRWE = "Display as Available Home in RWE";
        public static string Active = "Home for Sale Status";
        public static string ActiveForSearch = "Status for Search";
        //Sanjeevi
        public static string StatusforAvailability = "Status for Availability";


	}

	public class PlanBaseFields
	{
        public static string NumberOfBedrooms = "Number of Bedrooms";
        public static string NumberOfBathrooms = "Number of Bathrooms";
	   
	}
    }
}
