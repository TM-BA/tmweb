﻿using System.Collections.Generic;

namespace TM.Web.Custom.Constants
{
	public class CommonValues
	{
        public const string TaylorMorrisonDomain = "taylormorrison";
        public const string MonarchGroupDomain = "monarchgroup";
        public const string DarlingHomesDomain = "darlinghomes";
	    public const string Skyestone = "skyestone";
	}

    public class GoogleAnalytics
    {
        public const string TaylorMorrisonAccountNumber = "UA-3623926-1";
        public const string MonarchGroupAccountNumber = "UA-3623926-3";
        public const string DarlingHomesAccountNumber = "UA-2714508-1";
        public const string SkyestoneAccountNumber = "UA-37702556-1";
    }
}