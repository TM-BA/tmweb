﻿<%@ control language="C#" autoeventwireup="true" codebehind="DarlingHomesSplashScreen.ascx.cs" inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.DarlingHomesSplashScreen" %>
<link rel="stylesheet" href="/Styles/animate.min.css" />
<script src="/javascript/animate-modal/animatedModal.min.js"></script>



<!--DEMO02-->

<% if (CookieValue == null) {
%>
<!--Call your modal-->
<%--<ul>
    <li><a id="demo02" href="#modal-02">DEMO02</a></li>
</ul>--%>
<div id="modal-02" style="z-index: 9999; position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; overflow-y: hidden; opacity: 1;" class="animated">
    <div class="modal-content" style="width: 100%; height: 100%;">
        <div class="progress loading-iframe">
            <div>Loading...</div>
        </div>
        <iframe src="<%= iFrameURL %>" style="width: 100%; height: 100%; opacity: 0;" class="animated" id="iframe-content"></iframe>
    </div>
</div>


<script>

    var animationIn = "fadeIn";
    var animationOut = "fadeOut";
    function closeDHSplashWithCookie() {
        closeDHSplash();
        createCookie('<%= CookieName %>', true);
    }

    function closeDHSplash() {
        $j('#modal-02').addClass(animationOut);
        $j('html').css({ 'overflow': 'auto' });
        $j('body').css({ 'overflow': 'auto !important' });
    }

    function showDHSplash() {
        $j('#modal-02').removeClass(animationOut);
        $j('#modal-02').addClass(animationIn);
    }

    function QuitModalColor() {
        $j('#modal-02').addClass('transparent');
    }

    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

    $j('document').ready(function () {
        $j('html').css({ 'overflow': 'hidden' });
        $j('body').css({ 'overflow': 'hidden !important' });
        /*//demo 02
        $j("#demo02").animatedModal({
            modalTarget: 'modal-02',
            animatedIn: 'fadeIn',
            animatedOut: 'fadeOut',
            color: '#3498db',
            // Callbacks
            beforeOpen: function () {
                $j('html').css({ 'overflow': 'hidden' });
                console.log("The animation was called");
            },
            afterOpen: function () {
                console.log("The animation is completed");
            },
            beforeClose: function () {
                $j('html').css({ 'overflow': 'auto' });
                console.log("The animation was called");
            },
            afterClose: function () {

                console.log("The animation is completed");
            }
        });*/

    });

    $j('#iframe-content').on('load', function () {
        $j(".loading-iframe").remove();
        $j(this).addClass(animationIn);
        setTimeout(function () {
            QuitModalColor();
        },1000);
    });
</script>
<%}
%>