﻿using System;
using Sitecore.Links;
using Sitecore.Modules.WeBlog.Items.WeBlog;
using Sitecore.Modules.WeBlog.Managers;

namespace Sitecore.Modules.WeBlog.Layouts
{
    public partial class CommentsListing : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var totalToShow = CurrentBlog.DisplayCommentSidebarCountNumeric;

            var comments = ManagerFactory.CommentManagerInstance.GetCommentsByBlog(CurrentBlog.ID, totalToShow);

            rptSteps.DataSource = comments;
            rptSteps.DataBind();
            
        }

        /// <summary>
        /// Get the URL of the blog entry a comment was made against
        /// </summary>
        /// <param name="comment">The comment to find the blog entry URL for</param>
        /// <returns>The URL if found, otherwise an empty string</returns>
        protected virtual string GetEntryUrlForComment(CommentItem comment)
        {
            if (comment != null)
                return LinkManager.GetItemUrl(ManagerFactory.EntryManagerInstance.GetBlogEntryByComment(comment).InnerItem);
            else
                return string.Empty;
        }
    }
}