﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código. 
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace Sitecore.Modules.WeBlog.Layouts {
    
    
    public partial class Blog {
        
        /// <summary>
        /// Control phBlogMain.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder phBlogMain;
        
        /// <summary>
        /// Control phBlogSidebar.
        /// </summary>
        /// <remarks>
        /// Campo generado automáticamente.
        /// Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder phBlogSidebar;
    }
}
