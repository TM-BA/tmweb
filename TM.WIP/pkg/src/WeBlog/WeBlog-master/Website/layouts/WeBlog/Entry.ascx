﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogEntry.ascx.cs" Inherits="Sitecore.Modules.WeBlog.Layouts.BlogEntry" %>
<%@ Register TagPrefix="gl" Namespace="Sitecore.Modules.WeBlog.Globalization" Assembly="Sitecore.Modules.WeBlog" %>

    
    <% if (ShowEntryImage == CHECKBOX_TRUE) { %>
    <div class="blog-banner">
           <sc:Image runat="server" ID="EntryImage" Field="Image"/>
    </div>
    <!--end blog-lt-->    
    <% } %>

    <div class="blog-lt">
    

    <% if (ShowEntryTitle == CHECKBOX_TRUE) { %>
        <h1><sc:Text ID="txtTitle" Field="Title" runat="server" /></h1>
    <% } %>
    <% if (ShowEntryMetadata == CHECKBOX_TRUE) { %>
        <p><%=Sitecore.Modules.WeBlog.Globalization.Translator.Format("ENTRY_DETAILS", CurrentEntry.Created, CurrentEntry.CreatedBy.LocalName) %></p>
    <% } %>

    <% if (ShowEntryIntroduction == CHECKBOX_TRUE) { %>

        <sc:Placeholder runat="server" key="phBlogBelowEntryTitle" />
    
    <% if(DoesFieldRequireWrapping("Introduction")) { %>
    <p>
    <% } %>
        <sc:Text ID="txtIntroduction" Field="Introduction" runat="server" />
    <% if(DoesFieldRequireWrapping("Introduction")) { %>
    </p>
    <% } %>
    <% } %>

    <% if(DoesFieldRequireWrapping("Content")) { %>
    <p>
    <% } %>
    <sc:Text ID="txtContent" Field="Content" runat="server" />
    <% if(DoesFieldRequireWrapping("Content")) { %>
    </p>
    <% } %>

    <sc:Placeholder ID="Placeholder1" runat="server" key="phBlogTags" /> 

    <div class="blog-lt2">
        <sc:Placeholder ID="Placeholder2" runat="server" key="phBlogCommentList" /> 
    </div>

    <sc:Placeholder ID="Placeholder3" runat="server" key="phBlogSubmitComment" /> 

    </div>
