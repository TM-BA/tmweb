﻿using System;
using System.Configuration;
using Sitecore.Links;
using TM.UrlRewriter;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.UrlRedirecting.Transforms
{
    public class GetCommunityUrlTransform : IRewriteTransform
    {
        public string ApplyTransform(string input)
        {
            var contentDB = TM.UrlRewriter.Utilities.RewriterContants.Constants.Get("db");
            var divisionItem =  new CommunityQueries(contentDB).GetCommunityFromLegacyId(input);
            return  UrlTransformUtilities.GetUrl(divisionItem);
        }

        public string Name
        {
            get { return "getcommunityurl"; }
        }
    }
}