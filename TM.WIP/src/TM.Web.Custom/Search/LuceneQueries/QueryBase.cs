﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class QueryBase
    {
        private string _ctxDB;

        public QueryBase()
            : this(Sitecore.Context.Database.Name)
        {
        }

        public QueryBase(string dbName)
        {
            _ctxDB = dbName.ToLowerInvariant();
        }

        public string IndexName {
            get
            {
                return string.Format("sitecore_{0}_index",  _ctxDB);
            }
        }
    }
}