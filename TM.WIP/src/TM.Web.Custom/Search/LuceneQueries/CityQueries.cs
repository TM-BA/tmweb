﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.DynamicFields;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class CityQueries : QueryBase
    {
        string _closedCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.Closed);
        string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true);
        string _inactiveCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.InActive);


        public IEnumerable<ID> GetAllActiveCitiesUnderDivision(ID divisionItemID)
        {
           

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                IQueryable<TMSearchResultItem> allActiveCommunities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage)
                    .Where(
                        i =>
                            i[SCFieldNames.CommunityFields.CommunityStatus] != _closedCommunity
                            && i[SCFieldNames.CommunityFields.CommunityStatus] != _inactiveCommunity
                            && i[SCFieldNames.CommunityFields.StatusforSearch] == _active
                            && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                            && i.Paths.Contains(divisionItemID)
                    );

                IEnumerable<ID> activeCities = allActiveCommunities.ToList()
                    .Select(c => c.ParentID)
                    .Distinct()
                    .Where(i => !i.IsNull);
                    


                return activeCities;
            }


            
        }

        public IEnumerable<TMSearchResultItem> GetAllActiveCommunitiesUnderDivision(ID divisionItemID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                IQueryable<TMSearchResultItem> allActiveCommunities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage)
                    .Where(
                        i =>
                            i[SCFieldNames.CommunityFields.CommunityStatus] != _inactiveCommunity
                            && i[SCFieldNames.CommunityFields.CommunityStatus] !=    _closedCommunity
                            && i[SCFieldNames.CommunityFields.StatusforSearch] == _active
                            && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                            && i.Paths.Contains(divisionItemID));

                return allActiveCommunities.ToList();
            }
        }


        public IEnumerable<TMSearchResultItem> GetAllCitiesUnderDivision(ID divisionItemID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                IQueryable<TMSearchResultItem> allCities = context.GetQueryable<TMSearchResultItem>()
                    .Where(
                        i =>
                            i.TemplateId == SCIDs.TemplateIds.CityPage
                            && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                            && i.Paths.Contains(divisionItemID));

                return allCities.ToList();
            }
        }
    }
}