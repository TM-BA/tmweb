﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using QueryBase = TM.Web.Custom.Queries.LuceneQueries.QueryBase;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class CompanyQueries:QueryBase
    {
        public IEnumerable<Item> GetBlogsUnderCompany(ID companyID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                return context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.Blog.TM.Entry
                                && i.Paths.Contains(companyID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").Select(i => i.GetItem());

            }
        }

        public List<Item> Search(string fullTextQuery, out int totalResults, ID companyID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var myTmNumberQuery =
                    PredicateBuilder.True<TMSearchResultItem>()
                        .And(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage)
                        .And(i => (i["mytm number"] == fullTextQuery).Boost(14f));

                var mlsidquery = PredicateBuilder.True<TMSearchResultItem>()
                    .And(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage)
                    .And(i => (i["mls number"] == fullTextQuery).Boost(12f));

                const string statusForSearchField = "status for search";

                var commonPredicate = PredicateBuilder.True<TMSearchResultItem>();

                commonPredicate = commonPredicate
                    .And(i => i[statusForSearchField] == IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true))
                    .And(i => i.Content.Like(fullTextQuery, 0.8f));

                var communityQuery = commonPredicate.And(i =>
                    (i.TemplateId == SCIDs.TemplateIds.CommunityPage).Boost(10f));

                /*Plan Page Query*/
                var planQuery = commonPredicate.And(i =>
                    (i.TemplateId == SCIDs.TemplateIds.PlanPage).Boost(8f));

                /*Home for sale query*/
                var homesForSaleQuery = commonPredicate.And(i =>
                    (i.TemplateId == SCIDs.TemplateIds.HomeForSalePage).Boost(6f));

                /*City QUERY */
                var cityQuery = commonPredicate.And(i =>
                   (i.TemplateId == SCIDs.TemplateIds.CityPage).Boost(4f));

                /*Division Page Query*/
                var divQuery = commonPredicate.And(i =>
                    (i.TemplateId == SCIDs.TemplateIds.DivisionPage).Boost(2f));

                var standardContentTemplateID = SCIDs.TemplateIds.StandardContent.ToShortID().ToString();
                var contentPagesQuery =
                    PredicateBuilder.True<TMSearchResultItem>()
                        .And(i => i.AllTemplates.Contains(standardContentTemplateID))
                        .And(i => i["campaign status"] == IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true))
                        .And(i => i.Content.Like(fullTextQuery, 0.8f));

                var promoBaseTemplateID = SCIDs.TemplateIds.PromoBase.ToShortID().ToString();
                var promoPagesQuery = PredicateBuilder.True<TMSearchResultItem>()
                    .And(i => i.AllTemplates.Contains(promoBaseTemplateID))
                    .And(i => i.PromoEndDate <= DateTime.Now)
                    .And(i => i.Content.Like(fullTextQuery, 0.8f));

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(myTmNumberQuery)
                    .Or(mlsidquery)
                    .Or(communityQuery)
                    .Or(homesForSaleQuery)
                    .Or(planQuery)
                    .Or(cityQuery)
                    .Or(divQuery)
                    .Or(contentPagesQuery)
                    .Or(promoPagesQuery);

                var salesCardQuery = PredicateBuilder.True<TMSearchResultItem>()
                    .And(i => i.TemplateId == SCIDs.TemplateIds.SalesTeamCard)
                    .And(i => i.Content.Like(fullTextQuery, 0.9f));
                
                var salesCards = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .Where(salesCardQuery)
                    .GetResults().Hits.Select(i => i.Document.ItemId.ToString());

                foreach (var card in salesCards)
                {
                    var cardquery = PredicateBuilder.True<TMSearchResultItem>()
                            .And(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage)
                            .And(i => i["Sales Team Card"].Contains(IdHelper.NormalizeGuid(card,true)));
                    combinedPredicate = combinedPredicate.Or(cardquery);
                }

                var results = context.GetQueryable<TMSearchResultItem>()
                     .Where(i => i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values" && i.Paths.Contains(companyID))
                     .Where(combinedPredicate).GetResults();

                totalResults = results.TotalSearchResults;
                return results.Hits.Take(25).Select(h => h.Document.GetItem()).ToList();
            }
        }
    }
}
