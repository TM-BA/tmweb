﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Enums;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using Sitecore.ContentSearch.Linq.Utilities;

namespace TM.Web.Custom.Search.LuceneQueries
{
    public class PlanQueries : QueryBase
    {
        private string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active,true);

        public PlanQueries(string dbName):base(dbName)
        {
            
        }

        public PlanQueries()
        {

        }

        public Item GetPlanFromLegacyId(string legacyId)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem plan = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage
                                && i[SCFieldNames.PlanBaseFields.PlanLegacyID] == IdHelper.NormalizeGuid(legacyId,true)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return plan == null ? null : plan.GetItem();
            }
        }

        public IEnumerable<ID> GetHomesForSaleForRWEInDivision(ID divisionID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var homeForSaleForRWE = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.DisplayAsAvailableHomeInRWE] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == _active
                                && i[SCFieldNames.HomeForSaleFields.StatusForSearch] == _active
                                && i.Paths.Contains(divisionID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (homeForSaleForRWE.TotalSearchResults == 0)
                {
                    homeForSaleForRWE = context.GetQueryable<TMSearchResultItem>()
                        .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                    && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == _active
                                    && i[SCFieldNames.HomeForSaleFields.StatusForSearch] == _active
                                    && i.Paths.Contains(divisionID)
                                    && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();
                }

                return homeForSaleForRWE.Hits.Select(h => h.Document.ItemId).ToList();
            }
        }

        public Item GetHomeForSaleFromLegacyId(string legacyId)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem homeForSale = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyID] == IdHelper.NormalizeGuid(legacyId,true)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return homeForSale == null ? null : homeForSale.GetItem();
            }
        }

        public HomeTourOption IsAvailableAsModelHome(Item plan, Item currentDivision, Item currentCommunity,
            out Item matchedInventory)
        {
            matchedInventory = null;
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var availableAsModelHome = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                &&
                                i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHome.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHome.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsModelHome;
                }

                return HomeTourOption.None;
            }
        }

        public HomeTourOption GetHomeTourOption(Item plan, Item currentDivision, Item currentCommunity,
            out Item nearByCommunity, out Item matchedInventory)
        {
            nearByCommunity = null;
            matchedInventory = null;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var availableAsModelHome = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] ==IdHelper.NormalizeGuid( plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHome.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHome.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsModelHome;
                }

                var communitiesWithin15Miles = SCUtils.CommunityWithinDistanceOf(currentCommunity, 15);

                communitiesWithin15Miles.Remove(currentCommunity.ID.ToString());

                var availableAsModelHomeInNearbyCommunity = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] ==IdHelper.NormalizeGuid( plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.PlanPageFields.IsModelHome] == "1"
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] ==_active
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsModelHomeInNearbyCommunity.TotalSearchResults > 0)
                {
                    var results = availableAsModelHomeInNearbyCommunity.Hits;
                    if (
                        results.Any(
                            r => r.Document.Paths.Any(id => communitiesWithin15Miles.Any(cid => IdHelper.NormalizeGuid(cid, true) == IdHelper.NormalizeGuid(id)))))
                    {
                        var modelHome = availableAsModelHomeInNearbyCommunity.Hits.FirstOrDefault();
                        matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                        nearByCommunity = matchedInventory == null ? null : matchedInventory.Parent.Parent;
                        return HomeTourOption.AvailableAsModelHomeInNearByCommunity;
                    }
                }

                var homeReadyNow = IdHelper.NormalizeGuid(SCIDs.HomesForSalesFields.ReadyNow);
                var availableAsInventory = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(plan[SCIDs.PlanBaseFields.MasterPlanID],true)
                                && i[SCFieldNames.HomeForSaleFields.StatusForAvailability] == homeReadyNow
                                && i.Paths.Contains(currentCommunity.ID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values").GetResults();

                if (availableAsInventory.TotalSearchResults > 0)
                {
                    var modelHome = availableAsModelHomeInNearbyCommunity.Hits.FirstOrDefault();
                    matchedInventory = modelHome == null ? null : modelHome.Document.GetItem();
                    return HomeTourOption.AvailableAsInventory;
                }
            }


            return HomeTourOption.None;
        }


        public IEnumerable<TMSearchResultItem> GetPlansInCommunityMatchingMasterPlan(ID communityID, string masterPlanID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var plansInCommunityMatchingMasterPlan = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage
                                && i[SCFieldNames.PlanPageFields.MasterPlanID] == IdHelper.NormalizeGuid(masterPlanID,true)
                                && i[SCFieldNames.PlanPageFields.PlanStatus] ==_active
                                && i.Paths.Contains(communityID)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return plansInCommunityMatchingMasterPlan.ToList();
            }
        }


        public IEnumerable<TMSearchResultItem> GetActiveHomesForSaleUnderPlan(ID plan)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var homeForSale = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                                && i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == _active
                                && i[SCFieldNames.HomeForSaleFields.StatusForSearch] == _active
                                && i.Paths.Contains(plan)
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values");

                return homeForSale.ToList();
            }
        }

        public List<Item> GetPlans(string divisionID, string communityID, string planID, string status, string homePath)
        {

            if(string.IsNullOrEmpty(divisionID) && string.IsNullOrEmpty(communityID) && string.IsNullOrEmpty(planID) && string.IsNullOrEmpty(status))
                return new List<Item>();

            var companyID = Sitecore.Context.Database.GetItem(homePath).ID;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var planQuery = PredicateBuilder.True<TMSearchResultItem>()
                    .And(i => i.TemplateId == SCIDs.TemplateIds.PlanPage);

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(planQuery);

                if (!string.IsNullOrEmpty(divisionID))
                {
                    Item division = new DivisionQueries().GetDivisionFromLegacyId(divisionID);

                    if (division != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(division.ID));
                    }
                }

                if (!string.IsNullOrEmpty(communityID))
                {
                    Item community = new CommunityQueries().GetCommunityFromLegacyId(communityID);
                   
                    if (community != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(community.ID));
                    }
                }

                if (!string.IsNullOrEmpty(planID))
                {
                    combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.PlanBaseFields.PlanLegacyID] == planID);
                }

                if (!string.IsNullOrEmpty(status))
                {
                    string idStatus = GetPlanStatus(status);
                    if (!string.IsNullOrEmpty(idStatus))
                    {
                        combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.PlanBaseFields.PlanStatus] == idStatus);
                    }
                    else if (string.IsNullOrEmpty(idStatus) && status.ToLower().Equals("all"))
                    {
                        //do nothing
                    }
                    else
                    {
                        return new List<Item>();
                    }
                        
                }

                var results = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                                && i.Paths.Contains(companyID))
                                .Where(combinedPredicate).GetResults();

                return results.Hits.Select(h => h.Document.GetItem()).ToList();
            }
        }

        private string GetPlanStatus(string planStatus)
        {
            string idStatus = null;

            switch (planStatus.ToLower())
            {
                case "all":
                    break;
                case "active":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active);
                    break;
                case "inactive":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.InActive);
                    break;
                default:
                    break;
            }

            return idStatus;
        }
    }
}