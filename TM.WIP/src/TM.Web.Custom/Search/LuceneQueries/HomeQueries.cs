﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Enums;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using Sitecore.ContentSearch.Linq.Utilities;
namespace TM.Web.Custom.Search.LuceneQueries
{
    public class HomeQueries : QueryBase
    {
        private string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true);

        public HomeQueries(string dbName):base(dbName)
        {

        }

        public HomeQueries()
        {

        }

        public List<Item> GetHomesForSale(string divisionId, string communityId, string planId, string specId, string status, string homePath)
        {

            if (string.IsNullOrEmpty(divisionId) && string.IsNullOrEmpty(communityId) && string.IsNullOrEmpty(planId) && string.IsNullOrEmpty(status) && string.IsNullOrEmpty(specId))
                return new List<Item>();


            var companyID = Sitecore.Context.Database.GetItem(homePath).ID;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {

                var homeQuery = PredicateBuilder.True<TMSearchResultItem>()
                        .And(i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage);

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(homeQuery);

                if (!string.IsNullOrEmpty(specId))
                {
                    combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyID] == specId);
                }

                if (!string.IsNullOrEmpty(divisionId))
                {
                    Item division = new DivisionQueries().GetDivisionFromLegacyId(divisionId);

                    if (division != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(division.ID));
                    }
                    else
                    {
                        return new List<Item>();
                    }
                }

                if (!string.IsNullOrEmpty(communityId))
                {
                    Item community = new CommunityQueries().GetCommunityFromLegacyId(communityId);

                    if (community != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(community.ID));
                    }
                    else
                    {
                        return new List<Item>();
                    }
                }

                if (!string.IsNullOrEmpty(planId))
                {
                    Item plan = new PlanQueries().GetPlanFromLegacyId(planId);

                    if (plan != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(plan.ID));
                    }
                    else
                    {
                        return new List<Item>();
                    }
                }

                if (!string.IsNullOrEmpty(status))
                {
                    string idStatus = GetSpecStatus(status);
                    if (!string.IsNullOrEmpty(idStatus))
                    {
                        combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == idStatus);
                    }
                    else if (string.IsNullOrEmpty(idStatus) && status.ToLower().Equals("all"))
                    {
                        //do nothing
                    }
                    else
                    {
                        return new List<Item>();
                    }

                }

                var results = context.GetQueryable<TMSearchResultItem>()
                     .Where(i => i[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == _active
                                //&& i[SCFieldNames.HomeForSaleFields.StatusForSearch] == _active
                                && i.IsLatestVersion 
                                && !i.IsTemplate 
                                && i.Language == "en"
                                && i.Paths.Contains(companyID)
                                && i.Name != "__Standard Values") 
                     .Where(combinedPredicate).GetResults();

                return results.Hits.Select(h => h.Document.GetItem()).ToList();
            }
        }

        private string GetSpecStatus(string specStatus)
        {
            string idStatus = null;

            switch (specStatus.ToLower())
            {
                case "all":
                    break;
                case "active":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active);
                    break;
                case "inactive":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.InActive);
                    break;
                default:
                    break;
            }

            return idStatus;
        }

    }//
}