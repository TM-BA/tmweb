﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.Search.DynamicFields;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.ContentSearch.Linq;


namespace TM.Web.Custom.Search.LuceneQueries
{
    public class CommunityQueries : QueryBase
    {

        string _closedCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.Closed);
        string _inactive = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.InActive, true);
        string _inactiveCommunity = IdHelper.NormalizeGuid(SCIDs.StatusIds.CommunityStatus.InActive);
        private string _active = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true);

        public CommunityQueries(string dbName)
            : base(dbName)
        {

        }

        public CommunityQueries()
        {

        }

        public IEnumerable<TMSearchResultItem> GetAllActiveCommunitesUnderCity(ID cityItemID)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allActiveCommunities = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage)
                    .Where(
                        i =>
                           i[SCFieldNames.CommunityFields.CommunityStatus] != _inactiveCommunity
                            && i[SCFieldNames.CommunityFields.CommunityStatus] != _closedCommunity
                            && i[SCFieldNames.CommunityFields.StatusforSearch] == _active
                            && i.IsLatestVersion
                            && !i.IsTemplate
                            && i.Language == "en"
                            && i.Name != "__Standard Values"
                            && i.Name != "$name"
                            && i.Paths.Contains(cityItemID));
                return allActiveCommunities.ToList();
            }
        }

        public IEnumerable<ID> GetCommunitiesMatchingMasterPlanInDivision(ID divisionID, string masterPlanID)
        {
            var masterPlan = IdHelper.NormalizeGuid(masterPlanID, true);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allPlansMatchingMasterPlanID = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.PlanPage)
                    .Where(
                        i => i[SCFieldNames.PlanPageFields.MasterPlanID] == masterPlan
                             && i[SCFieldNames.PlanPageFields.PlanStatus] == _active
                             && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                             && i.Paths.Contains(divisionID));

                return
                    allPlansMatchingMasterPlanID.Select(c => c.ParentID).ToList()
                        .Distinct();
            }
        }


        public IEnumerable<TMSearchResultItem> GetAllSearchableHomesForSaleUnderCommunity(ID community)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var allHFSUnderCommunity = context.GetQueryable<TMSearchResultItem>()
                    .Where(
                        i => i.TemplateId == SCIDs.TemplateIds.HomeForSalePage
                             && i["Home for Sale Status"] == _active
                             && i["Status for Search"] == _active
                             && i["Display on Homes for Sale Page"] == "1"
                             && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values"
                             && i.Paths.Contains(community));

                return allHFSUnderCommunity.ToList();
            }
        }


        public Item GetCommunityFromLegacyId(string legacyId)
        {
            legacyId = IdHelper.NormalizeGuid(legacyId, true);
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                TMSearchResultItem community = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage
                                && i[SCFieldNames.CommunityFields.CommunityLegacyId] == legacyId
                                && i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values")
                    .FirstOrDefault();

                return community == null ? null : community.GetItem();
            }
        }

        public List<Item> GetCommunities(string divisionID, string communityID, string communityStatus, string homePath)
        {
            var companyID = Sitecore.Context.Database.GetItem(homePath).ID;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var communityQuery = PredicateBuilder.True<TMSearchResultItem>()
                    .And(i => i.TemplateId == SCIDs.TemplateIds.CommunityPage);

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(communityQuery);

                if (!string.IsNullOrEmpty(communityID))
                {
                    combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.CommunityFields.CommunityLegacyId] == communityID);
                }

                if (!string.IsNullOrEmpty(divisionID))
                {
                    Item division = new DivisionQueries().GetDivisionFromLegacyId(divisionID);

                    if (division != null)
                    {
                        combinedPredicate = combinedPredicate.And(i => i.Paths.Contains(division.ID));
                    }
                    else
                    {
                        //if there is no results for the division, return an empty list
                        return new List<Item>();
                    }
                }

                if (!string.IsNullOrEmpty(communityStatus))
                {
                    string idStatus = getCommunityStatus(communityStatus);
                    if (!string.IsNullOrEmpty(idStatus))
                    {
                        combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.CommunityFields.CommunityStatus] == idStatus);
                    }
                    else
                    {
                        return new List<Item>();
                    }   
                }


                var results = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values" 
                                && i.Paths.Contains(companyID))
                                .Where(combinedPredicate).GetResults();

                return results.Hits.Select(h => h.Document.GetItem()).ToList();
            }
        }

        public List<Item> GetSalesTeam(string communityID, string homePath)
        {
            var companyID = Sitecore.Context.Database.GetItem(homePath).ID;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var salesTeamQuery = PredicateBuilder.True<TMSearchResultItem>()
                        .And(i => i.TemplateId == SCIDs.TemplateIds.SalesTeamCard);

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(salesTeamQuery);

                var results = context.GetQueryable<TMSearchResultItem>()
                     .Where(i => i.IsLatestVersion 
                        && !i.IsTemplate 
                        && i.Language == "en" 
                        && i.Name != "__Standard Values" 
                        && i.Paths.Contains(companyID))
                     .Where(combinedPredicate).GetResults();

                return results.Hits.Select(h => h.Document.GetItem()).ToList();
            }
        }

        private string getCommunityStatus(string communityStatus)
        {
            string idStatus = null;

            switch (communityStatus.ToLower())
            {
                case "closed":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.Closed);
                    break;
                case "closeout":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.Closeout);
                    break;   
                case "comingsoon":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.ComingSoon);
                    break;
                case "grandopening":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.GrandOpening);
                    break;
                case "open":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.Open);
                    break;
                case "inactive":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.InActive);
                    break;
                case "preselling":
                    idStatus = IdHelper.NormalizeGuid(SCIDs.CommunityFields.CommunityStatuses.PreSelling);
                    break;
                default:
                    idStatus = null;
                    break;
            }

            return  idStatus;
        }
    }
}