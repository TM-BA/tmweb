﻿using System;
using System.ComponentModel;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class TMSearchResultItem:SearchResultItem 
    {
        [IndexField(BuiltinFields.LatestVersion)]
        public bool IsLatestVersion { get; set; }
          
        [IndexField("istemplate")]
        public bool IsTemplate { get; set; }

        [IndexField(BuiltinFields.AllTemplates)]
        public string AllTemplates { get; set; }

        [TypeConverter(typeof(IndexFieldDateTimeValueConverter))]
        [IndexField("end date")]
        public DateTime PromoEndDate { get; set; }

        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        [IndexField("parentid")]
        public ID ParentID { get; set; }

        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        [IndexField("grandparentid")]
        public ID GrandParentID { get; set; }
       
    }

    public class TMSearchResultImage : TMSearchResultItem
    {
        public string Alt { get; set; }
        public string Extension { get; set; }
    }
}