﻿using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Web.Custom.Queries.LuceneQueries;

namespace TM.Web.Custom.Search.LuceneQueries
{

    public class ImageQueries : QueryBase
    {
        public List<Item> GetImagesByTemplates(List<ID> Ids)
        {
            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var items = context.GetQueryable<TMSearchResultImage>()
                    .Where(i => Ids.Contains(i.TemplateId))
                    .Select(i => i.GetItem())
                    .ToList();

                items.RemoveAll(item => item.Fields["Alt"] == null ||
                        item.Fields["Alt"].Value != "" ||
                        !String.IsNullOrEmpty(item.Fields["Alt"].ToString()));


                return items;
            }
        }
    }

}