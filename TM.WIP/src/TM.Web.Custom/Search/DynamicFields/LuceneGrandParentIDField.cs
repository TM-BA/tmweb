﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;


namespace TM.Web.Custom.Search.DynamicFields
{
    public class LuceneGrandParentIDField : IComputedIndexField
    {
        private string GetGrandParent(Item item)
        {
            Assert.ArgumentNotNull(item, "item");
            Item parent = item.Parent;
            if (parent != null)
             parent = parent.Parent;
            if (parent != null)
               return parent.ID.ToShortID().ToString();

            return null;
        }

        public object ComputeFieldValue(IIndexable indexable)
        {
            var indexItem = indexable as SitecoreIndexableItem;
            Assert.ArgumentNotNull(indexItem, "indexItem");
            var item = indexItem.Item;
            return GetGrandParent(item);
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}