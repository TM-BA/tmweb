﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.SCHelpers
{
    public static class HomeAttributeExtension
    {
        public static HomeAttributes GetHomeAttributes(this HomeAttributes homeAttributes)
        {
            var scContextItem = Sitecore.Context.Item;
            homeAttributes.IsCurrentItemAPlan = scContextItem.TemplateID == SCIDs.TemplateIds.PlanPage;
            var priceFieldID = homeAttributes.IsCurrentItemAPlan
                                   ? SCIDs.PlanFields.PricedFromValue
                                   : SCIDs.HomesForSalesFields.Price;
            var squareFootageFieldID = SCIDs.PlanFields.SquareFootage;


            var descriptionFieldID = homeAttributes.IsCurrentItemAPlan
                                         ? SCIDs.PlanFields.PlanDescription
                                         : SCIDs.HomesForSalesFields.PlanDescription;

            homeAttributes.Description = scContextItem.Fields[descriptionFieldID].Value;
	    if(homeAttributes.IsCurrentItemAPlan)
	    {
	        homeAttributes.PriceText = scContextItem.Fields[SCIDs.PlanFields.PricedFromText].Value;
	    }
            homeAttributes.Price = scContextItem.Fields[priceFieldID].Value.CastAs<int>(0);
            homeAttributes.SqFt = scContextItem.Fields[squareFootageFieldID].Value.CastAs<int>(0);
            homeAttributes.Bedrooms = scContextItem.Fields[SCIDs.PlanBaseFields.NumberOfBedrooms].Value.CastAs<int>(0);
            homeAttributes.Bathrooms = scContextItem.Fields[SCIDs.PlanBaseFields.NumberOfBathrooms].Value.CastAs<int>(0);
            homeAttributes.HalfBaths = scContextItem.Fields[SCIDs.PlanBaseFields.NumberOfHalfBathrooms].Value.CastAs<int>(0);
            homeAttributes.Garage = scContextItem.Fields[SCIDs.PlanBaseFields.NumderOfGarages].Value.CastAs<int>(0);
            homeAttributes.Stories = scContextItem.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value.CastAs<int>(0);


            return homeAttributes;

        }


    }
}