﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore;
using Sitecore.Data;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.WebControls;
using Convert = System.Convert;

namespace TM.Web.Custom.SCHelpers
{
    public class HomeForSaleSearchHelper
    {
        private readonly Cache _webCache = new Cache();
        private readonly SearchHelper _searchHelper = new SearchHelper();
        private static readonly CommonHelper CommonHelper = new CommonHelper();

        private Cache WebCache
        {
            get { return _webCache; }
        }

        private readonly TMSession _webSession = new TMSession();

        public TMSession WebSession
        {
            get { return _webSession; }
        }

        /// <summary>
        /// Hydrate Search Results by search
        /// </summary>
        /// <param name="searchLocation"></param>
        /// <param name="sortBy"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public HomeforSaleSearchResults HydrateHomeforSaleSearchResults(string searchLocation, string sortBy, Dictionary<string, string> filters, bool isRealtor, bool isClearSession)
        {
            if (isRealtor) //Home for sale from realtor
            {
                if (isClearSession)
                {
                    WebSession[SessionKeys.SearchFacetsKey(true)] = null;
                    WebSession.Save();
                }

                if (filters == null)
                {
                    filters = WebSession[SessionKeys.SearchFacetsKey(true)].CastAs<Dictionary<string, string>>();
                }
                else
                {
                    WebSession[SessionKeys.SearchFacetsKey(true)] = filters;
                    WebSession.Save();
                }

                if (filters != null)
                {
                    var searchUrl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SearchUrl.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SearchUrl.ToString()] : String.Empty;
                    if (searchLocation != searchUrl)
                        filters = null;
                }
            }
            else //Home for sale from inventory landing page
            {
                if (isClearSession)
                {
                    WebSession[SessionKeys.InventorySearchFacetsKey] = null;
                    WebSession.Save();
                }
                if (filters == null)
                {
                    filters = WebSession[SessionKeys.InventorySearchFacetsKey].CastAs<Dictionary<string, string>>();
                }
                else
                {
                    WebSession[SessionKeys.InventorySearchFacetsKey] = filters;
                    WebSession.Save();
                }

                if (filters != null)
                {
                    var searchUrl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SearchUrl.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SearchUrl.ToString()] : String.Empty;
                    if (searchLocation != searchUrl)
                        filters = null;
                }
            }

            var areaId = string.Empty;

            //if (searchLocation.Contains("comid=") && (areaId == "0" || areaId == ""))
            //{
            //    var comId = searchLocation.Replace("comid=", "");
            //    //filters = null;
            //    searchLocation =
            //        string.Format(
            //            "*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']/*[@@templateid='{4}' and @@id='{5}']",
            //            SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage, SCIDs.TemplateIds.CommunityPage, comId);

            //    return GetHomeforSaleBySearch(searchLocation, sortBy, filters, true, areaId);
            //}

            Guid newaid;
            if (Guid.TryParse(searchLocation, out newaid))
            {
                searchLocation = _searchHelper.GetStateDivisionPathById(newaid.ToString());

                //areaId = newaid.ToString().ToUpper();
                //if (!areaId.Contains("{") && !areaId.Contains("}"))
                //    areaId = "{" + areaId + "}";

                //searchLocation =
                //    string.Format(
                //        "*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}' and @@id='{3}']/*[@@templateid='{4}']",
                //        SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, newaid,
                //        SCIDs.TemplateIds.CityPage);
            }
            else
            {
                searchLocation = _searchHelper.GetUniqueSearchPath(searchLocation);

                if (filters != null)
                {
                    areaId = filters.ContainsKey(GlobalEnums.SearchFilterKeys.DivisionArea.ToString())
                                 ? filters[GlobalEnums.SearchFilterKeys.DivisionArea.ToString()]
                                 : areaId;

                    if (!string.IsNullOrWhiteSpace(areaId) && areaId != "0")
                    {
                        searchLocation = _searchHelper.GetStateDivisionPathById(areaId);
                        //searchLocation =
                        //    string.Format(
                        //        "*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}' and @@id='{3}']/*[@@templateid='{4}']",
                        //        SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage,
                        //        areaId, SCIDs.TemplateIds.CityPage);
                    }
                }
            }

            return GetHomeforSaleBySearch(searchLocation, sortBy, filters, false, areaId);
        }

        public HomeforSaleSearchResults GetHomeforSaleBySearch(string selectedPath, string sortBy, Dictionary<string, string> filters, bool isRealtor, string areaId)
        {
            var company = Sitecore.Context.GetSiteName().ToLower();
            var hfsSearchKey = CacheKeys.GetHomeforSaleSearchKey(selectedPath + company);
            var hInfos = WebCache.Get(hfsSearchKey, () => HytrateHomeforSale(selectedPath, isRealtor));

            var refinedInfos = filters != null ? RefinedHomeforSaleInfo(filters, hInfos) : DefaultRefinedHomeforSaleInfo(hInfos);

            return GetHfSResults(hInfos, refinedInfos, sortBy, selectedPath, filters, areaId, isRealtor);
        }

        public List<HomeForSaleInfo> HytrateHomeforSale(string selectedPath, bool isRealtor)
        {
            var items = isRealtor ? SearchHelper.GetHomefoSalesbyRealtor(selectedPath) : SearchHelper.GetHomefoSalesbyDivisonPath(selectedPath);
            //var items = SearchHelper.GetHomefoSalesbyDivisonPath(selectedPath);

            return _searchHelper.HomeForSales(items, string.Empty, true, isRealtor).OrderBy(x => x.HomeforSaleCommunityName).ThenBy(x => x.Price).ToList();
        }

        private List<HomeForSaleInfo> DefaultRefinedHomeforSaleInfo(IEnumerable<HomeForSaleInfo> hInfos)
        {
            var newhInfo = hInfos.ToList();

            if (newhInfo.Count > 0)
            {
                var minPrice = CommonHelper.RoundPrice(hInfos.Min(mi => mi.Price), true);
                var maxPrice = CommonHelper.RoundPrice(hInfos.Max(mi => mi.Price), false);

                var minSqft = hInfos.Min(mi => mi.SquareFootage);
                var maxSqft = hInfos.Max(mi => mi.SquareFootage);

                //filter by price
                var fltpri = newhInfo.Where(pr => pr.Price >= minPrice && pr.Price <= maxPrice).ToList();
                newhInfo = fltpri;

                //filter by sq ft
                var filterSqft = newhInfo.Where(pr => pr.SquareFootage >= minSqft && pr.SquareFootage <= maxSqft).ToList();
                newhInfo = filterSqft;
            }

            return newhInfo.Distinct().ToList();
        }

        private List<HomeForSaleInfo> RefinedHomeforSaleInfo(Dictionary<string, string> filters, IEnumerable<HomeForSaleInfo> hInfos)
        {
            //Filters
            var minPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingPrice.ToString()].StringToDouble() : 0;
            var maxPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingPrice.ToString()].StringToDouble() : 0;
            var minSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()].StringToInt32() : 0;
            var maxSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()].StringToInt32() : 0;
            var city = filters.ContainsKey(GlobalEnums.SearchFilterKeys.City.ToString()) ? filters[GlobalEnums.SearchFilterKeys.City.ToString()] : String.Empty;
            var schl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()] : String.Empty;
            var avai = filters.ContainsKey(GlobalEnums.SearchFilterKeys.Availability.ToString()) ? filters[GlobalEnums.SearchFilterKeys.Availability.ToString()] : String.Empty;
            var bonusDenSolar = filters.ContainsKey(GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()) ? filters[GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()] : String.Empty;

            var noofBeds = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()] : String.Empty;
            var beds = noofBeds.StringToList();

            var noofBaths = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()] : String.Empty;
            var baths = noofBaths.StringToList();

            var noofGarages = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()] : String.Empty;
            var garage = noofGarages.StringToList();

            var noofStories = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofStories.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofStories.ToString()] : String.Empty;
            var story = noofStories.StringToList();

            var newhInfo = hInfos.ToList();

            //filter by price
            var fltpri = newhInfo.Where(pr => pr.Price >= minPrice && pr.Price <= maxPrice).ToList();
            newhInfo = fltpri;

            //filter by city
            if (city != "0")
            {
                var fltcit = newhInfo.Where(ci => ci.City == city).ToList();
                newhInfo = fltcit;
            }

            //Beds
            if (beds.Count > 0)
            {
                var filterbeds = new List<HomeForSaleInfo>();
                foreach (var st in beds)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        filterbeds.AddRange(newhInfo.Where(pr => pr.NumberofBedrooms >= b).ToList());
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterbeds.AddRange(newhInfo.Where(pr => pr.NumberofBedrooms == b).ToList());
                    }
                }

                newhInfo = filterbeds;
            }

            //baths
            if (baths.Count > 0)
            {
                var filterbaths = new List<HomeForSaleInfo>();

                foreach (var st in baths)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        filterbaths.AddRange(newhInfo.Where(pr => pr.NumberofBathrooms == b).ToList());
                    }
                    else if (st.Contains("½"))
                    {
                        b = st.Replace("½", "").StringToInt32();
                        filterbaths.AddRange(newhInfo.Where(pr => pr.NumberofBathrooms == b && pr.NumberofHalfBathrooms > 0).ToList());
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterbaths.AddRange(newhInfo.Where(pr => pr.NumberofBathrooms == b).ToList());
                    }
                }

                newhInfo = filterbaths;
            }

            //garage
            if (garage.Count > 0)
            {
                var filtergarages = new List<HomeForSaleInfo>();
                foreach (var st in garage)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        filtergarages.AddRange(newhInfo.Where(pr => pr.NumberofGarages >= b).ToList());
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filtergarages.AddRange(newhInfo.Where(pr => pr.NumberofGarages == b).ToList());
                    }
                }

                newhInfo = filtergarages;
            }

            //story
            if (story.Count > 0)
            {
                var filterStories = new List<HomeForSaleInfo>();
                foreach (var st in story)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        filterStories.AddRange(newhInfo.Where(pr => pr.NumberofStories >= b).ToList());
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterStories.AddRange(newhInfo.Where(pr => pr.NumberofStories == b).ToList());
                    }
                }

                newhInfo = filterStories;
            }

            //bonusRooms
            if (bonusDenSolar == "B")
            {
                var filterBonusRooms = newhInfo.Where(ci => ci.NumberofBonusRooms > 0).ToList();
                newhInfo = filterBonusRooms;
            }

            //dens
            if (bonusDenSolar == "D")
            {
                var filterDens = newhInfo.Where(ci => ci.NumberofDens > 0).ToList();
                newhInfo = filterDens;
            }

            //solarium
            if (bonusDenSolar == "S")
            {
                var filterSolariumRange = newhInfo.Where(ci => ci.NumberofSolariums > 0).ToList();
                newhInfo = filterSolariumRange;
            }

            //filter by sq ft
            var filterSqft = newhInfo.Where(pr => pr.SquareFootage >= minSqft && pr.SquareFootage <= maxSqft).ToList();
            newhInfo = filterSqft;

            ////filter by school dst
            if (schl != "0")
            {
                var fltscl = newhInfo.Where(scl => scl.SchoolDistrict == schl.ToUpper()).ToList();
                newhInfo = fltscl;
            }

            if (avai != "0")
            {
                var avaiFilterList = newhInfo.Where(a => a.AvailabilityItemId == avai.ToUpper()).ToList();
                newhInfo = avaiFilterList;
            }

            //if (bnuden != "0")
            //    newcInfo = newcInfo.Where(scl => scl.SchoolDistrict == schl.ToUpper()).ToList();

            return newhInfo.Distinct().ToList();
        }

        private HomeforSaleSearchResults GetHfSResults(List<HomeForSaleInfo> hInfos, List<HomeForSaleInfo> refinedInfos, string sortBy, string selectedPath, Dictionary<string, string> filters, string areaId, bool isRealtor)
        {
            var srchFact = GetSearchFacet(hInfos, selectedPath, refinedInfos, filters, isRealtor);
            srchFact.Division = areaId;

            return new HomeforSaleSearchResults
                {
                    HomeForSaleInfo = filters == null ? HomeforSaleGroupByDomain(hInfos, sortBy).Distinct(new HomeForSaleInfoComparer()).ToList() : HomeforSaleGroupByDomain(refinedInfos, sortBy).Distinct(new HomeForSaleInfoComparer()).ToList(),
                    SearchFacet = srchFact
                };
        }

        private List<HomeForSaleInfo> HomeforSaleGroupByDomain(List<HomeForSaleInfo> hInfos, string sortBy)
        {
            var domain = Context.GetDeviceName().GetDomainFromDeviceName();

            hInfos = SortbyDomainHomeForSaleInfo(hInfos, sortBy);
            var currentCompany = hInfos.Where(c => c.Domain == domain).ToList();
            var otherCompany = hInfos.Where(c => c.Domain != domain).ToList();

            var allCompanies = new List<HomeForSaleInfo>();
            allCompanies.AddRange(currentCompany);
            allCompanies.AddRange(otherCompany);

            return allCompanies;
        }

        private List<MultiNameValueStatus> GetMultiButtonValuesBathandHalf(int totalVal, IEnumerable<int> filterdList, int totalHalfVal, IEnumerable<int> filterdHalfList, List<string> selecteditem, bool isFaceted)
        {
            var nList = new List<MultiNameValueStatus>();
            var i = 1;
            var half = "½";

            while (i <= totalVal)
            {
                string newVal = GlobalEnums.FacetStatus.On.ToString();
                newVal = !isFaceted
                             ? GlobalEnums.FacetStatus.On.ToString()
                             : (filterdList.Any() ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString());

                foreach (var fl in filterdList.Where(v => v != 0))
                {
                    if (fl == i)
                    {
                        newVal = GlobalEnums.FacetStatus.On.ToString();
                        break;
                    }
                    newVal = GlobalEnums.FacetStatus.Off.ToString();
                }

                bool isSelected;
                isSelected = isFaceted && (selecteditem.Any());// || selecteditem.Any(fl => fl == i.ToString());

                foreach (var fl in selecteditem)
                {
                    var fls = fl.Contains("+") ? fl.Replace("+", "") : fl;
                    if (fls == i.ToString())
                    {
                        isSelected = true;
                        break;
                    }
                    isSelected = false;
                }

                nList.Add(new MultiNameValueStatus { Value = i.ToString(), ItemStatus = isSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isSelected });
                if (totalHalfVal > 0)
                {
                    if (isFaceted && !filterdHalfList.Any(v => v != 0))
                    {
                        newVal = GlobalEnums.FacetStatus.Off.ToString();
                    }

                    bool isHlfSelected = false;
                    foreach (var fl in selecteditem)
                    {
                        if (fl == i.ToString() + half)
                        {
                            isHlfSelected = true;
                            break;
                        }
                        isHlfSelected = false;
                    }

                    nList.Add(new MultiNameValueStatus { Value = i + half, ItemStatus = isHlfSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isHlfSelected });
                }

                //if (i >= 4 && isSelected)
                //{
                //    var updatedItem = new MultiNameValueStatus()
                //    {
                //        Value = "4",
                //        ItemStatus = GlobalEnums.FacetStatus.On.ToString(),
                //        IsSelected = true
                //    };
                //    nList.RemoveAt(6);
                //    nList.Add(updatedItem);
                //}
                //else if (i >= 4 && !isSelected)
                //{
                //    var updatedItem = new MultiNameValueStatus()
                //    {
                //        Value = "4",
                //        ItemStatus = newVal,
                //        IsSelected = false
                //    };
                //    nList.RemoveAt(6);
                //    nList.Add(updatedItem);
                //}
                if (nList.Count >= 7)
                    break;
                i++;
            }
            return nList.OrderBy(o => o.Value).ToList();
        }

        private List<MultiValueStatus> GetMultiButtonValues(int totalVal, IEnumerable<int> filterdList, List<string> selecteditem, bool isFaceted)
        {
            var nList = new List<MultiValueStatus>();
            var i = 1;
            while (i <= totalVal)
            {
                string newVal = GlobalEnums.FacetStatus.On.ToString();
                newVal = !isFaceted
                             ? GlobalEnums.FacetStatus.On.ToString()
                             : (filterdList.Any() ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString());

                var newfilterdList = filterdList.Where(v => v != 0);
                if (isFaceted && !newfilterdList.Any())
                {
                    newVal = GlobalEnums.FacetStatus.Off.ToString();
                }
                else
                {
                    foreach (var fl in newfilterdList)
                    {
                        if (fl == i)
                        {
                            newVal = GlobalEnums.FacetStatus.On.ToString();
                            break;
                        }
                        newVal = GlobalEnums.FacetStatus.Off.ToString();
                    }
                }

                bool isSelected;
                isSelected = isFaceted && (selecteditem.Any());// || selecteditem.Any(fl => fl == i.ToString());

                foreach (var fl in selecteditem)
                {
                    var fls = fl.Contains("+") ? fl.Replace("+", "") : fl;
                    if (fls == i.ToString())
                    {
                        isSelected = true;
                        break;
                    }
                    isSelected = false;
                }

                nList.Add(new MultiValueStatus { Value = i, ItemStatus = isSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isSelected });

                if (i >= 8 && isSelected)
                {
                    var updatedItem = new MultiValueStatus()
                        {
                            Value = 7,
                            ItemStatus = GlobalEnums.FacetStatus.On.ToString(),
                            IsSelected = true
                        };
                    nList.RemoveAt(6);
                    nList.Add(updatedItem);
                }
                else if (i >= 8 && !isSelected)
                {
                    var updatedItem = new MultiValueStatus()
                    {
                        Value = 7,
                        ItemStatus = newVal,
                        IsSelected = false
                    };
                    nList.RemoveAt(6);
                    nList.Add(updatedItem);
                }
                if (i >= 7)
                    break;
                i++;
            }
            return nList.OrderBy(o => o.Value).ToList();
        }

        public SearchFacet GetSearchFacet(List<HomeForSaleInfo> hInfos, string selectedPath, List<HomeForSaleInfo> flthInfos, Dictionary<string, string> filters, bool isRealtor)
        {
            var searchFacet = new SearchFacet();
            searchFacet.Division = "0";
            flthInfos = flthInfos ?? new List<HomeForSaleInfo>();

            var allBonusDensSolar = new List<NameandValue>()
				{
					new NameandValue() {Name = "Bonus Room", Value = "B"},
					new NameandValue() {Name = "Den", Value = "D"},
					new NameandValue() {Name = "Solarium", Value = "S"}
				};
            var aviBonusDensSolar = new List<NameandValue>();

            bool bounsRooms;
            bool dens;
            bool solarium;

            var avblCities = new List<NameandValue>();
            if (flthInfos.Count > 0)
            {
                avblCities = flthInfos.GroupBy(g => new { g.City })
                                      .Select(s => new NameandValue { Name = s.Key.City, Value = s.Key.City })
                                      .OrderBy(o => o.Name)
                                      .ToList();
                bounsRooms = flthInfos.Any(b => b.NumberofBonusRooms > 0);
                dens = flthInfos.Any(b => b.NumberofDens > 0);
                solarium = flthInfos.Any(b => b.NumberofSolariums > 0);
            }
            else
            {
                avblCities = hInfos.GroupBy(g => new { g.City })
                                      .Select(s => new NameandValue { Name = s.Key.City, Value = s.Key.City })
                                      .OrderBy(o => o.Name)
                                      .ToList();
                bounsRooms = hInfos.Any(b => b.NumberofBonusRooms > 0);
                dens = hInfos.Any(b => b.NumberofDens > 0);
                solarium = hInfos.Any(b => b.NumberofSolariums > 0);
            }

            if (bounsRooms)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Bonus Room", Value = "B" });
            if (dens)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Den", Value = "D" });
            if (solarium)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Solarium", Value = "S" });

            var bonDenSol = new List<NameandValue>();
            if (hInfos.Count > 0)
            {
                var beds = new List<string>();
                var baths = new List<string>();
                var garage = new List<string>();
                var story = new List<string>();

                if (filters != null)
                {
                    var noofBeds = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString())
                                       ? filters[GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()]
                                       : String.Empty;
                    beds = noofBeds.StringToList();

                    var noofBaths = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString())
                                        ? filters[GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()]
                                        : String.Empty;
                    baths = noofBaths.StringToList();

                    var noofGarages = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofGarage.ToString())
                                          ? filters[GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()]
                                          : String.Empty;
                    garage = noofGarages.StringToList();

                    var noofStories = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofStories.ToString())
                                          ? filters[GlobalEnums.SearchFilterKeys.NumberofStories.ToString()]
                                          : String.Empty;
                    story = noofStories.StringToList();
                }

                searchFacet.NumberofBedrooms = GetMultiButtonValues(GetFacetBeds(hInfos), GetAvailableFacetBeds(flthInfos), beds, filters != null);
                searchFacet.NumberofBathrooms = GetMultiButtonValuesBathandHalf(GetFacetBaths(hInfos), GetAvailableFacetBaths(flthInfos), GetFacetHalfBaths(hInfos), GetAvailableFacetHalfBaths(flthInfos), baths, filters != null);
                searchFacet.NumberofGarages = GetMultiButtonValues(GetFacetGarages(hInfos), GetAvailableFacetGarages(flthInfos), garage, filters != null);
                searchFacet.NumberofHalfBathrooms = GetMultiButtonValues(GetFacetHalfBaths(hInfos), GetAvailableFacetHalfBaths(flthInfos), new List<string>(), filters != null);
                searchFacet.NumberofStories = GetMultiButtonValues(GetFacetStories(hInfos), GetAvailableFacetStories(flthInfos), story, filters != null);
            }

            var allSchoolDistricts = SearchHelper.GetSchoolDistricts(selectedPath, isRealtor);
            var allAvailability = _searchHelper.GetAvailabilities();

            if (hInfos.Count > 0)
            {
                searchFacet.MinPrice = CommonHelper.RoundPrice(hInfos.Min(mi => mi.Price), true);
                searchFacet.MaxPrice = CommonHelper.RoundPrice(hInfos.Max(mi => mi.Price), false);
                searchFacet.PriceTick = CommonHelper.FindPriceTick(searchFacet.MinPrice);
                searchFacet.MinSqFt = hInfos.Min(mi => mi.SquareFootage);
                searchFacet.MaxSqFt = hInfos.Max(mi => mi.SquareFootage);
                searchFacet.SqFtTick = 500;
            }

            if (filters != null)
            {
                ////Filters
                var minPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingPrice.ToString()].StringToDouble() : 0;
                var maxPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingPrice.ToString()].StringToDouble() : 0;
                var minSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()].StringToInt32() : 0;
                var maxSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()].StringToInt32() : 0;
                var city = filters.ContainsKey(GlobalEnums.SearchFilterKeys.City.ToString()) ? filters[GlobalEnums.SearchFilterKeys.City.ToString()] : String.Empty;
                var schl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()] : String.Empty;
                var avai = filters.ContainsKey(GlobalEnums.SearchFilterKeys.Availability.ToString()) ? filters[GlobalEnums.SearchFilterKeys.Availability.ToString()] : String.Empty;
                searchFacet.Division = filters.ContainsKey(GlobalEnums.SearchFilterKeys.DivisionArea.ToString()) ? (filters[GlobalEnums.SearchFilterKeys.DivisionArea.ToString()] ?? "0") : "0";
                var bodeso = filters.ContainsKey(GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()) ? filters[GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()] : String.Empty;

                if (hInfos.Count > 0)
                {
                    searchFacet.SelectedMinPrice = minPrice;
                    searchFacet.SelectedMaxPrice = maxPrice;
                    searchFacet.SelectedMinSqFt = minSqft;
                    searchFacet.SelectedMaxSqFt = maxSqft;
                }

                if (hInfos.Count > 0)
                {
                    searchFacet.MinPrice = CommonHelper.RoundPrice(flthInfos.Min(mi => mi.Price), true);
                    searchFacet.MaxPrice = CommonHelper.RoundPrice(flthInfos.Max(mi => mi.Price), false);
                    searchFacet.PriceTick = CommonHelper.FindPriceTick(searchFacet.MinPrice);
                    searchFacet.MinSqFt = flthInfos.Min(mi => mi.SquareFootage);
                    searchFacet.MaxSqFt = flthInfos.Max(mi => mi.SquareFootage);
                    searchFacet.SqFtTick = 500;
                }

                searchFacet.Cities = hInfos.GroupBy(g => new { g.City })
                                           .Select(
                                               s =>
                                               new NameandValue
                                                   {
                                                       Name = s.Key.City,
                                                       Value = s.Key.City,
                                                       ItemStatus =
                                                           avblCities.Any(f => f.Name == s.Key.City)
                                                               ? GlobalEnums.FacetStatus.On.ToString()
                                                               : GlobalEnums.FacetStatus.Off.ToString(),
                                                       IsSelected = city == s.Key.City
                                                   }).OrderBy(o => o.Name).ToList();

                var nwIds = flthInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var sIds = hInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var nameandIds =
                    sIds.Select(val => allSchoolDistricts.FirstOrDefault(s => s.ID.ToString() == val.Key.SchoolDistrict))
                        .Where(item => item != null)
                        .Select(item => new NameandId
                            {
                                ID = item.ID,
                                Name = item.DisplayName,
                                ItemStatus =
                                    nwIds.Any(a => a.Key.SchoolDistrict == item.ID.ToString())
                                        ? GlobalEnums.FacetStatus.On.ToString()
                                        : GlobalEnums.FacetStatus.Off.ToString(),
                                IsSelected = schl.ToUpper().ToString() == item.ID.ToString()
                            }).ToList();
                searchFacet.SchoolDistricts = nameandIds.OrderBy(o => o.Name).ToList();

                var aIds = hInfos.Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var nwaIds = flthInfos.Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var avaisIds =
                    aIds.Select(val => allAvailability.FirstOrDefault(s => s.ID.ToString() == val))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                                {
                                    ID = item.ID,
                                    Name = item.Name,
                                    ItemStatus =
                                        nwaIds.Any(a => a.ToUpper() == item.ID.ToString())
                                            ? GlobalEnums.FacetStatus.On.ToString()
                                            : GlobalEnums.FacetStatus.Off.ToString(),
                                    IsSelected = avai.ToUpper() == item.ID.ToString()
                                })
                        .ToList();
                searchFacet.Availability = avaisIds.OrderBy(o => o.Name).ToList();

                bonDenSol.AddRange(allBonusDensSolar.Select(bo => new NameandValue()
                {
                    Name = bo.Name,
                    Value = bo.Value,
                    ItemStatus = aviBonusDensSolar.Any(f => f.Value == bo.Value) ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString(),
                    IsSelected = bo.Value == bodeso
                }));

                searchFacet.BonusRoomsDensSolarium = bonDenSol;
            }
            else
            {
                if (hInfos.Count > 0)
                {
                    searchFacet.SelectedMinPrice = searchFacet.MinPrice;
                    searchFacet.SelectedMaxPrice = searchFacet.MaxPrice;
                    searchFacet.SelectedMinSqFt = searchFacet.MinSqFt;
                    searchFacet.SelectedMaxSqFt = searchFacet.MaxSqFt;
                }

                searchFacet.Cities = hInfos.GroupBy(g => new { g.City })
                                           .Select(
                                               s =>
                                               new NameandValue
                                                   {
                                                       Name = s.Key.City,
                                                       Value = s.Key.City,
                                                       ItemStatus =
                                                          avblCities.Any(f => f.Name == s.Key.City)
                                                              ? GlobalEnums.FacetStatus.On.ToString()
                                                              : GlobalEnums.FacetStatus.Off.ToString(),
                                                       IsSelected = false
                                                   }).OrderBy(o => o.Name).ToList();

                var nwIds = flthInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var sIds = hInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var nameandIds =
                    sIds.Select(val => allSchoolDistricts.FirstOrDefault(s => s.ID.ToString() == val.Key.SchoolDistrict))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                                {
                                    ID = item.ID,
                                    Name = item.DisplayName,
                                    ItemStatus =
                                       nwIds.Any(a => a.Key.SchoolDistrict == item.ID.ToString())
                                           ? GlobalEnums.FacetStatus.On.ToString()
                                           : GlobalEnums.FacetStatus.Off.ToString(),
                                    IsSelected = false
                                })
                        .ToList();
                searchFacet.SchoolDistricts = nameandIds.OrderBy(o => o.Name).ToList();

                var aIds = hInfos.Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var nwaIds = flthInfos.Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var avaisIds =
                    aIds.Select(val => allAvailability.FirstOrDefault(s => s.ID.ToString() == val))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                                {
                                    ID = item.ID,
                                    Name = item.Name,
                                    ItemStatus =
                                        nwaIds.Any(a => a.ToUpper() == item.ID.ToString())
                                            ? GlobalEnums.FacetStatus.On.ToString()
                                            : GlobalEnums.FacetStatus.Off.ToString(),
                                    IsSelected = false
                                })
                        .ToList();
                searchFacet.Availability = avaisIds.OrderBy(o => o.Name).ToList();

                bonDenSol.AddRange(allBonusDensSolar.Select(bo => new NameandValue()
                {
                    Name = bo.Name,
                    Value = bo.Value,
                    ItemStatus = aviBonusDensSolar.Any(f => f.Value == bo.Value) ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString(),
                    IsSelected = false
                }));
                searchFacet.BonusRoomsDensSolarium = bonDenSol;
            }

            return searchFacet;
        }

        private int GetFacetBeds(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBedrooms) : 0;
        }

        private int GetFacetBaths(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBathrooms) : 0;
        }

        private int GetFacetHalfBaths(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofHalfBathrooms) : 0;
        }

        private int GetFacetGarages(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofGarages) : 0;
        }

        private int GetFacetBonusRooms(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBonusRooms) : 0;
        }

        private int GetFacetDens(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofDens) : 0;
        }

        private int GetFacetStories(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofStories) : 0;
        }

        private IEnumerable<int> GetAvailableFacetBeds(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBedrooms) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetBaths(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBathrooms) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetHalfBaths(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofHalfBathrooms) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetGarages(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofGarages) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetBonusRooms(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBonusRooms) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetDens(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofDens) : new List<int>();
        }

        private IEnumerable<int> GetAvailableFacetStories(List<HomeForSaleInfo> hfsInfos)
        {
            return hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofStories) : new List<int>();
        }

        private static List<HomeForSaleInfo> SortbyDomainHomeForSaleInfo(List<HomeForSaleInfo> homeForSaleInfo, string sortBy)
        {
            //Sort order
            //-	Price low to high  (secondary default sort)
            //-	Price high to low
            //-	Plan Name
            //-	Availability (default)
            //-	Number of bedrooms
            //-	Square footage
            //-	Stories

            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceLowtoHigh:	//2
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenByDescending(o => o.Price).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.SquareFootage: //6
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenBy(o => o.SquareFootage).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.Numberofbedrooms: //7
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenBy(o => o.NumberofBedrooms).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.PlanName: //5
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenBy(o => o.HomeforSalePlanName).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.Stories: //9
                    homeForSaleInfo = homeForSaleInfo.OrderBy(f => f.Domain).ThenBy(o => o.NumberofStories).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.AvailabilityDate: //8
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.AvailabilityDate).ThenBy(o => o.Price).ToList();
                    break;

                default: //AvailabilityDate:
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.HomeforSaleCommunityName).ThenBy(o => o.Price).ToList();
                    break;
            }

            return homeForSaleInfo;
        }
    }
}