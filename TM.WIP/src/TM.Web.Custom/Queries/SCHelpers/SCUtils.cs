﻿using System;
using System.Globalization;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;
using Sitecore.Web;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.WebControls;
using System.Linq;

namespace TM.Web.Custom.SCHelpers
{
    public class SCUtils
    {
        private static Dictionary<string, CommunityLatLong> _allCommunityLatLongs = new Dictionary<string, CommunityLatLong>();
        private static volatile object _syncObj = new object();
        private static Dictionary<string, CommunityLatLong> AllCommunityLatLongs
        {
            get
            {
                if ((_allCommunityLatLongs != null) && _allCommunityLatLongs.Any())
                {
                    return _allCommunityLatLongs;
                }
                else
                {
                    var communities = new List<Item>();
                    _allCommunityLatLongs = new Dictionary<string, CommunityLatLong>();
                    string query = String.Format("fast://*[@@templateid='{0}' and @Community Status!='{1}']",
                                                 SCIDs.TemplateIds.CommunityPage, SCIDs.StatusIds.CommunityStatus.Closed);
                    Database database = Context.Database;
                    var items = database.SelectItems(query);
                    if (items != null)
                    {
                        communities.AddRange(database.SelectItems(query));

                        foreach (Item community in communities)
                        {
                            try
                            {

                                if (community != null && community.ID != ID.Null)
                                    lock (_syncObj)
                                    {
                                        _allCommunityLatLongs.Add(community.ID.ToString(),
                                                                             new CommunityLatLong(community.ID.ToString(),
                                                                                                  community["Community Latitude"].CastAs
                                                                                                      <float>(0),
                                                                                                  community["Community Longitude"].CastAs
                                                                                                      <float>(0)));
                                    }

                            }
                            catch (System.ArgumentException ex)
                            {
                                Log.Error("Error when loading community lat long", ex, typeof(SCUtils));
                                return new Dictionary<string, CommunityLatLong>();
                            }
                        }
                    }
                }
                return _allCommunityLatLongs;
            }
        }
        public static string GetItemUrl(Item item)
        {
            if (item != null)
            {
                if (!string.IsNullOrWhiteSpace(item["Navigation Destination URL"]))
                {
                    return item["Navigation Destination URL"];
                }
                if (item.Paths.IsMediaItem)
                {
                    return MediaManager.GetMediaUrl(item);
                }
                return TM.Web.Custom.SCHelpers.TMLinkProvider.GetItemUrl(item).Replace(".aspx", String.Empty);
            }
            return String.Empty;
        }
        public static string GetItemUrl(Item item, Sitecore.Links.UrlOptions options)
        {
            if (item != null)
            {
                if (item.Paths.IsMediaItem)
                {
                    return MediaManager.GetMediaUrl(item);
                }
                return TM.Web.Custom.SCHelpers.TMLinkProvider.GetItemUrl(item, options).Replace(".aspx", String.Empty);
            }
            return String.Empty;
        }
        public static string GetSCParameter(string parameterKey, Sublayout sublayout)
        {
            string rawParameters = sublayout.Parameters;
            var parameters = WebUtil.ParseUrlParameters(rawParameters);

            //todo: need to refactor this so that ParseUrlParameters does not have to run through entire set of params and keys are case insensitive

            /*
                var lowerCaseParams = new NameValueCollection();

                var allKeys = parameters.AllKeys;
                foreach (var key in allKeys)
                {
            lowerCaseParams.Add(key.ToLowerInvariant(),parameters[key]);
                }
            */
            if (parameters[parameterKey] != null)
                return HttpUtility.UrlDecode(parameters[parameterKey].ToString(CultureInfo.InvariantCulture)) ?? String.Empty;

            return String.Empty;
        }



        public static Item GetItem(ID itemId)
        {
            return Context.Database.GetItem(itemId);
        }
        public static List<Item> SelectItems(ID templateId, ID TopNodeID)
        {
            List<Item> items = new List<Item>();



            return items;
        }
        /// <summary>
        /// Converts From Sitecore Timestamp to .net Date Time object
        /// </summary>
        /// <param name="inDateTime"></param>
        /// <returns></returns>
        public static DateTime SCDateTimeToMsDateTime(string inDateTime)
        {
            return DateUtil.ParseDateTime(inDateTime, default(DateTime));
        }
        /// <summary>
        /// Converts From .net Date time to Sitecore formatted datetime string
        /// </summary>
        /// <param name="inDateTime"></param>
        /// <returns></returns>
        public static string MSDateTimeToSCDateTime(DateTime inDateTime)
        {
            return String.Format("{0:d4}{1:d2}{2:d2}T{3:d2}{4:d2}{5:d2}", inDateTime.Year, inDateTime.Month, inDateTime.Day, inDateTime.Hour, inDateTime.Minute, inDateTime.Second);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lng1"></param>
        /// <param name="lat2"></param>
        /// <param name="lng2"></param>
        /// <returns>Distance in Miles</returns>
        public static double getDistance(float lat1, float lng1, float lat2, float lng2)
        {
            var R = 6371; // km
            var dLat = (lat2 - lat1) * (3.1415 / 180);
            var dLon = (lng2 - lng1) * (3.1415 / 180);
            var lat1r = lat1 * (3.1415 / 180);
            var lat2r = lat2 * (3.1415 / 180);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c;
            return d * .621371;
        }


        public static List<string> CommunityWithinDistanceOf(Item thisCommunity, int withinMileRadius)
        {
            var matchedCommunities = IDsOfCommunitiesMatching(thisCommunity, withinMileRadius);

            return matchedCommunities.ToList();

        }


        public static List<SimilarCommunity> getSimilarCommunities(Item moreLikeThis, int acceptableDistance, float priceVariance)
        {

            List<SimilarCommunity> similarCommunities = new List<SimilarCommunity>();
            IEnumerable<string> linqQ = IDsOfCommunitiesMatching(moreLikeThis, acceptableDistance);


            string[] esacpedPath = moreLikeThis.Paths.FullPath.Split('/');
            string qualifiedPath = String.Empty;
            foreach (string s in esacpedPath)
            {
                qualifiedPath += s.Contains("-") ? ("/#" + s + "#") : ("/" + s);
            }

            var comIds = linqQ.ToList();

            Database database = Sitecore.Context.Database;
            //get our community avgPrice
            SimilarCommunity closedCommunity = getCommunitySimilarityDetails(qualifiedPath, database, moreLikeThis.ID.ToString());

            foreach (var coms in comIds)
            {
                SimilarCommunity possibleMatch = getCommunitySimilarityDetails(qualifiedPath, database, coms);

                if ((possibleMatch.priceAvg < (closedCommunity.priceAvg + (closedCommunity.priceAvg * priceVariance))) &&
                  (possibleMatch.priceAvg > (closedCommunity.priceAvg - (closedCommunity.priceAvg * priceVariance))))
                    similarCommunities.Add(possibleMatch);
            }

            return similarCommunities;
        }

        private static IEnumerable<string> IDsOfCommunitiesMatching(Item moreLikeThis, int acceptableDistance)
        {
            float degreesPerMile = .0144144F;

            float desiredLat = Single.Parse(moreLikeThis["Community Latitude"]);
            float desiredLon = Single.Parse(moreLikeThis["Community Longitude"]);

            float latMax = desiredLat + (degreesPerMile * acceptableDistance);
            float latMin = desiredLat - (degreesPerMile * acceptableDistance);
            float lonMax = desiredLon + (degreesPerMile * acceptableDistance);
            float lonMin = desiredLon - (degreesPerMile * acceptableDistance);


            try
            {
                var keys = AllCommunityLatLongs.Keys.Where(key => key.IsNotEmpty());

                var linqQ = from key in keys
                            where
                                AllCommunityLatLongs[key].Lattitude < latMax && AllCommunityLatLongs[key].Lattitude > latMin
                                &&
                                AllCommunityLatLongs[key].Longitude < lonMax && AllCommunityLatLongs[key].Longitude > lonMin
                                &&
                                 getDistance(desiredLat, desiredLon, AllCommunityLatLongs[key].Lattitude, AllCommunityLatLongs[key].Longitude) <
                                acceptableDistance
                            select key;
                return linqQ;
            }
            catch (Exception ex)
            {
                Log.Error("Error when loading community lat long", ex, typeof(SCUtils));
                _allCommunityLatLongs = null;
                return new List<string>();
            }
        }

        private static SimilarCommunity getCommunitySimilarityDetails(string qualifiedPath, Database DB, string comid)
        {
            //todo: pass item corresponding to comid and use its fullpath in the query below 
            string query = String.Format("fast:{0}/ancestor::*[@@templateid = '{1}']//*[@@id='{2}']/*[@@templateid='{3}']",
                qualifiedPath,
                SCIDs.TemplateIds.ProductTypePage,
                comid,
                SCIDs.TemplateIds.PlanPage
                );
            List<Item> plans = new List<Item>();
            plans.AddRange(DB.SelectItems(query));
            SimilarCommunity newCom = new SimilarCommunity();

            float combinedPrice = 0f;
            foreach (Item plan in plans)
            {
                float thisPrice = plan["Priced from Value"].CastAs<float>(0);
                int thisSqft = plan["Square Footage"].CastAs<int>(0);
                int thisBed = plan["Number of Bedrooms"].CastAs<int>(0);
                int thisBath = plan["Number of Bathrooms"].CastAs<int>(0);


                newCom.priceMax = thisPrice > newCom.priceMax ? thisPrice : newCom.priceMax;
                newCom.priceMin = thisPrice < newCom.priceMin ? thisPrice : newCom.priceMin;

                newCom.sqftMax = thisSqft > newCom.sqftMax ? thisSqft : newCom.sqftMax;
                newCom.sqftMin = thisSqft < newCom.sqftMin ? thisSqft : newCom.sqftMin;

                newCom.bedMax = thisBed > newCom.bedMax ? thisBed : newCom.bedMax;
                newCom.bedMin = thisBed < newCom.bedMin ? thisBed : newCom.bedMin;

                newCom.bathMax = thisBath > newCom.bathMax ? thisBath : newCom.bathMax;
                newCom.bathMin = thisBath < newCom.bathMin ? thisBath : newCom.bathMin;

                combinedPrice += thisPrice;
            }
            newCom.comID = comid;
            newCom.priceAvg = plans.Count > 0 ? (combinedPrice / plans.Count) : 0f;
            return newCom;
        }

        /// <summary>
        /// Get current status from field
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <returns></returns>
        public static GlobalEnums.CommunityStatus CommunityCurrentStatus(string fieldValue)
        {
            if (!String.IsNullOrWhiteSpace(fieldValue))
            {
                switch (fieldValue.ToUpper())
                {
                    case SCIDs.CommunityFields.CommunityStatuses.Open:
                        return GlobalEnums.CommunityStatus.Open;
                    case SCIDs.CommunityFields.CommunityStatuses.ComingSoon:
                        return GlobalEnums.CommunityStatus.ComingSoon;
                    case SCIDs.CommunityFields.CommunityStatuses.PreSelling:
                        return GlobalEnums.CommunityStatus.PreSelling;
                    case SCIDs.CommunityFields.CommunityStatuses.GrandOpening:
                        return GlobalEnums.CommunityStatus.GrandOpening;
                    case SCIDs.CommunityFields.CommunityStatuses.Closeout:
                        return GlobalEnums.CommunityStatus.Closeout;
                    case SCIDs.CommunityFields.CommunityStatuses.Closed:
                        return GlobalEnums.CommunityStatus.Closed;
                    default:
                        return GlobalEnums.CommunityStatus.InActive;
                }
            }
            return GlobalEnums.CommunityStatus.InActive;
        }

        /// <summary>
        /// gets thequery needed to get google plave results back from the google server.
        /// </summary>
        /// <param name="centerLattitude">Lattitude of place we are searching around</param>
        /// <param name="centerLongitude">Longitude of place we are searching around</param>
        /// <param name="searchRadius">Number of meters from origin to search up to 50,000</param>
        /// <param name="placeTypes">Pipe(|) delimited list of google place types that are to be included in results</param>
        /// <returns></returns>
        public static string PlaceQuery(string centerLattitude, string centerLongitude, int searchRadius, string placeTypes)
        {
            return String.Format("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={0},{1}&radius={2}&types={3}&sensor=false&key={4}",
                centerLattitude,
                centerLongitude,
                searchRadius,
                placeTypes,
                Sitecore.Context.Site.Device.ToLower() == GlobalEnums.Domain.DarlingHomes.ToString().ToLower() ? Config.Settings.DHGoogleApiKey : Config.Settings.TMGoogleApiKey);
        }
        /// <summary>
        /// Get current status from field
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <returns></returns>
        public static GlobalEnums.SearchStatus SearchStatus(string fieldValue)
        {
            if (!String.IsNullOrWhiteSpace(fieldValue))
            {
                switch (fieldValue)
                {
                    case SCIDs.StatusIds.Status.Active:
                        return GlobalEnums.SearchStatus.Active;
                    default:
                        return GlobalEnums.SearchStatus.InActive;
                }
            }
            return GlobalEnums.SearchStatus.InActive;
        }


        /// <summary>
        /// Returns a max and min of fields in collection
        /// </summary>
        /// <param name="itemCollection">SC item collection.</param>
        /// <param name="fieldIDs">ID's of fields for which max and min should be calculated</param>
        /// <param name="typeConvertors">list of func's to use for casting values of the fields</param>
        /// <returns>a dict with fieldid+"max" and fieldid+"min" as keys and max and min for the fields as values</returns>
        public static Dictionary<string, double> GetRangeValuesFrom(IEnumerable<TMSearchResultItem> itemCollection, string[] fieldNames,
                                                                     Func<string, double>[] typeConvertors)
        {
            var dict = new Dictionary<string, double>();


            Guard.AgainstNull(fieldNames);

            var items = itemCollection.Select(i => i.GetItem());
            foreach (var item in items)
            {
                int i = 0;
                foreach (var fieldName in fieldNames)
                {
                    var fieldValue = item.Fields[fieldName].Value;

                    if (fieldValue.IsNotEmpty())
                    {
                        var currentValue = typeConvertors[i](fieldValue);

                        var existingValue = dict.ContainsKey(fieldName + "max") ? dict[fieldName + "max"] : currentValue;

                        dict[fieldName + "max"] = currentValue > existingValue ? currentValue : existingValue;

                        existingValue = dict.ContainsKey(fieldName + "min") ? dict[fieldName + "min"] : currentValue;
                        dict[fieldName + "min"] = currentValue < existingValue ? currentValue : existingValue;
                    }
                    else
                    {
                        var currentValue = typeConvertors[i](fieldValue);
                        dict[fieldName + "max"] = currentValue;
                        dict[fieldName + "min"] = currentValue;
                    }


                    i++;
                }
            }
            return dict;
        }
        /// <summary>
        /// gets a distinct grouping based on key value pairs for use when you need some item type from this level and something else from another item type
        /// </summary>
        /// <param name="keyValueItems">A dictionary containg the parentID and the templateID for the desired type</param>
        /// <returns>A Dictionary containg all items that match your query keyed off the original parent node type</returns>
        public static Dictionary<ID, Item[]> GetItemsOfType(Dictionary<ID, ID> keyValueItems, string fastQueryParameters)
        {
            Dictionary<ID, Item[]> myReturnValue = new Dictionary<ID, Item[]>();
            string query;

            foreach (ID parent in keyValueItems.Keys)
            {
                query = "fast://*[@@id='{0}']/*[@@templateid='{1}']/*[{2}]".FormatWith(
                    parent,
                    keyValueItems[parent],
                    fastQueryParameters);

                myReturnValue[parent] = Context.Database.SelectItems(query);
            }


            return myReturnValue;
        }

        public static Item GetNearestParentOfTemplateID1(Sitecore.Data.ID desiredType, Sitecore.Data.ID startingNodeID, Item currentHomeItem)
        {
            Item originalItem = Sitecore.Context.Database.GetItem(startingNodeID);
            Item currentNode = originalItem;
            if (currentNode == null) return null;
            while ((currentHomeItem.ID != currentNode.ID) && (currentNode.TemplateID != desiredType))
            {
                currentNode = currentNode.Parent;
            }
            if (currentNode.TemplateID == desiredType)
            {
                return currentNode;
            }
            return originalItem;
        }

        /// <summary>
        /// Get parent template id
        /// </summary>
        /// <param name="desiredParentTemplateID"></param>
        /// <param name="startingNodeId"></param>
        /// <returns></returns>

        public static Item GetNearestParentOfTemplateID(ID desiredParentTemplateID, ID startingNodeID, Item currentHomeItem = null, Database DB = null)
        {
            Item originalItem = (DB ?? Context.Database).GetItem(startingNodeID);
            Item currentNode = originalItem;
            if (currentNode == null) return null;
            bool currentItemNotRoot = currentHomeItem != null ?
                     (currentHomeItem.ID != currentNode.ID) && (currentNode.TemplateID != desiredParentTemplateID)
                                : (ItemIDs.ContentRoot != currentNode.ID) && (currentNode.TemplateID != desiredParentTemplateID);

            while (currentItemNotRoot)
            {
                if (currentNode.TemplateID == SCIDs.TemplateIds.HomeTemplateId || currentNode.TemplateID.ToString() == SCIDs.TemplateIds.MicrositeTemplate) return currentNode; //some cases while loop goes infinity
                currentNode = currentNode.Parent;

                currentItemNotRoot = currentHomeItem != null ?
                         (currentHomeItem.ID != currentNode.ID) && (currentNode.TemplateID != desiredParentTemplateID)
                         : (ItemIDs.ContentRoot != currentNode.ID) && (currentNode.TemplateID != desiredParentTemplateID);
            }
            if (currentNode.TemplateID == desiredParentTemplateID)
            {
                return currentNode;
            }
            return originalItem;
        }



        public static string GetIFPBuilderID(Item item)
        {
            var itempath = item.Paths.FullPath.ToLowerInvariant();
            var companyName = "global";
            
            if (itempath.Contains("monarchgroup") & itempath.Contains("new condo"))
            {
                companyName = "monarchhighrise";
            }
            else if (itempath.Contains("monarchgroup"))
            {
                companyName = "monarchlowrise";
            }
            else if (itempath.Contains("taylor"))
            {
                companyName = "taylormorrison";
            }
            else if (itempath.Contains("darling"))
            {
                companyName = "darlinghomes";
            }
             return companyName;
        }


       

       
       
    }




}

