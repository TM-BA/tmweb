﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Domain.Enums;

namespace TM.Web.Custom.SCHelpers.Search
{
    public class CompanySearch
    {
        public static Item[] GetCrossCompaniesStateMarketingList(Database db = null)
        {
            return CrossCompaniesStateMarketingList(db);
        }

       

        public static Item[] GetCurrentCompanyStateMarketingList( Database db = null)
        {


            var itemPath = SearchHelper.EscapePath(Context.Site.StartPath);
            string qry =
                String.Format(
                    "fast:/{0}/*[@@templateid='{1}']/*[@@templateid='{2}' and @Status for Search = '{6}']/*[@@templateid='{3}' and @Status for Search = '{6}']/*[@@templateid='{4}']/*{5}/ancestor::*[@@templateid='{2}' or @@templateid='{3}']",
                    itemPath, SCIDs.TemplateIds.ProductTypePage,
                    SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage,
                    SCFastQueries.ActiveCommunitiesCondition, SCIDs.StatusIds.Status.Active);
            return (db ?? SearchHelper.ContextDB).SelectItems(qry);
        }

        public static Item[] CrossCompaniesStateMarketingList(Database db = null)
        {
            string qry =
                String.Format(
                    "fast:/sitecore/content/*{0}/home/*[@@templateid='{1}']/*[@@templateid='{2}' and @Status for Search = '{6}']/*[@@templateid='{3}' and @Status for Search = '{6}']/*[@@templateid='{4}']/*{5}/ancestor::*[@@templateid='{2}' or @@templateid='{3}']",
                    SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage,
                    SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage,
                    SCFastQueries.ActiveCommunitiesCondition, SCIDs.StatusIds.Status.Active);
            return (db ?? SearchHelper.ContextDB).SelectItems(qry);
        }

        public static Item[] GetSelectedCompaniesStateMarketingList(ID Company,Database db = null)
        {
            string searchCondition = "";

            searchCondition = String.Format("[@@id='{0}']", Company); 
            

            string qry =
                String.Format(
                    "fast:/sitecore/content/*{0}/home/*[@@templateid='{1}']/*[@@templateid='{2}' and @Status for Search = '{6}']/*[@@templateid='{3}' and @Status for Search = '{6}']/*[@@templateid='{4}']/*{5}/ancestor::*[@@templateid='{2}' or @@templateid='{3}']",
                    searchCondition, SCIDs.TemplateIds.ProductTypePage,
                    SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage,
                    SCFastQueries.ActiveCommunitiesCondition, SCIDs.StatusIds.Status.Active);
            return (db ?? SearchHelper.ContextDB).SelectItems(qry);
        }

      
    }
}