﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Common;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.WebControls;
using Convert = System.Convert;
using SCID = Sitecore.Data.ID;

namespace TM.Web.Custom.SCHelpers
{
    public class SearchHelper : BaseHelper
    {
        private static readonly CommonHelper CommonHelper = new CommonHelper();
        public static readonly Database ContextDB = Context.Database;
        private static readonly Cache _webCache = new Cache();

        private static Cache WebCache
        {
            get { return _webCache; }
        }

        private readonly TMSession _webSession = new TMSession();

        public TMSession WebSession
        {
            get { return _webSession; }
        }

        public DeviceType SCCurrentDeviceType
        {
            get
            {
                switch (Sitecore.Context.GetDeviceName())
                {
                    case "TaylorMorrison":
                        return DeviceType.TaylorMorrison;

                    case "MonarchGroup":
                        return DeviceType.MonarchGroup;

                    case "DarlingHomes":
                        return DeviceType.DarlingHomes;

                    default:
                        return DeviceType.TaylorMorrison;
                }
            }
        }

        #region Search Queries

        public IEnumerable<NameandId> GetCrossDivisions(Database db = null)
        {
            //string path = GetUniqueSearchPath(Context.Site.StartPath);
            string path = GetUniquePath(Context.Site.StartPath);

            string qry =
                string.Format(
                    "fast:/sitecore/content/*{0}/Home/*[@@templateid='{1}']{2}/*[@@templateid='{3}']/ancestor::*[@@templateid='{4}']",
                    SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, path, SCIDs.TemplateIds.CommunityPage,
                    SCIDs.TemplateIds.DivisionPage);
            return HydrateCrossDivision((db ?? ContextDB).SelectItems(qry));
        }

        public IEnumerable<NameandId> HydrateCrossDivision(Item[] items)
        {
            const string DASH_SEPARATOR = " - ";

            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            var crossDivisions = (from childItem in items
                                  let crossSiteSpecification = childItem.GetCrossSiteSpecification(sitecoreUrlOptions, uri)
                                  select new NameandId
                                  {
                                      ID = childItem.ID,
                                      Name = childItem.DisplayName,
                                      ParentName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID).DisplayName,
                                      Domain = crossSiteSpecification.Domain
                                  });

            //group divisions that are named the same
            var duplicateNameDivisions = crossDivisions.GroupBy
                (n => new { n.Name, n.Domain })
                .Where(g => g.Count() > 1)
                .SelectMany(x => x.Select(d => new NameandId
                {
                    ID = d.ID,
                    Name = d.Name + DASH_SEPARATOR + SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, d.ID).DisplayName.GetStateAbbreviationByState(),
                    ParentName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, d.ID).DisplayName,
                    Domain = d.Domain

                })).ToList();

            //filter the original list down and only take divisions that have a unique name
            var filteredDivisionList = crossDivisions.GroupBy(xDiv => new { xDiv.Name, xDiv.Domain }).Where(g => g.Count() == 1).SelectMany(x => x);

            var domain = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

            var currentCompany = filteredDivisionList.Where(c => c.Domain == domain).ToList();
            var otherCompany = filteredDivisionList.Where(c => c.Domain != domain).ToList();

            //add duplicate divisions in for sorting with 
            currentCompany.AddRange(duplicateNameDivisions.Where(x => x.Domain == domain).OrderBy(o => o.Name));

            var allCompanies = new List<NameandId>();
            allCompanies.AddRange(currentCompany.OrderBy(o => o.ParentName).ThenBy(s => s.Name));
            allCompanies.AddRange(otherCompany.OrderByDescending(f => f.Domain).ThenBy(o => o.ParentName).ThenBy(s => s.Name));

            return allCompanies;
        }

        public List<NameandId> GetAllCrossDivisions()
        {
            var allCrossDivisionsKey = CacheKeys.AllCrossDivisionsKey;
            return WebCache.Get(allCrossDivisionsKey, () => GetCrossDivisions()).ToList();
        }

        public static Item[] GetAllDivisions(string path, Database db = null)
        {
            string qry =
                string.Format(
                    "fast:{0}/*{1}/ancestor::*[@@templateid='{2}']",
                    path, SCFastQueries.ActiveCommunitiesCondition, SCIDs.TemplateIds.CityPage);
            return (db ?? ContextDB).SelectItems(qry);
        }

        public static Item[] GetHomefoSalesbyRealtor(string path)
        {
            string qry =
                string.Format("fast:/sitecore/content/*{0}/Home/{1}/*{2}/*{3}",
                    SCFastQueries.SameCompanyCondition, TrimPath(path), SCFastQueries.ActivePlans, SCFastQueries.ActiveHomeForSales);
            return ContextDB.SelectItems(qry);
        }

        public static Item[] GetHomefoSalesbyDivisonPath(string path)
        {
            string qry =
                string.Format("fast:/sitecore/content/*{0}/Home/{1}/*{2}/*{3}/*{4}",
                    SCFastQueries.SameCompanyCondition, TrimPath(path), SCFastQueries.ActiveCommunitiesCondition, SCFastQueries.ActivePlans, SCFastQueries.ActiveHomeForSales);
            return ContextDB.SelectItems(qry);
        }

        public static Item GetSelectedArea(string areaId)
        {
            string qry = string.Format("fast:/sitecore/content/*{0}/home/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@id='{3}']",
                                       SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, areaId);
            return ContextDB.SelectSingleItem(qry);
        }

        private static string TrimPath(string path)
        {
            return path.TrimStart('/').TrimEnd("/");
        }

        //public static Item[] GetHomefoSalesbyCommunity(string cId)
        //{
        //    string path = string.Format("{0}/{1}", Context.Site.StartPath, "new homes");

        //    string qry =
        //        string.Format(
        //            "fast:{0}/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']/*[@@templateid='{4}' and @@id='{5}']/*[@@templateid='{6}' and @Status for Search = '{7}']/*[@@templateid='{8}' and @Status for Search = '{7}']",
        //            EscapePath(path), SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage,
        //            SCIDs.TemplateIds.CityPage, SCIDs.CommunityFields.CommunityTemplateID, cId,
        //            SCIDs.TemplateIds.PlanPage, SCIDs.StatusIds.Status.Active, SCIDs.TemplateIds.HomeForSalePage);
        //    return _scContextDB.SelectItems(qry);
        //}

        public static Item[] GetSchoolDistricts(string path, bool isRealtor = false)
        {
            string qry = string.Format("fast:/sitecore/content/*[@@id = '{0}']/Home/{1}/ancestor::*[@@templateid='{2}']/*[@@templateid='{3}']/*", CommonHelper.GetCompanyItemIDByHostName(), TrimPath(path), SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.SchoolDistricts);
            return ContextDB.SelectItems(qry);
        }

        private IEnumerable<NameandId> GetAvailabilityData()
        {
            string qry =
                string.Format("fast:/sitecore/content/Global Data/Shared Field Values/Inventory Home Availability Status/*", SCFastQueries.CrossCompaniesCondition);
            return HydrateAvailabilities(ContextDB.SelectItems(qry));
        }

        private IEnumerable<NameandId> HydrateAvailabilities(Item[] items)
        {
            return items.Select(childItem => new NameandId
            {
                ID = childItem.ID,
                Name = childItem.DisplayName
            });
        }

        public List<NameandId> GetAvailabilities()
        {
            var allAvailabilitiesKey = CacheKeys.AllAvailabilitiesKey;
            return WebCache.Get(allAvailabilitiesKey, () => GetAvailabilityData().OrderBy(o => o.Name).ToList());
        }

        #endregion Search Queries

        #region Favorites

        public List<CommunityInfo> HydrateFavoritesCommunitiesByItems(IEnumerable<Item> communityItems)
        {
            var cInfos = new List<CommunityInfo>();

            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            //Do not change this order - Site Plan Image, Community Logo, Alternate Image
            var avaiImage = new List<ID>
				{
					//SCIDs.CommunityFields.SitePlanImageID,
					SCIDs.CommunityFields.CommunityLogoID,
					SCIDs.CommunityFields.AlternateImageID
				};

            var mappinCompanyAbb = Sitecore.Context.GetDeviceName().GetCurrentCompanyNameAbbreviation();

            foreach (var item in communityItems)
            {
                var lat = item["Community Latitude"].StringToDouble();
                var lng = item["Community Longitude"].StringToDouble();

                //if community does have lat and lng
                if (lat != 0.0 && lng != 0.0)
                {
                    var crossSiteSpecification = item.GetCrossSiteSpecification(sitecoreUrlOptions, uri);

                    var communityStatus = SCUtils.CommunityCurrentStatus(item["Community Status"]);
                    var statusforSearch = SCUtils.SearchStatus(item["Status for Search"]);

                    var status = communityStatus.ToString().ToLower();
                    status = FixSearchStatus(status);

                    var city = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CityPage, item.ID);
                    //var state = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, item.ID);
                    //var division = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, item.ID);
                    var division = city.Parent;
                    var state = division.Parent;

                    //var liveChatSkill = !string.IsNullOrWhiteSpace(cityName["Live Chat Skill"]) ? cityName["Live Chat Skill"] : diviName["Live Chat Skill"];
                    var liveChatSkill = division["Live Chat Skill"];

                    var communityUrl = crossSiteSpecification.SiteSpecificUrl;
                    var stProAbbr = state.DisplayName.GetStateAbbreviationByState().ToString();
                    var priceOverrideText = item["Price Override Text"];

                    var statusFlag = string.Empty;
                    switch (communityStatus)
                    {
                        case GlobalEnums.CommunityStatus.ComingSoon:
                            statusFlag = string.Format("/Images/{0}/comingsoon_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;

                        case GlobalEnums.CommunityStatus.Closeout:
                            statusFlag = string.Format("/Images/{0}/closeout_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;

                        case GlobalEnums.CommunityStatus.PreSelling:
                            statusFlag = string.Format("/Images/{0}/preselling_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;
                    }

                    //Filter SubcommunityInfo
                    var subCommunitySearchKey = CacheKeys.GetSubCommunitySearchKey(item.ID.ToString());
                    var scInfos = WebCache.Get(subCommunitySearchKey, () => GetSubcommunities(item.ID));
                    //TODO: Time being commented
                    //var scInfos = GetSubcommunities(item.ID);

                    var plansSearchKey = CacheKeys.GetPlansSearchKey(item.ID.ToString());
                    var allPlans = WebCache.Get(plansSearchKey, () => GetFavoritesPlans(item.ID, crossSiteSpecification, liveChatSkill));
                    //TODO: Time being commented
                    //var allPlans = GetFavoritesPlans(item.ID, crossSiteSpecification);
                    //var actpInfos = allPlans.Where(s => s.StatusforSearch == GlobalEnums.SearchStatus.Active).ToList();

                    var communityPromoKey = CacheKeys.GetCommunityPromoKey(item.ID.ToString());
                    var proInfo = WebCache.Get(communityPromoKey, () => GetCommunityPromo(item.ID));
                    //TODO: Time being commented
                    //var proInfo = GetCommunityPromo(item.ID);

                    //Filiter home for sale
                    var allhfsInfos = new List<HomeForSaleInfo>();
                    allhfsInfos.AddRange(allPlans.SelectMany(h => h.HomeForSaleList));

                    //Filiter home for sale
                    var acthfsInfos = allhfsInfos.Where(s => s.HomeforSaleStatus == GlobalEnums.SearchStatus.Active && s.StatusforSearch == GlobalEnums.SearchStatus.Active && s.DisplayonHomesforSalePage == "1").ToList();

                    var prilist = allPlans.Count > 0 ? allPlans.Where(c => c.PricedfromValue != 0.0).ToList() : new List<PlanInfo>();
                    var comPrice = string.IsNullOrEmpty(priceOverrideText)
                                       ? (prilist.Count > 0 ? string.Format("Priced from the {0:c0}", prilist.Min(p => p.PricedfromValue)) : string.Empty)
                                       : priceOverrideText;

                    if (scInfos.Count > 0)
                        cInfos.AddRange(
                            scInfos.Select(
                                scinfo =>
                                GetFavoritesCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos, acthfsInfos, communityStatus, statusforSearch, stProAbbr, priceOverrideText, status, comPrice,
                                                            statusFlag, lat, lng, city, state, division, avaiImage, scinfo, proInfo, liveChatSkill, mappinCompanyAbb)));
                    else
                        cInfos.Add(GetFavoritesCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos, acthfsInfos, communityStatus, statusforSearch, stProAbbr, priceOverrideText, status,
                                                               comPrice, statusFlag, lat, lng, city, state, division, avaiImage,
                                                               new SubcommunityInfo(), proInfo, liveChatSkill, mappinCompanyAbb));
                }
            }

            return cInfos.Distinct(new CommunityInfoComparer()).ToList();
        }

        private CommunityInfo GetFavoritesCommunitiyItems(Item item, string communityUrl, Uri uri, CrossSiteSpecification crossSiteSpecification, List<SubcommunityInfo> scInfos, List<PlanInfo> allPlans, List<HomeForSaleInfo> allhfsInfos, List<HomeForSaleInfo> acthfsInfos,
            GlobalEnums.CommunityStatus communityStatus, GlobalEnums.SearchStatus statusforSearch, string stProAbbr, string priceOverrideText, string status, string comPrice, string statusFlag, double lat, double lng, Item city, Item state,
            Item division, List<ID> avaiImage, SubcommunityInfo scinfo, PromoInfo proInfo, string liveChatSkill, string mappinCompanyAbb)
        {
            var phoneNumber = GetIhcPhoneDetails(item);

            return new CommunityInfo
            {
                CommunityID = item.ID.ToGuid(),
                CommunityParentID = item.ParentID.ToGuid(),
                CommunityDetailsURL = communityUrl,
                CommunityCityName = city.DisplayName,
                CommunityDivisionName = division.DisplayName,
                CommunityStateName = state.DisplayName,
                IsCurrentDomain = communityUrl.Contains(uri.Authority),
                Domain = crossSiteSpecification.Domain,
                HostName = crossSiteSpecification.HostName,
                CompanyName = crossSiteSpecification.CompanyName,
                CommunityName = !string.IsNullOrWhiteSpace(item["Community Name"]) ? item["Community Name"] : item.DisplayName,
                SubCommunityName = scinfo.SubcommunityName,
                Name = string.Format("{0}{1}", !string.IsNullOrWhiteSpace(item["Community Name"]) ? item["Community Name"] : item.DisplayName, !string.IsNullOrEmpty(scinfo.SubcommunityName) ? ": " + scinfo.SubcommunityName : string.Empty),
                SubcommunityList = scInfos,
                CommunityLegacyID = item["Community Legacy ID"],
                CommunityStatus = communityStatus,
                CommunityStatusId = item["Community Status"],
                StatusforSearch = statusforSearch,
                MapPinStatus = status,
                CommunityPrice = comPrice == "$0" ? string.Empty : comPrice,
                PriceOverrideText = priceOverrideText,
                CommunityDescription = item["Community Description"],
                CommunityPPCDescription = item["Community PPC Description"],
                CommunityDisclaimerImage = item["Community Disclaimer Image"],
                CommunityLogo = GetMediaURL(item, SCIDs.CommunityFields.CommunityLogoID),
                CommunityImageStatusFlag = statusFlag,
                CommunityStatusChangeDate = item["Community Status Change Date"],
                CommunityBrochure = item["Community Brochure"],
                CommunityLatitude = lat,
                CommunityLongitude = lng,
                StreetAddress1 = item["Street Address 1"],
                StreetAddress2 = item["Street Address 2"],
                AddressLink = GetAddressLink(item, city, stProAbbr),
                City = city.DisplayName,
                StateProvince = state.DisplayName,
                StateProvinceAbbreviation = stProAbbr,
                ZipPostalCode = item["Zip or Postal Code"],
                Phone = !string.IsNullOrWhiteSpace(phoneNumber) ? phoneNumber : item["Phone"],
                Fax = item["Fax"],
                SchoolDistrict = item["School District"],
                AlternateImage = GetMediaURL(item, SCIDs.CommunityFields.AlternateImageID),
                SitePlanImage = GetMediaURL(item, SCIDs.CommunityFields.SitePlanImageID),
                HomeForSaleList = allhfsInfos,
                PlanList = allPlans,
                CompanyNameAbbreviation = mappinCompanyAbb.ToLower(),
                CommunityCardImage = GetAvailableMediaUrl(item, avaiImage),
                SquareFootage = GetSquareFoot(allPlans, acthfsInfos),
                Bedrooms = GetBedrooms(allPlans, acthfsInfos),
                Bathrooms = GetBathrooms(allPlans, acthfsInfos),
                Stories = GetStory(allPlans, acthfsInfos, SCCurrentDeviceType),
                Garage = GetGarage(allPlans, acthfsInfos),
                SpecialIncentivesPromotions = string.Empty, //"Up T0 50% OFF Builder Options!" TODO: where is this field?
                CommunityZoom = 12,
                ClusteredMapIcon = string.Format("/Images/mappins/clustered_{0}.png", stProAbbr),
                SingleClusteredMapIcon = string.Format("/Images/mappins/{0}.png", stProAbbr),
                DivisionOf = division.DisplayName,
                PromoInfo = proInfo,
                LiveChatSkill = liveChatSkill,
                ContactName = GetContactName(item)
            };
        }

        #endregion Favorites

        public List<CommunityInfo> HydrateCommunities(string selectedPath, string favorites, bool isPPC, bool isRealtor)
        {
            IEnumerable<Item> communityItems = new List<Item>();

            communityItems = isPPC ? GetCommunitiesByCurrentDomain(selectedPath) : GetCommunities(selectedPath, true);
            communityItems = communityItems.Concat(GetLinkedCommunities(selectedPath, true));
            return HydrateCommunitiesByItems(communityItems, selectedPath, favorites, isRealtor).Distinct(new CommunityInfoComparer()).ToList();
        }

        public List<CommunityInfo> HydrateCommunitiesHTH(string selectedPath, string favorites, bool isPPC, bool isRealtor, bool isHTH)
        {
            IEnumerable<Item> communityItems = new List<Item>();
            communityItems = isPPC ? GetCommunitiesByCurrentDomainHTH(selectedPath) : GetCommunities(selectedPath);
            communityItems = communityItems.Concat(GetLinkedCommunities(selectedPath, true));
            return HydrateCommunitiesByItems(communityItems, selectedPath, favorites, isRealtor).Distinct(new CommunityInfoComparer()).ToList();
        }

        public List<CommunityInfo> HydrateCommunitiesByItems(IEnumerable<Item> communityItems, string selectedPath, string favorites, bool isRealtor)
        {
            var cInfos = new List<CommunityInfo>();

            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            //Do not change this order - Site Plan Image, Community Logo, Alternate Image
            var avaiImage = new List<ID>
				{
					//SCIDs.CommunityFields.SitePlanImageID,
					SCIDs.CommunityFields.CommunityLogoID,
					SCIDs.CommunityFields.AlternateImageID
				};

            var mappinCompanyAbb = Sitecore.Context.GetDeviceName().GetCurrentCompanyNameAbbreviation();

            foreach (var item in communityItems)
            {
                var lat = item["Community Latitude"].StringToDouble();
                var lng = item["Community Longitude"].StringToDouble();

                //if community does have lat and lng
                if (lat != 0.0 && lng != 0.0)
                {
                    var crossSiteSpecification = item.GetCrossSiteSpecification(sitecoreUrlOptions, uri);

                    var communityStatus = SCUtils.CommunityCurrentStatus(item["Community Status"]);
                    var statusforSearch = SCUtils.SearchStatus(item["Status for Search"]);

                    var status = communityStatus.ToString().ToLower();
                    status = FixSearchStatus(status);

                    var city = item.Parent;
                    var division = city.Parent;
                    var state = division.Parent;

                    var communityUrl = crossSiteSpecification.SiteSpecificUrl;
                    var stProAbbr = state.DisplayName.GetStateAbbreviationByState().ToString();
                    var priceOverrideText = item["Price Override Text"];
                    var announcementeText = item["Announcement Text"];

                    var statusFlag = string.Empty;
                    switch (communityStatus)
                    {
                        case GlobalEnums.CommunityStatus.ComingSoon:
                            statusFlag = string.Format("/Images/{0}/comingsoon_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;

                        case GlobalEnums.CommunityStatus.Closeout:
                            statusFlag = string.Format("/Images/{0}/closeout_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;

                        case GlobalEnums.CommunityStatus.PreSelling:
                            statusFlag = string.Format("/Images/{0}/preselling_flag.png", SCCurrentDeviceNameAbbreviation);
                            break;
                    }

                    //Filter SubcommunityInfo
                    var subCommunitySearchKey = CacheKeys.GetSubCommunitySearchKey(item.ID.ToString());
                    var scInfos = WebCache.Get(subCommunitySearchKey, () => GetSubcommunities(item.ID));

                    var plansSearchKey = CacheKeys.GetPlansSearchKey(item.ID.ToString());

                    //TODO: refactor getplans using lucene
                    var allPlans = WebCache.Get(plansSearchKey, () => GetPlans(item, crossSiteSpecification, isRealtor));
                    
                    var communityPromoKey = CacheKeys.GetCommunityPromoKey(item.ID.ToString());
                    var proInfo = WebCache.Get(communityPromoKey, () => GetCommunityPromo(item.ID));
                    
                    var allhfsInfos = new List<HomeForSaleInfo>();
                    allhfsInfos.AddRange(allPlans.SelectMany(h => h.HomeForSaleList));

                    //After getting the homes, filter plans to avoid getting invalid plan data.
                    allPlans = allPlans.Where(s => s.PlanStatus == GlobalEnums.SearchStatus.Active && s.StatusforSearch == GlobalEnums.SearchStatus.Active).ToList();

                    var prilist = allPlans.Where(c => c.PricedfromValue > 0.0).ToList();
                    var comPrice = string.IsNullOrEmpty(priceOverrideText)
                                       ? (prilist.Count > 0 ? string.Format("Starting From {0:c0}", prilist.Min(p => p.PricedfromValue)) : string.Empty)
                                       : priceOverrideText;

                    if (!isRealtor)
                    {
                        if (scInfos.Count > 0)
                            cInfos.AddRange(
                                scInfos.Select(
                                    scinfo =>
                                    GetCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos, communityStatus,
                                                       statusforSearch, stProAbbr, priceOverrideText, status, comPrice, statusFlag, lat, lng,
                                                       city, state, division, avaiImage, favorites, scinfo, proInfo, mappinCompanyAbb, announcementeText)));
                        else
                            cInfos.Add(GetCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos, communityStatus, statusforSearch, stProAbbr, priceOverrideText, status, comPrice,
                                                          statusFlag, lat, lng, city, state, division, avaiImage, favorites,
                                                          new SubcommunityInfo(), proInfo, mappinCompanyAbb, announcementeText));
                    }
                    else
                    {
                        if (allhfsInfos.Count > 0)
                        {
                            if (scInfos.Count > 0)
                                cInfos.AddRange(
                                    scInfos.Select(
                                        scinfo =>
                                        GetCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos, communityStatus,
                                                           statusforSearch, stProAbbr, priceOverrideText, status, comPrice, statusFlag, lat, lng,
                                                           city, state, division, avaiImage, favorites, scinfo, proInfo, mappinCompanyAbb, announcementeText, true)));
                            else
                                cInfos.Add(GetCommunitiyItems(item, communityUrl, uri, crossSiteSpecification, scInfos, allPlans, allhfsInfos,
                                                              communityStatus, statusforSearch, stProAbbr, priceOverrideText, status, comPrice,
                                                              statusFlag, lat, lng, city, state, division, avaiImage, favorites,
                                                              new SubcommunityInfo(), proInfo, mappinCompanyAbb, announcementeText, true));
                        }
                    }
                }
            }

            return cInfos;
        }

        private CommunityInfo GetCommunitiyItems(Item item, string communityUrl, Uri uri, CrossSiteSpecification crossSiteSpecification, List<SubcommunityInfo> scInfos, List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos,
            GlobalEnums.CommunityStatus communityStatus, GlobalEnums.SearchStatus statusforSearch, string stProAbbr, string priceOverrideText, string status, string comPrice, string statusFlag, double lat, double lng, Item city, Item state,
            Item division, List<ID> avaiImage, string favorites, SubcommunityInfo scinfo, PromoInfo proInfo, string mappinCompanyAbb, string announcementText, bool isRealtor = false)
        {
            return new CommunityInfo
            {
                CommunityID = item.ID.ToGuid(),
                CommunityParentID = item.ParentID.ToGuid(),
                CommunityDetailsURL = communityUrl,
                CommunityCityName = city.DisplayName,
                CommunityDivisionName = division.DisplayName,
                CommunityStateName = state.DisplayName,
                IsCurrentDomain = communityUrl.Contains(uri.Authority),
                Domain = crossSiteSpecification.Domain,
                HostName = crossSiteSpecification.HostName,
                CompanyName = crossSiteSpecification.CompanyName,
                CommunityName = !string.IsNullOrWhiteSpace(item["Community Name"]) ? item["Community Name"] : item.DisplayName,
                SubCommunityName = scinfo.SubcommunityName,
                Name = string.Format("{0}{1}", !string.IsNullOrWhiteSpace(item["Community Name"]) ? item["Community Name"] : item.DisplayName, !string.IsNullOrEmpty(scinfo.SubcommunityName) ? ": " + scinfo.SubcommunityName : string.Empty),
                SubcommunityList = scInfos,
                CommunityLegacyID = item["Community Legacy ID"],
                CommunityStatus = communityStatus,
                CommunityStatusId = item["Community Status"],
                StatusforSearch = statusforSearch,
                MapPinStatus = status,
                CommunityPrice = comPrice == "$0" ? string.Empty : comPrice,
                PriceOverrideText = priceOverrideText,
                CommunityDescription = isRealtor ? item["Community Description"] : string.Empty,
                CommunityPPCDescription = isRealtor ? item["Community PPC Description"] : string.Empty,
                CommunityDisclaimerImage = item["Community Disclaimer Image"],
                CommunityLogo = GetMediaURL(item, SCIDs.CommunityFields.CommunityLogoID),
                CommunityImageStatusFlag = statusFlag,
                CommunityBrochure = item["Community Brochure"],
                CommunityLatitude = lat,
                CommunityLongitude = lng,
                StreetAddress1 = item["Street Address 1"],
                StreetAddress2 = item["Street Address 2"],
                AddressLink = GetAddressLink(item, city, stProAbbr),
                City = city.DisplayName,
                StateProvince = state.DisplayName,
                StateProvinceAbbreviation = stProAbbr,
                ZipPostalCode = item["Zip or Postal Code"],
                Phone = item["Phone"],
                Fax = item["Fax"],
                SchoolDistrict = item["School District"],
                AlternateImage = GetMediaURL(item, SCIDs.CommunityFields.AlternateImageID),
                SitePlanImage = GetMediaURL(item, SCIDs.CommunityFields.SitePlanImageID),
                hasSitePlan = (GetMediaURL(item, SCIDs.CommunityFields.SitePlanImageID) != "" || item["Site Plan Page Description"] != "") ? true : false,
                HomeForSaleList = hfsInfos,
                PlanList = pInfos,
                CompanyNameAbbreviation = mappinCompanyAbb.ToLower(), //crossSiteSpecification.CompanyAbbreviation.ToLower(),
                CommunityCardImage = GetAvailableMediaUrl(item, avaiImage),
                //SquareFootage = GetSquareFoot(pInfos, hfsInfos),
                //Bedrooms = ${CommunityPrice}GetBedrooms(pInfos, hfsInfos),
                //Bathrooms = GetBathrooms(pInfos, hfsInfos),
                //Stories = GetStory(pInfos, hfsInfos, SCCurrentDeviceType),
                //Garage = GetGarage(pInfos, hfsInfos),
                //Sanjeevi
                SquareFootage = !string.IsNullOrWhiteSpace(item["SqFt Override Text"]) ? item["SqFt Override Text"] : GetSquareFoot(pInfos, hfsInfos),
                Bedrooms = !string.IsNullOrWhiteSpace(item["No of Beds Override Text"]) ? item["No of Beds Override Text"] : GetBedrooms(pInfos, hfsInfos),
                Bathrooms = !string.IsNullOrWhiteSpace(item["No of Baths Override Text"]) ? item["No of Baths Override Text"] : GetBathrooms(pInfos, hfsInfos),
                HalfBathrooms = !string.IsNullOrWhiteSpace(item["No of Half Baths Override Text"]) ? item["No of Half Baths Override Text"] : GetHalfBathrooms(pInfos, hfsInfos),
                Stories = !string.IsNullOrWhiteSpace(item["No of Story Homes Override Text"]) ? item["No of Story Homes Override Text"] : GetStory(pInfos, hfsInfos, SCCurrentDeviceType),
                Garage = !string.IsNullOrWhiteSpace(item["No of Car Garage Override Text"]) ? item["No of Car Garage Override Text"] : GetGarage(pInfos, hfsInfos),

                SpecialIncentivesPromotions = string.Empty, //"Up T0 50% OFF Builder Options!" TODO: where is this field?
                IsFavorite = !string.IsNullOrWhiteSpace(favorites) && favorites.ToUpper().Contains(item.ID.ToString().ToUpper()),
                CommunityZoom = 12,
                ClusteredMapIcon = string.Format("/Images/mappins/clustered_{0}.png", stProAbbr),
                SingleClusteredMapIcon = string.Format("/Images/mappins/{0}.png", stProAbbr),
                DivisionOf = division.DisplayName,
                PromoInfo = proInfo,
                MinPrice = pInfos.Count > 0 ? pInfos.Min(mi => mi.PricedfromValue) : 0,
                MinSqFt = pInfos.Count > 0 ? pInfos.Min(mi => mi.SquareFootage) : hfsInfos.Count > 0 ? hfsInfos.Min(mi => mi.SquareFootage) : 0,
                MaxSqFt = pInfos.Count > 0 ? pInfos.Max(mi => mi.SquareFootage) : hfsInfos.Count > 0 ? hfsInfos.Max(mi => mi.SquareFootage) : 0,
                PriceRange = pInfos.Count > 0 ? pInfos.Where(m => m.PricedfromValue > 0).Select(mi => mi.PricedfromValue).Distinct().ToList() : new List<double>(),
                SqFtRange = pInfos.Count > 0 ? pInfos.Where(m => m.SquareFootage > 0).Select(mi => mi.SquareFootage).Distinct().ToList() : new List<int>(),
                HalfBath = pInfos.Count > 0 ? pInfos.Max(mi => mi.NumberofHalfBathrooms) : pInfos.Count > 0 ? pInfos.Min(mi => mi.NumberofHalfBathrooms) : hfsInfos.Count > 0 ? hfsInfos.Max(mi => mi.NumberofHalfBathrooms) : hfsInfos.Count > 0 ? hfsInfos.Min(mi => mi.NumberofHalfBathrooms) : 0,
                BedRange = pInfos.Count > 0 ? pInfos.Select(mi => mi.NumberofBedrooms).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Select(mi => mi.NumberofBedrooms).Distinct().ToList() : new List<int>(),
                BathRange = pInfos.Count > 0 ? pInfos.Select(mi => mi.NumberofBathrooms).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Select(mi => mi.NumberofBathrooms).Distinct().ToList() : new List<int>(),
                GarageRange = pInfos.Count > 0 ? pInfos.Select(mi => mi.NumberofGarages).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Select(mi => mi.NumberofGarages).Distinct().ToList() : new List<int>(),
                StoryRange = pInfos.Count > 0 ? pInfos.Select(mi => mi.NumberofStories).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Select(mi => mi.NumberofStories).Distinct().ToList() : new List<int>(),
                BonusRoomRange = pInfos.Count > 0 ? pInfos.Where(m => m.NumberofBonusRooms > 0).Select(mi => mi.NumberofBonusRooms).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Where(m => m.NumberofBonusRooms > 0).Select(mi => mi.NumberofBonusRooms).Distinct().ToList() : new List<int>(),
                DenRange = pInfos.Count > 0 ? pInfos.Where(m => m.NumberofDens > 0).Select(mi => mi.NumberofDens).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Where(m => m.NumberofDens > 0).Select(mi => mi.NumberofDens).Distinct().ToList() : new List<int>(),
                SolariumRange = pInfos.Count > 0 ? pInfos.Where(m => m.NumberofSolariums > 0).Select(mi => mi.NumberofSolariums).Distinct().ToList() : hfsInfos.Count > 0 ? hfsInfos.Where(m => m.NumberofSolariums > 0).Select(mi => mi.NumberofSolariums).Distinct().ToList() : new List<int>(),
                CurrentDevice = Convert.ToInt32(SCCurrentDeviceType),
                ContactName = GetContactName(item),
                AnnouncementText = announcementText
            };
        }

        private string GetContactName(Item item)
        {
            string contact = "";

            if (!string.IsNullOrEmpty(item["Sales Team Card"]))
            {
                string salesTeamInfo = item["Sales Team Card"].Split('|').First();
                Item itemTeamCard = Sitecore.Context.Database.GetItem(salesTeamInfo);

                if (itemTeamCard != null)
                {
                    contact = itemTeamCard.Fields["Name"].Value;
                }
            }

            return contact;
        }

        private string GetAddressLink(Item item, Item city, string stProAbbr)
        {
            var useCoordinates = item["Use Coordinates for Route Generation"] == "1";
            var lat = item["Community Latitude"];
            var lon = item["Community Longitude"];
            if (useCoordinates && !string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lon))
                return string.Format("{0},{1}", lat, lon);
            return string.Format("{0},{1},{2}, {3} {4}", item["Street Address 1"], item["Street Address 2"], city.DisplayName, stProAbbr, item["Zip or Postal Code"]);
        }

        public List<CommunityInfo> GetAllActiveCommunitiesBySearchHTH(string selectedPath, string sortBy)
        {
            var communitySearchKey = CacheKeys.GetCommunitySearchKey(selectedPath);
            var cInfos = WebCache.Get(communitySearchKey, () => HydrateCommunitiesHTH(selectedPath, string.Empty, true, false, true));

            return CommunitiesGroupByDomain(cInfos, sortBy, true);
        }

        public List<CommunityInfo> GetAllActiveCommunitiesBySearch(string selectedPath, string sortBy)
        {
            var newselectedPath = selectedPath.TrimStart('/').TrimUrlLastText();
            var communitySearchKey = CacheKeys.GetCommunitySearchKey(selectedPath);
            var cInfos = WebCache.Get(communitySearchKey, () => HydrateCommunities(newselectedPath, string.Empty, true, false));
            //TODO: time being commented
            //var cInfos = HydrateCommunities(newselectedPath, string.Empty, true, false);

            return CommunitiesGroupByDomain(cInfos, sortBy, true);
        }

        public SearchResults GetCommunitiesBySearch(string selectedPath, string sortBy, string favorites, bool isRealtor, Dictionary<string, string> filters, bool isHTH)
        {
            var cInfos = new List<CommunityInfo>();
            if (isHTH)
            {
                cInfos = HydrateCommunitiesHTH(selectedPath, string.Empty, true, false, true);
            }
            else
            {
                cInfos = HydrateCommunities(selectedPath, favorites, false, isRealtor);
            }

            var refinedcomInfos = filters != null ? RefinedCommunityInfo(filters, cInfos) : DefaultRefinedCommunityInfo(cInfos);

            return GetMapAndCommunityResults(cInfos, refinedcomInfos, sortBy, selectedPath, isRealtor, filters);
        }

        private List<CommunityInfo> DefaultRefinedCommunityInfo(IEnumerable<CommunityInfo> cInfos)
        {
            var pInfos = new List<PlanInfo>();
            pInfos.AddRange(cInfos.SelectMany(p => p.PlanList));

            var newcInfo = cInfos.ToList();
            if (pInfos.Count > 0)
            {
                var minPrice = CommonHelper.RoundPrice(pInfos.Min(mi => mi.PricedfromValue), true);
                var maxPrice = CommonHelper.RoundPrice(pInfos.Max(mi => mi.PricedfromValue), false);
                var minSqft = pInfos.Min(mi => mi.SquareFootage);
                var maxSqft = pInfos.Max(mi => mi.SquareFootage);

                var fltpri = newcInfo.Where(info => info.PriceRange.Any(price => price >= minPrice && price <= maxPrice)).ToList();
                newcInfo = fltpri;

                var filterSqft = newcInfo.Where(info => info.SqFtRange.Any(sq => sq >= minSqft && sq <= maxSqft)).ToList();
                newcInfo = filterSqft;

                //var filsts =
                //    newcInfo.Where(
                //        sts => sts.CommunityStatusId.ToString() != SCIDs.StatusIds.CommunityStatus.Closed.ToString().ToUpper()).ToList();
                //newcInfo = filsts;
            }

            return newcInfo.Distinct().ToList();
        }

        private List<CommunityInfo> RefinedCommunityInfo(Dictionary<string, string> filters, IEnumerable<CommunityInfo> cInfos)
        {
            //Filters
            var minPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingPrice.ToString()].StringToDouble() : 0;
            var maxPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingPrice.ToString()].StringToDouble() : 0;
            var minSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()].StringToInt32() : 0;
            var maxSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()].StringToInt32() : 0;
            var city = filters.ContainsKey(GlobalEnums.SearchFilterKeys.City.ToString()) ? filters[GlobalEnums.SearchFilterKeys.City.ToString()] : String.Empty;
            var comSts = filters.ContainsKey(GlobalEnums.SearchFilterKeys.CommunityStatus.ToString()) ? filters[GlobalEnums.SearchFilterKeys.CommunityStatus.ToString()] : String.Empty;
            var schl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()] : String.Empty;
            var avai = filters.ContainsKey(GlobalEnums.SearchFilterKeys.Availability.ToString()) ? filters[GlobalEnums.SearchFilterKeys.Availability.ToString()] : String.Empty;
            var bonusDenSolar = filters.ContainsKey(GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()) ? filters[GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()] : String.Empty;

            var noofBeds = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()] : String.Empty;
            var beds = noofBeds.StringToList();

            var noofBaths = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()] : String.Empty;
            var baths = noofBaths.StringToList();

            var noofGarages = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()] : String.Empty;
            var garage = noofGarages.StringToList();

            var noofStories = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofStories.ToString()) ? filters[GlobalEnums.SearchFilterKeys.NumberofStories.ToString()] : String.Empty;
            var story = noofStories.StringToList();

            var newcInfo = cInfos.ToList();

            var fltpri = newcInfo.Where(info => info.PriceRange.Any(price => price >= minPrice && price <= maxPrice)).ToList();
            newcInfo = fltpri;

            //filter by city
            if (city != "0")
            {
                var fltcity = newcInfo.Where(ci => ci.City == city).ToList();
                newcInfo = fltcity;
            }

            //Beds
            if (beds.Count > 0)
            {
                var filterbeds = new List<CommunityInfo>();
                foreach (var st in beds)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        foreach (var citm in newcInfo.Where(n => n.BedRange.Count > 0))
                        {
                            filterbeds.AddRange(from comItm in citm.BedRange where comItm >= b select citm);
                        }
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterbeds.AddRange(newcInfo.Where(pr => pr.BedRange.Contains(b)).ToList());
                    }
                }

                newcInfo = filterbeds;
            }

            //baths
            if (baths.Count > 0)
            {
                var filterbaths = new List<CommunityInfo>();
                foreach (var st in baths)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();

                        foreach (var citm in newcInfo.Where(n => n.BathRange.Count > 0))
                        {
                            filterbaths.AddRange(from comItm in citm.BathRange where comItm >= b select citm);
                        }
                    }
                    else if (st.Contains("½"))
                    {
                        b = st.Replace("½", "").StringToInt32();
                        filterbaths.AddRange(newcInfo.Where(pr => pr.BathRange.Contains(b) && pr.HalfBath > 0).ToList());
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterbaths.AddRange(newcInfo.Where(pr => pr.BathRange.Contains(b)).ToList());
                    }
                }

                newcInfo = filterbaths;
            }

            //garage
            if (garage.Count > 0)
            {
                var filtergarages = new List<CommunityInfo>();
                foreach (var st in garage)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        foreach (var citm in newcInfo.Where(n => n.GarageRange.Count > 0))
                        {
                            filtergarages.AddRange(from comItm in citm.GarageRange where comItm >= b select citm);
                        }
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filtergarages.AddRange(newcInfo.Where(pr => pr.GarageRange.Contains(b)).ToList());
                    }
                }

                newcInfo = filtergarages;
            }

            //story
            if (story.Count > 0)
            {
                var filterStories = new List<CommunityInfo>();
                foreach (var st in story)
                {
                    int b;
                    if (st.Contains("+"))
                    {
                        b = st.Replace("+", "").StringToInt32();
                        foreach (var citm in newcInfo.Where(n => n.StoryRange.Count > 0))
                        {
                            filterStories.AddRange(from comItm in citm.StoryRange where comItm >= b select citm);
                        }
                    }
                    else
                    {
                        b = st.StringToInt32();
                        filterStories.AddRange(newcInfo.Where(pr => pr.StoryRange.Contains(b)).ToList());
                    }
                }

                newcInfo = filterStories;
            }

            //bonusRooms
            if (bonusDenSolar == "B")
            {
                var filterBonusRooms = new List<CommunityInfo>();
                foreach (var citm in newcInfo.Where(n => n.BonusRoomRange.Count > 0))
                {
                    filterBonusRooms.AddRange(from comItm in citm.BonusRoomRange where comItm > 0 select citm);
                }
                newcInfo = filterBonusRooms;
            }

            //dens
            if (bonusDenSolar == "D")
            {
                var filterDens = new List<CommunityInfo>();
                foreach (var citm in newcInfo.Where(n => n.DenRange.Count > 0))
                {
                    filterDens.AddRange(from comItm in citm.DenRange where comItm > 0 select citm);
                }
                newcInfo = filterDens;
            }

            //solarium
            if (bonusDenSolar == "S")
            {
                var filterSolariumRange = new List<CommunityInfo>();
                foreach (var citm in newcInfo.Where(n => n.SolariumRange.Count > 0))
                {
                    filterSolariumRange.AddRange(from comItm in citm.SolariumRange where comItm > 0 select citm);
                }
                newcInfo = filterSolariumRange;
            }

            var filterSqft = newcInfo.Where(info => info.SqFtRange.Any(sq => sq >= minSqft && sq <= maxSqft)).ToList();
            newcInfo = filterSqft;

            //filter by school dst
            if (schl != "0")
            {
                var filterscl = newcInfo.Where(scl => scl.SchoolDistrict == schl.ToUpper()).ToList();
                newcInfo = filterscl;
            }

            if (avai != "0")
            {
                var avaiFilterList = newcInfo.Where(comm => comm.HomeForSaleList.Any(a => a.AvailabilityItemId == avai.ToUpper())).ToList();
                newcInfo = avaiFilterList;
            }

            //filter by community status
            if (comSts != "0")
            {
                var filsts = newcInfo.Where(sts => sts.CommunityStatusId.ToString() == comSts.ToUpper()).ToList();
                newcInfo = filsts;
            }
            else
            {
                var filsts = newcInfo.Where(sts => sts.CommunityStatusId.ToString() != SCIDs.StatusIds.CommunityStatus.Closed.ToString().ToUpper()).ToList();
                newcInfo = filsts;
            }

            return newcInfo.Distinct().ToList();
        }

        public List<CommunityInfo> CommunitiesGroupByDomain(List<CommunityInfo> cInfos, string sortBy, bool isPPC)
        {
            var domain = Context.GetDeviceName().GetDomainFromDeviceName();

            cInfos = isPPC ? CommunitySort(cInfos, sortBy) : CommunitySortByDomain(cInfos, sortBy);
            var currentCompany = cInfos.Where(c => c.Domain == domain).ToList();
            var otherCompany = cInfos.Where(c => c.Domain != domain).ToList();

            var allCompanies = new List<CommunityInfo>();
            allCompanies.AddRange(currentCompany);
            allCompanies.AddRange(otherCompany);

            return allCompanies;
        }

        private IEnumerable<SearchCentroids> HydrateSearchCentroids(IEnumerable<Item> centroidItems)
        {
            var searchCentroids = centroidItems.Select(item => new SearchCentroids()
            {
                Name = item.DisplayName,
                CentroidLatitude = item["Search Centroid Latitude"].StringToDouble(),
                CentroidLongitude = item["Search Centroid Longitude"].StringToDouble(),
                CentroidZoom = item["Search Centroid Zoom"].StringToInt32()
            }).ToList();

            return searchCentroids.GroupBy(c => new { c.Name, c.CentroidLatitude, c.CentroidLongitude, c.CentroidZoom })
                                             .Select(g => new SearchCentroids
                                             {
                                                 Name = g.Key.Name,
                                                 CentroidLatitude = g.Key.CentroidLatitude,
                                                 CentroidLongitude = g.Key.CentroidLongitude,
                                                 CentroidZoom = g.Key.CentroidZoom
                                             }).ToList();
        }

        private SearchResults GetMapAndCommunityResults(List<CommunityInfo> cInfos, List<CommunityInfo> refinedcomInfos, string sortBy, string selectedPath, bool isRealtor, Dictionary<string, string> filters)
        {
            var designStudioInfo = GetDesignStudio(selectedPath);
            var dsInfoforMap = new List<CommunityInfo>();
            foreach (var item in designStudioInfo)
            {
                dsInfoforMap.Add(new CommunityInfo()
                {
                    CommunityName = item.City + " " + item.DesignStudioName,
                    CommunityPrice = string.Empty,
                    SquareFootage = string.Empty,
                    Bedrooms = string.Empty,
                    CommunityLatitude = item.DesignStudioLatitude,
                    CommunityLongitude = item.DesignStudioLongitude,
                    Bathrooms = string.Empty,
                    CommunityDetailsURL = item.DesignStudioDetailsUrl,
                    CommunityID = item.DesignStudioId,
                    CommunityCardImage = string.Empty,
                    IsCurrentDomain = item.IsCurrentDomain,
                    DynamicMapIcon = item.MapIcon,
                    City = item.City,
                    StateProvince = item.StateProvince,
                    StateProvinceAbbreviation = item.StateProvinceAbbreviation,
                    ZipPostalCode = item.ZipPostalCode,
                    MapPinStatus = GlobalEnums.MapRenderingType.DesignCenter.ToString(),
                    ClusteredMapIcon = item.ClusteredMapIcon,
                    SingleClusteredMapIcon = item.SingleClusteredMapIcon,
                    Phone = item.Phone,
                    OfficeHoursDay = item.Hours,
                    StreetAddress1 = item.StreetAddress1
                });
            }

            var communityInfo = filters == null
                                    ? CommunitiesGroupByDomain(cInfos, sortBy, false)
                                    : CommunitiesGroupByDomain(refinedcomInfos, sortBy, false);

            communityInfo.AddRange(dsInfoforMap);

            communityInfo = FetchMapPins(communityInfo, isRealtor, filters);

            var searchResults = new SearchResults
            {
                CommunityInfo = communityInfo.ToList(),
                //	MapCommunityInfo =communityInfo,
                MapCommunityInfo = null,
                SearchFacet = GetSearchFacet(cInfos, selectedPath, refinedcomInfos, filters)
            };

            return GetMapBounds(searchResults);
        }

        public SaveItemResults GetMapBounds(SaveItemResults searchResults)
        {
            if (searchResults.MapCommunityInfo.Count > 0)
            {
                //Map bounds
                var minlat = searchResults.MapCommunityInfo.Min(ml => ml.CommunityLatitude);
                var minlng = searchResults.MapCommunityInfo.Min(ml => ml.CommunityLongitude);
                var maxlat = searchResults.MapCommunityInfo.Max(ml => ml.CommunityLatitude);
                var maxlng = searchResults.MapCommunityInfo.Max(ml => ml.CommunityLongitude);

                //Assign min max values if it is zero
                minlat = (minlat == 0.0) ? maxlat : minlat;
                minlng = (minlng == 0.0) ? maxlng : minlng;
                maxlat = (maxlat == 0.0) ? minlat : maxlat;
                maxlng = (maxlng == 0.0) ? minlng : maxlng;

                searchResults.MapBounds = new MapBounds()
                {
                    MinLatitude = minlat,
                    MaxLongitude = maxlng,
                    MaxLatitude = maxlat,
                    MinLongitude = minlng
                };
            }
            else
            {
                searchResults.DefaultMapPoint = new DefaultMapPoint() { Latitude = 40.111689, Longitude = -95.888672 };
                //TODO: Have to remove this.
            }

            return searchResults;
        }

        public SearchResults GetMapBounds(SearchResults searchResults)
        {
            if (searchResults.CommunityInfo.Count > 0)
            {
                //Map bounds
                var minlat = searchResults.CommunityInfo.Min(ml => ml.CommunityLatitude);
                var minlng = searchResults.CommunityInfo.Min(ml => ml.CommunityLongitude);
                var maxlat = searchResults.CommunityInfo.Max(ml => ml.CommunityLatitude);
                var maxlng = searchResults.CommunityInfo.Max(ml => ml.CommunityLongitude);

                //Assign min max values if it is zero
                minlat = (minlat == 0.0) ? maxlat : minlat;
                minlng = (minlng == 0.0) ? maxlng : minlng;
                maxlat = (maxlat == 0.0) ? minlat : maxlat;
                maxlng = (maxlng == 0.0) ? minlng : maxlng;

                searchResults.MapBounds = new MapBounds()
                {
                    MinLatitude = minlat,
                    MaxLongitude = maxlng,
                    MaxLatitude = maxlat,
                    MinLongitude = minlng
                };
            }
            else
            {
                searchResults.DefaultMapPoint = new DefaultMapPoint() { Latitude = 40.111689, Longitude = -95.888672 };
                //TODO: Have to remove this.
            }

            return searchResults;
        }

        private List<MultiNameValueStatus> GetMultiButtonValuesBathandHalf(int totalVal, IEnumerable<int> filterdList, int totalHalfVal, IEnumerable<int> filterdHalfList, List<string> selecteditem, bool isFaceted)
        {
            var nList = new List<MultiNameValueStatus>();
            var i = 1;
            var half = "½";

            while (i <= totalVal)
            {
                string newVal = GlobalEnums.FacetStatus.On.ToString();
                newVal = !isFaceted
                             ? GlobalEnums.FacetStatus.On.ToString()
                             : (filterdList.Any() ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString());

                foreach (var fl in filterdList.Where(v => v != 0))
                {
                    if (fl == i)
                    {
                        newVal = GlobalEnums.FacetStatus.On.ToString();
                        break;
                    }
                    newVal = GlobalEnums.FacetStatus.Off.ToString();
                }

                bool isSelected;
                isSelected = isFaceted && (selecteditem.Any());// || selecteditem.Any(fl => fl == i.ToString());

                foreach (var fl in selecteditem)
                {
                    var fls = fl.Contains("+") ? fl.Replace("+", "") : fl;
                    if (fls == i.ToString())
                    {
                        isSelected = true;
                        break;
                    }

                    isSelected = false;
                }

                nList.Add(new MultiNameValueStatus { Value = i.ToString(), ItemStatus = isSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isSelected });
                if (totalHalfVal > 0)
                {
                    if (isFaceted && !filterdHalfList.Any(v => v != 0))
                    {
                        newVal = GlobalEnums.FacetStatus.Off.ToString();
                    }
                    bool isHlfSelected = false;
                    foreach (var fl in selecteditem)
                    {
                        if (fl == i.ToString() + half)
                        {
                            isHlfSelected = true;
                            break;
                        }
                        isHlfSelected = false;
                    }

                    nList.Add(new MultiNameValueStatus { Value = i + half, ItemStatus = isHlfSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isHlfSelected });
                }

                if (nList.Count >= 7)
                    break;

                i++;
            }
            return nList.OrderBy(o => o.Value).ToList();
        }

        private List<MultiValueStatus> GetMultiButtonValues(int totalVal, IEnumerable<int> filterdList, List<string> selecteditem, bool isFaceted)
        {
            var nList = new List<MultiValueStatus>();
            var i = 1;
            while (i <= totalVal)
            {
                string newVal = GlobalEnums.FacetStatus.On.ToString();
                newVal = !isFaceted
                             ? GlobalEnums.FacetStatus.On.ToString()
                             : (filterdList.Any() ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString());

                var newfilterdList = filterdList.Where(v => v != 0);
                if (isFaceted && !newfilterdList.Any())
                {
                    newVal = GlobalEnums.FacetStatus.Off.ToString();
                }
                else
                {
                    foreach (var fl in newfilterdList)
                    {
                        if (fl == i)
                        {
                            newVal = GlobalEnums.FacetStatus.On.ToString();
                            break;
                        }
                        newVal = GlobalEnums.FacetStatus.Off.ToString();
                    }
                }

                bool isSelected = isFaceted && (selecteditem.Any());

                foreach (var fl in selecteditem)
                {
                    var fls = fl.Contains("+") ? fl.Replace("+", "") : fl;
                    if (fls == i.ToString())
                    {
                        isSelected = true;
                        break;
                    }
                    isSelected = false;
                }

                nList.Add(new MultiValueStatus { Value = i, ItemStatus = isSelected ? GlobalEnums.FacetStatus.On.ToString() : newVal, IsSelected = isSelected });

                if (i >= 8 && isSelected)
                {
                    var updatedItem = new MultiValueStatus()
                    {
                        Value = 7,
                        ItemStatus = GlobalEnums.FacetStatus.On.ToString(),
                        IsSelected = true
                    };
                    nList.RemoveAt(6);
                    nList.Add(updatedItem);
                }
                else if (i >= 8 && !isSelected)
                {
                    var updatedItem = new MultiValueStatus()
                    {
                        Value = 7,
                        ItemStatus = newVal,
                        IsSelected = false
                    };
                    nList.RemoveAt(6);
                    nList.Add(updatedItem);
                }
                if (i >= 7)
                    break;
                i++;
            }
            return nList.OrderBy(o => o.Value).ToList();
        }

        public SearchFacet GetSearchFacet(List<CommunityInfo> cInfos, string selectedPath, List<CommunityInfo> flteredcInfos, Dictionary<string, string> filters)
        {
            var searchFacet = new SearchFacet();

            var pInfos = new List<PlanInfo>();
            var hfsInfos = new List<HomeForSaleInfo>();

            var newpInfos = new List<PlanInfo>();
            var newhfsInfos = new List<HomeForSaleInfo>();
            var avblCities = new List<NameandValue>();

            var allBonusDensSolar = new List<NameandValue>()
				{
					new NameandValue() {Name = "Bonus Room", Value = "B"},
					new NameandValue() {Name = "Den", Value = "D"},
					new NameandValue() {Name = "Solarium", Value = "S"}
				};
            var aviBonusDensSolar = new List<NameandValue>();

            bool bounsRooms;
            bool dens;
            bool solarium;

            searchFacet.Division = "0";
            flteredcInfos = flteredcInfos ?? new List<CommunityInfo>();
            if (flteredcInfos.Count > 0)
            {
                newpInfos.AddRange(flteredcInfos.SelectMany(p => p.PlanList));
                newhfsInfos.AddRange(flteredcInfos.SelectMany(h => h.HomeForSaleList));
                avblCities = flteredcInfos.GroupBy(g => new { g.City })
                                       .Select(s => new NameandValue { Name = s.Key.City, Value = s.Key.City }).OrderBy(o => o.Name).ToList();
                bounsRooms = flteredcInfos.Any(b => b.BonusRoomRange.Count > 0);
                dens = flteredcInfos.Any(b => b.DenRange.Count > 0);
                solarium = flteredcInfos.Any(b => b.SolariumRange.Count > 0);
            }
            else
            {
                newpInfos.AddRange(cInfos.SelectMany(p => p.PlanList));
                newhfsInfos.AddRange(cInfos.SelectMany(h => h.HomeForSaleList));
                avblCities = cInfos.GroupBy(g => new { g.City })
                                       .Select(s => new NameandValue { Name = s.Key.City, Value = s.Key.City }).OrderBy(o => o.Name).ToList();
                bounsRooms = cInfos.Any(b => b.BonusRoomRange.Count > 0);
                dens = cInfos.Any(b => b.DenRange.Count > 0);
                solarium = cInfos.Any(b => b.SolariumRange.Count > 0);
            }

            if (bounsRooms)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Bonus Room", Value = "B" });
            if (dens)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Den", Value = "D" });
            if (solarium)
                aviBonusDensSolar.Add(new NameandValue() { Name = "Solarium", Value = "S" });

            pInfos.AddRange(cInfos.SelectMany(p => p.PlanList));
            hfsInfos.AddRange(cInfos.SelectMany(h => h.HomeForSaleList));

            //if (hfsInfos.Count > 0)
            //{
            var beds = new List<string>();
            var baths = new List<string>();
            var garage = new List<string>();
            var story = new List<string>();
            var bonDenSol = new List<NameandValue>();

            if (filters != null)
            {
                var noofBeds = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString())
                                   ? filters[GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString()]
                                   : String.Empty;
                beds = noofBeds.StringToList();

                var noofBaths = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString())
                                    ? filters[GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString()]
                                    : String.Empty;
                baths = noofBaths.StringToList();

                var noofGarages = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofGarage.ToString())
                                      ? filters[GlobalEnums.SearchFilterKeys.NumberofGarage.ToString()]
                                      : String.Empty;
                garage = noofGarages.StringToList();

                var noofStories = filters.ContainsKey(GlobalEnums.SearchFilterKeys.NumberofStories.ToString())
                                      ? filters[GlobalEnums.SearchFilterKeys.NumberofStories.ToString()]
                                      : String.Empty;
                story = noofStories.StringToList();
            }

            searchFacet.NumberofBedrooms = GetMultiButtonValues(CommonHelper.GetFacetBeds(pInfos, hfsInfos), CommonHelper.GetAvailableFacetBeds(newpInfos, newhfsInfos), beds, filters != null);
            searchFacet.NumberofBathrooms = GetMultiButtonValuesBathandHalf(CommonHelper.GetFacetBaths(pInfos, hfsInfos), CommonHelper.GetAvailableFacetBaths(newpInfos, newhfsInfos), CommonHelper.GetFacetHalfBaths(pInfos, hfsInfos), CommonHelper.GetAvailableFacetHalfBaths(newpInfos, newhfsInfos), baths, filters != null);
            searchFacet.NumberofGarages = GetMultiButtonValues(CommonHelper.GetFacetGarages(pInfos, hfsInfos), CommonHelper.GetAvailableFacetGarages(newpInfos, newhfsInfos), garage, filters != null);
            searchFacet.NumberofHalfBathrooms = GetMultiButtonValues(CommonHelper.GetFacetHalfBaths(pInfos, hfsInfos), CommonHelper.GetAvailableFacetHalfBaths(newpInfos, newhfsInfos), new List<string>(), filters != null);
            searchFacet.NumberofStories = GetMultiButtonValues(CommonHelper.GetFacetStories(pInfos, hfsInfos), CommonHelper.GetAvailableFacetStories(newpInfos, newhfsInfos), story, filters != null);
            //}

            var allSchoolDistricts = GetSchoolDistricts(selectedPath);
            var allAvailability = GetAvailabilities();

            var communityStatus = new List<NameandId>
				{
					new NameandId() {Name = "Coming soon", ID = SCIDs.StatusIds.CommunityStatus.ComingSoon, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false},
					new NameandId() {Name = "Grand Opening", ID = SCIDs.StatusIds.CommunityStatus.GrandOpening, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false},
					new NameandId() {Name = "Open", ID =SCIDs.StatusIds.CommunityStatus.Open, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false},
					new NameandId() {Name = "Closeout", ID = SCIDs.StatusIds.CommunityStatus.Closeout, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false},
					new NameandId() {Name = "Closed", ID = SCIDs.StatusIds.CommunityStatus.Closed, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false},
					new NameandId() {Name = "Pre Selling", ID = SCIDs.StatusIds.CommunityStatus.PreSelling, ItemStatus = GlobalEnums.FacetStatus.On.ToString(), IsSelected = false}
				};

            if (pInfos.Count > 0)
            {
                searchFacet.MinPrice = CommonHelper.RoundPrice(pInfos.Min(mi => mi.PricedfromValue), true);
                searchFacet.MaxPrice = CommonHelper.RoundPrice(pInfos.Max(mi => mi.PricedfromValue), false);
                searchFacet.PriceTick = CommonHelper.FindPriceTick(searchFacet.MinPrice);
                searchFacet.MinSqFt = pInfos.Min(mi => mi.SquareFootage);
                searchFacet.MaxSqFt = pInfos.Max(mi => mi.SquareFootage);
                searchFacet.SqFtTick = 500;
            }

            if (filters != null)
            {
                ////Filters
                var minPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingPrice.ToString()].StringToDouble() : 0;
                var maxPrice = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingPrice.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingPrice.ToString()].StringToDouble() : 0;
                var minSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.StartingSqFt.ToString()].StringToInt32() : 0;
                var maxSqft = filters.ContainsKey(GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()) ? filters[GlobalEnums.SearchFilterKeys.EndingSqFt.ToString()].StringToInt32() : 0;
                var city = filters.ContainsKey(GlobalEnums.SearchFilterKeys.City.ToString()) ? filters[GlobalEnums.SearchFilterKeys.City.ToString()] : String.Empty;
                var comSts = filters.ContainsKey(GlobalEnums.SearchFilterKeys.CommunityStatus.ToString()) ? filters[GlobalEnums.SearchFilterKeys.CommunityStatus.ToString()] : String.Empty;
                var schl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString()] : String.Empty;
                var avai = filters.ContainsKey(GlobalEnums.SearchFilterKeys.Availability.ToString()) ? filters[GlobalEnums.SearchFilterKeys.Availability.ToString()] : String.Empty;
                var bodeso = filters.ContainsKey(GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()) ? filters[GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString()] : String.Empty;

                searchFacet.Division = filters.ContainsKey(GlobalEnums.SearchFilterKeys.DivisionArea.ToString()) ? (filters[GlobalEnums.SearchFilterKeys.DivisionArea.ToString()] ?? "0") : "0";

                if (pInfos.Count > 0)
                {
                    searchFacet.SelectedMinPrice = minPrice;
                    searchFacet.SelectedMaxPrice = maxPrice;
                    searchFacet.SelectedMinSqFt = minSqft;
                    searchFacet.SelectedMaxSqFt = maxSqft;
                }

                if (pInfos.Count > 0)
                {
                    searchFacet.MinPrice = CommonHelper.RoundPrice(newpInfos.Min(mi => mi.PricedfromValue), true);
                    searchFacet.MaxPrice = CommonHelper.RoundPrice(newpInfos.Max(mi => mi.PricedfromValue), false);
                    searchFacet.PriceTick = CommonHelper.FindPriceTick(searchFacet.MinPrice);
                    searchFacet.MinSqFt = newpInfos.Min(mi => mi.SquareFootage);
                    searchFacet.MaxSqFt = newpInfos.Max(mi => mi.SquareFootage);
                    searchFacet.SqFtTick = 500;
                }

                searchFacet.Cities = cInfos.GroupBy(g => new { g.City })
                                           .Select(
                                               s =>
                                               new NameandValue
                                               {
                                                   Name = s.Key.City,
                                                   Value = s.Key.City,
                                                   ItemStatus =
                                                       avblCities.Any(f => f.Name == s.Key.City)
                                                           ? GlobalEnums.FacetStatus.On.ToString()
                                                           : GlobalEnums.FacetStatus.Off.ToString(),
                                                   IsSelected = city == s.Key.City
                                               }).OrderBy(o => o.Name).ToList();

                var nwIds = flteredcInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var sIds = cInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var nameandIds =
                    sIds.Select(val => allSchoolDistricts.FirstOrDefault(s => s.ID.ToString() == val.Key.SchoolDistrict))
                        .Where(item => item != null)
                        .Select(item => new NameandId
                        {
                            ID = item.ID,
                            Name = item.DisplayName,
                            ItemStatus =
                                nwIds.Any(a => a.Key.SchoolDistrict == item.ID.ToString())
                                    ? GlobalEnums.FacetStatus.On.ToString()
                                    : GlobalEnums.FacetStatus.Off.ToString(),
                            IsSelected = schl.ToUpper().ToString() == item.ID.ToString()
                        }).ToList();
                searchFacet.SchoolDistricts = nameandIds.OrderBy(o => o.Name).ToList();

                var nwcomsts = flteredcInfos.GroupBy(g => new { g.CommunityStatusId }).Select(s => new { s.Key });
                var newCom =
                    communityStatus.Select(
                        item =>
                        new NameandId
                        {
                            ID = item.ID,
                            Name = item.Name,
                            ItemStatus =
                                nwcomsts.Any(a => a.Key.CommunityStatusId.ToString() == item.ID.ToString())
                                    ? GlobalEnums.FacetStatus.On.ToString()
                                    : GlobalEnums.FacetStatus.Off.ToString(),
                            IsSelected = item.ID.ToString() == comSts.ToUpper()
                        }).ToList();
                searchFacet.CommunityStatus = newCom;

                var aIds = cInfos.SelectMany(a => a.HomeForSaleList).Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var nwaIds = flteredcInfos.SelectMany(a => a.HomeForSaleList).Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var avaisIds =
                    aIds.Select(val => allAvailability.FirstOrDefault(s => s.ID.ToString() == val))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                            {
                                ID = item.ID,
                                Name = item.Name,
                                ItemStatus =
                                    nwaIds.Any(a => a.ToUpper() == item.ID.ToString())
                                        ? GlobalEnums.FacetStatus.On.ToString()
                                        : GlobalEnums.FacetStatus.Off.ToString(),
                                IsSelected = avai.ToUpper() == item.ID.ToString()
                            })
                        .ToList();
                searchFacet.Availability = avaisIds.OrderBy(o => o.Name).ToList();

                bonDenSol.AddRange(allBonusDensSolar.Select(bo => new NameandValue()
                {
                    Name = bo.Name,
                    Value = bo.Value,
                    ItemStatus = aviBonusDensSolar.Any(f => f.Value == bo.Value) ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString(),
                    IsSelected = bo.Value == bodeso
                }));

                searchFacet.BonusRoomsDensSolarium = bonDenSol;
            }
            else
            {
                if (pInfos.Count > 0)
                {
                    searchFacet.SelectedMinPrice = searchFacet.MinPrice;
                    searchFacet.SelectedMaxPrice = searchFacet.MaxPrice;
                    searchFacet.SelectedMinSqFt = searchFacet.MinSqFt;
                    searchFacet.SelectedMaxSqFt = searchFacet.MaxSqFt;
                }

                searchFacet.Cities = cInfos.GroupBy(g => new { g.City })
                                           .Select(
                                               s =>
                                               new NameandValue
                                               {
                                                   Name = s.Key.City,
                                                   Value = s.Key.City,
                                                   ItemStatus =
                                                       avblCities.Any(f => f.Name == s.Key.City)
                                                           ? GlobalEnums.FacetStatus.On.ToString()
                                                           : GlobalEnums.FacetStatus.Off.ToString(),
                                                   IsSelected = false
                                               }).OrderBy(o => o.Name).ToList();

                var sIds = cInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var nwIds = flteredcInfos.Where(n => n.SchoolDistrict != "").GroupBy(g => new { g.SchoolDistrict }).Select(s => new { s.Key });
                var nameandIds =
                    sIds.Select(val => allSchoolDistricts.FirstOrDefault(s => s.ID.ToString() == val.Key.SchoolDistrict))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                            {
                                ID = item.ID,
                                Name = item.DisplayName,
                                ItemStatus =
                               nwIds.Any(a => a.Key.SchoolDistrict == item.ID.ToString())
                                   ? GlobalEnums.FacetStatus.On.ToString()
                                   : GlobalEnums.FacetStatus.Off.ToString(),
                                IsSelected = false
                            })
                        .ToList();
                searchFacet.SchoolDistricts = nameandIds.OrderBy(o => o.Name).ToList();

                var nwcomsts = flteredcInfos.GroupBy(g => new { g.CommunityStatusId }).Select(s => new { s.Key });
                var newCom =
                    communityStatus.Select(
                        item =>
                        new NameandId
                        {
                            ID = item.ID,
                            Name = item.Name,
                            ItemStatus =
                                nwcomsts.Any(a => a.Key.CommunityStatusId.ToString() == item.ID.ToString())
                                    ? GlobalEnums.FacetStatus.On.ToString()
                                    : GlobalEnums.FacetStatus.Off.ToString(),
                            IsSelected = false
                        }).ToList();
                searchFacet.CommunityStatus = newCom;

                var aIds = cInfos.SelectMany(a => a.HomeForSaleList).Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var nwaIds = flteredcInfos.SelectMany(a => a.HomeForSaleList).Select(hs => hs.AvailabilityItemId).Where(n => n != "").Distinct().ToList();
                var avaisIds =
                    aIds.Select(val => allAvailability.FirstOrDefault(s => s.ID.ToString() == val))
                        .Where(item => item != null)
                        .Select(
                            item =>
                            new NameandId
                            {
                                ID = item.ID,
                                Name = item.Name,
                                ItemStatus =
                                     nwaIds.Any(a => a.ToUpper() == item.ID.ToString())
                                         ? GlobalEnums.FacetStatus.On.ToString()
                                         : GlobalEnums.FacetStatus.Off.ToString(),
                                IsSelected = false
                            })
                        .ToList();
                searchFacet.Availability = avaisIds.OrderBy(o => o.Name).ToList();

                bonDenSol.AddRange(allBonusDensSolar.Select(bo => new NameandValue()
                {
                    Name = bo.Name,
                    Value = bo.Value,
                    ItemStatus = aviBonusDensSolar.Any(f => f.Value == bo.Value) ? GlobalEnums.FacetStatus.On.ToString() : GlobalEnums.FacetStatus.Off.ToString(),
                    IsSelected = false
                }));
                searchFacet.BonusRoomsDensSolarium = bonDenSol;
            }

            return searchFacet;
        }

        /// <summary>
        /// Hydrate for HTH
        /// </summary>
        /// <param name="searchLocation"></param>
        /// <param name="sortBy"></param>
        /// <param name="favorites"></param>
        /// <param name="isRealtor"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public SearchResults HydrateSearchResults(string path, string sortBy, string favorites, bool isRealtor, Dictionary<string, string> filters, bool isClearSession, bool isHTH)
        {
            return GetCommunitiesBySearch(path, sortBy, favorites, isRealtor, filters, isHTH);
        }

        /// <summary>
        /// Hydrate Search Results by search
        /// </summary>
        /// <param name="searchLocation"></param>
        /// <param name="sortBy"></param>
        /// <param name="favorites"></param>
        /// <param name="isRealtor"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public SearchResults HydrateSearchResults(string path, string sortBy, string favorites, bool isRealtor, Dictionary<string, string> filters, bool isClearSession)
        {
            if (isClearSession)
            {
                WebSession[SessionKeys.SearchFacetsKey(isRealtor)] = null;
                WebSession.Save();
            }

            if (filters == null)
            {
                filters = WebSession[SessionKeys.SearchFacetsKey(isRealtor)].CastAs<Dictionary<string, string>>();
            }
            else
            {
                WebSession[SessionKeys.SearchFacetsKey(isRealtor)] = filters;
                WebSession.Save();
            }

            if (filters != null)
            {
                var searchUrl = filters.ContainsKey(GlobalEnums.SearchFilterKeys.SearchUrl.ToString()) ? filters[GlobalEnums.SearchFilterKeys.SearchUrl.ToString()] : String.Empty;
                if (path != searchUrl)
                    filters = null;
            }

            var searchLocation = path;
            if (isRealtor)
            {
                Guid newaid;
                if (Guid.TryParse(path, out newaid))
                {
                    searchLocation =
                        string.Format(
                            "*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}' and @@id='{3}']/*[@@templateid='{4}']",
                            SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, newaid, SCIDs.TemplateIds.CityPage);
                }
                else
                {
                    searchLocation =
                        string.Format(
                            "{0}/*[@@templateid='{1}']/*[@@templateid='{2}']/*[@@templateid='{3}']",
                            path, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
                }
                if (filters != null)
                {
                    var area = filters.ContainsKey(GlobalEnums.SearchFilterKeys.DivisionArea.ToString()) ? filters[GlobalEnums.SearchFilterKeys.DivisionArea.ToString()] : String.Empty;
                    if (area != "0")
                    {
                        searchLocation = GetRealtorUniqueSearchPath(path, area);
                    }
                }
            }
            else
            {
                searchLocation = GetUniqueSearchPath(path);
            }
            return GetCommunitiesBySearch(searchLocation, sortBy, favorites, isRealtor, filters, false);
        }

        public List<CommunityInfo> FetchMapPins(IList<CommunityInfo> communityInfo, bool isRealtor,
                                                Dictionary<string, string> filters = null)
        {
            communityInfo = filters == null
                                ? communityInfo.Where(c => c.CommunityStatus != GlobalEnums.CommunityStatus.Closed).ToList()
                                : communityInfo;

            var sortedInfo = new List<CommunityInfo>();
            var count = 0;
            var imageHelper = new ImageHelper();

            communityInfo = isRealtor
                                ? communityInfo.Where(
                                    c => c.MapPinStatus != GlobalEnums.MapRenderingType.DesignCenter.ToString()).ToList()
                                : communityInfo;

            foreach (CommunityInfo info in communityInfo)
            {
                count++;
                var mapIcon = info.MapPinStatus == GlobalEnums.MapRenderingType.DesignCenter.ToString().ToLower()
                                  ? string.Empty
                                  : string.Format(
                                      "<div  onClick=\"ZoomMap({0},{1})\" class=\"{2}_{3}\"><span>{4}</span></div>",
                                      info.CommunityLatitude, info.CommunityLongitude, info.CompanyNameAbbreviation,
                                      info.MapPinStatus, count);
                info.MapCount = count;
                info.MapIcon = mapIcon;
                info.DynamicMapIcon = info.MapPinStatus == GlobalEnums.MapRenderingType.DesignCenter.ToString()
                                          ? PageUrls.DesignCeterMapIconPath
                                          : imageHelper.DrawText(count.ToString(CultureInfo.InvariantCulture),
                                                                 info.CompanyNameAbbreviation, info.MapPinStatus);

                info.StateClusteredCommunityFlyouts = GetStateClusteredCommunityFlyouts(communityInfo, info, isRealtor);
                info.DivisionClusteredCommunityFlyouts = GetDivisionClusteredCommunityFlyouts(communityInfo, info,
                                                                                              isRealtor);
                info.CommunityFlyouts = GetCommunityFlyouts(info, isRealtor);

                sortedInfo.Add(info);
            }
            return sortedInfo;
        }

        private string GetCommunityFlyouts(CommunityInfo info, bool isRealtor)
        {
            if (isRealtor && info.MapPinStatus == GlobalEnums.MapRenderingType.DesignCenter.ToString()) return string.Empty;
            var price = !string.IsNullOrEmpty(info.CommunityPrice) ? "<b>" + info.CommunityPrice + "</b>" : string.Empty;
            var sqft = !string.IsNullOrWhiteSpace(info.SquareFootage) ? " | " + info.SquareFootage : string.Empty;
            var bed = !string.IsNullOrWhiteSpace(info.Bedrooms) ? " | " + info.Bedrooms : string.Empty;
            var bath = !string.IsNullOrWhiteSpace(info.Bathrooms) ? " | " + info.Bathrooms : string.Empty;

            var url = GetCommunityCityURLs(isRealtor, info.CommunityDetailsURL, info.CommunityDivisionName, info.CommunityCityName);
            var sb = new StringBuilder();
            sb.AppendFormat("{{\"imgsrc\":\"{0}\",\"url\":\"{1}\",\"target\":\"{2}\",\"facet\":\"{3}\"}}", info.CommunityCardImage + "?w=180&h=120", url, info.IsCurrentDomain ? string.Empty : "target=_blank", GetFacet(info, bath, bed, price, sqft));

            return sb.ToString();
        }

        private static string GetFacet(CommunityInfo info, string bath, string bed, string price, string sqft)
        {
            if (info.MapPinStatus != GlobalEnums.MapRenderingType.DesignCenter.ToString())
            {
                if (string.IsNullOrWhiteSpace(price) && string.IsNullOrWhiteSpace(sqft) && string.IsNullOrWhiteSpace(bed) &&
                    string.IsNullOrWhiteSpace(bath))
                    return "<p>Details available soon</p>";

                return string.Format("<p>{0}{1}{2}{3}</p>", price, sqft, bed, bath);
            }
            return string.Empty;
        }

        private string GetStateClusteredCommunityFlyouts(IEnumerable<CommunityInfo> communityInfo, CommunityInfo info, bool isRealtor)
        {
            return string.Empty;
            var sb = new StringBuilder();
            sb.Append("<div id=\"content\"><ul>");
            foreach (var clusData in communityInfo.Where(clusData => clusData.StateProvince == info.StateProvince))
            {
                sb.AppendFormat("<li><a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href='{0}' {2}>{1}</a></li>",
                                GetCommunityURLs(isRealtor, clusData.CommunityDetailsURL, clusData.CommunityDivisionName),
                                clusData.CommunityName, info.IsCurrentDomain ? string.Empty : "target=_blank");
            }
            sb.Append("</ul></div>");
            return sb.ToString();
        }

        private string GetDivisionClusteredCommunityFlyouts(IEnumerable<CommunityInfo> communityInfo, CommunityInfo info, bool isRealtor)
        {
            return string.Empty;
            var sb = new StringBuilder();
            sb.Append("<div id=\"content\"><ul>");
            foreach (var clusData in communityInfo.Where(clusData => clusData.DivisionOf == info.DivisionOf))
            {
                sb.AppendFormat("<li><a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href='{0}' {2} >{1}</a></li>",
                                GetCommunityURLs(isRealtor, clusData.CommunityDetailsURL, clusData.CommunityDivisionName),
                                clusData.CommunityName, info.IsCurrentDomain ? string.Empty : "target=_blank");
            }
            sb.Append("</ul></div>");
            return sb.ToString();
        }

        private string GetCommunityCityURLs(bool isRealtor, string communityDetailsURL, string communityDivisionName, string communityCityName)
        {
            if (isRealtor)
                return string.Format("Realtor-Search/Realtor-Search-Results-{0}-in-{1}", communityCityName, communityDivisionName);

            return communityDetailsURL;
        }

        private string GetCommunityURLs(bool isRealtor, string communityDetailsURL, string communityDivisionName)
        {
            if (isRealtor)
                return string.Format("Realtor-Search/Realtor-Search-Results-in-{0}", communityDivisionName);

            return communityDetailsURL;
        }

        //private List<Item> GetStateCentroid()
        //{
        //    string qry =
        //        string.Format("fast:/sitecore/content/*{0}/Home/*[@@templateid = '{1}']/*[@@templateid = '{2}']",
        //                      SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage);
        //    return _scContextDB.SelectItems(qry).ToList();
        //}
        //private List<Item> GetDivisionCentroid()
        //{
        //    string qry =
        //        string.Format(
        //            "fast:/sitecore/content/*{0}/Home/*[@@templateid = '{1}']/*[@@templateid = '{2}']/*[@@templateid = '{3}']",
        //            SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage,
        //            SCIDs.TemplateIds.DivisionPage);
        //    return _scContextDB.SelectItems(qry).ToList();
        //}
        //private List<Item> GetCityCentroid()
        //{
        //    string qry =
        //        string.Format(
        //            "fast:/sitecore/content/*{0}/Home/*[@@templateid = '{1}']/*[@@templateid = '{2}']/*[@@templateid = '{3}']/*[@@templateid = '{4}']",
        //            SCFastQueries.CrossCompaniesCondition, SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage,
        //            SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
        //    return _scContextDB.SelectItems(qry).ToList();
        //}

        //private IEnumerable<Item> GetRealtorCommunities(string qry)
        //{
        //    return _scContextDB.SelectItems(qry).ToList();
        //}

        private IEnumerable<Item> GetCommunities(string path, bool sameCompany = false)
        {
            string qry =
                string.Format("fast:/sitecore/content/*{0}/Home/{1}/*{2}", sameCompany ? SCFastQueries.SameCompanyCondition : SCFastQueries.CrossCompaniesCondition, TrimPath(path),
                              SCFastQueries.ActiveCommunitiesCondition);
            return ContextDB.SelectItems(qry);
        }

        private IEnumerable<Item> GetLinkedCommunities(string path, bool sameCompany = false)
        {
            //IEnumerable<Item> comms = new List<Item>();
            var communities = new List<Item>();
            string qry =
               string.Format("fast:/sitecore/content/*{0}/Home/{1}", sameCompany ? SCFastQueries.SameCompanyCondition : SCFastQueries.CrossCompaniesCondition, TrimPath(path));

            var result = ContextDB.SelectItems(qry).
                Where(i => i.GetAllChildrenByTemplate(SCIDs.TemplateIds.CommunityLink).Count > 0).
                SelectMany(e => e.Children).
                Where(e => e.TemplateID == SCIDs.TemplateIds.CommunityLink);

            foreach (var item in result)
            {
                if (item.Fields != null &&
                    item.Fields["Community Link"] != null)
                {
                    if (!String.IsNullOrEmpty(item.Fields["Community Link"].Value))
                    {
                        var selected = item.Fields["Community Link"].
                          Value.
                          Split('|');

                        foreach (var community in selected)
                        {
                            var comm = ContextDB.GetItem(community);
                            if (comm != null)
                            {
                                if (comm.Fields["Community Status"] != null &&
                                    comm.Fields["Community Status"].Value != SCIDs.StatusIds.CommunityStatus.InActive.ToString() &&
                                    comm.Fields["Status for Search"].Value == SCIDs.StatusIds.Status.Active.ToString())
                                {
                                    communities.Add(comm);
                                }
                            }
                        }
                    }
                }
            }

            return communities;
        }

        private IEnumerable<Item> GetFavCommunities(string path)
        {
            string qry =
                string.Format("fast:/sitecore/content/*{0}/Home/{1}/*{2}", SCFastQueries.CrossCompaniesCondition, TrimPath(path),
                              SCFastQueries.AllCommunitiesCondition);
            return ContextDB.SelectItems(qry);
        }

        public Item GetPpcDivision(string path)
        {
            string qry =
                string.Format("fast:{0}/ancestor::*[@@templateid='{1}']", path, SCIDs.TemplateIds.DivisionPage);
            return ContextDB.SelectSingleItem(qry);
        }

        private IEnumerable<Item> GetCommunitiesByCurrentDomain(string path)
        {
            string qry =
                string.Format("fast:{0}/*[@@templateid = '{1}']/*{2}", path, SCIDs.TemplateIds.CityPage,
                              SCFastQueries.ActiveCommunitiesCondition);
            return ContextDB.SelectItems(qry);
        }

        private IEnumerable<Item> GetCommunitiesByCurrentDomainHTH(string path)
        {
            string qry =
                string.Format("fast:{0}/*[@@templateid = '{1}']/*{2}", path, SCIDs.TemplateIds.CityPage,
                              SCFastQueries.ActiveCommunitiesConditionHTH);
            return ContextDB.SelectItems(qry);
        }

        private List<DesignStudioInfo> GetDesignStudio(string path)
        {
            string qry =
                string.Format("fast:/sitecore/content/*[@@id = '{0}']/Home{1}/*{2}", CommonHelper.GetCompanyItemIDByHostName(), path.TrimUrlLastText(), SCFastQueries.ActiveDesignStudio);

            return DesignStudio(ContextDB.SelectItems(qry));
        }

        private List<SubcommunityInfo> GetSubcommunities(ID itemId)
        {
            var qry = string.Format("{0}/*[@@templateid = '{1}' and @@id = '{2}']/*{3}",
                                    SCFastQueries.CrossCompanyProductTypeStateDiviCityCondition, SCIDs.TemplateIds.CommunityPage, itemId, SCFastQueries.ActiveSubCommunitiesCondition);

            return Subcommunity(ContextDB.SelectItems(qry));
        }

        private List<PlanInfo> GetPlans(Item item, CrossSiteSpecification crossSiteSpecification, bool isRealtor)
        {
            //FB Case 324: Display View Homes For Sale when plan is inactive.
            var itemPath = SCFastQueries.EscapeFastQueryPath(item.Paths.FullPath);
            var qry = string.Format("{0}/*{1}", itemPath
                                    , SCFastQueries.AllPlans);

            return Plans(ContextDB.SelectItems(qry), item, crossSiteSpecification, isRealtor);
        }

        private PromoInfo GetCommunityPromo(ID itemId)
        {
            var qry = string.Format("{0}/*[@@templateid = '{1}' and @@id = '{2}']/*{3}",
                                    SCFastQueries.CrossCompanyProductTypeStateDiviCityCondition, SCIDs.TemplateIds.CommunityPage, itemId, SCFastQueries.CommunityPromo);

            return Promo(ContextDB.SelectItems(qry));
        }

        private List<PlanInfo> GetFavoritesPlans(ID itemId, CrossSiteSpecification crossSiteSpecification, string liveChatSkill)
        {
            var qry = string.Format("{0}/*[@@templateid = '{1}' and @@id = '{2}']/*{3}",
                                    SCFastQueries.CrossCompanyProductTypeStateDiviCityCondition, SCIDs.TemplateIds.CommunityPage, itemId, SCFastQueries.AllFavPlans);

            return FavPlans(ContextDB.SelectItems(qry), crossSiteSpecification, liveChatSkill);
        }

        private List<HomeForSaleInfo> GetHomeForSales(Item item, string planName, bool isRealtor)
        {
            var itemPath = SCFastQueries.EscapeFastQueryPath(item.Paths.FullPath);
            var qry = string.Format("{0}/*{1}", itemPath, SCFastQueries.ActiveHomeForSales);
            return HomeForSales(ContextDB.SelectItems(qry), planName, isRealtor);
        }

        private List<HomeForSaleInfo> GetFavHomeForSales(ID itemId, string liveChatSkill)
        {
            var qry = string.Format("{0}/*[@@templateid = '{1}' and @@id = '{2}']/*{3}",
                                    SCFastQueries.CrossCompanyProductTypeStateDiviCityCommunityCondition, SCIDs.TemplateIds.PlanPage, itemId, SCFastQueries.AllHomeForSales);
            return FavHomeForSales(ContextDB.SelectItems(qry), liveChatSkill);
        }

        private static string GetPlanMediaUrl(Item item)
        {
            string imageitems = item["Elevation Images"];
            var firstId = !string.IsNullOrWhiteSpace(imageitems) ? imageitems.Substring(0, 38) : string.Empty;

            string imageUrl = PageUrls.NoImagePath;

            if (!string.IsNullOrEmpty(firstId))
            {
                imageUrl = string.Format("{0}", SCUtils.GetItemUrl(Context.Database.GetItem(firstId)));
            }
            return imageUrl;
        }

        /// <summary>
        /// Get plan items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<PlanInfo> FavPlans(Item[] items, CrossSiteSpecification crossSiteSpecification, string liveChatSkill)
        {
            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };
            var pln = items.FirstOrDefault();
            var phoneNumber = GetIhcPhoneDetails(pln);

            return (from childItem in items
                    let communityDtl = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, childItem.ID)
                    let staDtl = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID)
                    select new PlanInfo
                    {
                        PlanID = childItem.ID.ToGuid(),
                        PlanParentID = childItem.ParentID.ToGuid(),
                        PlanName =
                            !string.IsNullOrWhiteSpace(childItem["Plan Name"]) ? childItem["Plan Name"] : childItem.DisplayName,
                        PlanLegacyID = childItem["Plan Legacy ID"],
                        MasterPlanID = childItem["Master Plan ID"],
                        PricedfromValue = childItem["Priced from Value"].StringToDouble(),
                        PricedfromText = childItem["Priced from Text"],
                        PlanStatus = SCUtils.SearchStatus(childItem["Plan Status"]),
                        StatusforSearch = SCUtils.SearchStatus(childItem["Status for Search"]),
                        StatusforAssistance = childItem["Status for Assistance"],
                        StatusforAdvertising = childItem["Status for Advertising"],
                        SubCommunityID = childItem["Sub-Community ID"],
                        //PlanDescription = childItem["Plan Description"],
                        SquareFootage = childItem["Square Footage"].StringToInt32(),
                        NumberofStories = childItem["Number of Stories"].StringToInt32(),
                        NumberofBedrooms = childItem["Number of Bedrooms"].StringToInt32(),
                        NumberofBathrooms = childItem["Number of Bathrooms"].StringToInt32(),
                        NumberofHalfBathrooms = childItem["Number of Half Bathrooms"].StringToInt32(),
                        NumberofGarages = childItem["Number of Garages"].StringToInt32(),
                        NumberofDens = childItem["Number of Dens"].StringToInt32(),
                        NumberofSolariums = childItem["Number of Solariums"].StringToInt32(),
                        NumberofBonusRooms = childItem["Number of Bonus Rooms"].StringToInt32(),
                        NumberofFamilyRooms = childItem["Number of Family Rooms"].StringToInt32(),
                        NumberofLivingAreas = childItem["Number of Living Areas"].StringToInt32(),
                        NumberofDiningAreas = childItem["Number of Dining Areas"].StringToInt32(),
                        MasterPlanElevationID = childItem["Master Plan Elevation ID"],
                        HomeForSaleList = GetFavHomeForSales(childItem.ID, liveChatSkill),
                        CompanyName = crossSiteSpecification != null ? crossSiteSpecification.CompanyName : string.Empty,
                        Domain = crossSiteSpecification != null ? crossSiteSpecification.Domain : string.Empty,
                        CommunityName =
                            !string.IsNullOrWhiteSpace(communityDtl["Community Name"])
                                ? communityDtl["Community Name"]
                                : communityDtl.DisplayName,
                        CommunityDetailsURL = communityDtl.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                        IsCurrentDomain = crossSiteSpecification.SiteSpecificUrl.Contains(uri.Authority),
                        PlanPageUrl = childItem.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                        Phone = !string.IsNullOrWhiteSpace(phoneNumber) ? phoneNumber : communityDtl["Phone"],
                        ElevationPlanImage = GetPlanMediaUrl(childItem),
                        PlanCommunityID = communityDtl.ID.ToGuid(),
                        State = staDtl.DisplayName,
                        LiveChatSkill = liveChatSkill
                    }).ToList();
        }

        /// <summary>
        /// Get plan items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public PromoInfo Promo(Item[] items)
        {
            items = items.OrderBy(i => Sitecore.DateUtil.IsoDateToDateTime(i["End Date"])).ToArray();
            foreach (var item in items)
            {
                if (item["Disabled"] != "1")
                {
                    var sdate = DateTime.MinValue;
                    var edate = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(item["Start Date"]))
                        sdate = Sitecore.DateUtil.IsoDateToDateTime(item["Start Date"]);

                    if (!string.IsNullOrEmpty(item["End Date"]))
                        edate = Sitecore.DateUtil.IsoDateToDateTime(item["End Date"]);

                    if (sdate != DateTime.MinValue && edate != DateTime.MaxValue)
                    {
                        if (sdate <= DateTime.Now && edate >= DateTime.Now)
                        {
                            bool onBro;
                            return new PromoInfo
                            {
                                Title = item["Title"],
                                ExternalLink = item["External Link"],
                                OpenInNewBrowser = bool.TryParse(item["Open in new browser"], out onBro)
                            };
                        }
                    }
                }
            }
            return new PromoInfo();
        }

        /// <summary>
        /// Get plan items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<PlanInfo> Plans(Item[] items, Item communityItem, CrossSiteSpecification crossSiteSpecification, bool isRealtor)
        {
            return items.Select(childItem => new PlanInfo
            {
                PlanID = childItem.ID.ToGuid(),
                PlanParentID = childItem.ParentID.ToGuid(),
                PlanName = childItem["Plan Name"],
                PlanLegacyID = childItem["Plan Legacy ID"],
                MasterPlanID = childItem["Master Plan ID"],
                PricedfromValue = childItem["Priced from Value"].StringToDouble(),
                PricedfromText = childItem["Priced from Text"],
                PlanStatus = SCUtils.SearchStatus(childItem["Plan Status"]),
                StatusforSearch = SCUtils.SearchStatus(childItem["Status for Search"]),
                StatusforAssistance = childItem["Status for Assistance"],
                StatusforAdvertising = childItem["Status for Advertising"],
                SubCommunityID = childItem["Sub-Community ID"],
                PlanDescription = isRealtor ? childItem["Plan Description"] : string.Empty,
                SquareFootage = childItem["Square Footage"].StringToInt32(),
                NumberofStories = childItem["Number of Stories"].StringToInt32(),
                NumberofBedrooms = childItem["Number of Bedrooms"].StringToInt32(),
                NumberofBathrooms = childItem["Number of Bathrooms"].StringToInt32(),
                NumberofHalfBathrooms = childItem["Number of Half Bathrooms"].StringToInt32(),
                NumberofGarages = childItem["Number of Garages"].StringToInt32(),
                NumberofDens = childItem["Number of Dens"].StringToInt32(),
                NumberofSolariums = childItem["Number of Solariums"].StringToInt32(),
                NumberofBonusRooms = childItem["Number of Bonus Rooms"].StringToInt32(),
                NumberofFamilyRooms = childItem["Number of Family Rooms"].StringToInt32(),
                NumberofLivingAreas = childItem["Number of Living Areas"].StringToInt32(),
                NumberofDiningAreas = childItem["Number of Dining Areas"].StringToInt32(),
                MasterPlanElevationID = childItem["Master Plan Elevation ID"],
                HomeForSaleList = GetHomeForSales(childItem, childItem.DisplayName, isRealtor), // ["Plan Name"]),
                CompanyName = crossSiteSpecification != null ? crossSiteSpecification.CompanyName : string.Empty,
                Domain = crossSiteSpecification != null ? crossSiteSpecification.Domain : string.Empty,
                CommunityName = communityItem["Community Name"]
            }).ToList();
        }

        #region IHC

        public Item GetIhcCardValues(Item item)
        {
            if (item == null) return null;
            string values = item.Fields["IHC Card"].Value;
            if (!string.IsNullOrEmpty(values))
            {
                string[] valuelist = values.Split('|');

                return valuelist.Select(info => Sitecore.Context.Database.GetItem(info)).FirstOrDefault();
            }
            return null;
        }

        public string GetIHCCardDetails(Item ihcItem)
        {
            StringBuilder sb = new StringBuilder();

            string ihcImage = "/images/tm/ihc_person.jpg";
            if (!string.IsNullOrEmpty(ihcItem.Fields["Consultant Image"].Value))
            {
                Sitecore.Data.Fields.ImageField imageField = ihcItem.Fields["Consultant Image"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                    ihcImage = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image)) + "?h=55";
                }
            }

            sb.AppendFormat("<img src=\"{0}\" alt=\"{1}\">", ihcImage, "ihccard");
            sb.AppendFormat(@"<p><span>{0} {1}</span><br>Internet Home Consultant<br/><span class=""ihc-phone"">{3}</span><br/>{2}<p>Inspired by you <sup style='font-size: 7pt;'>&reg;</sup></p></p>", ihcItem["First Name"], ihcItem["Last Name"], ihcItem["Regulating Authority Designation"].Replace("\n", "<br />"), ihcItem["Phone"]);
            return sb.ToString();
        }

        public string GetIHCCardDetailsForMailer(Item ihcItem)
        {
            StringBuilder sb = new StringBuilder();

            string ihcImage = "/images/tm/ihc_person.jpg";
            if (!string.IsNullOrEmpty(ihcItem.Fields["Consultant Image"].Value))
            {
                Sitecore.Data.Fields.ImageField imageField = ihcItem.Fields["Consultant Image"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                    ihcImage = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image)) + "?h=55";
                }
            }

            sb.AppendFormat("<div style='float: left; margin: 25px 0 0 456px;'><img src=\"{0}\" alt=\"{1}\"></div>", ihcImage, "ihccard");
            sb.AppendFormat(@"<div class='ihc-info'><h1>{0} {1} </h1><span>DRE# {2} <br/>Internet Home Consultant<br/></span><p>{3}</p></div>", ihcItem["First Name"], ihcItem["Last Name"], ihcItem["Regulating Authority Designation"].Replace("\n", "<br />"), ihcItem["Phone"]);
            return sb.ToString();
        }

        private string GetIhcPhoneDetails(Item pln)
        {
            if (pln == null) return string.Empty;
            string phoneNumber = string.Empty;
            //var cityDtl = GetIhcCardValues(SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CityPage, pln.ID));
            //phoneNumber = cityDtl != null ? cityDtl["Phone"] : string.Empty;
            //if (string.IsNullOrWhiteSpace(phoneNumber))
            //{
            var diviDtl = GetIhcCardValues(SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, pln.ID));
            phoneNumber = diviDtl != null ? diviDtl["Phone"] : string.Empty;
            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                var statDtl = GetIhcCardValues(SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, pln.ID));
                phoneNumber = statDtl != null ? statDtl["Phone"] : string.Empty;
            }
            //}
            return phoneNumber;
        }

        #endregion IHC

        #region Filtering Items

        /// <summary>
        /// Get state province items
        /// </summary>
        /// <param name="stateItems"></param>
        /// <returns></returns>
        public List<StateProvinceInfo> GetStateProvinceInfos(List<Item> stateItems)
        {
            return stateItems.Select(item => new StateProvinceInfo
            {
                StateName = item["State Name"],
                StatusforSearch = item["Status for Search"],
                StatusforAssistance = item["Status for Assistance"],
                SearchCentroidLatitude = item["Search Centroid Latitude"].StringToDouble(),
                SearchCentroidLongitude = item["Search Centroid Longitude"].StringToDouble(),
                SearchCentroidZoom = item["Search Centroid Zoom"].StringToInt32(),
                StateDisclaimerImage = item["State Disclaimer Image"],
                FacebookURL = item["Facebook URL"],
                TwitterURL = item["Twitter URL"],
                YouTubeURL = item["YouTube URL"],
                PinterestURL = item["Pinterest URL"],
                GooglePlusURL = item["Google+ URL"],
                BlogURL = item["Blog URL"]
            }).ToList();
        }

        public List<DesignStudioInfo> DesignStudio(Item[] items)
        {
            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            var dsInfo = new List<DesignStudioInfo>();
            foreach (var childItem in items)
            {
                var crossSiteSpecification = childItem.GetCrossSiteSpecification(sitecoreUrlOptions, uri);
                var stateName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID);

                var url = crossSiteSpecification.SiteSpecificUrl;
                var stProAbbr = stateName.DisplayName.GetStateAbbreviationByState().ToString();

                dsInfo.Add(new DesignStudioInfo
                {
                    DesignStudioId = childItem.ID.ToGuid(),
                    DesignStudioName = childItem.DisplayName,
                    DesignStudioDetailsUrl = url,
                    DesignStudioStatus = SCUtils.SearchStatus(childItem["Design Center Status"]),
                    DesignStudioLatitude = childItem["Design Center Latitude"].StringToDouble(),
                    DesignStudioLongitude = childItem["Design Center Longitude"].StringToDouble(),
                    StreetAddress1 = childItem["Street Address 1"],
                    StreetAddress2 = childItem["Street Address 2"],
                    City = childItem["City"],
                    StateProvince = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID).DisplayName,
                    StateProvinceAbbreviation = stateName.DisplayName.GetStateAbbreviationByState().ToString(),
                    ZipPostalCode = childItem["Zip or Postal Code"],
                    Phone = childItem["Phone"],
                    Fax = childItem["Fax"],
                    DrivingDirectionsText = childItem["Driving Directions Text"],
                    MapIcon = "/Images/mappins/designcenter.png",
                    ClusteredMapIcon = string.Format("/Images/mappins/clustered_{0}.png", stProAbbr),
                    SingleClusteredMapIcon = string.Format("/Images/mappins/{0}.png", stProAbbr),
                    Hours = childItem["Hours"]
                });
            }

            return dsInfo;
        }

        /// <summary>
        /// Get subcommunity items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<SubcommunityInfo> Subcommunity(Item[] items)
        {
            return items.Select(childItem => new SubcommunityInfo
            {
                SubCommunityID = childItem.ID.ToGuid(),
                SubCommunityParentID = childItem.ParentID.ToGuid(),
                SubcommunityName = childItem.DisplayName, //childItem["Subcommunity Name"],
                SubcommunityLegacyID = childItem["Subcommunity Legacy ID"],
                SubcommunitySortOrder = childItem["Subcommunity Sort Order"].StringToInt32(),
                SubcommunityStatus = SCUtils.SearchStatus(childItem["Subcommunity Status"])
            }).ToList();
        }

        /// <summary>
        /// Get home for sales items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<HomeForSaleInfo> HomeForSales(Item[] items, string planName)
        {
            return HomeForSales(items, planName, false);
        }

        /// <summary>
        /// Get home for sales items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<HomeForSaleInfo> HomeForSales(Item[] items, string planName, bool isCompanyNeeded, bool isRealtor = false)
        {
            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            var hfsInfos = new List<HomeForSaleInfo>();
            foreach (Item childItem in items)
            {
                string companyName = string.Empty;
                string domain = string.Empty;
                bool isCurrentDomain = false;
                string schools = string.Empty;

                string hfsImage;
                if (!string.IsNullOrWhiteSpace(childItem["Elevation Images"]))
                {
                    try
                    {
                        string[] imgIds = childItem["Elevation Images"].Split('|');
                        Item image = Context.Database.GetItem(new SCID(imgIds[0]));
                        hfsImage = MediaManager.GetMediaUrl(image);
                    }
                    catch (Exception ex)
                    {
                        Sitecore.Diagnostics.Log.Error("Elevation images are not in sync for: " + childItem.Paths.FullPath, ex, this);
                        hfsImage = PageUrls.NoImagePath;
                    }
                }

                else
                    hfsImage = PageUrls.NoImagePath;

                var ad = new DateTime();
                ad = string.IsNullOrEmpty(childItem["Availability Date"]) ? DateTime.MinValue : Sitecore.DateUtil.IsoDateToDateTime(childItem["Availability Date"]);
                var aviMon = ad.ToString("MMMM yyyy", CultureInfo.InvariantCulture);

                Item cityName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CityPage, childItem.ID);
                Item stateName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID);
                Item communityDtl = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, childItem.ID);

                if (isCompanyNeeded)
                {
                    var crossSiteSpecification = childItem.GetCrossSiteSpecification(sitecoreUrlOptions, uri);
                    companyName = crossSiteSpecification.CompanyName;
                    domain = crossSiteSpecification.Domain;
                    var communityUrl = crossSiteSpecification.SiteSpecificUrl;
                    isCurrentDomain = communityUrl.Contains(uri.Authority);
                    var comItems = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, childItem.ID);
                    schools = comItems["School District"];
                }
                else
                    isCurrentDomain = communityDtl.GetSiteSpecificUrl(sitecoreUrlOptions, uri).Contains(uri.Authority);

                var availability = GetAvailabilities().FirstOrDefault(a => a.ID.ToString() == childItem["Status for Availability"]);

                hfsInfos.Add(new HomeForSaleInfo
                {
                    HomeforSaleID = childItem.ID.ToGuid(),
                    HomeforSaleName = childItem.GetItemName(),
                    HomeforSaleParentID = childItem.ParentID.ToGuid(),
                    HomeforSalePlanName = planName,
                    HomeforSaleDetailsURL = childItem.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                    HomeforSaleCommunityName = !string.IsNullOrWhiteSpace(communityDtl["Community Name"]) ? communityDtl["Community Name"] : communityDtl.DisplayName,
                    HomeforSaleCommunityDetailsURL = communityDtl.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                    MasterPlanID = childItem["Master Plan ID"],
                    Price = childItem["Price"].StringToDouble(),
                    HomeforSaleStatus = SCUtils.SearchStatus(childItem["Home for Sale Status"]),
                    StatusforSearch = SCUtils.SearchStatus(childItem["Status for Search"]),
                    AvailabilityDate = ad,
                    AvailabilityItemId = childItem["Status for Availability"],
                    Availability = availability != null ? availability.Name : aviMon,
                    //AvailabilityMonth = aviMon,
                    SubCommunityID = childItem["SubCommunity ID"],
                    StreetAddress1 = childItem["Street Address 1"],
                    StreetAddress2 = childItem["Street Address 2"],
                    City = cityName.DisplayName,
                    State = stateName.DisplayName,
                    PlanDescription = isRealtor ? childItem["Plan Description"] : string.Empty,
                    SquareFootage = childItem["Square Footage"].StringToInt32(),
                    NumberofStories = childItem["Number of Stories"].StringToInt32(),
                    NumberofBedrooms = childItem["Number of Bedrooms"].StringToInt32(),
                    NumberofBathrooms = childItem["Number of Bathrooms"].StringToInt32(),
                    NumberofHalfBathrooms = childItem["Number of Half Bathrooms"].StringToInt32(),
                    NumberofGarages = childItem["Number of Garages"].StringToInt32(),
                    NumberofBonusRooms = childItem["Number of Bonus Rooms"].StringToInt32(),
                    NumberofDens = childItem["Number of Dens"].StringToInt32(),
                    NumberofSolariums = childItem["Number of Solariums"].StringToInt32(),
                    ElevationImages = hfsImage,
                    LotNumber = childItem["Lot Number"],
                    RealtorBrokerCommissionRate = childItem["Realtor Broker Commission Rate"],
                    RealtorIncentiveAmount = childItem["Realtor Incentive Amount"],
                    CompanyName = companyName,
                    Domain = domain,
                    IsCurrentDomain = isCurrentDomain,
                    SchoolDistrict = schools,
                    Phone = communityDtl["Phone"],
                    HomeforSaleCommunityID = communityDtl.ID.ToGuid(),
                    IsRealtor = isRealtor
                });
            }

            return hfsInfos;
        }

        /// <summary>
        /// Get home for sales items
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public List<HomeForSaleInfo> FavHomeForSales(Item[] items, string liveChatSkill)
        {
            Uri uri = HttpContext.Current.Request.Url;
            var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };

            var hfsInfos = new List<HomeForSaleInfo>();
            foreach (Item childItem in items)
            {
                string companyName = string.Empty;
                string domain = string.Empty;
                bool isCurrentDomain = false;
                string schools = string.Empty;

                var crossSiteSpecification = childItem.GetCrossSiteSpecification(sitecoreUrlOptions, uri);
                companyName = crossSiteSpecification.CompanyName;
                domain = crossSiteSpecification.Domain;
                var communityUrl = crossSiteSpecification.SiteSpecificUrl;
                isCurrentDomain = communityUrl.Contains(uri.Authority);
                var comItems = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, childItem.ID);
                schools = comItems["School District"];

                string hfsImage;
                if (!string.IsNullOrWhiteSpace(childItem["Elevation Images"]))
                {
                    string[] imgIds = childItem["Elevation Images"].Split('|');
                    Item image = Context.Database.GetItem(new SCID(imgIds[0]));
                    if (image != null)
                    {
                        hfsImage = MediaManager.GetMediaUrl(image);
                    }
                    else
                    {
                        hfsImage = PageUrls.NoImagePath;
                    }
                }
                else
                {
                    hfsImage = PageUrls.NoImagePath;
                }

                var ad = new DateTime();
                ad = string.IsNullOrEmpty(childItem["Availability Date"]) ? DateTime.MinValue : Sitecore.DateUtil.IsoDateToDateTime(childItem["Availability Date"]);

                var phoneNumber = GetIhcPhoneDetails(childItem);

                Item cityName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CityPage, childItem.ID);
                Item stateName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, childItem.ID);
                Item communityDtl = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, childItem.ID);

                var availability = GetAvailabilities().FirstOrDefault(a => a.ID.ToString() == childItem["Status for Availability"]);

                var aviMon = ad.ToString("MMMM yyyy", CultureInfo.InvariantCulture);
                hfsInfos.Add(new HomeForSaleInfo
                {
                    HomeforSaleID = childItem.ID.ToGuid(),
                    HomeforSaleName = childItem.DisplayName,
                    HomeforSaleParentID = childItem.ParentID.ToGuid(),
                    HomeforSalePlanName = childItem.DisplayName,
                    HomeforSaleDetailsURL = childItem.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                    HomeforSaleCommunityName = !string.IsNullOrWhiteSpace(communityDtl["Community Name"]) ? communityDtl["Community Name"] : communityDtl.DisplayName,
                    HomeforSaleCommunityDetailsURL = communityDtl.GetSiteSpecificUrl(sitecoreUrlOptions, uri),
                    MasterPlanID = childItem["Master Plan ID"],
                    Price = childItem["Price"].StringToDouble(),
                    HomeforSaleStatus = SCUtils.SearchStatus(childItem["Home for Sale Status"]),
                    StatusforSearch = SCUtils.SearchStatus(childItem["Status for Search"]),
                    AvailabilityDate = ad,
                    AvailabilityItemId = childItem["Status for Availability"],
                    Availability = availability != null ? availability.Name : aviMon,
                    AvailabilityMonth = aviMon,
                    SubCommunityID = childItem["SubCommunity ID"],
                    StreetAddress1 = childItem["Street Address 1"],
                    StreetAddress2 = childItem["Street Address 2"],
                    City = cityName.DisplayName,
                    State = stateName.DisplayName,
                    //PlanDescription = childItem["Plan Description"],
                    SquareFootage = childItem["Square Footage"].StringToInt32(),
                    NumberofStories = childItem["Number of Stories"].StringToInt32(),
                    NumberofBedrooms = childItem["Number of Bedrooms"].StringToInt32(),
                    NumberofBathrooms = childItem["Number of Bathrooms"].StringToInt32(),
                    NumberofHalfBathrooms = childItem["Number of Half Bathrooms"].StringToInt32(),
                    NumberofGarages = childItem["Number of Garages"].StringToInt32(),
                    NumberofBonusRooms = childItem["Number of Bonus Rooms"].StringToInt32(),
                    NumberofDens = childItem["Number of Dens"].StringToInt32(),
                    NumberofSolariums = childItem["Number of Solariums"].StringToInt32(),
                    ElevationImages = hfsImage,
                    LotNumber = childItem["Lot Number"],
                    RealtorBrokerCommissionRate = childItem["Realtor Broker Commission Rate"],
                    RealtorIncentiveAmount = childItem["Realtor Incentive Amount"],
                    CompanyName = companyName,
                    Domain = domain,
                    IsCurrentDomain = isCurrentDomain,
                    SchoolDistrict = schools,
                    Phone = !string.IsNullOrWhiteSpace(phoneNumber) ? phoneNumber : communityDtl["Phone"],
                    HomeforSaleCommunityID = communityDtl.ID.ToGuid(),
                    DisplayonHomesforSalePage = childItem["Display on Homes for Sale Page"],
                    LiveChatSkill = liveChatSkill
                });
            }

            return hfsInfos;
        }

        #endregion Filtering Items

        #region Common Methods

        public static List<string> CanadaProvinces()
        {
            return new List<string>()
				{
					"Alberta",
					"British Columbia",
					"Manitoba",
					"New Brunswick",
					"Newfoundland and Labrador",
					"Northwest Territories",
					"Nova Scotia",
					"Nunavut",
					"Ontario",
					"Prince Edward Island",
					"Quebec",
					"Saskatchewan",
					"Yukon"
				};
        }

        public string GetRealtorUniqueSearchPath(string searchLocation, string area)
        {
            var newPath = string.Format("*[@@templateid='{0}']{1}", SCIDs.TemplateIds.ProductTypePage, SCFastQueries.StateDivisionCityCondition);

            if (!string.IsNullOrEmpty(area))
            {
                newPath = string.Format("*[@@templateid='{0}']/*[@@templateid='{1}']/*[@@templateid='{2}' and @@id='{3}']/*[@@templateid='{4}']",
                                SCIDs.TemplateIds.ProductTypePage, SCIDs.TemplateIds.StatePage, SCIDs.TemplateIds.DivisionPage, area, SCIDs.TemplateIds.CityPage);
            }

            return newPath;
        }

        public string GetUniquePath(string searchLocation)
        {
            var startPath = Sitecore.Context.Site.StartPath.ToLower();

            var newPath = searchLocation.ToLower().Replace(startPath, string.Empty);
            newPath = newPath.TrimStart('/');
            string[] searchPath = newPath.Split('/');

            switch (searchPath.Length)
            {
                case 3:
                    newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']", SCIDs.TemplateIds.ProductTypePage, GetParsedPath(newPath), SCIDs.TemplateIds.CityPage);
                    break;

                case 2:
                    newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']/*[@@templateid='{3}']", SCIDs.TemplateIds.ProductTypePage, GetParsedPath(newPath), SCIDs.TemplateIds.DivisionPage,
                                      SCIDs.TemplateIds.CityPage);
                    break;

                case 1:
                    newPath =
                        string.Format("{0}{1}", newPath, SCFastQueries.StateDivisionCityCondition);
                    break;

                default:
                    newPath = string.Format("*[@@templateid='{0}']/{1}", SCIDs.TemplateIds.ProductTypePage, GetParsedPath(newPath));
                    break;
            }
            return newPath;
        }

        public string GetUniqueSearchPath(string searchLocation)
        {
            var startPath = Sitecore.Context.Site.StartPath.ToLower();

            var newPath = searchLocation.ToLower().Replace(startPath, string.Empty);

            //newPath = newPath.Replace("/new homes/", string.Empty).Replace("/new-homes/", string.Empty);
            //newPath = newPath.Replace("/new condos/", string.Empty).Replace("/new-condos/", string.Empty);
            newPath = newPath.TrimStart('/');

            string[] searchPath = newPath.Split('/');

            switch (searchPath.Length)
            {
                case 4:
                    newPath = string.Format("/{0}", newPath);
                    break;

                case 3:
                    newPath = string.Format("/{0}/*[@@templateid='{1}']", newPath, SCIDs.TemplateIds.CityPage);
                    break;

                case 2:
                    newPath = string.Format("/{0}/*[@@templateid='{1}']/*[@@templateid='{2}']", newPath, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
                    break;

                default:
                    newPath = newPath.Replace("/new homes/", string.Empty).Replace("/new-homes/", string.Empty);
                    newPath = newPath.Replace("/new condos/", string.Empty).Replace("/new-condos/", string.Empty);
                    newPath = string.Format("*[@@templateid='{0}']/{1}", SCIDs.TemplateIds.ProductTypePage, newPath);
                    break;
            }
            return newPath;
        }

        //public string GetUniqueSearchPath(string searchLocation)
        //{
        //    var startPath = Sitecore.Context.Site.StartPath.ToLower();

        //    var newPath = "/" + searchLocation.ToLower().Replace(startPath, string.Empty);

        //    newPath = newPath.Replace("/new homes/", string.Empty).Replace("/new-homes/", string.Empty);
        //    newPath = newPath.Replace("/new condos/", string.Empty).Replace("/new-condos/", string.Empty);
        //    newPath = newPath.TrimStart('/');

        //    //var newPath = searchLocation.ToLower().Replace(startPath + "/new homes", string.Empty);
        //    //newPath = newPath.Replace(startPath + "/new-homes", string.Empty);
        //    //newPath = newPath.Replace(startPath + "/new condos", string.Empty);
        //    //newPath = newPath.Replace(startPath + "/new-condos", string.Empty);

        //    string[] searchPath = newPath.Split('/');

        //    switch (searchPath.Length)
        //    {
        //        case 2:
        //            newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']", SCIDs.TemplateIds.ProductTypePage, newPath, SCIDs.TemplateIds.CityPage);
        //            break;
        //        case 1:
        //            newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']/*[@@templateid='{3}']", SCIDs.TemplateIds.ProductTypePage, newPath, SCIDs.TemplateIds.DivisionPage, SCIDs.TemplateIds.CityPage);
        //            break;
        //        default:
        //            newPath = string.Format("*[@@templateid='{0}']/{1}", SCIDs.TemplateIds.ProductTypePage, newPath);
        //            break;
        //    }

        //    //switch (searchPath.Length)
        //    //{
        //    //    case 3:
        //    //        newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']", SCIDs.TemplateIds.ProductTypePage, newPath, SCIDs.TemplateIds.CityPage);
        //    //        break;
        //    //    case 2:
        //    //        newPath = string.Format("*[@@templateid='{0}']/{1}/*[@@templateid='{2}']/*[@@templateid='{3}']", SCIDs.TemplateIds.ProductTypePage, newPath, SCIDs.TemplateIds.DivisionPage,
        //    //                          SCIDs.TemplateIds.CityPage);
        //    //        break;
        //    //    case 1:
        //    //        newPath =
        //    //            string.Format("{0}{1}", newPath, SCFastQueries.StateDivisionCityCondition);
        //    //        break;
        //    //    default:
        //    //        newPath = string.Format("*[@@templateid='{0}']/{1}", SCIDs.TemplateIds.ProductTypePage, newPath);
        //    //        break;
        //    //}
        //    return newPath;
        //}

        /// <summary>
        /// since to get both newhome and newcondos results you have to remove new condos and new homes path.
        /// </summary>
        /// <param name="newPath"></param>
        /// <returns></returns>
        private string GetParsedPath(string newPath)
        {
            newPath = newPath.Replace("/new-homes/", "/").Replace("/new-condos/", "/");
            return newPath.Replace("/new homes/", "/").Replace("/new condos/", "/").TrimStart('/');
        }

        //private static string GetCrossDomainPathForHomeforSale(string searchLocation)
        //{
        //    var domain = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

        //    var newPath = searchLocation.ToLower().Replace(domain, string.Format("*{0}", SCFastQueries.CrossCompaniesCondition));
        //    newPath = newPath.Replace("new homes", string.Format("*[@@templateid='{0}']", SCIDs.TemplateIds.ProductTypePage));
        //    newPath = newPath.Replace("new condos", string.Format("*[@@templateid='{0}']", SCIDs.TemplateIds.ProductTypePage));
        //    return newPath;
        //}

        #endregion Common Methods

        public string GetStateDivisionUrlById(string id)
        {
            var diviName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, new ID(id));
            var stateName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, new ID(id));
            var productype = stateName.Parent;
            return productype.Name + "/" + stateName.DisplayName + "/" + diviName.DisplayName;
        }

        public string GetStateDivisionPathById(string id)
        {
            var diviName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, new ID(id));
            var stateName = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, new ID(id));
            var productype = stateName.Parent;
            return GetUniqueSearchPath(productype.Name + "/" + stateName.DisplayName + "/" + diviName.DisplayName);
        }

        #region Campain Details

        public Item GetCampaignValues(Item item, string fieldName)
        {
            if (item == null) return null;
            string values = item.Fields[fieldName].Value;
            if (!string.IsNullOrEmpty(values))
            {
                string[] valuelist = values.Split('|');

                return valuelist.Select(info => Sitecore.Context.Database.GetItem(info)).FirstOrDefault();
            }
            return null;
        }

        public string GetCampaignDetails(Item item)
        {
            var sb = new StringBuilder();

            if (item != null)
            {
                var sdate = DateTime.MinValue;
                var edate = DateTime.MaxValue;

                var status = item["Campaign status"];

                if (!string.IsNullOrEmpty(item["Campaign start date"]))
                    sdate = Sitecore.DateUtil.IsoDateToDateTime(item["Campaign start date"]);

                if (!string.IsNullOrEmpty(item["Campaign end date"]))
                    edate = Sitecore.DateUtil.IsoDateToDateTime(item["Campaign end date"]);

                var incentiveImage = item["Incentive image"];
                var targetURL = item["Incentive target URL"];

                if (!string.IsNullOrEmpty(status))
                    if (status == "1")
                    {
                        if (sdate <= DateTime.Now && edate >= DateTime.Now)
                            sb.AppendFormat("<a href=\"{0}\" target=\"_blank\">{1}</a>", targetURL, incentiveImage);
                    }
            }
            return sb.ToString();
        }

        #endregion Campain Details

        #region Division LiveChat

        public string CurrentDivisionLiveChatSkill(Item item, Item CurrentHomeItem)
        {
            Item currentNode = item;
            while (currentNode.ID != CurrentHomeItem.ID)
            {
                if (!string.IsNullOrWhiteSpace(currentNode[SCFieldNames.LiveChatSkillCode]))
                {
                    return currentNode[SCFieldNames.LiveChatSkillCode];
                }
                currentNode = currentNode.Parent;
            }
            return "";
        }

        #endregion Division LiveChat

        #region Sory by

        public List<Item> CommunitySortByField(List<Item> communityItems, string sortBy)
        {
            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceLowtoHigh:	//2
                    return communityItems.OrderBy(o => o["Price Override Text"]).ToList();
                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    return communityItems.OrderByDescending(o => o["Price Override Text"]).ToList();
                case GlobalEnums.SortBy.Location: //4
                    return communityItems.OrderBy(o => o["City"]).ThenBy(s => s["Community Name"]).ToList();
                default: //CommunityName: //1
                    return communityItems.OrderBy(o => o["Community Name"]).ToList();
            }
        }

        public List<CommunityInfo> CommunitySortByDomain(List<CommunityInfo> communityItems, string sortBy)
        {
            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceLowtoHigh:	//2
                    return communityItems.OrderBy(f => f.Domain).ThenBy(o => o.CommunityPrice).ToList();
                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    return communityItems.OrderBy(f => f.Domain).ThenByDescending(o => o.CommunityPrice).ToList();
                case GlobalEnums.SortBy.Location: //4
                    //return communityItems.OrderBy(f => f.Domain).ThenBy(o => o.City).ThenBy(s => s.CommunityName).ToList();
                    return communityItems.OrderBy(f => f.Domain).ThenBy(o => o.City).ThenBy(s => s.Name).ToList();
                default: //CommunityName: //1
                    //return communityItems.OrderBy(f => f.Domain).ThenBy(o => o.CommunityName).ToList();
                    return communityItems.OrderBy(f => f.Domain).ThenBy(o => o.Name).ToList();
            }
        }

        public List<CommunityInfo> CommunitySort(List<CommunityInfo> communityItems, string sortBy)
        {
            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceLowtoHigh:	//2
                    return communityItems.OrderBy(o => o.CommunityPrice).ToList();
                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    return communityItems.OrderByDescending(o => o.CommunityPrice).ToList();
                case GlobalEnums.SortBy.Location: //4
                    return communityItems.OrderBy(o => o.City).ThenBy(s => s.Name).ToList();
                default: //CommunityName: //1
                    return communityItems.OrderBy(o => o.Name).ToList();
            }
        }

        public List<HomeForSaleInfo> SortbyHomeForSaleInfo(List<HomeForSaleInfo> homeForSaleInfo, string sortBy)
        {
            //Sort order
            //-	Price low to high  (secondary default sort)
            //-	Price high to low
            //-	Plan Name
            //-	Availability (default)
            //-	Number of bedrooms
            //-	Square footage
            //-	Stories

            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceLowtoHigh:	//2
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    homeForSaleInfo = homeForSaleInfo.OrderByDescending(o => o.Price).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.SquareFootage: //6
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.SquareFootage).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.Numberofbedrooms: //7
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.NumberofBedrooms).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.PlanName: //5
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.HomeforSalePlanName).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.Stories: //9
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.NumberofStories).ThenBy(o => o.Price).ToList();
                    break;

                case GlobalEnums.SortBy.AvailabilityDate: //8
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.AvailabilityDate).ThenBy(o => o.Price).ToList();
                    break;

                default: //AvailabilityDate:
                    homeForSaleInfo = homeForSaleInfo.OrderBy(o => o.HomeforSaleCommunityName).ThenBy(o => o.Price).ToList();
                    break;
            }

            return homeForSaleInfo;
        }

        public List<PlanInfo> SortbyPlanInfo(List<PlanInfo> planInfo, string sortBy)
        {
            //Sort order
            //-	Price low to high  (default)
            //-	Price high to low
            //-	Plan Name
            //-	Number of bedrooms
            //-	Square footage
            //-	Stories

            switch (sortBy.GetSearchSortByValue())
            {
                case GlobalEnums.SortBy.PriceHightoLow:	//3
                    planInfo = planInfo.OrderByDescending(o => o.PricedfromValue).ToList();
                    break;

                case GlobalEnums.SortBy.SquareFootage: //6
                    planInfo = planInfo.OrderBy(o => o.SquareFootage).ToList();
                    break;

                case GlobalEnums.SortBy.Numberofbedrooms: //7
                    planInfo = planInfo.OrderBy(o => o.NumberofBedrooms).ToList();
                    break;

                case GlobalEnums.SortBy.PlanName: //5
                    planInfo = planInfo.OrderBy(o => o.PlanName).ToList();
                    break;

                case GlobalEnums.SortBy.Stories: //9
                    planInfo = planInfo.OrderBy(o => o.NumberofStories).ToList();
                    break;

                default: //PriceLowtoHigh:	//2
                    planInfo = planInfo.OrderBy(o => o.PricedfromValue).ToList();
                    break;
            }

            return planInfo;
        }

        #endregion Sory by

        #region Private Methods

        /// <summary>
        /// get cleaned path
        /// </summary>
        /// <param name="uncleanPath"></param>
        /// <returns></returns>
        public static string EscapePath(string uncleanPath)
        {
            string[] esacpedPath = uncleanPath.TrimStart('/').Split('/');
            string qualifiedPath = esacpedPath.Aggregate(string.Empty, (current, s) => current + (s.Contains("-") ? ("/#" + s + "#") : ("/" + s)));
            return qualifiedPath.TrimEnd('/');
        }

        /// <summary>
        /// Fix the relavant search status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string FixSearchStatus(string status)
        {
            if (status == GlobalEnums.CommunityStatus.Open.ToString().ToLower())
                return GlobalEnums.MapRenderingType.NowSelling.ToString().ToLower();
            return status == GlobalEnums.CommunityStatus.GrandOpening.ToString().ToLower() ? GlobalEnums.MapRenderingType.ComingSoon.ToString().ToLower() : status;
        }

        /// <summary>
        /// Get square ft from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public string GetSquareFoot(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList)
        {
            int minVal;
            int maxVal;

            if (planList.Count > 0)
            {
                minVal = planList.Min(m => m.SquareFootage);
                maxVal = planList.Max(m => m.SquareFootage);
            }
            else if (homeForSaleList.Count > 0)
            {
                minVal = homeForSaleList.Min(m => m.SquareFootage);
                maxVal = homeForSaleList.Max(m => m.SquareFootage);
            }
            else
                return string.Empty;

            if (minVal == 0 && maxVal == 0)
                return string.Empty;

            var rval = string.Empty;
            if (minVal == maxVal)
                rval = string.Format("{0:N0} Sq. Ft.", maxVal);
            else if (minVal != 0 && maxVal != 0)
                rval = string.Format("{0:N0} to {1:N0} Sq. Ft.", minVal, maxVal);
            else if (minVal == 0 && maxVal != 0)
                rval = string.Format("{0:N0} Sq. Ft.", maxVal);
            else if (minVal != 0 && maxVal == 0)
                rval = string.Format("{0:N0} Sq. Ft.", minVal);

            return rval == "0 Sq. Ft" ? string.Empty : rval;
        }

        /// <summary>
        /// Get beds from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public static string GetBedrooms(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList)
        {
            int minVal;
            int maxVal;
            int minDen;

            if (planList.Count > 0)
            {
                minVal = planList.Min(m => m.NumberofBedrooms);
                maxVal = planList.Max(m => m.NumberofBedrooms);
                minDen = planList.Max(m => m.NumberofDens);
            }
            else if (homeForSaleList.Count > 0)
            {
                minVal = homeForSaleList.Min(m => m.NumberofBedrooms);
                maxVal = homeForSaleList.Max(m => m.NumberofBedrooms);
                minDen = homeForSaleList.Max(m => m.NumberofDens);
            }
            else
                return string.Empty;

            string beds = string.Empty;

            if (minVal == 0 && maxVal == 0 && minDen == 0)
                return string.Empty;

            if (minVal == maxVal)
                beds = string.Format("{0} Bed{1}", minVal, minVal > 1 ? "s" : "");
            else if (minVal != 0 && maxVal != 0)
                beds = string.Format("{0} to {1} Beds", minVal, maxVal);
            else if (minVal == 0 && maxVal != 0)
                beds = string.Format("{0} Beds", maxVal);
            else if (minVal != 0 && maxVal == 0)
                beds = string.Format("{0} Bed{1}", minVal, minVal > 1 ? "s" : "");

            beds += minDen > 0 ? " with Den" : string.Empty;

            return beds == "0 Bed" ? string.Empty : beds;
        }

        /// <summary>
        /// Get baths from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public static string GetBathrooms(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList)
        {
            int minVal = 0;
            int maxVal = 0;

            if (planList.Count > 0)
            {
                minVal = planList.Min(m => m.NumberofBathrooms);
                maxVal = planList.Max(m => m.NumberofBathrooms);
            }
            else if (homeForSaleList.Count > 0)
            {
                minVal = homeForSaleList.Min(m => m.NumberofBathrooms);
                maxVal = homeForSaleList.Max(m => m.NumberofBathrooms);
            }
            else
                return string.Empty;

            if (minVal == 0 && maxVal == 0)
                return string.Empty;

            var rval = string.Empty;

            //no range in number of bathrooms
            if (minVal == maxVal)
            {
                rval = string.Format("{0} Bath{1}", minVal, minVal > 1 ? "s" : string.Empty);

            }
            //there is a range
            else if (minVal != 0 && maxVal != 0)
            {
                rval = string.Format("{0} to {1} Bath{2}", minVal, maxVal, minVal > 1 ? "s" : string.Empty);
            }

            return rval == "0 Bath" ? string.Empty : rval;
        }

        /// <summary>
        /// Get baths from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public static string GetHalfBathrooms(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList)
        {
            int minHalfBathrooms = 0;
            int maxHalfBathrooms = 0;
            string halfBathroomsText = string.Empty;

            if (planList.Count > 0)
            {
                minHalfBathrooms = planList.Min(m => m.NumberofHalfBathrooms);
                maxHalfBathrooms = planList.Max(m => m.NumberofHalfBathrooms);
            }
            else if (homeForSaleList.Count > 0)
            {
                minHalfBathrooms = homeForSaleList.Min(m => m.NumberofHalfBathrooms);
                maxHalfBathrooms = homeForSaleList.Max(m => m.NumberofHalfBathrooms);
            }

            //no range in number of half baths
            if (maxHalfBathrooms != 0 && minHalfBathrooms == maxHalfBathrooms)
            {
                halfBathroomsText = string.Format("{0} Half Bath{1}", maxHalfBathrooms, maxHalfBathrooms > 1 ? "s" : string.Empty);
            }
            //range in number of half baths
            else if (maxHalfBathrooms > minHalfBathrooms && maxHalfBathrooms != 0)
            {
                halfBathroomsText = string.Format("{0} to {1} Half Bath{2}", minHalfBathrooms, maxHalfBathrooms, maxHalfBathrooms > 1 ? "s" : string.Empty);
            }
            return halfBathroomsText;
        }

        /// <summary>
        /// Get story from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public static string GetStory(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList, DeviceType device)
        {
            int minVal;
            int maxVal;

            if (planList.Count > 0)
            {
                minVal = planList.Min(m => m.NumberofStories);
                maxVal = planList.Max(m => m.NumberofStories);
            }
            else if (homeForSaleList.Count > 0)
            {
                minVal = homeForSaleList.Min(m => m.NumberofStories);
                maxVal = homeForSaleList.Max(m => m.NumberofStories);
            }
            else
                return string.Empty;

            if (minVal == 0 && maxVal == 0)
                return string.Empty;

            string sufix = "";

            if (device == DeviceType.TaylorMorrison)
            {
                sufix = "Story";
            }
            else if (device == DeviceType.MonarchGroup)
            {
                sufix = "Storey";
            }
            else if (device == DeviceType.DarlingHomes)
            {
                sufix = "Story";
            }

            var rval = string.Empty;
            if (minVal == maxVal)
                rval = string.Format("{0} {2} Home{1}", minVal, minVal > 1 ? "s" : "", sufix);
            else if (minVal != 0 && maxVal != 0)
                rval = string.Format("{0} to {1} {2} Homes", minVal, maxVal, sufix);
            else if (minVal == 0 && maxVal != 0)
                rval = string.Format("{0} {2} Homes", maxVal, sufix);
            else if (minVal != 0 && maxVal == 0)
                rval = string.Format("{0} {2} Home{1}", minVal, minVal > 1 ? "s" : "", sufix);

            return rval == "0 Story Home" ? string.Empty : rval;
        }

        /// <summary>
        /// Get car garage from plan and home for sales
        /// </summary>
        /// <param name="planList"></param>
        /// <param name="homeForSaleList"></param>
        /// <returns></returns>
        public static string GetGarage(List<PlanInfo> planList, List<HomeForSaleInfo> homeForSaleList)
        {
            int minVal;
            int maxVal;

            if (planList.Count > 0)
            {
                minVal = planList.Min(m => m.NumberofGarages);
                maxVal = planList.Max(m => m.NumberofGarages);
            }
            else if (homeForSaleList.Count > 0)
            {
                minVal = homeForSaleList.Min(m => m.NumberofGarages);
                maxVal = homeForSaleList.Max(m => m.NumberofGarages);
            }
            else
                return string.Empty;

            if (minVal == 0 && maxVal == 0)
                return string.Empty;

            var rval = string.Empty;
            if (minVal == maxVal)
                rval = string.Format("{0} Car Garage", minVal);
            else if (minVal != 0 && maxVal != 0)
                rval = string.Format("{0} to {1} Car Garage", minVal, maxVal);
            else if (minVal == 0 && maxVal != 0)
                rval = string.Format("{0} Car Garage", maxVal);
            else if (minVal != 0 && maxVal == 0)
                rval = string.Format("{0} Car Garage", minVal);

            return rval == "0 Car Garage" ? string.Empty : rval;
        }

        /// <summary>
        /// Get media url by field name
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fld"></param>
        /// <returns></returns>
        public static string GetMediaURL(Item item, ID fld)
        {
            FileField fileField = ((FileField)item.Fields[fld]);

            if (fileField.MediaItem != null)
                return MediaManager.GetMediaUrl(fileField.MediaItem);

            return string.Empty;
        }

        /// <summary>
        /// Get available media url by field
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fIds"></param>
        /// <returns></returns>
        public static string GetAvailableMediaUrl(Item item, List<ID> fIds)
        {
            string imageitems = item["Slideshow Image"];
            var firstId = !string.IsNullOrWhiteSpace(imageitems) ? imageitems.Substring(0, 38) : string.Empty;

            string imageUrl = PageUrls.NoImagePath;

            if (!string.IsNullOrEmpty(firstId))
            {
                imageUrl = string.Format("{0}", SCUtils.GetItemUrl(Context.Database.GetItem(firstId)));
            }
            else
            {
                foreach (ID fld in fIds)
                {
                    var path = GetMediaURL(item, fld);
                    if (string.IsNullOrEmpty(path)) continue;
                    imageUrl = string.Format("{0}", path);
                    break;
                }
            }
            return imageUrl;
        }

        #endregion Private Methods
    }
}
