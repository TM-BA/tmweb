﻿using System;
using System.Collections.Generic;
using System.Linq;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.SCHelpers
{

    public class CommonHelper : BaseHelper
    {

        public string GetProductTypeByUrl(string uri)
        {
            if (uri.Contains("/new-homes/") || uri.Contains("/new homes/"))
                return "new homes";
            if (uri.Contains("/new-condos/") || uri.Contains("/new condos/"))
                return "new condos";

            return "new homes";
        }

        public int GetFacetBeds(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofBedrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBedrooms) : 0;
        }
        public int GetFacetBaths(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofBathrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBathrooms) : 0;
        }

        public int GetFacetHalfBaths(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofHalfBathrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofHalfBathrooms) : 0;
        }

        public int GetFacetGarages(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofGarages)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofGarages) : 0;
        }

        public int GetFacetBonusRooms(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofBonusRooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofBonusRooms) : 0;
        }

        public int GetFacetDens(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofDens)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofDens) : 0;
        }

        public int GetFacetStories(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {
            return pInfos.Count > 0
                       ? pInfos.Max(m => m.NumberofStories)
                       : hfsInfos.Count > 0 ? hfsInfos.Max(m => m.NumberofStories) : 0;
        }

        public IEnumerable<int> GetAvailableFacetBeds(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofBedrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBedrooms) : new List<int>();
        }
        public IEnumerable<int> GetAvailableFacetBaths(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofBathrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBathrooms) : new List<int>();
        }

        public IEnumerable<int> GetAvailableFacetHalfBaths(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofHalfBathrooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofHalfBathrooms) : new List<int>();
        }

        public IEnumerable<int> GetAvailableFacetGarages(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofGarages)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofGarages) : new List<int>();
        }

        public IEnumerable<int> GetAvailableFacetBonusRooms(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofBonusRooms)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofBonusRooms) : new List<int>();
        }

        public IEnumerable<int> GetAvailableFacetDens(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {

            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofDens)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofDens) : new List<int>();
        }

        public IEnumerable<int> GetAvailableFacetStories(List<PlanInfo> pInfos, List<HomeForSaleInfo> hfsInfos)
        {
            return pInfos.Count > 0
                       ? pInfos.Select(m => m.NumberofStories)
                       : hfsInfos.Count > 0 ? hfsInfos.Select(m => m.NumberofStories) : new List<int>();
        }

        public Double RoundPrice(double price, bool isMin)
        {
            if (price == 0) return price;
            const double roundingPrice = 50000;

            var fitys = 0;
            if (price > roundingPrice)
            {
                //fitys = Convert.ToInt32(price / roundingPrice);
                fitys = (price / roundingPrice).ToString().Split('.')[0].StringToInt32();
            }

            return isMin ? (price > roundingPrice ? (fitys - 1) * roundingPrice : 0) : (fitys + 1) * roundingPrice;
        }


        /// <summary>
        /// Starting price
        /// Under $500k --> $50,000 increments: 50K, 100K, 150K
        /// $500K+ - $1m --> $100,000 increments: 600K, 700K
        /// Over $1m --> $500,000 increments: 1.5M, 2M, 2.5M…
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public Double FindPriceTick(double price)
        {
            if (price < 500000)
                return 50000;
            if (price >= 500000 && price < 1000000)
                return 100000;
            if (price >= 1000000)
                return 1500000;

            return 50000;
        }

        public static Sitecore.Data.ID GetCompanyItemIDByHostName()
        {
            string hostName = string.Empty;
            Sitecore.Data.ID id = new Sitecore.Data.ID(new Guid());

            hostName = Sitecore.Context.GetSiteName().ToLower();

            switch (hostName)
            {
                case CommonValues.TaylorMorrisonDomain:
                    {
                        id = SCIDs.CompanyIds.TaylorMorrison;
                        break;
                    }
                case CommonValues.DarlingHomesDomain:
                    {
                        id = SCIDs.CompanyIds.DarlingHomes;
                        break;
                    }
                case CommonValues.MonarchGroupDomain:
                    {
                        id = SCIDs.CompanyIds.MonarchGroup;
                        break;
                    }
                default:
                    {
                        //Defaulting to this because it is the most high traffic of the three sites.
                        id = SCIDs.CompanyIds.TaylorMorrison;
                        break;
                    }
            }
            return id;
        }
    }
}