﻿using System.Collections.Generic;
using System.Linq;
using Lucene.Net.Search;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Search;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.DynamicFields;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;
using scSearchContrib.Searcher.Utilities;

namespace TM.Web.Custom.Queries.LuceneQueries
{

    public class CityQueries
    {
        protected string FullTextQuery;
        private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";

        public CityQueries()
        {
            _ctxDB = Sitecore.Context.Database.Name;
            _index = SearchManager.GetIndex(IndexName);
        }

        public IEnumerable<ID> GetAllActiveCitiesUnderDivision(ID divisionItemID)
        {


            var inactiveComms = new FieldSearchParam()
            {

                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.InActive.ToString(),
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };



            var closedComm = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.Closed.ToString(),
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };

            var searchableComms = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.StatusforSearch,
                FieldValue =
                    SCIDs.StatusIds.Status.Active,
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };

            var isLatestVersionParameter = new FieldSearchParam()
            {
                FieldName = Sitecore.Search.BuiltinFields.LatestVersion,
                FieldValue = "1",
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString(),
                Condition = QueryOccurance.Must
            };

            var q1 = inactiveComms.ProcessQuery(QueryOccurance.Must, _index);
            var q2 = closedComm.ProcessQuery(QueryOccurance.Must, _index);
            var q3 = searchableComms.ProcessQuery(QueryOccurance.Must, _index);
            var q4 = isLatestVersionParameter.ProcessQuery(QueryOccurance.Must, _index);
            var bool1 = new BooleanClause(q1, BooleanClause.Occur.MUST_NOT);
            var bool2 = new BooleanClause(q2, BooleanClause.Occur.MUST_NOT);
            var bool3 = new BooleanClause(q3, BooleanClause.Occur.MUST);
            var bool4 = new BooleanClause(q4, BooleanClause.Occur.MUST);

            var boolQ = new BooleanQuery();
            boolQ.Add(bool3);
            boolQ.Add(bool1);
            boolQ.Add(bool2);
            boolQ.Add(bool4);
            List<SkinnyItem> allActiveCommunities;
            using (var runner = new QueryRunner(IndexName))
            {
                allActiveCommunities = runner.RunQuery(boolQ);
            }

            var activeCities = allActiveCommunities.Select(c => c.Fields[LuceneDynamicFieldNames.ParentID]).Distinct().Where(i=> i!= null);
	    if ( activeCities.Count() > 0)
            return activeCities.Select(a => new ID(a));

        return null;
	}

        public IEnumerable<SkinnyItem> GetAllActiveCommunitiesUnderDivision(ID divisionItemID)
        {

            var inactiveComms = new FieldSearchParam()
            {

                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.InActive.ToString(),
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };



            var closedComm = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.CommunityStatus,
                FieldValue =
                    SCIDs.StatusIds.CommunityStatus.Closed.ToString(),
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };

            var searchableComms = new FieldSearchParam()
            {
                Database = _ctxDB,
                FieldName =
                    SCFieldNames.CommunityFields.StatusforSearch,
                FieldValue =
                    SCIDs.StatusIds.Status.Active,
                LocationIds = divisionItemID.ToString(),
                TemplateIds =
                    SCIDs.TemplateIds.CommunityPage.ToString()
            };

            var q1 = inactiveComms.ProcessQuery(QueryOccurance.Must, _index);
            var q2 = closedComm.ProcessQuery(QueryOccurance.Must, _index);
            var q3 = searchableComms.ProcessQuery(QueryOccurance.Must, _index);

            var bool1 = new BooleanClause(q1, BooleanClause.Occur.MUST_NOT);
            var bool2 = new BooleanClause(q2, BooleanClause.Occur.MUST_NOT);
            var bool3 = new BooleanClause(q3, BooleanClause.Occur.MUST);

            var boolQ = new BooleanQuery();
            boolQ.Add(bool3);
            boolQ.Add(bool1);
            boolQ.Add(bool2);

            using (var runner = new QueryRunner(IndexName))
            {
                return runner.RunQuery(boolQ);
            }

        }


        public IEnumerable<SkinnyItem> GetAllCitiesUnderDivision(ID divisionItemID)
        {
            var fiedQuery = new FieldSearchParam()
                                {
                                    Condition = QueryOccurance.Must,
                                    Database = _ctxDB,
                                    TemplateIds = SCIDs.TemplateIds.CityPage.ToString(),
                                    LocationIds = divisionItemID.ToString(),
				   
                                };
            using (var runner = new QueryRunner(IndexName))
            {
               return runner.GetItems(fiedQuery);
            }

	   
        }
    }
}