﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Search;
using TM.Domain.Entities;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.DynamicFields;
using scSearchContrib.Searcher;
using scSearchContrib.Searcher.Parameters;

namespace TM.Web.Custom.Queries.LuceneQueries
{
    public class DivisionQueries
    {
        protected string FullTextQuery;
        private Index _index;
        private string _ctxDB;
        private const string IndexName = "TMWebIndex";

        public DivisionQueries() : this(Sitecore.Context.Database.Name)
        {
        }
	

        public DivisionQueries(string db)
        {
            _ctxDB = db;
            _index = SearchManager.GetIndex(IndexName);
        }

	public Item GetDivisionFromLegacyId(string legacyId)
	{
	    var divisionQuery = new FieldSearchParam()
	                            {
	                                Database = _ctxDB,
	                                TemplateIds = SCIDs.TemplateIds.DivisionPage.ToString(),
	                                FieldName = SCFieldNames.DivisionFields.LegacyDivisionID,
	                                FieldValue = legacyId
	                            };

	    SkinnyItem division;
	    using (var runner = new QueryRunner(IndexName))
	    {
	        division = runner.GetItems(divisionQuery).FirstOrDefault();
	    }
	    if (division != null) return division.GetItem();
	    return null;
	}

    public IEnumerable<Item> GetAllCommunitiesAvaialableForAssitance(string divisionID)
    {
        var divisionQuery = new FieldSearchParam()
        {
            Database = _ctxDB,
            TemplateIds = SCIDs.TemplateIds.CommunityPage.ToString(),
	    LocationIds = divisionID,
            FieldName = SCFieldNames.CommunityFields.StatusforAssistance,
            FieldValue = SCIDs.StatusIds.Status.Active
        };

        List<SkinnyItem> division;
        using (var runner = new QueryRunner(IndexName))
        {
            division = runner.GetItems(divisionQuery);
        }
        if (division != null) return division.Select(d => d.GetItem());
        return null;
    }

	public IEnumerable<ID>GetAllActiveDivisionUnder(ID stateID)
        {


            var conditions = new MultiFieldSearchParam()
            {
                Database = _ctxDB,
                LocationIds = stateID.ToString(),
                TemplateIds = SCIDs.TemplateIds.CommunityPage.ToString(),
                InnerCondition = QueryOccurance.Must
            };
            var refineMents = new List<MultiFieldSearchParam.Refinement>();

            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.CommunityFields.CommunityStatus, SCIDs.StatusIds.Status.Active));
            refineMents.Add(new MultiFieldSearchParam.Refinement(SCFieldNames.CommunityFields.StatusforSearch, SCIDs.StatusIds.Status.Active));
	    refineMents.Add(new MultiFieldSearchParam.Refinement(Sitecore.Search.BuiltinFields.LatestVersion, "1"));


            IEnumerable<SkinnyItem> allActiveCommunities;

            using (var runner = new QueryRunner(IndexName))
            {
                allActiveCommunities = runner.GetItems(conditions);
            }

            var activeDivisions = allActiveCommunities.Select(c => c.Fields[LuceneDynamicFieldNames.GrandParentID]).Distinct().Where(i => i != null);

            if (activeDivisions.Count() > 0)
                return activeDivisions.Select(a => new ID(a));

            return null;
        }


    public IEnumerable<Item> GetAllActiveStatesAndDivisionsAcrossAllSites()
    {
        var conditions = new MultiFieldSearchParam()
        {
            Database = _ctxDB,
            TemplateIds = SCIDs.TemplateIds.CommunityPage.ToString(),
            InnerCondition = QueryOccurance.Must
        };
     
        var refineMents = new List<MultiFieldSearchParam.Refinement>
                                  {
                                      new MultiFieldSearchParam.Refinement(SCFieldNames.CommunityFields.CommunityStatus,
                                                                           SCIDs.StatusIds.Status.Active),
                                      new MultiFieldSearchParam.Refinement(SCFieldNames.CommunityFields.StatusforSearch,
                                                                           SCIDs.StatusIds.Status.Active),

	                              new MultiFieldSearchParam.Refinement(Sitecore.Search.BuiltinFields.LatestVersion, "1")
                                  };

        IEnumerable<SkinnyItem> searchResults;
        using (var runner = new QueryRunner(IndexName))
        {
           searchResults= runner.GetItems(conditions);
        }

	var activeDivisions = searchResults.Select(c => c.Fields[LuceneDynamicFieldNames.GrandParentID]).Distinct();

        var activeDivisionItems = activeDivisions.Select(d => Sitecore.Context.Database.GetItem(d));

        var activeStates = activeDivisionItems.Select(s => s.Parent);

	var activeDivisionsAndStates = new List<Item>(activeDivisionItems);

	activeDivisionsAndStates.AddRange(activeStates);
        return activeDivisionsAndStates;
    }

        public List<Item> GetDivisions(string divisionId, string homePath)
        {

            var companyID = Sitecore.Context.Database.GetItem(homePath).ID;

            using (var context = ContentSearchManager.GetIndex(IndexName).CreateSearchContext())
            {
                var divisionQuery = PredicateBuilder.True<TMSearchResultItem>()
                        .And(i => i.TemplateId == SCIDs.TemplateIds.DivisionPage);

                var combinedPredicate = PredicateBuilder.False<TMSearchResultItem>()
                    .Or(divisionQuery);

                if (!string.IsNullOrEmpty(divisionId))
                {
                    combinedPredicate = combinedPredicate.And(i => i[SCFieldNames.DivisionFields.LegacyDivisionID] == divisionId);
                }

                var results = context.GetQueryable<TMSearchResultItem>()
                     .Where(i => i.IsLatestVersion && !i.IsTemplate && i.Language == "en" && i.Name != "__Standard Values" && i.Paths.Contains(companyID))
                     .Where(combinedPredicate).GetResults();

                return results.Hits.Select(h => h.Document.GetItem()).ToList();
            }
        }

    }



}