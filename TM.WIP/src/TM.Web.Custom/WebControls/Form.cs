﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
    public class Form
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string ID { get; set; }
        public List<Fields> Fields { get; set; }
    }
}