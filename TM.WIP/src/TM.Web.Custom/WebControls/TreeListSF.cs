﻿using Sitecore.Diagnostics;
using Sitecore.Shell.Applications.ContentEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.WebControls
{
    public class TreeListSF : TreeList
    {
        public TreeListSF()
        {
            base.Class = "scContentControl scContentControlTreelist TreeListSF";
        }
        protected override string GetHeaderValue(Sitecore.Data.Items.Item item)
        {
            Assert.ArgumentNotNull((object)item, "item");

            var str = string.IsNullOrEmpty(this.DisplayFieldName) ? item.DisplayName : item[this.DisplayFieldName];
            var parentStr = GetFormName(item);
            
            return string.IsNullOrEmpty(str) ?
                parentStr + item.DisplayName :
                parentStr + str;
        }

        private string GetFormName(Sitecore.Data.Items.Item item)
        {
            var formName = String.Empty;

            /*while (item.TemplateID != SCIDs.TemplateIds.WFFMForm) {
                item = item.Parent;
            }*/
            var parentSufix = item.Parent.Parent.DisplayName + "/" + item.Parent.DisplayName + "/";
            return parentSufix;
        }
    }
}


