﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class StateProvinceInfo
	{
		public string StateName { get; set; }
		public string StatusforSearch { get; set; }
		public string StatusforAssistance { get; set; }
		public double SearchCentroidLatitude { get; set; }
		public double SearchCentroidLongitude { get; set; }
		public int SearchCentroidZoom { get; set; }
		public string StateDisclaimerImage { get; set; }
		public string FacebookURL { get; set; }
		public string TwitterURL { get; set; }
		public string YouTubeURL { get; set; }
		public string PinterestURL { get; set; }
		public string GooglePlusURL { get; set; }
		public string BlogURL { get; set; }
	}
}