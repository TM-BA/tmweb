﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.Design;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Web.UI;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Pipelines.RequestProcessor;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.WebControls
{
   
        /// <summary>Breadcrumb control</summary>
        //[Designer(typeof(WebControlDesigner))]
        [ToolboxData("<{0}:Breadcrumb runat=server></{0}:Breadcrumb>")]
        public class Breadcrumb : WebControl
        {

            #region Fields


            

            string m_dividerText = "";
           
            string m_textField = "";

            #endregion

            #region Properties

            private ArrayList _itemList;
            public ArrayList ItemList
            {
                get {
                    if (_itemList == null)
                    {
                        _itemList = new ArrayList();
                    }
                    return _itemList; 
                }
                set { _itemList = value; }
            }


            /// <summary>Divider text</summary>
            [Category("Appearance")]
            [Description("Text (or character) used for dividing items.")]
            //[WebControlProperty]
            public string DividerText
            {
                get
                {
                    return m_dividerText;
                }
                set
                {
                    m_dividerText = value;
                }
            }

          
            /// <summary>Name of field containing text</summary>
            [Category("Fields")]
            //[WebControlProperty]
            public string TextField
            {
                get
                {
                    return m_textField;
                }
                set
                {
                    m_textField = value;
                }
            }

            #endregion

            #region Protected methods

            /// <summary>Render control</summary>
            protected override void DoRender(HtmlTextWriter output)
            {
                
                Item rootItem = Sitecore.Context.Database.Items[Sitecore.Context.Site.StartPath].Parent;
                Item itm = GetItem();
                Item originalItem = GetItem();
                if (Sitecore.Context.Request.FilePath != null)
                {
                    var lowerCasePath = Sitecore.Context.Request.FilePath.ToLowerInvariant().TrimEnd("/");
                    var virtualUrlSegment = PageUrls.VirtualUrls.Find(lowerCasePath.EndsWith);


                    if (virtualUrlSegment == "request-information")
                    {

                        var itemPath = lowerCasePath;
                        var tabUrlSegment = itemPath.LastNthSegment(2);
                        if (PageUrls.VirtualUrls.Contains(tabUrlSegment) || PageUrls.AdditionalCommunityPages.Contains(tabUrlSegment))
                        {
                            ItemList.Add(new BreadCrumbItem(tabUrlSegment.ToTitleCase().Replace("-", " "), null)
                            {
                                VirtualLink =
                                    itemPath.Replace("request-information", string.Empty).TrimEnd("/")
                            });


                        }

                    }
                    else if (virtualUrlSegment == "thank-you")
                    {
                        var itemPath = lowerCasePath;
                        var lastButOneSegment = itemPath.LastNthSegment(2);
                        var tabUrlSegment = itemPath.LastNthSegment(3);
                        if (lastButOneSegment == "request-information" || lastButOneSegment == "sign-up-for-updates")
                        {
                            ItemList.Add(new BreadCrumbItem("Request Information", null)
                            {
                                VirtualLink =
                                    itemPath.Replace("thank-you", string.Empty).TrimEnd("/")
                            });

                        }
                        if (PageUrls.VirtualUrls.Contains(tabUrlSegment) || PageUrls.AdditionalCommunityPages.Contains(tabUrlSegment))
                        {
                            ItemList.Add(new BreadCrumbItem(tabUrlSegment.ToTitleCase().Replace("-", " "), null)
                            {
                                VirtualLink =
                                itemPath.Replace("request-information/thank-you", string.Empty).TrimEnd("/")
                            });
                        }


                    }
                    else if (itm.TemplateID == SCIDs.TemplateIds.CommunityPage || itm.TemplateID == SCIDs.TemplateIds.PlanPage || itm.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
                    {
                        var itemPath = lowerCasePath;
                        var tabUrlSegment = itemPath.LastNthSegment(1);
                        if (PageUrls.VirtualUrls.Contains(tabUrlSegment))
                        {
                            ItemList.Add(new BreadCrumbItem(tabUrlSegment.ToTitleCase().Replace("-", " "), itm) { VirtualLink = itemPath.Replace(tabUrlSegment, string.Empty).TrimEnd("/") });
                        }
                    }
                }
                
                

                while (itm != null && itm.ID != rootItem.ID )
                {
                    if (itm.Visualization.Layout != null)
                    {
                        string text = TextField == string.Empty ? itm.Name : itm.Fields[TextField].Value;
                        ItemList.Add(new BreadCrumbItem(text, itm));
                    }
                    itm = itm.Parent;
                }

                ItemList.Reverse();
                if (ItemList.Count > 1)
                {
                    if (rootItem.TemplateID.ToString() == SCIDs.TemplateIds.MicrositeTemplate || originalItem.TemplateID.ToString() == SCIDs.TemplateIds.SplashPage)
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb-tm");
                    }
                    else
                    {
                        output.AddAttribute(HtmlTextWriterAttribute.Class, ControlStyle.CssClass);
                    }

                    output.AddAttribute(HtmlTextWriterAttribute.Style, ControlStyle.CssClass);
                    output.RenderBeginTag(HtmlTextWriterTag.Ul); //<ul>

                    if (ItemList.Count == 0)
                        output.Write("[No item]");

                    for (int i = 0; i < ItemList.Count; i++)
                    {
                        var item = (BreadCrumbItem) ItemList[i];
                        output.RenderBeginTag(HtmlTextWriterTag.Li); //<li>
                        if (item.Node != null && item.Node.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
                        {
                            var regex =
                                new System.Text.RegularExpressions.Regex(
                                    @"^Home Available Now at\s(?<street>.*)\s(\d*)$");
                            var match = regex.Match(item.Text);
                            if (match.Success)
                            {
                                item.Text = match.Groups["street"].Value;
                            }


                        }
                        //The link
                        if (i == ItemList.Count - 1)
                        {
                            if (item.Node != null && item.Node.TemplateID == SCIDs.TemplateIds.PromoFolder)
                            {
                                output.Write("Updates");
                            }
                            else if (item.Node != null && item.Node.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
                            {
                                output.Write(item.Text.Replace("Lot", "Homesite"));
                            }
                            else
                            {
                                if (item.Node != null && item.Node.ParentID != SCIDs.TemplateIds.Account)
                                {
                                    output.Write(item.Text);
                                }

                            }
                        }
                        else
                        {

                            if (item.Node == null ||
                                (item.Node.TemplateID != SCIDs.TemplateIds.ContentFolders &&
                                 item.Node.TemplateID != SCIDs.TemplateIds.WhyCompanyFolder))
                            {
                                var link = item.Node == null
                                    ? item.VirtualLink
                                    : item.Node.TemplateID == SCIDs.TemplateIds.ProductTypePage
                                        ? "/"
                                        : SCUtils.GetItemUrl(item.Node);

                                output.AddAttribute(HtmlTextWriterAttribute.Href, link);
                                output.RenderBeginTag(HtmlTextWriterTag.A);
                            }
                            if (item.Node != null && item.Node.TemplateID == SCIDs.TemplateIds.PromoFolder)
                            {
                                output.Write("Updates");
                            }
                            else
                            {
                                if (item.Node == null || (item.Node.TemplateID != SCIDs.TemplateIds.Account))
                                {
                                    output.Write(item.Text);
                                }

                            }
                            if (item.Node == null ||
                                (item.Node.TemplateID != SCIDs.TemplateIds.ContentFolders &&
                                 item.Node.TemplateID != SCIDs.TemplateIds.WhyCompanyFolder))
                            {
                                output.RenderEndTag(); //</a>
                            }
                        }
                        //Divider span
                        output.RenderBeginTag(HtmlTextWriterTag.Span);

                        if (i < ItemList.Count - 1)
                        {

                            if (item.Node == null || (item.Node.TemplateID != SCIDs.TemplateIds.Account))
                            {

                                if (rootItem.TemplateID.ToString() == SCIDs.TemplateIds.MicrositeTemplate || originalItem.TemplateID.ToString() == SCIDs.TemplateIds.SplashPage)
                                {
                                    if (String.IsNullOrEmpty(DividerText))
                                        DividerText = ">";
                                    output.Write(DividerText);
                                }
                                else
                                {
                                    output.Write(DividerText);
                                }

                            }



                        }
                        output.RenderEndTag(); //</span>
                        output.RenderEndTag(); //</li>
                    }
                    output.RenderEndTag(); //</ul>
                }
                else
                {
                    //When list is less than 1
                    if (Sitecore.Context.PageMode.IsPageEditor)
                    {
                        if (rootItem.TemplateID.ToString() == SCIDs.TemplateIds.MicrositeTemplate || originalItem.TemplateID.ToString() == SCIDs.TemplateIds.SplashPage)
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, "breadcrumb-tm");
                        }
                        else
                        {
                            output.AddAttribute(HtmlTextWriterAttribute.Class, ControlStyle.CssClass);
                        }
                        output.AddAttribute(HtmlTextWriterAttribute.Style, ControlStyle.CssClass);
                        output.RenderBeginTag(HtmlTextWriterTag.Ul); //<ul>
                        output.Write("[Breadcrumb: No items or in Home node]");
                        output.RenderEndTag(); //</ul>
                    }
                }
            }
            #endregion
        }
        public class BreadCrumbItem
        {

            public string Text;
            public Item Node;
            public string VirtualLink;
            public BreadCrumbItem(string text, Item node)
            {
                this.Text = text;
                this.Node = node;
            }
        }
    }
