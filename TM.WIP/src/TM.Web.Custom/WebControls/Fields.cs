﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
    public class Fields
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public SalesForceInformation SFInfo { get; set; }
    }

    public class SalesForceInformation
    {
        public string SFName { get; set; }
        public string SFDisplayName { get; set; }
        public string SFID { get; set; }
    }
}