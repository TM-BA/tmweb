﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class SubcommunityInfo
	{
		public Guid SubCommunityID { get; set; }
		public Guid SubCommunityParentID { get; set; }		
		public String SubcommunityName { get; set; }
		public String SubcommunityLegacyID { get; set; }
		public int SubcommunitySortOrder { get; set; }
		public GlobalEnums.SearchStatus SubcommunityStatus { get; set; }
	}

}