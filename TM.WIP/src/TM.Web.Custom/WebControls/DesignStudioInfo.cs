﻿using System;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class DesignStudioInfo
	{
		public Guid DesignStudioId { get; set; }
		public string DesignStudioName { get; set; }
		public String DesignStudioLegacyId { get; set; }
		public GlobalEnums.SearchStatus DesignStudioStatus { get; set; }
		public String Hours { get; set; }
		public Double DesignStudioLatitude { get; set; }
		public Double DesignStudioLongitude { get; set; }
		public String StreetAddress1 { get; set; }
		public String StreetAddress2 { get; set; }
		public String City {get; set;}
		public String StateProvince{ get; set; }
		public String StateProvinceAbbreviation {get; set;}
		public String ZipPostalCode { get; set; }
		public String Phone { get; set; }
		public String Fax { get; set; }
		public String DrivingDirectionsText { get; set; }
		public string MapIcon { get; set; }
		public string ClusteredMapIcon { get; set; }
		public string SingleClusteredMapIcon { get; set; }
		public string DesignStudioDetailsUrl { get; set; }
		public bool IsCurrentDomain { get; set; }
		public string Domain { get; set; }
		public string CompanyName { get; set; }
		public string CompanyNameAbbreviation { get; set; }
		public string HostName { get; set; }
		public string MapPinStatus { get; set; }
		public int MapCount { get; set; }
		public string StateClusteredDesignStudioFlyouts { get; set; }
		public string DivisionClusteredDesignStudioFlyouts { get; set; }
		public string DesignStudioFlyouts { get; set; }
	}
}