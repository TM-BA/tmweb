﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TM.Web.Custom.WebControls
{
    public class DivisionInfo
    {

        Guid divisionId;

        public Guid DivisionId
        {
            get { return divisionId; }
            set { divisionId = value; }
        }

        string productTypeName;

        public string ProductTypeName
        {
            get { return productTypeName; }
            set { productTypeName = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string streetAddress1;

        public string StreetAddress1
        {
            get { return streetAddress1; }
            set { streetAddress1 = value; }
        }

        string streetAddress2;

        public string StreetAddress2
        {
            get { return streetAddress2; }
            set { streetAddress2 = value; }
        }

        string city;

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        string stateProvinceAbbreviation;

        public string StateProvinceAbbreviation
        {
            get { return stateProvinceAbbreviation; }
            set { stateProvinceAbbreviation = value; }
        }

        string zipPostalCode;

        public string ZipPostalCode
        {
            get { return zipPostalCode; }
            set { zipPostalCode = value; }
        }

        string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        string fullAddress;

        public string FullAddress
        {
          get { return fullAddress; }
          set { fullAddress = value; }
        }

        string cityState;

        public string CityState
        {
            get { return cityState; }
            set { cityState = value; }
        }

        string url;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        string warrantyLine;
        public string WarrantyLine
        {
            get { return warrantyLine; }
            set { warrantyLine = value; }
        }

        string afterHoursEmergency;
        public string AfterHoursEmergency
        {
            get { return afterHoursEmergency; }
            set { afterHoursEmergency = value; }
        }
    }
}
