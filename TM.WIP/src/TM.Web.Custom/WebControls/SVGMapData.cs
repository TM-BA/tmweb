﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
	[Serializable]
	public class SvgMapData
	{
		public Dictionary<string, string> ActiveSvgMapStateProAndAbbreviation { get; set; }
		public Dictionary<string, List<SvgDivisionData>>SvgMapCityDetails { get; set; } 
	}

	[Serializable]
	public class SvgDivisionData
	{ 
		public string N {get; set;}
		public string h {get; set;}
		public bool tar { get; set; }
		public Int32 X {get; set;}
		public Int32 Y {get; set;}
		public bool rtl {get; set;}
	}

	/// <summary>
	/// Custom comparer for the SvgDivisionData class
	/// </summary>
	public class SvgDivisionDataComparer : IEqualityComparer<SvgDivisionData>
	{
		public bool Equals(SvgDivisionData x, SvgDivisionData y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
				return false;
			return x.N == y.N && x.N == y.N;
		}

		public int GetHashCode(SvgDivisionData info)
		{
			if (ReferenceEquals(info, null)) return 0;
			var hashName1 = info.N == null ? 0 : info.N.GetHashCode();
			var hashName2 = info.N.GetHashCode();
			return hashName1 ^ hashName2;
		}
	}
}