﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.WebControls
{
    public class SalesForceMapping
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Id { get; set; }
        public List<ItemMapping> Mapps { get; set; }
    }

    public class ItemMapping
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}