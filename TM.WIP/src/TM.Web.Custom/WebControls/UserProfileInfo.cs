﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Utils.Web;

namespace TM.Web.Custom.WebControls
{
	public class UserProfileInfo
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string Password { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string City { get; set; }
		public string StateAbbreviation { get; set; }
		public string Zipcode { get; set; }
		public string Phone { get; set; }
		public string PhoneExtension { get; set; }
		public string AreaofInterest { get; set; }
		public GlobalEnums.MyCompanies MyCompany { get; set; }
	}
}