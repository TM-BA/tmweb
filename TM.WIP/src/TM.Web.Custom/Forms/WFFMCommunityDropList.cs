﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Sites;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using ListItemCollection = Sitecore.Form.Web.UI.Controls.ListItemCollection;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Forms
{
    public class WFFMCommunityDropList :DropList 
    {

        private Item _currentDivision;
        public Item CurrentDivision
        {
            get
            {
                return _currentDivision ??
                       (_currentDivision =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, Sitecore.Context.Item.ID,
                                                              Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath)));
            }
        }
        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }
        public Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        public Item CurrentHomeItem
        {
            get
            {
                return SCContextDB.GetItem(SCCurrentSite.StartPath);
            }
        }

        protected string SCCurrentSitePath
        {
            get { return SCCurrentSite.StartPath; }
        }

        protected string SCCurrentHomePath
        {
            get { return CurrentHomeItem.Paths.FullPath; }
        }

        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(BooleanField)), Localize]
        public string HideTitle
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value == "No"; }
        }
        
        protected override void InitItems(ListItemCollection items)
        {
            //load items in init so viewstate can be set
            base.InitItems(GetItems());

            var WebSession = new TMSession();
            if (WebSession["communityLegacyId"] != null)
            {
                base.Visible = false;
            }
        }

        private ListItemCollection GetItems()
        {
            base.KeepHiddenValue = false;

            var WebSession = new TMSession();
            var parentID = WebSession["parentID"] as Sitecore.Data.ID;
            Item[] communities = new CommunityQueries().GetAllActiveCommunitesUnderCity(parentID).Select(i => i.GetItem()).ToArray();
                        
            List<ListItem> sortedList = new List<ListItem>();
            foreach (var community in communities)
            {
                var communityIdFld = community.Fields[SCIDs.CommunityFields.CommunityLegacyID];
                var communityNameFld = community.Fields[SCIDs.CommunityFields.CommunityName];
                if (communityIdFld != null && communityIdFld.Value.IsNotEmpty())
                {
                    sortedList.Add(new ListItem((communityNameFld.Value + ", " + community.Parent.GetItemName(true)), communityIdFld.Value.ToString()));
                }
            }

            sortedList = (sortedList.OrderBy(x => x.Text.Split(',')[0].TrimEnd()).ThenBy(y => y.Text.Split(',')[1])).ToList();
            foreach (ListItem item in sortedList)
            {
                items.Add(item);
            }         

            items[0].Value = "";

            return new ListItemCollection(items);           
        }      
    }
}