﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Sites;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using ListItemCollection = Sitecore.Form.Web.UI.Controls.ListItemCollection;
using Sitecore.Data;

namespace TM.Web.Custom.Forms
{
    public class WFFMShortAnswerList : DropList
    {
        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }
        public Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        public Item CurrentHomeItem
        {
            get
            {
                return SCContextDB.GetItem(SCCurrentSite.StartPath);
            }
        }

        protected string SCCurrentSitePath
        {
            get { return SCCurrentSite.StartPath; }
        }

        protected string SCCurrentHomePath
        {
            get { return CurrentHomeItem.Paths.FullPath; }
        }

        protected override void InitItems(ListItemCollection items)
        {
            base.KeepHiddenValue = false;

            Item sharedFieldValues = SCContextDB.GetItem(SCIDs.SharedFieldValues.ShortAnswerList);

            Item[] shortAnwserList = sharedFieldValues.GetChildren().ToArray();

            foreach (Item item in shortAnwserList)
                {
                    items.Add(new ListItem(item.DisplayName, item.DisplayName));
                }

            base.InitItems(items);

            
        }

    }
}