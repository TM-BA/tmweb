﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Sites;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Forms
{
    public abstract class WFFMSingleLineTextBase:SingleLineText
    {

        private readonly TMSession _webSession = new TMSession();

        public TMSession WebSession
        {
            get { return _webSession; }
        }

        protected WFFMSingleLineTextBase()
	{
	    
	}

        protected WFFMSingleLineTextBase(HtmlTextWriterTag tag)
            : base(tag)
        {
            
        }
        
        public TMUser CurrentWebUser
        {
            get { return WebSession.GetUser<TMUser>(); }
           
        }


        public string SCCurrentDomainName
        {
            get { return SCCurrentSite.Domain.Name; }
        }

        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }

        [VisualProperty("PlaceHolder Text", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(EditField)), Localize]
        public string PlaceHolder
        {
            get { return base.textbox.Attributes["placeholder"]; }

            set { textbox.Attributes["placeholder"] = value; }
        }

        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(BooleanField)), Localize]
        public string HideTitle 
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value=="No"; }
        }

        [VisualProperty("Is Hidden",500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(BooleanField)), Localize]
        public string IsHidden
        {
            get {return this.Visible.ToString(CultureInfo.InvariantCulture); }
            set {

                this.Visible = value == "No"; ;
            
            }
        }

        [VisualProperty("Field Name", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(EditField)), Localize]
        public string FieldName { get; set; }

        protected Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        protected Item SCContextItem
        {
            get { return Sitecore.Context.Item; }
        }

        protected Item CurrentHomeItem
        {
            get
            {
                return SCContextDB.GetItem(SCCurrentSite.StartPath);
            }
        }


        protected Item CurrentProductItem
        {
            get
            {   
                var url = HttpContext.Current.Request.Url;
                var homePath = SCCurrentSite.StartPath;
                var productSegment = url.Segments[1].TrimEnd("/");
                var productPath =homePath+"/"+MainUtil.DecodeName(productSegment);
                var item = Sitecore.Context.Site.Database.GetItem(productPath);

                return item;


            }
        }

        protected Item CurrentCommunity
        {
            get
            {
                var url = HttpContext.Current.Request.Url;

                if (url.ToString().ToLowerInvariant().Contains("request-information"))
                {
                    // Get the path to the Home item
                    var homePath = Sitecore.Context.Site.StartPath;
                    if (!homePath.EndsWith("/"))
                        homePath += "/";
                    var absPath = url.AbsolutePath;
                    var idx = absPath.LastIndexOf("/", System.StringComparison.Ordinal) + 1;
                    var path = absPath.Substring(0, idx);
                    // Get the path to the item, removing virtual path if any
                    var itemPath = MainUtil.DecodeName(path);
                    if (itemPath.StartsWith(Sitecore.Context.Site.VirtualFolder))
                        itemPath = itemPath.Remove(0, Sitecore.Context.Site.VirtualFolder.Length);

                    // Obtain the item
                    var fullPath = homePath + itemPath;
                    var item = Sitecore.Context.Site.Database.GetItem(fullPath);
                    return item;
                }
                else
                {
                 return   SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, SCContextItem.ID,
                                                                  CurrentHomeItem);
                }
            }

        }

        protected Item CurrentItem
        {
            get
            {
                var url = HttpContext.Current.Request.Url;
		   
                    // Get the path to the Home item
                    var homePath = Sitecore.Context.Site.StartPath;
                    if (!homePath.EndsWith("/"))
                        homePath += "/";


                var absPath = Regex.Replace(url.AbsolutePath, "request-information", string.Empty,
                                            RegexOptions.IgnoreCase);
                    var idx = absPath.LastIndexOf("/", System.StringComparison.Ordinal) + 1;
                    var path = absPath.Substring(0, idx);
                    // Get the path to the item, removing virtual path if any
                    var itemPath = MainUtil.DecodeName(path);
                    if (itemPath.StartsWith(Sitecore.Context.Site.VirtualFolder))
                        itemPath = itemPath.Remove(0, Sitecore.Context.Site.VirtualFolder.Length);

                    // Obtain the item
                    var fullPath = homePath + itemPath;
                    var item = Sitecore.Context.Site.Database.GetItem(fullPath);
                    return item;

            }
        }

    }
}