﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using TM.Utils.Extensions;
using TM.Utils.Web;


namespace TM.Web.Custom.Forms
{
    public class RedirectThankYouPage : ISaveAction
    {
        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            var root = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
            //var item = Context.Database.GetItem(formid);
            
            var alternate = fields.GetEntryByName("Thank You Page");

            if (alternate != null)
            {
                if (String.IsNullOrEmpty(alternate.Value))
                {
                    HttpContext.Current.Response.Redirect(root + "/thank-you");
                }
                else
                {
                    HttpContext.Current.Response.Redirect(root + "/" + alternate.Value);
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect(root + "/thank-you");
            }

            
        }

        public string TestItemID
        {
            get;
            set;
        }

    }
}