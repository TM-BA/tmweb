﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Linq;
using DateUtil = Sitecore.DateUtil;
using Sitecore.Diagnostics;
using System.Configuration;
using Sitecore.Data.Fields;

namespace TM.Web.Custom.Forms
{
    public class FormPostSalesForce : ISaveAction, ISubmit
    {

        private List<Item> _mappingList;

        public bool IsDebug
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Debug);
                if (Item == null ||
                    Item.Fields == null ||
                    String.IsNullOrEmpty(Item[SCFieldNames.SFIntegration.Debug.Status]))
                {
                    return false;
                }
                var Active = (CheckboxField)Item.Fields[SCFieldNames.SFIntegration.Debug.Status];
                return Active.Checked;
            }
        }

        public string DebugField
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Debug);
                return Item[SCFieldNames.SFIntegration.Debug.Field] ?? "debug";
            }
        }

        public string DebugEmail
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.DebugEmail);
                return Item[SCFieldNames.SFIntegration.DebugEmail.Email] ?? "";
            }
        }

        public string DebugEmailField
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.DebugEmail);
                return Item[SCFieldNames.SFIntegration.DebugEmail.Field] ?? "debugEmail";
            }
        }
        public List<Item> MappingList
        {
            get
            {
                if (_mappingList == null ||
                    _mappingList.Count() == 0)
                {
                    var mappingNode = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Mapping);
                    _mappingList = mappingNode.Axes
                        .GetDescendants()
                        .Where(x => x.TemplateID == SCIDs.TemplateIds.MappingItem)
                        .ToList();
                }
                return _mappingList;
            }
        }

        public string PostURL
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.SFPostUrl);
                return Item[SCFieldNames.SFIntegration.PostURL.URL];
            }
        }

        public string Oid
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Oid);
                return Item[SCFieldNames.SFIntegration.Oid.FieldID];
            }
        }

        public string OidName
        {
            get
            {
                var Item = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Oid);
                return Item[SCFieldNames.SFIntegration.Oid.FieldName];
            }
        }

        private string UseMapping(FieldItem field)
        {
            var response = field.Name;
            MappingList.ForEach(i =>
            {
                var wffmFields = i[SCFieldNames.SFIntegration.Mapping.WFFMFields];
                var fieldId = i[SCFieldNames.SFIntegration.Mapping.FieldId];

                if (!string.IsNullOrEmpty(wffmFields) &&
                    !string.IsNullOrEmpty(fieldId))
                {
                    var wffmSplit = wffmFields
                        .Split('|').ToList();

                    wffmSplit.ForEach(x =>
                    {
                        if (x == field.ID.ToString())
                            response = fieldId;
                    });

                }
            });
            return response;
        }

        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                var formFieldNameValues = new NameValueCollection();
                foreach (AdaptedControlResult field in fields)
                {
                    var fieldItem = new FieldItem(Context.Database.GetItem(field.FieldID));
                    formFieldNameValues.Add(UseMapping(fieldItem), field.Value);

                }

                formFieldNameValues.Add(OidName, Oid);

                if (IsDebug)
                {
                    formFieldNameValues.Add(DebugField, "1");
                    if (!String.IsNullOrEmpty(DebugEmail))
                    {
                        formFieldNameValues.Add(DebugEmailField, DebugEmail);
                    }
                }

                Post(formFieldNameValues, PostURL);
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error while submitting form: " + formid, e, this);
            }
        }

        private void Post(NameValueCollection formFieldNameValues, string url)
        {
            if (url.IsEmpty() || formFieldNameValues == null)
            {
                return;
            }

            System.Net.ServicePointManager
                .SecurityProtocol = System.Net.SecurityProtocolType.Tls11;

            var dataStr = new StringBuilder();
            var ampersand = string.Empty;

            foreach (string key in formFieldNameValues.AllKeys)
            {
                dataStr.Append(ampersand);
                dataStr.Append(HttpUtility.UrlEncode(key));
                dataStr.Append("=");
                dataStr.Append(HttpUtility.UrlEncode(formFieldNameValues[key]));
                ampersand = "&";
            }

            HttpWebRequest request;
            try
            {
                var postData = dataStr.ToString();
                byte[] bytes = Encoding.UTF8.GetBytes(postData);

                request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = bytes.Length;

                // add post data to request
                Stream postStream = request.GetRequestStream();
                postStream.Write(bytes, 0, bytes.Length);
                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                postStream.Flush();
                postStream.Close();
            }
            catch (UriFormatException)
            {
                request = null;
            }
        }

        public void Submit(ID formid, AdaptedResultList fields)
        {
            Execute(formid, fields, fields);
        }
    }
}