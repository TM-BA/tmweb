﻿using System;
using System.Web;
using System.Web.UI;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Forms
{
    public class WFFMHoneypotCaptcha : WFFMSingleLineTextBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "WFFMHide";
        }

        public override Sitecore.Form.Core.Controls.Data.ControlResult Result
        {
            get
            {
                var result = base.Result;

                if (result.Value == null || !string.IsNullOrEmpty(result.Value.ToString()))
                {
                    Sitecore.Diagnostics.Log.Error("Honeypot validation failed", this);
                    throw new Exception("Honeypot validation failed");
                }
                return result;
            }
        }

        
    }
}