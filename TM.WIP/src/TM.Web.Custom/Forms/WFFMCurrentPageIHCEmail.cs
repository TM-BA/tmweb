﻿using System;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Controls.Data;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Net;
using System.Text;
using System.Web;

namespace TM.Web.Custom.Forms
{
    public class WFFMCurrentPageIHCEmail : WFFMHiddenField
    {
        /*
         * Add additional field to template called "Lead Email In Addition to IHC" field that can be used to send email to Sales Office or Other email. If none specified use IHC email.
         */

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (SCContextItem != null)
	    {
             Text = SCContextItem["Responding Email Address"]??Text; 
	    }

           
        }
    }
}