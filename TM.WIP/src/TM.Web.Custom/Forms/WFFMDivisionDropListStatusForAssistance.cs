﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Visual;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using SCContext = Sitecore.Context;

namespace TM.Web.Custom.Forms
{
    public class WFFMDivisionDropListStatusForAssistance : DropList
    {
        private Item _currentDivision;

        public Item CurrentDivision
        {
            get { return _currentDivision ?? (_currentDivision = GetCurrentDivision()); }
        }


        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof (BooleanField)), Localize]
        public string HideTitle
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value == "No"; }
        }

        protected override void OnInit(EventArgs e)
        {
            if (CurrentDivision != null)
            {
                Field divisionIdFld = CurrentDivision.Fields[SCIDs.DivisionFieldIDs.LegacyDivisionID];
                Field divisionNameFld = CurrentDivision.Fields[SCIDs.DivisionFieldIDs.Divisionname];

                if (divisionIdFld != null && divisionIdFld.Value.IsNotEmpty())
                {
                    var item = new ListItem(divisionNameFld.Value, divisionIdFld.Value);
                    item.Selected = true;
                    items.Add(item);
                    droplist.Attributes["style"] = "display:none";
                    title.Style.Add("display", "none");
                    generalPanel.Style.Add("display", "none");
                    base.InitItems(items);
                    return;
                }
            }

            string query = SCFastQueries.AllDivisionsAvaialbleForAssistanceUnder(SCContext.Site.StartPath);

            Item[] divisions = SCContext.Database.SelectItems(query);


            var sortedList = new List<ListItem>();
            foreach (Item division in divisions)
            {
                string divisionIdFld = division.ID.ToString();
                string divisionNameFld = division[SCIDs.DivisionFieldIDs.Divisionname];
                string divisionWarrantyEmail = division["Warranty Email Address"];
                if (divisionIdFld.IsNotEmpty())
                {
                    string name = divisionNameFld + ", " + division.Parent.GetItemName(true);
                    sortedList.Add(new ListItem(name, divisionIdFld + "#" + divisionWarrantyEmail + "#" + name));
                }
            }

            sortedList.Sort((a, b) => string.Compare(a.Text, b.Text));

            foreach (ListItem item in sortedList)
            {
                items.Add(item);
            }

            droplist.Items.AddRange(items.ToArray());

            HttpContext.Current.Items.Add("contactusareaddlID", this.ID);
            base.OnInit(e);
        }


        protected override void OnLoad(EventArgs e)
        {
            var WebSession = new TMSession();

            string selectedValue = InnerListControl.SelectedValue;

            if (selectedValue.IsNotEmpty())
                selectedValue = selectedValue.Split('#')[0];

            string siteName = SCContext.Site.Name;

            WebSession[string.Format("WFFM{0}contactusareaddl", siteName)] = selectedValue;
            WebSession.Save();
            HttpContext.Current.Items.Add("contactusareaddl", selectedValue);
            droplist.AutoPostBack = true;
            base.OnLoad(e);
        }


        private Item GetCurrentDivision()
        {
            return SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, SCContext.Item.ID,
                SCContext.Database.GetItem(SCContext.Site.StartPath));
        }
    }
}
