﻿using System;
using System.Web;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Forms
{
    public class WFFMDateSubmitted : WFFMSingleLineTextBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            help.Visible = false;
            title.Visible = false;
            textbox.Style.Add("display","none");

            Text = DateTime.Now.ToString("s");
        }
    }
}