﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Events;

namespace TM.Web.Custom.Forms
{
    public class FormSubmitEventHandler
    {

	public void OnFormsSaved(object sender, EventArgs args)
	{
           var formID =  Event.ExtractParameter<ID>(args, 0);

           HttpContext.Current.Items["WFFMPostedFormID"] = formID.ToString();
	}
    }
}