﻿using System;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Controls.Data;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Net;
using System.Text;
using System.Web;
using TM.Utils.Web;

namespace TM.Web.Custom.Forms
{
    public class WFFMCommunityID : WFFMHiddenField
    {
      
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            var WebSession = new TMSession();
            
	        if(Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.CommunityPage)
	        {
	            Text = Sitecore.Context.Item[SCIDs.CommunityFields.CommunityLegacyID];
	        }
	        else if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["cid"]))
	        {
	            Text = HttpContext.Current.Request.QueryString["cid"];
	        }
	        else if(CurrentCommunity!=null && CurrentCommunity.TemplateID == SCIDs.TemplateIds.CommunityPage)
	        {
                   Text = CurrentCommunity[SCIDs.CommunityFields.CommunityLegacyID];
	        }
            else if (WebSession["communityLegacyId"] != null)
            {
                Text = WebSession["communityLegacyId"].ToString();
            }  
        }
    }
}