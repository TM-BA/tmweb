﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Web;
using Sitecore.Data;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using TM.Utils.Extensions;
using TM.Utils.Web;


namespace TM.Web.Custom.Forms
{
    public class SaveToSession : ISaveAction
    {
        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            TMSession WebSession = new TMSession();
            string url = string.Empty;
            var formFieldNameValues = new NameValueCollection();
            foreach (AdaptedControlResult field in fields)
            {
                var fieldItem = new FieldItem(Sitecore.Context.Database.GetItem(field.FieldID));

                var fieldName = ExtensionsToString.Between(fieldItem.LocalizedParameters, "<FieldName>", "</FieldName>");

                fieldName = fieldName.IsEmpty() ? fieldItem.Title : fieldName;

                WebSession["WFFM" + fieldName] = field.Value;
            }
            WebSession.Save();
        }
    }
}