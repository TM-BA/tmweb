﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Web.Custom.Layouts.SubLayouts;
using Sitecore.Form.Core.Visual;
using System.Globalization;

namespace TM.Web.Custom.Forms
{
    public class WFFMPhoneNumber : Telephone
    {
        [VisualProperty("Hide Title", 500)]
        [VisualCategory("Appearance")]
        [VisualFieldType(typeof(BooleanField)), Localize]
        public string HideTitle
        {
            get { return base.title.Visible.ToString(CultureInfo.InvariantCulture); }

            set { title.Visible = value == "No"; }
        }

        [VisualProperty("PlaceHolder Text", 500)]
        [VisualCategory("Appearance")]
        public string PlaceHolder
        {
            get { return base.textbox.Attributes["placeholder"]; }

            set { textbox.Attributes["placeholder"] = value; }
        }
    }
}