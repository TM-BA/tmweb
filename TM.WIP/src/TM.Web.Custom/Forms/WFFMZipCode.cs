﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.Forms
{
    public class WFFMZipCode : WFFMSingleLineTextBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (CurrentWebUser !=null && CurrentWebUser.Zipcode.IsNotEmpty()) textbox.Text = CurrentWebUser.Zipcode;
        }




        [VisualProperty("Default Value:", 5), DefaultValue(""), Localize]
        public override string Text
        {
            get
            {

                return textbox.Text;
            }
            set
            {
                if (CurrentWebUser != null && CurrentWebUser.Zipcode.IsNotEmpty()) textbox.Text = CurrentWebUser.Zipcode;
                else
		    textbox.Text =  value; ;
            }
        }


    }
}