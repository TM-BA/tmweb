﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Client.Data.Submit;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Linq;
using DateUtil = Sitecore.DateUtil;

namespace TM.Web.Custom.Forms
{
    public class EmailWarrantySaveAction : ISaveAction, ISubmit
    {
        private Item _currentDivision;

        public void Execute(ID formid, AdaptedResultList fields, params object[] data)
        {
            try
            {
                var emailMessage = new StringBuilder();
                string emailFromAddress = "warrantyrequest@taylormorrison.com";
                string emailToAddress = string.Empty;
                foreach (AdaptedControlResult field in fields)
                {
                    var fieldItem = new FieldItem(Context.Database.GetItem(field.FieldID));
                    var fieldType = ExtensionsToString.Between(fieldItem.LocalizedParameters, "<ContactType>",
                                                               "</ContactType>");
                    if (field.FieldName == "Community")
                    {
                        emailMessage.AppendLine("<b>" + "Community Name" + ": </b>" + field.Parameters);
                        emailMessage.AppendLine("<br/>");
                    }

                    if (fieldType != "Secondary" && field.FieldName == "Email Address")
                    {
                        emailFromAddress = field.Value;
                    }
                    if (fieldType != "Secondary" && field.FieldName == "Area of interest")
                    {
                        var split = field.Value.Split('#');
                        if (field.Value != null) emailToAddress = split[1];
                        {
                            emailMessage.AppendLine("<b>"+field.FieldName + ": </b>" + split[2]);
                            emailMessage.AppendLine("<br/>");
                            continue;
                        }
                    }
                    if (fieldType == "Secondary" && fieldItem.Title == "First Name")
                    {
                        emailMessage.AppendLine("<br/>");
                        emailMessage.AppendLine("<br/>");
                        emailMessage.AppendLine("<b>Secondary Contact Information:</b>");
                        emailMessage.AppendLine("<br/>");
                    }

                    if (!string.IsNullOrEmpty(field.Value) && !field.FieldName.Contains("Confirm"))
                    {
                        emailMessage.AppendLine("<b>" + field.FieldName + ": </b>" + field.Value);
                        emailMessage.AppendLine("<br/>");
                    }
                }

                if (emailToAddress.IsNotEmpty())
                    EmailHelper.SendEmail(emailFromAddress, emailToAddress, "Warranty Request", emailMessage.ToString(),
                                          true);

                HttpContext.Current.Items["WFFMPostedFormID"] = formid.ToString();
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error while submitting form: " + formid, e, this);
            }
        }


        public void Submit(ID formid, AdaptedResultList fields)
        {
            Execute(formid, fields, fields);
        }


        public Item CurrentDivision
        {
            get
            {
                return _currentDivision ??
                       (_currentDivision =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, Sitecore.Context.Item.ID,
                                                             Sitecore.Context.Database.GetItem(
                                                                 Sitecore.Context.Site.StartPath)));
            }
        }
    }
}
