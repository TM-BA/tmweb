﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Sitecore.Data.Validators;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
namespace TM.Web.Custom.Validators.Common
{
    [Serializable]
    public class AllowForCommunityOfType : StandardValidator
    {
        public AllowForCommunityOfType() { }
        public AllowForCommunityOfType(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public override string Name { get { return ("Require For Community Of Type"); } }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            //base.Text = 
            return (GetFailedResult(ValidatorResult.FatalError));
        }
        protected override ValidatorResult Evaluate()
        {
            bool require = Sitecore.MainUtil.GetBool(base.Parameters["require"], true);
            //Sitecore.Data.ID CommunityStatus = Sitecore.MainUtil.GetID(base.Parameters["CommunityStatusCode"], null);
            //Sitecore.Data.ID comStatusType = new Sitecore.Data.ID(currentItem[SCIDs.CommunityFields.CommunityStatusField]);
            Item currentItem = base.GetItem();
            if (base.Parameters["CommunityStatusCode"] == currentItem[SCIDs.CommunityFields.CommunityStatusField])
            {
                if (require)
                {
                    if (String.IsNullOrWhiteSpace(base.ControlValidationValue))
                    {
                        base.Text = "The field {0} is required for communities of this type.".FormatWith(base.GetFieldDisplayName());
                        return ValidatorResult.Error;
                    }
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(base.ControlValidationValue))
                    {
                        base.Text = "The field {0} is not allowed for communities of this type.".FormatWith(base.GetFieldDisplayName());
                        return ValidatorResult.Error;
                    }
                }
            }
            return ValidatorResult.Valid;
        }
    }
}