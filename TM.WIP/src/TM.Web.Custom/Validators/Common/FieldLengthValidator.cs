﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;
using Sitecore.Data.Validators;
using Sitecore.Data.Items;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Validators.Common
{
    [Serializable]
    public class FieldLengthValidator : StandardValidator 
    {
        public FieldLengthValidator() {}
        public FieldLengthValidator(SerializationInfo info,StreamingContext context) :base(info,context) {}
        public override string Name { get { return ("Field Length"); } }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return (GetFailedResult(ValidatorResult.FatalError));
        }
        protected override ValidatorResult Evaluate()
        {

            int max = Sitecore.MainUtil.GetInt(base.Parameters["max"], int.MaxValue);
            int min = Sitecore.MainUtil.GetInt(base.Parameters["min"], 0);
            if (max < min)
                
                return ValidatorResult.Valid;
            else
            {
                int fieldLength = base.ControlValidationValue.Length;
                if ((fieldLength > max) || (fieldLength < min))
                {
                    base.Text = "The field {0} has {1} characters. it needs to be between {2} and {3}.".FormatWith(base.GetFieldDisplayName(), fieldLength, min, max);
                    return ValidatorResult.FatalError;
                }
            }
            return ValidatorResult.Valid;
        }
    }
}