﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization; 
using Sitecore.Data.Validators;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Validators.Common
{
    [Serializable] 
    public class ImageDimensionsValidator : StandardValidator 
    {
        public ImageDimensionsValidator() {}
        public ImageDimensionsValidator(SerializationInfo info,StreamingContext context) :base(info,context) {}

        public override string Name { get { return ("Image Dimensions"); } } 
        
        protected override ValidatorResult GetMaxValidatorResult() 
        { 
            return(GetFailedResult(ValidatorResult.FatalError));
        }

        protected ValidatorResult CheckImage(Item image, int width, int height, string dimensions, DateTime minDate, bool allowScaling )
        {
            if (image != null && image.Paths.IsMediaItem 
                && (((MediaItem)image).FileBased ||
                ((MediaItem)image).HasMediaStream("blob")))
            {
                int imageWidth = Sitecore.MainUtil.GetInt(image["width"], 0);
                int imageHeight = Sitecore.MainUtil.GetInt(image["height"], 0);
                DateTime created = (image).Statistics.Updated;
                if (minDate < created)
                {
                    
                    if (((height > 0 && imageHeight != height) || (width > 0 && imageWidth != width))&&
                    (!(allowScaling && ((imageHeight / (double)imageWidth ) == (height/(double)width)) && (imageHeight > height))))
                    {
                        base.Text = base.GetText("The image '{3}' does not conform to the required dimensions. The existing image size is {0} x {1}. Please resize the image to {2} before referencing it in this field.", imageWidth.ToString(), imageHeight.ToString(), dimensions, image.Name);
                        return base.GetFailedResult(ValidatorResult.Error);
                    }
                }
            }
            return ValidatorResult.Valid;
        }

        protected override ValidatorResult Evaluate() 
        {
            int height = Sitecore.MainUtil.GetInt(base.Parameters["h"], 0);
            int width = Sitecore.MainUtil.GetInt(base.Parameters["w"], 0);
            int allowSave = Sitecore.MainUtil.GetInt(base.Parameters["allowSave"], 0);
            bool allowScaledLarger = Sitecore.MainUtil.GetBool(base.Parameters["allowScaledLarger"], false);
            string dateCheckStr = Sitecore.StringUtil.GetString(base.Parameters["effectiveDate"]);
            
            Sitecore.Data.ID comStatusType = Sitecore.Data.ID.Null;
            Sitecore.Data.ID CommunityStatus = Sitecore.MainUtil.GetID(base.Parameters["CommunityStatusCode"], Sitecore.Data.ID.Null);
            Item currentItem = base.GetItem();
            if (!string.IsNullOrWhiteSpace(currentItem["Community Status"]))
            {
                comStatusType = new Sitecore.Data.ID(currentItem["Community Status"]);
            }
            if (((CommunityStatus != Sitecore.Data.ID.Null) && (CommunityStatus == comStatusType)) || (CommunityStatus == Sitecore.Data.ID.Null))
            {
                DateTime dateCheck;

                if (!DateTime.TryParse(dateCheckStr, out dateCheck))
                {
                    dateCheck = DateTime.MinValue;
                }


                if (height <= 0 && width <= 0)
                {
                    return ValidatorResult.Valid;
                }
                string controlValidationValue = base.ControlValidationValue;
                if ((base.GetField().Type == "Visuallist" || base.GetField().Type == "CodedSourceVisuallist") && string.IsNullOrEmpty(controlValidationValue))
                    controlValidationValue = base.GetField().Database.GetItem(base.GetItem().ID)[base.GetField().Name];
                if (string.IsNullOrEmpty(controlValidationValue))
                {
                    return ValidatorResult.Valid;
                }

                ValidatorResult result = ValidatorResult.Valid;

                string dimensions = string.Format("{0} x {1}", (width > 0) ? width.ToString() : "variable", (height > 0) ? height.ToString() : "variable");

                if (base.GetField().Type == "Image")
                {
                    Item image;
                    string SCID = System.Text.RegularExpressions.Regex.Match(controlValidationValue, "mediaid=\"(?<SCID>[^\"]*)\"").Groups["SCID"].Value;
                    if (!string.IsNullOrEmpty(SCID))
                    {
                        image = base.GetField().Database.Items[Sitecore.Data.ID.Parse(SCID)];
                    }
                    else
                    {
                        string path = System.Text.RegularExpressions.Regex.Match(controlValidationValue, "mediapath=\"(?<URL>[^\"]*)\"").Groups["URL"].Value;
                        image = base.GetField().Database.Items[path];
                    }

                    result = CheckImage(image, width, height, dimensions, dateCheck, allowScaledLarger);
                }
                else if ((base.GetField().Type == "Visuallist")||(base.GetField().Type == "CodedSourceVisuallist"))
                {
                    string[] ids = controlValidationValue.Split(new char[] { '|' });
                    foreach (string id in ids)
                    {
                        Item image = base.GetField().Database.Items[Sitecore.Data.ID.Parse(id)];
                        result = CheckImage(image, width, height, dimensions, dateCheck, allowScaledLarger);
                        if (result != ValidatorResult.Valid) break;
                    }
                }

                if (result == ValidatorResult.FatalError && allowSave == 1)
                    return ValidatorResult.Error;
                else
                {
                    return result;
                }
            }
            return ValidatorResult.Valid;
        } 
    } 
}