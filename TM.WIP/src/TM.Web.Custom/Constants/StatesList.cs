﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TM.Domain.Entities;


namespace TM.Web.Custom.Constants
{
    public class StatesList
    {

        public static State[] USstates = new State[62] { new State("Alaska","AK"), new State("Alabama", "AL"), new State("Arkansas","AR"), new State("Arizona","AZ"),
                                                        new State("California","CA"),new State("Colorado","CO"), new State("Connecticut","CT"), new State("Delaware","DE"),
                                                        new State("Florida","FL"), new State("Georgia","GA"), new State("Hawaii","HI"), new State("Idaho","ID"), new State("Illinois","IL"),
                                                        new State("Indiana","IN"), new State("Iowa","IA"), new State("Kansas","KS"), new State("Kentucky","KY"),new State("Louisiana","LA"),
                                                        new State("Maine","ME"),new State("Maryland","MD"),new State("Massachusetts","MA"),new State("Michigan","MI"),new State("Minnesota","MN"),new State("Mississipi","MS"),new State("Missouri","MO"),
                                                        new State("Montana","MT"),new State("Nebraska","NE"),new State("Nevada","NV"),new State("New Hampshire","NH"),new State("New Jersey","NY"),new State("New Mexico","NM"),new State("New York","NY"),
                                                        new State("North Carolina","NC"),new State("North Dakota","ND"),new State("Ohio","OH"),new State("Oklahoma","OK"),new State("Oregon","OR"),new State("Pennsylvania","PA"),new State("Rhode Island","RI"),new State("South Carolina","SC"),new State("South Dakota","SD"),new State("Tennessee","TN"),
                                                        new State("Texas","TX"),new State("Utah","UT"),new State("Vermont","VT"),new State("Virginia","VA"),new State("Washington","WA"),new State("West Virginia","WV"),new State("Wisconsin","WI"),new State("Wyoming","WY"),
            new State("Alberta", "AB"),
            new State("British Columbia", "BC"),
            new State("Manitoba", "MB"),
            new State("New Brunswick", "NB"),
            new State("Newfoundland", "NF"),
            new State("Northwest Territories", "NT"),
            new State("Nova Scotia", "NS"),
            new State("Ontario", "ON"),
            new State("Prince Edward Island", "PE"),
            new State("Quebec", "QC"),
            new State("Saskatchewan", "SK"),
            new State("Yukon", "YT")
        };


        

        public static string[] Months = new string[12] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

    }
}