﻿using Sitecore.Data;

namespace TM.Web.Custom.Constants
{
    public class SCFieldNames
    {
        public const string LiveChatSkillCode = "Live Chat Skill";
        public const string InternetHomeConsultant = "IHC Card";

        //Home Page/Company Fields
        public const string BlogRssURL = "Blog Rss URL";

        public const string Canada = "Canada";
        public const string Condos = "Condos";
        public const string SingleFamily = "Single Family";

        public const string TaylorMorrison = "Taylor Morrison";
        public const string MonarchGroup = "Monarch Group";
        public const string DarlingHomes = "Darling Homes";

        public const string NewHomes = "new-homes";
        public const string NewCondos = "new-condos";

        public class SchoolDistrict
        {
            public const string Name = "School District";
            public const string SchoolDistrictID = "School District ID";
        }

        public class SalesTeam
        {
            public const string Name = "Name";
            public const string Phone = "Phone";
        }

        public class School
        {
            public const string Name = "School Name";
            public const string SourceID = "School Source ID";
            public const string Type = "School Type";
        }

        public class NavigationItemFields
        {
            public const string DisplayText = "Display Text";
            public const string Link = "Link";
        }

        public class CommunityFields
        {
            public const string StatusforAssistance = "Status for Assistance";
            public const string CommunityLegacyId = "Community Legacy ID";
            public const string SlideShowImage = "Slideshow Image";
            public const string CommunityStatus = "Community Status";
            public const string StatusforSearch = "Status for Search";
            public const string CommunityStatusChangeDate = "Community Status Change Date";
            public const string CommunityName = "Community Name";
            public const string IsGated = "Is Gated";
            public const string IsMasterPlanned = "Is Master Planned";
            public const string IsActiveAdult = "Is Active Adult";
            public const string IsCondoOnly = "Is Condo Only";
            public const string HasPlayground = "Has a Playground";
            public const string HasPool = "Has a Pool";
            public const string HasTrails = "Has Trails";
            public const string HasView = "Has a View";
            public const string HasPark = "Has a Park";
            public const string HasGreenbelt = "Has a Greenbelt";
            public const string HasLake = "Has a Lake";
            public const string HasVolleyball = "Has Volleyball";
            public const string HasPond = "Has a Pond";
            public const string HasBasketball = "Has Basketball";
            public const string HasBaseball = "Has Baseball";
            public const string HasClubhouse = "Has a Clubhouse";
            public const string HasTennis = "Has Tennis";
            public const string HasGolfCourse = "Has a Golf Course";
            public const string HasSoccer = "Has Soccer";
            public const string HasMarina = "Has a Marina";
            public const string HasBeach = "Has a Beach";
            public const string IsWaterfront = "Is Waterfront";
            public const string StreetAddress1 = "Street Address 1";
            public const string StreetAddress2 = "Street Address 2";
            public const string Zip = "Zip or Postal Code";
            public const string Longitude = "Community Longitude";
            public const string Latitude = "Community Latitude";
            public const string Phone = "Phone";
            public const string Fax = "Fax";
            public const string Email = "Responding Email Address";
            public const string UseOfficeHoursText = "Use Office Hours Text";
            public const string OfficeHoursText = "Office Hours Text";
            public const string Description = "Community Description";
            public const string SiteplanImage = "Site Plan Image";
            public const string SchoolDistrict = "School District";
            public const string ImageSource = "Image Source";
            public const string SlideshowImageRotationSpeed = "Slideshow Image Rotation Speed";
            public const string IsTownSend = "";
            public const string Directions = "Driving Directions Text";
            public const string SquareFeets = "SqFt Override Text";
            public const string Beds = "No of Beds Override Text";
            public const string Baths = "No of Baths Override Text";
            public const string HalfBaths = "No of Half Baths Override Text";
            public const string Stories = "No of Story Homes Override Text";
            public const string Garage = "No of Car Garage Override Text";
            public const string SundayOpen = "Sunday Open";
            public const string SundayClose = "Sunday Close";
            public const string MondayOpen = "Monday Open";
            public const string MondayClose = "Monday Close";
            public const string TuesdayOpen = "Tuesday Open";
            public const string TuesdayClose = "Tuesday Close";
            public const string WednesdayOpen = "Wednesday Open";
            public const string WednesdayClose = "Wednesday Close";
            public const string ThursdayOpen = "Thursday Open";
            public const string ThursdayClose = "Thursday Close";
            public const string FridayOpen = "Friday Open";
            public const string FridayClose = "Friday Close";
            public const string SaturdayOpen = "Saturday Open";
            public const string SaturdayClose = "Saturday Close";
            public const string SalesTeamCard = "Sales Team Card";
            public const string CommunityLogo = "Community Logo";
            public const string IHCCard = "IHC Card";
            public const string NSEProjectID = "NSE Project ID";
            public const string StatusChangeDate = "Community Status Change Date";
        }

        public class City
        {
            public const string Name = "City Name";
            public const string StatusForAssistance = "Status for Assistance";
            public const string StatusForSearch = "Status for Search";
        }

        public class Images
        {
            public static string Alt = "Alt";
            public static string Extension = "Extension";
        }

        public class DivisionFields
        {
            public static string LegacyDivisionID = "Legacy Division ID";
            public const string StatusForSearch = "Status for Search";
            public static string DivisionName = "Division Name";
            public static string IHCCard = "IHC Card";
            public static string HotSheetDisclaimer = "Hot Sheet Disclaimer";
            public static string NSEOunit = "NSE Ounit";
            public static string Disclaimer = "Disclaimer";

        }

        public class IHCCard
        {
            public static string FirstName = "First Name";
            public static string LastName = "Last Name";
            public static string EmailAddress = "Email Address";
            public static string Phone = "Phone";
            public static string Status = "Status";
        }

        public class Status
        {
            public static string Active = "Active";
            public static string Inactive = "Inactive";
        }

        public class HomePageFields
        {
            public static string TabsDisplayInSeconds = "Tab Display in seconds";
            public static string YoutubeUrl = "YouTube URL";
            public static string FacebookUrl = "Facebook URL";
            public static string TwitterUrl = "Twitter URL";
            public static string PinterestUrl = "Pinterest URL";
            public static string GooglePlusUrl = "GooglePlus URL";
            public static string BlogUrl = "Blog URL";
        }

        public class StateFields
        {
            public static string SalesForceState = "Salesforce State Value";
            public static string Code = "Code";
            public static string Name = "Name";

        }

        public class UserTexts
        {
            public static string NewAccount = "Welcome to ";
            public static string PasswordReset = "Reset your password.";

            public static string PasswordRecoveryEmail =
                "Please check your email and follow the instruction to reset your password.";

            public static string InvalidPasswordRecovery = "Password recovery URL might have expired or invalid.";
            public static string PasswordResetConfirmation = "Your password is successfully updated.";
            public static string UserNotExist = "User does not exist.";
            public static string UserNotExistBig = "We did not find a user with this provided email. Please try again.";
            public static string EnterValidData = "Please provide a valid information.";
            public static string Updated = "Your changes have been successfully updated.";
        }

        public class VirtualTour
        {
            public static string Url = "Virtual Tour URL";
            public static string InternalNote = "Internal Note";
        }

        public class MasterPlan
        {
            public static string Status = "Master Plan Status";
            public static string ProductType = "BHI Product Type";
            public static string ProductDisplayType = "BHI Product Display Type";
            public static string ImageSource = "Image Source";
            public static string PlanDepth = "Plan Depth";
            public static string PlanWidth = "Plan Width";
            public static string VirtualTourId = "Virtual Tours Master Plan ID";
        }

        public class PlanPageFields
        {
            public static string FloorPlan = "Floor Plan";
            public static string IsModelHome = "Is a Model Home";
            public static string MasterPlanID = "Master Plan ID";
            public static string PlanStatus = "Plan Status";
            public static string StatusForAdvertising = "Status for Advertising";
            public static string PricedFromValue = "Priced from Value";
            public static string PricedFromText = "Priced from Text";
            public static string VirtualTourStatus = "Virtual Tour Status";
        }

        public class HomeForSaleFields
        {
            public static string ElevationImages = "Elevation Images";
            public static string FloorPlanImages = "Floor Plan";
            public static string InteriorImages = "Interior";
            public static string HomeForSaleStatus = "Home for Sale Status";
            public static string HomeForSaleLegacyID = "Home for Sale Legacy ID";
            public static string DisplayAsAvailableHomeInRWE = "Display as Available Home in RWE";
            public static string StatusForSearch = "Status for Search";
            public static string StatusForAssistance = "Status for Assistance";
            public static string MasterPlanID = "Master Plan ID";
            public static string StatusForAvailability = "Status for Availability";
            public static string StreetAddress1 = "Street Address 1";
            public static string StreetAddress2 = "Street Address 2";
            public static string Price = "Price";
            public static string Sqft = "Square Footage";
            public static string IsModel = "Is a Model Home";
            public static string AvailabilityDate = "Availability Date";
            public static string LotNumber = "Lot Number";
            public static string Description = "Plan Description";
            public static string Ifp = "Ifp Url";
            public static string Beds = "Number of Bedrooms";
            public static string Bathrooms = "Number of Bathrooms";
            public static string HalfBathrooms = "Number of Half Bathrooms";
            public static string Stories = "Number of Stories";
            public static string Garages = "Number of Garages";
            public static string HasLivingRoom = "Has a Living Room";
            public static string HasDiningRoom = "Has a Dining Room";
            public static string HasSunRoom = "Has a Sun Room";
            public static string HasStudy = "Has a Study";
            public static string HasLoft = "Has a Loft";
            public static string HasOffice = "Has an Office";
            public static string HasGameRoom = "Has a Game Room";
            public static string HasMediaRoom = "Has a Media Room";
            public static string HasGuestBedroom = "Has a Guest Bedroom";
            public static string HasBonusRoom = "Has a Bonus Room";
            public static string HasBasement = "Has a Basement";
            public static string HasFireplace = "Has a Fireplace";
            public static string HasMasterBedroom = "Has a Master Bedroom";
            public static string HasFamilyRoom = "Has a Family Room";

        }

        public class PlanBaseFields
        {
            public static string ElevationImages = "Elevation Images";
            public static string FloorPlanImages = "Floor Plan";
            public static string InteriorImages = "Interior";
            public static string Bedrooms = "Number of Bedrooms";
            public static string Bathrooms = "Number of Bathrooms";
            public static string PlanLegacyID = "Plan Legacy ID";
            public static string PlanName = "Plan Name";
            public static string HasLivingRoom = "Has a Living Room";
            public static string HasDiningRoom = "Has a Dining Room";
            public static string HasSunRoom = "Has a Sun Room";
            public static string HasStudy = "Has a Study";
            public static string HasLoft = "Has a Loft";
            public static string HasOffice = "Has an Office";
            public static string HasMediaRoom = "Has a Media Room";
            public static string HasGuestBedroom = "Has a Guest Bedroom";
            public static string HasBonusRoom = "Has a Bonus Room";
            public static string HasBasement = "Has a Basement";
            public static string HasFireplace = "Has a Fireplace";
            public static string HasFamilyRoom = "Has a Family Room";
            public static string HasGameRoom = "Has a Game Room";
            public static string VirtualToursMasterPlanID = "Virtual Tours Master Plan ID";
            public static string StatusForSearch = "Status for Search";
            public static string StatusForAssistance = "Status for Assistance";
            public static string Stories = "Number of Stories";
            public static string HalfBathrooms = "Number of Half Bathrooms";
            public static string Garages = "Number of Garages";
            public static string Sqft = "Square Footage";
            public static string GarageEntry = "Garage Entry Location";
            public static string MasterBedLocation = "Master Bedroom Location";
            public static string DiningAreas = "Number of Dining Areas";
            public static string LivingAreas = "Number of Living Areas";
            public static string Description = "Plan Description";
            public static string PlanStatus = "Plan Status";
            public static string IFP = "IFP URL";
            public static string PricedFromValue = "Priced from Value";
            public static string HasMasterBedroom = "Has a Master Bedroom";



        }

        public class BlogEntry
        {
            public static string Title = "Title";
            public static string Introduction = "Introduction";
            public static string Content = "Content";
            public static string ThumbnailImage = "Thumbnail Image";
            public static string Author = "Author";
            public static string DateCreated = "DateCreated";
            public static string DateModified = "DateModified";
            public static string BlogRssURL = "Blog Rss URL";
            public static string Category = "Category";
            public static string Tags = "Tags";
            public static string BlogTitle = "Blog Title";
            public static string DisableComments = "Disable comments";
        }

        public class SFIntegration
        {
            public class PostURL
            {
                public const string URL = "SF Post URL";
            }

            public class Mapping
            {
                public const string FieldName = "SF Field Name";
                public const string FieldId = "SF Field ID";
                public const string WFFMFields = "WFFM Fields";
            }

            public class Oid
            {
                public const string FieldID = "Oid";
                public const string FieldName = "Field Name";
            }


            public class Debug
            {
                public const string Field = "Debug Field";
                public const string Status = "Active";
            }

            public class DebugEmail
            {
                public const string Field = "Debug Field";
                public const string Email = "Debug Email";
            }
        }
    }
}