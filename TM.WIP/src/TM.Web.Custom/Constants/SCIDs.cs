﻿using Sitecore.Data;

namespace TM.Web.Custom.Constants
{
    public class SCIDs
    {
        public class TemplateIds
        {
            public static ID ProductTypePage = new ID("{913373DC-F779-49D0-A163-3F17DB5F2B53}");
            public static ID StatePage = new ID("{2B7CD906-758A-4E29-BAAC-453349506CE1}");
            public static ID DivisionPage = new ID("{C70FF0BA-F242-45A3-AE34-1BD9B700B06E}");
            public static ID CityPage = new ID("{DE2DA9D0-C765-46B9-820D-28325FA1B227}");
            public static ID School = new ID("{1B4197AA-0BBA-481B-9413-C49B5031D684}");
            public static ID SchoolList = new ID("{F8D946B0-8C33-482F-AB3D-E239934152FA}");
            public static ID PromotionWithImage = new ID("{6F79C430-08AA-4F08-937C-190611BC24CA}");
            public static ID CommunityPage = new ID("{553E95A0-D2F3-4409-BF50-7A2C5A494D7C}");
            public static ID PlanPage = new ID("{8722CBBA-4A10-41FB-B6DD-6E56051EF666}");
            public static ID HomeForSalePage = new ID("{7E05E34B-1262-4059-877D-335F1F7246FC}");
            public static ID PPCLandingPage = new ID("{F92A42F0-BE2A-44A7-83CA-F5AE810B2A36}");
            public static ID HomeFeatureItem = new ID("{E1131B83-D3CD-4BD4-96F0-AD0E1A4F5BA4}");
            public static ID HomeFeaturesContainer = new ID("{CDF1D3B5-8AB5-43B8-9C32-37396769B359}");
            public static ID LocalInterestItem = new ID("{987D9883-EE0C-4F94-91C1-D46F6470C958}");
            public static ID LocalInterestsContainer = new ID("{0A2B7CB7-71FD-4A0E-8C50-C260E00FB72E}");
            public static ID SystemNode = new ID("{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B}");
            public static ID HomeTemplateId = new ID("{CFF47DD0-6A86-4F25-887D-8F959278A2E0}");
            public static ID SchoolDistricts = new ID("{343C4D6B-7B0A-4D82-953B-98EF72078820}");
            public static ID SchoolDistrict = new ID("{B894D2D3-20DF-40BA-99A3-C3D7726110DF}");
            public static ID DesignStudio = new ID("{2C967DBA-1E5B-44E1-8586-834116217760}");
            public static ID SalesTeamCards = new ID("{A06E44C4-3BEA-4BD4-9964-042764F57FF4}");
            public static ID CommunityLink = new ID("{0D8F018F-125B-461B-A713-4C1A743B9E59}");
            public static ID PromoFolder = new ID("{D6700838-CDE5-4575-B4E8-0DA154D8A3E4}");
            public static ID ContactUsPage = new ID("{7AA21CC5-FD5A-4ED7-BC4D-32A7BCD15FF1}");
            public static ID UniversalRFIPage = new ID("{189E68E5-80DF-4E0C-A4D3-7863B947C193}");
            public static ID UniversalThankyou = new ID("{9FCFE0F4-1EF0-4D9D-A207-D947FFBB2B12}");
            public static ID SubCommunities = new ID("{8B668B17-F0F9-4FBD-BF7B-F983ABA071FD}");

            public static ID PlanTemplateStandardValue = new ID("{CAADF677-2151-435B-8358-D93144A19318}");
            public static ID HomeForSaleTemplateStandardValue = new ID("{08109AE1-FCAB-4EF5-A4CD-806BE8EA7649}");
            public static ID RealtorSearch = new ID("{32C985BE-E459-4347-B597-30C041DFADA4}");
            public static ID RealtorSearchResult = new ID("{7B790B14-AB5D-4EB6-AABD-15F810DD07B7}");
            public static ID StandardContent = new ID("{6C11EB29-2EFF-4634-BB99-57DFE5308BDE}");
            public static ID ContentFolders = new ID("{47D8A380-2D03-499B-A5DA-7464824DFEB3}");
            public static ID Folders = new ID("{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}");
            public static ID PromoBase = new ID("{2DBCB58D-E214-4242-8EB1-25D96F95E075}");
            public static ID SchoolsFolder = new ID("{F8D946B0-8C33-482F-AB3D-E239934152FA}");

            public static ID Account = new ID("{CBD1DE28-512F-4E39-A296-0B2015DCCB3C}");
            public static ID SalesTeamCard = new ID("{6BE8A25B-74FD-4672-BCE9-B5F7E14F074C}");

            public static ID CustomMediaFolder = new ID("{F7CA0131-3C01-4CA1-BC9D-7F0902954305}");
            public static ID MasterPlanFolder = new ID("{585045FF-EBE1-45C8-8C1D-6BE8DCCFF8AB}");
            public static ID MasterPlan = new ID("{11E21296-E52A-40BC-8CA5-2F00570E1800}");
            public static ID ElevationImage = new ID("{A42D1722-5EEA-4910-8A64-4FD0EE08928F}");
            public static ID FloorPlanImage = new ID("{21B8B05B-5F4F-4852-A6BE-06DD1DEDABFF}");
            public static ID InteriorImage = new ID("{0A592F4F-2593-497A-B0D3-533BE74B2809}");
            public static ID VirtualToursFolder = new ID("{99D49047-2212-491C-A255-CD47553CA2FC}");
            public static ID VirtualTour = new ID("{25E8955B-5664-4B21-B9D3-28E248FB09A5}");
            public static ID System = new ID("{13D6D6C6-C50B-4BBD-B331-2B04F1A58F21}");
            public static ID Templates = new ID("{3C1715FE-6A13-4FCF-845F-DE308BA9741D}");
            public static ID Form = new ID("{FFB1DA32-2764-47DB-83B0-95B843546A7E}");
            public static ID FIeld = new ID("{C9E1BF85-800A-4247-A3A3-C3F5DFBFD6AA}");

            //Const String Values for use in switch statements
            public const string SwitchCompanyPage = "{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B}";
            public const string SwitchProductPage = "{913373DC-F779-49D0-A163-3F17DB5F2B53}";
            public const string SwitchHomePage = "{CFF47DD0-6A86-4F25-887D-8F959278A2E0}";
            public const string SwitchStatePage = "{2B7CD906-758A-4E29-BAAC-453349506CE1}";
            public const string SwitchDivisionPage = "{C70FF0BA-F242-45A3-AE34-1BD9B700B06E}";
            public const string SwitchCityPage = "{DE2DA9D0-C765-46B9-820D-28325FA1B227}";
            public const string SwitchCommunityPage = "{553E95A0-D2F3-4409-BF50-7A2C5A494D7C}";
            public const string SwitchPlanPage = "{8722CBBA-4A10-41FB-B6DD-6E56051EF666}";
            public const string SwitchInventoryPage = "{7E05E34B-1262-4059-877D-335F1F7246FC}";
            public const string SwitchSubCommunity = "{83A575CD-4937-4915-9376-17C99E96C0F0}";
            public const string SwitchIHCPage = "{7E05E34B-1262-4059-877D-335F1F7246FC}";
            public const string SwitchGenericCampaignPage = "{69D60A3B-6282-43FD-A496-078CA5BF4536}";
            public const string SwitchSpecificCampaignPage = "{9FA2DC72-51FE-4DFA-8D1F-A0B1331ED747}";
            public const string SwitchSchoolPage = "{1B4197AA-0BBA-481B-9413-C49B5031D684}";
            public const string SwitchLookupItem = "{A9E74B92-8790-4AD7-8114-BD8F4CB83991}";
            public static ID WhyCompanyFolder = new ID("{195CCBA8-C4D3-4B7F-AF32-D5A35088C2F7}");
            public const string MicrositeTemplate = "{AA82B5C1-C2F3-4DC7-8F48-B608B8E33A46}";

            public const string ProductLinks = "{C09036AB-2FE3-4A7E-9666-A13C500F3463}";
            public const string CommunityLinks = "{AA46C679-3599-4DD0-8C9C-D95F2B51E3D0}";
            public const string SplashPage = "{B1C0AEF0-15DE-4A87-A431-BDD4887C0D5C}";

            public static ID MappingItem = new ID("{0C303AB8-9F55-4ED6-8D59-B88901565880}");
            public static ID WFFMForm = new ID("{FFB1DA32-2764-47DB-83B0-95B843546A7E}");

        }

        public class CompanyIds
        {
            public static ID DarlingHomes = new ID("{98EF538F-1FAF-4A50-A0EE-06EBE380A103}");
            public static ID MonarchGroup = new ID("{6F977C23-FEC4-4479-8931-EABD5840533F}");
            public static ID TaylorMorrison = new ID("{78BC1EFB-24C1-4228-B2E8-3209649AB529}");

            public static ID DarlingHomesHomeItem = new ID("{BCFFBCF8-6DB9-4DD7-BE6A-B65F12F9FD6B}");
            public static ID MonarchGroupHomeItem = new ID("{F902235D-E308-4FFF-AFA9-1C064CA01917}");
            public static ID TaylorMorrisonHomeItem = new ID("{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}");
        }

        public class StatusIds
        {
            public class InventoryHomeAvailabilityStatus
            {
                public const string ReadyNow = "{161A4D9D-95D5-4682-B863-E3E4632E6BAF}";
            }

            public class PlanAdvertisingStatus
            {
                public const string Available = "{427CCB71-2177-4F52-92A2-55F0A36CCCBD}";
                public const string SeeHomesInProgress = "{15B6BDC8-0962-4DF5-9BE3-3E96CD8454F2}";
                public const string SoldOut = "{5D3791EB-2E1B-41EA-8304-A7797EE8D854}";
                public const string TemporarilySoldOut = "{2559AB7B-AA49-410A-B25F-0975C3A6AF8D}";
                public const string ToBeReleased = "{DFD6E923-FACA-4884-9C60-A4AB06D0B1EA}";
            }

            public class MasterBedroomLocation
            {
                public const string Upstairs = "{91B6FB39-31B4-402B-87C0-543B71857789}";
                public const string Downstairs = "{BA51D5E7-0AFE-4B98-97A9-609812056923}";
            }

            public class GarageEntryType
            {
                public const string Front = "{032B2F31-88ED-4ED4-9D73-4897FC9A6460}";
                public const string Rear = "{A7D4347F-23F8-4A98-B884-7374B2AD71FE}";
                public const string Side = "{2F9B3FAD-FF69-4E08-94E9-A9C56D39C4FE}";
            }

            public class BHIProductType
            {
                public const string SingleFamily = "{401D5C49-91B2-4B24-84A4-3E08D3843C51}";
                public const string MultiFamily = "{875CA111-9BBE-4920-AACF-348F5030F779}";
            }

            public class BHIProductDisplayType
            {
                public const string Duplex = "{560F5890-CFEC-4CDF-B584-0323F0C775E4}";
                public const string Multiplex = "{9BC44C36-ABEF-49E9-B4B6-7D454FBB757F}";
                public const string CarriageHouse = "{E791BCA7-425A-4761-8B11-0622AA8CBCF2}";
                public const string Condominium = "{DB20378C-37EF-4676-B420-C84BD2690EF9}";
                public const string GardenHome = "{4E17ED65-C98C-4A10-BBE6-312822B39F89}";
                public const string SingleFamily = "{FA61BA32-76E8-4741-AB46-050BB210955B}";
                public const string TownHome = "{74751415-D55F-4E7F-AA93-40CB0DFF8E97}";
                public const string TownHouse = "{64A7606E-CFBE-4166-9306-F2892CDBAD8F}";
            }

            public class Status
            {
                public const string Active = "{5B3598C0-B87C-41E5-87A8-388CAA114715}";
                public const string InActive = "{F790311A-F688-47F1-8E80-F51BBF14B6D3}";
            }


            public class CommunityStatus
            {
                public static ID Closed = new ID("{81F99D66-94CF-48C5-B1D5-EE09998A4366}");
                public static ID Closeout = new ID("{93F8DE6F-8F1A-41CF-BA8D-8DB615617EC3}");
                public static ID ComingSoon = new ID("{0D1066E2-08E6-4C08-A5D2-91470A901A10}");
                public static ID GrandOpening = new ID("{BE62866E-8524-4352-9F37-6B98D7CCFE63}");
                public static ID Open = new ID("{F1B5AF6A-35A0-4433-A275-E111066A7B20}");
                public static ID InActive = new ID("{573E0723-1F83-4F2C-9DB8-F5AF87BC5330}");
                public static ID PreSelling = new ID("{38074C3C-1B98-4A8C-9551-76955C255687}");

                public class Fields
                {
                    public static ID Code = new ID("{A32B9B91-92CE-4922-9B37-3B1AFED4A30C}");
                    public static ID Name = new ID("{32EEC75D-C70F-47EE-A6A8-6EEAD7845E7A}");
                }
            }
        }

        public class PrintStudio
        {
            public static ID Folder = new ID("{F1B4E272-43D4-4564-853B-699D77D75419}");
            public static ID PageTemplate = new ID("{6BFA47BA-F73C-48DB-9170-C0CC94179EC7}");
            public static ID HSH = new ID("{AB6DA6B1-A832-49D2-A0DC-A1A01494B42B}");
            public static ID Document = new ID("{B2C7D259-B0EE-4E9A-B344-30E789ADEFA6}");
        }

        public class SchoolDistrict
        {
            public static ID SchoolDistrictName = new ID("{FADAF80F-EF3D-4280-BF0D-6FFE155FD2E0}");
            public static ID SchoolDisrictID = new ID("{AF7B6C9B-D49D-4C4E-B356-B573CB45BDCF}");
        }

        public class ProductTypeIds
        {
            public static ID NewCondosID = new ID("{E53458A5-7408-4B10-96C6-D69CB339478D}");
            public static ID NewHomesID = new ID("{7B112A0C-98C5-47C4-80D8-F61C2C1B5EAE}");
        }

        public class Common
        {
            public class DataFields
            {
                public static ID BathroomCounts = new ID("{4EAC2122-C009-493F-8BD3-B231704617BF}");
                public static ID HalfBathroomCounts = new ID("{4998092C-BACB-4C47-9C13-0F7DE07789B8}");
                public static ID BedroomCounts = new ID("{9C4BF70D-5276-44CE-824D-76E1E61459D3}");
                public static ID CommunityStatuses = new ID("{22C1C751-FC5F-4E1E-8D1C-37106C852100}");
                public static ID RecordStatus = new ID("{410B786A-3527-482E-AFB4-C3901EB83234}");
                public static ID AdvertisingStatus = new ID("{6CFF2982-0DEB-429A-A460-729FFD955565}");
                public static ID HomeAvailabilityStatus = new ID("{8B8AFBAD-06AC-46DF-AF9D-7AD059E9C21A}");

            }

            public class SearchModes
            {
                public static ID HomeforSale = new ID("{FB63B97E-5387-4449-A513-0838A1E14186}");
                public static ID Multi = new ID("{ED833CB1-0D59-4020-A66E-416B0D32AF37}");
            }
        }

        public class CommunityFields
        {
            public class CommunityStatuses
            {
                public const string Closed = "{81F99D66-94CF-48C5-B1D5-EE09998A4366}";
                public const string Closeout = "{93F8DE6F-8F1A-41CF-BA8D-8DB615617EC3}";
                public const string ComingSoon = "{0D1066E2-08E6-4C08-A5D2-91470A901A10}";
                public const string GrandOpening = "{BE62866E-8524-4352-9F37-6B98D7CCFE63}";
                public const string Open = "{F1B5AF6A-35A0-4433-A275-E111066A7B20}";
                public const string InActive = "{573E0723-1F83-4F2C-9DB8-F5AF87BC5330}";
                public const string PreSelling = "{38074C3C-1B98-4A8C-9551-76955C255687}";
            }

            public static ID CommunityTemplateID = new ID("{553E95A0-D2F3-4409-BF50-7A2C5A494D7C}");
            public static ID CommunityStatusField = new ID("{68B4B141-4361-4238-9B5B-148CD10EA836}");
            public static ID CommunityStatusChangeDateField = new ID("{3F5E90FB-8E92-4993-9919-2F7CAD8FAAB1}");
            public static ID SubCommunityTemplateID = new ID("{83A575CD-4937-4915-9376-17C99E96C0F0}");
            public static ID Lattitude = new ID("{E93CDA2F-225C-4772-A861-49CC5D19BE1B}");
            public static ID Longitude = new ID("{F9EF22EC-E06A-423D-82D9-FEA6250F1B32}");
            public static ID SitePlanImageID = new ID("{01FB90B7-116E-4000-AA05-A2477A15C75C}");
            public static ID CommunityLogoID = new ID("{15F0C785-FE1E-4C7A-B55F-E3C41C010BBB}");
            public static ID AlternateImageID = new ID("{D38D09EC-2BBA-400E-AB04-005DA066D7F3}");
            public static ID SlideShowImages = new ID("{F0BAD41A-AFD3-4D19-9840-752F9A782F0C}");
            public static ID CommunityName = new ID("{19A3D680-967E-4AA4-B5AF-16BB85A0BFF8}");
            public static ID StreetAddress1 = new ID("{8ABE4570-3889-421F-B058-E822F124A9EC}");
            public static ID StreetAddress2 = new ID("{4CC8A387-3A48-4B12-9A5E-0731F56F4272}");
            public static ID ZipOrPostalCode = new ID("{2B751C9F-97A8-4600-9183-518030FCF62D}");
            public static ID CommunityDescription = new ID("{A5ED4D7D-091E-497E-83DD-E8A446D2576D}");
            public static ID LeadEmailInAdditionToIHC = new ID("{AA495262-97CF-4120-B6E2-2217E6D7F169}");
            public static ID CommunityLegacyID = new ID("{7F7CBA09-1FCD-40F9-8E34-AE64CE547C32}");
        }

        public class Global
        {
            public static ID NowTrendingText = new ID("{E8DEA739-F13E-4C0F-B683-341E5B376082}");
            public static ID CompanyLogo = new ID("{6DCD57EE-E230-490D-AEC0-6544CDACCD32}");
            public static ID LeftPromoAreaTitle = new ID("{6798C2D3-1107-4A1C-9F39-AFD280CEC589}");
            public static ID LeftPromoAreaContent = new ID("{3A674F93-BB76-4FBC-8196-F3EF270870AF}");
        }

        public class HomePage
        {
            public static ID MainNav = new ID("{E730DE78-7037-4B0D-BD51-26E1F700A81F}");
            public static ID PressRoomURL = new ID("{151A25D8-A0CF-4FE7-9B6D-3A9A57E98030}");
            public static ID CareersLinkURL = new ID("{AA723282-0FC1-45F5-BBCE-9E32C0FBADC9}");
            public static ID SelectCampaigns = new ID("{E94B9396-8F5B-4FCE-B18B-8DEC1EF35439}");
            public static ID TM_FinanceYourHome = new ID("{4C42D7F3-8B9C-4D8C-B54B-D5FC58702446}");
            public static ID DH_FinanceYourHome = new ID("{0FD278A1-8A25-410B-9772-ADD8B0D6331C}");
            public static ID TM_FindYourNewHome = new ID("{9CA4C886-EC9A-4D44-8E3A-8EED35E4AFA5}");
            public static ID CompanyWebSiteUrl = new ID("{97C20545-7CF2-4853-8AA1-4E9D579F23C4}");
            public static ID WorkflowID = new ID("{0AB18278-C8F1-4F46-8D0B-E98463634095}");
        }

        public class SmallStageCampaign
        {
            public static ID TabSubtitle = new ID("{360A1216-D7CB-4568-BEF7-BD3E2EC32F59}");
            public static ID TabTitle = new ID("{28DEAAB4-D886-45B7-88AC-38F96CF86A89}");
            public static ID LargeStageImage = new ID("{FC01AB26-0C2C-4613-A96D-E19EBEB3A542}");
            public static ID SmallTabImage = new ID("{00DC554C-710B-4217-9D40-A3C750641BAE}");
            public static ID TargetURL = new ID("{BA239624-DF37-4CD0-9E01-440BF64EC326}");
        }

        public class PlanFields
        {
            public static ID PricedFromValue = new ID("{655C6D93-AA58-4B55-BE17-2FDD82C6226F}");
            public static ID PricedFromText = new ID("{2EECD67E-895E-4482-BDE1-159FD9A8CBFA}");
            public static ID SquareFootage = new ID("{4CDA4A5E-9B60-441B-B12E-A222733A0B38}");
            public static ID MasterPlanID = new ID("{5BCDCFDF-BF2A-4A83-8299-6BA4DEEFB9F0}");
            public static ID ElevationImages = new ID("{55A14BEF-969E-4B00-8A03-37E67F069E3D}");
            public static ID PlanName = new ID("{83D8146D-EFEB-46B6-BDBA-C6143AAF401B}");
            public static ID PlanDescription = new ID("{531E4F58-67EF-4116-A47C-FCB01816B0EA}");
            public static ID SubCommunityID = new ID("{455A3E98-A627-4B40-8035-E683A0331AC7}");
            public static ID PlanStatus = new ID("{BF332405-E397-4F22-9543-BB4BE1D1FB0C}");
        }

        public class HomesForSalesFields
        {

            public static ID Price = new ID("{CB15C16C-F133-4661-AF37-0837F427B4DF}");
            public static ID StreetAddress1 = new ID("{93CFD58A-4DF8-4FE2-AE17-7A8AEACC7248}");
            public static ID StreetAddress2 = new ID("{79CB1085-92CA-4222-B6B7-F2EE3876669E}");
            public static ID AvaialabilityDate = new ID("{E98A8262-FB5F-49CB-AD4E-FE2AA70611D5}");
            public static ID PlanDescription = new ID("{C88A47EF-F8F6-4C55-A8D4-DCAF0478A05E}");
            //todo: this could go to planbase template
            public static ID SquareFootage = new ID("{741B547D-C914-4956-8BF9-005217FB1A2D}");

            public static ID ElevationImages = new ID("{5418AB5B-68EB-43C1-8A9A-D702DDFFCB43}");
            public static ID StatusForAvailability = new ID("{55C17A68-4A5E-4AE3-91A7-56BAE63D6579}");
            public static ID HomesForSaleSatus = new ID("{651B4A85-2294-40FB-A1F0-384DB0FB4A6D}");
            public static ID RealtorIncentiveAmount = new ID("{D70159AC-1741-4D44-9800-42240D930782}");
            public static ID RealtorBrokerCommissionRate = new ID("{B6F1BC4C-A0A1-463F-BD2C-CA34161A84E1}");
            public static ID RealtorBrokerCommissionRateOverride = new ID("{0ED01A9B-D660-47B4-8111-8FDB03E5AF59}");

            public static ID MLS = new ID("{09317504-B070-4B22-9B05-5EF777C97F9D}");
            public static ID LotNumber = new ID("{0CEE027D-7999-4CEB-9F41-DF0DD9C5E1A8}");

            public static ID FeaturedHome = new ID("{43249935-CFC3-4DBD-A292-04F06B0E75E8}");
            public static ID AvailableHome = new ID("{B5B5DAFC-EC12-4B16-BC81-7059EE2DB3D6}");

            public static ID ReadyNow = new ID("{161A4D9D-95D5-4682-B863-E3E4632E6BAF}");


        }

        public class PlanBaseFields
        {
            public static ID NumberOfStories = new ID("{ADD15C2B-F7DF-4FE7-AF3A-078D70787FD9}");
            public static ID NumberOfBedrooms = new ID("{FE5ABCE3-78BB-40EB-98BE-97F401519922}");
            public static ID NumberOfBathrooms = new ID("{E141D5EB-2EF0-45FB-874D-91E1A886D2F9}");
            public static ID NumderOfGarages = new ID("{24CFB438-D9C5-4863-97A0-8A17F6C5C4BE}");
            public static ID NumberOfHalfBathrooms = new ID("{49C7CF48-C394-4E92-88F6-10A4883AC609}");
            public static ID SquareFootage = new ID("{4CDA4A5E-9B60-441B-B12E-A222733A0B38}");
            public static ID PlanDescription = new ID("{531E4F58-67EF-4116-A47C-FCB01816B0EA}");
            public static ID ElevationImages = new ID("{55A14BEF-969E-4B00-8A03-37E67F069E3D}");
            public static ID Interior = new ID("{81BB094D-8C55-4723-A602-C7E7F1E1C5DF}");
            public static ID FloorPlan = new ID("{ED4733D5-EFD7-4C18-A8C3-D418E85910A5}");
            public static ID MasterPlanID = new ID("{5BCDCFDF-BF2A-4A83-8299-6BA4DEEFB9F0}");
            public static ID VirtualToursMasterPlanID = new ID("{2E565CB5-378D-434D-BE51-AB94748E72EE}");
            public static ID PlanBrochureMasterID = new ID("{52090ED2-0A28-4498-9580-F2E0D6A9ED72}");
            public static ID VirtualTourStatus = new ID("{D4047B64-6ED0-4511-86E2-066AFFF167F7}");
            public static ID PlanBrochureStatus = new ID("{FDF4B419-721C-4524-9224-3540000FB67B}");
            public static ID StatusForSearch = new ID("{112B8C71-E575-4496-A0EE-E1074882705B}");
            public static ID FlashIFPAvailable = new ID("{4432784A-F704-4AF2-847E-56677259A8EA}");
            public static ID GarageEntryLocation = new ID("{03EF2C53-7631-46CF-B666-467627FDEA44}");
            public static ID NumberOfDens = new ID("{811228B3-BD23-48E4-9739-C706A3607A47}");
            public static ID NumberOfSolariums = new ID("{5FD4B618-CBD4-4F0A-A1B3-307BFF06A3EC}");
            public static ID NumberOfBonusRooms = new ID("{92A23EE5-D29F-4047-B588-0DFCFDAF3BD4}");
            public static ID HasaFamilyRoom = new ID("{D4DA35E5-EAAF-4279-92AD-925D9210D7BA}");
            public static ID NumberOfFamilyRooms = new ID("{3E5EBDE3-D9A8-4B04-854E-B3BFC581BE35}");
            public static ID NumberOfLivingAreas = new ID("{A495BC45-C238-49B9-895D-3741C4880559}");
            public static ID NumberOfDiningAreas = new ID("{2A77522C-41D5-4144-A3E9-FEB89C28A603}");
            public static ID HasaLivingRoom = new ID("{F6C50A71-5928-4091-AC6E-7B21259A3965}");
            public static ID HasaDiningRoom = new ID("{2A07EE2D-7BCF-4198-B075-E198E20FEF93}");
            public static ID HasaSunRoom = new ID("{02541046-7551-46B5-B50C-3845D1DD4A75}");
            public static ID HasaStudy = new ID("{2DCA9C5E-0700-4A34-81FC-13BE8F454FFE}");
            public static ID HasaLoft = new ID("{55F886EC-C44B-4B0C-A3EE-BA96B4BCB608}");
            public static ID HasanOffice = new ID("{BFB61A1F-6350-41EB-8319-CB0A76EE31FC}");
            public static ID HasaGameRoom = new ID("{D06814B1-ECB8-4463-BBD1-E80AF705030C}");
            public static ID HasaMediaRoom = new ID("{0EF3157D-DECD-4BDC-81FE-E794E7D7BEFB}");
            public static ID HasaGuestBedroom = new ID("{E857B938-D3B0-4678-AEC8-FC641A658B89}");
            public static ID HasaBonusRoom = new ID("{A2161332-BA6C-424F-B6B4-209E062B030C}");
            public static ID HasaBasement = new ID("{8DEF55B1-5BC9-40B5-9DBB-D5BEAE5191BF}");
            public static ID HasaFireplace = new ID("{BB465FE8-E2AF-44BA-8214-3E0A25EF5244}");
            public static ID HasaMasterBedroom = new ID("{514DDF10-2D3C-40F5-BD5D-2AEB07635144}");
            public static ID MasterBedroomLocation = new ID("{8BD9B993-CCCC-4056-9414-183AB3B1227F}");
            public static ID IFPUrl = new ID("{77CF2BAC-4483-41A3-82B4-CB87069F105F}");
        }

        public class Homebuying
        {
            public static ID StepSelection = new ID("{856DAFEF-B6AE-4EDC-A867-C12703EA7755}");
        }

        public class AvailablePositions
        {
            public static ID PositionDetails = new ID("{0032AA1F-974F-4E10-95D0-88B7A79C0490}");
        }

        public class Home
        {
            public static ID TM_Home = new ID("{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}");
            public static ID MonarchHome = new ID("{F902235D-E308-4FFF-AFA9-1C064CA01917}");
            public static ID DarlingHome = new ID("{BCFFBCF8-6DB9-4DD7-BE6A-B65F12F9FD6B}");
        }

        public class GlobalSharedFieldValues
        {

            public static ID ShortAnswersList = new ID("{D9399EFF-0874-4922-BFBF-AA8C07034A23}");
            public static ID States = new ID("{F3C388B2-F930-4024-8C8C-778D83E1AB70}");

            public class RecordStatus
            {
                public static ID Active = new ID("{5B3598C0-B87C-41E5-87A8-388CAA114715}");
            }

            public class HomebuyingStepType
            {
                public static ID HB101 = new ID("{F3E7C0FD-0502-4E70-A6E8-401C2D47738E}");
                public static ID HB201 = new ID("{9A907A11-833A-4F83-A293-B8B763FB50C0}");

            }

        }

        public class SharedFieldValues
        {
            public static ID ShareFieldValues = new ID("{980D91F6-2AE3-4A1C-A9FD-13788EC000F5}");
            public static ID HowDidYouHearList = new ID("{175D74ED-1C0E-4FEE-BAB9-C502DDC75E99}");
            public static ID MoveInTimeFrameList = new ID("{54580077-D172-4024-953A-0704A134F95C}");
            public static ID ShortAnswerList = new ID("{D9399EFF-0874-4922-BFBF-AA8C07034A23}");
            public static ID SupportOptions = new ID("{FB42182F-C549-4B5F-9013-48EC4EE87DCC}");

        }

        public class SupportOptions
        {
            public static ID GeneralInformation = new ID("{1ED28216-59C6-4E9D-8F19-149966EEA67A}");
            public static ID LookingForHome = new ID("{B3F5A0E5-FDC1-40FC-A383-C60E4FB9FA5C}");
            public static ID InterestedFinancingOptions = new ID("{54602B76-C14F-41F5-A299-8D706E7B3230}");
            public static ID WarrantyRequest = new ID("{92930D9C-8DFE-4E26-979C-6FD3688148E0}");
            public static ID Default = new ID("{6A274831-8B67-47F0-81E4-E1C2054A74B4}");

        }

        public class ContentItems
        {

            public static ID WhyTaylorMorrison = new ID("{92AD83A0-34E2-4B4E-8F4C-54C4369FBCA6}");

        }

        public class DivisionFieldIDs
        {
            public static ID LegacyDivisionID = new ID("{32FB9529-23B1-4FB8-893E-30F444BA1A18}");
            public static ID Divisionname = new ID("{470D848B-6386-42AC-AFC6-B2A3D6F8FA15}");
        }

        public class SubCommunityFields
        {
            public static ID SubCommunityName = new ID("{83A575CD-4937-4915-9376-17C99E96C0F0}");
        }

        public class CompanyFields
        {
            public static ID MyLogo = new ID("{176779E3-72F7-41A3-BD34-C0E17466B79D}");
            public static ID LegacyCompanyID = new ID("{6E0923E1-EA26-4E18-B860-DC69A96E2A2F}");
        }

        public class Promos
        {
            public static ID Promotions = new ID("{6F79C430-08AA-4F08-937C-190611BC24CA}");
            public static ID GenericCampaign = new ID("{69D60A3B-6282-43FD-A496-078CA5BF4536}");
            public static ID SpecificCampaign = new ID("{9FA2DC72-51FE-4DFA-8D1F-A0B1331ED747}");

        }

        public class Blog
        {
            public class TM
            {
                public static ID Blog = new ID("{46663E05-A6B8-422A-8E13-36CD2B041278}");
                public static ID Catergories = new ID("{61FF8D49-90D7-4E59-878D-DF6E03400D3B}");
                public static ID Entry = new ID("{5FA92FF4-4AC2-48E2-92EB-E1E4914677B0}");
                public static ID Comments = new ID("{70949D4E-35D8-4581-A7A2-52928AA119D5}");
            }
        }

        public class WebForms
        {
            public static ID JoinOurInterestList = new ID("{E059951C-1C5A-464E-A876-4C06FFD10F26}");
            public static ID GeneralInformationRequest = new ID("{669D0BB1-8491-421B-A7FE-D4749F2FAA53}");
            public static ID UniversalRequestForm = new ID("{1914091C-7D26-4281-9233-8775A6A71F5B}");

        }

        public class HomeBuying101
        {
            public static ID RealStateGlossary = new ID("{25834EE0-D51A-4B98-8F1A-8DF4D3677752}");
            public static ID StepDetail = new ID("{72272A99-7C62-4BBB-BFF3-D3842A4E8169}");
            public static ID Tweeter = new ID("{450AD53A-AF7E-44A5-8F20-4612A237EA3F}");

        }

        public class ContactUsForms
        {
            public static ID ThankYou = new ID("{9FCFE0F4-1EF0-4D9D-A207-D947FFBB2B12}");

        }

        public class VirtualTours
        {
            public static ID VirtualTourUrl = new ID("{C4F15803-7818-4A56-811C-28713B5A3B1B}");
        }

        public class LookupItem
        {
            public static ID Name = new ID("{126B89AC-85F4-4223-BADC-88037D78A799}");
            public static ID Code = new ID("{3C3DA81A-08AE-48DE-AE3B-0D5B3D1556FF}");
        }

        public class StatePage
        {
            public static ID StateName = new ID("{7CFCBB2C-E477-440B-8D22-895D9E793DF2}");
        }

        public class Navigation
        {
            public static ID menuItemURL = new ID("{1844F178-C44A-4DFF-B9E7-68EFF60B6BF0}");
            public static ID primaryTargeURL = new ID("{2242434C-6269-4954-BEF5-769A2444B4D4}");
            public static ID primaryStartDate = new ID("{A1349668-79A0-4BA6-891C-E47AC948B226}");
            public static ID primaryEndDate = new ID("{B72BEEAA-2B0F-42C9-944D-6CF821452C91}");
            public static ID primaryCampaignStatus = new ID("{73BD80AA-5443-42AC-B055-97850C421973}");
            public static ID primaryContent = new ID("{20B8EC35-434A-43FC-898E-125FA9A859AA}");

            public static ID secondaryTargeURL = new ID("{A2CB4CA3-6F39-4F8F-9E57-6B506A93D999}");
            public static ID secondaryStartDate = new ID("{9F10D6C5-E2A6-4573-860A-05EF255014F7}");
            public static ID secondaryEndDate = new ID("{DF093A90-9A7A-49E2-8B35-C81CB8E5A1E2}");
            public static ID secondaryCampaignStatus = new ID("{15FE58C4-BDF5-4E2E-9DA2-825BFA8FD57D}");
            public static ID secondaryContent = new ID("{A705106D-B02F-435E-ACDD-B6DB360F3CCA}");
        }

        public class School
        {
            public static ID SchoolImage = new ID("{CFFC324E-9CDF-4857-B29A-6A84AA3502D1}");
        }

        public class SchoolType
        {
            public static ID Elementary = new ID("{9E7C6A13-10CD-4745-A72C-CAF80E249DA1}");
            public static ID Middle = new ID("{6E404DC0-943E-4F24-8A0F-B29955AFF8DE}");
            public static ID High = new ID("{95E6E7DA-B7D0-4B71-93EC-6D6D95BED61B}");
        }

        public class PromoBase
        {
            public static ID TeaserImage = new ID("{1976DB64-F1C6-4BD4-A702-9B121E17D616}");
            public static ID StartDate = new ID("{99A3BF22-7E78-4BC1-BC7A-8413173BC661}");
            public static ID EndDate = new ID("{D12DF992-D92D-47FD-BB99-4F06A2FDD508}");
            public static ID Disabled = new ID("{3F476104-5662-44D2-9BCA-8A20E4891F80}");

        }

        public class ContactUsPage
        {
            public static ID DivisionsToDisplay = new ID("{8F0CC81E-6D82-41A6-8027-1F980B456642}");
        }

        public class SFIntegration
        {
            public static ID SFPostUrl = new ID("{D39C8BF7-F37B-4A14-931A-FC8C054A0EA9}");
            public static ID Mapping = new ID("{5BE30F9A-09FA-4B70-BC24-6075CB2492F5}");
            public static ID Oid = new ID("{B2F8FE49-D7B1-4772-B42A-0698673089FE}");
            public static ID Debug = new ID("{EBE5456F-5768-46A7-BEBD-BE23B79E887A}");
            public static ID DebugEmail = new ID("{0DD003F8-5EAD-4091-9D01-1E451BD60CA9}");
        }

        public class Images
        {
            public static ID Unversioned = new ID("{F1828A2C-7E5D-4BBD-98CA-320474871548}");
            public static ID Versioned = new ID("{C97BA923-8009-4858-BDD5-D8BE5FCCECF7}");
        }
    }
}