﻿using System.Collections.Generic;

namespace TM.Web.Custom.Constants
{
	public class PageUrls
	{
		public const string SitecorePath = "/sitecore/content";
		public const string NoImagePath = "/Images/img-not-available.jpg";
		public const string ConfigSitePath = "sites";
		public const string MapIconPath = "/Images/mappins/";
		public const string DesignCeterMapIconPath = "/Images/mappins/designcenter.png";
		public const string ApplicationName = "sitecore";
                public static List<string> VirtualUrls = new List<string> {
							    "request-information", 
							    "floorplans", 
							    "photos",
							    "homesforsale",
							    "locations", 
							    "options", 
							    "promotions", 
							    "homes-ready-now", 
							    "floor-plans",
							    "schools", 
							    "thank-you",
							    "event-details",
				                "make-an-appointment"          
						           };
                public static List<string> AdditionalCommunityPages = new List<string> {
							    "home-features",
				                            "local-interests",
					                    "site-plan"
						           };
	}
}