﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class PlanDetailsEvents
    {
	public static GAEvent SelectCommunityInfo= new GAEvent("Details", "Click", "SelectCommunityInfo");
public static GAEvent OtherPlansScroller= new GAEvent("Details", "Click", "OtherPlansScroller");
public static GAEvent PinterestPin= new GAEvent("Details", "Click", "PinterestPin");
public static GAEvent Tab_FloorPlan= new GAEvent("Details", "Click", "Tab_FloorPlan");
public static GAEvent Tab_Photos= new GAEvent("Details", "Click", "Tab_Photos");
public static GAEvent Tab_Locations= new GAEvent("Details", "Click", "Tab_Locations");
public static GAEvent Tab_Options= new GAEvent("Details", "Click", "Tab_Options");
public static GAEvent Promo_Options= new GAEvent("Details", "Click", "Promo_Options");
public static GAEvent Button_Options= new GAEvent("Details", "Click", "Button_Options");
public static GAEvent Tab_VirtualTour= new GAEvent("Details", "Click", "Tab_VirtualTour");
public static GAEvent HomeAddressLink= new GAEvent("Details", "Click", "HomeAddressLink");
public static GAEvent ScheduleAppt= new GAEvent("Details", "Click", "ScheduleAppt");
public static GAEvent CrossSellCommunityLink= new GAEvent("Details", "Click", "CrossSellCommunityLink");
    }
}