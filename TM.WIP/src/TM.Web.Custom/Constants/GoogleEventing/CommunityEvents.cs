﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class CommunityEvents
    {
	public static GAEvent CommunityImage_Pinterest= new GAEvent("SocialMedia", "Click", "CommunityImage_Pinterest");
public static GAEvent LeftRail_SignUpForUpdates= new GAEvent("Updates", "Click", "LeftRail_SignUpForUpdates");
public static GAEvent StartLiveChat= new GAEvent("IHC", "Click", "StartLiveChat");
public static GAEvent AddToFavorites= new GAEvent("Favorites", "Click", "AddToFavorites");
public static GAEvent ViewAllAreas= new GAEvent("Search", "Click", "ViewAllAreas");
public static GAEvent DrivingDirections= new GAEvent("Directions", "Click", "DrivingDirections");
public static GAEvent CommunityLinks_LinkName= new GAEvent("Details", "Click", "CommunityLinks_LinkName");
public static GAEvent Finance= new GAEvent("Finance", "Click", "Finance");
public static GAEvent LeftBox_SignUpForUpdates= new GAEvent("Updates", "Click", "LeftBox_SignUpForUpdates");
public static GAEvent Tab_CommunityInfo= new GAEvent("Details", "Click", "Tab_CommunityInfo");
public static GAEvent Tab_HomeForSale= new GAEvent("Search", "Click", "Tab_HomeForSale");
public static GAEvent Tab_FloorPlans= new GAEvent("Details", "Click", "Tab_FloorPlans");
public static GAEvent Tab_Schools= new GAEvent("Search", "Click", "Tab_Schools");
public static GAEvent Tab_Promotions= new GAEvent("Search", "Click", "Tab_Promotions");
public static GAEvent FooterSearch= new GAEvent("Search", "Click", "FooterSearch");
public static GAEvent HomesForSale= new GAEvent("Details", "Click", "HomesForSale");
public static GAEvent HomeAddressLink= new GAEvent("Details", "Click", "HomeAddressLink");
public static GAEvent AddtoFavorites= new GAEvent("Favorites", "Click", "AddtoFavorites");
public static GAEvent ViewHome= new GAEvent("Details", "Click", "ViewHome");
public static GAEvent Tab_VirtualTour= new GAEvent("Details", "Click", "Tab_VirtualTour");
public static GAEvent ModelHome= new GAEvent("Details", "Click", "ModelHome");
public static GAEvent SchoolURL= new GAEvent("Details", "Click", "SchoolURL");
public static GAEvent PromotionURL= new GAEvent("Promotions", "Click", "PromotionURL");
public static GAEvent LeadForm= new GAEvent("LeadForm", "Click", "LeadForm");
public static GAEvent CommunityLink= new GAEvent("Details", "Click", "CommunityLink");
public static GAEvent CreateAccount= new GAEvent("CreateAccount", "Click", "CreateAccount");
public static GAEvent Pinterest= new GAEvent("SocialMedia", "Click", "Pinterest");
public static GAEvent Facebook= new GAEvent("SocialMedia", "Click", "Facebook");
public static GAEvent Homebuyin101= new GAEvent("HomeBuying", "Click", "Homebuyin101");
public static GAEvent PrequalLink= new GAEvent("Finance", "Click", "PrequalLink");
public static GAEvent SelectCommunityInfo= new GAEvent("Details", "Click", "SelectCommunityInfo");
public static GAEvent SignUpForUpdates= new GAEvent("Updates", "", "Click	SignUpForUpdates");
public static GAEvent GetDrivingDirections= new GAEvent("Directions", "Click", "GetDrivingDirections");
public static GAEvent EmailDrivingDirections= new GAEvent("Directions", "Click", "EmailDrivingDirections");
public static GAEvent PrintDrivingDirections= new GAEvent("Directions", "Click", "PrintDrivingDirections");

    }
}