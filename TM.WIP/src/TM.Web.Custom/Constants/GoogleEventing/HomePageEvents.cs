﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Constants.GoogleEventing
{
    public class HomePageEvents
    {
        public static GAEvent InHeaderFindYourNewHomes = new GAEvent("Search", "Click", "InHeaderFindYourNewHomes");
        public static GAEvent InHeader = new GAEvent("JoinInterestList", "Click", "InHeader");
        public static GAEvent SearchBox = new GAEvent("Search", "Click", "SearchBox");
        public static GAEvent LeftRailFindYourNewHome = new GAEvent("Search", "Click", "LeftRailFindYourNewHome");
        public static GAEvent CampaignPosition1 = new GAEvent("Campaign", "Click", "CampaignPosition1");
        public static GAEvent CampaignPosition2 = new GAEvent("Campaign", "Click", "CampaignPosition2");
        public static GAEvent CampaignPosition3 = new GAEvent("Campaign", "Click", "CampaignPosition3");
        public static GAEvent CampaignPosition4 = new GAEvent("Campaign", "Click", "CampaignPosition4");
        public static GAEvent CampaignImage = new GAEvent("Campaign", "Click", "CampaignImage");
        public static GAEvent Login = new GAEvent("Login", "Click", "Login");
        public static GAEvent CreateAccountLink = new GAEvent("CreateAccount", "Click", "CreateAccountLink");
        public static GAEvent BlogLink = new GAEvent("Blog", "Click", "BlogLink");
        public static GAEvent InFooterFindYourNewHome = new GAEvent("Search", "Click", "InFooterFindYourNewHome");
    }
}