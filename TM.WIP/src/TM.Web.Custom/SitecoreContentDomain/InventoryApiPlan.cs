﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TM.Web.Custom.SitecoreContentDomain
{
    [Serializable]
    [DataContract]
    public class InventoryApiPlan
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Name { get; set; }

        [DataMember(Order = 2)]
        public string URL { get; set; }

        [DataMember(Order = 3)]
        public string Status { get; set; }

        [DataMember(Order = 4)]
        public string Description { get; set; }

        [DataMember(Order = 5)]
        public InventoryApiImage[] Images { get; set; }

        [DataMember(Order = 6)]
        public string Ifp { get; set; }

        [DataMember(Order = 7)]
        public string Price { get; set; }

        [DataMember(Order = 8)]
        public string Sqft { get; set; }

        [DataMember(Order = 9)]
        public string Beds { get; set; }

        [DataMember(Order = 10)]
        public string Baths { get; set; }

        [DataMember(Order = 11)]
        public string HalfBaths { get; set; }

        [DataMember(Order = 12)]
        public string Stories { get; set; }

        [DataMember(Order = 13)]
        public string Garage { get; set; }

        [DataMember(Order = 14)]
        public string HasLivingRoom { get; set; }

        [DataMember(Order = 15)]
        public string HasDiningRoom { get; set; }

        [DataMember(Order = 16)]
        public string HasSunRoom { get; set; }

        [DataMember(Order = 17)]
        public string HasStudy { get; set; }

        [DataMember(Order = 18)]
        public string HasLoft { get; set; }

        [DataMember(Order = 19)]
        public string HasOffice { get; set; }

        [DataMember(Order = 20)]
        public string HasGameRoom { get; set; }

        [DataMember(Order = 21)]
        public string HasMediaRoom { get; set; }

        [DataMember(Order = 22)]
        public string HasGuestBedroom { get; set; }

        [DataMember(Order = 23)]
        public string HasBonusRoom { get; set; }

        [DataMember(Order = 24)]
        public string HasBasement { get; set; }

        [DataMember(Order = 25)]
        public string HasFireplace { get; set; }

        [DataMember(Order = 26)]
        public string HasMasterBedroom { get; set; }

        [DataMember(Order = 27)]
        public string HasFamilyRoom { get; set; }

        [DataMember(Order = 28)]
        public string DivisionID { get; set; }

        [DataMember(Order = 29)]
        public string DivisionName { get; set; }

        [DataMember(Order = 30)]
        public string DivisionUrl { get; set; }

        [DataMember(Order = 31)]
        public string CommunityId { get; set; }

        [DataMember(Order = 32)]
        public string CommunityUrl { get; set; }

        [DataMember(Order = 33)]
        public string CommunityName { get; set; }

        [DataMember(Order = 34)]
        public string PlanWidth { get; set; }

        [DataMember(Order = 35)]
        public string PlanDepth { get; set; }

        [DataMember(Order = 36)]
        public string PlanType { get; set; }

    }

    [Serializable]
    [DataContract]
    public class InventoryApiPlanV2
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Name { get; set; }

        [DataMember(Order = 2)]
        public string URL { get; set; }

        [DataMember(Order = 3)]
        public string Status { get; set; }

        [DataMember(Order = 4)]
        public string Description { get; set; }

        [DataMember(Order = 5)]
        public InventoryApiImage[] Images { get; set; }

        [DataMember(Order = 6)]
        public string Ifp { get; set; }

        [DataMember(Order = 7)]
        public string Price { get; set; }

        [DataMember(Order = 8)]
        public string Sqft { get; set; }

        [DataMember(Order = 9)]
        public string Beds { get; set; }

        [DataMember(Order = 10)]
        public string Baths { get; set; }

        [DataMember(Order = 11)]
        public string HalfBaths { get; set; }

        [DataMember(Order = 12)]
        public string Stories { get; set; }

        [DataMember(Order = 13)]
        public string Garage { get; set; }

        [DataMember(Order = 14)]
        public string HasLivingRoom { get; set; }

        [DataMember(Order = 15)]
        public string HasDiningRoom { get; set; }

        [DataMember(Order = 16)]
        public string HasSunRoom { get; set; }

        [DataMember(Order = 17)]
        public string HasStudy { get; set; }

        [DataMember(Order = 18)]
        public string HasLoft { get; set; }

        [DataMember(Order = 19)]
        public string HasOffice { get; set; }

        [DataMember(Order = 20)]
        public string HasGameRoom { get; set; }

        [DataMember(Order = 21)]
        public string HasMediaRoom { get; set; }

        [DataMember(Order = 22)]
        public string HasGuestBedroom { get; set; }

        [DataMember(Order = 23)]
        public string HasBonusRoom { get; set; }

        [DataMember(Order = 24)]
        public string HasBasement { get; set; }

        [DataMember(Order = 25)]
        public string HasFireplace { get; set; }

        [DataMember(Order = 26)]
        public string HasMasterBedroom { get; set; }

        [DataMember(Order = 27)]
        public string HasFamilyRoom { get; set; }

        [DataMember(Order = 28)]
        public string DivisionID { get; set; }

        [DataMember(Order = 29)]
        public string DivisionName { get; set; }

        [DataMember(Order = 30)]
        public string DivisionUrl { get; set; }

        [DataMember(Order = 31)]
        public string CommunityId { get; set; }

        [DataMember(Order = 32)]
        public string CommunityUrl { get; set; }

        [DataMember(Order = 33)]
        public string CommunityName { get; set; }

        [DataMember(Order = 34)]
        public string PlanWidth { get; set; }

        [DataMember(Order = 35)]
        public string PlanDepth { get; set; }

        [DataMember(Order = 36)]
        public string PlanType { get; set; }

        [DataMember(Order = 37)]
        public string VirtualTourURL { get; set; }

        [DataMember(Order=38)]
        public string CommunityPhone { get; set; }

        [DataMember(Order=39)]
        public List<IHCCard> IHCCard { get; set; }

    }
}