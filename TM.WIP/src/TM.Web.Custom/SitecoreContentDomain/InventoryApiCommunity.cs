﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TM.Web.Custom.SitecoreContentDomain
{
    [Serializable]
    [DataContract]
    public class InventoryApiCommunity
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Name { get; set; }

        [DataMember(Order = 2)]
        public string URL { get; set; }

        [DataMember(Order = 3)]
        public string Status { get; set; }

        [DataMember(Order = 4)]
        public string Description { get; set; }

        [DataMember(Order = 5)]
        public string Logo { get; set; }

        [DataMember(Order = 6)]
        public int IsTownsend { get; set; }

        [DataMember(Order = 7)]
        public string Directions { get; set; }

        [DataMember(Order = 8)]
        public string Latitude { get; set; }

        [DataMember(Order = 9)]
        public string Longitude { get; set; }

        [DataMember(Order = 10)]
        public string Address { get; set; }

        [DataMember(Order = 11)]
        public string Zip { get; set; }

        [DataMember(Order = 12)]
        public string Phone { get; set; }

        [DataMember(Order = 13)]
        public SalesTeam[] SalesTeam { get; set; }

        [DataMember(Order = 14)]
        public InventoryApiImage[] Images { get; set; }

        [DataMember(Order = 16)]
        public string SquareFeet { get; set; }

        [DataMember(Order = 17)]
        public string Beds { get; set; }

        [DataMember(Order = 18)]
        public string Baths { get; set; }

        [DataMember(Order = 19)]
        public string HalfBaths { get; set; }

        [DataMember(Order = 20)]
        public string Stories { get; set; }

        [DataMember(Order = 21)]
        public string Garage { get; set; }

        [DataMember(Order = 22)]
        public string SundayHours { get; set; }

        [DataMember(Order = 23)]
        public string MondayHours { get; set; }

        [DataMember(Order = 24)]
        public string TuesdayHours { get; set; }

        [DataMember(Order = 25)]
        public string WednesdayHours { get; set; }

        [DataMember(Order = 26)]
        public string ThursdayHours { get; set; }

        [DataMember(Order = 27)]
        public string FridayHours { get; set; }

        [DataMember(Order = 28)]
        public string SaturdayHours { get; set; }

        [DataMember(Order = 29)]
        public int UseOfficeHoursText { get; set; }

        [DataMember(Order = 30)]
        public string OfficeHoursText { get; set; }

        [DataMember(Order = 31)]
        public string SchoolDistrict { get; set; }

        [DataMember(Order = 32)]
        public School[] Schools { get; set; }

        [DataMember(Order = 33)]
        public int IsActiveAdult { get; set; }

        [DataMember(Order = 34)]
        public int IsMasterPlanned { get; set; }

        [DataMember(Order = 35)]
        public int IsGated { get; set; }

        [DataMember(Order = 36)]
        public int IsCondoOnly { get; set; }

        [DataMember(Order = 37)]
        public int HasPool { get; set; }

        [DataMember(Order = 38)]
        public int HasPlayground { get; set; }

        [DataMember(Order = 39)]
        public int HasGolfCourse { get; set; }

        [DataMember(Order = 40)]
        public int HasTennis { get; set; }

        [DataMember(Order = 41)]
        public int HasSoccer { get; set; }

        [DataMember(Order = 42)]
        public int HasVolleyball { get; set; }

        [DataMember(Order = 43)]
        public int HasBasketball { get; set; }

        [DataMember(Order = 44)]
        public int HasBaseball { get; set; }

        [DataMember(Order = 45)]
        public int HasView { get; set; }

        [DataMember(Order = 46)]
        public int HasLake { get; set; }

        [DataMember(Order = 47)]
        public int HasPond { get; set; }

        [DataMember(Order = 48)]
        public int HasMarina { get; set; }

        [DataMember(Order = 49)]
        public int HasBeach { get; set; }

        [DataMember(Order = 50)]
        public int IsWaterfront { get; set; }

        [DataMember(Order = 51)]
        public int HasPark { get; set; }

        [DataMember(Order = 52)]
        public int HasTrails { get; set; }

        [DataMember(Order = 53)]
        public int HasGreenbelt { get; set; }

        [DataMember(Order = 54)]
        public int HasClubhouse { get; set; }

        [DataMember(Order = 55)]
        public string DivisionID { get; set; }

        [DataMember(Order = 56)]
        public string DivisionName { get; set; }

        [DataMember(Order = 57)]
        public string DivisionUrl { get; set; }

    }

    [Serializable]
    [DataContract]
    public class SalesTeam
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Phone { get; set; }
    }

    [Serializable]
    [DataContract]
    public class School
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Type { get; set; }
    }


    [Serializable]
    [DataContract]
    public class InventoryApiCommunityV2
    {
        [DataMember(Order = 0)]
        public string Id { get; set; }

        [DataMember(Order = 1)]
        public string Name { get; set; }

        [DataMember(Order = 2)]
        public string URL { get; set; }

        [DataMember(Order = 3)]
        public string Status { get; set; }

        [DataMember(Order = 4)]
        public string Description { get; set; }

        [DataMember(Order = 5)]
        public string Logo { get; set; }

        [DataMember(Order = 6)]
        public int IsTownsend { get; set; }

        [DataMember(Order = 7)]
        public string Directions { get; set; }

        [DataMember(Order = 8)]
        public string Latitude { get; set; }

        [DataMember(Order = 9)]
        public string Longitude { get; set; }

        [DataMember(Order = 10)]
        public string Address { get; set; }

        [DataMember(Order = 11)]
        public string Zip { get; set; }

        [DataMember(Order = 12)]
        public string Phone { get; set; }

        [DataMember(Order = 13)]
        public SalesTeam[] SalesTeam { get; set; }

        [DataMember(Order = 14)]
        public InventoryApiImage[] Images { get; set; }

        [DataMember(Order = 16)]
        public string SquareFeet { get; set; }

        [DataMember(Order = 17)]
        public string Beds { get; set; }

        [DataMember(Order = 18)]
        public string Baths { get; set; }

        [DataMember(Order = 19)]
        public string HalfBaths { get; set; }

        [DataMember(Order = 20)]
        public string Stories { get; set; }

        [DataMember(Order = 21)]
        public string Garage { get; set; }

        [DataMember(Order = 22)]
        public string SundayHours { get; set; }

        [DataMember(Order = 23)]
        public string MondayHours { get; set; }

        [DataMember(Order = 24)]
        public string TuesdayHours { get; set; }

        [DataMember(Order = 25)]
        public string WednesdayHours { get; set; }

        [DataMember(Order = 26)]
        public string ThursdayHours { get; set; }

        [DataMember(Order = 27)]
        public string FridayHours { get; set; }

        [DataMember(Order = 28)]
        public string SaturdayHours { get; set; }

        [DataMember(Order = 29)]
        public int UseOfficeHoursText { get; set; }

        [DataMember(Order = 30)]
        public string OfficeHoursText { get; set; }

        [DataMember(Order = 31)]
        public string SchoolDistrict { get; set; }

        [DataMember(Order = 32)]
        public School[] Schools { get; set; }

        [DataMember(Order = 33)]
        public int IsActiveAdult { get; set; }

        [DataMember(Order = 34)]
        public int IsMasterPlanned { get; set; }

        [DataMember(Order = 35)]
        public int IsGated { get; set; }

        [DataMember(Order = 36)]
        public int IsCondoOnly { get; set; }

        [DataMember(Order = 37)]
        public int HasPool { get; set; }

        [DataMember(Order = 38)]
        public int HasPlayground { get; set; }

        [DataMember(Order = 39)]
        public int HasGolfCourse { get; set; }

        [DataMember(Order = 40)]
        public int HasTennis { get; set; }

        [DataMember(Order = 41)]
        public int HasSoccer { get; set; }

        [DataMember(Order = 42)]
        public int HasVolleyball { get; set; }

        [DataMember(Order = 43)]
        public int HasBasketball { get; set; }

        [DataMember(Order = 44)]
        public int HasBaseball { get; set; }

        [DataMember(Order = 45)]
        public int HasView { get; set; }

        [DataMember(Order = 46)]
        public int HasLake { get; set; }

        [DataMember(Order = 47)]
        public int HasPond { get; set; }

        [DataMember(Order = 48)]
        public int HasMarina { get; set; }

        [DataMember(Order = 49)]
        public int HasBeach { get; set; }

        [DataMember(Order = 50)]
        public int IsWaterfront { get; set; }

        [DataMember(Order = 51)]
        public int HasPark { get; set; }

        [DataMember(Order = 52)]
        public int HasTrails { get; set; }

        [DataMember(Order = 53)]
        public int HasGreenbelt { get; set; }

        [DataMember(Order = 54)]
        public int HasClubhouse { get; set; }

        [DataMember(Order = 55)]
        public string DivisionID { get; set; }

        [DataMember(Order = 56)]
        public string DivisionName { get; set; }

        [DataMember(Order = 57)]
        public string DivisionUrl { get; set; }

        [DataMember(Order=58)]
        public string NSEProjectID { get; set; }

        [DataMember(Order = 59)]
        public string StatusChangeDate { get; set; }

        [DataMember(Order = 60)]
        public string OfficeCity { get; set; }

        [DataMember(Order = 61)]
        public string OfficeState { get; set; }

        [DataMember(Order = 62)]
        public OfficeAddress OfficeAddress { get; set; }

        [DataMember(Order = 63)]
        public string CalculatedSquareFeet { get; set; }

        [DataMember(Order = 64)]
        public string CalculatedBeds { get; set; }

        [DataMember(Order = 65)]
        public string CalculatedBaths { get; set; }

        [DataMember(Order = 66)]
        public string CalculatedHalfBaths { get; set; }

        [DataMember(Order = 67)]
        public string CalculatedStories { get; set; }

        [DataMember(Order = 68)]
        public string CalculatedGarage { get; set; }

        [DataMember(Order = 69)]
        public List<IHCCard> IHCCard { get; set; }

        [DataMember(Order = 70)]
        public string SalesforceStateValue { get; set; }

        [DataMember(Order = 71)]
        public string NSEOunit { get; set; }

    }
    [Serializable]
    [DataContract]
    public class OfficeAddress
    {
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
    }
}
