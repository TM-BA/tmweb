﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TM.Web.Custom.SitecoreContentDomain
{
    [Serializable]
    [DataContract]
    public class InventoryApiImage
    {   
        [DataMember (Order=0)]
        public string URL { get; set; }
        [DataMember(Order = 1)]
        public string Type { get; set; }
    }
}