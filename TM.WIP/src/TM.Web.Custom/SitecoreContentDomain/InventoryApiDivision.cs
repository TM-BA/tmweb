﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace TM.Web.Custom.SitecoreContentDomain
{
    [Serializable]
    [DataContract]
    public class InventoryApiDivision
    {
        [DataMember]
        public String Id { get; set; }
        [DataMember]
        public String Url { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public int Active { get; set; }
    }

    [Serializable]
    [DataContract]
    public class InventoryApiDivisionV2
    {
        [DataMember]
        public String Id { get; set; }
        [DataMember]
        public String Url { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public int Active { get; set; }
        [DataMember]
        public string NSEOunit { get; set; }
        [DataMember]
        public string SalesforceStateValue { get; set; }
        [DataMember]
        public List<IHCCard> IHCCard { get; set; }
        [DataMember]
        public string WebDisclaimer { get; set; }
        [DataMember]
        public string PrintDisclaimer { get; set; }
    }



    [Serializable]
    [DataContract]
    public class IHCCard
    {
        [DataMember]
        public string IHCFirstName { get; set; }
        [DataMember]
        public string IHCLastName { get; set; }
        [DataMember]
        public string IHCEmail { get; set; }
        [DataMember]
        public string IHCPhone { get; set; }
    }
}