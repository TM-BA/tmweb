﻿using Outercore.FieldTypes;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Fields
{
    public class CodedSourceVisualList : VisualListContentField
    {
        private string _imageSourceFldName = "Image Source";
        public new string Source
        {
            get
            {
                if (!string.IsNullOrEmpty(ItemID))
                {
                    Item item = Client.ContentDatabase.GetItem(ItemID);
                    Field sourceFld = item.Fields[_imageSourceFldName];
                    
                    if (sourceFld != null && sourceFld.HasValue)
                    {
                        var path = Client.ContentDatabase.GetItem(sourceFld.Value).Paths.FullPath;
                        base.Source = path;
                    }
                }

                return base.Source;
            }
            set
            {

                base.Source = value == null ? value : value.Trim();
                if (!string.IsNullOrEmpty(ItemID) && Sitecore.Data.ID.IsID(ItemID))
                {
                    Item item = Client.ContentDatabase.GetItem(ItemID);
                    if (item != null)
                    {
                        Field sourceFld = item.Fields[_imageSourceFldName];
                        if (sourceFld != null && sourceFld.HasValue && string.IsNullOrWhiteSpace(sourceFld.Value))
                        {
                            var sourceItem = Client.ContentDatabase.GetItem(sourceFld.Value);
			    if(sourceItem!=null)
			    {
				 base.Source = sourceItem.Paths.FullPath;
			    }
                        }
                       
                    }
                }
                 
               
            }
        }
    }
}