﻿
namespace TM.Web.Custom.Modules.PXM.Rendering
{

    using System;
    using System.Linq;
    using Sitecore.PrintStudio.PublishingEngine.Rendering;
    using Sitecore;
    using TM.Web.Custom.Constants;
    using Sitecore.Data.Items;
    using System.Collections.Generic;

    public class XmlRepeater : InDesignItemRendererBase
    {

        protected override void RenderContent(Sitecore.PrintStudio.PublishingEngine.PrintContext printContext, System.Xml.Linq.XElement output)
        {
            if (!printContext.Settings.Parameters.ContainsKey("community"))
            {
                return;
            }

            string query = string.Format("fast://*[@@id='{0}']//*[@@templateid='{1}' and @Home for Sale Status='{2}' and @Is a model home='']",
                                            printContext.Settings.Parameters["community"].ToString(),
                                            TM.Web.Custom.Constants.SCIDs.TemplateIds.HomeForSalePage,
                                            SCIDs.StatusIds.Status.Active);

            var data = Sitecore.Context.Database.SelectItems(query);
            var pages = (int)Math.Ceiling((double)data.Count() / 4);
            var grouped = Split(data);

            grouped.ForEach(t =>
            {
                var count = 0;
                t.ForEach(s =>
                {
                    count++;

                    if (printContext.Settings.Parameters.ContainsKey("home" + count))
                        printContext.Settings.Parameters["home" + count] = s.ID;
                    else
                        printContext.Settings.Parameters.Add("home" + count, s.ID);
                });

                this.RenderChildren(printContext, output);

                for (var y = 0; y < count; y++)
                {
                    if (printContext.Settings.Parameters.ContainsKey("home" + (y + 1)))
                        printContext.Settings.Parameters["home" + (y + 1)] = "";
                    else
                        printContext.Settings.Parameters.Add("home" + (y + 1), "");
                    
                }
            });

            if (grouped.Count() == 0)
                this.RenderChildren(printContext, output);
        }


        public List<List<Item>> Split(Item[] source)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / 4)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}