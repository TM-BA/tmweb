﻿using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using System;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
  public class InventorySnippet : InDesignItemRendererBase
  {
    protected override void RenderContent(PrintContext printContext, XElement output)
    {   
      if (String.IsNullOrEmpty(this.DataSource) || this.DataSource == null) {
            return;
      }
      XElement xelement = output;
      if (this.RenderingItem == printContext.StartItem)
      {
        xelement = RenderItemHelper.CreateXElement("Grid", this.RenderingItem, printContext.Settings.IsClient);
        output.Add((object) xelement);
      }
      if (ScriptHelper.ExecuteScriptReference(printContext, this.RenderingItem, this.GetDataItem(printContext), xelement))
        return;
      this.RenderChildren(printContext, xelement);
    }

    protected override void BeginRender(PrintContext printContext)
    {
      if (!string.IsNullOrEmpty(this.RenderingItem["Item Reference"]))
        this.DataSource = this.RenderingItem["Item Reference"];
      try
      {
        if (string.IsNullOrEmpty(this.RenderingItem["Data Key"]) || !printContext.Settings.Parameters.ContainsKey(this.RenderingItem["Data Key"]))
          return;
        string path = printContext.Settings.Parameters[this.RenderingItem["Data Key"]].ToString();
        if (string.IsNullOrEmpty(path))
          return;
        Item obj1 = printContext.Database.GetItem(path);
        if (obj1 == null)
          return;
        this.DataSource = obj1.ID.ToString();
        string query = this.RenderingItem["Item Selector"];
        if (string.IsNullOrEmpty(query))
          return;
        Item obj2 = obj1.Axes.SelectSingleItem(query);
        if (obj2 == null)
          return;
        this.DataSource = obj2.ID.ToString();
      }
      catch (Exception ex)
      {
        Logger.Error(ex.Message, (Exception) null);
      }
    }
  }
}
