﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunityPriceRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields["Price Override Text"];
                    if (field == null)
                        return null;

                    var price = field.Value;

                    if (String.IsNullOrEmpty(price))
                    {
                        var plans = dataItem.Children
                            .Where(i =>
                                    i.TemplateID == SCIDs.TemplateIds.PlanPage &&
                                    i["Priced from Value"].StringToDouble() > 0.0 &&
                                    SCUtils.SearchStatus(i["Plan Status"]) == GlobalEnums.SearchStatus.Active);

                        if (plans.Count() == 0)
                        {
                            return null;
                        }
                        price = string.Format("Priced from the {0:c0}", plans.Min(p => p["Priced from Value"].StringToDouble()));
                        return this.FormatText(str, price);
                    }
                    else
                    {
                        return this.FormatText(str, price);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}