﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunityPrintSubCommunity : XmlTextFrameRenderer
    {
        protected override void RenderContent(PrintContext printContext, XElement output)
        {

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var subcomm = printContext.Settings.Parameters["SubCommunity"].ToString();

                    if (string.IsNullOrEmpty(subcomm))
                    {
                        return;
                    }

                    var item = Sitecore.Context.Database.GetItem(subcomm.ToString());

                    output.Add(new XCData(item.Fields["Subcommunity Name"].Value));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
        }

    }
}