﻿namespace TM.Web.Custom.Modules.PXM.Rendering
{
    using System;
    using System.Linq;
    using Sitecore.PrintStudio.PublishingEngine.Rendering;
    using Sitecore;
    using TM.Web.Custom.Constants;
    using Sitecore.Form.Web.UI.Controls;
    using System.Collections.Generic;
    using Sitecore.Data.Items;


    public class test
    {
        public List<Item> items { get; set; }
        public string id { get; set; }
    }
    public class DefaultXmlRepeaterPlan : InDesignItemRendererBase
    {
        private List<test> _listHomes = new List<test>();
        /// <summary>
        /// Gets or sets the data sources.
        /// </summary>
        /// <value>
        /// The data sources.
        /// </value>
        public string DataSources { get; set; }

        /// <summary>
        /// Gets or sets the repeat count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public string Count { get; set; }

        public bool HasSubCommunities { get; set; }

        /// <summary>
        /// Gets or sets the name of the child data key.
        /// </summary>
        /// <value>
        /// The name of the child data key.
        /// </value>
        public string ChildDataKeyName { get; set; }

        public string IsSingleHome { get; set; }

        public List<test> ListHomes { get { return _listHomes; } }

        /// <summary>
        /// Preliminary render action invoked before RenderContent <see cref="RenderContent"/>.
        /// </summary>
        /// <param name="printContext">The print context.</param>
        protected override void BeginRender(Sitecore.PrintStudio.PublishingEngine.PrintContext printContext)
        {
            if (!string.IsNullOrEmpty(this.RenderingItem["Item Reference"]))
            {
                this.DataSource = this.RenderingItem["Item Reference"];
            }
           
            if (!string.IsNullOrEmpty(this.RenderingItem["Data Key"]) && printContext.Settings.Parameters.ContainsKey(this.RenderingItem["Data Key"]))
            {
                var data = printContext.Settings.Parameters[this.RenderingItem["Data Key"]].ToString();
                if (!string.IsNullOrEmpty(data))
                {
                    var items = StringUtil.Split(data, '|', true);
                    if (items.Count() > 1)
                    {
                        this.DataSources = data;
                        return;
                    }

                    var contextItem = printContext.Database.GetItem(data);
                    if (contextItem != null)
                    {
                        this.DataSource = contextItem.ID.ToString();
                    }
                }
            }

            // Get the data item assigned to the repeater
            var dataItem = this.GetDataItem(printContext);
            if (dataItem != null)
            {
                HasSubCommunities = dataItem
                    .Axes
                    .GetDescendants()
                    .Any(it => it.TemplateID.ToString().Equals(SCIDs.TemplateIds.SwitchSubCommunity));

                var homes = dataItem
                        .Axes
                        .GetDescendants()
                        .Where(home => home.TemplateID.Equals(SCIDs.TemplateIds.HomeForSalePage) &&
                            home.Fields["Home for Sale Status"] != null &&
                            home.Fields["Home for Sale Status"].Value == SCIDs.StatusIds.Status.Active &&
                            home.Fields["Is a model home"].Value == "");

             
                
                if (HasSubCommunities)
                {
                    var grouped = homes
                        .Where(home => !string.IsNullOrEmpty(home.Fields["SubCommunity ID"].Value))
                        .GroupBy(x => x.Fields["SubCommunity ID"].ToString());

                    grouped
                        .ToList()
                        .ForEach(h =>
                            _listHomes.Add(new test
                            {
                                id = h.Key,
                                items = h.ToList()
                            })
                        );
                }
                else
                {
                    _listHomes.Add(new test
                    {
                        items = homes.ToList()
                    });
                }
            }
        }

        /// <summary>
        /// Renders the content.
        /// </summary>
        /// <param name="printContext">The print context.</param>
        /// <param name="output">The output.</param>
        protected override void RenderContent(Sitecore.PrintStudio.PublishingEngine.PrintContext printContext, System.Xml.Linq.XElement output)
        {

            if (HasSubCommunities) {
                if (ListHomes.Count() > 0)
                {
                    ListHomes
                        .ForEach(x => {
                            printContext.Settings.Parameters["SubCommunity"] = x.id;
                            printContext.Settings.Parameters[this.ChildDataKeyName] = x.items;
                            if (IsSingleHome != "True")
                            {
                                RenderChildren(printContext, output);
                            }
                        });
                    return;
                }
            }
            else
            {
                if (ListHomes.Count() > 0)
                {
                    ListHomes
                        .ForEach(x =>
                        {
                            printContext.Settings.Parameters[this.ChildDataKeyName] = x.items;
                            if (IsSingleHome == "True")
                            {
                                RenderChildren(printContext, output);
                            }
                        });
                    return;
                }
            }
        }
    }
}