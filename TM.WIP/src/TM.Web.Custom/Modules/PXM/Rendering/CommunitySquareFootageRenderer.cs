﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunitySquareFootageRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields["SqFt Override Text"];
                    if (field == null)
                        return null;

                    var sqftOverride = field.Value;
                    if (String.IsNullOrEmpty(sqftOverride))
                    {
                        var plans = dataItem.Children
                        .Where(plan =>
                                plan.TemplateID == SCIDs.TemplateIds.PlanPage &&
                                SCUtils.SearchStatus(plan["Plan Status"]) == GlobalEnums.SearchStatus.Active);

                        if (plans.Count() == 0)
                        {
                            return null;
                        }

                        var minVal = 0;
                        var maxVal = 0;
                        var rval = string.Empty;

                        minVal = plans.Min(plan => plan["Square Footage"].StringToInt32());
                        maxVal = plans.Max(plan => plan["Square Footage"].StringToInt32());

                        if (minVal == 0 && maxVal == 0)
                            return null;

                        if (minVal == maxVal)
                        {
                            rval = string.Format("{0:N0} Sq. Ft.", maxVal);
                        }
                        else
                        {
                            rval = string.Format("{0:N0} to {1:N0} Sq. Ft.", minVal, maxVal);
                        }

                        return this.FormatText(str, rval);
                    }
                    else
                    {
                        return this.FormatText(str, sqftOverride);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}