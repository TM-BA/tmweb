﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Configuration;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class ImageFieldRenderer : InDesignItemRendererBase
    {
        protected override void RenderContent(PrintContext printContext, XElement output)
        {
            var dataItem = GetDataItem(printContext);
            var itemField = base.RenderingItem["Item Field"];

            if (dataItem != null && !string.IsNullOrEmpty(itemField))
            {
                try
                {
                    var field = dataItem.Fields[itemField];
                    if (field == null)
                    {
                        return;
                    }
                    var index = 0;
                    Int32.TryParse(base.RenderingItem["Index"], out index);
                    var images = field.Value
                        .Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                    if (index < 0 || index >= images.Length)
                    {
                        return;
                    }

                    var id = images[index];
                    var image = printContext.Database.GetItem(id);
                    if (image == null)
                    {
                        return;
                    }

                    var absoluteFilePath = ImageRendering.CreateImageOnServer(printContext.Settings, image);
                    var relativeFilePath = printContext.Settings.FormatResourceLink(absoluteFilePath);
                    var xElement = RenderItemHelper.CreateXElement("Image", image, printContext.Settings.IsClient, null);
                    xElement.SetAttributeValue("LowResSrc", relativeFilePath);
                    xElement.SetAttributeValue("HighResSrc", relativeFilePath);
                    xElement.SetAttributeValue("Width", base.RenderingItem["Width"]);
                    xElement.SetAttributeValue("Height", base.RenderingItem["Height"]);
                    xElement.SetAttributeValue("X", 0);
                    xElement.SetAttributeValue("Y", 0);

                    var imageFrame = RenderItemHelper.CreateXElement("ImageFrame", base.RenderingItem, printContext.Settings.IsClient, null);
                    imageFrame.SetAttributeValue("SitecoreFieldname", base.RenderingItem["item field"]);
                    imageFrame.SetAttributeValue("SitecoreMediaID", base.RenderingItem["medialibrary reference"]);
                    imageFrame.SetAttributeValue("ItemReferenceID", base.RenderingItem["item reference"]);
                    imageFrame.SetAttributeValue("RenderingID", base.RenderingItem["xml renderer"]);
                    imageFrame.SetAttributeValue("ItemReferenceDisplayName", string.Empty);
                    imageFrame.Add(xElement);

                    output.Add(imageFrame);
                    this.RenderChildren(printContext, imageFrame);
                }
                catch (Exception ex)
                {
                    Sitecore.Diagnostics.Log.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
                 //   Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
                }
            }
        }
    }
}