﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class DisclaimerRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;
            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }
            if (string.IsNullOrEmpty(ContentFieldName))
            {
                return null;
            }

            try
            {
                var baseItem = GetDataItem(printContext);
                if (baseItem != null)
                {
                    return this.FormatText(str, GetDisclaimerText(baseItem, ContentFieldName));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }


        private string GetDisclaimerText(Item baseItem, string name, bool getHSH = true)
        {
            var prevName = name;
            var prevItem = baseItem;
            if (getHSH)
                name = "Hot Sheet Disclaimer";

            while (baseItem.Parent != null )
            {
                if (baseItem.Fields != null && baseItem.Fields[name] != null)
                {
                    if (!String.IsNullOrEmpty(baseItem.Fields[name].Value))
                    {
                        return baseItem.Fields[name].Value;
                    }
                }
                baseItem = baseItem.Parent;
            }
            return getHSH ? GetDisclaimerText(prevItem, prevName, false) : null;
        }
    }
}