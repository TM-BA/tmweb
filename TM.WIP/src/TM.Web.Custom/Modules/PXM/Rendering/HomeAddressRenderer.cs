﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;
using Sitecore.Data;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class HomeAddressRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    if (dataItem.TemplateID != SCIDs.TemplateIds.HomeForSalePage)
                    {
                        return null;
                    }

                    var text = new StringBuilder();
                    if (dataItem.Fields["Street Address 1"] != null)
                    {
                        var street = dataItem.Fields["Street Address 1"].Value;
                        if (!String.IsNullOrEmpty(street))
                        {
                            text.AppendFormat("{0} | ", street);
                        }
                    }

                    var plan = dataItem.Parent;
                    if (plan != null && plan.TemplateID == SCIDs.TemplateIds.PlanPage)
                    {
                        var planName = plan.Fields["Plan Name"];
                        if (planName != null)
                        {
                            text.Append(planName.Value);
                        }
                    }


                    return this.FormatText(str, text.ToString());

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}