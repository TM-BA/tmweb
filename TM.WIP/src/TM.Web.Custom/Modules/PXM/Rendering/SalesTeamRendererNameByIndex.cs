﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class SalesTeamRendererNameByIndex : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            try
            {

                var context = new ParseContext(printContext.Database, printContext.Settings)
                {
                    DefaultParagraphStyle = str,
                    ParseDefinitions = RichTextParser.GetParseDefinitionCollection(this.RenderingItem)
                };


                var index = 0;
                Int32.TryParse(base.RenderingItem["Index"], out index);

                if (index < 0)
                {
                    return null;
                }

                var element = new XElement((XName)"temp");
                switch (index)
                {
                    case 0:
                        XElementExtensions.AddFragment(element,
                            RichTextParser.ConvertToXml(printContext.Settings.Parameters["sales_member_1_name"].ToString(), context, printContext.Language));
                        break;
                    case 1:
                        XElementExtensions.AddFragment(element,
                            RichTextParser.ConvertToXml(printContext.Settings.Parameters["sales_member_2_name"].ToString(), context, printContext.Language));
                        break;
                }

                return element.Elements();

            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}
