﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CustomFieldRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;
            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }
            if (string.IsNullOrEmpty(ContentFieldName))
            {
                return null;
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields[ContentFieldName];
                    if (field == null)
                        return null;
                    switch (field.Type)
                    {
                        case "Rich Text":
                            var context = new ParseContext(printContext.Database, printContext.Settings)
                            {
                                DefaultParagraphStyle = str,
                                ParseDefinitions = RichTextParser.GetParseDefinitionCollection(this.RenderingItem)
                            };
                            var content1 = RichTextParser.ConvertToXml(field.Value, context, printContext.Language);
                            var element = new XElement((XName)"temp");
                            XElementExtensions.AddFragment(element, content1);
                            return element.Elements();
                        case "Droplink":
                            var linkedItem = printContext.Database.GetItem(field.Value);
                            return this.FormatText(str, linkedItem["Name"]);
                        default :
                            var content = SitecoreHelper.FetchFieldValue(dataItem, field.Name, printContext.Database, str);
                            if (string.IsNullOrEmpty(content))
                            {
                                content = field.Value;
                            }
                            return this.FormatText(str, content);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}