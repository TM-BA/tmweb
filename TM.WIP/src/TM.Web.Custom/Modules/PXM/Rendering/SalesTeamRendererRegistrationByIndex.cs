﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class SalesTeamRendererRegistrationByIndex : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;
            try
            {
                var context = new ParseContext(printContext.Database, printContext.Settings)
                {
                    DefaultParagraphStyle = str,
                    ParseDefinitions = RichTextParser.GetParseDefinitionCollection(this.RenderingItem)
                };


                var index = 0;
                Int32.TryParse(base.RenderingItem["Index"], out index);

                if (index < 0)
                {
                    return null;
                }
                //DEBUG
                //printContext.Settings.Parameters["sales_member_1_registration"] = "12345";
                var element = new XElement((XName)"temp");
                switch (index)
                {
                    case 0:
                        var bre = printContext.Settings.Parameters["sales_member_1_registration"];
                        if (bre == null || String.IsNullOrEmpty(bre.ToString())) {
                            return null;
                        }
                        XElementExtensions.AddFragment(element,
                            RichTextParser.ConvertToXml(bre.ToString(), context, printContext.Language));
                        break;
                    case 1:
                        var bre2 = printContext.Settings.Parameters["sales_member_2_registration"];
                        if (bre2 == null || String.IsNullOrEmpty(bre2.ToString()))
                        {
                            return null;
                        }
                        XElementExtensions.AddFragment(element,
                            RichTextParser.ConvertToXml(bre2.ToString(), context, printContext.Language));
                        break;
                }

                return element.Elements();
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame Registration: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}
