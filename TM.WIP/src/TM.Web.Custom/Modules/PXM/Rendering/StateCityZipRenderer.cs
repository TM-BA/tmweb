﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;
using Sitecore.Data;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class StateCityZipRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var zipField = dataItem.Fields["Zip or Postal Code"];
                    if (zipField == null)
                    {
                        return null;
                    }
                    return this.FormatText(str, string.Format(" {1}, {0} {2}", GetState(dataItem), GetCityName(dataItem), zipField.Value));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }

        private string GetCityName(Item context)
        {
            if (context == null || context.Parent == null)
            {
                return null;
            }
            return context.Parent.Fields["City Name"].Value;
        }

        private string GetState(Item context)
        {
            var originalContext = context;
            while (context.Parent != null &&
                   context.TemplateID != SCIDs.TemplateIds.StatePage)
            {
                if (context.Parent.Fields["State Name"] != null)
                {
                    var state = Sitecore.
                        Context.
                        Database.
                        GetItem(new ID(context.Parent.Fields["State Name"].Value));

                    if (state != null) {
                        return state.Fields["Code"].Value;
                    }
                }
                context = context.Parent;
            }
            return null;
        }
    }
}