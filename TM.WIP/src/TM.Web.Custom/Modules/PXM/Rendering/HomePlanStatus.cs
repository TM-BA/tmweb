﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class HomePlanStatus : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;
            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }
            if (string.IsNullOrEmpty(ContentFieldName))
            {
                return null;
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var status = string.Empty;
                    var planName = string.Empty;
                    var text = new StringBuilder();

                    var plan = dataItem.Parent;
                    if (plan != null && plan.TemplateID == SCIDs.TemplateIds.PlanPage)
                    {
                        var nameItem = plan.Fields["Plan Name"];
                        if (nameItem != null)
                        {
                            text.AppendFormat("{0} | ", nameItem.Value);
                        }
                    }

                    var statusItem = dataItem.Fields["Status for Availability"];
                    if (statusItem != null )
                    {
                        var member = printContext.Database.GetItem(statusItem.Value);
                        if (member != null) {
                            text.Append(member.Name);
                        }
                    }

                    return this.FormatText(str, text.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}