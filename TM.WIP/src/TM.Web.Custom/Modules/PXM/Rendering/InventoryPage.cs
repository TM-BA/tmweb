﻿using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using System;
using System.Linq;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
  public class InventoryPage : InDesignItemRendererBase
  {
    protected override void RenderContent(PrintContext printContext, XElement output)
    {
      if (this.RenderingItem["Skip"].Equals("1"))
        return;
      XElement parentNode = new XElement((XName) "ScriptResult");
      if (ScriptHelper.ExecuteScriptReference(printContext, this.RenderingItem, this.GetDataItem(printContext), parentNode))
      {
        int num = Enumerable.Count<XElement>(parentNode.Descendants((XName) "Page"));
        if (num > 0)
          printContext.PageCount += num;
        output.Add((object) parentNode.Elements());
      }
      else
      {
        XElement page = this.CreatePage(printContext, true, false);
        ++printContext.PageCount;
        this.RenderChildren(printContext, page);
        output.Add((object) page);
      }
    }

    protected virtual XElement CreatePage(PrintContext printContext, bool skipNumber, bool emptyNumber)
    {
      Item renderingItem = this.RenderingItem;
      string elementName = "Page";
      if (renderingItem.Fields["Flow"] != null && renderingItem.Fields["Flow"].Value.Equals("true", StringComparison.InvariantCultureIgnoreCase))
        elementName = "Flow";
      XElement xelement = RenderItemHelper.CreateXElement(elementName, renderingItem, printContext.Settings.IsClient, (Item) null, true);
      if (printContext.Settings.IsClient && renderingItem.Fields["PageXML"] != null)
        XElementExtensions.AddInnerXml(xelement, renderingItem.Fields["PageXML"].Value, false);
      if (emptyNumber)
        xelement.SetAttributeValue((XName) "Number", (object) string.Empty);
      else if (skipNumber)
        xelement.SetAttributeValue((XName) "Number", (object) printContext.PageCount.ToString());
      xelement.SetAttributeValue((XName) "HorizontalGuides", (object) renderingItem["HorizontalGuides"]);
      xelement.SetAttributeValue((XName) "VerticalGuides", (object) renderingItem["VerticalGuides"]);
      xelement.SetAttributeValue((XName) "Margin", (object) renderingItem["Margin"]);
      return xelement;
    }
  }
}
