﻿namespace TM.Web.Custom.Modules.PXM.Rendering
{
    using System;
    using System.Linq;
    using Sitecore.PrintStudio.PublishingEngine.Rendering;
    using Sitecore;
    using System.Collections.Generic;
    using Sitecore.Data.Items;

    public class DefaultXmlRepeater : InDesignItemRendererBase
    {
        /// <summary>
        /// Gets or sets the data sources.
        /// </summary>
        /// <value>
        /// The data sources.
        /// </value>
        public string DataSources { get; set; }

        /// <summary>
        /// Gets or sets the repeat count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public string Count { get; set; }

        /// <summary>
        /// Gets or sets the name of the child data key.
        /// </summary>
        /// <value>
        /// The name of the child data key.
        /// </value>
        public string ChildDataKeyName { get; set; }

        /// <summary>
        /// Preliminary render action invoked before RenderContent <see cref="RenderContent"/>.
        /// </summary>
        /// <param name="printContext">The print context.</param>
        protected override void BeginRender(Sitecore.PrintStudio.PublishingEngine.PrintContext printContext)
        {
            if (!string.IsNullOrEmpty(this.RenderingItem["Item Reference"]))
            {
                this.DataSource = this.RenderingItem["Item Reference"];
            }

            if (!string.IsNullOrEmpty(this.RenderingItem["Data Key"]) && printContext.Settings.Parameters.ContainsKey(this.RenderingItem["Data Key"]))
            {
                var data = printContext.Settings.Parameters[this.RenderingItem["Data Key"]];
                if (data != null)
                {
                    var items = (List<Item>)data;

                    if (!items.Any())
                        return;

                    this.DataSources = items
                        .Select(x=> x.ID.ToString())
                        .Aggregate((x1,x2) => x1 + "|"+ x2);       
                }
            }
        }

        /// <summary>
        /// Renders the content.
        /// </summary>
        /// <param name="printContext">The print context.</param>
        /// <param name="output">The output.</param>
        protected override void RenderContent(Sitecore.PrintStudio.PublishingEngine.PrintContext printContext, System.Xml.Linq.XElement output)
        {
            if (!string.IsNullOrEmpty(this.ChildDataKeyName))
            {
                printContext.Settings.Parameters[this.ChildDataKeyName] = this.DataSource;
            }

            if (!string.IsNullOrEmpty(this.DataSources))
            {
                foreach (var dataSource in this.DataSources.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    this.DataSource = dataSource;
                    if (!string.IsNullOrEmpty(this.ChildDataKeyName))
                    {
                        printContext.Settings.Parameters[this.ChildDataKeyName] = dataSource;
                    }

                    RenderChildren(printContext, output);
                }

                return;
            }

            var dataItem = this.GetDataItem(printContext);
            if (dataItem == null && !printContext.Settings.IsClient)
            {
                return;
            }

            int count;
            if (!string.IsNullOrEmpty(this.Count) && int.TryParse(this.Count, out count) && count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    // Render child elements
                    this.RenderChildren(printContext, output);
                }

                return;
            }

           //this.RenderChildren(printContext, output);
        }
    }
}