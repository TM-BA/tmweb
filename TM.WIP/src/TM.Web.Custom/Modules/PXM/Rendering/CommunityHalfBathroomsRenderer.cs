﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunityHalfBathroomsRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields["No of Half Baths Override Text"];
                    if (field == null)
                        return null;

                    var baths = field.Value;
                    if (String.IsNullOrEmpty(baths))
                    {
                        var plans = dataItem.Children
                        .Where(plan =>
                                plan.TemplateID == SCIDs.TemplateIds.PlanPage &&
                                SCUtils.SearchStatus(plan["Plan Status"]) == GlobalEnums.SearchStatus.Active);

                        if (plans.Count() == 0)
                        {
                            return null;
                        }

                        var minVal = 0;
                        var maxVal = 0;
                        var halfBathroomsText = string.Empty;

                        minVal = plans.Min(home => home["Number of Half Bathrooms"].StringToInt32());
                        maxVal = plans.Max(home => home["Number of Half Bathrooms"].StringToInt32());

                        if (minVal == 0 && maxVal == 0)
                            return null;

                        if (minVal == maxVal)
                        {
                            halfBathroomsText = string.Format("{0} Half Bath{1}", maxVal, maxVal > 1 ? "s" : string.Empty);
                        }
                        else
                        {
                            halfBathroomsText = string.Format("{0} to {1} Half Bath{2}", minVal, maxVal, maxVal > 1 ? "s" : string.Empty);
                        }

                        return this.FormatText(str, halfBathroomsText);
                    }
                    else
                    {
                        return this.FormatText(str, baths);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}