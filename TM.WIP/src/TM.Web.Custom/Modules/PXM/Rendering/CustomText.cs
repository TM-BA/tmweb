﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CustomText : XmlTextFrameRenderer
    {
        protected override void RenderContent(PrintContext printContext, XElement output)
        {

          
                if ( !String.IsNullOrEmpty(this.RenderingItem["Selector"]))
                {
                    output.Add(new XCData(this.RenderingItem["Selector"]));
                }
                else if (String.IsNullOrEmpty(printContext.Settings.Parameters["custom-text"].ToString())) 
                {
                    output.Add(new XCData(this.RenderingItem["custom-text"]));
                }
            
           // output.Add(paragraph);
        }
    }
}