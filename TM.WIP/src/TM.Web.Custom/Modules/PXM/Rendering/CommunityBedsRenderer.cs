﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunityBedsRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields["No of Beds Override Text"];
                    if (field == null)
                        return null;

                    var beds = field.Value;

                    if (String.IsNullOrEmpty(beds))
                    {
                        var plans = dataItem.Children
                            .Where(plan =>
                                    plan.TemplateID == SCIDs.TemplateIds.PlanPage &&
                                    SCUtils.SearchStatus(plan["Plan Status"]) == GlobalEnums.SearchStatus.Active);

                        if (plans.Count() == 0)
                        {
                            return null;
                        }

                        var minVal = 0;
                        var maxVal = 0;
                        var maxDen = 0;
                        var rval = string.Empty;

                        minVal = plans.Min(m => m["Number of Bedrooms"].StringToInt32());
                        maxVal = plans.Max(m => m["Number of Bedrooms"].StringToInt32());
                        maxDen = plans.Max(m => m["Number of Dens"].StringToInt32());

                        if (minVal == 0 && maxVal == 0 && maxDen == 0)
                            return null;

                        if (minVal == maxVal)
                        {
                            beds = string.Format("{0} Bed{1}", minVal, minVal > 1 ? "s" : "");
                        }
                        else
                        {
                            beds = string.Format("{0} to {1} Beds", minVal, maxVal);
                        }

                        beds += maxDen > 0 ? " with Den" : string.Empty;

                        return this.FormatText(str, beds);
                    }
                    else
                    {
                        return this.FormatText(str, beds);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}