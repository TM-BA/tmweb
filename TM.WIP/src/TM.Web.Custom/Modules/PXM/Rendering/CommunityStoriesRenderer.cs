﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;


namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class CommunityStoriesRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields["No of Story Homes Override Text"];
                    if (field == null)
                        return null;

                    var stories = field.Value;

                    if (String.IsNullOrEmpty(stories))
                    {
                        var plans = dataItem.Children
                            .Where(i =>
                                    i.TemplateID == SCIDs.TemplateIds.PlanPage &&
                                    SCUtils.SearchStatus(i["Plan Status"]) == GlobalEnums.SearchStatus.Active);

                        if (plans.Count() == 0)
                        {
                            return null;
                        }

                        var minVal = 0;
                        var maxVal = 0;
                        var rval = string.Empty;

                        minVal = plans.Min(m => m["Number of Stories"].StringToInt32());
                        maxVal = plans.Max(m => m["Number of Stories"].StringToInt32());

                        if (minVal == 0 && maxVal == 0)
                            return null;

                        if (minVal == maxVal)
                        {
                            rval = string.Format("{0} Story Home{1}", minVal, minVal > 1 ? "s" : "");
                        }
                        else
                        {
                            rval = string.Format("{0} to {1} Story Homes", minVal, maxVal);
                        }

                        return this.FormatText(str, rval);
                    }
                    else
                    {
                        return this.FormatText(str, stories);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}