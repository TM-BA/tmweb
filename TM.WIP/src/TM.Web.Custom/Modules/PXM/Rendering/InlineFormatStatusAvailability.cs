﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class InlineFormatStatusAvailability : XmlTextFrameRenderer
    {
        protected override void RenderContent(PrintContext printContext, XElement output)
        {
            
            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields[this.ContentFieldName];
                    if (field == null)
                        return;

                    if (String.IsNullOrEmpty(field.Value))
                    {
                        return;
                    }
                    else
                    {
            
                        var member = printContext.Database.GetItem(field.Value);

                        if (member == null)
                        {
                            return;
                        }
                        output.Add(new XCData(member.Name));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
        }
    }
}