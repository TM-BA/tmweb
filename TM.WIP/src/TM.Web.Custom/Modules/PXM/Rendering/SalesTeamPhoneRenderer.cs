﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.PrintStudio.PublishingEngine;
using Sitecore.PrintStudio.PublishingEngine.Helpers;
using Sitecore.PrintStudio.PublishingEngine.Rendering;
using Sitecore.PrintStudio.PublishingEngine.Scripting;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TM.Web.Custom.Modules.PXM.Rendering
{
    public class SalesTeamPhoneRenderer : XmlTextFrameRenderer
    {
        protected override IEnumerable<XElement> GetContent(PrintContext printContext, XElement textFrameNode)
        {
            var xattribute = textFrameNode.Attribute("ParagraphStyle");
            var str = xattribute == null || string.IsNullOrEmpty(xattribute.Value) ? "NormalParagraphStyle" : xattribute.Value;

            if (!string.IsNullOrEmpty(InDesignContent))
            {
                return FormatText(str, InDesignContent);
            }
            if (string.IsNullOrEmpty(ContentFieldName))
            {
                return null;
            }

            try
            {
                var dataItem = GetDataItem(printContext);
                if (dataItem != null)
                {
                    var field = dataItem.Fields[this.ContentFieldName];
                    if (field == null)
                        return null;

                    if (String.IsNullOrEmpty(field.Value))
                    {
                        return null;
                    }
                    else
                    {
                        var salesTeam = field.Value
                            .Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        var index = 0;
                        Int32.TryParse(base.RenderingItem["Index"], out index);

                        if (index < 0 || index >= salesTeam.Length)
                        {
                            return null;
                        }

                        var member = printContext.Database.GetItem(salesTeam[index]);

                        if (member == null)
                        {
                            return null;
                        }
                        return this.FormatText(str, member["Phone"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Rendering TextFrame: " + RenderingItem.ID, ex);
            }
            return null;
        }
    }
}