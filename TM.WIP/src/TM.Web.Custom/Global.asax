﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Runtime.InteropServices" %>
<%@ Import Namespace="Sitecore.Diagnostics" %>
<script runat="server">

     void Application_Error(object sender, EventArgs e)
    {
        if (null != Context && null != Context.AllErrors)
        {
            foreach (Exception err in Context.AllErrors)
            {
                Log.SingleError(err.Message, sender);
            }
        }

        //bool isUnexpectedException = true;
        HttpContext context = ((HttpApplication) sender).Context;

        Exception ex = context.Server.GetLastError();
        if (ex.InnerException != null)
            ex = ex.InnerException;

        Log.Error(ex.Message, sender);

        //Response.Redirect("/sitecore/service/error.aspx");
    }

    public void FormsAuthentication_OnAuthenticate(object sender, FormsAuthenticationEventArgs args)
    {
        string frameworkVersion = GetFrameworkVersion();
        if (!string.IsNullOrEmpty(frameworkVersion) &&
            frameworkVersion.StartsWith("v4.", StringComparison.InvariantCultureIgnoreCase))
        {
            args.User = Sitecore.Context.User;
        }
    }

    private string GetFrameworkVersion()
    {
        try
        {
            return RuntimeEnvironment.GetSystemVersion();
        }
        catch (Exception ex)
        {
            Log.Error("Cannot get framework version", ex, this);
            return string.Empty;
        }
    }


</script>