﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TM.Web.Custom.Pipelines
{
    using SC = Sitecore;
    using Sitecore.Shell.Applications.ContentEditor.Pipelines.RenderContentEditor;
    public class SetEditorFormatter
    {
        public void Process(RenderContentEditorArgs args)
        {
            args.EditorFormatter =new TM.Web.Custom.Pipelines.EditorFormatter {Arguments = args};
        }
    }
}