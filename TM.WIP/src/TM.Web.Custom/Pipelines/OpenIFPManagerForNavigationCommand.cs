﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;
using TM.Utils;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Pipelines
{
    /// <summary>
    /// Represents the OpenIFPManagerForNavigationCommand .
    /// 
    /// </summary>
    [Serializable]
    public class OpenIFPManagerForNavigationCommand : Command
    {
        /// <summary>
        /// Executes the command in the specified context.
        /// 
        /// </summary>
        /// <param name="context">The context.</param>
        public override void Execute(CommandContext context)
        {
            var item = context.Items[0];
            var itemID = context.Items[0].ID.ToShortID().ToString();
            var user = Sitecore.Context.User.GetLocalName();
            var ifpbuilderid = SCUtils.GetIFPBuilderID(item);

            var token =
                CryptographyHelpers.EncryptString(user + ":" + DateTime.Now.AddMinutes(30).ToUniversalTime().Ticks,
                    "IFP_ROCKS!");
            token = HttpUtility.UrlEncode(token);
            var webSiteUrl = "http://ifp.taylormorrison.com/manager/TMAuthenticationProxy.ashx?token=" + token +
                             "&bid=" + ifpbuilderid;
            SheerResponse.Eval("window.open('" + webSiteUrl + "', '_blank')");
          

        }



        /// <summary>
        /// Queries the state of the command.
        /// 
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The state of the command.
        /// </returns>
        public override CommandState QueryState(CommandContext context)
        {
            return CommandState.Enabled;
        }

         
    }
}