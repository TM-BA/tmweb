﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.Events;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Pipelines
{
    public class SCItemEventHandler
    {
        public static readonly SynchronizedCollection<ID> InProcess = new SynchronizedCollection<ID>();
        public string Database { get;set; } 

        public void OnPlanItemSaving(object sender, EventArgs args)
        {
            var item = Event.ExtractParameter<Item>(args, 0);

	    //custom event handler only for plan saves
            if (item.TemplateID != SCIDs.TemplateIds.PlanPage || item.ID == SCIDs.TemplateIds.PlanTemplateStandardValue)
	        return;



	    //if item was already processed and if DB is not master return
            if (InProcess.Contains(item.ID) ||
                (item.Database != null &&
                String.Compare(item.Database.Name, this.Database,StringComparison.InvariantCultureIgnoreCase) != 0))
            {
                InProcess.Remove(item.ID);
                return;
            }
            InProcess.Add(item.ID);
            try
            {
                new MasterPlanInheritor(item).Process(Context.ContentDatabase);
            }
            finally
            {
                InProcess.Remove(item.ID);
            }
            
          
        }


	public void OnHomeForSaleAdded(object sender, EventArgs  args)
	{
        var itemCreatedEventArg = Event.ExtractParameter<ItemCreatedEventArgs>(args, 0);

	 var item = itemCreatedEventArg.Item;

        //custom event handler only for plan saves
        if ( item.TemplateID != SCIDs.TemplateIds.HomeForSalePage
            || item.ID == SCIDs.TemplateIds.HomeForSaleTemplateStandardValue)
            return;


        //if item was already processed and if DB is not master return
        if (InProcess.Contains(item.ID) ||
            (item.Database != null &&
            String.Compare(item.Database.Name, this.Database, StringComparison.InvariantCultureIgnoreCase) != 0))
        {
            InProcess.Remove(item.ID);
            return;
        }
        InProcess.Add(item.ID);
        try
        {
            new MasterPlanInheritor(item,true).Process(Context.ContentDatabase);
        }
        finally
        {
            InProcess.Remove(item.ID);
        }
            

	}

	


      
    }
}