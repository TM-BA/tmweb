﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using TM.Utils.Web;
using System.Configuration;

namespace TM.Web.Custom.Pipelines.RequestProcessor
{
    public class VirtualRequestProcessor : HttpRequestProcessor
    {

        private List<string> _virtualUrlSegments = PageUrls.VirtualUrls;
        public override void Process(HttpRequestArgs args)
        {
	    //dont process sitecore content management urls
            if (Sitecore.Context.Domain.Name != "extranet" || args.Url.FilePath.Contains("sitecore") || args.Url.FilePath.Contains("visitoridentification") || args.Url.FilePath.Contains("searchmethods.asmx") || args.Url.FilePath.Contains("disclaimer.aspx"))
            {
                return;
            }

            if (args.Url.FilePath.Contains("robots.txt"))
            {
                HttpResponse httpResponse = HttpContext.Current.Response;
                httpResponse.ContentType = "text/plain";

                string isPrivate = string.Empty;
                if (ConfigurationManager.AppSettings["IsPrivate"] != null)
                    isPrivate = ConfigurationManager.AppSettings["IsPrivate"];
                
                if (isPrivate != "true")
                {
                    var sitemapfile = "Sitemap/TaylorMorrison.xml";
                    var digital = String.Empty;
                    var company = Sitecore.Context.GetSiteName().ToLower();

                    switch (company)
                    {
                        case CommonValues.DarlingHomesDomain:
                            sitemapfile = "Sitemap/DarlingHomes.xml";
                            break;
                        case CommonValues.MonarchGroupDomain:
                            sitemapfile = "Sitemap/MonarchGroup.xml";
                            break;
                        default:
                            digital = "/Digital";
                            break;
                    }
                    sitemapfile = "http://"+Sitecore.Context.Site.HostName + "/"+ sitemapfile;

                    httpResponse.Write(@"User-agent: *
    Disallow: /NotIndexed");
                    if (!String.IsNullOrEmpty(digital)) {
                        httpResponse.Write(@"
    Disallow: " + digital );
                    }
                    httpResponse.Write(@"    
    Sitemap: " + sitemapfile);

                }
                else
                {
                    httpResponse.Write(@"User-agent: *
    Disallow: *");
                }
                
                HttpContext.Current.Response.End();
                args.AbortPipeline();
                return;
            }

            //only process if we don't have a context item resolved or if its schools
	    //the page schools doesn not correspond to the item schools under a community
            if ((args.Url.FilePath.ToLowerInvariant().EndsWith("schools") || args.Url.FilePath.ToLowerInvariant().EndsWith("promotions") || Sitecore.Context.Item == null) && args.Url.ItemPath.IsNotEmpty())
            {
                try
                {
		    //initiate within try 
                    var webSession = new TMSession();
                    var lowerCasePath = args.Url.ItemPath.ToLowerInvariant().TrimEnd("/");
                    var virtualUrlSegment = _virtualUrlSegments.Find(lowerCasePath.EndsWith);
                    if (virtualUrlSegment == "request-information")
                    {
                        Sitecore.Context.Item = GetItemFromPath(Sitecore.Context.Site.StartPath+"/Contact Us/Request Information");
                        var itemPath = lowerCasePath;
                        var tabUrlSegment = itemPath.LastNthSegment(2);
                        if (PageUrls.VirtualUrls.Contains(tabUrlSegment) || PageUrls.AdditionalCommunityPages.Contains(tabUrlSegment))
                        {
                            itemPath = itemPath.Replace(itemPath.LastNSegments(2), string.Empty);
                        }
                        else
                        {
                            itemPath = itemPath.Replace(itemPath.LastNthSegment(1), string.Empty);
                        }
                        Item parent = GetItemFromPath(itemPath);
                        if (parent != null)
                        {
                            webSession["rfiID"] = parent.ID.ToString();
                            webSession.Save();
                        }
                    }
                    else if (virtualUrlSegment == "make-an-appointment")
                    {
                        Sitecore.Context.Item = GetItemFromPath(Sitecore.Context.Site.StartPath+"/Contact Us/Make an Appointment");
                        var itemPath = args.Url.ItemPath.TrimEnd("/").Replace('-',' ');

                        string communityLegacyId = string.Empty;
                        bool underCommunity = false;
                        var splitVals = itemPath.Split('/');
                        string val = string.Empty;
                        var count = splitVals.Count() - 2;
                        if (count > 8)
                        {
                            underCommunity = true;
                            count = 8;
                        }
                            
                        for (int i = 1; i <= count; i++)
                        {
                            val += "/" + splitVals[i];
                        }

                        if (underCommunity)
                        {
                            communityLegacyId = GetItemFromPath(val + "/" + splitVals[9]).Fields[SCIDs.CommunityFields.CommunityLegacyID].Value;

                            if (communityLegacyId != string.Empty)
                            {
                                webSession["communityLegacyId"] = communityLegacyId;
                                webSession.Save();
                            }
                        }
                        else
                        {
                            webSession["communityLegacyId"] = null;
                            webSession.Save();
                        }

                        Sitecore.Data.ID itemID = GetItemFromPath(val).ID;

                        if (!itemID.IsNull)
                        {
                            webSession["parentID"] = itemID;
                            webSession.Save();
                        }

                        var subpage = false;

                        if (underCommunity)
                        {
                            if (splitVals.Contains("locations") || splitVals.Contains("floor plan")
                                || splitVals.Contains("homes ready now") || splitVals.Contains("floor plans")
                                || splitVals.Contains("schools") || splitVals.Contains("floorplans")
                                || splitVals.Contains("photos"))
                                subpage = true;
                        }

                        if (subpage)
                            itemPath = itemPath.Replace(itemPath.LastNthSegment(1), string.Empty).Replace(itemPath.LastNthSegment(2), string.Empty).TrimEnd("/");
                        else
                            itemPath = itemPath.Replace(itemPath.LastNthSegment(1), string.Empty);
                        Item parent = GetItemFromPath(itemPath);
                        if (parent != null)
                        {
                            webSession["rfiID"] = parent.ID.ToString();
                            webSession.Save();
                        }
                    }
                    else if(virtualUrlSegment=="thank-you")
                    {
                        var itemPath = lowerCasePath;
                        var lastButOneSegment = itemPath.LastNthSegment(2);
                        var tabUrlSegment = itemPath.LastNthSegment(3);
                        if (lowerCasePath.Contains("/signup/"))
                        {
                            //regular thank you page
                            Sitecore.Context.Item = GetItemFromPath(Sitecore.Context.Site.StartPath + "/Contact Us/SignUp Thank You");
                        }
                        else
                        {
                            //regular thank you page
                            Sitecore.Context.Item = GetItemFromPath(Sitecore.Context.Site.StartPath + "/Contact Us/Thank You");
                        }

                        if (lastButOneSegment == "request-information" || lastButOneSegment == "sign-up-for-updates" || lastButOneSegment == "make-an-appointment")
                        {
                            itemPath = itemPath.Replace(itemPath.LastNSegments(2), string.Empty);
                            if(PageUrls.VirtualUrls.Contains(tabUrlSegment))
                            {
                                itemPath = itemPath.Replace(itemPath.LastNSegments(1), string.Empty);
                            }
                        }
                        Item parent = GetItemFromPath(itemPath);
                        if (parent != null)
                        {
                            webSession["rfiID"] = parent.ID.ToString();
                            webSession.Save();
                        }
                       
                    }
                    else if (lowerCasePath.IsNotEmpty())
                    {
                        string firstPart = lowerCasePath.Substring(0, lowerCasePath.LastIndexOf('/'));
                        if (firstPart.Length > 0)
                        {
                            Item nextBestMatch = GetItemFromPath(firstPart);

                            if ((nextBestMatch != null) && (TM.Web.Custom.Constants.SCIDs.TemplateIds.CommunityPage == nextBestMatch.TemplateID 
								|| Constants.SCIDs.TemplateIds.PlanPage == nextBestMatch.TemplateID
								|| Constants.SCIDs.TemplateIds.HomeForSalePage == nextBestMatch.TemplateID
								|| Constants.SCIDs.TemplateIds.RealtorSearch == nextBestMatch.TemplateID
								))
                            {
                                Sitecore.Context.Item = nextBestMatch;
                            }
                        }
                    }
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Log.Warn("virtualrequestprocessor:" + args.Url, ex, typeof(VirtualRequestProcessor));

                }
            }
        }

        // if you have the full URL with protocol and host
        public static Item GetItemFromUrl(string rawUrl)
        {
            string path = new Uri(rawUrl).AbsolutePath;
            return GetItemFromPath(path);
        }

        // if you have just the path after the hostname
        public static Item GetItemFromPath(string path)
        {
            // remove query string
            if (path.Contains("?"))
                path = path.Split('?')[0];

            path = path.Replace(".aspx", "").Replace('-', ' ');
            try
            {
                return Sitecore.Context.Database.GetItem(path);
            }
            catch (NullReferenceException)
            {
                
            }
            return null;
        }
    }
}
