﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using TM.Utils.Web;

namespace TM.Web.Custom.Pipelines.RequestProcessor
{
    public class LowercaseUrlCheck : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            if (Sitecore.Context.Item == null)
                return;

            var requestUrl = HttpContext.Current.Request.RawUrl;
            var parts = requestUrl.Split('?');
            parts[0] = parts[0].ToLowerInvariant();
            var loweredUrl = String.Join("?",parts);
            
            //dont process sitecore content management urls
            if (loweredUrl.Contains("sitecore"))
                return;

            if (requestUrl == loweredUrl)
                return;

            HttpContext.Current.Response.Redirect(loweredUrl);
        }
    }
}
