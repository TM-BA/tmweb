﻿using System;
using System.Collections.Generic;
using Sitecore;
using Sitecore.Caching;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using Sitecore.Publishing.Pipelines.PublishItem;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.CrmIntegration;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Pipelines.PublishItem
{
    public class BeforeItemPublished : PublishItemProcessor
    {
        private static Dictionary<string, string> _statusImages;

        private Dictionary<string, string> statusImages
        {
            get
            {
                if (_statusImages == null)
                {
                    _statusImages = new Dictionary<string, string>();

                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.Closed] = "CommunityClosed.png";
                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.Closeout] = "CommunityCloseout.png";
                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.ComingSoon] = "CommunityComingSoon.png";
                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.GrandOpening] = "CommunityGrandOpening.png";
                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.Open] = "CommunityOpen.png";
                    _statusImages[SCIDs.CommunityFields.CommunityStatuses.PreSelling] = "CommunityPreSelling.png";
                }
                return _statusImages;
            }
        }


        public override void Process(PublishItemContext context)
        {
            string sourceItemName = string.Empty;
            try
            {
                Database targetDB = context.PublishOptions.TargetDatabase;
                Database sourceDB = context.PublishOptions.SourceDatabase;
		
                Item sourceItem = sourceDB.GetItem(context.ItemId);
                
                if (sourceItem == null)
                    return;
                sourceItemName = sourceItem.DisplayName;
                if (sourceItemName == "__Standard Values")
                    return;
                //is this a community
                if (sourceItem.TemplateID == new ID(CommunityItem.TemplateId))
                {
                    ChangeCommunityItemIcon(sourceItem);
                   var legacyID =  new CommunityIntegrater(sourceDB, targetDB).Integrate(sourceItem);
		   //if there is a new legacy id , might not be refreshed in sourceItem, so pass from func.
		   new AssetsIntegrater(sourceDB, targetDB).Integrate(sourceItem,legacyID);
                }
                if (sourceItem.TemplateID == new ID(DivisionPageItem.TemplateId))
                {
                    new DivisionIntegrater(sourceDB, targetDB).Integrate(sourceItem);
                }
                if (sourceItem.TemplateID == new ID(PlanItem.TemplateId))
                {
                    ChangePlanItemIcon(sourceItem);
                    var legacyID = new PlanIntegrater(sourceDB, targetDB).Integrate(sourceItem);
                    new AssetsIntegrater(sourceDB, targetDB).Integrate(sourceItem,legacyID);
                }
                if (sourceItem.TemplateID == new ID(HomeForSaleItem.TemplateId))
                {
                    var legacyID = new InventoryIntegrater(sourceDB, targetDB).Integrate(sourceItem);
                    new AssetsIntegrater(sourceDB, targetDB).Integrate(sourceItem,legacyID);
                }
                if (sourceItem.TemplateID == new ID(HomeFeaturesCategoryItem.TemplateId) ||
                    sourceItem.TemplateID == new ID(LocalInterestCategoryItem.TemplateId))
                {
                    new DivisionIntegrater(sourceDB, targetDB).ProcessDSOItems(sourceItem);
                    sourceItem.Reload();
                }

                if (sourceItem.TemplateID == new ID(SchoolTypeItem.TemplateId) ||
                    sourceItem.TemplateID == new ID(SchoolInfoItem.TemplateId) ||
                    sourceItem.TemplateID == new ID(LocalInterestItem.TemplateId) ||
                    sourceItem.TemplateID == new ID(HomeFeaturesItem.TemplateId)
                    )
                {
                    new CommunityIntegrater(sourceDB, targetDB).ProcessDSOItems(sourceItem);
                }

                RefreshCache(sourceItem, sourceDB);
            }
            catch (Exception e)
            {
                Log.Error("CRM Integration Error", e, context);


                string message =
                    sourceItemName +
                    " was not published!. There was an error while sending data to crm integration tables.  Please ensure required fields are entered and is correct and try again. More technical info about the error:\n" +
                    e.Message.Substring(0, Math.Min(e.Message.Length,100)) + "...";

                context.AddMessage(message);
                context.PublishContext.AddMessage(message, PipelineMessageType.Error);
                context.Job.Status.Messages.Add(message);
                context.AbortPipeline();
                context.PublishContext.AbortPipeline();
            }
        }

        public void ChangePlanItemIcon(Item currentItem)
        {
            string newTimeDate = DateTime.Now.ToString("yyyyMMddTHHmmss");

            try
            {
                using (new EventDisabler())
                using (new EditContext(currentItem))
                {
                  

                    if (currentItem.Fields["Plan Status"].Value == SCIDs.StatusIds.Status.Active)
                    {
                        currentItem.Fields["__icon"].Value = "home_green.png";
                    }
                    else {
                        currentItem.Fields["__icon"].Value = "home_green_d.png";
                    }
               }
		
            }
            catch (Exception ex)
            {
                Log.Error("error in recorpublishing newdate:" + newTimeDate, ex, currentItem);
            }


           
        }
        public void ChangeCommunityItemIcon(Item currentItem)
        {
            string newTimeDate = DateTime.Now.ToString("yyyyMMddTHHmmss");

            try
            {
                using (new EventDisabler())
                using (new EditContext(currentItem))
                {
                    currentItem.Fields["__icon"].Value =
                        statusImages[currentItem[SCIDs.CommunityFields.CommunityStatusField]];
                    currentItem[SCIDs.CommunityFields.CommunityStatusChangeDateField] = newTimeDate;
                }
             
            }
            catch (Exception ex)
            {
                Log.Error("error in recorpublishing newdate:" + newTimeDate, ex, currentItem);
            }


            //update Community Lat Long Cache
            string latLongKey = currentItem.ID.ToString();
	    //context not available
           /* if (SCUtils.AllCommunityLatLongs != null)
            {
                SCUtils.AllCommunityLatLongs.Remove(latLongKey);
                var commLatLong = new CommunityLatLong(latLongKey,
                                                       currentItem[SCIDs.CommunityFields.Lattitude].CastAs<float>(0),
                                                       currentItem[SCIDs.CommunityFields.Longitude].CastAs<float>(0));
                SCUtils.AllCommunityLatLongs.Add(latLongKey, commLatLong);
            }*/
        }

        /// <summary>
        /// Refreshes the screen.
        /// </summary>
        /// <param name="currentItem">The current item.</param>
        /// <param name="sourceDB"> </param>
        private void RefreshCache(Item currentItem, Database sourceDB)
        {

            if (!currentItem.Editing.IsEditing)
                    {
                        if (sourceDB != null)
                        {
                            sourceDB.Caches.DataCache.RemoveItemInformation(currentItem.ID);
                            sourceDB.Caches.ItemCache.RemoveItem(currentItem.ID);
			    sourceDB.Caches.ItemPathsCache.InvalidateCache(currentItem.ID);
			    sourceDB.Caches.PathCache.RemoveMappingsContaining(currentItem.ID);
			    sourceDB.Caches.StandardValuesCache.RemoveKeysContaining(currentItem.ID.ToString());

        
                         
                        }
                      
                 }

	
            
        }

    }
}