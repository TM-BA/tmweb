﻿using System;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;
using TM.Utils.Extensions;
using System.Collections.Generic;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Pipelines.RequestProcessor;

namespace TM.Web.Custom.Pipelines.PublishItem
{
    public class RecordPublishing
    {

        //for recording the time a status changed
        //use PublishRelatedItems from lennar as a template
        //may want to include PublishRelatedItems in this project as well
        //private static Dictionary<string, string> _statusImages;
        //private Dictionary<string,string> statusImages
        //{
        //    get
        //    {
        //        if (_statusImages == null)
        //        {
        //            _statusImages = new Dictionary<string, string>();

        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.Closed] = "CommunityClosed.png";
        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.Closeout] = "CommunityCloseout.png";
        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.ComingSoon] = "CommunityComingSoon.png";
        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.GrandOpening] = "CommunityGrandOpening.png";
        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.Open] = "CommunityOpen.png";
        //            _statusImages[SCIDs.CommunityFields.CommunityStatuses.PreSelling] = "CommunityPreSelling.png";
        //        }
        //        return _statusImages;
        //    }
        //}


        public void OnStartPublishing(object sender, EventArgs args)
        {
            //Item currentItem = ((Sitecore.Publishing.Publisher)(((Sitecore.Events.SitecoreEventArgs)(args)).Parameters[0])).Options.RootItem;
            //if (currentItem == null)
            //    return;

            ////is this a community
            //if (currentItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
            //{
            //    //does status have a value
            //    //if (string.IsNullOrWhiteSpace(currentItem[TM.Web.UserControls.Constants.SCIDs.CommunityFields.CommunityStatusField]))
            //    //{
            //    //    currentItem[TM.Web.UserControls.Constants.SCIDs.CommunityFields.CommunityStatusChangeDateField] = string.Empty;
            //    //    return;
            //    //}

            //    Item webItem = Sitecore.Data.Database.GetDatabase("web").GetItem(currentItem.ID);

            //    if (webItem != null)
            //    {
            //        if (currentItem[SCIDs.CommunityFields.CommunityStatusField]
            //        != webItem[SCIDs.CommunityFields.CommunityStatusField])
            //        {
            //            string newTimeDate = DateTime.Now.ToString("yyyyMMddTHHmmss");
            //            using (new Sitecore.Data.Items.EditContext(currentItem))
            //            {
            //                try
            //                {
            //                    currentItem.Fields["__icon"].Value = statusImages[currentItem[SCIDs.CommunityFields.CommunityStatusField]];
            //                }
            //                catch(Exception ex){
            //                    Log.Error("error in recorpublishing newdate:" + newTimeDate, ex, typeof(RecordPublishing));
            //                }
            //                currentItem[SCIDs.CommunityFields.CommunityStatusChangeDateField] = newTimeDate;
            //            }
            //            using (new Sitecore.Data.Items.EditContext(webItem))
            //            {
            //                try
            //                {
            //                    webItem.Fields["__icon"].Value = statusImages[currentItem[SCIDs.CommunityFields.CommunityStatusField]];
            //                }
            //                catch (Exception ex)
            //                {
            //                    Log.Error("error in recorpublishing newdate:" + newTimeDate, ex, typeof(RecordPublishing));
            //                }
            //                webItem[SCIDs.CommunityFields.CommunityStatusChangeDateField] = newTimeDate;
            //            }
            //        }
            //        //update Community Lat Long Cache
            //        //todo may not work in a multi host environment
            //        if (TM.Web.Custom.SCHelpers.SCUtils.AllCommunityLatLongs.ContainsKey(currentItem.ID.ToString()))
            //        {
            //            if (currentItem[SCIDs.CommunityFields.Lattitude] != webItem[SCIDs.CommunityFields.Lattitude])
            //            {
            //                TM.Web.Custom.SCHelpers.SCUtils.AllCommunityLatLongs[currentItem.ID.ToString()].Lattitude = currentItem[SCIDs.CommunityFields.Lattitude].CastAs<float>(0);
            //            }
            //            if (currentItem[SCIDs.CommunityFields.Longitude] != webItem[SCIDs.CommunityFields.Longitude])
            //            {
            //                TM.Web.Custom.SCHelpers.SCUtils.AllCommunityLatLongs[currentItem.ID.ToString()].Longitude = currentItem[SCIDs.CommunityFields.Longitude].CastAs<float>(0);
            //            }
            //        }
            //        else
            //        {
            //            TM.Web.Custom.SCHelpers.SCUtils.AllCommunityLatLongs.Add(currentItem.ID.ToString(), new WebControls.CommunityLatLong(currentItem.ID.ToString(), currentItem[SCIDs.CommunityFields.Lattitude].CastAs<float>(0), currentItem[SCIDs.CommunityFields.Longitude].CastAs<float>(0)));
            //        }
            //    }
            //    else
            //    {
            //        string newTimeDate = DateTime.Now.ToString("yyyyMMddTHHmmss");
            //        using (new Sitecore.Data.Items.EditContext(currentItem))
            //        {
            //                try
            //                {
            //                    currentItem.Fields["__icon"].Value = statusImages[currentItem[SCIDs.CommunityFields.CommunityStatusField]];
            //                }
            //                catch(Exception ex){
            //                    Log.Error("error in recorpublishing newdate:" + newTimeDate, ex, typeof(RecordPublishing));
            //                }
            //            currentItem[SCIDs.CommunityFields.CommunityStatusChangeDateField] = newTimeDate;
            //        }
            //    }
            //}
        }
    }
}