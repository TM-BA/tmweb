﻿//-----------------------------------------------------------------------------------
// <copyright file="ReplaceInvalidCharacters.cs" company="Sitecore Shared Source">
// Copyright (c) Sitecore.  All rights reserved.
// </copyright>
// <summary>
// Defines the 
// Sitecore.Sharedsource.Rules.Actions.Naming.ReplaceInvalidCharacters 
// type.
// </summary>
// <license>
// http://sdn.sitecore.net/Resources/Shared%20Source/Shared%20Source%20License.aspx
// </license>
// <url>http://trac.sitecore.net/ItemNamingRules/</url>
//-----------------------------------------------------------------------------------

using System.Text.RegularExpressions;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using System;
namespace TM.Web.Custom.SitecoreRules.Actions.Naming
{

    /// <summary>
  /// Rules engine action to replace invalid characters in item names.
  /// </summary>
  /// <typeparam name="T">Type providing rule context.</typeparam>
  public class RenameInventory<T> :
    RenamingAction<T>
    where T : Sitecore.Rules.RuleContext
  {

      public string BeginsWith 
      {
          get;
          set;
      }

    /// <summary>
    /// Action implementation.
    /// </summary>
    /// <param name="ruleContext">The rule context.</param>
    public override void Apply(T ruleContext)
    {
       
        var inventoryItem = ruleContext.Item;

       var prefix = BeginsWith.IsEmpty() ? "Home Available Now at" : BeginsWith;
       var streetAddress = inventoryItem[SCIDs.HomesForSalesFields.StreetAddress1];
       var nonAlpha = new Regex(@"\W");
       streetAddress = nonAlpha.Replace(streetAddress, " ");
       var multiSpace = new Regex(@"\s{2,}");//multispace
       streetAddress = multiSpace.Replace(streetAddress, " ");
       var zipCode = inventoryItem.Parent.Parent["Zip or Postal Code"];
       string newName = string.Format("{0} {1} {2}", prefix, streetAddress, zipCode);

      if ( zipCode.IsNotEmpty() && streetAddress.IsNotEmpty() && ruleContext.Item.Name != newName )
      {
        this.RenameItem(ruleContext.Item, newName);
      }
    }
  }
}