﻿//-----------------------------------------------------------------------------------
// <copyright file="ReplaceInvalidCharacters.cs" company="Sitecore Shared Source">
// Copyright (c) Sitecore.  All rights reserved.
// </copyright>
// <summary>
// Defines the 
// Sitecore.Sharedsource.Rules.Actions.Naming.ReplaceInvalidCharacters 
// type.
// </summary>
// <license>
// http://sdn.sitecore.net/Resources/Shared%20Source/Shared%20Source%20License.aspx
// </license>
// <url>http://trac.sitecore.net/ItemNamingRules/</url>
//-----------------------------------------------------------------------------------

namespace TM.Web.Custom.SitecoreRules.Actions.Naming
{
  using System;
  using System.Text.RegularExpressions;

  /// <summary>
  /// Rules engine action to replace invalid characters in item names.
  /// </summary>
  /// <typeparam name="T">Type providing rule context.</typeparam>
  public class EndsWith<T> :
    TM.Web.Custom.SitecoreRules.Actions.Naming.RenamingAction<T>
    where T : Sitecore.Rules.RuleContext
  {
    /// <summary>
    /// Gets or sets the string with which to end item names.
    /// </summary>
    public string EndWith
    {
      get; 
      set;
    }

    /// <summary>
    /// Action implementation.
    /// </summary>
    /// <param name="ruleContext">The rule context.</param>
    public override void Apply(T ruleContext)
    {
        Sitecore.Diagnostics.Assert.IsNotNull(this.EndWith, "EndWith");
      Regex patternMatcher = new Regex(".+" +this.EndWith);
      string newName = String.Empty;
      
        if (patternMatcher.IsMatch(ruleContext.Item.Name))
        {
            newName = ruleContext.Item.Name;
        }
        else if (!String.IsNullOrEmpty(this.EndWith))
        {
            newName = ruleContext.Item.Name + this.EndWith;
        }

      if (ruleContext.Item.Name != newName)
      {
        this.RenameItem(ruleContext.Item, newName);
      }
    }
  }
}