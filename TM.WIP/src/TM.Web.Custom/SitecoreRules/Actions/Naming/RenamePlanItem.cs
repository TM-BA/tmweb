﻿//-----------------------------------------------------------------------------------
// <copyright file="ReplaceInvalidCharacters.cs" company="Sitecore Shared Source">
// Copyright (c) Sitecore.  All rights reserved.
// </copyright>
// <summary>
// Defines the 
// Sitecore.Sharedsource.Rules.Actions.Naming.ReplaceInvalidCharacters 
// type.
// </summary>
// <license>
// http://sdn.sitecore.net/Resources/Shared%20Source/Shared%20Source%20License.aspx
// </license>
// <url>http://trac.sitecore.net/ItemNamingRules/</url>
//-----------------------------------------------------------------------------------

using System.Text.RegularExpressions;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using System;
namespace TM.Web.Custom.SitecoreRules.Actions.Naming
{

    /// <summary>
  /// Rules engine action to rename plan item
  /// </summary>
  /// <typeparam name="T">Type providing rule context.</typeparam>
  public class RenamePlanItem<T> :
    RenamingAction<T>
    where T : Sitecore.Rules.RuleContext
  {

     

    /// <summary>
    /// Action implementation.
    /// </summary>
    /// <param name="ruleContext">The rule context.</param>
    public override void Apply(T ruleContext)
    {
      
        
        var planItem = ruleContext.Item;

        var planName = planItem[SCIDs.PlanFields.PlanName];
     

      if ( planName.IsNotEmpty() &&  !ruleContext.Item.Name.Contains(planName) )
      {
          if(!planName.EndsWith(" Plan"))
          {
              planName = planName + " Plan";
          }

          var nonAlpha = new Regex(@"\W");
          planName = nonAlpha.Replace(planName, " ");
          var multiSpace = new Regex(@"\s{2,}");//multispace
          planName = multiSpace.Replace(planName, " ");
	  this.RenameItem(ruleContext.Item, planName, true);
          
      }
    }
  }
}