﻿using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Rules;
using Sitecore.Rules.Conditions;

namespace TM.Web.Custom.SitecoreRules
{
    public class QueryStringCondition<T> : StringOperatorCondition<T> where T : RuleContext
    {
        //Properties
        public string QueryStringName { get; set; }
        public string QueryStringValue { get; set; }

        //Methods
        protected override bool Execute(T ruleContext)
        {
            bool returnValue;
            bool foundExactMatch = false;
            bool foundCaseInsensitiveMatch = false;
            bool foundContains = false;
            bool foundStartsWith = false;
            bool foundEndsWith = false;

            Assert.ArgumentNotNull(ruleContext, "ruleContext");

            string myQueryStringName = QueryStringName ?? string.Empty;
            string myQueryStringValue = QueryStringValue ?? string.Empty;
                //Populated with Value selected in Sitecore Rule by Content Author

            if (!string.IsNullOrWhiteSpace(myQueryStringName))
            {
                if (HttpContext.Current != null)
                {
                    //Populated with QueryString coming into current Page
                    string incomingQueryStringValue = HttpContext.Current.Request.QueryString[myQueryStringName] ??
                                                      string.Empty;

                    if (incomingQueryStringValue == myQueryStringValue)
                    {
                        //Indicates that QueryString coming into Page is equal to QueryString selected by Content Author
                        foundExactMatch = true;
                        foundCaseInsensitiveMatch = true;
                        foundContains = true;
                        foundStartsWith = true;
                        foundEndsWith = true;
                    }
                    else if (incomingQueryStringValue.ToLower() == myQueryStringValue.ToLower())
                    {
                        //Indicates that QueryString coming into Page has case-insensitive match to QueryString selected by Content Author
                        foundCaseInsensitiveMatch = true;
                        //Check other "Found" variables that are not inherently true
                        if (incomingQueryStringValue.Contains(myQueryStringValue))
                        {
                            foundContains = true;
                        }
                        if (incomingQueryStringValue.StartsWith(myQueryStringValue))
                        {
                            foundStartsWith = true;
                        }
                        if (incomingQueryStringValue.EndsWith(myQueryStringValue))
                        {
                            foundEndsWith = true;
                        }
                    }
                    else if (incomingQueryStringValue.Contains(myQueryStringValue))
                    {
                        //Indicates that QueryString coming into Page contains QueryString selected by Content Author
                        foundContains = true;
                        //Check other "Found" variables that are not inherently true
                        if (incomingQueryStringValue.StartsWith(myQueryStringValue))
                        {
                            foundStartsWith = true;
                        }
                        if (incomingQueryStringValue.EndsWith(myQueryStringValue))
                        {
                            foundEndsWith = true;
                        }
                    }
                }
            }

            switch (GetOperator())
            {
                case StringConditionOperator.Equals:
                    returnValue = foundExactMatch;
                    break;
                case StringConditionOperator.NotEqual:
                    returnValue = !foundExactMatch;
                    break;
                case StringConditionOperator.CaseInsensitivelyEquals:
                    returnValue = foundCaseInsensitiveMatch;
                    break;
                case StringConditionOperator.NotCaseInsensitivelyEquals:
                    returnValue = !foundCaseInsensitiveMatch;
                    break;
                case StringConditionOperator.Contains:
                    returnValue = foundContains;
                    break;
                case StringConditionOperator.StartsWith:
                    returnValue = foundStartsWith;
                    break;
                case StringConditionOperator.EndsWith:
                    returnValue = foundEndsWith;
                    break;
                default:
                    returnValue = false;
                    break;
            }

            return returnValue;
        }
    }
}