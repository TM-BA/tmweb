﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Layouts
{
    public partial class HTM_Main : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = PageTitle;
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccount) ?
                                          "UA-37702556-1" :
                                           Config.Settings.GoogleAnalyticsAccount;

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            if (companyLogo != null && companyLogo.MediaItem != null)
            {
                imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
                imgSiteLogo.AlternateText = companyLogo.Alt;
            }

            LinkField youtubelink = CurrentHomeItem.Fields["YouTube URL"];
			//if url is new homes - redirect to home page
			var item = SCContextDB.GetItem(SCContextItem.Paths.FullPath);
			if (item.TemplateID == SCIDs.TemplateIds.ProductTypePage)
				Response.Redirect("/");
            var contextItem = SCContextItem;
            if (contextItem.Fields["Tracking Code"] != null)
            {
                yahooPixels.Text = contextItem.Fields["Tracking Code"].Value;
            }
            else { yahooPixels.Visible = false; }
        }
    }
}