﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Domain;
using TM.Web.Custom.SCHelpers;
using System.Text;
using System.IO;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.LuceneQueries;
using Sitecore.Data.Items;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class UniversalRFIThankYou : ControlBase
    {
        private Sitecore.Data.Items.Item realContextItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            realContextItem = Sitecore.Context.Item;
            if (CurrentPage.WebSession["rfiID"] != null)
            {
                var proposedContextItem = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(CurrentPage.WebSession["rfiID"].ToString()));
                while (
                    (proposedContextItem.TemplateID == SCIDs.TemplateIds.UniversalThankyou)
                    || (proposedContextItem.TemplateID == SCIDs.TemplateIds.UniversalRFIPage)
                    || (proposedContextItem.TemplateID == SCIDs.TemplateIds.ContactUsPage))
                {
                    proposedContextItem = proposedContextItem.Parent;
                }
                Sitecore.Context.Item = proposedContextItem;
            }
            
        }

        public string InterestItemGALabel
        {
            get
            {
                //If you need a different GA Label Finish out this switchStatement
                switch (Sitecore.Context.Item.TemplateID.ToString())
                {
                    case SCIDs.TemplateIds.SwitchCommunityPage:
                        return "CommunityLink";
                }
                return "CommunityLink";

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] == null)
            {
                CurrentPage.Title = "Thank You - " + CurrentPage.PageTitle;
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Thank You", Sitecore.Context.Item));
            }

            if (CurrentPage.CurrentWebUser != null)
            {
                pnlCreateAccount.Visible = false;
            }

            litPrivacyPolicy.Text = @"<p>{0}</p>".FormatWith(realContextItem["New Account Privacy"]);
            if (CurrentPage.WebSession["parentID"] != null)
            {
                string legacyId = string.Empty;
                Item community = null;
                if (WebSession["communityLegacyId"] != null)
                {
                    legacyId = CurrentPage.WebSession["communityLegacyId"].ToString();
                    CommunityQueries comm = new CommunityQueries();
                    community = comm.GetCommunityFromLegacyId(legacyId);
                }
                string date = CurrentPage.WebSession["WFFMDate of Appointment*"] != null ? CurrentPage.WebSession["WFFMDate of Appointment*"].ToString() : CurrentPage.WebSession["WFFMDateofVisit"].ToString();
                string time = CurrentPage.WebSession["WFFMTime of Visit*"] != null ? CurrentPage.WebSession["WFFMTime of Visit*"].ToString() : CurrentPage.WebSession["WFFMTimeofVisit"].ToString();
                string dateTime = String.Format("{0:MM/dd/yyyy}", DateTime.ParseExact(date, "yyyyMMddTHHmmss", null)) + " at  " + time;

                string domain = Sitecore.Context.GetDeviceName();
                string companyName = domain.GetCurrentCompanyName();

                if (SCContextItem.TemplateID == SCIDs.TemplateIds.StatePage ||
                    SCContextItem.TemplateID == SCIDs.TemplateIds.CityPage ||
                    SCContextItem.TemplateID == SCIDs.TemplateIds.DivisionPage)
                {
                    litThankYouText.Text = @"<p>Thank you for requesting an appointment with {0}.  The Internet Home Consultant will confirm your appointment as quickly as possible.  You requested {1}.  We look forward to seeing you at our community soon.</p>".FormatWith(
                    companyName,
                    dateTime
                    );
                }
                else if (SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage ||
                    SCContextItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
                {
                    litThankYouText.Text = @"<p>Thank you for requesting an appointment at <a href=""{0}"" class=""InterestItem"">{1}</a>.  The Internet Home Consultant will confirm your appointment as quickly as possible.  You requested {2}.  We look forward to seeing you at <a href=""{0}"" class=""InterestItem"">{1}</a> soon.</p>".FormatWith(
                    community.GetItemUrl(),
                    community.GetItemName(),
                    dateTime
                    );
                }
                else
                {
                    litThankYouText.Text = @"<p>Thank you for requesting an appointment at <a href=""{0}"" class=""InterestItem"">{1}</a>.  The Internet Home Consultant will confirm your appointment as quickly as possible.  You requested {2}.  We look forward to seeing you at <a href=""{3}"" class=""InterestItem"">{4}</a> soon.</p>".FormatWith(
                    SCContextItem.GetItemUrl(),
                    SCContextItem.GetItemName(),
                    dateTime,
                    community.GetItemUrl(),
                    community.GetItemName()
                    ); 
                }          
            }
            else 
                if (CurrentPage.WebSession["rfiID"] != null)
            {
                litThankYouText.Text = @"<p>Thank you for your interest. Your request has been submitted and you will hear from us soon.</p>";
            }
            else
            {
                litThankYouText.Text = @"<p>Thank you for your interest in <a href=""{0}"" class=""InterestItem"">{1}</a>. Your request has been submitted and you will hear from us soon.</p>".FormatWith(
                SCContextItem.GetItemUrl(),
                SCContextItem.GetItemName()
                );
            }

            

            if(!string.IsNullOrWhiteSpace(CurrentPage.CurrentHomeItem["Financing Call to Action"]) ){
                litPreQual.Text = @"<p>Ready to get pre-qualified? Start the <a href=""{0}"">pre-qualification process</a>. </p>".FormatWith(
                    CurrentPage.CurrentHomeItem["Financing Call to Action"]
                    );
            }


            if(CurrentPage.FacebookUrl.IsNotEmpty() ){
                litSocialMedia.Text = @"<p>Visit our <a href=""{0}"">Facebook</a>".FormatWith(
                    CurrentPage.FacebookUrl
                    );
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @" and <a href=""{0}"">Twitter</a> pages.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
                else
                    litSocialMedia.Text += " page.</p>";
            } else{
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @"<p>Visit our <a href=""{0}""><img src=""/images/tm/iconTwitter.png"" class=""tySocialIcn tw"">Twitter</a> page.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
            }


            if (CurrentPage.CurrentIHC != null)
            {
                litEmailAdress.Text = @"<p>Want to make sure our emails get to your inbox? Be sure to include <a href='mailto:{0}'>{0}</a> in your email address book.</p>".FormatWith(
                    CurrentPage.CurrentIHC["Email Address"]); 

                litIhc.Text = @"<p>Questions? Call Internet Home Consultant, {0} {1} {3} at: <a href='tel:{2}'>{2}.</a></p>".FormatWith(
                    CurrentPage.CurrentIHC["First Name"],
                        CurrentPage.CurrentIHC["Last Name"],
                        CurrentPage.CurrentIHC["Phone"],
                        CurrentPage.CurrentIHC["Regulating Authority Designation"].IsNotEmpty() ? "(" + CurrentPage.CurrentIHC["Regulating Authority Designation"] + ")" : string.Empty
                         );
            }

            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem["Blog Url"]))
                litBlogLink.Text = "<p>Have you seen our <a href='{0}'>Blog</a>? Visit <a href='{1}'>Our Blog</a> for the latest in home trends, technology, and more.</p>".FormatWith(CurrentPage.BlogURL,CurrentPage.BlogURL);


            //Universal Thank You Next Steps
            litNextSteps.Text = realContextItem["Next Steps"];

            lnkBtnCreateAccount.Click += new EventHandler(btnCreateAccount_Click);
        }

        void btnCreateAccount_Click(object sender, EventArgs e)
        {
            pnlCreateAccount.Visible = false;
            var device = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

			var newUser = new TMUser(TMUserDomain.TaylorMorrison, WebSession["WFFMtxtEmail"].CastAs<string>())
				{
                    FirstName = WebSession["WFFMtxtFirstName"].CastAs<string>(),
                    LastName = WebSession["WFFMtxtLastName"].CastAs<string>(),
                    Email = WebSession["WFFMtxtEmail"].CastAs<string>(),
					Password = txtPassword.Text,
					AreaOfInterest = CurrentPage.CurrentDivision.DisplayName,
					Device = device
				};

			CurrentWebUser = null;


            if (newUser.Create(GetCreateAccountTemplate()))
            {
                litAccountCreated.Text = "<p>Account created successfully</p>";
            }
            else
            {
                litAccountCreated.Text = "<p>User account already exists. Please <a href=\"/Account/Login\">Login</a> here.</p>";
            }
        }

        private string GetCreateAccountTemplate()
        {
            // Declare stringbuilder to render control to
            StringBuilder sb = new StringBuilder();

            // Load the control
            UserControl ctrl = (UserControl)LoadControl("/tmwebcustom/SubLayouts/Users/WelcomeEmail.ascx");

            // Do stuff with ctrl here

            // Render the control into the stringbuilder
            StringWriter sw = new StringWriter(sb);
            Html32TextWriter htw = new Html32TextWriter(sw);
            ctrl.RenderControl(htw);

            // Get full body text
            return sb.ToString();
        }
    }
}