﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;
using System.Web.UI.HtmlControls;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public partial class MakeAnAppointment : ControlBase
    {
        private string _longTitle;
        public string LongTitle
        {
            get
            {
                return _longTitle;
            }
            set
            {
                _longTitle = value;
            }
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            Sitecore.Context.Item = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(CurrentPage.WebSession["rfiID"].ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var title = CurrentPage.GetTagsReplaced(SCContextItem["Make an Appointment Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
            
            if (!string.IsNullOrWhiteSpace(title))
                CurrentPage.Title = title;
            else
                CurrentPage.Title = "Make an Appointment - " + CurrentPage.PageTitle;

            var description = CurrentPage.GetTagsReplaced(SCContextItem["Make an Appointment Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
            if (!string.IsNullOrWhiteSpace(description))
                CurrentPage.MetaDescription = description;
            
            int count = 0;

            if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.CommunityPage)
                count = 7;
            else if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.PlanPage)
                count = 8;
            else if (Sitecore.Context.Item.TemplateID == SCIDs.TemplateIds.HomeForSalePage)
                count = 9;
            else {
                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Make an Appointment", CurrentPage.CurrentCommunity));
            }

            if (count > 0)
            {
                HtmlLink canonical = new HtmlLink();
                canonical.Attributes.Add("rel", "canonical");
                string finalUrl = string.Empty;
                var splitVals = Request.Url.AbsoluteUri.TrimEnd('/').Split('/');
                finalUrl += splitVals[0];
                for (int i = 1; i <= count; i++)
                {
                    finalUrl += "/" + splitVals[i];
                }
                canonical.Href = finalUrl + "/make-an-appointment";
                CurrentPage.FindControl("phHeadSection").Controls.Add(canonical);
            }                        
        }
    }
}
