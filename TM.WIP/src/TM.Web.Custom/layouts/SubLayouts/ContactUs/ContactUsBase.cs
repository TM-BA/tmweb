﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;
using SCID = Sitecore.Data.ID;

namespace TM.Web.Custom.Layouts.SubLayouts.ContactUs
{
    public class ContactUsBase : ControlBase
    {
        public List<DivisionInfo> PopulateLocalAreas()
        {
            DivisionInfo divisionInfo;
            var divisionList = new List<DivisionInfo>();


            // Find all division pages Items
            MultilistField selectedDivisions = SCContextItem.Fields[SCIDs.ContactUsPage.DivisionsToDisplay];


            Item[] divisionTemplates = selectedDivisions.GetItems();

            foreach (Item division in divisionTemplates)
            {
                divisionInfo = new DivisionInfo();

                divisionInfo.DivisionId = Guid.Parse(division.ID.ToString());


                //if (division.Fields["Division Name"] != null && productType.Fields["Product Type Name"] != null)
                //    divisionInfo.Name = productType.Fields["Product Type Name"].ToString() + " " + division.Fields["Division Name"].ToString();

                if (division.Fields["Division Name"] != null)
                    divisionInfo.Name = division.Fields["Division Name"].ToString().Replace("Morrison", "");

                if (division.Fields["Street Address 1"] != null)
                    divisionInfo.StreetAddress1 = division.Fields["Street Address 1"].ToString();

                if (division.Fields["Street Address 2"] != null)
                    divisionInfo.StreetAddress2 = division.Fields["Street Address 2"].ToString();

                if (division.Fields["City"] != null)
                    divisionInfo.City = division.Fields["City"].ToString();

                if (division.Fields["State or Province Abbreviation"] != null)
                    divisionInfo.StateProvinceAbbreviation =
                        division.Fields["State or Province Abbreviation"].ToString();

                if (division.Fields["Zip or Postal Code"] != null)
                    divisionInfo.ZipPostalCode = division.Fields["Zip or Postal Code"].ToString();

                if (division.Fields["Phone"] != null)
                    divisionInfo.Phone = division.Fields["Phone"].ToString();

                string currentUrl = Request.Url.AbsoluteUri;

                if (currentUrl.ToLower().Contains("submit-warranty-request"))
                {
                    if (division.Fields["Warranty Line"] != null)
                        divisionInfo.WarrantyLine = division.Fields["Warranty Line"].ToString();

                    if (division.Fields["After Hours Emergency"] != null)
                        divisionInfo.AfterHoursEmergency = division.Fields["After Hours Emergency"].ToString();
                }
                var fullAddress = new StringBuilder();
                var cityState = new StringBuilder();

                if (!string.IsNullOrEmpty(divisionInfo.StreetAddress1))
                    fullAddress.Append(divisionInfo.StreetAddress1);

                if (!string.IsNullOrEmpty(divisionInfo.StreetAddress2))
                    fullAddress.Append(", " + divisionInfo.StreetAddress2);
                fullAddress.AppendLine();

                if (!string.IsNullOrEmpty(divisionInfo.City))
                    cityState.Append(" " + divisionInfo.City + ", ");

                if (!string.IsNullOrEmpty(division.Parent.GetItemName(true)))
                    cityState.Append(division.Parent.GetItemName(true) + " ");

                if (!string.IsNullOrEmpty(divisionInfo.ZipPostalCode))
                    cityState.Append(divisionInfo.ZipPostalCode);

                divisionInfo.CityState = cityState.ToString();

                divisionInfo.FullAddress = fullAddress.ToString();

                divisionInfo.Url = SCUtils.GetItemUrl(division).Replace(SCCurrentHomePath, "");

                divisionList.Add(divisionInfo);
            }


          
            return divisionList;
        }


        private static string StateCheck(DivisionInfo x)
        {
            try
            {
                var split = x.CityState.Split(' ');
                if (split.Length >= 3)
                {
                    if (split[2].Length == 2)
                    {
                        return split[2];
                    }
                    return "";
                }
                return "";
            }
            catch (Exception e)
            {
                Log.Error("Error While Sorting Division List in Contact US Page", e);
                return "";
            }
        }

        public List<string> PopulateYearList()
        {
            var years = new List<string>();

            for (int i = 2006; i <= DateTime.Now.Year; i++)
            {
                years.Add(i.ToString());
            }

            return years;
        }

        //public List<string> PopulateMonthList(string year)
        //{
        //    List<string> months = new List<string>();

        //    if (year == DateTime.Now.Year.ToString())
        //    {
        //        for (int i = 0; i == DateTime.Now.Month; i++)
        //        {
        //            months.Add(Constants.StatesList.Months[i]);
        //        }
        //    }
        //    else { months = Constants.StatesList.Months.ToList(); }

        //    return months;
        //}
    }
}