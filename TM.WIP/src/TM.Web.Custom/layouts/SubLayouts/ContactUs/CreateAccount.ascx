﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.CreateAccount" %>
		
<div class="gr-bk">
   <p>Thank You </p>        
</div>

<div class="create-acc">
       
        <p><sc:fieldrenderer runat="server" id="HeaderContent" fieldname="Header Content"></sc:fieldrenderer></p>
        
        <div class="next-steps">
        <h1>Next Steps</h1>
        
        <asp:Panel ID="pnlAcccount" runat="server" Visible="false">   
                    <asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx" EnableClientScript="True"
		ShowSummary="True" DisplayMode="SingleParagraph" Enabled="False" HeaderText=""
		ShowMessageBox="False" />

            <p>Provide a password to save communities and homes to your favorites.<br>
            * required information</p>


            <span> Password *</span>
            <asp:TextBox ID="txtPassword" runat="server" class="tm-account"></asp:TextBox>
            <span> Confirm Password *</span>
            <asp:TextBox ID="txtConfirmPassword" runat="server" class="tm-account"></asp:TextBox>

	
            <asp:Button ID="btnCreateAccount" runat="server" Text="" class="create-acc-btn" 
                    onclick="btnCreateAccount_Click" />

       

            <div class="th-bt">
            

            <p> We take your privacy seriously. Please read our 
               <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
               { %>
                <a href="/Privacy-Policy">Privacy Policy</a>
              <% } else { %>
               <a href="/Privacy-Policies">Privacy Policy</a>
               <% } %> 
            </p>
        
            </div> 
        </asp:Panel> 
        <sc:fieldrenderer runat="server" id="FooterContent" fieldname="Footer Content"></sc:fieldrenderer>
        </div>

         

        <div class="ways-connect">
        <h1>More Ways to Connect</h1>
        <p> Visit our <a href="#"><img src="images/iconFb.png" border="0"></a> Facebook and <a href="#"><img src="images/iconTwitter.png" border="0"></a>Twitter pages.</p>
        <p> Questions? Call Internet Home Consultant, Brandon Cleveland, at 480-346-1738.</p>
        <blockquote>
          <p> Have you seen our blog? Visit our <a href="#">blog</a> for the latest in home trends, technology and more.</p></blockquote>
        
        </div>
</div>
   
