﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.ContactUs" %>



<div runat="server" id="headerTitle" class="gr-bk">
    <p>Contact Us </p>     
</div>

<p class="c-u"> You may contact us by completing the following form or reach us directly at one of our locations. </p>

<div class="contus-lt">
    <sc:placeholder runat="server" id="LeadForm" key="LeadForm">
    </sc:placeholder>
</div>

<div runat="server" id="rtCol" class="contus-rt">
<sc:placeholder runat="server" id="ContactInformationListing" key="ContactInformationListing">
</sc:placeholder>
</div>