﻿using System;
using System.Web;
using System.Web.UI;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch
{
	public partial class RealtorSearchResults : ControlBase
	{
		private static readonly CommonHelper CommonHelper = new CommonHelper(); 

		protected void Page_Load(object sender, EventArgs e)
		{				
			Uri uri = HttpContext.Current.Request.Url;
			var lastSegment = Request.Url.AbsolutePath.ToLower();

			var realtorSearchDiviKey = SessionKeys.RealtorSearchDiviKey;
			var sessDiviId = WebSession[realtorSearchDiviKey].CastAs<string>();

			if (!string.IsNullOrWhiteSpace(sessDiviId))
			{
				liturl.Text = sessDiviId;
				var searchHelper = new SearchHelper();

				if (!string.IsNullOrWhiteSpace(sessDiviId))
				{
					var item = SearchHelper.GetSelectedArea(sessDiviId);

					//ihc card
					var ihcItem = searchHelper.GetIhcCardValues(item);
					if (ihcItem != null)
						litihccard.Text = searchHelper.GetIHCCardDetails(ihcItem);
					
					//campaign
					var realtorCamp = searchHelper.GetCampaignValues(item, "Realtor Inventory Page Campaign");
					if (realtorCamp != null)
						litLandingPageCampaign.Text = searchHelper.GetCampaignDetails(realtorCamp);

					CurrentPage.CurrentLiveChatSkill = item[SCFieldNames.LiveChatSkillCode];
					var diviPath = searchHelper.GetStateDivisionUrlById(sessDiviId.ToString());

					WebSession[SessionKeys.SpecificRfiLocationKey] = string.Format("{0}://{1}/{2}", uri.Scheme, uri.Authority, diviPath);
					WebSession[realtorSearchDiviKey] = string.Empty; 
				}
			}
			else
			{
				var prodType = CommonHelper.GetProductTypeByUrl(HttpContext.Current.Request.Url.AbsolutePath.ToLower());

				lastSegment = HttpUtility.UrlDecode(lastSegment);  
				var location = lastSegment.Remove(0,49);
                location = location.Replace("nc", string.Empty).Replace("sc", string.Empty);
				var divicity = location.Replace("-in-", "|").Split('|');
                
				if (divicity.Length > 1)
				{
					liturl.Text = string.Format("{3}/*[@@templateid='{0}']/{1}/{2}", SCIDs.TemplateIds.StatePage, divicity[1], divicity[0], prodType);
				}
				else
				{
					location = location.Remove(0, 3);
                    liturl.Text = string.Format("{2}/*[@@templateid='{0}']/{1}", SCIDs.TemplateIds.StatePage, location, prodType);
				}
			}
		}
	}
}