﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealtorSearch.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.RealtorSearch" %>
<%@ Register TagPrefix="uc" TagName="RealtorSearchFacet" Src="~/tmwebcustom/SubLayouts/RealtorSearch/RealtorSearchFacet.ascx" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="~/tmwebcustom/SubLayouts/Search/SearchMap.ascx" %>
<script type="text/javascript">
	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		$j(window).load(function () {
			// GetRealtorCommunities(searchpath, 1, true, "");
			BoundFacetValues(searchpath, false);
		});
	});
</script>
<style>
	.searchfact
	{
		display: block;
	}
</style>
<div class="lt-col">
	<input type="hidden" id="hndsearchPath" value="<%= liturl.Text%>" />
	<input type="hidden" id="hndRealtor" value="true" />
	<div class="blue-bar">
		<a href="#">Home for Sale Search</a>
	</div>
	<uc:RealtorSearchFacet ID="ucRealtorSearchFacet" runat="server" />
</div>
<div class="rt-col">
	<uc:SearchMap ID="ucSearchMap" runat="server" />
</div>
