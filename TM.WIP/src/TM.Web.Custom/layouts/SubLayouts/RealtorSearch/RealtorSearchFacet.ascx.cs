﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.layouts.SubLayouts.RealtorSearch
{
	public partial class RealtorSearchFacet : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string mode = SCContextItem.Fields["Mode"].Value;
			liturl.Text = mode;

			if (mode.ToLower() == "home for sale")
			{
				var searchHelper = new SearchHelper();
				var diviItems = searchHelper.GetAllCrossDivisions();
				foreach (var item in diviItems)
				{
					var listItem = new ListItem(item.Name, item.ID.ToString());

					listItem.Attributes.Add("optgroup", item.Domain.GetCurrentCompanyName());
					ddldivision.Items.Add(listItem);
				}
				ddldivision.Items.Insert(0, new ListItem("No preference", "0"));
			}
		}

		protected void OnClick_HomeSearch(object sender, EventArgs e)
		{
			Uri uri = HttpContext.Current.Request.Url; 
			var realtorSearchDiviKey = SessionKeys.RealtorSearchDiviKey;
			WebSession[realtorSearchDiviKey] = ddldivision.SelectedValue;

			Response.Redirect(uri.AbsolutePath + "/Realtor-Search-Results-in-" + ddldivision.SelectedItem.Text);
		}

	}
}