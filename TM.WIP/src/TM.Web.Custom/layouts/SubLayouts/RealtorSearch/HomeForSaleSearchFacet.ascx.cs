﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch
{
    public partial class HomeForSaleSearchFacet : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            const string NORTH_CAROLINA_ABBREVIATION = "nc";
            const string SOUTH_CAROLINA_ABBREVIATION = "sc";

            const string NORTH_CAROLINA_ITEMID = "{EF986DBD-B1A5-49F9-9A07-608CA73B0D9D}";
            const string SOUTH_CAROLINA_ITEMID = "{6230FEC6-4794-4B3D-9933-2EF6DC19F486}";


            string mode = SCContextItem.Fields["Mode"].Value;
            liturl.Text = mode;

            if (mode.ToLowerInvariant() == "home for sale")
            {
                var searchHelper = new SearchHelper();
                string realtorSearchArea = string.Empty;
                List<NameandId> diviItems = searchHelper.GetAllCrossDivisions();
 
                var isRealtorSearch = (SCContextItem.TemplateID == SCIDs.TemplateIds.RealtorSearch);
                Sitecore.Data.ID realtorSearchAreaID = new Sitecore.Data.ID();

                if (isRealtorSearch)
                {
                    //TODO: Refactor...temporary solution for the NC/SC Charlotte division issue
                    realtorSearchArea = CurrentPage.Request.Url.ToString().GetLastTextfromString('-').ToLowerInvariant();

                    if (realtorSearchArea.Contains(NORTH_CAROLINA_ABBREVIATION))
                    {
                        realtorSearchAreaID = new Sitecore.Data.ID(NORTH_CAROLINA_ITEMID);
                    }
                    else if (realtorSearchArea.Contains(SOUTH_CAROLINA_ABBREVIATION))
                    {
                        realtorSearchAreaID = new Sitecore.Data.ID(SOUTH_CAROLINA_ITEMID);
                    }
                }
                else
                {
                    realtorSearchAreaID = Sitecore.Context.Item.Parent.ID;
                }

                var currentDomainName = SCCurrentDeviceName.ToLowerInvariant();
                foreach (var item in diviItems)
                {
                    var listItem = new ListItem(item.Name, item.ID.ToString());
                    var itemDomainName = item.Domain.ToLowerInvariant();
                    listItem.Selected = itemDomainName == currentDomainName && (item.ID == realtorSearchAreaID || item.Name.ToLowerInvariant() == realtorSearchArea);
                    if (listItem.Selected)
                    {
                        SelectedArea = listItem.Value;
                    }
                    listItem.Attributes.Add("optgroup", item.Domain.GetCurrentCompanyName());
                    ddldivision.Items.Add(listItem);
                }
            }
        }

        protected string SelectedArea { get; set; }
    }
}