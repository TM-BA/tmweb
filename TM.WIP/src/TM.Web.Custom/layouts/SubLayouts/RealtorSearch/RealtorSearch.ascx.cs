﻿using System;
using System.Web;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorSearch
{
	public partial class RealtorSearch : ControlBase
	{
		private static readonly CommonHelper CommonHelper = new CommonHelper(); 
		protected void Page_Load(object sender, EventArgs e)
		{
			//liturl.Text = SCContextItem.Paths.FullPath.ToLower().Replace("/realtor search", "/new homes");
			liturl.Text = CommonHelper.GetProductTypeByUrl(HttpContext.Current.Request.Url.AbsolutePath.ToLower());
		}
	}
}
