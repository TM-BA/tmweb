﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeForSaleSearchCard.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.HomeForSaleSearchCard" %>
<div class="h-realtor">
    <span id="homecount"></span><span>
        <select id="searchsortby" name="sortBy" class="sb2">
            <option value="1">Community</option>
            <option value="2" selected>Price low to high</option>
            <option value="3">Price high to low</option>
            <option value="5">Plan Name</option>
            <option value="8">Availability</option>
            <option value="7">Number of bedrooms</option>
            <option value="6">Square Footage</option>
            <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
               { %>
            <option value="9">Stories</option>
            <% }
               else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
               {  %>
            <option value="9">Storeys</option>
            <% }
               else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
               {  %>
            <option value="9">Stories</option>
            <% } %>
        </select></span>
</div>
<div id="homeforsaleSearchResult">
</div>
<script id="homeforsalecard" type="text/x-jquery-tmpl">
		{{each(companyname, homes) $data}}
		<div class="com-hdr">
			<div id="headerName" class="h-sear1">Homes by ${companyname}</div>
		</div>
		{{each homes}}
			<div class="realtor-comm-card">
				<div class="realtor-image">
					<a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}}><img src="${ElevationImages}?w=160&h=90" alt="${HomeforSalePlanName}" onError="this.onerror=null;this.src='/Images/img-not-available.jpg';"></a></div>
				<div class="realtor-middle">
					<p class="rp0"><a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}} onclick="TM.Common.GAlogevent('Realtor','Click','HomeAddressLink')">${HomeforSalePlanName} at ${StreetAddress1}, ${City}, ${State}</a>{{if LotNumber!= ""}}(${LotNumber}){{/if}}</p>
					<p class="rp0">At <a href="${HomeforSaleCommunityDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}} onclick="TM.Common.GAlogevent('Realtor','Click','HomeAddressLink')">${HomeforSaleCommunityName}</a></P>
					<span class="rp1">
					{{if Availability != ""}}
						${Availability}
					{{/if}}
					</span>
					<span class="rp2">$${Price.formatMoney(0, ".", ",")}</span>
				</div>
				<div class="realtor-right">
					<p>
						{{if IsRealtor == true && RealtorBrokerCommissionRate != "" }}
							${RealtorBrokerCommissionRate}
						{{/if}}
					</p>
					<div>
						<a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}} class="button" onclick="TM.Common.GAlogevent('Details','Click','HomeForSale')">View Homes<span></span></a></div>
				</div>
				<div class="realtor-details">
					<div class="ask-abt">
						<a href="${HomeforSaleDetailsURL}/request-information/" {{if IsCurrentDomain == false }}target="_blank"{{/if}} class="button" onclick="TM.Common.GAlogevent('Realtor','Click','AskAboutThisHome')">Ask about this home</a>
					</div>
					<span>
						{{if SquareFootage != ""}}
							${SquareFootage.formatMoney(0, ".", ",")} Sq. Ft.
						{{/if}}
						{{if NumberofBedrooms != ""}} |
							{{if NumberofBedrooms > 1}}
								${NumberofBedrooms} Beds
							{{else}}
								${NumberofBedrooms} Bed
							{{/if}}
						{{/if}}
						{{if NumberofBathrooms != ""}} |
							{{if NumberofBathrooms > 1}}
								${NumberofBathrooms} Baths
							{{else}}
								${NumberofBathrooms} Bath
							{{/if}}
						{{/if}}
                        {{if NumberofHalfBathrooms != ""}} |
							{{if NumberofHalfBathrooms > 1}}
								${NumberofHalfBathrooms} Half Baths
							{{else}}
								${NumberofHalfBathrooms} Half Bath
							{{/if}}
						{{/if}}
						{{if NumberofGarages != ""}} |
								${NumberofGarages} Garage
						{{/if}}
						{{if NumberofStories != ""}} |
							{{if NumberofStories > 1}}
								${NumberofStories} Stories
							{{else}}
								${NumberofStories} Story
							{{/if}}
						{{/if}}
					</span>
					<p>${PlanDescription} {{if PlanDescription.length > 0 }}<a href="${HomeforSaleDetailsURL}" {{if IsCurrentDomain == false }}target="_blank"{{/if}}>see more&raquo;</a>{{/if}}
					</p>
				</div>
			</div>
		{{/each}}
	 {{/each}}
</script>
<script id="nohomeforsalecard" type="text/x-jquery-tmpl">
			<div class="commCard">
				<p>No records found</p>
			</div>
</script>