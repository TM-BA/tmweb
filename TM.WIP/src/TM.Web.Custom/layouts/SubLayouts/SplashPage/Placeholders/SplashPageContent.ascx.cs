﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Pipelines.InsertRenderings.Processors;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class SplashPageContent : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item source = DataSource;
            if (source != null)
            {
                txtContent.DataSource = source.ID.ToString();
                txtContent.DataBind();
                litMessage.Visible = false;
            }
            else
            {
                litMessage.Visible = true;
            }
        }
    }
}