﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LayoutC.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.LayoutC" %>
<div class="row">
	<div class="col-3">
		<sc:Placeholder runat="server" ID="phOneThird" Key="SplashPageOneThird1"/>
	</div>
	<div class="col-6">
		<sc:Placeholder runat="server" ID="phTwoThird" Key="SplashPageTwoThird1"/>
	</div>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth" Key="SplashPageFullWidth1"/>
</div>
<div class="row">
	<div class="col-3">
		<sc:Placeholder runat="server" ID="phOneThird2" Key="SplashPageOneThird2"/>
	</div>
	<div class="col-6">
		<sc:Placeholder runat="server" ID="phTwoThird2" Key="SplashPageTwoThird2"/>
	</div>
</div>
<%--<div class="row">
	<div class="col-3">
		<sc:Placeholder runat="server" ID="phOneThird3" Key="SplashPageOneThird3"/>
	</div>
	<div class="col-6">
		<sc:Placeholder runat="server" ID="phTwoThird3" Key="SplashPageTwoThird3"/>
	</div>
</div>--%>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth2" Key="SplashPageFullWidth2"/>
</div>