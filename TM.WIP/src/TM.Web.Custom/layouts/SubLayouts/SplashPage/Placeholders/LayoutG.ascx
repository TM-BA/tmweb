﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LayoutG.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.LayoutG" %>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth" Key="SplashPageFullWidth1"/>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth2" Key="SplashPageFullWidth2"/>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth3" Key="SplashPageFullWidth3"/>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth4" Key="SplashPageFullWidth4"/>
</div>
<div class="col-1">
	<sc:Placeholder runat="server" ID="phFullWidth5" Key="SplashPageFullWidth5"/>
</div>