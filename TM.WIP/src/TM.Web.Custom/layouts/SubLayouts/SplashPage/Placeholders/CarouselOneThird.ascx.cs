﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Web.UI.WebControls;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    //TODO we need to unified the 3 cs files from the 3 carousels files, full with, one third and two third, they are pretty much the same code, only thing changing is the size.
    //TODO I know I know.... not the best practice :( may the lord save my soul. 
    public partial class CarouselOneThird : ControlBase
    {
        //private int _width = 576;
        //private int _height = 377;
        private string _rotateDelay;
        //public string Datasource { get; set; }
        public List<string> Fieldnames = new List<string>();
        public int PrintWidth { get; set; }
        public int PrintHeight { get; set; }
        private string _slideOptions = string.Empty;

        public string SlideOptions
        {
            get { return _slideOptions; }
            set { _slideOptions = value; }
        }

        private string LetterBoxColor { get; set; }
        private bool _neverShowPinterest = false;

        private bool NeverShowPinterest
        {
            get { return _neverShowPinterest; }
            set { _neverShowPinterest = value; }
        }

        private bool neverExpandOnClick { get; set; }

        //public int Width
        //{
        //    get { return _width > -1 ? _width : 0; }
        //    set { _width = value; }
        //}

        //public int Height
        //{
        //    get { return _height > -1 ? _height : 0; }
        //    set { _height = value; }
        //}

        public bool IsPrintMode
        {
            get { return (Request["sc_device"] == "print"); }
        }

        private int SkipCount;

        /// <summary>
        /// Slide show rotation delay in seconds
        /// </summary>
        public string RotateDelay
        {
            get
            {
                int delay = ExtensionsToObject.CastAs<int>(Config.Settings.DefaultSlideShowDelay);

                if (!string.IsNullOrEmpty(_rotateDelay))
                {
                    delay = _rotateDelay.CastAs(delay);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(DataSource.Fields["Auto Rotation Speed In Seconds"].Value))
                    {
                        int.TryParse(DataSource.Fields["Auto Rotation Speed In Seconds"].Value, out delay);
                    }
                }

                return (delay * 1000).ToString(CultureInfo.InvariantCulture);
            }
            set { _rotateDelay = value; }
        }

        private int _maxImages = 1000;

        public int MaxImages
        {
            get { return _maxImages; }
            set { _maxImages = (value > 0) ? value : 1; }
        }

        protected string ItemClass { get; set; }

        private string _onClickCode;

        protected string OnClickCode
        {
            get { return _onClickCode; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    _onClickCode = " style=\"cursor:pointer\" " + value;
            }
        }

        private bool _renderFromContext = true;
        public Sublayout SubLayout;

        public bool RenderFromContext
        {
            get { return _renderFromContext; }
            set { _renderFromContext = value; }
        }

        public string ContainerClass { get; set; }
        protected string WrapperClass { get; set; }

        protected string SlideShowWidth { get; set; }
        protected string ULContainerWidth { get; set; }

        public bool LoadScripts = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DataSource == null)
            {
                this.spanNoDataSource.Visible = true;
                return;
            }
            else
            {
                this.spanNoDataSource.Visible = false;
            }
            //Datasource = SCContextItem.ID.ToString();
            if (SubLayout == null)
            {
                SubLayout = ((Sublayout)Parent);
            }

            //if (!Fieldnames.Any())
            //{
            //    Fieldnames.Add(SCUtils.GetSCParameter("fieldName", SubLayout));
            //}

            Fieldnames.Add("Slide Show");

            //Width = SCUtils.GetSCParameter("width", SubLayout).CastAs(default(int));
            //Width = 307;

            //PrintWidth = SCUtils.GetSCParameter("printWidth", SubLayout).CastAs(Width);
            //Height = SCUtils.GetSCParameter("height", SubLayout).CastAs(default(int));
            //Height = 380;
            //PrintHeight = SCUtils.GetSCParameter("printHeight", SubLayout).CastAs(Height);
            MaxImages = SCUtils.GetSCParameter("NumberPrintImages", SubLayout).CastAs(int.MaxValue);
            OnClickCode = SCUtils.GetSCParameter("OnClick", SubLayout).CastAs<string>();
            ItemClass = SCUtils.GetSCParameter("ItemClass", SubLayout).CastAs<string>();
            ContainerClass = SCUtils.GetSCParameter("ContainerClass", SubLayout).CastAs<string>();
            WrapperClass = SCUtils.GetSCParameter("WrapperClass", SubLayout).CastAs<string>();
            SlideShowWidth = SCUtils.GetSCParameter("SlideShowWidth", SubLayout).CastAs<string>("100%");
            ULContainerWidth = SCUtils.GetSCParameter("ULContainerWidth", SubLayout).CastAs<string>("100%");
            var slideShowOptionsObj = SCUtils.GetSCParameter("SlideOptions", SubLayout).FromJSON<SlideShowOptions>();

            if (slideShowOptionsObj == null)
            {
                slideShowOptionsObj = new SlideShowOptions();
                if (!string.IsNullOrWhiteSpace(DataSource.Fields["Auto Rotation Speed In Seconds"].Value))
                {
                    int speedBetweenSlides;
                    int.TryParse(DataSource.Fields["Auto Rotation Speed In Seconds"].Value, out speedBetweenSlides);
                    if ((speedBetweenSlides > 0))
                    {
                        slideShowOptionsObj.auto = speedBetweenSlides * 1000;
                    }
                }

                if (slideShowOptionsObj.visible != 0 && !slideShowOptionsObj.pagination)
                {
                    SkipCount = slideShowOptionsObj.visible;
                    hypNext.Visible = true;
                    hypPrevious.Visible = true;
                }

                SlideOptions = slideShowOptionsObj.ToJSON(true);
            }

            neverExpandOnClick = SCUtils.GetSCParameter("neverExpandOnClick", SubLayout).CastAs<bool>(false);
            LetterBoxColor = SCUtils.GetSCParameter("LetterBoxColor", SubLayout).CastAs("FFFFFF");
            NeverShowPinterest = SCUtils.GetSCParameter("neverPinterest", SubLayout).CastAs<bool>(false);
            litSlideImages.Text = GetSlidesHtml();
        }


        private string GetSlidesHtml()
        {
            var sb = new StringBuilder();
            int slideShowImagesCount = 0;
            Item dataSource = DataSource;
            if (dataSource != null)
            {
                bool showPinterest = false;
                if (!String.IsNullOrEmpty(dataSource.Fields["Allow Pinning of Images to Pinterest"].Value))
                {
                    if (dataSource.Fields["Allow Pinning of Images to Pinterest"].Value == "1")
                    {
                        showPinterest = true;
                    }
                }
                bool shouldExpandImagesOnClick = !string.IsNullOrEmpty(SCContextItem["Expand Images on Click"]);

                foreach (var fieldName in Fieldnames)
                {
                    var slideShowImageField = (MultilistField)dataSource.Fields[fieldName];
                    string ImgID = "id=\"firstSlide\"";
                    if (slideShowImageField != null)
                    {
                        Item[] slideShowImages = slideShowImageField.GetItems();
                        slideShowImagesCount += slideShowImages.Count();

                        for (int i = 0; i < MaxImages && i < slideShowImages.Count(); i++)
                        {
                            string image = slideShowImages[i].GetMediaUrl();
                            string pinterestCode = ((showPinterest) && (!IsPrintMode))
                                                       ? GetPinterestURL()
                                                       : "";
                            string imageLink = ImageLink(image);
                            string li = string.Empty;
                            if (IsPrintMode)
                            {
                                li = @"<li class=""slide""><img src=""{0}"" /></li>"
                                    .FormatWith(imageLink);
                            }
                            else
                            {
                                string linkStart = string.Empty;
                                string linkStop = string.Empty;
                                if (shouldExpandImagesOnClick && !neverExpandOnClick)
                                {
                                    linkStart =
                                        @"<a class=""expandImg"" href=""{0}"" title=""{1}"">".FormatWith(
                                            image + "?w=1150&as=0", slideShowImages[i]["Alt"]);
                                    linkStop = "</a>";
                                    OnClickCode = string.Empty;
                                }
                                li = @"<li class=""slide"" {9}>{6}<img ni={8} alt=""{5}"" title=""{5}"" {4} {3} class=""{2}"" src=""{0}"" style='max-width:307px' />{7}{1}</li>"
                                    .FormatWith(imageLink, pinterestCode, ItemClass, OnClickCode ?? string.Empty, ImgID, slideShowImages[i]["Alt"], linkStart, linkStop, i, i > 0 ? "" : string.Empty);

                                //li = @"<li class=""slide"" {11}>{8}<img ni={10} alt=""{7}"" title=""{7}"" {6} {3} class=""{2}"" src=""{0}"" height=""{4}"" width=""{5}"" />{9}{1}</li>"
                                //    .FormatWith(imageLink, pinterestCode, ItemClass, OnClickCode ?? string.Empty, Height,
                                //    Width, ImgID, slideShowImages[i]["Alt"], linkStart, linkStop, i, i > 0 ? "" : string.Empty);
                            }
                            sb.Append(li);
                            ImgID = string.Empty;
                        }
                    }
                    else
                    {
                        string li = @"<li class=""slide""><img {3} {1} class=""{2}"" src=""{0}"" /></li>"
                            .FormatWith("/images/tm/DefaultCommunity.jpg", ItemClass, OnClickCode, ImgID);
                        sb.Append(li);
                    }
                }
            }

            if (slideShowImagesCount <= SkipCount)
            {
                hypNext.Visible = false;
                hypPrevious.Visible = false;
            }
            return sb.ToString();
        }

        private string ImageLink(string image)
        {
            string imageLink = string.Empty;
            imageLink = string.Empty;
            imageLink = imageLink + (LetterBoxColor.IsNotEmpty() ? "&bc=" + LetterBoxColor : "&bc=FFFFFF");
            return image + "?" + imageLink;
        }

        private string GetPinterestURL()
        {
            return
                "<span style=\"Position:absolute;top:0;right:0;\" class='st_pinterest_large' title='Pin this image to your Pinboard' displayText='Pinterest'></span>";
        }
    }
}