﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class SplashPageSocialMediaFollow : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item source = DataSource;
            if (DataSource == null)
            {
                this.spanNoDataSource.Visible = true;
                return;
            }
            else
            {
                this.spanNoDataSource.Visible = false;
            }
            txtContent.DataSource = source.ID.ToString();
            txtContent.DataBind();

            txtTitle.DataSource = source.ID.ToString();
            txtTitle.DataBind();

            if (source.Fields["Show Facebook"].Value == "1")
            {
                this.liFacebook.Visible = true;
                var item = source.Fields["Facebook URL"];
                if (item != null && item.Value.IsNotEmpty())
                    hrefFacebook.HRef = item.GetLinkFieldPath();
            }
            if (source.Fields["Show Twitter"].Value == "1")
            {
                this.liTwitter.Visible = true;
                var item = source.Fields["Twitter URL"];
                if (item != null && item.Value.IsNotEmpty())
                    this.hrefTwitter.HRef = item.GetLinkFieldPath();
            }
            if (source.Fields["Show YouTube"].Value == "1")
            {
                this.liYoutube.Visible = true;
                var item = source.Fields["YouTube URL"];
                if (item != null && item.Value.IsNotEmpty())
                    this.hrefYoutube.HRef = item.GetLinkFieldPath();
            }
            if (source.Fields["Show Pinterest"].Value == "1")
            {
                this.liPinterest.Visible = true;
                var item = source.Fields["Pinterest URL"];
                if (item != null && item.Value.IsNotEmpty())
                    this.hrefPinterest.HRef = item.GetLinkFieldPath();
            }
            if (source.Fields["Show GooglePlus"].Value == "1")
            {
                this.liGoogle.Visible = true;
                var item = source.Fields["GooglePlus URL"];
                if (item != null && item.Value.IsNotEmpty())
                    this.hrefGoogle.HRef = item.GetLinkFieldPath();
            }
            if (source.Fields["Show Blog"].Value == "1")
            {
                this.liBlog.Visible = true;
                var item = source.Fields["Blog URL"];
                if (item != null && item.Value.IsNotEmpty())
                    this.hrefBlog.HRef = item.GetLinkFieldPath();
            }
            
        }
    }
}