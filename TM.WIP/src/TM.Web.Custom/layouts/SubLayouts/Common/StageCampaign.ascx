﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StageCampaign.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.StageCampaign" %>
<script src="/javascript/lib/slideshow.min.js" type="text/javascript"></script>
<link href="/Styles/lib/slideshow.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $j(document).ready(function () {
        // Create slideshow instances
        var $s = $j('.slideshow').slides({
            speed: 1300
        });

        // Access an instance API
        var api = $s.eq(0).data('slides');

        $j(document).on('mouseenter', "a[data-slidenumber], li[data-slidenumber]", function () {
            var $this = jQuery(this);
            api.to($this.data('slidenumber'), true);
            api.pause();
        });

        $j(document).on('mouseleave', "a[data-slidenumber], li[data-slidenumber]", function () {
            if(api.paused)
		api.play();
        });





    });
</script>	        
           

<div class="slideshow banner" data-transition="crossfade" data-loop="true" data-auto="5000">
    <ul  class="carousel">
        <asp:Repeater runat="server" ID="rptCarousel">
            <ItemTemplate>
                <li class="slide " <%# Container.ItemIndex == 0 ? "" : "style='display:none'" %>>
                   <a href="<%# Eval("TargetURL") %>" >
                        <img  src="<%# Eval("LargeImageSrc") %>"  alt="<%# Eval("Title") %>" data-img="large" onclick="TM.Common.GAlogevent('Campaign','Click','CampaignImage')" />
                    </a>	
                </li>
            </ItemTemplate>
        </asp:Repeater>
                                
    </ul>
              
</div>	
                        			
<ul class="bannertbtm">
    <asp:Repeater runat="server" ID="rptStageImages">
        <ItemTemplate>
            <div class="stageimages">
            <li data-img="small" data-slidenumber="<%# Container.ItemIndex %>">
                <img src="<%# Eval("SmallImageSrc") %>" onclick="TM.Common.GAlogevent('Campaign', 'Click','CampaignPosition<%# Container.ItemIndex + 1 %>');" /> 
		        <div class="hmbnr-overlay"></div>
                
                    <a onclick="TM.Common.GAlogevent('Campaign', 'Click','CampaignPosition<%# Container.ItemIndex + 1 %>');" href="<%# Eval("TargetURL") %>" data-slidenumber="<%# Container.ItemIndex %>">
                        <%# Eval("Title") %><br><span><%# Eval("SubTitle") %></span>
		            </a>
	        </li>
            </div>
            <div class="stageimagesmobile">
            <li data-img="small">
                <img src="<%# Eval("SmallImageSrc") %>" onclick="TM.Common.GAlogevent('Campaign', 'Click','CampaignPosition<%# Container.ItemIndex + 1 %>');" /> 
		        <div class="hmbnr-overlay"></div>
                
                     <a href="<%# Eval("TargetURL") %>" onclick="TM.Common.GAlogevent('Campaign', 'Click','CampaignPosition<%# Container.ItemIndex + 1 %>');">
                        <%# Eval("Title") %><br><span><%# Eval("SubTitle") %></span>
		            </a>
	        </li>
            </div>
        </ItemTemplate>
    </asp:Repeater>									
</ul>
	