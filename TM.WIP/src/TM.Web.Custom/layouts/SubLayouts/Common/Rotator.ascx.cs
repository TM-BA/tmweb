﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class Rotator : ControlBase
    {
        private int _width = 576;
        private int _height = 377;
        private string _rotateDelay;
        public string Datasource { get; set; }
        public List<string> Fieldnames = new List<string>();
        public int PrintWidth { get; set; }
        public int PrintHeight { get; set; }
        private string _slideOptions = string.Empty;

        public string SlideOptions
        {
            get { return _slideOptions; }
            set { _slideOptions = value; }
        }

        private string LetterBoxColor { get; set; }
        private bool _neverShowPinterest = false;

        private bool NeverShowPinterest
        {
            get { return _neverShowPinterest; }
            set { _neverShowPinterest = value; }
        }

        private bool neverExpandOnClick { get; set; }

        public int Width
        {
            get { return _width > -1 ? _width : 0; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height > -1 ? _height : 0; }
            set { _height = value; }
        }

        public bool IsPrintMode
        {
            get { return (Request["sc_device"] == "print"); }
        }

        private int SkipCount;

        /// <summary>
        /// Slide show rotation delay in seconds
        /// </summary>
        public string RotateDelay
        {
            get
            {
                int delay = ExtensionsToObject.CastAs<int>(Config.Settings.DefaultSlideShowDelay);

                if (!string.IsNullOrEmpty(_rotateDelay))
                {
                    delay = _rotateDelay.CastAs(delay);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(SCContextItem["Slideshow Image Rotation Speed"]))
                    {
                        int.TryParse(SCContextItem["Slideshow Image Rotation Speed"], out delay);
                    }
                }

                return (delay*1000).ToString(CultureInfo.InvariantCulture);
            }
            set { _rotateDelay = value; }
        }

        private int _maxImages = 1000;

        public int MaxImages
        {
            get { return _maxImages; }
            set { _maxImages = (value > 0) ? value : 1; }
        }

        protected string ItemClass { get; set; }

        private string _onClickCode;

        protected string OnClickCode
        {
            get { return _onClickCode; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    _onClickCode = " style=\"cursor:pointer\" " + value;
            }
        }

        private bool _renderFromContext = true;
        public Sublayout SubLayout;

        public bool RenderFromContext
        {
            get { return _renderFromContext; }
            set { _renderFromContext = value; }
        }

        public string ContainerClass { get; set; }
        protected string WrapperClass { get; set; }

        protected string SlideShowWidth { get; set; }
        protected string ULContainerWidth { get; set; }

        public bool LoadScripts = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            Datasource = SCContextItem.ID.ToString();
            if (SubLayout == null)
            {
                SubLayout = ((Sublayout) Parent);
            }

            if (!Fieldnames.Any())
            {
                Fieldnames.Add(SCUtils.GetSCParameter("fieldName", SubLayout));
            }

            Width = SCUtils.GetSCParameter("width", SubLayout).CastAs(default(int));
            PrintWidth = SCUtils.GetSCParameter("printWidth", SubLayout).CastAs(Width);
            Height = SCUtils.GetSCParameter("height", SubLayout).CastAs(default(int));
            PrintHeight = SCUtils.GetSCParameter("printHeight", SubLayout).CastAs(Height);
            MaxImages = SCUtils.GetSCParameter("NumberPrintImages", SubLayout).CastAs(int.MaxValue);
            OnClickCode = SCUtils.GetSCParameter("OnClick", SubLayout).CastAs<string>();
            ItemClass = SCUtils.GetSCParameter("ItemClass", SubLayout).CastAs<string>();
            ContainerClass = SCUtils.GetSCParameter("ContainerClass", SubLayout).CastAs<string>();
            WrapperClass = SCUtils.GetSCParameter("WrapperClass", SubLayout).CastAs<string>();
            SlideShowWidth = SCUtils.GetSCParameter("SlideShowWidth", SubLayout).CastAs<string>("100%");
            ULContainerWidth = SCUtils.GetSCParameter("ULContainerWidth", SubLayout).CastAs<string>("100%");
            var slideShowOptionsObj = SCUtils.GetSCParameter("SlideOptions", SubLayout).FromJSON<SlideShowOptions>();

            if (slideShowOptionsObj != null)
            {
                if (!string.IsNullOrWhiteSpace(SCContextItem["Slideshow Image Rotation Speed"]))
                {
                    int speedBetweenSlides;
                    int.TryParse(SCContextItem["Slideshow Image Rotation Speed"], out speedBetweenSlides);
                    if ((speedBetweenSlides > 0))
                    {
                        slideShowOptionsObj.auto = speedBetweenSlides*1000;
                    }
                }
                else
                    slideShowOptionsObj.auto = 10000;

                if (slideShowOptionsObj.visible != 0 && !slideShowOptionsObj.pagination)
                {
                    SkipCount = slideShowOptionsObj.visible;
                    hypNext.Visible = true;
                    hypPrevious.Visible = true;
                }

                SlideOptions = slideShowOptionsObj.ToJSON(true);
            }

            neverExpandOnClick = SCUtils.GetSCParameter("neverExpandOnClick", SubLayout).CastAs<bool>(false);
            LetterBoxColor = SCUtils.GetSCParameter("LetterBoxColor", SubLayout).CastAs("FFFFFF");
            NeverShowPinterest = SCUtils.GetSCParameter("neverPinterest", SubLayout).CastAs<bool>(false);
            litSlideImages.Text = GetSlidesHtml();
        }


        private string GetSlidesHtml()
        {
            var sb = new StringBuilder();
            int slideShowImagesCount = 0;
            Item dataSource = SCContextDB.GetItem(Datasource);
            if (dataSource != null)
            {
                bool showPinterest = (!NeverShowPinterest &&
                                      !string.IsNullOrEmpty(SCContextItem["Allow Pinning of Images to Pinterest"]));
                bool shouldExpandImagesOnClick = !string.IsNullOrEmpty(SCContextItem["Expand Images on Click"]);

                foreach (var fieldName in Fieldnames)
                {
                    var slideShowImageField = (MultilistField) dataSource.Fields[fieldName];
                    string ImgID = "id=\"firstSlide\"";
                    if (slideShowImageField != null)
                    {
                        Item[] slideShowImages = slideShowImageField.GetItems();
                        slideShowImagesCount += slideShowImages.Count();

                        for (int i = 0; i < MaxImages && i < slideShowImages.Count(); i++)
                        {
                            string image = slideShowImages[i].GetMediaUrl();
                            string pinterestCode = ((showPinterest) && (!IsPrintMode))
                                                       ? GetPinterestURL()
                                                       : "";
                            string imageLink = ImageLink(image);
                            string li = string.Empty;
                            if (IsPrintMode)
                            {
                                li = @"<li class=""slide""><img src=""{0}"" /></li>"
                                    .FormatWith(imageLink);
                            }
                            else
                            {
                                string linkStart = string.Empty;
                                string linkStop = string.Empty;
                                if (shouldExpandImagesOnClick && !neverExpandOnClick)
                                {
                                    linkStart =
                                        @"<a class=""expandImg"" href=""{0}"" title=""{1}"">".FormatWith(
                                            image + "?w=1150&as=0", slideShowImages[i]["Alt"]);
                                    linkStop = "</a>";
                                    OnClickCode = string.Empty;
                                }

                                li = @"<li class=""slide"" {11}>{8}<img ni={10} alt=""{7}"" title=""{7}"" {6} {3} class=""{2}"" src=""{0}"" height=""{4}"" width=""{5}"" />{9}{1}</li>"
                                    .FormatWith(imageLink, pinterestCode, ItemClass, OnClickCode ?? string.Empty, Height,
                                    Width, ImgID, slideShowImages[i]["Alt"], linkStart, linkStop, i, i>0?"":string.Empty );
                            }
                            sb.Append(li);
                            ImgID = string.Empty;
                        }
                    }
                    else
                    {
                        string li = @"<li class=""slide""><img {3} {1} class=""{2}"" src=""{0}"" /></li>"
                            .FormatWith("/images/tm/DefaultCommunity.jpg", ItemClass, OnClickCode, ImgID);
                        sb.Append(li);
                    }
                }
            }

            if (slideShowImagesCount <= SkipCount)
            {
                hypNext.Visible = false;
                hypPrevious.Visible = false;
            }
            return sb.ToString();
        }

        private string ImageLink(string image)
        {
            string imageLink = (Height != 0 ? "&h=" + Height : string.Empty);
            imageLink = imageLink + (Width != 0 ? "&w=" + Width : string.Empty);
            imageLink = imageLink + (LetterBoxColor.IsNotEmpty() ? "&bc=" + LetterBoxColor : "&bc=FFFFFF");
            return image + "?" + imageLink;
        }

        private string GetPinterestURL()
        {
            return
                "<span style=\"Position:absolute;top:0;right:0;\" class='st_pinterest_large' displayText='Pinterest'></span>";
        }
    }
}
