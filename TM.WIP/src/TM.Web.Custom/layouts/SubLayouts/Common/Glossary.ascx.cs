﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Common;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Text;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;
using TM.Utils.Web;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class Glossary : ControlBase 
    {
      
        private string _realEstateGlossaryPage;

        struct glossaryItem 
        {
            string name;

            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            string url;

            public string Url
            {
                get { return url; }
                set { url = value; }
            }

            string cssClass;

            public string CssClass
            {
                get { return cssClass; }
                set { cssClass = value; }
            }
        
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            btnSearch.Attributes.Add("onclick", "SetGAEventbyText('txtLookup','Homebuying');");

            _realEstateGlossaryPage = "/home-buying/real-estate-glossary?stype=1&search=";

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.HomeBuying101.RealStateGlossary));

            Item contextItem = matchingTemplates.FirstOrDefault();

            Sitecore.Collections.ChildList GlossaryItems = contextItem.GetChildren();

            List<glossaryItem> glossaryItemList = new List<glossaryItem>();
            StringBuilder glossaryTerms = new StringBuilder();
            bool hasChildrens = false;

            if (GlossaryItems != null)
            {

                foreach (Item GlossaryItem in GlossaryItems)
                {
                    glossaryItem item = new glossaryItem();

                    if (GlossaryItem.GetChildren().Count > 0)
                    { hasChildrens = true; }
                    else { hasChildrens = false; }

                    item.Name = GlossaryItem.Fields["Title"].ToString() != string.Empty ? GlossaryItem.Fields["Title"].ToString() : GlossaryItem.DisplayName;
                      
                    if (hasChildrens)
                    {
                        if (GlossaryItem.Fields["Title"].Value != string.Empty)
                        {
                            if (GlossaryItem.Fields["Title"].Value == "#")
                            {
                                item.Url = _realEstateGlossaryPage + "num";
                            }
                            else
                            {

                                item.Url = _realEstateGlossaryPage + GlossaryItem.Fields["Title"].Value;
                            }
                        }
                        else
                        {

                            if (GlossaryItem.DisplayName.ToLower() == "number")
                            {
                                item.Url = _realEstateGlossaryPage + "num";
                            }
                            else
                            {

                                item.Url = _realEstateGlossaryPage + GlossaryItem.DisplayName;
                            }
                        }
                    }
                    else
                    { 
                        item.Url = "javascript:void(0);";
 
                    }
                     
                    
                    glossaryItemList.Add(item);
                    populateGlossaryTerms(glossaryTerms, GlossaryItem);
                
                }
            }

            Terms.Value = glossaryTerms.ToString();
            glossaryItemList = glossaryItemList.OrderBy(a => a.Name).ToList(); // Order list
            rptGlossary.DataSource = glossaryItemList;
            rptGlossary.DataBind();


        }

        private void populateGlossaryTerms(StringBuilder glossaryTerms, Item contextItem)
        {
            List<Item> ActiveGlossaryItems = contextItem.GetChildren().ToList();
            List<Item> GlossaryItems = ActiveGlossaryItems.Where(i => i.Fields["Status"].Value.Equals(SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString())).ToList();
            

            if (GlossaryItems != null)
            {

                foreach (Item GlossaryTerm in GlossaryItems)
                {

                    glossaryTerms.Append(GlossaryTerm.Fields["Term"].ToString() + ",");

                }
            }

        }

        protected void rptGlossary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

       

        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            _realEstateGlossaryPage = "/home-buying/real-estate-glossary?stype=2&search=";

            if (!String.IsNullOrEmpty(txtLookup.Text))
            {
                Response.Redirect(_realEstateGlossaryPage + txtLookup.Text.Trim());

            }
            else
            { Response.Redirect(_realEstateGlossaryPage+"*"); }
        }

 
       
    }
}