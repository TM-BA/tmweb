﻿using System;
using System.Collections.Generic;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Constants;

using Sitecore.Data.Fields;
using Sitecore.Extensions;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class NavigationItem
    {

        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        int level;

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        string url;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        string cssClass;

        public string CssClass
        {
            get { return cssClass; }
            set { cssClass = value; }
        }

        string cssClassCol;

        public string CssClassCol
        {
            get { return cssClassCol; }
            set { cssClassCol = value; }
        }

        int posIndex;

        public int PosIndex
        {
            get { return posIndex; }
            set { posIndex = value; }
        }

        bool setUL = false;

        public bool SetUL
        {
            get { return setUL; }
            set { setUL = value; }
        }

        string style;

        public string Style
        {
            get { return style; }
            set { style = value; }
        }

    }

    public partial class LeftNavigation : ControlBase
    {
        NavigationItem navItem;
        List<NavigationItem> itemList;

        protected void Page_Load(object sender, EventArgs e)
        {


            Item currentItem = Sitecore.Context.Item;
            string Root = currentItem.Fields["Root"].ToString().Trim();

            if (!String.IsNullOrEmpty(Root))
            {

                itemList = new List<NavigationItem>();
                Item contextItem = Sitecore.Context.Database.GetItem(Root);

                if (contextItem != null)
                {
                    lblNavTitle.Text = contextItem.DisplayName;

                    Traverse(contextItem, 0);
                    rptLeftNavigation.DataSource = itemList;
                    rptLeftNavigation.DataBind();
                }
            }
            else
            {
                throw new Exception("Menu Root not found");
            }

            PopulateCampaigns();
        }




        public bool isExpired(string date)
        {
            DateTime endDate;
            bool expired = false;


            endDate = SCUtils.SCDateTimeToMsDateTime(date);

            if (endDate != DateTime.MinValue)
            {
                if (endDate <= DateTime.Now)
                {
                    expired = true;
                }

            }


            return expired;

        }

        private void PopulateCampaigns()
        {

            Item currentItem = Sitecore.Context.Item;


            // Populate primary campaign image 

            string primaryCampaignImageURL = string.Empty;
            bool primaryCampaignStatus = false;
            bool primaryCampaignExpirationStatus = false;
            string primaryCampaignTargetURL = "";

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryCampaignStatus].Value))
                if (currentItem.Fields[SCIDs.Navigation.primaryCampaignStatus].ToString() == "Active")
                {
                    primaryCampaignStatus = true;
                }

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryEndDate].Value))
            {
                primaryCampaignExpirationStatus = isExpired(currentItem.Fields[SCIDs.Navigation.primaryEndDate].Value);

            }


            if (primaryCampaignStatus && primaryCampaignExpirationStatus == false)
            {

                ImageField imageField1 = currentItem.Fields[SCIDs.Navigation.primaryContent];
                if (imageField1 != null && imageField1.MediaItem != null)
                {
                    MediaItem image = new MediaItem(imageField1.MediaItem);
                    primaryCampaignImageURL = Sitecore.StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image));
                }

                if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.primaryTargeURL].Value))
                {
                    LinkField field = currentItem.Fields[SCIDs.Navigation.primaryTargeURL];
                    primaryCampaignTargetURL = field.GetFriendlyUrl();
                }

                campaignLink1.NavigateUrl = primaryCampaignTargetURL;
                campaignImage1.ImageUrl = primaryCampaignImageURL;
                campaignImage1.Visible = true;
            }
            else
            {
                campaignImage1.Visible = false;
            }



            // Populate seconday campaign image 

            string secondaryCampaignImageURL = string.Empty;
            bool secondaryCampaignStatus = false;
            bool secondaryCampaignExpirationStatus = false;
            string secondaryCampaignTargetURL = "";

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.secondaryCampaignStatus].Value))
                if (currentItem.Fields[SCIDs.Navigation.secondaryCampaignStatus].ToString() == "Active")
                {
                    secondaryCampaignStatus = true;
                }

            if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.secondaryEndDate].Value))
            {
                secondaryCampaignExpirationStatus = isExpired(currentItem.Fields[SCIDs.Navigation.secondaryEndDate].Value);
            }


            if (secondaryCampaignStatus && secondaryCampaignExpirationStatus == false)
            {

                ImageField imageField2 = currentItem.Fields[SCIDs.Navigation.secondaryContent];
                if (imageField2 != null && imageField2.MediaItem != null)
                {
                    MediaItem image = new MediaItem(imageField2.MediaItem);
                    secondaryCampaignImageURL = Sitecore.StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image));
                }

                if (!string.IsNullOrEmpty(currentItem.Fields[SCIDs.Navigation.secondaryTargeURL].Value))
                {
                    LinkField field = currentItem.Fields[SCIDs.Navigation.secondaryTargeURL];
                    secondaryCampaignTargetURL = field.GetFriendlyUrl();
                }

                campaignLink2.NavigateUrl = secondaryCampaignTargetURL;
                campaignImage2.ImageUrl = secondaryCampaignImageURL;

                campaignImage2.Visible = true;
            }
            else
            {
                campaignImage2.Visible = false;
            }

        }

        private void Traverse(Item Item, int Level)
        {

            string showInMenu = "";
            bool isFolder = false;
            bool isContentFolder = false;
            bool hasChildrens = false;
            if (Item.Fields["Show In Menu"] != null)
            {
                showInMenu = Item.Fields["Show In Menu"].ToString();
            }
            else if (Item.TemplateID == SCIDs.TemplateIds.Folders)
            {
                isFolder = true;
            }
            else if (Item.TemplateID == SCIDs.TemplateIds.ContentFolders)
            {
                isContentFolder = true;
            }

            Sitecore.Collections.ChildList childNodes = Item.GetChildren();
            if (childNodes.Count > 0)
            {
                foreach (Item child in childNodes)
                {
                    if (child.Fields["Show In Menu"] != null)
                        if (child.Fields["Show In Menu"].Value == "1")
                        {
                            hasChildrens = true;
                        }

                }
            }

            string navTitle = "";
            if (Item.Fields["Navigation Title"] != null)
            {
                navTitle = Item.Fields["Navigation Title"].ToString();
            }

            if (showInMenu == "1" || (isFolder && hasChildrens) || (isContentFolder && hasChildrens))
            {
                navItem = new NavigationItem();
                navItem.Level = Level;


                if (Level == 0)
                {
                    navItem.Style = "con-left1";

                }
                else if (Level == 1)
                {
                    navItem.Style = "con-left2";
                }
                else if (Level == 2)
                {
                    navItem.Style = "con-left3";
                }

                navItem.Name = navTitle != "" ? navTitle : Item.DisplayName;

                if (isFolder == false && isContentFolder == false)
                {
                    //FB Case 363: Allow override of left menu url.
                    LinkField linkField = Item.Fields[SCIDs.Navigation.menuItemURL];

                    navItem.Url = !string.IsNullOrWhiteSpace(linkField.Value) ? linkField.GetFriendlyUrl() : SCUtils.GetItemUrl(Item);
                }
                itemList.Add(navItem);
            }

            Sitecore.Collections.ChildList letterNode = Item.GetChildren();
            foreach (Item child in letterNode)
            {
                Traverse(child, (Level + 1));
            }

        }
    }
}