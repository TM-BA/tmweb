﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Common;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Text;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class GlossaryItemDisplay : ControlBase
    {
        struct glossaryTerm
        {

            string term;

            public string Term
            {
                get { return term; }
                set { term = value; }
            }

            string definition;

            public string Definition
            {
                get { return definition; }
                set { definition = value; }
            }

        }

        struct glossaryItem
        {
            string name;

            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            string url;

            public string Url
            {
                get { return url; }
                set { url = value; }
            }

            string cssClass;

            public string CssClass
            {
                get { return cssClass; }
                set { cssClass = value; }
            }

        }

        List<glossaryTerm> glossaryTermList;
        List<glossaryTerm> glossaryTermListResults;
        string[] number = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" };


        protected void Page_Load(object sender, EventArgs e)
        {
            string keyword = Request.QueryString["search"] != null ? Request.QueryString["search"].ToString() : "*";
            string searchType = Request.QueryString["stype"] != null ? Request.QueryString["stype"].ToString() : "*";

            if (!string.IsNullOrEmpty(keyword))
            {
                Search(keyword, searchType);
            }

        
        }

        /// <summary>
        /// Populate terms and definitions 
        /// </summary>
        /// <param name="glossaryTerms"></param>
        /// <param name="contextItem"></param>
        private void populateGlossaryTerms(StringBuilder glossaryTerms, Item contextItem)
        {
            Sitecore.Collections.ChildList GlossaryItems = contextItem.GetChildren();
            glossaryTerm term = new glossaryTerm();

            if (GlossaryItems != null)
            {

                foreach (Item GlossaryTerm in GlossaryItems)
                {

                    glossaryTerms.Append(GlossaryTerm.Fields["Term"].ToString() + ",");
                    term.Term = GlossaryTerm.Fields["Term"].ToString();
                    term.Definition = GlossaryTerm.Fields["Definition"].ToString();
                    glossaryTermList.Add(term);
                }
            }

        }

        private void Search(string keyword, string searchType)
        {
            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.HomeBuying101.RealStateGlossary));

            Item contextItem = matchingTemplates.FirstOrDefault();

            Sitecore.Collections.ChildList GlossaryItems = contextItem.GetChildren();
            glossaryTerm term = new glossaryTerm();

            List<glossaryItem> glossaryItemList = new List<glossaryItem>();
            StringBuilder glossaryTerms = new StringBuilder();
            glossaryTermList = new List<glossaryTerm>();


            if (GlossaryItems != null)
            {

                foreach (Item GlossaryItem in GlossaryItems)
                {
                    glossaryItem item = new glossaryItem();


                    glossaryItemList.Add(item);
                    populateGlossaryTerms(glossaryTerms, GlossaryItem);

                }
            }

            if (keyword == "*")
            {

                rptTerms.DataSource = glossaryTermList;
                rptTerms.DataBind();
            }
            if (keyword == "num")
            {
                 int value;

                 if (searchType == "1")
                 {
                     glossaryTermListResults = new List<glossaryTerm>();

                     foreach(string startWith in number.ToList())
                     {
                         List<glossaryTerm> temp;

                         temp = (from glossaryTerm data in glossaryTermList where (data.Term[0].ToString().StartsWith(startWith)) select data).ToList();

                         glossaryTermListResults.AddRange(temp);
                     }
                 }
                rptTerms.DataSource = glossaryTermListResults;
                rptTerms.DataBind();
            }
            else
            {

                 if (searchType == "1")
                 {
                     glossaryTermListResults = (from glossaryTerm data in glossaryTermList where data.Term.ToLower().StartsWith(keyword.ToLower()) select data).ToList();

                 }
                 else if (searchType == "2")
                 {
                      glossaryTermListResults = (from glossaryTerm data in glossaryTermList where data.Term.ToLower().Contains(keyword.ToLower()) select data).ToList();
                 }

               

                rptTerms.DataSource = glossaryTermListResults;
                rptTerms.DataBind();

            }
           

            
        }
        
    }
}