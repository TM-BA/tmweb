﻿using System;
using TM.Utils.Web;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class USCanadaSVGMap : ControlBase
    {
        private readonly MenuStateProvinceList _menustprolist = new MenuStateProvinceList();

        protected void Page_Load(object sender, EventArgs e)
        {
            var cacheKey = CacheKeys.FindYourHomeAndLeftNavMenuCacheKey(Sitecore.Context.GetDeviceName());
            var menuText = WebCache.Get<string>(cacheKey, () => GetRenderedMenu());

            litMenu.Text = menuText;
        }

        private string GetRenderedMenu()
        {
            _menustprolist.RenderForSVGMap = true;
            _menustprolist.CssClass = "ddlist";
            return _menustprolist.RenderAsText();
        }
    }
}