﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USCandaSearchMap.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.USCandaSearchMap" %>
<div class="searchmdl">
	<div class="lt">
		<sc:XslFile ID="menustatemarketlising" runat="server" Path="/XSL/MenuStateMarketList.xslt" />   		
	</div>
	<div id="canvas">
		<div id="paper">
		</div>							
	</div>
</div>
	<script src="/javascript/svg/raphael-1.5.2.js" type="text/javascript"></script>
	<script src="/javascript/svg/us-can-map.js" type="text/javascript"></script>	
	<script type="text/javascript">
		if (typeof jQuery == 'undefined') {
			document.write(unescape("%3Cscript src='http://code.jquery.com/jquery-latest.min.js' type='text/javascript'%3E%3C/script%3E"));
		}
		if (typeof $j == 'undefined') {
			var $j = jQuery.noConflict();
		}
	</script>
	<script src="/javascript/svg/json2.min.js" type="text/javascript"></script>
	<style type="text/css">		
		svg, tspan
		{
			text-shadow: 0 2px 2px #151515;
			filter: Shadow(Color=#151515, Direction=120, Strength=2);
		}
	</style>
	<style type="text/css">
		html, body, canvas, shape, group
		{
			margin: 0;
			padding: 0;
			border: 0;
		}
	</style>
	<script type="text/javascript">
		var isAdmin = false;
		var TaMoUSStates = {
			'AZ': 'Arizona',
			'CA': 'California',
			'CO': 'Colorado',
			'DE': 'Delaware',
			'FL': 'Florida',
			'GA': 'Georgia',
			'IL': 'Illinois',
			'MD': 'Maryland',
			'MA': 'Massachusetts',
			'MN': 'Minnesota',
			'NV': 'Nevada',
			'NJ': 'New Jersey',
			'NC': 'North Carolina',
			'OR': 'Oregon',
			'SC': 'South Carolina',
			'TX': 'Texas',
			'VA': 'Virginia',
			'WA': 'Washington'
		};
		var TaMoCANStates = {
			'ON': 'ontario'
		};
		var TaMoCANUSStateMarkets = {
			AZ: [{ "N": "Phoenix", "h": "/New-Homes/Arizona/Phoenix", "X": 275, "Y": 223, "rtl": false }, { "N": "Tucson", "h": "/New-Homes/Arizona/Tucson", "X": 317, "Y": 285, "rtl": true}],
			CA: [{ "N": "Bakersfield", "h": "/New-Homes/California/Bakersfield", "X": 271, "Y": 209, "rtl": false }, { "N": "Fresno / Central Valley", "h": "/New-Homes/California/Fresno-Central-Valley", "X": 249, "Y": 170, "rtl": false }, { "N": "Inland Empire", "h": "/New-Homes/California/Inland-Empire", "X": 301, "Y": 263, "rtl": false }, { "N": "Los Angeles / Valencia", "h": "/New-Homes/California/Los-Angeles-Valencia", "X": 258, "Y": 249, "rtl": true }, { "N": "Orange County", "h": "/New-Homes/California/Orange-County", "X": 293, "Y": 292, "rtl": true }, { "N": "Palm Springs / Coachella", "h": "/New-Homes/California/Palm-Springs-Coachella", "X": 319, "Y": 278, "rtl": false }, { "N": "Sacramento", "h": "/New-Homes/California/Sacramento", "X": 253, "Y": 96, "rtl": false }, { "N": "San Diego", "h": "/New-Homes/California/San-Diego", "X": 336, "Y": 336, "rtl": true }, { "N": "San Francisco / Bay Area", "h": "/New-Homes/California/San-Francisco-Bay-Area", "X": 215, "Y": 125, "rtl": false}],
			CO: [{ "N": "Denver", "h": "/New-Homes/Colorado/Denver", "X": 331, "Y": 129, "rtl": true}],
			DE: [{ "N": "Millsboro", "h": "/New-Homes/Delaware/Millsboro", "X": 356, "Y": 331, "rtl": false}],
			FL: [{ "N": "Clermont", "h": "/New-Homes/Florida/Clermont", "X": 396, "Y": 141, "rtl": true }, { "N": "Ft. Lauderdale", "h": "/New-Homes/Florida/Ft-Lauderdale", "X": 518, "Y": 285, "rtl": true }, { "N": "Jacksonville / St. Augustine", "h": "/New-Homes/Florida/Jacksonville-St-Augustine", "X": 400, "Y": 25, "rtl": true }, { "N": "Lakeland", "h": "/New-Homes/Florida/Lakeland", "X": 383, "Y": 156, "rtl": true }, { "N": "Melbourne/Viera", "h": "/New-Homes/Florida/Melbourne-Viera", "X": 482, "Y": 194, "rtl": true }, { "N": "Miami", "h": "/New-Homes/Florida/Miami", "X": 514, "Y": 313, "rtl": false }, { "N": "Naples / Ft. Myers", "h": "/New-Homes/Florida/Naples-Ft-Myers", "X": 402, "Y": 275, "rtl": true }, { "N": "Orlando", "h": "/New-Homes/Florida/Orlando", "X": 414, "Y": 149, "rtl": false }, { "N": "Palm Beach", "h": "/New-Homes/Florida/Palm-Beach", "X": 510, "Y": 239, "rtl": true }, { "N": "Sarasota / Manatee", "h": "/New-Homes/Florida/Sarasota-Manatee", "X": 379, "Y": 213, "rtl": true }, { "N": "Tampa", "h": "/New-Homes/Florida/Tampa", "X": 358, "Y": 180, "rtl": true}],
			GA: [{ "N": "Atlanta", "h": "/New-Homes/Georgia/Atlanta", "X": 198, "Y": 104, "rtl": false}],
			IL: [{ "N": "Chicago", "h": "/New-Homes/Illinois/Chicago", "X": 343, "Y": 28, "rtl": true}],
			MD: [{ "N": "Baltimore", "h": "/New-Homes/Maryland/Baltimore", "X": 373, "Y": 142, "rtl": true }, { "N": "MD / DC Metro", "h": "/New-Homes/Maryland/MD-DC-Metro", "X": 337, "Y": 193, "rtl": false}],
			MA: [{ "N": "Boston", "h": "/New-Homes/Massachusetts/Boston", "X": 360, "Y": 125, "rtl": true}],
			MN: [{ "N": "Minneapolis / St. Paul", "h": "/New-Homes/Minnesota/Minneapolis-St-Paul", "X": 309, "Y": 280, "rtl": true}],
			NV: [{ "N": "Las Vegas", "h": "/New-Homes/Nevada/Las-Vegas", "X": 319, "Y": 311, "rtl": false }, { "N": "Reno", "h": "/New-Homes/Nevada/Reno", "X": 205, "Y": 108, "rtl": false}],
			NJ: [{ "N": "Burlington Township", "h": "/New-Homes/New-Jersey/Burlington-Township", "X": 290, "Y": 187, "rtl": false }, { "N": "Hanover", "h": "/New-Homes/New-Jersey/Hanover", "X": 302, "Y": 71, "rtl": true }, { "N": "Mays Landing", "h": "/New-Homes/New-Jersey/Mays-Landing", "X": 301, "Y": 287, "rtl": false }, { "N": "Monroe Township", "h": "/New-Homes/New-Jersey/Monroe-Township", "X": 326, "Y": 150, "rtl": false }, { "N": "Morris Plains", "h": "/New-Homes/New-Jersey/Morris-Plains", "X": 289, "Y": 49, "rtl": true }, { "N": "Rockaway Township", "h": "/New-Homes/New-Jersey/Rockaway-Township", "X": 277, "Y": 24, "rtl": false }, { "N": "Waretown", "h": "/New-Homes/New-Jersey/Waretown", "X": 329, "Y": 270, "rtl": false }, { "N": "Weehawken", "h": "/New-Homes/New-Jersey/Weehawken", "X": 329, "Y": 62, "rtl": false}],
			NC: [{ "N": "Charlotte", "h": "/New-Homes/North-Carolina/Charlotte", "X": 219, "Y": 218, "rtl": false }, { "N": "Raleigh", "h": "/New-Homes/North-Carolina/Raleigh", "X": 391, "Y": 139, "rtl": true}],
			OR: [],
			SC: [{ "N": "Charleston", "h": "/New-Homes/South-Carolina/Charleston", "X": 363, "Y": 319, "rtl": true }, { "N": "Charlotte", "h": "/New-Homes/South-Carolina/Charlotte", "X": 251, "Y": 8, "rtl": false }, { "N": "Myrtle Beach", "h": "/New-Homes/South-Carolina/Myrtle-Beach", "X": 485, "Y": 139, "rtl": true}],
			TX: [{ "N": "Austin", "h": "/New-Homes/Texas/Austin", "X": 332, "Y": 222, "rtl": false }, { "N": "Dallas / Ft. Worth", "h": "/New-Homes/Texas/Dallas-Ft-Worth", "X": 371, "Y": 152, "rtl": true }, { "N": "Houston", "h": "/New-Homes/Texas/Houston", "X": 401, "Y": 247, "rtl": false }, { "N": "San Antonio", "h": "/New-Homes/Texas/San-Antonio", "X": 311, "Y": 252, "rtl": true}],
			VA: [{ "N": "Southern Virginia", "h": "/New-Homes/Virginia/Southern-Virginia", "X": 496, "Y": 168, "rtl": true }, { "N": "VA / DC Metro", "h": "/New-Homes/Virginia/VA-DC-Metro", "X": 407, "Y": 49, "rtl": true}],
			WA: [{ "N": "Seattle", "h": "/New-Homes/Washington/Seattle", "X": 203, "Y": 87, "rtl": false}],
			ON: [{ "N": "ontario", "h": "/New-Homes/ontario/Phoenix", "X": 275, "Y": 223, "rtl": false }, { "N": "Tucson", "h": "/New-Homes/ontario/Tucson", "X": 317, "Y": 285, "rtl": true}]
		};
		
	</script>
	<script type="text/javascript" charset="utf-8">
		var simple = false;
		function addCANUSEvents(target, stateObj, stateCode, us, R) {
			var current = null;
			target.style.cursor = "pointer";
			stateObj.click(function () {
				var thisState = TaMoCANUSStateMarkets[stateCode];
				var j = thisState.length;
				if (!isAdmin && (j < 0)) {
					if (j == 1) {
						myClick(thisState[0].h, stateCode);
					}
					else {
						myClick(absoluteUrl("/New-Homes/" + TaMoUSStates[stateCode].replace(" ", "-")), stateCode);
					}
				} else {
					showState(stateCode, us, R);
				}
			});
		}


		function showState(stateCode, us, R) {
			var marketDots;
			//add a cover so nothing else is clickable
			var shade = R.rect(0, -10, 628, 450).attr({ opacity: "1", fill: "#fff", stroke: "#fff" });

			//clone our current state so we can manipulate it with out have to track our changes for a restore
			_selectedState = us[stateCode].clone();			

			//compute scale factor
			var bounds = _selectedState.getBBox();
			var xMultiplier = 510 / bounds.width;
			var yMultiplier = 360 / bounds.height;
			var itemScale = (xMultiplier > yMultiplier) ? yMultiplier : xMultiplier;

			//compute translation so the state is centered over the us map
			_selectedState.scale(itemScale, itemScale);
			newbounds = _selectedState.getBBox();
			_selectedState.scale(1, 1);
			var dx = (285) - ((newbounds.x) + (newbounds.width / 2.0));
			var dy = (210) - ((newbounds.y) + (newbounds.height / 2.0));

			//alert(dx + " - " + dy + " - " +  itemScale);
			
			if (simple) {
				//simple way
				_selectedState.translate(dx, dy).scale(itemScale, itemScale);
				shade.attr({ opacity: "0.5" });

				//add markets to map
				marketDots = addMarket(stateCode, R);
				
				
			} else {
				//display the growing state to the user (attributes,delay,[scale easing]
				_selectedState.animate({ translation: [dx, dy], scale: [itemScale, itemScale] }, 1100, ">", function () {
					//add markets to map					
					marketDots = addMarket(stateCode, R);
					});
				shade.animateWith(_selectedState, { opacity: "0.9" }, 1100, ">");
			}



			//add the click event to the Shade so it closes the selected state
			function closeState() {
				if (simple) {
					_selectedState.remove();
					shade.remove();
					marketDots.remove();
				} else {
					marketDots.remove();
					_selectedState.animate({ scale: [1, 1], translation: [-dx, -dy] }, 500, "<", function () { _selectedState.remove(); shade.remove(); });
					shade.animateWith(_selectedState, { opacity: "0" }, 500, "<");
				}
				//$j("#stateLabel").hide();
			}

			shade.click(closeState);

			_selectedState.click(closeState);

			R.safari();
		}



		function myClick(event, stateCode) {
			var sessionValues = new Array();
			sessionValues['mapStateCode'] = stateCode;
			addWebSessionValues(sessionValues, function () { window.parent.location.href = event; });

		}
		function enableTaMoCANUSStates(st, stateName, us, R) {
			if (st[0][0] !== undefined) {
				addCANUSEvents(st[0][0], st, stateName, us, R);
			} else {
				addCANUSEvents(st[0], st, stateName, us, R);
			}
		}


		function addMarket(stateCode, localR) {
			var marketAtom;
			var fontAttr = { font: 'Bold 13px/16px Arial', 'font-size': 13, 'text-anchor': 'start', fill: "#FFF" };
			var dot;
			var mLabel;
			var halo;
			var stateMarkets = localR.set();
			var thisState = TaMoCANUSStateMarkets[stateCode];

			if (thisState) {
				var j = thisState.length;
				for (var i = 0; i < j; i++) {
					marketAtom = localR.set();
					halo = localR.rect(0, 0, 1, 1, 8);
					dot = localR.circle(thisState[i].X - 1, thisState[i].Y + 1, 5)
                .attr({ fill: "#fff", stroke: 2, "stroke": "#FFF" })
                .mouseover(function () {
                	this.prev.attr({ opacity: 1 });
                })
                .mouseout(function () {
                	this.prev.attr({ opacity: .5 });
                });
					var dest = "javascript:myClick('" + thisState[i].h + "','" + stateCode + "');";

					mLabel = localR.text(thisState[i].X + 8, thisState[i].Y + 1, thisState[i].N).attr(fontAttr)
                .attr({ href: dest })
                    .mouseover(function () {
                    	this.prev.prev.attr({ opacity: 1 });
                    })
                    .mouseout(function () {
                    	this.prev.prev.attr({ opacity: .5 });
                    });

					//use this code to set text on left or right of dot default is text on the right of the dot
					if (thisState[i].rtl) {
						var labelBounds = mLabel.getBBox();
						mLabel.attr({ translation: "" + (-labelBounds.width - 14) + ", 0" });
					}
					marketAtom.push(
                    dot,
                    mLabel
                );
					var bbox = marketAtom.getBBox();

					halo.attr({ x: bbox.x - 1, y: bbox.y - 1, width: bbox.width + 2, height: bbox.height + 2, 'stroke-width': 0, fill: "#234089", opacity: .5 });
					halo
                    .mouseover(function () {
                    	this.attr({ opacity: 1 });
                    })
                    .mouseout(function () {
                    	this.attr({ opacity: .5 });
                    });

					marketAtom.push(halo);

					if (isAdmin) {
						dot.attr({ fill: "#F00" });
						dot.drag(move, start, up);
					} else {
						marketAtom.attr({ href: dest });
						dot.attr({ href: dest });
						halo.attr({ href: dest });
					}

					stateMarkets.push(marketAtom);
				}
			}
			return stateMarkets;
		}

		var start = function () {
			// storing original coordinates
			this.ox = this.attr("cx");
			this.oy = this.attr("cy");
			this.attr({ opacity: .5 });
		},
        move = function (dx, dy) {
        	// move will be called with dx and dy
        	this.attr({ cx: this.ox + dx, cy: this.oy + dy });
        },
        up = function () {
        	// restoring state
        	this.attr({ opacity: 1, title: "new X: " + this.attr("cx") + " \nnewY: " + this.attr("cy") });
        };
		var us;
		var can;
		var R;
		window.onload = function () {
			simple = (navigator.userAgent.indexOf("Mozilla/5.0") == -1);
			R = Raphael("paper", 528, 400);	

			var attrOn = {
				fill: "45-#234089",
				stroke: "#fff",
				"stroke-width": 1,
				"stroke-linejoin": "round"
			};
			var attrOff = {
				fill: "#dfded8",
				stroke: "#fff",
				"stroke-width": 1,
				"stroke-linejoin": "round"				
			};
			var attrBox = {
				fill: "#4672a3",
				stroke: "#fff",
				"stroke-width": 1,
				"stroke-linejoin": "round",
				"style": "border:1px solid #fff"
			};

			var attrGlow = {
				width: 3,
				fill: true,				
				color: "#000"
			};

			var attrCanOn = {
				fill: "45-#d31e46",
				stroke: "#fff",
				"stroke-width": 1,
				"stroke-linejoin": "round"
			};

			us = {};
			var calloutBoxY = 0;
			var boxHeight = 30;
			var boxBottomPadding = 1;


			for (var state in usmap) {
				if (TaMoUSStates[state]) {
					us[state] = R.path(usmap[state]).attr({ title: TaMoUSStates[state] }).attr(attrOn);
					enableTaMoCANUSStates(us[state], state, us, R); 
				} else {
					us[state] = R.path(usmap[state]).attr(attrOff);
				}
			}
		
			can = {};
			for (var state in canmap) {
				
				if (TaMoCANStates[state]) {
					us[state] = R.path(canmap[state]).attr({ title: TaMoCANStates[state] }).attr(attrCanOn);
					enableTaMoCANUSStates(us[state], state, us, R);

				} else {
					us[state] = R.path(canmap[state]).attr(attrOff);
				}
			}


		};
	</script>
