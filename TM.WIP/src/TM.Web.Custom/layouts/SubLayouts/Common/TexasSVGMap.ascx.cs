﻿using System;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
	public partial class TexasSVGMap : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            menustprolist.RenderForSVGMap = true;
		}
	}
}