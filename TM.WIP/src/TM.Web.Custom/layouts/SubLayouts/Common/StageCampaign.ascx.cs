﻿using System;
using System.Collections.Generic;
using System.Linq;
using Outercore.FieldTypes.VisualList;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using TM.Domain.Entities;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class StageCampaign : ControlBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            //if we know location of user from previous visit or ip lookup 
            //then use this location's division. 

            //if there is previous visited community
            //then display that community if its still active

            //fallback to random community rule:

            //1.Random community randomized per session
            //random community for a division if last location is known

            try
            {
                var community = GetCommunityBasedOnLocation() ?? GetPrevioslyVisitedCommunity() ?? GetRandomCommunity();
                var campaigns = new List<Campaign>();

                //pick first image from the community
                var visualListField = community.Fields[SCIDs.CommunityFields.SlideShowImages];
                if (visualListField.Value.IsNotEmpty())
                {
                    var slideShowImages = (VisualListField) visualListField;
                    var slideShowmediaItems = slideShowImages.MediaItems();
                    if (slideShowmediaItems != null && slideShowmediaItems.Count > 0)
                    {
                        var fistSlideShowImage = slideShowmediaItems[0];
                        var imageSrc = fistSlideShowImage.GetMediaUrl();
                        var title = community.Fields[SCIDs.CommunityFields.CommunityName].Value;

                        var communityCityFld = community.Parent.Fields["City Name"];
                        var communityCity = communityCityFld != null && communityCityFld.HasValue
                                                ? communityCityFld.Value + ", "
                                                : string.Empty;
                        var communityStateFld = community.Parent.Parent.Parent.Fields["State Name"];
                        var communityState = communityStateFld != null && communityStateFld.HasValue
                                                 ? communityStateFld.Value
                                                 : string.Empty;
                        if (communityState.IsNotEmpty())
                        {
                            var commStateItem = SCContextDB.GetItem(communityState);
                            if (commStateItem != null)
                            {
                                var commStateFld = commStateItem.Fields["Name"];
                                communityState = commStateFld != null && commStateFld.HasValue
                                                     ? commStateFld.Value
                                                     : string.Empty;
                            }
                        }

                        var subtitle = communityCity + communityState;

                        var mainImage = new Campaign
                                            {
                                                LargeImageSrc = imageSrc + "?h=383",
                                                SmallImageSrc = imageSrc+"?w=93",
                                                Title = title,
                                                SubTitle = subtitle,
                                                TargetURL = community.GetItemUrl()

                                            };

                        campaigns.Add(mainImage);
                    }
                }

                var smallCampaigns = GetSmallStageCampaigns();

          

                campaigns.AddRange(smallCampaigns);


                rptStageImages.DataSource = campaigns;
                rptStageImages.DataBind();

                rptCarousel.DataSource = campaigns;
                rptCarousel.DataBind();
            }
            catch(Exception exception)
            {
		Log.Error("Error While generating HomePage StageCampaign",exception, this);
            }

            /*RotateDelay = SCCurrentHomeItem
                                    .Fields[SCFieldNames.HomePageFields.TabsDisplayInSeconds]
                                    .Value;*/

            //ucRotator.Render(community.ToString(), SCFieldNames.CommunityFields.SlideShowImage, width,height);

            //display same corresponding thumbnail image in small tab image

            //display community name and city state as tab title


        }

        private IEnumerable<Campaign> GetSmallStageCampaigns()
        {
            var homePageCampaigns = ((MultilistField)SCCurrentHomeItem.Fields[SCIDs.HomePage.SelectCampaigns]).GetItems();

            var smallCampaigns = new List<Campaign>();

            foreach (var homePageCampaign in homePageCampaigns)
            {
                var smallCampaignItem = homePageCampaign;
                var smallImageSrc = smallCampaignItem.Fields[SCIDs.SmallStageCampaign.SmallTabImage].GetMediaUrl();
                var largeImageSrc = smallCampaignItem.Fields[SCIDs.SmallStageCampaign.LargeStageImage].GetMediaUrl();
                var title = smallCampaignItem.Fields[SCIDs.SmallStageCampaign.TabTitle].Value;
                var subTitle = smallCampaignItem.Fields[SCIDs.SmallStageCampaign.TabSubtitle].Value;
                var targetURL = smallCampaignItem.Fields[SCIDs.SmallStageCampaign.TargetURL].GetLinkFieldPath();

                var smallImage = new Campaign
                                     {
                    SmallImageSrc = smallImageSrc,
                    LargeImageSrc = largeImageSrc,
                    Title = title,
                    SubTitle = subTitle,
                    TargetURL = targetURL
                };
                smallCampaigns.Add(smallImage);
            }
            return smallCampaigns;
        }

        private Item GetPrevioslyVisitedCommunity()
        {
            try
            {
                var lastVisitedCommunityID =
                    WebSession[SessionKeys.LastVisitedCommunity(SCCurrentDomainName)].CastAs<ID>();

                if (lastVisitedCommunityID != default(ID) && !lastVisitedCommunityID.IsNull)
                    return SCContextDB.GetItem(lastVisitedCommunityID);
            }
	    catch
	    {
		 return null;
	    }
            return null;
        }



        private Item GetCommunityBasedOnLocation()
        {
            return null;
        }

        private Item GetRandomCommunity()
        {

            var allActiveCommunities = SCContextDB.SelectItems(SCFastQueries.AllActiveCommunitiesUnder(SCCurrentHomePath));

            var allActiveCommunitiesWithImages =
                allActiveCommunities.Where(
                    x => x.Fields[SCIDs.CommunityFields.SlideShowImages].Value != string.Empty && x.Fields[SCIDs.CommunityFields.CommunityStatusField].Value != SCIDs.CommunityFields.CommunityStatuses.Closed).ToArray();

            var numberOfCommunities = allActiveCommunitiesWithImages.Length;


            //random pick is per session
            //following code check for randomIndex for the session
            //if none available creates a new randomIndex for the session
            var randomCommunityIndex = WebSession[SessionKeys.LastRandomCommunityIndex(SCCurrentDomainName)].CastAs(0);
            if (randomCommunityIndex < 1)
            {
                randomCommunityIndex = new Random().Next(1, numberOfCommunities);
                WebSession[SessionKeys.LastRandomCommunityIndex(SCCurrentDomainName)] = randomCommunityIndex;
            }

            int count = allActiveCommunitiesWithImages.Count();
            int newIndex = count == randomCommunityIndex - 1 ? 0 : randomCommunityIndex;
            if (newIndex >= count)
                newIndex = count - 1;
            var randomCommunity = allActiveCommunitiesWithImages[newIndex];

            return randomCommunity;


        }
    }
}