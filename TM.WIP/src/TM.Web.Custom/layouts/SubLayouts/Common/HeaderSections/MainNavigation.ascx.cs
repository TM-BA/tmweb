﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.Layouts.SubLayouts.Common.HeaderSections
{
    public partial class MainNavigation : ControlBase
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MultilistField mainNav = SCCurrentHomeItem.Fields[SCIDs.HomePage.MainNav];
            if (mainNav != null)
            {
                var selectedItems = mainNav.GetItems();
                var navItems = new Dictionary<string, string>();
                var homeBuying = new Dictionary<string, string>();
                foreach (var selectedItem in selectedItems)
                {
                    var link = selectedItem.Fields[SCFieldNames.NavigationItemFields.Link].Value;
                    var displayText = selectedItem.Fields[SCFieldNames.NavigationItemFields.DisplayText].Value;
                    var firstWord = displayText.Split(' ')[0];
                    if (firstWord.ToLowerInvariant().Contains("homebuying"))
                    {
                        if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
                        {
                            homeBuying.Add(link, "<img src='/images/tm/homebuying101.png' />");
                        }
                        else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
                        {
                            homeBuying.Add(link, "<img src='/images/mg/homebuying101.png' />");
                        }
                        else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
                        {
                            homeBuying.Add(link, "<img src='/images/dh/homebuying101.png' />");
                        }
                        continue;
                    }

                    var restOfDisplayText = displayText.Replace(firstWord, string.Empty).Trim();

                    //Redirect check for ABA
                    if (selectedItem.ID != SCIDs.HomePage.TM_FinanceYourHome && selectedItem.ID != SCIDs.HomePage.DH_FinanceYourHome)
                    {
                        navItems.Add("<span>{0}</span><span>{1}</span>".FormatWith(firstWord.ToUpperInvariant(), restOfDisplayText.ToUpperInvariant()), link);
                    }
                    else
                    {
                        //User has clicked the Finance menu item. See if they have previously accepted the ABA. If so, route them to the appropriate finance page.
                        HttpCookie myCookie = new HttpCookie("AcceptABA");
                        myCookie = Request.Cookies["AcceptABA"];

                        if (myCookie != null)
                        {
                            if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
                            {
                                navItems.Add("<span>{0}</span><span>{1}</span>".FormatWith(firstWord.ToUpperInvariant(), restOfDisplayText.ToUpperInvariant()), "/Finance-Your-Home/Our-Finance-Partners");
                            }
                            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
                            {
                                navItems.Add("<span>{0}</span><span>{1}</span>".FormatWith(firstWord.ToUpperInvariant(), restOfDisplayText.ToUpperInvariant()), "/Finance-With-Darling/Our-Finance-Partners");
                            }
                        }
                        else
                        {
                            navItems.Add("<span>{0}</span><span>{1}</span>".FormatWith(firstWord.ToUpperInvariant(), restOfDisplayText.ToUpperInvariant()), link);
                        }
                    }
                }

                NavItemCount = navItems.Count;
                rptNavigation.DataSource = navItems;
                rptNavigation.DataBind();
                if (homeBuying.Count > 0)
                {
                    rptHomeBuying101.DataSource = homeBuying;
                    rptHomeBuying101.DataBind();
                }
                else
                {
                    rptHomeBuying101.Visible = false;
                }
            }
        }

        protected int NavItemCount { get; set; }
    }
}