﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.Layouts.SubLayouts.Common.HeaderSections
{
    public partial class NowTrending : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var nowTrendingFieldTxt = SCCurrentHomeItem.Fields[SCIDs.Global.NowTrendingText];

            if((nowTrendingFieldTxt != null) && (!string.IsNullOrEmpty(nowTrendingFieldTxt.Value)))
            {
                litNowTrending.Text = nowTrendingFieldTxt.Value;
            }
                
                
                
        }
    }
}