﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavigation.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.LeftNavigation" %>
             
<div class="blue-bar">
    <asp:Label ID="lblNavTitle" runat="server" Text="" CssClass="blue-bar_link"></asp:Label>
</div>
   <ul class="lt-con">
    <asp:Repeater ID="rptLeftNavigation" runat="server">
        <ItemTemplate>
         <li class='<%# Eval("Style") %>'>
             <asp:HyperLink ID="hl_item" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Name") %></asp:HyperLink>
         </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<div class="content-banner">
        <asp:HyperLink ID="campaignLink1" runat="server" Target="_blank" >
         <asp:Image ID="campaignImage1" runat="server" />
            </asp:HyperLink>
</div>
<div class="content-banner">
    <asp:HyperLink ID="campaignLink2" runat="server" Target="_blank" >
        <asp:Image ID="campaignImage2" runat="server" />   
        </asp:HyperLink>
</div>




