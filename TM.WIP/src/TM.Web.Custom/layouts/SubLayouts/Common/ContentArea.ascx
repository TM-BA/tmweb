﻿<%@ Control Language="c#" AutoEventWireup="True" CodeBehind="ContentArea.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.ContentArea" %>
<asp:Panel id="pnlContentWrapper" runat="server"> 
      <sc:fieldrenderer runat="server" id="StyleBlock" fieldname="Style Block"></sc:fieldrenderer>
      <sc:fieldrenderer runat="server" id="MainContentArea" fieldname="Content Area"></sc:fieldrenderer>
</asp:Panel>
