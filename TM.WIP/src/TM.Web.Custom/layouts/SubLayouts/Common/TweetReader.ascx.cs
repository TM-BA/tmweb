﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Common;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Text;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;
using TM.Domain.Enums;

namespace TM.Web.Custom.Layouts.SubLayouts.Common
{
    public partial class TweetReader : ControlBase
    {
        string script = "";
        string TweetApi = "";
        

        protected void Page_Load(object sender, EventArgs e)
        {
         

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.HomeBuying101.Tweeter));

            Item contextItem = matchingTemplates.FirstOrDefault();

            TweetApi = contextItem.Fields["API Url"].ToString();
            TweetApi = TweetApi.Replace("USERNAME", contextItem.Fields["AccountName"].ToString());
            API.Value = TweetApi;


            Item currentItem = Sitecore.Context.Item;

            if (currentItem.Fields["Twitter Target"] != null)
            {

                if (currentItem.Fields["Twitter Target"].Value != string.Empty)
                {
                    tweetAccount.NavigateUrl = "https://twitter.com/#!/" + currentItem.Fields["Twitter Target"].ToString();
                }


            }
            else
            {
                tweetAccount.NavigateUrl = "https://twitter.com/#!/" + contextItem.Fields["AccountName"].ToString();
            }

            
         
        }
    }
}