﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteSearchResults.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.SiteSearchResults" %>
<style>
    span.title a {
        font-size: 26px;
        color: #19398a;
        padding: 10px;
        line-height: 26px;
        display: block;
    }


    div.desc {
        border-bottom: 1px solid #afac9f;
        padding: 10px;
    }
</style>
<div class="sitesearch">

    <h1>Showing  <%= Showing %> of <%= TotalResults%>  Results found for  "<%=FullTextQuery%>" </h1>
    <asp:Repeater runat="server" ID="rptSearchResults">
        <ItemTemplate>
            <%#Eval("key") %>
            <%#Eval("value") %>
        </ItemTemplate>
    </asp:Repeater>

</div>
