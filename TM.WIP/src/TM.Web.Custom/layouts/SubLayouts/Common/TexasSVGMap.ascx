﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TexasSVGMap.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.TexasSVGMap" %>

<div id="svgmap">
    <p>Please select an area where you would like to live.</p>
    <div id="canvas">
        <div id="paper" style="text-align: center">
        </div>
    </div>
    <div class="rt">
        <tm:menustateprovincelist id="menustprolist" runat="server" cssclass="ddlist" />
    </div>
    <a id="svgclose" class="close" href="#">close</a>
</div>
<script type="text/javascript">
    $j(document).ready(function () {
        $j(window).load(function () { GetTexasSvgData(); });
    });

</script>



