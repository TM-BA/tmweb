﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USCanadaSVGMap.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.USCanadaSVGMap" %>

<div id="svgmap">
    <p>Please select an area where you would like to live.</p>
    <div id="canvas">
        <div id="paper">
        </div>
    </div>
    <div class="rt">

        <asp:Literal ID="litMenu" runat="server" />

    </div>
    <a id="svgclose" class="close" href="#">close</a>
</div>
<script type="text/javascript">
    $j(document).ready(function () {
        $j(window).load(function () { GetSVGMapData(); });
    });

</script>



