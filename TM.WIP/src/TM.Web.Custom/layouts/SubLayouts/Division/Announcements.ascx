﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Announcements.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Division.Announcements" %>
<div class="sr-lf" id="divAnnouncements" runat="server" visible="false">
    <asp:Literal ID="litAnnouncements" runat="server"></asp:Literal>   
</div>

