﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyProfile.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.MyProfile" %>
<div class="gr-bk">
	<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
		<img src="<%=MycompanyLogo %>"/>
	</div>
	<p>
		Account
	</p>
</div>
<div class="create-acc">
	<h1>
		Update <%=MycompanySmall%> account</h1>
	<asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx" EnableClientScript="True"
		ShowSummary="True" DisplayMode="SingleParagraph" Enabled="False" HeaderText=""
		ShowMessageBox="False" />
	<p>
		*= required information</p>
	<div class="block1">
		<span>First Name *</span>
		<asp:TextBox runat="server" ID="txtFirstName" CssClass="tm-account" TabIndex="1"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqFirstName" CssClass="errtext" ControlToValidate="txtFirstName"
			ErrorMessage="First name is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<span>Email Address *</span>
		<asp:TextBox runat="server" ID="txtEmailAddress" CssClass="tm-account" TabIndex="3"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" CssClass="errtext"
			ControlToValidate="txtEmailAddress" ErrorMessage="Email is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<asp:RegularExpressionValidator runat="server" ID="regemail" ErrorMessage="Invalid email address format"
			ControlToValidate="txtEmailAddress" CssClass="errtext" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
		<span>Password *</span>
		<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="tm-account" TabIndex="4"></asp:TextBox>
		<%--<asp:RequiredFieldValidator runat="server" ID="reqPassword" CssClass="errtext" ControlToValidate="txtPassword"
			ErrorMessage="Password is required" EnableClientScript="false"></asp:RequiredFieldValidator>--%>
		<span>Confirm Password *</span>
		<asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" CssClass="tm-account" TabIndex="5"></asp:TextBox>
		<%--<asp:RequiredFieldValidator runat="server" ID="reqConfirmPassword" CssClass="errtext"
			ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required"
			EnableClientScript="false"></asp:RequiredFieldValidator>--%>
		<asp:CompareValidator runat="server" ID="comconfpwd" CssClass="errtext" ControlToValidate="txtPassword"
			ErrorMessage="Non-matching passwords" ControlToCompare="txtConfirmPassword"></asp:CompareValidator>
	</div>
	<!--end block1-->
	<div class="block2">
		<span>Last Name *</span>
		<asp:TextBox runat="server" ID="txtLastName" CssClass="tm-account" TabIndex="2"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqLastName" CssClass="errtext" ControlToValidate="txtLastName"
			ErrorMessage="Last name is required" EnableClientScript="True"></asp:RequiredFieldValidator>
	</div>
	<!--end block2-->
	<div class="block3">
		<span>Address 1</span>
		<asp:TextBox runat="server" ID="txtAddress1" CssClass="tm-account pro-addr" TabIndex="6"/>
		<span>Address 2</span>
		<asp:TextBox runat="server" ID="txtAddress2" CssClass="tm-account pro-addr" TabIndex="7" />
	</div>
	<!--end block3-->
	<div class="block4">
		<span>City</span>
		<asp:TextBox runat="server" ID="txtCity" CssClass="tm-account" TabIndex="8"></asp:TextBox>
		<span>Zip Code</span>
		<asp:TextBox runat="server" ID="txtZip" CssClass="tm-account" TabIndex="10"></asp:TextBox>
		<span>Phone</span>
		<asp:TextBox runat="server" ID="txtPhone" CssClass="tm-account" TabIndex="11"></asp:TextBox>
	</div>
	<div class="block5">
		<span>State/Province</span>
		<select class="tm-account" runat="server" id="ddlState" TabIndex="9">
			<option value="">Choose a state</option>
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>

            <option value="AB">Alberta</option>
            <option value="BC">British Columbia</option>
            <option value="MB">Manitoba</option>
            <option value="NB">New Brunswick</option>
            <option value="NF">Newfoundland</option>
            <option value="NT">Northwest Territories</option>
            <option value="NS">Nova Scotia</option>
            <option value="ON">Ontario</option>
            <option value="PE">Prince Edward Island</option>
            <option value="QC">Quebec</option>
            <option value="SK">Saskatchewan</option>
            <option value="YT">Yukon</option>
		</select>
		<div class="ppadd">
		</div>
		<span>Ext</span>
		<asp:TextBox runat="server" ID="txtPhoneExt" CssClass="tm-account-ext" TabIndex="12"></asp:TextBox>
	</div>
	<div class="clearfix"></div>
<asp:LinkButton runat="server" CssClass="button" Text="update<span></span>"  ID="butUpdate"  TabIndex="13" OnClick="OnClick"></asp:LinkButton>
</div> <div class="disc"> We take your privacy seriously and assure that the information
you supply will not be shared with anyone outside our company. <asp:hyperlink ID="linkPrivacyPolicy" runat="server">Please
read our Privacy Policy.</asp:hyperlink></div> 
