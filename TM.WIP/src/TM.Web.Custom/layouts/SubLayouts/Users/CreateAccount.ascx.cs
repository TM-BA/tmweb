﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Links;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class CreateAccount : ControlBase
	{
		public string Mycompany = string.Empty;
		public string MycompanySmall = string.Empty;
		public string MycompanyLogo = string.Empty;
		public string MycompanyEmail = string.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			phthankyou.Visible = false;
			phcreateaccount.Visible = true;

            if (CurrentPage.FacebookUrl.IsNotEmpty())
            {
                litSocialMedia.Text = @"<p>Visit our <a href=""{0}""><img src=""/Images/tm/icon_FB.jpg"" class=""tySocialIcn""/> Facebook</a>".FormatWith(
                    CurrentPage.FacebookUrl
                    );
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @" and <a href=""{0}""><img src=""/images/tm/iconTwitter.png""  class=""tySocialIcn"">Twitter</a> pages.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
                else
                    litSocialMedia.Text += " page.</p>";
            }
            else
            {
                if (CurrentPage.TwitterURL.IsNotEmpty())
                    litSocialMedia.Text += @"<p>Visit our <a href=""{0}""><img src=""/images/tm/iconTwitter.png"">Twitter</a> page.</p>".FormatWith(
                        CurrentPage.TwitterURL
                        );
            }


            if (CurrentPage.CurrentIHC != null)
                litIhc.Text = @"<p>Questions? Call Internet New Home Consultant<br/>{0} {1} {3} at <span class=""ihc-phone"">{2}.</span></p>".FormatWith(
                    CurrentPage.CurrentIHC["First Name"],
                        CurrentPage.CurrentIHC["Last Name"],
                        CurrentPage.CurrentIHC["Phone"],
                        CurrentPage.CurrentIHC["Regulating Authority Designation"].IsNotEmpty() ? "(" + CurrentPage.CurrentIHC["Regulating Authority Designation"] + ")" : string.Empty
                         );

            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem["Blog Url"]))
                litBlogLink.Text = "<p>Have you seen our Blog. Visit <a href='{0}'>Our Blog</a> for the latest in home trends, technology, and more.</p>".FormatWith(CurrentPage.BlogURL);

            if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentHomeItem["Financing Call to Action"]))
            {
                litPreQual.Text = @"<p>Ready to get pre-qualified? <a href=""{0}"">Start the pre-qualification</a> process.</p>".FormatWith(
                    CurrentPage.CurrentHomeItem["Financing Call to Action"]
                    );
            }

			var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

			if (myTMLogo != null && myTMLogo.HasValue)
			{
				MycompanyLogo = myTMLogo.GetMediaUrl();
			}

			switch (SCCurrentDeviceName.ToLower())
			{
				case "monarchgroup":
					MycompanySmall = "myFavs";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
					break;
				case "darlinghomes":
					MycompanySmall = "myDH";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
					break;
				default:
					MycompanySmall = "myTM";
					Mycompany = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
					break;
			}

			var searchHelper = new SearchHelper();
			var diviItems = searchHelper.GetAllCrossDivisions();
			foreach (var item in diviItems)
			{
				var listItem = new ListItem(item.Name, item.ID.ToString());

				listItem.Attributes.Add("optgroup", item.Domain.GetCurrentCompanyName());
				ddlAreaofinterest.Items.Add(listItem);
			}
			ddlAreaofinterest.Items.Insert(0, new ListItem("Choose an area of interest", "0"));
		}

		public void OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;
			string firstName = txtFirstName.Text.Trim();
			string lastName = txtLastName.Text.Trim();
			string password = txtPassword.Text;
			string email = txtEmailAddress.Text.Trim();
			string area = ddlAreaofinterest.SelectedValue;

			var device = Sitecore.Context.GetDeviceName().GetDomainFromDeviceName();

			var newUser = new TMUser(TMUserDomain.TaylorMorrison, email)
				{
					FirstName = firstName,
					LastName = lastName,
					Email = email,
					Password = password,
					AreaOfInterest = area,
					Device = device
				};

			CurrentWebUser = null;


            if (newUser.Create(GetCreateAccountTemplate()))
			{
				CurrentWebUser = newUser;
				ClientScriptManager cs = Page.ClientScript;
				cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, false);", true);
				
				phthankyou.Visible = true;
				phcreateaccount.Visible = false;

				Item item = SearchHelper.GetSelectedArea(newUser.AreaOfInterest);
				var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };
				Uri uri = HttpContext.Current.Request.Url;
				var comurl =item.GetSiteSpecificUrl(sitecoreUrlOptions, uri);
				litSelectedArea.Text = string.Format("<a style=\"text-transform:capitalize;\" href=\"{0}\" {2}>{1} Area</a>.", comurl, item.DisplayName.ToLower(), comurl.Contains(uri.Authority) ? "" : "target=_blank");

				switch (SCCurrentDeviceName.ToLower())
				{
					case "monarchgroup":
						MycompanySmall = "myFavs";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
						MycompanyEmail = Config.Settings.MG_FromEmail;
						break;
					case "darlinghomes":
						MycompanySmall = "myDH";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
						MycompanyEmail = Config.Settings.DH_FromEmail;
						break;
					default:
						MycompanySmall = "myTM";
						Mycompany = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
						MycompanyEmail = Config.Settings.TM_FromEmail;
						break;
				}


				ClientScriptManager csmgr = Page.ClientScript;
				csmgr.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(true, false);", true);

			}
			else
			{
				vlsErrorMessage.Enabled = true;
				var err = new CustomValidator
					{
						ErrorMessage =
							string.Format("User account already exists. Please <a href=\"/Account/Login\">Login</a> here."),
						IsValid = false
					};
				Page.Validators.Add(err);
			}
		}

		private string GetCreateAccountTemplate()
		{
			// Declare stringbuilder to render control to
			StringBuilder sb = new StringBuilder();

			// Load the control
			UserControl ctrl = (UserControl)LoadControl("WelcomeEmail.ascx");

			// Do stuff with ctrl here

			// Render the control into the stringbuilder
			StringWriter sw = new StringWriter(sb);
			Html32TextWriter htw = new Html32TextWriter(sw);
			ctrl.RenderControl(htw);

			// Get full body text
			return sb.ToString();
		}

		//private string WelcomeEmail(TMUser info, string emailString)
		//{
		//    string mycompany = string.Empty;
		//    string mycompanyname = string.Empty;

		//    //var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];
		//    //var mycompanylogo = string.Empty;

		//    //if (myTMLogo != null && myTMLogo.HasValue)
		//    //{
		//    //    mycompanylogo = myTMLogo.GetMediaUrl();
		//    //}

		//    switch (info.Device.ToLower())
		//    {
		//        case "monarchgroup":
		//            mycompany = "myMG";
		//            mycompanyname = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
		//            break;
		//        case "darlinghomes":
		//            mycompany = "myDH";
		//            mycompanyname = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
		//            break;
		//        default:
		//            mycompany = "myTM";
		//            mycompanyname = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
		//            break;
		//    }

		//    emailString = WebUtility.HtmlDecode(emailString);

		//    Uri uri = HttpContext.Current.Request.Url;
		//    emailString = emailString.Replace("{0}", GetCurrentAuthority()).Replace("{1}", MycompanyLogo).Replace("{2}", mycompany).Replace("{3}", info.FirstName).Replace("{4}", info.Email).Replace("{5}", mycompanyname);

		//    return emailString;
		//}

		//private string GetCurrentAuthority()
		//{
		//    Uri uri = HttpContext.Current.Request.Url;
		//    return string.Format("{0}://{1}", uri.Scheme, uri.Authority);
		//}

		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(Sitecore.Context.Site.StartPath);
			}
		}
	}
}
