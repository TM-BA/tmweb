﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.Login" %>
<div class="gr-bk">
	<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
			<img src="<%=MycompanyLogo %>"/>
		</div>
	<p>
		Account
	</p>
</div>
<div class="create-acc">
	<h1>
		Login to your <%=MycompanySmall%> account</h1>
	<p>
		Need a new account? Please <a href="/Account/Register">register now</a> to continue.</p> 
	
	<asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx"  EnableClientScript="True" ShowSummary="True" 
	DisplayMode="SingleParagraph" Enabled="True" HeaderText="We did not find a user with the email and password provided.  Please try again or request your password." ShowMessageBox="False"/>
	
	<span>Email Address *</span>
	<asp:TextBox runat="server" ID="txtUsername" CssClass="tm-account loginEnterSubmit"></asp:TextBox>	
	<asp:RequiredFieldValidator runat="server" ID="requsername" ControlToValidate="txtUsername" EnableClientScript="True"></asp:RequiredFieldValidator>
	<span>Password *</span>
	<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="tm-account loginEnterSubmit"></asp:TextBox>  
	<asp:RequiredFieldValidator runat="server" ID="reqPassword" ControlToValidate="txtPassword" EnableClientScript="True"></asp:RequiredFieldValidator>
	<div class="bl">
		<asp:LinkButton runat="server" ID="butLogin" OnClick="OnClick" CssClass="button" Text="Login<span></span>"></asp:LinkButton>
		<a href="/Account/Forgot-Password">Forgot your password?</a>
	</div>
</div>
<script type="text/javascript">
    $j(document).ready(function () {
        $j('.loginEnterSubmit').keypress(function (event) {
            if (event.which == 13) {
                $j("#<%= butLogin.ClientID %>")[0].click();
                return false;
            }

        });
    });
   
</script>
