﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BHI.Session;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
	public partial class Logout : ControlBase 
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			WebUserExtensions.Logout();
			CurrentWebUser = null;
			CurrentPage.WebSession.Logout();
			ClientScriptManager cs = Page.ClientScript;
			cs.RegisterStartupScript(GetType(), "logintext", "UpdateCredentialText(false, false);", true);
		}
	}
}
