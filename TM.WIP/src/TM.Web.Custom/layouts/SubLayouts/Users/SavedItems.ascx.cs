﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Users
{
    public partial class SavedItems : ControlBase
    {
        private static readonly FavoritesHelper _favoritesHelper = new FavoritesHelper();
        private static readonly SearchHelper _searchHelper = new SearchHelper();
        public SaveItemResults _saveItemResults;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentWebUser == null)
                Response.Redirect("/Account/Login");

            litComFav.Text = CurrentWebUser.CommunityFavorites;
            litPlanFav.Text = CurrentWebUser.PlanFavorites;
            litHfsFav.Text = CurrentWebUser.HomeforSaleFavorites;
            litAoiFav.Text = CurrentWebUser.AreaOfInterest;

            BoundCommunityCards();
        }

        private void BoundCommunityCards()
        {
            var scuser = Sitecore.Context.User;
            CurrentWebUser = scuser.HydrateToTMUser(TMUserDomain.TaylorMorrison);
            string favCom = CurrentWebUser.CommunityFavorites;
            string favPlan = CurrentWebUser.PlanFavorites;
            string favHfs = CurrentWebUser.HomeforSaleFavorites;
            string favAoi = CurrentWebUser.AreaOfInterest;
            _saveItemResults = _favoritesHelper.GetAllFavoriteItems(favCom, favPlan, favHfs, favAoi);
            if (_saveItemResults.Companies.Any()
                && (!string.IsNullOrEmpty(favCom)
                    || !string.IsNullOrEmpty(favPlan)
                    || !string.IsNullOrEmpty(favHfs)))
            {
                noComRecords.Visible = false;
                phAllfavorites.Visible = true;
                rptCompanyHeader.DataSource = _saveItemResults.Companies;
                rptCompanyHeader.DataBind();
            }
            else
            {
                noComRecords.Visible = true;
                phAllfavorites.Visible = false;
            }
        }

        protected void rptCompanyHeader_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var headerinfo = (CrossCompanyHeader)e.Item.DataItem;

                var rptCommunityCard = (Repeater)e.Item.FindControl("rptCommunityCard");
                var rptPlanCard = (Repeater)e.Item.FindControl("rptPlanCard");
                var rptHomeforSaleCard = (Repeater)e.Item.FindControl("rptHomeforSaleCard");
                var litCompanyhder = (Literal)e.Item.FindControl("litCompanyhder");
                var litCommunityLinks = (Literal)e.Item.FindControl("litCommunityLinks");
                var litPlanLinks = (Literal)e.Item.FindControl("litPlanLinks");
                var litHomeforSaleLinks = (Literal)e.Item.FindControl("litHomeforSaleLinks");
                var phComfavo = (PlaceHolder)e.Item.FindControl("phComfavo");
                var phPlanfavo = (PlaceHolder)e.Item.FindControl("phPlanfavo");
                var phHomeforSalefavo = (PlaceHolder)e.Item.FindControl("phHomeforSalefavo");

                //Community
                var cInfos = _saveItemResults.CommunityInfo != null ? _saveItemResults.CommunityInfo.Where(info => info.CompanyName == headerinfo.Company).ToList() : new List<CommunityInfo>();
                rptCommunityCard.DataSource = _searchHelper.CommunitySort(cInfos.Distinct().ToList(), "1");
                rptCommunityCard.DataBind();

                //Plans
                var pInfos = _saveItemResults.PlanInfo != null ? _saveItemResults.PlanInfo.Where(info => info.CompanyName == headerinfo.Company).ToList() : new List<PlanInfo>();
                rptPlanCard.DataSource = _searchHelper.SortbyPlanInfo(pInfos.Distinct().ToList(), "2");
                rptPlanCard.DataBind();

                //Home for sale
                var hInfos = _saveItemResults.HomeForSaleInfo != null ? _saveItemResults.HomeForSaleInfo.Where(info => info.CompanyName == headerinfo.Company).ToList() : new List<HomeForSaleInfo>();
                rptHomeforSaleCard.DataSource = _searchHelper.SortbyHomeForSaleInfo(hInfos.Distinct().ToList(), "8");
                rptHomeforSaleCard.DataBind();

                litCompanyhder.Text = headerinfo.Company;

                var clink = cInfos.Count > 0 ? string.Format("<a href=\"javascript:ScorllItem('community_{2}')\">You have <span id=\"{2}_Community_count\">{0}</span> saved communit{1}</a>", cInfos.Count, cInfos.Count == 1 ? "y" : "ies", headerinfo.Domain) : string.Empty;
                var plink = pInfos.Count > 0 ? string.Format("<a href=\"javascript:ScorllItem('plan_{2}')\">You have <span id=\"{2}_Plan_count\">{0}</span> saved plan{1}</a>", pInfos.Count, pInfos.Count == 1 ? "" : "s", headerinfo.Domain) : string.Empty;
                var hlink = hInfos.Count > 0 ? string.Format("<a href=\"javascript:ScorllItem('hfs_{2}')\">You have <span id=\"{2}_HomeForsale_count\">{0}</span> saved home{1} for sale</a>", hInfos.Count, hInfos.Count == 1 ? "" : "s", headerinfo.Domain) : string.Empty;

                var cSpan = cInfos.Count > 0 ? string.Format("<span>You have <span id=\"{2}_Community_count\">{0}</span> saved communit{1}</span>", cInfos.Count, cInfos.Count == 1 ? "y" : "ies", headerinfo.Domain) : string.Empty;
                var pSpan = pInfos.Count > 0 ? string.Format("<span>You have <span id=\"{2}_Plan_count\">{0}</span> saved plan{1}</span>", pInfos.Count, pInfos.Count == 1 ? "" : "s", headerinfo.Domain) : string.Empty;
                var hSpan = hInfos.Count > 0 ? string.Format("<span>You have <span id=\"{2}_HomeForsale_count\">{0}</span> saved home{1} for sale</span>", hInfos.Count, hInfos.Count == 1 ? "" : "s", headerinfo.Domain) : string.Empty;

                phComfavo.Visible = false;
                if (cInfos.Count > 0)
                {
                    phComfavo.Visible = true;
                    litCommunityLinks.Text = cSpan + (!string.IsNullOrWhiteSpace(plink) ? " | " + plink : "") + (!string.IsNullOrWhiteSpace(hlink) ? " | " + hlink : "");
                }

                phPlanfavo.Visible = false;
                if (pInfos.Count > 0)
                {
                    phPlanfavo.Visible = true;
                    litPlanLinks.Text = pSpan + (!string.IsNullOrWhiteSpace(hlink) ? " | " + hlink : "") + (!string.IsNullOrWhiteSpace(clink) ? " | " + clink : "");
                }

                phHomeforSalefavo.Visible = false;
                if (hInfos.Count > 0)
                {
                    phHomeforSalefavo.Visible = true;
                    litHomeforSaleLinks.Text = hSpan + (!string.IsNullOrWhiteSpace(plink) ? " | " + plink : "") + (!string.IsNullOrWhiteSpace(clink) ? " | " + clink : "");
                }
            }
        }

        public string AvalabilityText(string thisItem)
        {
            Sitecore.Data.ID thisID = null;
            if (Sitecore.Data.ID.TryParse(thisItem, out thisID))
            {
                string availabilityText;
                var inventory = SCUtils.GetItem(thisID);
                var statusForAvailability = inventory.Fields[SCIDs.HomesForSalesFields.StatusForAvailability].GetValue(string.Empty);

                if (Sitecore.Data.ID.IsID(statusForAvailability))
                {
                    availabilityText = SCContextDB.GetItem(new ID(statusForAvailability)).Fields["Name"].GetValue(string.Empty);
                }
                else
                {
                    availabilityText = GetAvailabilityFromDate();
                }

                return availabilityText != string.Empty ? "<span class='invavltxt'>{0}  </span>".FormatWith(availabilityText) : string.Empty;
            }
            return string.Empty;
        }

        private string GetAvailabilityFromDate()
        {
            var inventory = SCContextItem;
            var dateAvailable = DateUtil.ParseDateTime(inventory.Fields[SCIDs.HomesForSalesFields.AvaialabilityDate].Value,
                                                     default(DateTime));

            return dateAvailable == default(DateTime)
                                   ? string.Empty
                                   : dateAvailable.CompareTo(DateTime.Now) < 0
                   ? "Available Now!"
                                   : "Available in " + dateAvailable.ToString("MMMM");
        }
    }
}