﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PositionDetails.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Career.PositionDetails" %>


<div class="career-dt">

    <div class="position-header">
          <asp:Image ID="Career" runat="server" width="172" height="115" />
    </div>

        <h1>TAYLOR MORRISON, INC. OPEN POSITION ADVERTISEMENT</h1>
        <p><span>Job Title:</span><sc:fieldrenderer runat="server" id="fr_Title" fieldname="Title"></sc:fieldrenderer></p>
        <p><span>Job ID:</span><sc:fieldrenderer runat="server" id="fr_Jobid" fieldname="Job ID"></sc:fieldrenderer></p>
        <p><span>Specific Location:</span><asp:Label ID="lblLocation" runat="server" ></asp:Label></p>
        <p><span>Relocation Provided:</span><asp:Label ID="lblRealocationProvided" runat="server" Text=""></asp:Label></p>
        <p><span>Travel Required:</span><sc:fieldrenderer runat="server" id="fr_TravelRequired" fieldname="TravelRequired"></sc:fieldrenderer></p>
        <p><span>Job Type:</span><sc:fieldrenderer runat="server" id="fr_JobType" fieldname="JobType"></sc:fieldrenderer></p>
        <p><span> </span><sc:fieldrenderer runat="server" id="fr_Copy" fieldname="Copy"></sc:fieldrenderer></p>

    <div class="position-footer">

    <div class="iconF2-2"><a href="#"></a></div>
    <div class="iconF1-2"><a href="#"></a></div>

   <div class="sm-logo-f">
        <img src="/Images/tm/tm_logo.jpg">
    </div>


    </div>
    <div class="clearfix"></div>

</div>



