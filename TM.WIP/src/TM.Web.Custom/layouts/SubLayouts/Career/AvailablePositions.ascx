﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailablePositions.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Carrer.AvailablePositions" %>


<style type="text/css">
    .hidden {
        display: none;
    }
</style>
<div class="lt-col">
    <sc:Placeholder runat="server" ID="LeftNavigation" Key="LeftNavigation"></sc:Placeholder>
</div>
<div class="rt-col">
    <div class="content-pg">
        <sc:Placeholder runat="server" ID="ContentArea" Key="ContentArea"></sc:Placeholder>
    </div>

    <div class="hidden">

        <div class="filter-left">

            <p>Filter by City, State</p>

            <asp:DropDownList ID="ddlCityState" runat="server" AutoPostBack="True"
                OnSelectedIndexChanged="ddlCityState_SelectedIndexChanged">
            </asp:DropDownList>

        </div>

        <div class="filter-right">

            <p>Filter by Department</p>

            <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True"
                OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
            </asp:DropDownList>



        </div>



    </div>

    <asp:Repeater ID="rptAvailablePositions" runat="server"
        OnItemDataBound="rptAvailablePositions_ItemDataBound">
        <ItemTemplate>
            <div class="career-box">
                <asp:HyperLink ID="hlkTitle" runat="server" Target="_blank" CssClass="career-box-link"></asp:HyperLink>
                <asp:Label ID="PostingStartDate" runat="server" CssClass="career-date"></asp:Label>
                <asp:Label ID="locationlbl" runat="server" CssClass="career-box-label"></asp:Label>

            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="results-msg">
        <asp:Label ID="lblNoResults" runat="server" Text=""></asp:Label></div>
</div>



