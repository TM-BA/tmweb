﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace TM.Web.Custom.Layouts.SubLayouts.Career
{
    public partial class PositionDetails : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            populatePositionDetails();


        }

        private void populatePositionDetails()
        {
            string realocation = "No";
            Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;

            if (currentItem.Fields["RelocationProvided"] != null)
            {
                if(currentItem.Fields["RelocationProvided"].ToString() == "1")
                {
                    realocation = "Yes";

                } 
            }

            lblRealocationProvided.Text = realocation;

            lblLocation.Text = SCContextDB.GetItem(currentItem.Fields["Area"].ToString()).Name;

            Item parentNode = Sitecore.Context.Database.GetItem(SCCurrentHomePath);

            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = parentNode.Fields["Career Pages Header Image"];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }

            Career.ImageUrl = imageURL;
        
        }
    }
}