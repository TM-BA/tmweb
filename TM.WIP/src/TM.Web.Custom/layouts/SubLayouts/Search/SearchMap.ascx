﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchMap.ascx.cs" Inherits="TM.Web.Custom.layouts.SubLayouts.Search.SearchMap" %>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<%= GoogleMapsApiKey %>&libraries=geometry"></script>
<div id="searchmap" class="srchmap">
	<div class="mapprogress" style="display:none"><div>Loading...</div></div>	
	<div id="map_canvas" class="mapWindow">
	</div>
</div>

<script type="text/javascript">
    setTimeout(function () { $j(".mapprogress").hide(); }, 4000);
</script>