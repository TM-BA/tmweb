﻿using System;
using TM.Web.Custom.Layouts.SubLayouts;

namespace TM.Web.Custom.layouts.SubLayouts.Search
{
	public partial class Search : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            liturl.Text = SCContextItem.Paths.FullPath;
			var tmUser = CurrentWebUser;
			litcomFav.Text = string.Empty;
			if (tmUser != null)
				litcomFav.Text = tmUser.CommunityFavorites;
		}
	}
}