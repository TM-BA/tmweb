﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MG_Search.ascx.cs" Inherits="TM.Web.Custom.layouts.SubLayouts.Search.MG_Search" %>
<%@ Register TagPrefix="uc" TagName="SearchFacet" Src="SearchFacet.ascx" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="SearchMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="IHCCard" Src="~/tmwebcustom/SubLayouts/Common/IHCCard.ascx" %>
<%@ Register TagPrefix="uc" TagName="CommunitySearchCard" Src="CommunitySearchCard.ascx" %>
<script type="text/javascript">
	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		var comFav = "<asp:Literal ID='litcomFav' runat='server' />";
		$j(window).load(function () { GetCommunities(searchpath, 1, false, comFav, false); });

		$j("#searchsortby").change(function () {
			GetCommunities(searchpath, $j("#searchsortby").val(), false, comFav, false);
		});

		var container = $j("div#searchfacet");
		$j("a#searchfacet").click(
				function (event) {
					event.preventDefault();
					if (container.is(":visible")) {
						$j("a#searchfacet").css('background-image', 'url(/images/tm/arrowUp.png)');
						$j("a#searchfacet").html("see search options");
						container.slideUp(500);
					} else {
						$j("a#searchfacet").css('background-image', 'url(/images/tm/arrowDown.png)');
						$j("a#searchfacet").html("hide search options");
						container.slideDown(500);
					}
				}
		);
	});
</script>
<div class="lt-col-s">
	<section class="hm-lt_sr">
		<input type="hidden" id="hndsearchPath" value="<%= liturl.Text%>" />
		<input type="hidden" id="hndFavi" value="<%= litcomFav.Text%>" />
		<input type="hidden" id="hndRealtor" value="false" />
		<div class="leftBars">
			WELCOME!
		</div>
		<div class="sr-m-l">
			<sc:Placeholder runat="server" ID="phLeftAreaPromo" Key="LeftAreaPromo" />
			<div class="sr-lf">
				<sc:Placeholder ID="phContent" runat="server" Key="LeftContent" />
			</div>
		</div>
		<div id="srhfactbx">
			<div class="leftBars">
				<a id="searchfacet" href="#">ADVANCED SEARCH</a>
			</div>
			<uc:SearchFacet ID="ucSearchFact" runat="server" />
		</div>		
		<div class="leftBars">
			HOMES READY NOW
		</div>
	</section>
</div>
<div class="rt-col">
	<uc:SearchMap ID="ucSearchMap" runat="server" />
	<div class="legend-map">
		<ul>
			<li class="marker-1">Now Selling</li>
			<li class="marker-2">Coming Soon</li>
			<li class="marker-3">Closeout</li>
			<li class="marker-4">Design Studio</li>
		</ul>
	</div>
	<!--end of legend-map-->
	<div class="ihc">
		<ul class="ltnavbut2">
			<li><a href="#">Sign Up For Updates </a></li>
			<li id="liveChat" class="chat-btn">
				<sc:Placeholder ID="Placeholder1" runat="server" Key="LiveChat" />
			</li>
		</ul>
		<uc:IHCCard id="ucIHCCard" runat="server" />
	</div>
</div>
<uc:communitysearchcard ID="uccomcard" runat="server" />   


