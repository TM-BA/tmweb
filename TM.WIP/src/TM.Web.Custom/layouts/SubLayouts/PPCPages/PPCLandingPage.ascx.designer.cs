﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace TM.Web.Custom.Layouts.SubLayouts.PPCPages {
    
    
    public partial class PPCLandingPage {
        
        /// <summary>
        /// ltldivi control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltldivi;
        
        /// <summary>
        /// phPPCDivisionLeadForm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Sitecore.Web.UI.WebControls.Placeholder phPPCDivisionLeadForm;
        
        /// <summary>
        /// litihccard control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litihccard;
        
        /// <summary>
        /// LiveChat control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UserControl LiveChat;
        
        /// <summary>
        /// litseocontent control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litseocontent;
        
        /// <summary>
        /// litLandingPageCampaign control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litLandingPageCampaign;
        
        /// <summary>
        /// ucPPCCommunityCard control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::TM.Web.Custom.Layouts.SubLayouts.PPCPages.PPCCommunityCard ucPPCCommunityCard;
    }
}
