﻿using System;
using System.Web; 
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;

namespace TM.Web.Custom.Layouts.SubLayouts.PPCPages
{
	public partial class PPCLandingPage : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var divi = SCContextItem.Paths.FullPath.TrimUrlLastText().GetLastTextfromString('/');
			ltldivi.Text = divi;
			Item item = SCContextDB.GetItem(SCContextItem.Paths.FullPath.TrimUrlLastText());
			litseocontent.Text = item["SEO Content"];

			var searchHelper = new SearchHelper();
			var path = SCContextItem.Paths.FullPath;
			var diviitem = searchHelper.GetPpcDivision(path);

			//ihc card
			var ihcItem = searchHelper.GetIhcCardValues(diviitem);
			if (ihcItem != null)
				litihccard.Text = searchHelper.GetIHCCardDetails(ihcItem);

			//campaign
			var realtorCamp = searchHelper.GetCampaignValues(diviitem, "PPC Landing Campaign");
			if (realtorCamp != null)
				litLandingPageCampaign.Text = searchHelper.GetCampaignDetails(realtorCamp);

			CurrentPage.CurrentLiveChatSkill = diviitem[SCFieldNames.LiveChatSkillCode];

			Uri uri = HttpContext.Current.Request.Url;
			string diviPath = uri.AbsolutePath.TrimStart('/').TrimUrlLastText();
			WebSession[SessionKeys.SpecificRfiLocationKey] = diviPath;	
		}
	}
}