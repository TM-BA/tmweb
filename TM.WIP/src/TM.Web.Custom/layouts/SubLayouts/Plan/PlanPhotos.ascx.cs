﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts.Common;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanPhotos : ControlBase
    {
        protected bool IsPrint;
        public string IFPUrl
        {
            get
            {
                var url = CurrentPage.SCContextItem.Fields["IFP URL"].GetLinkFieldPath();
                if (url.IsEmpty())
                {
                    url = CurrentPage.SCContextItem.Fields["IFP URL"].Value;
                }
                if (!string.IsNullOrWhiteSpace(url) && Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    var newUrl = new Uri(url).AppendQuery("planid", SCContextItem.ID.ToShortID().ToString());
                    return newUrl;
                }
                return string.Empty;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            var rotator = (Rotator)tmrotator;

            rotator.Datasource = Sitecore.Context.Item.ID.ToString();
            rotator.LoadScripts = false;

            var page = Request["sc_device"];
            //normal request
            if (page.IsEmpty() || Request["sc_mode"] == "preview")
            {
                rotator.SubLayout = (Sublayout)(((this.Parent).Parent).Parent);
                page = Request.Url.LastSegment();
            }
            //print request
            else
            {
                rotator.SubLayout = (Sublayout)(((this.Parent)));
                page = SCUtils.GetSCParameter("p", rotator.SubLayout);
                IsPrint = true;

            }

            rotator.Fieldnames = page == "photos" ? new List<string> { "Elevation Images", "Interior" } : new List<string> { "Floor Plan" };
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            divIFPPromo.Visible = (!IsPrint && IFPUrl.IsNotEmpty());
            ulIFPPromo.Visible = divIFPPromo.Visible;

            if (Request.Url.LastSegment() == "photos")
            {
                var titleTemplateKey = "Home Photos Title Template";
                var pageTitleKey = "Home Photos Page Title";
                if (SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage)
                {
                    titleTemplateKey = "Floorplan Photos Title Template";
                    pageTitleKey = "Floorplan Photos Page Title";
                }

                var pageTitle = string.IsNullOrWhiteSpace(SCContextItem[titleTemplateKey])
                                           ? CurrentPage.GetTagsReplaced(SCContextItem[pageTitleKey].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject)
                                           : CurrentPage.GetTagsReplaced(SCContextItem[titleTemplateKey].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                if (!string.IsNullOrWhiteSpace(pageTitle))
                    CurrentPage.Title = pageTitle;
            }
        }
    }
}
