﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlansAlsoAvailable.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlansAlsoAvialable" %>
<div class="viewplanbox2" >
    <div class="planlist2"  runat="server" id="divViewPlanBox">
        <span>The <%=PlanName %> is also available in the following communities:</span>
        <ul class="planlist-bold show-mobile">
            <li>Community</li>
            <li>Address</li>
            <li>Price</li>
            <li>Bed</li>
            <li>Bath</li>
        </ul>
        <ul class="planlist-bold hide-mobile">
            <li>Community Name</li>
            <li>Address</li>
            <li>Price</li>
            <li>Bedrooms</li>
            <li>Bathrooms</li>
        </ul>
        <asp:Repeater runat="server" ID="rptPlansAlsoAvailableCommunities">
            <ItemTemplate>
                <ul>
                    <li><a onclick="TM.Common.GAlogevent('Details', 'Click', 'CrossSellCommunityLink')" href="<%#Eval("CommunityLink")%>">
                        <%#Eval("CommunityName")%>
                    </a></li>
                    <li>
                        <%#Eval("CommunityAddress")%></li>
                    <li>
                        <%#Eval("CommunityPriceText")%></li>
                    <li>
                        <%#Eval("CommunityBedroomRange")%></li>
                    <li>
                        <%#Eval("CommunityBathroomRange")%></li>
                </ul>
            </ItemTemplate>
        </asp:Repeater>
    </div>
 
<div id="divNoPlanOrHfsMessage" runat="server" visible="false">
   <% 
         if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
        { %>
       This plan is only available in this community. <a href="<%=CurrentPage.SCCurrentItemUrl %>/request-information/"> Contact us </a> to learn more about <%=CurrentPage.CurrentCommunity["Community Name"]%> or other great Taylor Morrison communities.
     
     <%} else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes) { %>
         This plan is only available in this community. <a href="<%=CurrentPage.SCCurrentItemUrl %>/request-information/"> Contact us </a> to learn more about <%=CurrentPage.CurrentCommunity["Community Name"]%> or other great Darling Homes communities. 
      <%}
        else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
        { %>
          This plan is only available in this community. <a href="<%=Request.Url.AbsolutePath %>/request-information/"> Contact us </a> to learn more about <%=CurrentPage.CurrentCommunity["Community Name"]%> or other great Monarch communities.
      <%} %>

</div>
</div>
