﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using Sitecore.StringExtensions;
namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanMenu : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            liFloorPlans.Visible = AreFloorPlansAvailable;
            liPhotos.Visible = ArePhotosAvailable;
            liOptions.Visible = IsIFPAvailable;
            liVirtualTour.Visible = AreVirtualToursAvailable;
            liPlanBrochure.Visible = BrochureTabTitle.IsNotEmpty();
            aPlanBrochure.InnerText = BrochureTabTitle;
            aPlanBrochure.HRef = BrochureImageUrl;

            if (AreVirtualToursAvailable)
            {
                var virtualTourFld = SCContextItem.Fields[SCIDs.PlanBaseFields.VirtualToursMasterPlanID];
                if (virtualTourFld.HasValue && Sitecore.Data.ID.IsID(virtualTourFld.Value))
                {
                    var virutalTourItem = SCContextDB.GetItem(new ID(virtualTourFld.Value));
                    if (virutalTourItem != null)
                    {
                        var virtualTourUrl = virutalTourItem.Fields[SCIDs.VirtualTours.VirtualTourUrl].GetLinkFieldPath();
                        aVirtualTour.HRef = virtualTourUrl;
                    }
                }
            }
        }

        protected bool AreVirtualToursAvailable
        {
            get
            {

                var virtualTourStatus = SCContextItem.Fields[SCIDs.PlanBaseFields.VirtualTourStatus];
                var virtualTour = SCContextItem.Fields[SCIDs.PlanBaseFields.VirtualToursMasterPlanID];

                var isVirtualTourActive =
                        virtualTourStatus != null &&
                        virtualTourStatus.Value == SCIDs.GlobalSharedFieldValues.RecordStatus.Active.ToString();

                var doesVirtualTourHaveValue =
                    virtualTour != null && virtualTour.HasValue && virtualTour.Value.IsNotEmpty();

                return isVirtualTourActive && doesVirtualTourHaveValue;

            }
        }

        protected bool IsIFPAvailable
        {
            get
            {
                return IFPUrl.IsNotEmpty();
            }
        }


        private bool AreFloorPlansAvailable
        {
            get
            {
                var floorPlanField = SCContextItem.Fields[SCIDs.PlanBaseFields.FloorPlan];
                return floorPlanField != null && floorPlanField.HasValue && floorPlanField.Value.IsNotEmpty();
            }
        }

        private bool ArePhotosAvailable
        {
            get
            {
                var elevations = SCContextItem.Fields[SCIDs.PlanBaseFields.ElevationImages];
                var interiors = SCContextItem.Fields[SCIDs.PlanBaseFields.Interior];

                return (elevations != null && elevations.HasValue && elevations.Value.IsNotEmpty()) || (interiors != null && interiors.HasValue && interiors.Value.IsNotEmpty());
            }
        }

        public string IFPUrl
        {
            get
            {
                var url = CurrentPage.SCContextItem.Fields["IFP URL"].GetLinkFieldPath();
                if (url.IsEmpty())
                {
                    url = CurrentPage.SCContextItem.Fields["IFP URL"].Value;
                }
                if (!string.IsNullOrWhiteSpace(url) && Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                {
                    var newUrl = new Uri(url).AppendQuery("planid", SCContextItem.ID.ToShortID().ToString());
                    return newUrl;
                }
                return string.Empty;
            }
        }

        public string BrochureImageUrl
        {
            get
            {
                var imageField = CurrentPage.SCContextItem.Fields["Brochure Image"];
                if (imageField != null)
                {
                    return imageField.GetLinkFieldPath();
                }
                return string.Empty;
            }
        }

        public string BrochureTabTitle
        {
            get
            {
                var titleField = CurrentPage.SCContextItem.Fields["Tab Title"];
                if (titleField != null) {
                    return titleField.Value;
                }
                return string.Empty;
            }
        }
    }
}
