﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanTourOptions : ControlBase
    {
        protected bool IsPlan
        {
            get { return SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage; }
        }
        
        public string LegacyPlanID
        {
            get
            {
                return IsPlan ? SCContextItem["Plan Legacy ID"] : SCContextItem["Home for Sale Legacy ID"];
            }
        }

        public string TourType
        {
            get { return IsPlan ? "p" : "s"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}