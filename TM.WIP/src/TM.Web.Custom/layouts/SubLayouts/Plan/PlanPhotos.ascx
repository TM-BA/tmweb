﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlanPhotos.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlanPhotos" %>
<%@ Import Namespace="TM.Web.Custom.SCHelpers" %>
<%@Register tagPrefix="tm" tagName="rotator" src="/tmwebcustom/sublayouts/common/rotator.ascx" %>
<div class="photos-sdshow">
                <!--<h1> Photos </h1>-->
		 <div class="rotatorControl">
                <tm:rotator runat="server" id="tmrotator"></tm:rotator>
		</div>
                <div class="options" id="divIFPPromo" runat="server">
                  <a href="<%=IFPUrl %>"  target="_blank"><img src="/images/tm/placeholder-rotator.jpg"></a>
                </div>
		 <ul class="personalize" id="ulIFPPromo" runat="server">
		  <li><a href="<%=IFPUrl%>">Personalize This Home</a></li>
		 </ul>
	    
                <div id="photoMain" class="pho-main" style="position:relative;">
                    <span id="phomain-pinterest"  class="st_pinterest_large" displayText='Pinterest' style="position:absolute;top:10px;left:50px"></span>
		    <img id="main-img" />
		     <strong id="imgTitle"></strong>
                </div>
            
 </div>
