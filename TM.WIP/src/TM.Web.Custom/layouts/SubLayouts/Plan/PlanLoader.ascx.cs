﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanLoader : ControlBase
    {
        protected bool IsCurrentItemAPlan;

        protected void Page_Load(object sender, EventArgs e)
        {
            var pageToLoad = Request.Url.LastSegment();
            LoadPage(pageToLoad);
        }

        private void LoadPage(string page)
        {
            var currentPlanHFS = SCContextItem;
            IsCurrentItemAPlan = currentPlanHFS.TemplateID == SCIDs.TemplateIds.PlanPage;

            HtmlLink canonical = new HtmlLink();
            canonical.Attributes.Add("rel", "canonical");
            canonical.Href = Request.Url.AbsoluteUri.Remove(Request.Url.AbsoluteUri.Length - (Request.Url.Segments.Last().Length + 1));

            switch (page.ToLower())
            {
                case "photos":
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanPhotos.ascx"));
                    CurrentPage.FindControl("phHeadSection").Controls.Add(canonical);
                    break;
                case "locations":
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/HomesForSale.ascx"));
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlansAlsoAvailable.ascx"));
                    CurrentPage.Title = CurrentPage.GetTagsReplaced(currentPlanHFS["Locations Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                    CurrentPage.FindControl("phHeadSection").Controls.Add(canonical);
                    break;
                case "hsh":
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Community/HSH.ascx"));
                    phPlanContent.Controls.Remove(LoadControl("/tmwebcustom/sublayouts/Plan/PlanMenu.ascx"));
                    break;
                case "floorplans":
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanPhotos.ascx"));
                    if (IsCurrentItemAPlan)
                        CurrentPage.Title = CurrentPage.GetTagsReplaced(currentPlanHFS["Floor Plans Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                    else
                        CurrentPage.Title = CurrentPage.GetTagsReplaced(currentPlanHFS["Floor Plan Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                    CurrentPage.FindControl("phHeadSection").Controls.Add(canonical);
                    break;
                default:
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanPhotos.ascx"));
                    var description = CurrentPage.GetTagsReplaced(currentPlanHFS["Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                    if (!string.IsNullOrWhiteSpace(description))
                        CurrentPage.MetaDescription = description;
                    break;
                case "options":
                    phPlanContent.Controls.Add(LoadControl("/tmwebcustom/sublayouts/Plan/PlanTourOptions.ascx"));
                    break;
            }
        }
    }
}