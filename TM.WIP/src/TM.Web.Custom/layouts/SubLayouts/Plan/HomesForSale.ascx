﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomesForSale.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.HomesForSale" %>

<div class="viewplanbox" runat="server" id="divViewPlanBox" >
    <div class="planlist">
        <span>
           The <%=PlanName %>
            is for sale at the following locations:</span>
        <ul class="planlist-bold show-mobile">
            <li>Address</li>
            <li>Price</li>
            <li>Bed</li>
            <li>Bath</li>
            

            <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
            { %> <li>Stor</li> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
            {  %>  <li>Stor</li> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
            {  %>  <li>Stor</li> <% } %>

            <li>Gar</li>
            <li>Avail</li>
            <li>&nbsp;</li>
        </ul>
        <ul class="planlist-bold hide-mobile">
            <li>Address</li>
            <li>Price</li>
            <li>Bed</li>
            <li>Bath</li>
            

            <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
            { %> <li>Stories</li> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
            {  %>  <li>Storeys</li> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
            {  %>  <li>Stories</li> <% } %>

            <li>Garage</li>
            <li>Availability</li>
            <li>&nbsp;</li>
        </ul>
        <asp:Repeater runat="server" ID="rptHomesForSale">
            <ItemTemplate>
                <ul>
                    <li><a onclick="TM.Common.GAlogevent('Details', 'Click', 'HomeAddressLink')" href="<%#Eval("Link") %>"><%#Eval("Address") %></a></li>
                    <li><%#Eval("Price") %></li>
                    <li><%#Eval("Bed") %></li>
                    <li><%#Eval("Bath") %></li>
                    <li><%#Eval("Stories") %></li>
                    <li><%#Eval("Garage") %></li>
                    <li><%#Eval("Availability")%></li>
                    <li><a onclick="TM.Common.GAlogevent('Details', 'Click', 'ScheduleAppt')" href="<%#Eval("Link")%>/request-information/">Schedule Appointment to View </a></li>
                </ul>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
