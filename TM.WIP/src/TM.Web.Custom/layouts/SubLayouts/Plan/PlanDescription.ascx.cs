﻿using System;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Search.LuceneQueries;
using System.Text;

namespace TM.Web.Custom.Layouts.SubLayouts.Plan
{
    public partial class PlanDescription : ControlBase
    {
        private const string PIPE_DELIMETER = " | ";
        protected string PlanName { get; set; }
        protected string PlanInfo { get; set; }
        protected string Description { get; set; }
        protected string TourOptionsText { get; set; }

        protected string CommunityRequestInfo
        {
            get { return SCContextItem.Parent.GetItemUrl() + "/request-information/"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            var isCurrentItemAPlan = SCContextItem.TemplateID == SCIDs.TemplateIds.PlanPage;
            var homeAttributes = new HomeAttributes().GetHomeAttributes();
            var infoPrefix = isCurrentItemAPlan ? "From" : AvalabilityText();

            const string SPACE = " ";
            string infoPrefixTemplate = "{0} ";
            string priceTemplate = "{1:c0}";
            string squareFootTemplate = "{2:n0} Sq. Ft.";
            string bedroomsTemplate = "{3:n0} Bedrooms";
            string bathroomsTemplate = "{4} Baths";
            string halfBathroomsTemplate = "{5} Half Baths";
            string garageTemplate = "{6:n0} Garage";
            string storiesTemplate = "{7:n0}";

            string storiesText = string.Empty;
            storiesText = homeAttributes.Stories == 1 ? SPACE + "Story" : SPACE + "Stories";
            

            var price = string.Empty;
            if (isCurrentItemAPlan && !string.IsNullOrWhiteSpace(homeAttributes.PriceText))
            {
                price = homeAttributes.PriceText;
                infoPrefix = string.Empty;
            }
            else
            {
                price = homeAttributes.Price.ToString("c0");
            }

            StringBuilder planInfoSb = new StringBuilder();

            //info text and pricing
            planInfoSb.Append(infoPrefixTemplate);
            planInfoSb.Append(priceTemplate);
            planInfoSb.Append(PIPE_DELIMETER);

            //square footage
            planInfoSb.Append(squareFootTemplate);
            planInfoSb.Append(PIPE_DELIMETER);

            //bedrooms
            planInfoSb.Append(bedroomsTemplate);
            planInfoSb.Append(PIPE_DELIMETER);

            //bathrooms
            planInfoSb.Append(bathroomsTemplate);
            planInfoSb.Append(PIPE_DELIMETER);

            //half baths (if any -- show)
            if (homeAttributes.HalfBaths > 0)
            {
                planInfoSb.Append(halfBathroomsTemplate);
                planInfoSb.Append(PIPE_DELIMETER);
            }

            //garage
            planInfoSb.Append(garageTemplate);
            planInfoSb.Append(PIPE_DELIMETER);

            //stories
            planInfoSb.Append(storiesTemplate);
            planInfoSb.Append(storiesText);

            string planInfo = planInfoSb.ToString().FormatWith(
                                                                infoPrefix,
                                                                price,
                                                                homeAttributes.SqFt,
                                                                homeAttributes.Bedrooms,
                                                                homeAttributes.Bathrooms,
                                                                homeAttributes.HalfBaths,
                                                                homeAttributes.Garage,
                                                                homeAttributes.Stories
                                                              );


            //clean empty values
            var regex = new System.Text.RegularExpressions.Regex(@"\| 0\.*0* \d*\w*");
            PlanInfo = regex.Replace(planInfo, string.Empty);

            Description = homeAttributes.Description;

            PlanName = isCurrentItemAPlan
                           ? SCContextItem[SCIDs.PlanFields.PlanName]
                           : GetInventoryName();


            if (isCurrentItemAPlan)
                SetTourOptionsText();
            else if (SCContextItem.Fields[SCFieldNames.PlanPageFields.IsModelHome].Value == "1")
                ulScheduleAnAppt.Visible = true;
            else
                ulScheduleAnAppt.Visible = false;
        }

        private void SetTourOptionsText()
        {
            Item nearByCommunity;
            Item home;
            var homeTourOptions = new PlanQueries().GetHomeTourOption(SCContextItem, CurrentPage.CurrentDivision,
                                                                    CurrentPage.CurrentCommunity, out nearByCommunity, out home);
            switch (homeTourOptions)
            {
                case HomeTourOption.AvailableAsModelHome:
                    TourOptionsText = "The {0} is available to tour today in a model home"
                              .FormatWith(PlanName.TrimStart("the ", StringComparison.OrdinalIgnoreCase));
                    break;
                case HomeTourOption.AvailableAsModelHomeInNearByCommunity:
                    TourOptionsText = "A model home is available in <a href=\"{1}\\floor-plans\" target=\"_blank\">{0}</a>"
                                       .FormatWith(
                                           nearByCommunity.Fields[SCIDs.CommunityFields.CommunityName].Value,
                                           nearByCommunity.GetItemUrl());

                    ulScheduleAnAppt.Visible = false;
                    break;
                case HomeTourOption.AvailableAsInventory:
                    TourOptionsText = "The {0} is available to tour today in a home for sale."
                                       .FormatWith(PlanName.TrimStart("the ", StringComparison.OrdinalIgnoreCase));
                    ulScheduleAnAppt.Visible = false;
                    break;
                default:
                    TourOptionsText = string.Empty;
                    ulScheduleAnAppt.Visible = false;
                    break;
            }

        }

        private string GetInventoryName()
        {
            var lotNumber = SCContextItem.DisplayName.Split('-');
            var lotNumberStr = string.Empty;
            if (lotNumber.Length > 1)
            {
                lotNumberStr = "(lot {0})".FormatWith(lotNumber[1]);
            }
            var currentCity = CurrentPage.CurrentCity.GetItemName();
            var currentState = CurrentPage.CurrentState.GetItemName(true);
            var zipCode = CurrentPage.CurrentCommunity["Zip or Postal Code"];
            var address1 = SCContextItem[SCIDs.HomesForSalesFields.StreetAddress1];
            if (address1.IsNotEmpty())
            {
                address1 = address1.Trim();
            }

            var address2 = "{0}, {1} {2}".FormatWith(currentCity, currentState, zipCode);
            return ("{0} at {1}, {2} <span class='invLot'>{3}</span>").FormatWith(
                SCContextItem.Parent[SCIDs.PlanFields.PlanName],
                address1,
                address2,
                lotNumberStr);
        }

        private string AvalabilityText()
        {
            string availabilityText;
            var inventory = SCContextItem;
            var statusForAvailability = inventory.Fields[SCIDs.HomesForSalesFields.StatusForAvailability].GetValue(string.Empty);

            if (Sitecore.Data.ID.IsID(statusForAvailability))
            {
                availabilityText = SCContextDB.GetItem(new ID(statusForAvailability)).Fields["Name"].GetValue(string.Empty);
            }
            else
            {
                availabilityText = GetAvailabilityFromDate();
            }

            return availabilityText != string.Empty ? "<span class='invavltxt'>{0}  </span>".FormatWith(availabilityText) : string.Empty;

        }

        private string GetAvailabilityFromDate()
        {
            var inventory = SCContextItem;
            var dateAvailable = DateUtil.ParseDateTime(inventory.Fields[SCIDs.HomesForSalesFields.AvaialabilityDate].Value,
                                                     default(DateTime));

            return dateAvailable == default(DateTime)
                                   ? string.Empty
                                   : dateAvailable.CompareTo(DateTime.Now) < 0
                   ? "Available Now!"
                                   : "Available in " + dateAvailable.ToString("MMMM");
        }


    }
}