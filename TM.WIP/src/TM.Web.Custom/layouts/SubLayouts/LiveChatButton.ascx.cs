﻿using System;
using TM.Web.Custom.Constants;
using TM.Web.Custom.layouts.SubLayouts;
using Sitecore.Data.Items;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;
using TM.Utils.Extensions;
using System.Linq;
using System.Collections.Generic;

namespace TM.Web.Custom.Layouts.SubLayouts
{
    public partial class LiveChatButton : ControlBase
    {
        public string LivePersonSkill;
        private string _rfiLocation;
        public string RFILocation
        {
            get
            {
                if (_rfiLocation == null)
                {
                    var specificRfiLocation = WebSession[SessionKeys.SpecificRfiLocationKey].CastAs<string>();

                    IEnumerable<Item> itemAncestors = CurrentItem.GetAncestors();
                    CurrentItem = itemAncestors.SingleOrDefault(x => x.IsDerivedFromTemplate(SCIDs.TemplateIds.CommunityPage)) ?? CurrentItem;

                    if (!string.IsNullOrEmpty(specificRfiLocation))
                    {
                        WebSession[SessionKeys.SpecificRfiLocationKey] = string.Empty;

                        if (CurrentItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
                        {
                            _rfiLocation = specificRfiLocation + "/request-information/?cid=" + CurrentItem.Fields["Community Legacy ID"].Value;
                        }
                        else
                        {
                            _rfiLocation = specificRfiLocation + "/request-information/";
                        }
                    }
                    else
                    {
                        if (CurrentItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
                        {
                            _rfiLocation = CurrentPage.SCVirtualItemUrl + "/request-information/?cid=" + CurrentItem.Fields["Community Legacy ID"].Value;
                        }
                        else
                        {
                            _rfiLocation = CurrentPage.SCVirtualItemUrl + "/request-information/";
                        }
                    }
                }
                return _rfiLocation;
            }
            set
            {
                _rfiLocation = value;
            }
        }

        public string serverName
        {
            get
            {
                return Page.Request.Url.Host;
            }
        }
        Item _currentItem;
        Item CurrentItem
        {
            get
            {
                if (_currentItem == null)
                    _currentItem = CurrentPage.SCContextItem;
                return _currentItem;
            }
            set
            {
                _currentItem = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //string skill = CurrentItem[SCFieldNames.LiveChatSkillCode];             
            LivePersonSkill = CurrentPage.CurrentLiveChatSkill;//skill;
            if (string.IsNullOrWhiteSpace(LivePersonSkill))
            {
                //if there is no skill then livePerson tells you the operator is available. so we manually hide the liveChat panel if there is no defined skill
                pnLiveChat.Visible = false;
            }
        }
    }
}