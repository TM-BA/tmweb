﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Express;
using Sitecore.Form.Web.UI.Controls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders
{
    public partial class MapComponent : ControlBase
    {

        public string CommunityJSON { get; set; }
        public List<CommunityInfo> CommunityInfosList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["GetDirectionsItemID"] = null;
            
            Item source = DataSource;
            if (source != null)
            {
                MultilistField communityList = source.Fields["Community List"];
                var searchHelper = new SearchHelper();
                var listCommunity = new List<Item>();
                foreach (Item item in communityList.GetItems())
                {
                    Item community = SCContextDB.GetItem(new Sitecore.Data.ID(item.ID.ToString()));
                    if (community["Status for Search"] == SCIDs.StatusIds.Status.Active)
                        listCommunity.Add(community);
                    
                }

                CommunityInfosList = searchHelper.HydrateCommunitiesByItems(listCommunity, "", "", false);
            }
            CommunityJSON = CommunityInfosList.ToJSON();
        }

        protected void btnGetDirections_Click(object sender, EventArgs e)
        {
            var itemID = hiddenItemId.Value;
            Session["GetDirectionsItemID"] = itemID;

            var query = (from c in CommunityInfosList
                where c.CommunityID.ToString() == itemID
                select c).FirstOrDefault();

            Response.Redirect("Get-Directions?lat=" + query.CommunityLatitude + "&long=" + query.CommunityLongitude, true);
        }
    }
}