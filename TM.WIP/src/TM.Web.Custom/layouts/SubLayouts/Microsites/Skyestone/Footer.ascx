﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone.Footer" %>
<div class="ft-bar" <asp:Literal ID="Class1" runat="server"></asp:Literal>>
</div>
<div class="footer" <asp:Literal ID="Class2" runat="server"></asp:Literal> >
    <div class="logo-footer">
        <asp:Literal ID="Logo" runat="server"></asp:Literal>
    </div>
    <div class="line">
        
    </div>
    <asp:Literal ID="FooterLinks" runat="server"></asp:Literal>
    <div class="ft-rgt">
        <asp:HyperLink ID="Facebook" runat="server" CssClass="icon1"></asp:HyperLink>
        <asp:HyperLink ID="Twitter" runat="server" CssClass="icon2"></asp:HyperLink>
        <asp:HyperLink ID="Pinterest" runat="server" CssClass="icon3"></asp:HyperLink>
        <asp:HyperLink ID="Instagram" runat="server" CssClass="icon4"></asp:HyperLink>
        <asp:HyperLink ID="GooglePlus" runat="server" CssClass="icon5"></asp:HyperLink>
        <p>
            <asp:Literal ID="Address" runat="server"></asp:Literal>
            <br />
            <asp:Literal ID="Phone" runat="server"></asp:Literal>
        </p>
    </div>
    <div class="footer-text">
        <p>
            <asp:Literal ID="Disclosure" runat="server"></asp:Literal>
            <br />
            <br />
            <asp:Literal ID="Copyright" runat="server"></asp:Literal>
        </p>
    </div>
</div>
