﻿using System;
using Sitecore.Data;

namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone
{
    public partial class Body : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Slider.Text = string.Empty;
            var x = this.DataSource;
            //x.Fields["First Name"].Value
            if (!string.IsNullOrWhiteSpace(SCCurrentHomeItem.Fields["Slider"].Value))
            {
                var imgIds = SCCurrentHomeItem.Fields["Slider"].Value.Split('|');
                foreach (var imgId in imgIds)
                {
                    var image = SCContextDB.GetItem(new ID(imgId));
                    Slider.Text += string.Format("<li><img src='{0}'></img></li>", Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                }

            }
            TheIdeaText.Text = SCCurrentHomeItem.Fields["The Idea Text"].Value;
            TheIdeaTitle.Text = SCCurrentHomeItem.Fields["The Idea Title"].Value;
            TheExperience.Text = SCCurrentHomeItem.Fields["The Experience"].Value;
            TheExperienceTitle.Text = SCCurrentHomeItem.Fields["The Experience Title"].Value;

        }
    }
}