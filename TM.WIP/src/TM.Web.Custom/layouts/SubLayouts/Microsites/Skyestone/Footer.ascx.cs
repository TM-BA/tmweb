﻿using System;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone
{
    public partial class Footer : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Address.Text = SCCurrentHomeItem.Fields["Address"].Value;
            Phone.Text = SCCurrentHomeItem.Fields["Phone"].Value;
            Disclosure.Text = SCCurrentHomeItem.Fields["Disclosure"].Value;
            Copyright.Text = SCCurrentHomeItem.Fields["Copyright"].Value;

            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Facebook"].Value))
                Facebook.NavigateUrl = SCCurrentHomeItem.Fields["Facebook"].Value;
            else
                Facebook.Visible = false;

            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Twitter"].Value))
                Twitter.NavigateUrl = SCCurrentHomeItem.Fields["Twitter"].Value;
            else
                Twitter.Visible = false;

            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Pinterest"].Value))
                Pinterest.NavigateUrl = SCCurrentHomeItem.Fields["Pinterest"].Value;
            else
                Pinterest.Visible = false;

            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Instagram"].Value))
                Instagram.NavigateUrl = SCCurrentHomeItem.Fields["Instagram"].Value;
            else
                Instagram.Visible = false;

            if (!string.IsNullOrEmpty(SCCurrentHomeItem.Fields["Google Plus"].Value))
                GooglePlus.NavigateUrl = SCCurrentHomeItem.Fields["Google Plus"].Value;
            else
                GooglePlus.Visible = false;

            Logo.Text = string.Format("<img src='{0}'></img>", SCCurrentHomeItem.Fields["Logo"].GetMediaUrl());
            FooterLinks.Text = SCCurrentHomeItem.Fields["Footer Links"].Value;
            Class1.Text = string.Empty;
            Class2.Text = string.Empty;
            if (!(SCCurrentItemPath == SCCurrentHomePath))
            {
                var format = string.Format("style='{0}'", "margin:0");
                Class1.Text = format;
                Class2.Text = format;

            }

            

        }
    }
}