﻿using System;

namespace TM.Web.Custom.Layouts.SubLayouts.Microsites.AdoraTrails
{
    public partial class Header : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var item = Sitecore.Context.Item;
            var redirect = item["Redirect Url"];
            if (!string.IsNullOrEmpty(redirect))
            {
                Response.Redirect(redirect);
            }
        }

        protected string HeaderClass
        {
            get
            {
                var cssClass = Sitecore.Context.Item["HeaderClass"];
                if (string.IsNullOrWhiteSpace(cssClass))
                    return Sitecore.Context.Item.Name.ToLowerInvariant().Replace(" ", "-");
                return cssClass;
            }
        }
    }
}