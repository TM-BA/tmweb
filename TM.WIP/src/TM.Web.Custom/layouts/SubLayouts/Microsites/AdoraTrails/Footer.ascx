﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.AdoraTrails.Footer" %>
<div id="footer">
    <div class="body">
        <div id="content-footer">
            <div class="column">
                <a href="/"><span>Home</span></a>
            </div>
            <div class="column">
                <span style="whitespace:nowrap;">Community Amenities</span>
                <ul class="navigation">
                    <li><a href="/community-amenities/overview">Overview &amp; Site Map</a></li>
                    <li><a href="/community-amenities/neighborhood-clubhouse">Neighborhood Clubhouse</a></li>
                    <li><a href="/community-amenities/outdoor-recreation">Outdoor Recreation</a></li>
                    <li><a href="/community-amenities/on-site-elementary">On-Site Elementary</a></li>
                    <li><a href="/community-amenities/event-gallery">Event Gallery</a></li>
                </ul>
            </div>
            <div class="column">
                <span style="whitespace:nowrap;">Area Information</span>
                <ul class="navigation">
                    <li><a href="/area-information/town-of-gilbert">Town of Gilbert</a></li>
                    <li><a href="/area-information/shopping-and-dining">Shopping &amp; Dining</a></li>
                    <li><a href="/area-information/golf">Golf</a></li>
                    <li><a href="/area-information/entertainment">Entertainment</a></li>
                    <li><a href="/area-information/schools">Schools</a></li>
                </ul>
            </div>
            <div class="column">
                <span style="whitespace:nowrap;">Find Your Home</span>
                <ul class="navigation">
                    <li><a href="/find-your-home/about-taylor-morrison">About Taylor Morrison</a></li>
                    <li><a href="/find-your-home/encoreii-collection">Encore II Collection</a></li>
                    <li><a href="/find-your-home/discoveryii-collection">Discovery II Collection</a></li>
                    <li><a href="/find-your-home/summit-collection">Summit Collection</a></li>
                    <li><a href="/find-your-home/passage-collection">Passage Collection</a></li>
                </ul>
            </div>
            <div class="column">
                <a href="/location-and-directions"><span style="whitespace:nowrap;">Location &amp; Directions</span></a>

            </div>
            <div class="column">
                <a href="/contact"><span>Contact</span></a>
            </div>
            <div class="column">
                <img src="/~/media/microsites/adoratrails/images/layout/tm-logo.png" alt="image" id="tm-logo">
            </div>
            <div style="clear:both;"></div>
        </div>
        <div id="footer-disclaimer">
            <div id="eho">
                <img src="/~/media/microsites/adoratrails/images/layout/eho.png" alt="image">
            </div>
            <div class="clear"></div>
            <small>Prices may not include lot premiums, upgrades and options. Community Association fees may be required. Prices, promotions, incentives, features, options, amenities, floor plans, elevations, designs, materials and dimensions are subject to change without notice. Square footage and dimensions are estimated and may vary in actual construction. Community improvements and recreational features and amenities described are based upon current development plans which are subject to change and which are under no obligation to be completed. Actual position of house on lot will be determined by the site plan and plot plan. Floor plans and elevations are artist's conception and are not intended to show specific detailing. Floor plans are the property of Taylor Morrison, Inc. and its affiliates and are protected by U.S. copyright laws. For further information, please see a Taylor Morrison Sales Associate and review our <a href="http://www.taylormorrison.com/Terms.aspx" target="_blank" style="font-size:9px;">Terms of Use</a>. This is not an offering in any state where prohibited or otherwise restricted by law. No offer to sell or lease may be made or to purchase or lease may be accepted prior to issuance of an Arizona Public Report.A public report is available at the State Real Estate Department's website at <a href="http://www.azre.gov" target="_self">www.azre.gov</a>. Taylor Morrison/Arizona, Inc., AZ ROC # 179178B . © 2010-2015 TM Homes of Arizona, Inc., AZ DRE # CO535669000. All rights reserved.</small>
        </div>
    </div>
</div>