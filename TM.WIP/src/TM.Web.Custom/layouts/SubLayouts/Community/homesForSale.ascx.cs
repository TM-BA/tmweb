﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Configuration;
using Sitecore.Shell.Framework.Commands.Carousel;
using Sitecore.StringExtensions;
using TM.Web.Custom.Constants;
using SCID = Sitecore.Data.ID;
using System.Text;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using Sitecore.Diagnostics;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class homesForSale : CommunityBase
    {
        public string HomeForSaleJson;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] != null)
            {
                litPrintHeader.Text = "<h1>Homes For Sale</h1>";
            }

            CurrentPage.Title = CurrentPage.GetTagsReplaced(CurrentCommunity["Homes for Sale Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);

            var description = CurrentPage.GetTagsReplaced(CurrentCommunity["Homes for Sale Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
            if (!string.IsNullOrWhiteSpace(description))
                CurrentPage.MetaDescription = description;

            GetValues();
        }

        private SCID currentItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetValues();
            }
        }
        protected void GetValues()
        {
            using (new SecurityDisabler())
            {
                using (new BulkUpdateContext())
                {
                    using (new EventDisabler())
                    {
                        HomeForSaleJson = AdminMode(HomesForSale);
                    }
                }
            }
        }

        private string AdminMode(IEnumerable<Item> Homes)
        {
            Homes = Homes.OrderBy(SortOrderOfSubCommunity)
                          .ThenBy(SubCommunityName)
                          .ThenBy(h => h["Price"].CastAs<int>(0))
                          .GroupBy(SubCommunityName)
                          .SelectMany(g => g);


            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            sb.AppendLine("var initialData = [");
            foreach (Item home in Homes)
            {
                try
                {

                    Item Plan = home.Parent;
                    Item subCommunity = null;
                    if ((!string.IsNullOrEmpty(home["SubCommunity ID"])) || (!string.IsNullOrEmpty(Plan["SubCommunity ID"])))
                    {
                        if (!string.IsNullOrEmpty(home["SubCommunity ID"]))
                        {
                            subCommunity = SCContextDB.GetItem(new SCID(home["SubCommunity ID"]));
                        }
                        else if (!string.IsNullOrEmpty(Plan["SubCommunity ID"]))
                        {
                            subCommunity = SCContextDB.GetItem(new SCID(Plan["SubCommunity ID"]));
                        }
                    }
                    string subComName = "";
                    if (null != subCommunity)
                    {
                        subComName = subCommunity.GetItemName();
                        subComName = subComName.Replace("'", "\\'");
                    }
                    sb2.Append("{");
                    if (!string.IsNullOrWhiteSpace(home["Elevation Images"]))
                    {
                        string[] imgIds = home["Elevation Images"].Split('|');
                        Item image = SCContextDB.GetItem(new SCID(imgIds[0]));
                        if (image != null)
                        {
                            sb2.AppendFormat("img: '{0}',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                        }
                        else
                        {
                            sb2.Append("img: '',");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(Plan["Elevation Images"]))
                        {
                            string[] imgIds = Plan["Elevation Images"].Split('|');
                            Item image = SCContextDB.GetItem(new SCID(imgIds[0]));
                            if (image != null)
                            {
                                sb2.AppendFormat("img: '{0}',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                            }
                            else
                            {
                                sb2.Append("img: '',");
                            }
                        }
                        else
                        {
                            sb2.Append("img: '',");
                        }
                    }
                    sb2.AppendFormat("url: '{0}',", Sitecore.Links.LinkManager.GetItemUrl(home).Replace(".aspx", string.Empty));
                    sb2.AppendFormat("purl: '{0}',", Sitecore.Links.LinkManager.GetItemUrl(Plan).Replace(".aspx", string.Empty));
                    sb2.AppendFormat("pname: '{0}',", Plan.GetItemName());
                    sb2.AppendFormat("name: '{0}',", home.GetItemName().Replace("Lot", "Homesite"));
                    sb2.AppendFormat("id: '{0}',", home.ID);
                    sb2.AppendFormat("addr: '{0}',", home["Street Address"]);
                    sb2.AppendFormat("lot: '{0}',", home["Lot Number"].Replace("Lot", "Homesite"));
                    sb2.AppendFormat("price: {0},", home["Price"].CastAs<int>(0));
                    sb2.AppendFormat("subCom: '{0}',", subComName);
                    sb2.AppendFormat("sqft: {0},", home["Square Footage"].CastAs<int>(0));
                    sb2.AppendFormat("bed: {0},", home["Number of Bedrooms"].CastAs<int>(0));
                    sb2.AppendFormat("bath: {0},", home["Number of Bathrooms"].CastAs<int>(0));
                    sb2.AppendFormat("halfbath: {0},", home["Number of Half Bathrooms"].CastAs<int>(0) > 0 ? home["Number of Half Bathrooms"].CastAs<int>(0) : 0);
                    sb2.AppendFormat("garage: {0},", home["Number of Garages"].CastAs<int>(0));
                    sb2.AppendFormat("story: {0},", home["Number of Stories"].CastAs<int>(0));
                    DateTime AvailDate = SCUtils.SCDateTimeToMsDateTime(home["Availability Date"]);
                    if ((CurrentPage.CurrentWebUser != null)
                        && !string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.HomeforSaleFavorites)
                        && CurrentPage.CurrentWebUser.HomeforSaleFavorites.Contains(home.ID.ToString()))
                    {
                        sb2.AppendFormat("isFav:{0},", "true");

                    }
                    else
                    {
                        sb2.AppendFormat("isFav:{0},", "false");
                    }
                    sb2.AppendFormat("dateS:'{0:d2}{1:d2}{2:d2}',", AvailDate.Year, AvailDate.Month, AvailDate.Day);
                    if (!string.IsNullOrWhiteSpace(home["Status for Availability"]))
                    {
                        try
                        {
                            Item item = SCContextDB.GetItem(new SCID(home["Status for Availability"]));

                            sb2.AppendFormat("date:'{0}'", item.GetItemName());
                        }
                        catch (Exception)
                        {
                            sb2.AppendFormat("date:'{0}'", home["Status for Availability"]);
                        }

                    }
                    else
                    {
                        if (AvailDate > DateTime.Now)
                        {
                            sb2.AppendFormat("date:'Available {0}'", AvailDate.ToString("MMMM, yyyy"));
                        }
                        else
                        {
                            sb2.Append("date:'Available Now'");
                        }
                    }
                    sb2.Append("},");
                }
                catch (Exception ex)
                {
                    sb2.Clear();
                    //important: do not remove the last space, if this is the last home it will be removed later.
                    sb2.Append("/* A Home had an error and was dropped please review the exception log on the server*/ ");
                    Log.Error(ex.Message, this);
                }
                finally
                {
                    sb.Append(sb2);
                    sb2.Clear();
                }
            }
            sb.Length = sb.Length - 1;
            sb.AppendLine("];");
            return sb.ToString();
        }

        private string SubCommunityName(Item homeForSale)
        {
            string subCommunityID = homeForSale["SubCommunity ID"];

            if (!SCID.IsID(subCommunityID))
            {
                var plan = homeForSale.Parent;
                subCommunityID = plan["SubCommunity ID"];

            }

            if (SCID.IsID(subCommunityID))
            {
                var subComm = new ID(subCommunityID).GetItemFromID();
                return subComm["Subcommunity Name"];
            }
            return string.Empty;
        }

        private int? SortOrderOfSubCommunity(Item homeForSale)
        {
            string subCommunityID = homeForSale["SubCommunity ID"];

            if (!SCID.IsID(subCommunityID))
            {
                var plan = homeForSale.Parent;
                subCommunityID = plan["SubCommunity ID"];

            }

            if (SCID.IsID(subCommunityID))
            {

                var subComm = new ID(subCommunityID).GetItemFromID();
                if (subComm != null)
                {

                    return subComm["__Sortorder"].CastAs<int>(0);

                }
            }

            return null;

        }
    }
}
