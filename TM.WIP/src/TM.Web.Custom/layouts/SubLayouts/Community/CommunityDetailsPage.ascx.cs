﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using SCID = Sitecore.Data.ID;
using Sitecore.Web.UI.WebControls;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;
using TM.Utils.Extensions;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityDetailsPage : CommunityBase
    {
        private List<string> _communityVirtualUrls = new List<string> { "floor-plans", "homes-ready-now", "schools", "promotions" };
        protected void Page_Init(object sender, EventArgs e)
        {
            var urlLastSegment = Request.Url.AbsolutePath.TrimEnd("/").LastNSegments(1);
            var communityUrl = CurrentPage.SCCurrentItemUrl.LastNSegments(1);

            if ((Request.QueryString.Count != 0) || (urlLastSegment.Length > 0))
            {
                urlLastSegment = urlLastSegment.ToLowerInvariant().Trim('/');

                if (!string.IsNullOrWhiteSpace(Request["hsh"]) || (urlLastSegment.ToLower() == "hsh"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/HSH.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if (!string.IsNullOrWhiteSpace(Request["Driving"]) || (urlLastSegment.ToLower() == "driving-directions"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/DrivingDirections.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if ((!string.IsNullOrWhiteSpace(Request["Siteplan"])) || (urlLastSegment == "site-plan"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/Siteplan.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if (!string.IsNullOrWhiteSpace(Request["photos"]) || (urlLastSegment == "photos"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/photos.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if ((!string.IsNullOrWhiteSpace(Request["features"])) || (urlLastSegment == "home-features"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/homefeatures.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if ((!string.IsNullOrWhiteSpace(Request["localInterest"])) || (urlLastSegment == "local-interests"))
                {
                    Sublayout ddSublayout = new Sublayout();
                    ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/LocalInterest.ascx";
                    phFullCommunity.Controls.Add(ddSublayout);
                }
                else if ((urlLastSegment != "event-details") && urlLastSegment != communityUrl 
                    && !_communityVirtualUrls.Contains(urlLastSegment) && Request["sc_device"] == null
                    && (Request["sc_mode"] == null || (Request["sc_mode"] != "edit" && Request["sc_mode"] != "preview")))
                {
                    Response.Redirect("/pagenotfound");
                }
                else
                {
                    LoadStatusSpecificCommunityPanel();

                    if (Request["sc_device"] != null)
                    {

                        if (Request["sc_device"].ToLower() == "print")
                        {

                            litHeader.Text = @"<div class=""print-top""><h1>{0}</h1><img src=""{1}""/></div>".FormatWith(
                                CurrentPage.CurrentCommunity.GetItemName(),
                                CurrentPage.CurrentHomeItem.Fields["Company Logo"].GetMediaUrl());

                            if (!string.IsNullOrWhiteSpace(Request["Driving"]) || (urlLastSegment.ToLower() == "driving-directions"))
                            {
                                Sublayout ddSublayout = new Sublayout();
                                ddSublayout.Path = "/tmwebcustom/SubLayouts/Community/DrivingDirections.ascx";
                                phFullCommunity.Controls.Add(ddSublayout);
                            }
                            else if (SCContextItem.TemplateID == SCIDs.TemplateIds.CommunityPage)
                            {
                                Sublayout sitePlanSublayout = new Sublayout();
                                sitePlanSublayout.Path = "/tmwebcustom/SubLayouts/Community/siteplan.ascx";
                                phFullCommunity.Controls.Add(sitePlanSublayout);

                                Sublayout drivingDirectionsSummarySublayout = new Sublayout();
                                drivingDirectionsSummarySublayout.Path = "/tmwebcustom/SubLayouts/Community/DrivingDirectionsSummary.ascx";
                                phFullCommunity.Controls.Add(drivingDirectionsSummarySublayout);

                                Sublayout homeFeaturesSublayout = new Sublayout();
                                homeFeaturesSublayout.Path = "/tmwebcustom/SubLayouts/Community/homeFeatures.ascx";
                                phFullCommunity.Controls.Add(homeFeaturesSublayout);

                                Sublayout localInterestSublayout = new Sublayout();
                                localInterestSublayout.Path = "/tmwebcustom/SubLayouts/Community/LocalInterest.ascx";
                                phFullCommunity.Controls.Add(localInterestSublayout);
                                
                            }
                        }
                    }
                }
            }
            else
            {
                LoadStatusSpecificCommunityPanel();
            }
        }

        private void LoadStatusSpecificCommunityPanel()
        {

            myCommunityStatus = SCUtils.CommunityCurrentStatus(CurrentPage.CurrentCommunity[SCIDs.CommunityFields.CommunityStatusField]);
            Sublayout displaySublayout = new Sublayout();
            switch (myCommunityStatus)
            {
                case GlobalEnums.CommunityStatus.Open:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityOpen.ascx";
                    break;
                case GlobalEnums.CommunityStatus.ComingSoon:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityCommingSoon.ascx";
                    break;
                case GlobalEnums.CommunityStatus.PreSelling:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityPreSelling.ascx";
                    break;
                case GlobalEnums.CommunityStatus.GrandOpening:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityGrandOpening.ascx";
                    break;
                case GlobalEnums.CommunityStatus.Closeout:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityCloseout.ascx";
                    break;
                case GlobalEnums.CommunityStatus.Closed:
                default:
                    displaySublayout.Path = "/tmwebcustom/SubLayouts/Community/CommunityClosed.ascx";
                    break;
            }
            phFullCommunity.Controls.Add(displaySublayout);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentPage.Session[TM.Utils.Web.SessionKeys.LastVisitedCommunity(SCCurrentDomainName)] = SCContextItem.ID;
        }
    }
}
