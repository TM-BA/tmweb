﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts.SubLayouts.Common;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class Photos : CommunityBase
    {
        private int delay = 10;
        public int rotationDelay
        {
            get
            {
                return delay * 1000;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["sc_device"] != null)
            {
                mainImage.Visible = false;
            }
            else
            {
                var pageTitle = string.IsNullOrWhiteSpace(CurrentCommunity["Community Photos Title Template"])
                                       ? CurrentPage.GetTagsReplaced(CurrentCommunity["Community Photos Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject)
                                       : CurrentPage.GetTagsReplaced(CurrentCommunity["Community Photos Title Template"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                if (string.IsNullOrWhiteSpace(pageTitle))
                    pageTitle = "View photos of " + CurrentPage.PageTitle;
                CurrentPage.Title = pageTitle;

                var description = CurrentPage.GetTagsReplaced(CurrentCommunity["Community Photos Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject); 
                if (!string.IsNullOrWhiteSpace(description))
                    CurrentPage.MetaDescription = description;

                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Community Photos", CurrentCommunity));

                if (!string.IsNullOrWhiteSpace(SCContextItem[SCFieldNames.CommunityFields.SlideshowImageRotationSpeed]))
                {
                    int.TryParse(SCContextItem[SCFieldNames.CommunityFields.SlideshowImageRotationSpeed], out delay);
                }
            }
        }
    }
}