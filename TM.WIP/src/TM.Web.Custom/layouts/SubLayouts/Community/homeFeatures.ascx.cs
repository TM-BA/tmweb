﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class homeFeatures : CommunityBase
    {
        public DateTime LastUpdate
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string templateTypeQuery = string.Format("fast:/{0}/*[@@templateid='{1}']/*", CurrentPage.CurrentDivision.Paths.FullPath, "{9E0E1D4B-E0B3-45F9-A91D-7F496825B93F}");
            Dictionary<string, string> typeLookup = new Dictionary<string, string>();
            var featureTypes = CurrentPage.SCContextDB.SelectItems(templateTypeQuery);
            foreach (Item type in featureTypes)
            {
                typeLookup[type.ID.ToString()] = type["Name"];
            }

            StringBuilder sb = new StringBuilder();
            string category = string.Empty;
            bool needsClose = false;
            foreach (Item feature in HomeFeaturesList.OrderBy(i => typeLookup.ContainsKey(i["Home Features Category Heading"]) ? typeLookup[i["Home Features Category Heading"]] : string.Empty))
            {
                string newCat = typeLookup.ContainsKey(feature["Home Features Category Heading"]) ? typeLookup[feature["Home Features Category Heading"]] : string.Empty;
                if (category != newCat)
                {
                    category = newCat;
                    sb.AppendFormat("{0}<ul class=\"home-fea\"><li><h3>{1}</h3></li><li>{2}</li>", (needsClose ? "</ul>" : ""), category, feature["Home Features Text"]);
                    needsClose = true;
                }
                else
                {
                    sb.AppendFormat("<li>{0}</li>", feature["Home Features Text"]);
                }
                LastUpdate = (LastUpdate > feature.Statistics.Updated) ? LastUpdate : feature.Statistics.Updated;
            }
            if (needsClose)
            {
                sb.Append("</ul>");
            }
            litContent.Text = sb.ToString();

            if (Request["sc_device"] != null)
            {
            }
            else
            {
                var pageTitle = string.IsNullOrWhiteSpace(CurrentCommunity["Home Features Title Template"])
                                       ? CurrentPage.GetTagsReplaced(CurrentCommunity["Home Features Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject)
                                       : CurrentPage.GetTagsReplaced(CurrentCommunity["Home Features Title Template"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                if (string.IsNullOrWhiteSpace(pageTitle))
                    pageTitle = "Home Features at " + CurrentPage.PageTitle;
                CurrentPage.Title = pageTitle;

                var description = CurrentPage.GetTagsReplaced(CurrentCommunity["Home Features Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);

                if (!string.IsNullOrWhiteSpace(description))
                    CurrentPage.MetaDescription = description;

                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Home Features", CurrentPage.CurrentCommunity));

                if (!string.IsNullOrWhiteSpace(SCContextItem["Display Home Features Images"]))
                {
                    StringBuilder sbImages = new StringBuilder();
                    var ImageField = (MultilistField)SCContextItem.Fields["Home Features Image"];
                    if (ImageField != null)
                    {
                        Item[] slideShowImages = ImageField.GetItems();

                        for (int i = 0; i < slideShowImages.Count(); i++)
                        {
                            string image = slideShowImages[i].GetMediaUrl();
                            sbImages.AppendFormat("<img src=\"{0}\" />", image);
                        }
                    }

                    litImages.Text = sbImages.ToString();
                }
            }
        }
    }
}