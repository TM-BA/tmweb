﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using SCID = Sitecore.Data.ID;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Extensions;
using System.Text;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public class CommunityBase : ControlBase
    {
        public Item CurrentCommunity { get { return CurrentPage.CurrentCommunity; } }
        public string HomeType
        {
            get
            {
                Item product = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.ProductTypePage, CurrentPage.CurrentCommunity.ID, CurrentPage.CurrentHomeItem);
                if (product != null)
                    return product.GetItemName();
                return "";
            }
        }

        public string HomeTypeDisplayName
        {
            get
            {
                Item product = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.ProductTypePage, CurrentPage.CurrentCommunity.ID, CurrentPage.CurrentHomeItem);
                if (product != null)
                    return product.DisplayName;
                return "";
            }
        }

        private string _comInfoNavUL = null;
        public string ComInfoNavUL
        {
            get
            {
                if (_comInfoNavUL == null)
                {
                    StringBuilder sb = new StringBuilder();

                    //community brochure
                    if (CommunityBrochure != string.Empty)
                    {
                        sb.AppendLine(@"<li><a href=""{0}"" target=""_blank"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Free Brochure');"">Community Brochure</a></li>".FormatWith(CommunityBrochure));
                    }

                    //driving directions
                    if (!Request.Browser.IsMobileDevice)
                    {
                        sb.AppendLine(@"<li><a href=""{0}"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Driving Directions');"">Driving Directions</a></li>".FormatWith(CurrentPage.SCCurrentItemUrl + "/driving-directions"));
                    }

                    //home features
                    if (HomeFeaturesList.Count > 0)
                    {
                        sb.AppendLine(@"<li><a href=""{0}"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Home Features');"">Home Features</a></li>".FormatWith(CurrentPage.SCCurrentItemUrl + "/home-features"));
                    }

                    //local amenities
                    if (LocalInterestList.Count > 0)
                    {
                        sb.AppendLine(@"<li><a href=""{0}"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Local Interests');"">Local Interests</a></li>".FormatWith(CurrentPage.SCCurrentItemUrl + "/local-interests"));
                    }

                    //photos
                    sb.AppendLine(@"<li><a href=""{0}"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Photos');"">Photos</a></li>".FormatWith(CurrentPage.SCCurrentItemUrl + "/photos"));

                    //siteplan
                    if ((!String.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Site Plan Image"])) || (!String.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Site Plan Page Description"])))
                    {
                        sb.AppendLine(@"<li><a href=""{0}"" onclick=""TM.Common.GAlogevent('Details', 'Click', 'CommunityLinks-Site Plan');"">Site Plan</a></li>".FormatWith(CurrentPage.SCCurrentItemUrl + "/site-plan"));
                    }

                    _comInfoNavUL = sb.ToString();
                }
                return _comInfoNavUL;
            }
        }
        public string ComFavText
        {
            get
            {
                if ((CurrentPage.CurrentWebUser != null) && (!string.IsNullOrWhiteSpace(CurrentPage.CurrentWebUser.CommunityFavorites)))
                    return (CurrentPage.CurrentWebUser.CommunityFavorites.Contains(CurrentPage.CurrentCommunity.ID.ToString())) ? "Remove from Favorites" : "Add to Favorites";
                return "Add to Favorites";
            }
        }
        public TM.Utils.Web.GlobalEnums.CommunityStatus myCommunityStatus
        {
            set
            {
                HttpContext.Current.Items["CommunityStatus"] = value;
            }
            get
            {

                return SCUtils.CommunityCurrentStatus(CurrentPage.CurrentCommunity.Fields[SCIDs.CommunityFields.CommunityStatusField].Value);
            }
        }
        public string UserIsLoggedIn
        {
            get
            {
                return (CurrentWebUser != null).ToString().ToLower();
            }
        }
        public string Address1
        {
            get
            {
                return CurrentPage.CurrentCommunity["Street Address 1"];
            }
        }
        public string Address2
        {
            get
            {
                return CurrentPage.CurrentCommunity["Street Address 2"];
            }
        }
        public string AddressLink
        {
            get
            {
                var useCoordinates = CurrentPage.CurrentCommunity["Use Coordinates for Route Generation"] == "1";
                var lat = CurrentPage.CurrentCommunity["Community Latitude"];
                var lon = CurrentPage.CurrentCommunity["Community Longitude"];
                if (useCoordinates && !string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lon))
                    return string.Format("{0},{1}", lat, lon);
                return string.Format("{0},{1},{2}, {3} {4}", Address1, Address2, CityName, State, Zip);
            }
        }
        public string CityName
        {
            get
            {
                return CurrentPage.CurrentCity.GetItemName();
            }
        }
        public string State
        {
            get
            {
                return CurrentPage.CurrentState.GetItemName(true);
            }
        }
        public string Division
        {
            get
            {
                return CurrentPage.CurrentDivision.GetItemName(true);
            }
        }
        public string Zip
        {
            get
            {
                return CurrentPage.CurrentCommunity["Zip or Postal Code"];
            }
        }
        private TM.Domain.Entities.IHC _ihcOveride;
        public TM.Domain.Entities.IHC IhcOveride
        {
            get
            {
                if (_ihcOveride != null)
                {
                    return _ihcOveride;
                }
                string _mainName = SCContextItem["Sales Team Name Override"];
                string _mainPhone = SCContextItem["Sales Team Phone Override"];
                string _mainRegistration = SCContextItem["Sales Team Registration Override"];

                if (!string.IsNullOrWhiteSpace(_mainName)
                    || !string.IsNullOrWhiteSpace(_mainPhone)
                    || !string.IsNullOrWhiteSpace(_mainRegistration))
                {
                    _ihcOveride = new Domain.Entities.IHC(_mainName, _mainPhone, _mainRegistration);
                }
                return _ihcOveride;
            }
        }
        public string MainIHCName
        {
            get
            {
                if (IhcOveride == null)
                {
                    return IHCName;
                }
                return IhcOveride.Name;
            }
        }
        public string IHCName
        {
            get
            {
                if (CurrentPage.CurrentIHC != null)
                    return CurrentPage.CurrentIHC["First Name"] + " " + CurrentPage.CurrentIHC["Last Name"];
                return "";
            }

        }
        public string MainIHCPhone
        {
            get
            {
                if (IhcOveride == null)
                {
                    return IHCPhone;
                }
                return IhcOveride.Phone;
            }
        }
        public string IHCPhone
        {
            get
            {
                if (CurrentPage.CurrentIHC != null)
                    return CurrentPage.CurrentIHC["Phone"];
                return "";
            }
        }

        public string LocationPhone
        {
            get
            {
                if (CurrentPage.CurrentCommunity != null)
                    return CurrentPage.CurrentCommunity["Phone"];
                return "";
            }
        }

        public string MainIHCRegistration
        {
            get
            {
                if (IhcOveride == null)
                {
                    return IHCRegistration;
                }
                return IhcOveride.Registration;
            }
        }
        public string IHCRegistration
        {
            get
            {
                if (CurrentPage.CurrentIHC != null)
                    return CurrentPage.CurrentIHC["Regulating Authority Designation"].Replace("\n", "<br />");
                return "";
            }
        }
        public string CommunityName
        {
            get
            {
                return CurrentPage.CurrentCommunity.GetItemName();
            }
        }
        public string CommunityStartingPrice
        {
            get
            {
                if (string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Price Override Text"]))
                {
                    if (Plans.Count > 0)
                    {
                        return "Starting From  $" + (Plans.Min(p => p["Priced from Value"].CastAs<int>(int.MaxValue))).ToString("#,#");
                    }
                    else
                    {
                        return "More Details Soon";
                    }
                }
                return CurrentPage.CurrentCommunity["Price Override Text"];
            }
        }
        public float CommunityLattitude
        {
            get
            {
                return CurrentPage.CurrentCommunity["Community Latitude"].CastAs<float>(0);
            }
        }
        public float CommunityLongitude
        {
            get
            {
                return CurrentPage.CurrentCommunity["Community Longitude"].CastAs<float>(0);
            }
        }
        private string _communityBrochure;
        public string CommunityBrochure
        {
            get
            {
                if (_communityBrochure == null)
                {
                    if (string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Community Brochure"]))
                    {
                        _communityBrochure = string.Empty;
                    }
                    else
                    {

                        Sitecore.Data.Fields.FileField fileField = ((Sitecore.Data.Fields.FileField)CurrentPage.CurrentCommunity.Fields["Community Brochure"]);

                        string url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fileField.MediaItem);


                        _communityBrochure = url;
                    }
                }
                return _communityBrochure;
            }
        }
        private List<Item> _homeFeaturesList;
        protected List<Item> HomeFeaturesList
        {
            get
            {
                if (_homeFeaturesList == null)
                {
                    _homeFeaturesList = new List<Item>();
                    if (HttpContext.Current.Items.Contains("HomeFeaturesList"))
                    {
                        _homeFeaturesList = HttpContext.Current.Items["HomeFeaturesList"].CastAs<List<Item>>();
                    }
                    else
                    {
                        if (HomeFeaturesNode != null)
                        {
                            string query = TM.Web.Custom.Queries.SCFastQueries.FindItemsMatchingTemplateUnder(HomeFeaturesNode.Paths.FullPath, SCIDs.TemplateIds.HomeFeatureItem);
                            _homeFeaturesList = CurrentPage.SCContextDB.SelectItems(query).ToList<Item>();
                            HttpContext.Current.Items["HomeFeaturesList"] = _homeFeaturesList;
                        }
                    }
                }
                return _homeFeaturesList;
            }
        }

        private Item _homeFeaturesNode;
        protected Item HomeFeaturesNode
        {
            get
            {

                if (_homeFeaturesNode == null)
                {
                    if (HttpContext.Current.Items.Contains("HomeFeatures"))
                    {
                        _homeFeaturesNode = HttpContext.Current.Items["HomeFeatures"].CastAs<Item>();
                    }
                    else
                    {
                        _homeFeaturesNode = CurrentPage.CurrentCommunity.Children.Where<Item>(i => i.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.HomeFeaturesContainer).FirstOrDefault<Item>();
                        HttpContext.Current.Items["HomeFeatures"] = _homeFeaturesNode;
                    }
                }
                return _homeFeaturesNode;
            }
        }
        private List<Item> _plans;
        protected List<Item> Plans
        {
            get
            {
                if (_plans == null)
                {
                    if (HttpContext.Current.Items.Contains("Plans"))
                    {
                        _plans = HttpContext.Current.Items["Plans"].CastAs<List<Item>>();
                    }
                    else
                    {
                        _plans = new List<Item>();
                        string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' and @Plan Status='{2}']".FormatWith(
                        CurrentPage.CurrentCommunity.ID,
                        TM.Web.Custom.Constants.SCIDs.TemplateIds.PlanPage,
                        SCIDs.StatusIds.Status.Active);
                        _plans.AddRange(Sitecore.Context.Database.SelectItems(query));
                        HttpContext.Current.Items["Plans"] = _plans;
                    }
                }
                return _plans;
            }
        }
        private List<Item> _homesForSale;
        protected List<Item> HomesForSale
        {
            get
            {
                if (_homesForSale == null)
                {
                    if (HttpContext.Current.Items.Contains("Inventory"))
                    {
                        _homesForSale = HttpContext.Current.Items["Inventory"].CastAs<List<Item>>();
                    }
                    else
                    {
                        _homesForSale = new List<Item>();
                        string query = "fast://*[@@id='{0}']//*[@@templateid='{1}' and @Home for Sale Status='{2}' and @Is a model home='']".FormatWith(
                        CurrentPage.CurrentCommunity.ID,
                        TM.Web.Custom.Constants.SCIDs.TemplateIds.HomeForSalePage,
                        SCIDs.StatusIds.Status.Active);

                        _homesForSale.AddRange(Sitecore.Context.Database.SelectItems(query));

                        HttpContext.Current.Items["Inventory"] = _homesForSale;
                    }
                }
                return _homesForSale;
            }
        }
        private List<Item> _promos;
        protected List<Item> Promos
        {
            get
            {
                if (_promos == null)
                {
                    if (HttpContext.Current.Items.Contains("Promos"))
                    {
                        _promos = HttpContext.Current.Items["Promos"].CastAs<List<Item>>();
                    }
                    else
                    {
                        _promos = new List<Item>();

                        Dictionary<SCID, SCID> lookups = new Dictionary<SCID, SCID>();
                        lookups.Add(CurrentPage.CurrentCommunity.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);
                        lookups.Add(CurrentPage.CurrentCity.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);
                        lookups.Add(CurrentPage.CurrentDivision.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);
                        lookups.Add(CurrentPage.CurrentState.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);
                        lookups.Add(CurrentPage.CurrentProduct.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);
                        lookups.Add(CurrentPage.CurrentHomeItem.ID, TM.Web.Custom.Constants.SCIDs.TemplateIds.PromoFolder);

                        string SCTimeNow = SCUtils.MSDateTimeToSCDateTime(DateTime.Now);
                        var SCPromos = TM.Web.Custom.SCHelpers.SCUtils.GetItemsOfType(lookups,
                            "@Disabled ='' and @Start Date <= '" + SCTimeNow + "' and @End Date >= '" + SCTimeNow + "'");

                        _promos.AddRange(SCPromos[CurrentPage.CurrentCommunity.ID].OrderBy(i => i["End Date"]));
                        _promos.AddRange(SCPromos[CurrentPage.CurrentCity.ID].OrderBy(i => i["End Date"]));
                        _promos.AddRange(SCPromos[CurrentPage.CurrentDivision.ID].OrderBy(i => i["End Date"]));
                        _promos.AddRange(SCPromos[CurrentPage.CurrentState.ID].OrderBy(i => i["End Date"]));
                        _promos.AddRange(SCPromos[CurrentPage.CurrentProduct.ID].OrderBy(i => i["End Date"]));
                        _promos.AddRange(SCPromos[CurrentPage.CurrentHomeItem.ID].OrderBy(i => i["End Date"]));
                        HttpContext.Current.Items["Promos"] = _promos;
                    }
                }
                return _promos;
            }
        }
        private List<Item> _schools;
        protected List<Item> Schools
        {
            get
            {
                if (_schools == null)
                {
                    if (HttpContext.Current.Items.Contains("Schools"))
                    {
                        _schools = HttpContext.Current.Items["Schools"].CastAs<List<Item>>();
                    }
                    else
                    {
                        var tempSchoolFolder = CurrentPage.CurrentCommunity.GetChildren().Where(i => (i.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.SchoolList));
                        _schools = new List<Item>();
                        foreach (Item school in tempSchoolFolder)
                        {
                            _schools.AddRange(school.GetChildren().Where(i => (i.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.School)));
                        }
                        HttpContext.Current.Items["Schools"] = _schools;
                    }
                }
                return _schools;
            }
        }
        private List<Item> _localInterestList;
        public List<Item> LocalInterestList
        {
            get
            {
                if (_localInterestList == null)
                {
                    if (HttpContext.Current.Items.Contains("LocalInterestList"))
                    {
                        _localInterestList = HttpContext.Current.Items["LocalInterestList"].CastAs<List<Item>>();
                    }
                    else
                    {
                        if (LocalInterests != null)
                        {
                            string query = TM.Web.Custom.Queries.SCFastQueries.FindItemsMatchingTemplateUnder(LocalInterests.Paths.FullPath, SCIDs.TemplateIds.LocalInterestItem);
                            _localInterestList = CurrentPage.SCContextDB.SelectItems(query).ToList();
                        }
                        else
                        {
                            _localInterestList = new List<Item>();
                        }
                        HttpContext.Current.Items["LocalInterestList"] = _localInterestList;
                    }
                }
                return _localInterestList;
            }
        }

        private Item _localInterestsNode;
        protected Item LocalInterests
        {
            get
            {
                if (_localInterestsNode == null)
                {
                    _localInterestsNode = CurrentPage.CurrentCommunity.Children.Where<Item>(i => i.TemplateID == TM.Web.Custom.Constants.SCIDs.TemplateIds.LocalInterestsContainer).FirstOrDefault<Item>();
                }
                return _localInterestsNode;
            }
        }
        protected string financeYourDreamsUrl
        {
            get
            {
                HttpCookie myCookie = new HttpCookie("AcceptABA");
                myCookie = Request.Cookies["AcceptABA"];

                if (myCookie != null)
                {
                    if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Show Finance Link"]) &&
                    !string.IsNullOrWhiteSpace(CurrentPage.CurrentHomeItem["Financing Call to Action"]))
                    {
                        return CurrentPage.CurrentHomeItem["Financing Call to Action"] + "comid={0}&cityname={1}&divid={2}".FormatWith(
                         CurrentPage.CurrentCommunity["Community Legacy ID"],
                         CurrentPage.CurrentCity.GetItemName(),
                         CurrentPage.CurrentDivision["Legacy Division ID"]
                         );
                    }
                }
                else
                {
                    return "/ABA?" + "comid={0}&cityname={1}&divid={2}".FormatWith(
                         CurrentPage.CurrentCommunity["Community Legacy ID"],
                         CurrentPage.CurrentCity.GetItemName(),
                         CurrentPage.CurrentDivision["Legacy Division ID"]);
                }
                return string.Empty;
            }
        }
        protected string ConstructionUpdatesUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Construction Updates"]))
                {
                    Item subItem = CurrentPage.CurrentCommunity;
                    Sitecore.Data.Fields.FileField fileField = ((Sitecore.Data.Fields.FileField)subItem.Fields["Construction Updates"]);
                    string url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(fileField.MediaItem);
                    return url;
                }
                return "";
            }
        }

        public bool IsPrintMode
        {
            get { return (Request["sc_device"] == "print"); }
        }
    }
}
