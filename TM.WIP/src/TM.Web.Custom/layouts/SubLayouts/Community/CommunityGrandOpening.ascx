﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityGrandOpening.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityGrandOpening" %>
    
<div class="lt-col-cd">
<section class="hm-lt_sr">
    <div class="banner-go">
        <div class="banner" style="float: right;">
            <sc:Placeholder ID="scImageRotator" runat="server" Key="Slides" />
        </div>
        <div class="across-lt" id="bannerLeftDiv" runat="server">
            <div class="go-logo">
                <sc:Image ID="Image1" Field="Community Logo" runat="server" />
            </div>
            <div class="go-date" id="goDate" runat="server">
                <h1>
                    Grand Opening</h1>
                <p>
                    <sc:Text ID="Text1" runat="server" Field="Extended Community Status Information" />
                </p>
            </div>
            
            <div id="callsToAction" class="callsToAction" runat="server">
                <ul class="ltnavbut">
                    <li><a href="<%=CurrentPage.SCVirtualItemUrl%>/request-information/">Ask a Question</a></li>
                </ul>
                <ul class="ltnavlnk">
                    <li class="fav"><a href="#" id="fav_<%=SCContextItem.ID.ToString().Substring(1,36) %>"
                        onclick="toggleCommunityFavorties(this,'<%=SCContextItem.ID %>');TM.Common.GAlogevent('Favorites','Click','AddToFavorites');">
                        <%=ComFavText%></a></li>
                    <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                        rel="nofollow">Print</a></li>
                </ul>
            </div>
            <div class="go-all-c ccs" id="goAllCom" runat="server">
                <asp:HyperLink ID="hypAreaCommunities" runat="server">View all <%= Division %> Area Communities</asp:HyperLink>
            </div>
        </div>
    </div>    
</section>
</div>

<div class="det-wrapper-bt">
    <div class="cd_left">
        <div class="geneInfo">
            <div>
                GENERAL INFORMATION</div>
        </div>
        <div class="geneInfoBox">
            <h2>
                Sales Center:
            </h2>
            <p class="address">               
                <a onclick="TM.Common.GAlogevent('Directions', 'Click', 'AddressLink');" href="https://www.google.com/maps?q=<%=AddressLink %>">
                    <span class="clearRightText">
                        <%=Address1 %>
                    </span> 
                    <span class="clearRightText">
                        <%=Address2 %>
                    </span> 
                    <span>
                        <%= CityName %>,
                        <%= State %>
                        <%=Zip %>
                    </span>
                </a>
                <br />
                <span>
                    <a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a>
                </span>
            </p>
            <% if (!Request.Browser.IsMobileDevice) { %>
                <asp:HyperLink CssClass="announ" ID="drivingDirections" runat="server"  onclick="TM.Common.GAlogevent('Directions', 'Click', 'DrivingDirections');">Driving Directions</asp:HyperLink>
            <% } %>
            <div class="grayLineS">
            </div>
            <h2>
                Sales Team:
            </h2>
            <p>
                <%=MainIHCName %><br />
                <span class="ihc-phone">
                    <%=MainIHCPhone%></span><br />
                <%=MainIHCRegistration %>
            </p>
            <div class="grayLineS">
            </div>
            <sc:Placeholder ID="phSecondaryNavWidgets" Key="secNavWidgets" runat="server" />
        </div>
    </div>
    <!--end of cd_left-->
    <sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />
    <!--end cd_right-->
</div>
<asp:Literal ID="litStyle" runat="server"></asp:Literal>