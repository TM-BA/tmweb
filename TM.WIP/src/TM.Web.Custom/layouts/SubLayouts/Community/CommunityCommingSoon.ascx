﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityCommingSoon.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityCommingSoon" %>
    
<div class="lt-col-cd">
<section class="hm-lt_sr">
    <div class="banner-go">
        <div class="banner" style="float: right;">
            <sc:Placeholder ID="scImageRotator" runat="server" Key="Slides" />
        </div>
        <div class="across-lt" id="bannerLeftDiv" runat="server">
            <div class="go-logo">
                <sc:Image ID="Image1" Field="Community Logo" runat="server" />
            </div>
            <div class="go-date" id="goDate" runat="server">
                <h1>
                    coming soon</h1>
                <p>
                    <sc:Text ID="Text1" runat="server" Field="Extended Community Status Information" />
                </p>
            </div>
            
            <div id="callsToAction" class="callsToAction" runat="server">
                <ul class="ltnavbut">
                    <li><a href="<%=CurrentPage.SCCurrentItemUrl%>/request-information/">Join Our Interest List
                    </a></li>
                </ul>
                <ul class="ltnavlnk">
                    <li class="fav"><a href="#" id="fav_<%=SCContextItem.ID.ToString().Substring(1,36) %>"
                        onclick="toggleCommunityFavorties(this,'<%=SCContextItem.ID %>');TM.Common.GAlogevent('Favorites','Click','AddToFavorites');">
                        <%=ComFavText%></a></li>
                    <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                        rel="nofollow">Print</a></li>
                </ul>
            </div>
            <div class="go-all-c ccs" id="goAllCom" runat="server">
                <asp:HyperLink ID="hypAreaCommunities" runat="server">View all <%= Division%> Area Communities</asp:HyperLink>
            </div>
        </div>
    </div>
</section>
</div>

<div class="det-wrapper-bt">
    <div class="cd_left">
        <div class="vipupbx">
            <sc:Placeholder ID="leadForm" Key="secNavWidgets" runat="server" />
        </div>
    </div>
    <sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />
</div>
<asp:Literal ID="litStyle" runat="server"></asp:Literal>