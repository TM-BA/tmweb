﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Photos.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.Photos" %>
<sc:Placeholder ID="header" Key="CompressedCommunityHeader" runat="server" />
<div class="photos-sdshow communityPhotos">
<h1>Photos</h1>
    <div class="rotatorControl">
        <sc:PlaceHolder  runat="server" ID="photoRotator" Key="photos"/>
        <hr style="clear:both;"/>
    </div>
<asp:Panel ID="mainImage" runat="server">
    <div class="pho-main">
    <h3 id="imgTitle"></h3>
        <img id="mainImage" src="/images/ph-img.jpg" onmouseover="pause()" onmouseout="play()">
    </div>
</asp:Panel>
  <script>
      function play(){
          TM.Rotator.$slidesObj.play();
      }

      function pause(){
          TM.Rotator.$slidesObj.pause();
      }

      function displayThisImage(sender, stop) {
          TM.Rotator.$slidesObj.to(parseInt($j(sender).attr("ni")));
        
          if(stop) {
              TM.Rotator.$slidesObj.stop();
          }
      }

    $j(function () {
        TM.Rotator.$slidesObj.opts.onupdate = function (x) {
            $j(this.$items).removeClass('on');
            $j(this.$items[x]).addClass('on');
            var img = document.getElementById("mainImage");
            var sender = $j('img', this.$items[x])[0]
            img.src = sender.src.substr(0, sender.src.indexOf('?')) + "?w=650&as=0";
            document.getElementById("imgTitle").innerHTML = sender.alt;

            if (TM.Rotator.$slidesObj.timeout) {
                TM.Rotator.$slidesObj.pause();
                TM.Rotator.$slidesObj.play();
            }
        };
        setTimeout(function () {
            TM.Rotator.$slidesObj.redraw();
        }, 100);
    });
    var maxImages = $j(".carousel .slide img").length;

    if (maxImages == 0) {
        $j(".rotatorControl").hide();
    }
</script>           
             
</div>
