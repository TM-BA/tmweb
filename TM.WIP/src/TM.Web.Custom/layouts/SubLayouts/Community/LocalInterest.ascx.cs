﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class LocalInterest : CommunityBase
    {
        private string _categoryJson;

        public string CategoryJson
        {
            get
            {
                return _categoryJson;
            }
        }

        private Item[] _categories;

        public Item[] Categories
        {
            get
            {
                if (_categories == null)
                {
                    string query = "/sitecore/Content/Global Data/Shared Field Values/Map Categories//*[@@templateid='{0}']".FormatWith("{19D69FCB-EE9D-4191-861E-309331703A5E}");
                    _categories = CurrentPage.SCContextDB.SelectItems(query);
                }
                return _categories;
            }
        }

        private Dictionary<string, string> _categoryType;

        public Dictionary<string, string> CategoryType
        {
            get
            {
                if (_categoryType == null)
                {
                    _categoryType = new Dictionary<string, string>();
                    string query = SCFastQueries.FindItemsMatchingTemplateUnder("/sitecore/Content/Global Data/Shared Field Values/Map Place Types", new Sitecore.Data.ID("{8BA7D320-A0FF-4862-9C39-F20D93394CE8}"));
                    foreach (Item mapPlaceType in CurrentPage.SCContextDB.SelectItems(query))
                    {
                        if (mapPlaceType["Should Display"].CastAs<bool>(true))
                        {
                            if (_categoryType.ContainsKey(mapPlaceType["TM Category"]))
                            {
                                _categoryType[mapPlaceType["TM Category"]] += ("|" + mapPlaceType["Google Key"]);
                            }
                            else
                            {
                                _categoryType[mapPlaceType["TM Category"]] = mapPlaceType["Google Key"];
                            }
                        }
                    }
                }
                return _categoryType;
            }
        }

        public int scMaxDistance
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(SCContextItem["Points of Interest Radius"]))
                {
                    return SCContextItem["Points of Interest Radius"].CastAs<int>(10);
                }
                return 10;
            }
        }

        public string myCommunity
        {
            get
            {
                return HttpUtility.JavaScriptStringEncode(this.CommunityName, true);
            }
        }

        public DateTime LastUpdate
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (LocalInterests == null && Request["sc_device"] == null)
            {
                Response.Redirect(Request.Path);
            }
            if (!Page.IsPostBack)
            {
                litContent.Text = InitializeList();
                litImages.Text = InitializePhotos();
                ShowList();
            }

            if (Request["sc_device"] == null)
            {
                var pageTitle = string.IsNullOrWhiteSpace(CurrentCommunity["Local Interest Title Template"])
                                       ? CurrentPage.GetTagsReplaced(CurrentCommunity["Local Interest Page Title"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject)
                                       : CurrentPage.GetTagsReplaced(CurrentCommunity["Local Interest Title Template"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                if (string.IsNullOrWhiteSpace(pageTitle))
                    pageTitle = "Things to do around " + CurrentPage.PageTitle;
                CurrentPage.Title = pageTitle;

                var description = CurrentPage.GetTagsReplaced(CurrentCommunity["Local Interest Description"].NamedFormat(CurrentPage.PageTitleObject), CurrentPage.PageTitleObject);
                if (!string.IsNullOrWhiteSpace(description))
                    CurrentPage.MetaDescription = description;

                var breadCrumb = CurrentPage.FindControl("bcBreadcrumb") as TM.Web.Custom.WebControls.Breadcrumb;
                breadCrumb.ItemList.Add(new TM.Web.Custom.WebControls.BreadCrumbItem("Local Interests", CurrentPage.CurrentCommunity));
            }
            else
            {
                LocalInterestHeader.Visible = false;
            }

            lBtnShowMap.Click += new EventHandler(lBtnShowMap_Click);
            lBtnShowList.Click += new EventHandler(lBtnShowList_Click);

            liturl.Text = SCContextItem.Paths.ParentPath;
            var tmUser = CurrentWebUser;
            litcomFav.Text = string.Empty;
            if (tmUser != null)
                litcomFav.Text = tmUser.CommunityFavorites;
        }

        private string InitializePhotos()
        {
            StringBuilder sbImages = new StringBuilder();

            if (Request["sc_device"] != null)
            {
            }
            else
            {
                var ImageField = (MultilistField)SCContextItem.Fields["Local Interest Images"];
                if (ImageField != null)
                {
                    Item[] slideShowImages = ImageField.GetItems();

                    for (int i = 0; i < slideShowImages.Count(); i++)
                    {
                        string image = slideShowImages[i].GetMediaUrl();
                        //sbImages.AppendFormat("<img src=\"{0}?w=198\" />", SCimage);
                        sbImages.AppendFormat("<img src=\"{0}\" />", image);
                    }
                }
            }
            return sbImages.ToString();
        }

        private string InitializeList()
        {
            string query = SCFastQueries.FindItemsMatchingTemplateUnder(CurrentPage.CurrentDivision.Paths.FullPath, new Sitecore.Data.ID("{93E6D17A-86DE-40E5-832A-6DA7E93D23BE}"));
            Dictionary<string, string> typeLookup = new Dictionary<string, string>();
            var featureTypes = CurrentPage.SCContextDB.SelectItems(query);
            typeLookup[""] = "";
            foreach (Item type in featureTypes)
            {
                typeLookup[type.ID.ToString()] = type["Name"];
            }

            StringBuilder sb = new StringBuilder();
            string category = string.Empty;
            bool needsClose = false;
            LastUpdate = DateTime.MinValue;
            foreach (Item interest in LocalInterestList.OrderBy(i => typeLookup.ContainsKey(i["Local Interests Category Heading"]) ? typeLookup[i["Local Interests Category Heading"]] : string.Empty))
            {
                Sitecore.Data.Fields.LinkField lnk = interest.Fields["Local Interests Target URL"];
                string url;
                if (lnk.LinkType == "media")
                {
                    MediaItem media = new MediaItem(lnk.TargetItem);
                    url = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(media));
                }
                else
                {
                    url = lnk.Url;
                }

                if (category != (typeLookup.ContainsKey(interest["Local Interests Category Heading"]) ? typeLookup[interest["Local Interests Category Heading"]] : string.Empty))
                {
                    category = typeLookup.ContainsKey(interest["Local Interests Category Heading"]) ? typeLookup[interest["Local Interests Category Heading"]] : string.Empty;
                    sb.Append(@"{0}<h2>{1}</h2><a href=""{2}"" target=""_blank"">{3}</a><span>{4}</span>".FormatWith(
                        (needsClose ? "</ul>" : ""),
                        category,
                        url,
                        interest.GetItemName(),
                        interest["Local Interests Text"]));
                    needsClose = true;
                }
                else
                {
                    sb.Append(@"<a href=""{0}"" target=""_blank"">{1}</a><span>{2}</span>".FormatWith(
                        url,
                        interest["Name"],
                        interest["Local Interests Text"]));
                }
                LastUpdate = (LastUpdate > interest.Statistics.Updated) ? LastUpdate : interest.Statistics.Updated;
            }
            if (needsClose)
            {
                sb.Append("</ul>");
            }
            return sb.ToString();
        }

        private void lBtnShowList_Click(object sender, EventArgs e)
        {
            ShowList();
        }

        private void ShowList()
        {
            pnlMap.Visible = false;
            pnlList.Visible = true;
            lBtnShowMap.CssClass = lBtnShowMap.CssClass.Replace(" on", "");
            lBtnShowList.CssClass += " on";
        }

        private void lBtnShowMap_Click(object sender, EventArgs e)
        {
            ShowMap();
        }

        private void ShowMap()
        {
            pnlMap.Visible = true;
            pnlList.Visible = false;
            lBtnShowList.CssClass = lBtnShowList.CssClass.Replace(" on", "");
            lBtnShowMap.CssClass += " on";

            StringBuilder sb = new StringBuilder();
            StringBuilder categoryJson = new StringBuilder("var categoryJson = {");

            Item category;
            for (int loopCounter = 0; loopCounter < Categories.Length; loopCounter++)
            {
                category = Categories[loopCounter];
                string image = (!string.IsNullOrWhiteSpace(category["Map Icon"])) ? category["Map Icon"] : string.Empty;
                string imageOn = (!string.IsNullOrWhiteSpace(category["Map Icon on"])) ? category["Map Icon on"] : string.Empty;
                string categoryName = string.IsNullOrWhiteSpace(category["Name"]) ? category.DisplayName : category["Name"];
                int start = image.IndexOf("src=") + 5;
                int end = (start == 4) ? -1 : image.IndexOf('"', start);
                string smallImage = (start != -1 && end != -1) ? image.Substring(start, end - start) : string.Empty;

                start = imageOn.IndexOf("src=") + 5;
                end = (start == 4) ? -1 : imageOn.IndexOf('"', start);
                string smallImageOn = (start != 4 && end != -1) ? imageOn.Substring(start, end - start) : string.Empty;

                sb.Append(@"<li><a class=""mapNav {4}"" onclick=""turnOn(this);requestNewPOIs('{0}')""><span class=""mapIcon"">{2}{3}</span>{1}</a></li>"
                    .FormatWith(
                    loopCounter,
                    categoryName,
                    image,
                    imageOn,
                    (loopCounter == 0) ? "on" : ""
                    ));

                categoryJson.Append(@"{5}'{3}':{{name : '{1}', marker : new google.maps.MarkerImage(""{0}?h=18"", new google.maps.Size(16, 18), new google.maps.Point(0, 0), new google.maps.Point(8, 18)), markerOn : new google.maps.MarkerImage(""{4}?h=18"", new google.maps.Size(16, 18), new google.maps.Point(0, 0), new google.maps.Point(8, 18)), fields:'{2}' }}"
                    .FormatWith(
                        smallImage,
                        categoryName,
                        CategoryType[category.ID.ToString()],
                        loopCounter,
                        smallImageOn,
                        (loopCounter > 0) ? "," : string.Empty
                    ));
            }
            categoryJson.Append("}");
            _categoryJson = categoryJson.ToString();
            ulCategories.InnerHtml = sb.ToString();
        }
    }
}