﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using SCID = Sitecore.Data.ID;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityCommingSoon : CommunityBase
    {
        public string CommingDate
        {
            get
            {
                return SCContextItem["Extended Community Status Information"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hypAreaCommunities.NavigateUrl = SCUtils.GetItemUrl(CurrentPage.CurrentDivision).Replace(".aspx", string.Empty);
            if (!string.IsNullOrWhiteSpace(SCContextItem["Left Section Background Transparency Color"]))
            {
                bannerLeftDiv.Attributes.Add("style", "background-color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Left Section Background Transparency Color"]))["Code"] + ");");
            }

            if (!string.IsNullOrWhiteSpace(SCContextItem["Left Section Background Box Color"]))
            {
                goDate.Attributes.Add("style", "background-color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Left Section Background Box Color"]))["Code"] + ");");
            }
            if (!string.IsNullOrWhiteSpace(SCContextItem["Left Section Link to Division Color"]))
            {
                litStyle.Text = "<style>.across-lt a,.across-lt .ltnavlnk li a {color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Left Section Link to Division Color"]))["Code"] + ");}</style>";
            }

            if (IsPrintMode)
            {
                goAllCom.Visible = false;
                Image1.Visible = false;
            }
        }
    }
}