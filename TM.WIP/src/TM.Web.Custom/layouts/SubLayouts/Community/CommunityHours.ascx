﻿<%@ control language="C#" autoeventwireup="true" codebehind="CommunityHours.ascx.cs" inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityHours" %>
<asp:Panel ID="pnlSalesCenterHours" CssClass="ComHours" runat="server">

    <style type="text/css">
        .noBullets
        {
            list-style:none;
        }
    </style>
    <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
{ %>
    <h2>Sales Center Hours:</h2>
    <% } 
else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
{  %>
    <h2>Sales Centre Hours:</h2>
    <% } 
else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
{  %>
    <h2>Sales Center Hours:</h2>
    <% } %>

    <sc:text id="scUseHoursText" field="Office Hours Text" runat="server" visible="false" />
    <div class="noBullets">
        <asp:Literal ID="litDetailedHours" runat="server"></asp:Literal>
    </div>
</asp:Panel>
