﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Security;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data.Validators;
using Sitecore.Data;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityOpen : CommunityBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hypAreaCommunities.NavigateUrl = SCUtils.GetItemUrl(CurrentPage.CurrentDivision).Replace(".aspx", string.Empty);
            drivingDirections.NavigateUrl = CurrentPage.SCCurrentItemUrl + "/driving-directions";
            CurrentPage.ShowCommunityHours = true;

            if (Request["sc_device"] != null)
            {
                if (Request["sc_device"].ToLower() == "print")
                {
                    callsToAction.Visible = false;
                    areaCommunities.Visible = false;
                    drivingDirections.Visible = false;
                    //communitySubNav.Visible = false;
                }
            }

            if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Price Override Text"]))
            {
                litProductType.Visible = false;
            }
            else
            {
                litProductType.Text = HomeTypeDisplayName;
            }
            if (string.IsNullOrWhiteSpace(SCContextItem["Community Logo"]))
            {
                communityTitle.Visible = true;
            }
            else
            {
                logo.Visible = true;
            }
            PopulateContactInfo();
            if (IsPrintMode)
            {
                Image1.Visible = false;
            }
        }

        public void PopulateContactInfo()
        {
            Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;
            List<IHC> contactList = new List<IHC>();
            string contacts = "";

            // Get all Sales Team assigned.
            if (currentItem.Fields["Sales Team Card"] != null)
            {
                if (!string.IsNullOrEmpty(currentItem.Fields["Sales Team Card"].Value))
                {
                    contacts = currentItem.Fields["Sales Team Card"].Value;

                    foreach (string salesTeamInfo in contacts.Split('|').ToList())
                    {
                        Item item = Sitecore.Context.Database.GetItem(salesTeamInfo);

                        if (item != null)
                        {
                            IHC contact = new IHC(item.Fields["Name"].Value, item.Fields["Phone"].Value, item.Fields["Registration"].Value);
                            contactList.Add(contact);
                        }
                    }
                } // If there is no Sales team cards assigned get IHC contact
                else
                {
                    Item currentNode;
                    try
                    {
                        currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                    }
                    catch (Exception ex)
                    {
                        currentNode = SCContextItem;
                    }
                    while (string.IsNullOrWhiteSpace(contacts) && (currentNode.TemplateID.ToString() != SCIDs.TemplateIds.SwitchCompanyPage) && currentNode.TemplateID != Sitecore.ItemIDs.RootID)
                    {
                        if (string.IsNullOrWhiteSpace(currentNode["IHC Card"]))
                        {
                            currentNode = currentNode.Parent;
                        }
                        else
                        {
                            contacts = currentNode["IHC Card"];
                        }
                    }

                    if (!string.IsNullOrEmpty(contacts))
                    {
                        foreach (string salesTeamInfo in contacts.Split('|').ToList())
                        {
                            Item item = Sitecore.Context.Database.GetItem(salesTeamInfo);

                            if (item != null)
                            {
                                IHC contact = new IHC((item.Fields["First Name"] + " " + item.Fields["Last Name"]), item.Fields["Phone"].Value, item.Fields["Regulating Authority Designation"].Value);
                                contactList.Add(contact);
                            }
                        }
                    }
                }

                rptContactInfo.DataSource = contactList;
                rptContactInfo.DataBind();
            }
        }

        protected void rptContactInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        private static bool IsItemValid(Item item)
        {
            item.Fields.ReadAll();
            Sitecore.Data.Validators.ValidatorCollection validators = ValidatorManager.GetFieldsValidators(
                ValidatorsMode.ValidatorBar, item.Fields.Select(f => new FieldDescriptor(item, f.Name)), item.Database);
            var options = new ValidatorOptions(true);
            ValidatorManager.Validate(validators, options);
            foreach (Sitecore.Data.Validators.BaseValidator validator in validators)
            {
                if (validator.Result != ValidatorResult.Valid)
                {
                    return false;
                }
            }
            return true;
        }
    }
}