﻿<%@ control language="C#" autoeventwireup="True" codebehind="PlanRepeater.ascx.cs" inherits="TM.Web.Custom.Layouts.SubLayouts.Community.PlanRepeater" %>
<script type="text/javascript">
    //Data for home cards
	<%= PlansRepeaterData %>
    var userIsLoggedIn = <%=UserIsLoggedIn %>;


    var myPlanModel;
    $j(document).ready(function () {

        if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace( /^\s+|\s+$/g , '');
            };
        }  
	    
        var PlansModel = function (Plans) {
            var self = this;
            self.PreviousSubCommunity = "";
            self.Plans = ko.observableArray(ko.utils.arrayMap(Plans,
                function(plan) {
		       
                    return {
                        img: plan.img + "?w=158&h=90&bc=ffffff",
                        id: plan.id,
                        url: plan.url,
                        name: plan.name,
                        price: "$" + plan.price.formatMoney(0, '.', ','),
                        pricet: function() {return plan.pricet != ''? plan.pricet: false},
                        sqft: plan.sqft.formatMoney(0, '.', ','),
                        bed: plan.bed,
                        bath: plan.bath,
                        halfbath: plan.halfbath,
                        garage: plan.garage,
                        story: plan.story,
                        subCom: plan.subCom,
                        vturl: plan.vtUrl,
                        mhurl: plan.mhUrl,
                        mhNear: plan.mhNear,
                        HFSUrl: plan.HFSUrl,
                        showFav: plan.showFav,
                        isfav: ko.observable(plan.isFav),
                        shouldShowSubCom: function(current) {
		               
                            if (self.PreviousSubCommunity != PlanInitialData[current].subCom) {
                                self.PreviousSubCommunity = PlanInitialData[current].subCom;
                                return true;
                            }
                            return false;
                        },
                        PtoggleFav: function() {
                            if (userIsLoggedIn) {
                                if (this.isfav()) {
                                    this.isfav(!this.isfav());
                                    AddRemoveFavorites(this.id, 'Plan', 'Remove');
                                } else {
                                    this.isfav(!this.isfav());
                                    AddRemoveFavorites(this.id, 'Plan', 'Add');

                                }
                            } else {
                                //TM.Common.ShowLoginModal(document.location.href);
                                TM.Common.ShowLoginModal("?id=" + this.id + "&type=Plan&action=Add");
                            }
                        }
                    };
                }));
            self.sortBy = function(sel) {
                var key = sel.options[sel.selectedIndex].value;
                self.Plans().sort(function(a, b) { return TM.Common.Plan.Sort(a, b, key); });
                self.PreviousSubCommunity = "";
                self.Plans.valueHasMutated();
            };
        };

        myPlanModel = new PlansModel(PlanInitialData);
        ko.applyBindings(myPlanModel, document.getElementById('planRepeater'));
    });
</script>
<style>
    .sorted .series, .hidden {
        display: none;
    }
</style>
<div>
    <select name="sortList" class="sort" onchange="myPlanModel.sortBy(this)">
        <option value="subCom-price">Sort List</option>
        <option value="subCom-price">Price low to high</option>
        <option value="subCom-priceReverse">Price high to low</option>
        <option value="subCom-name">Plan Name</option>
        <option value="subCom-bed">Number of Bedrooms</option>
        <option value="subCom-sqft">Square Footage</option>
        <option value="subCom-story">Stories</option>
    </select>
</div>
<asp:Literal ID="litPrintHeader" runat="server"></asp:Literal>
<div id="planRepeater" data-bind="foreach: Plans">
    <div data-bind="attr:{'class' : shouldShowSubCom($index())?'series':'hidden' }">
        <h1 data-bind="text:subCom"></h1>
    </div>
    <div class="CD_commCard" style="clear: both;">
        <div class="CDimage">
            <a data-bind="attr:{href: url}">
                <img data-bind="attr:{'src':img}" width="160" height="90"></a>
        </div>
        <div class="CDmiddle">
            <a data-bind="attr:{href: url}" class="CDti"><span data-bind="    text: name"></span></a>
            <p class="CDinfo">
                <span data-bind="text: sqft"></span> Sq. Ft.&nbsp;  |&nbsp;   
                <span data-bind="text: bed"></span> Bed&nbsp;  |&nbsp;  
                <span data-bind="text: bath"></span> Bath&nbsp;   |&nbsp;  
                <span data-bind="visible: halfbath > 0">
                    <span data-bind="text: halfbath"></span> Half Bath&nbsp;   |&nbsp;  
                </span>
                <span data-bind="text: garage"></span> Garage&nbsp; |&nbsp;     
				
                 <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
                {  %>  <span data-bind="text: story == 1 ? story + ' ' +  'story' : story + ' ' + 'stories'"></span><% }
                    else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
                {  %>  <span data-bind="text: story"></span>Storeys <% } 
                    else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
                {  %>  <span data-bind="text: story == 1 ? story + ' ' +  'story' : story + ' ' + 'stories'"></span><% }   %>
            </p>
            <div class="pbt">
                <span data-bind="if:pricet()"><p class="CDprice1" data-bind="    text:pricet()"></p></span>
                <span data-bind="ifnot:pricet()"><p class="CDprice1">Priced From </p><p class="CDprice2" data-bind="    text:price"></p></span>
                <span class="viewPlanBtn callToAction"><a data-bind="attr:{href: url}" class="button-arrow">View Plan<span class="arrow-ri"></span></a></span>
            </div>
        </div>
        <div class="grayLineV"></div>
        <ul class="CDright callToAction">
            <li data-bind="if:showFav"><a href="#" class="CDright1" data-bind="    text : (isfav() ? 'Favorite' : 'Add to Favorites'),click:PtoggleFav"></a></li>
            <li data-bind="if:vturl"><a href="#" target="_blank" class="CDright2" data-bind="    attr:{href:vturl}">Virtual Tour</a></li>
            <li data-bind="if:mhurl"><a href="#" class="CDright3" data-bind="    attr:{href:mhurl}">Model Home</a></li>
            <li data-bind="if:mhNear"><a href="#" class="CDright3" data-bind="    attr:{href:mhNear}">Model Nearby</a></li>
            <li data-bind="if:HFSUrl"><a href="#" class="CDright3" data-bind="    attr:{href:HFSUrl}">Home Available</a></li>
        </ul>
    </div>
</div>

<script>
    $j(document).ready(function () {
        $j(".CDright a.CDright1").click(function () { TM.Common.GAlogevent('Favorites', 'Click', 'AddtoFavorites') });
        $j(".CDright a.CDright2").click(function(){TM.Common.GAlogevent('Details', 'Click', 'Tab-VirtualTour')});
        $j(".CDright a.CDright3").click(function(){TM.Common.GAlogevent('Details', 'Click', 'ModelHome')});
    });
</script>
