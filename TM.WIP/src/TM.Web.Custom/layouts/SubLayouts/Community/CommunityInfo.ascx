﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityInfo.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityInfo" %>
<asp:Panel ID="pnlOneColumn" runat="server">
    <h1>About <sc:Text ID="Text1" runat="server" Field="Community Name" /></h1>
    <span class="justify">
    <sc:Text ID="CommunityDescription" runat="server" Field="Community Description" />
    </span>
</asp:Panel>
<asp:Panel ID="pnlTwoColumn" runat="server" Visible="false">
<div class="dt-lt cicl">
    <h1>About <sc:Text ID="Text4" runat="server" Field="Community Name" /></h1>
	<span class="justify">
	<sc:text ID="stCommDesc" field="Community Description" runat="server"></sc:text>
	</span>
</div>
<div class="dt-rt cicl">
    <h1><sc:Text ID="Text5" runat="server" Field="Column Two Heading" /></h1>
    <span class="justify">
    <asp:Literal ID="CommunityDesc" runat="server"></asp:Literal>
    </span>	
</div>
</asp:Panel>
<ul class="infobtb" id="nextSteps">
<%=ComInfoNavUL %>
</ul>
<div style="clear:both;"></div>
    <p id="financeDream" runat="server" style="padding-bottom:1.5em;" class="callToAction">
        Learn about options to <asp:HyperLink id="financeLink" Target="_blank" runat="server" class="cil">finance your dream home at <sc:Text ID="Text17" runat="server" Field="Community Name" /></asp:HyperLink>
    </p>
    <p id="contructionUpdates" runat="server" class="callToAction">
        Read the latest <asp:HyperLink ID="constructionLink" runat="server"><sc:Text ID="Text2" runat="server" Field="Community Name" /> construction updates</asp:HyperLink> and 
        <a href="<%=CurrentPage.SCCurrentItemUrl%>/request-information/">sign up to receive new updates about <sc:Text ID="Text3" runat="server" Field="Community Name" /></a>
    </p>

<script>
var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {	// check if geolocation is supported
        navigator.geolocation.getCurrentPosition(showPosition);  // get the current position, with showPosition as the callback function
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude; 
}
</script>

<script>
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $j('a[href*="driving-directions"]').attr('href', '//maps.google.com/maps?saddr=Current+Location&daddr=' + $j.trim($j('.address:eq(1)').text()).replace(/\n/g, '').replace(/\ /g, '+').replace(/\++/g, '+').substring(0, $j.trim($j('.address:eq(1)').text()).replace(/\n/g, '').replace(/\ /g, '+').replace(/\++/g, '+').length - 13)).attr('target', '_blank')
    }
</script>