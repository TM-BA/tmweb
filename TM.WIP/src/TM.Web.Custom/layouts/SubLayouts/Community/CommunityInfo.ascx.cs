﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TM.Web.Custom.layouts.SubLayouts;
using Sitecore.Data.Items;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class CommunityInfo : CommunityBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var currentCommunity = CurrentPage.CurrentCommunity;
            
            Text1.Item = currentCommunity;
            CommunityDescription.Item = currentCommunity;
            Text4.Item = currentCommunity;
            Text5.Item = currentCommunity;
            stCommDesc.Item = currentCommunity;

            if (!string.IsNullOrWhiteSpace(currentCommunity["Community Description2"]))
            {
                pnlOneColumn.Visible = false;
                pnlTwoColumn.Visible = true;

                List<string> communityDescList = currentCommunity["Community Description2"].Split('\n').ToList();

                StringBuilder communityDescUL = new StringBuilder();
                communityDescUL.Append("<ul class='seccolm'>");
                foreach (string li in communityDescList)
                {
                    communityDescUL.Append("<li>" + li + "</li>");
                }

                communityDescUL.Append("</ul>");

                CommunityDesc.Text = communityDescUL.ToString();
            }
            if (((myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.Closeout) 
                || (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.Open)
                || (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.ComingSoon)
                || (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.PreSelling)
                || (myCommunityStatus == Utils.Web.GlobalEnums.CommunityStatus.GrandOpening))
                && (!string.IsNullOrWhiteSpace(currentCommunity["Show Finance Link"])))
            {
                financeLink.NavigateUrl = financeYourDreamsUrl;
            }
            else
            {
                financeDream.Visible = false;
            }
            if (string.IsNullOrWhiteSpace(ConstructionUpdatesUrl))
            {
                contructionUpdates.Visible = false;
            }
            else
            {
                constructionLink.NavigateUrl = ConstructionUpdatesUrl;
            }
        }
    }
}