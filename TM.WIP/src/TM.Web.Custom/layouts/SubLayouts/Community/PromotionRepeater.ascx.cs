﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.SecurityModel;
using Sitecore.Data.Items;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Configuration;
using Sitecore.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using SCID = Sitecore.Data.ID;
using System.Text;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using Sitecore.Data.Fields;
namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class PromotionRepeater : CommunityBase
    {
        public string PromoRepeaterData;

        protected void Page_Load(object sender, EventArgs e)
        {
            PromoRepeaterData = BuildJSON(Promos);
            if (Request["sc_device"] != null)
            {
                litPrintHeader.Text = "<h1>Promotions</h1>";
            }
            else
            {
                CurrentPage.Title = "Special offers & promotions at " + CurrentPage.PageTitle;
            }
        }

        private SCID currentItem;
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PromoRepeaterData = BuildJSON(Promos);
            }
        }

        private string BuildJSON(IEnumerable<Item> Promos)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("var PromoInitialData = [");
            foreach (Item promo in Promos)
            {
                lblNoResults.Visible = false;
                sb.Append("{");
                if ((Request["sc_device"] == null) && (!string.IsNullOrWhiteSpace(promo["Teaser Image"])))
                {
                    
                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)promo.Fields["Teaser Image"]);
                    if (null != imgField.MediaItem)
                    {
                        sb.AppendFormat("img: '{0}?w=171&h=116',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem));
                    }
                    else
                    {
                        imgField = ((Sitecore.Data.Fields.ImageField)CurrentPage.CurrentHomeItem.Fields["Company Logo"]);
                        if (null != imgField.MediaItem)
                        {
                            sb.AppendFormat("img: '{0}?w=171&h=116',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem));
                        }
                        else
                        {
                            sb.Append("img: '',");
                        }
                    }
                }
                else
                {
                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)CurrentPage.CurrentHomeItem.Fields["Company Logo"]);
                    if (null != imgField.MediaItem)
                    {
                        sb.AppendFormat("img: '{0}?w=171&h=116',", Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem));
                    }
                    else
                    {
                        sb.Append("img: '',");
                    }
                }

                string destination = promo.Fields["External Link"].GetLinkFieldPath();
                if (string.IsNullOrWhiteSpace(destination))
                {
                    destination = "javascript:void(0);";
                    sb.AppendFormat("hasURL: false,");
                }
                else
                {
                    sb.AppendFormat("hasURL: true,");
                }
                sb.AppendFormat("url: '{0}',", destination);
                if (string.IsNullOrWhiteSpace(promo["Open in new browser"]))
                {
                    //sb.AppendFormat("url: '{0}',", Sitecore.Links.LinkManager.GetItemUrl(promo).Replace(".aspx", string.Empty));
                    sb.AppendFormat("target: '',");
                }
                else
                {
                    sb.AppendFormat("target: '_blank',");
                    
                }
                sb.AppendFormat("name: '{0}',",promo["Title"]);
                sb.AppendFormat("content: {0}", promo["Teaser Content"].EncodeJsString());
                sb.Append("},");
            }
            sb.Length = sb.Length-1;
            sb.AppendLine("];");
            return sb.ToString();
        }
    }
}
