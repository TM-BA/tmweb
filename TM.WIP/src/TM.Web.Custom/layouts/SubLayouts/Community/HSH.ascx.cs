﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Security;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using System.Text;
using Sitecore.PrintStudio.PublishingEngine;
using System.IO;
using Sitecore.Data;
using Sitecore.PrintStudio.PublishingEngine.Text;
using System.Xml;
using TM.Utils;

namespace TM.Web.Custom.Layouts.SubLayouts.Community
{
    public partial class HSH : CommunityBase
    {
        public string templateSection;
        public string salesMembers;
        public bool IsCommunityLevel
        {
            get
            {
                return CurrentPage.SCContextItem.TemplateID ==
                SCIDs.TemplateIds.CommunityPage;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hypAreaCommunities.NavigateUrl = SCUtils.GetItemUrl(CurrentPage.CurrentDivision).Replace(".aspx", string.Empty);
            drivingDirections.NavigateUrl = CurrentPage.SCCurrentItemUrl + "/driving-directions";
            CurrentPage.ShowCommunityHours = true;

            if (Request["sc_device"] != null)
            {
                if (Request["sc_device"].ToLower() == "print")
                {
                    callsToAction.Visible = false;
                    areaCommunities.Visible = false;
                    drivingDirections.Visible = false;
                    //communitySubNav.Visible = false;
                }
            }

            if (!string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Price Override Text"]))
            {
                litProductType.Visible = false;
            }
            else
            {
                litProductType.Text = HomeTypeDisplayName;
            }
            if (string.IsNullOrWhiteSpace(CurrentPage.CurrentCommunity["Community Logo"]))
            {
                logo.Visible = false;
            }
            else
            {
                logo.Visible = true;
            }
            if (IsCommunityLevel)
            {
                PopulateContactInfo();
                leftSide.Visible = true;

            }

            CreateChoosenSection();
            CreateSalesTeamSection();
            if (IsPrintMode)
            {
                Image1.Visible = false;
            }
        }

        public void CreateChoosenSection()
        {
            var page = Sitecore.Context.Database.
                GetItem(SCIDs.PrintStudio.HSH).Axes.
                GetDescendants().
                Where(c =>
                    c.TemplateID == SCIDs.PrintStudio.PageTemplate &&
                    c.Fields["IsForHome"] != null);


            if (IsCommunityLevel)
            {
                page = page.Where(c =>
                    String.IsNullOrEmpty(c.Fields["IsForHome"].Value));
            }
            else
            {
                page = page.Where(c =>
                    !String.IsNullOrEmpty(c.Fields["IsForHome"].Value));
            }

            if (SCCurrentDeviceType == DeviceType.TaylorMorrison) {
                page = page.Where(c =>
                    !String.IsNullOrEmpty(c.Fields["TM"].Value));
            }
            else
            {
                page = page.Where(c =>
                    !String.IsNullOrEmpty(c.Fields["DH"].Value));
            }
            var sb = new StringBuilder();

            if (page.Count() == 0)
            {
                sb.Append(@"<h1><span>No Hot Sheet available to print</span></h1>");
            }

            sb.AppendLine(@"<ul class=""template-selection"">");
            
            foreach (var element in page)
            {
                if (element.Fields.Count() > 0 && (element.Parent.Parent.Fields["__Thumbnail"].Value != null || element.Parent.Parent.Parent.Fields["__Thumbnail"].Value != null))
                {
                    var guid = Guid.NewGuid();
                    sb.AppendFormat(@"<li class=""template-item choose-template"" data-allowed=""{0}"" data-ihc=""{1}"">", String.IsNullOrEmpty(element.Fields["Allowed"].Value) ? "0" : element.Fields["Allowed"].Value, element.Fields["IHC"].Value);
                    sb.AppendFormat(@"<input id=""{1}"" type=""radio"" runat=""server""name=""print-template"" value=""{0}"">", element.Parent.Parent.ID.ToString(), guid.ToString());
                    sb.Append(@"<div class=""template-item"">");
                    sb.Append(@"<i></i>");
                    sb.AppendFormat(@"<h3>{0}</h3>", GetDocumentName(element));
                    //sb.AppendFormat(@"<h2>{0}</h2>", element.Fields["Name"]);
                    sb.AppendFormat(@"<img src=""{0}"" />", !String.IsNullOrEmpty(element.Parent.Parent.Fields["__Thumbnail"].GetMediaUrl()) ? element.Parent.Parent.Fields["__Thumbnail"].GetMediaUrl() : element.Parent.Parent.Parent.Fields["__Thumbnail"].GetMediaUrl());
                    sb.Append(@"</div>");
                    sb.Append(@"</li>");

                }
            }

            sb.AppendLine(@"</ul>");
            templateSection = sb.ToString();

        }

        private string GetDocumentName(Item baseItem)
        {
            while (baseItem.Parent != null)
            {
                if (baseItem.TemplateID == SCIDs.PrintStudio.Document)
                {
                    return baseItem.Name.
                        Replace("DH","").
                        Replace("TM","").
                        Trim();
                }
                baseItem = baseItem.Parent;
            }
            return null;
        }

        public void CreateSalesTeamSection()
        {

            var sb = new StringBuilder();
            var currentItem = CurrentPage.CurrentCommunity;
            var members = String.Empty;
            var salesDataSource = new List<IHC>();

            if (currentItem.Fields.Count() > 0 && 
                !String.IsNullOrEmpty(currentItem.Fields["Sales Team Card"].Value))
            {
                members = currentItem.Fields["Sales Team Card"].Value;
            }

            foreach (string salesTeamInfo in members.Split('|').ToList())
            {
                var item = Sitecore.Context.Database.GetItem(salesTeamInfo);

                if (item != null)
                {
                    salesDataSource.Add(new IHC(item.Fields["Name"].Value,
                        GetPhone(currentItem, item), 
                        item.Fields["Registration"].Value));
                }
            }

            extra_phone.Text = GetPhone(currentItem);
            salesteam_repeater.DataSource = salesDataSource;
            salesteam_repeater.DataBind();
        }

        private string GetPhone(Item community, Item item = null)
        {
            if (!String.IsNullOrEmpty(community["Phone"])) {
                return community["Phone"];
            }
            
            if (item != null && !String.IsNullOrEmpty(item["Phone"])) {
                return item["Phone"];
            }

            return null;
        }
        public void PopulateContactInfo()
        {
            var currentItem = CurrentPage.CurrentCommunity;
            var contactList = new List<IHC>();
            var contacts = "";

            // Get all Sales Team assigned.
            if (currentItem.Fields["Sales Team Card"] != null)
            {
                if (!string.IsNullOrEmpty(currentItem.Fields["Sales Team Card"].Value))
                {
                    contacts = currentItem.Fields["Sales Team Card"].Value;

                    foreach (string salesTeamInfo in contacts.Split('|').ToList())
                    {
                        var item = Sitecore.Context.Database.GetItem(salesTeamInfo);

                        if (item != null)
                        {
                            var contact = new IHC(item.Fields["Name"].Value, 
                                item.Fields["Phone"].Value, 
                                item.Fields["Registration"].Value);
                            contactList.Add(contact);
                        }
                    }
                } // If there is no Sales team cards assigned get IHC contact
                else
                {
                    Item currentNode;
                    try
                    {
                        currentNode = SCContextItem["Root"].IsNotEmpty() ? 
                            SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : 
                            SCContextItem;

                        while (string.IsNullOrWhiteSpace(contacts)
                        && (currentNode.TemplateID.ToString() != SCIDs.TemplateIds.SwitchCompanyPage)
                        && currentNode.TemplateID != Sitecore.ItemIDs.RootID)
                        {
                            if (string.IsNullOrWhiteSpace(currentNode["IHC Card"]))
                            {
                                currentNode = currentNode.Parent;
                            }
                            else
                            {
                                contacts = currentNode["IHC Card"];
                            }
                        }

                        if (!string.IsNullOrEmpty(contacts))
                        {
                            foreach (string salesTeamInfo in contacts.Split('|').ToList())
                            {
                                Item item = Sitecore.Context.Database.GetItem(salesTeamInfo);

                                if (item != null)
                                {
                                    IHC contact = new IHC((item.Fields["First Name"] + " " + item.Fields["Last Name"]), item.Fields["Phone"].Value, item.Fields["Regulating Authority Designation"].Value);
                                    contactList.Add(contact);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        currentNode = SCContextItem;
                    }
                }

                rptContactInfo.DataSource = contactList;
                rptContactInfo.DataBind();
            }
        }

        protected void rptContactInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        public void InventoryListAction(string id)
        {

        }

        protected void CreateHotSheet_Click(object sender, EventArgs e)
        {
            var id = new ID(Request.Form["print-template"]);
            var element = Sitecore.Context.Database.GetItem(id);
            var filename = string.Format("{0}_{1}", GetDocumentName(element), DateTime.Now.Ticks);

            var options = new PrintOptions()
            {
                PrintExportType = PrintExportType.Pdf,
                ResultFileName = filename,
                UseHighRes = false
            };

            var manager = new PrintManager(Sitecore.Context.Database, Sitecore.Context.Language);
            var counter = 0;
            if (IsCommunityLevel)
            {
                options.Parameters.Add("community", Sitecore.Context.Item.ID.ToString());
            }
            else
            {

                options.Parameters.Add("community", CurrentPage.CurrentCommunity.ID.ToString());
                options.Parameters.Add("plan", Sitecore.Context.Item.ID.ToString());
            }
            

            for (int i = 0; i < salesteam_repeater.Items.Count; i++)
            {
                var control = salesteam_repeater.Items[i];
                var chk = (CheckBox)
                    control.FindControl("salesCheckbox");
                
                if (chk.Checked)
                {
                    counter++;
                    options.Parameters.Add("sales_member_" + counter + "_name",
                        ((TextBox)control.FindControl("name")).Text);
                    options.Parameters.Add("sales_member_" + counter + "_phone",
                        ((TextBox)control.FindControl("phone")).Text);
                    options.Parameters.Add("sales_member_" + counter + "_email",
                        ((TextBox)control.FindControl("email")).Text);
                    /*options.Parameters.Add("sales_member_" + counter + "_title",
                        ((TextBox)control.FindControl("title")).Text);*/
                    options.Parameters.Add("sales_member_" + counter + "_registration",
                        ((TextBox)control.FindControl("registration")).Text);
                }
            }

            if (ExtraMember.Checked)
            {
                counter++;
                options.Parameters.Add("sales_member_" + counter + "_name",
                    extra_name.Text);
                options.Parameters.Add("sales_member_" + counter + "_phone",
                    extra_phone.Text);
                options.Parameters.Add("sales_member_" + counter + "_email",
                    extra_email.Text);
                /*options.Parameters.Add("sales_member_" + counter + "_title",
                    extra_title.Text);*/
            }

            options.Parameters.Add("custom-text", custom_text.Value);


            var result = manager.
                Print(element.ID.ToString(), options);

            if (!string.IsNullOrEmpty(result) && File.Exists(result))
            {
                var file = new FileInfo(result);
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", string.Format("attachment;filename={0}", file.Name));
                Response.AppendHeader("Content-Length", file.Length.ToString());
                Response.TransmitFile(file.FullName);
                Response.Flush();
                Response.End();
            }
            else
            {
                Sitecore.Diagnostics.Log.Info("HSH Result file: " + result, this); 
            }
        }
    }
}