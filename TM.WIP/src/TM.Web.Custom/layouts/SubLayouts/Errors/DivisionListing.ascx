﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DivisionListing.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Errors.DivisionListing" %>
<asp:Repeater ID="rptStates" runat="server" 
    onitemdatabound="rptStates_ItemDataBound">
    <ItemTemplate>
       
        <asp:Panel ID="pnlState" runat="server">
         <ul class="error-area">  
         <li><asp:Label ID="lblState" runat="server"></asp:Label></li>

       
            <asp:Repeater ID="rptDivisions" runat="server" onitemdatabound="rptDivisions_ItemDataBound">
                <ItemTemplate>
                        <li><asp:HyperLink ID="hlkTitle" runat="server"></asp:HyperLink></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        </asp:Panel>
       
         
    </ItemTemplate>
</asp:Repeater>

