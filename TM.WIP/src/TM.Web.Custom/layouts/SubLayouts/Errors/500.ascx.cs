﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.Errors
{
    public partial class _500 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitemap1.HideHeader = true;
        }
    }
}