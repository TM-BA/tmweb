﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace TM.Web.Custom.Layouts.SubLayouts.HomePage
{
    public partial class Accordion : ControlBase
    {
        public string BUTTON_BACKGROUND = string.Empty;
        public string BUTTON_FONT = string.Empty;
        public string CONTENT_BACKGROUND = string.Empty;
        public string CONTENT_FONT = string.Empty;
        public string BUTTON_WHEN_ON_BACKGROUND = string.Empty;
        public string BUTTON_WHEN_ON_FONT = string.Empty;
        public string BUTTON_HOVER_BACKGROUND = string.Empty;
        public string BUTTON_HOVER_FONT = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            var list = new List<AccordionItem>();

            if (DataSource != null && DataSource.Children.Count > 0)
            {
                var children = DataSource.Children;
                foreach (var child in children)
                {

                    var sitecoreItem = (Item)child;
                    if (!string.IsNullOrEmpty(sitecoreItem["Content"]) &&
                        !string.IsNullOrEmpty(sitecoreItem["Title"]))
                    {
                        var item = new AccordionItem
                                       {
                                           Content = sitecoreItem["Content"],
                                           Title = sitecoreItem["Title"]

                                       };
                        list.Add(item);
                    }
                }
                BUTTON_BACKGROUND = "#" + DataSource["Button Background Color"];
                BUTTON_FONT = "#" + DataSource["Button Font Color"];
                CONTENT_BACKGROUND = "#" + DataSource["Content Background Color"];
                CONTENT_FONT = "#" + DataSource["Content Font Color"];
                BUTTON_WHEN_ON_BACKGROUND = "#" + DataSource["Button When On Background Color"];
                BUTTON_WHEN_ON_FONT = "#" + DataSource["Button When On Font Color"];
                BUTTON_HOVER_BACKGROUND = "#" + DataSource["Button On Hover Background Color"];
                BUTTON_HOVER_FONT = "#" + DataSource["Button On Hover Font Color"];
            }
            rptAccordion.DataSource = list;
            rptAccordion.DataBind();
            

        }
    }
    public class AccordionItem
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}