﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TM.Web.Custom.Layouts.SubLayouts.HomePage
{
    public partial class DarlingHomesSplashHeader : ControlBase
    {
        private Item homeNode
        {
            get
            {
                return Sitecore
                .Context
                .Database
                .GetItem(SCCurrentHomePath);
            }
        }

        public string Header { get; set; }

        public Item Splash { get; set; }

        public string CookieValue { get; set; }

        public string CookieName
        {
            get
            {
                var name = homeNode.Fields["Splash Cookie Name"];
                return name != null ? name.Value : "";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (homeNode != null) {
                var node = homeNode.Fields["Initial Splash URL"];
                if (node != null) {
                    Splash = Sitecore.
                        Context.Database.GetItem(node.Value);

                    if (Splash != null)
                    {
                        Header = Splash.Fields["Header"].ToString();
                    }
                }
            }

            if (Request.Cookies[CookieName] != null)
            {
                CookieValue = Request.Cookies[CookieName].Value ?? null;
            }

        }
    }
}