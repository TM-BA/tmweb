﻿using System;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.HomePage
{
    public partial class MonarchHomePageLeftColumn : ControlBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var productsQuery = SCFastQueries.FindItemsMatchingTemplateUnder(SCCurrentHomePath,
                                                                                     SCIDs.TemplateIds.ProductTypePage);
            var products = SCContextDB.SelectItems(productsQuery);
            var condos = new Dictionary<string, string>(); 
            var singleFamilyHomes = new Dictionary<string, string>(); 
            foreach (var product in products)
            {
                if(product.Name.ToLowerInvariant().Contains("condo"))
                {
                    var states = product.Children;
                    foreach (Item state in states)
                    {
                        if (state.TemplateID == SCIDs.TemplateIds.StatePage)
                        {
                            var divisions = state.Children;
                            foreach (Item division in divisions)
                            {
                                if(division.TemplateID == SCIDs.TemplateIds.DivisionPage)
                                {
                                    condos.Add(division.GetItemUrl(), division.GetItemName());
                                }
                            }
                       
                        }

                    }

                }
                else
                {
                    var states = product.Children;

                    foreach (Item state in states)
                    {
                        if (state.TemplateID == SCIDs.TemplateIds.StatePage)
                        {
                            var divisions = state.Children;
                            foreach (Item division in divisions)
                            {
                                if (division.TemplateID == SCIDs.TemplateIds.DivisionPage)
                                    singleFamilyHomes.Add(division.GetItemUrl(), division.GetItemName());

                            }
                        }
                    }

                }
            }

            rptCondoHomes.DataSource = condos;
            rptSingleFamilyHomes.DataSource = singleFamilyHomes;
	    this.DataBind();
        }
    }
}