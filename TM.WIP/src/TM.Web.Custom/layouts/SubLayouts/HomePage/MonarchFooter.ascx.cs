﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.HomePage
{
    public partial class MonarchFooter : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkYoutube.HRef = CurrentPage.YouTubeURL;
            lnkYoutube.Visible = CurrentPage.YouTubeURL.IsNotEmpty();

            lnkFacebook.Visible = CurrentPage.FacebookUrl.IsNotEmpty();
            lnkFacebook.HRef = CurrentPage.FacebookUrl;

            lnkTwitter.Visible = CurrentPage.TwitterURL.IsNotEmpty();
            lnkTwitter.HRef = CurrentPage.TwitterURL;

            lnkPinterest.Visible = CurrentPage.PinterestURL.IsNotEmpty();
            lnkPinterest.HRef = CurrentPage.PinterestURL;

            lnkGooglePlus.Visible = CurrentPage.GooglePlusURL.IsNotEmpty();
            lnkGooglePlus.HRef = CurrentPage.GooglePlusURL;

            lnkBlog.Visible = CurrentPage.BlogURL.IsNotEmpty();
            lnkBlog.HRef = CurrentPage.BlogURL;

            Blog.HRef = lnkBlog.HRef;

            litDisclaimer.Text = "<iframe src='/NotIndexed/disclaimer.aspx?id={0}' style='width:100%;height:500px;border:0px;' frameBorder='0'></iframe>".FormatWith(SCContextItem.ID);

            var homeItem = SCCurrentHomeItem;
            var invFld = homeItem.Fields["Investor Relations"];
            if (invFld != null)
                hlkInvestors.NavigateUrl = invFld.GetLinkFieldPath();

            Sitecore.Data.Items.Item context = SCContextDB.GetItem(SCIDs.Home.TM_Home);
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = context.Fields["Company Logo"];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }

            tm_logo.Text = "<img src='" + imageURL + "' Class='logoFooter1'>";
        }
    }
}