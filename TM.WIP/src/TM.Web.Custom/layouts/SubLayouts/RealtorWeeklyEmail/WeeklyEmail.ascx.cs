﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Domain.Entities;
using TM.Domain.Enums;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Layouts.SubLayouts.RealtorWeeklyEmail
{
    public partial class WeeklyEmail : ControlBase
    {
        public string BASIC_COLOR = "#D31245";
        public string FEATURED_COLOR = "#1A3989";
        public string IMAGE_SRC = string.Empty;
        public bool DISPLAY_FEATURED = true;
        public bool DISPLAY_AVAILABLE = true;
        public const int MAX_COMMUNITY_NUMBER = 4;
        public string HOST = string.Empty;
        public string URL = string.Empty;
        public string IHC_IMAGE = string.Empty;
        public string IHC_NAME = string.Empty;
        public string IHC_PHONE = string.Empty;
        public string IHC_DRE = string.Empty;
        public string IHC_EMAIL = string.Empty;
        public string COMPANY_NAME = "Taylor Morrison";
        public string URL_NAME = "www.taylormorrison.com";
        public string FACEBOOK = string.Empty;
        public string TWITTER = string.Empty;
        public string REALTOR_URL = string.Empty;
        public string NODE_TEMPLATENAME = "Node";
        public string SITENAME = string.Empty;
        const string PERCENT_SIGN = "%";
        public const string SUBJECT = "Your Weekly Inventory Homes For Sale Listings";
        //*bc=ffffff was added so that it removes the black crop that Sitecore inserts on images that don't meet the 16:9 ratio*
        public const string THUMBNAIL_WIDTH_HEIGHT = "?w=160&h=90&bc=ffffff";

        protected void Page_Load(object sender, EventArgs e)
        {
            const string FACEBOOK_URL_FIELD = "Facebook URL";
            const string TWITTER_URL_FIELD = "Twitter URL";

            MediaUrlOptions urlOptions = new MediaUrlOptions();
            urlOptions.UseItemPath = false;

            IEnumerable<Item> itemAncestors = Sitecore.Context.Item.GetAncestors();
            Item homeNode = itemAncestors.SingleOrDefault(x => x.TemplateName == NODE_TEMPLATENAME);
            SITENAME = homeNode.Name.ToLower();

            switch (SITENAME)
            {
                case CommonValues.DarlingHomesDomain:
                    BASIC_COLOR = "#513528";
                    FEATURED_COLOR = "#006A71";
                    IMAGE_SRC = "-dh";
                    COMPANY_NAME = "Darling Homes";
                    URL_NAME = "www.darlinghomes.com";
                    break;
            }

            HOST = "http" + "://" + URL_NAME;
            URL = HOST + LinkManager.GetItemUrl(Sitecore.Context.Item).ShortenURL(SITENAME);
            FooterText.Text = Sitecore.Context.Item["FooterText"];
            REALTOR_URL = HOST + Sitecore.Context.Item.Parent.Parent.Parent.GetItemUrl().ShortenURL(SITENAME);

            if (Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields[FACEBOOK_URL_FIELD].Value.IsNotEmpty())
                FACEBOOK = ((Sitecore.Data.Fields.LinkField)Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields[FACEBOOK_URL_FIELD]).Url;
            if (Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields[TWITTER_URL_FIELD].Value.IsNotEmpty())
                TWITTER = ((Sitecore.Data.Fields.LinkField)Sitecore.Context.Item.Parent.Parent.Parent.Parent.Fields[TWITTER_URL_FIELD]).Url;
            var featuredHomesSelected = Sitecore.Context.Item["FeaturedHomes"].Split('|');
            var featuredHomes = new List<Home>();
            var availableHomes = new List<Home>();
            if (featuredHomesSelected.Length > 0 && featuredHomesSelected[0] != string.Empty)
            {
                for (var i = 0; i != featuredHomesSelected.Length && i < MAX_COMMUNITY_NUMBER; i++)
                {
                    featuredHomes.Add(CreateHomeObject(new ID(featuredHomesSelected[i])));
                }
            }
            else
            {
                DISPLAY_FEATURED = false;
            }

            var plans = new PlanQueries().GetHomesForSaleForRWEInDivision(CurrentPage.CurrentDivision.ID);
            foreach (var plan in plans)
            {
                availableHomes.Add(CreateHomeObject(plan));
            }
            if (!availableHomes.Any())
            {
                DISPLAY_AVAILABLE = false;
            }

            var searchHelper = new SearchHelper();

            var ihcItem = searchHelper.GetIhcCardValues(Sitecore.Context.Item);
            IHC_IMAGE = HOST + "/images/tm/ihc_person.jpg";
            if (ihcItem != null)
            {
                if (!string.IsNullOrEmpty(ihcItem.Fields["Consultant Image"].Value))
                {

                    Sitecore.Data.Fields.ImageField imageField = ihcItem.Fields["Consultant Image"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        var image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                        IHC_IMAGE = HOST + Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image, urlOptions)) + "?h=55";
                    }
                }

                IHC_NAME = ihcItem["First Name"] + " " + ihcItem["Last Name"];
                IHC_DRE = "DRE #" + ihcItem["Regulating Authority Designation"].Replace("\n", "<br />");
                IHC_PHONE = ihcItem["Phone"];
                IHC_EMAIL = ihcItem["Email Address"];
            }




            rptAvailableHomes.ItemDataBound += ItemDataBound;
            rptFeaturedResults.ItemDataBound += ItemDataBound;
            rptFeaturedResults.DataSource = featuredHomes.OrderBy(h => h.CommunityName).ThenBy(h => h.Price);
            rptFeaturedResults.DataBind();
            rptAvailableHomes.DataSource = availableHomes.OrderBy(h => h.CommunityName).ThenBy(h => h.Price);
            rptAvailableHomes.DataBind();
        }

        private void ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            const string PH_REALTORAMOUNT_CONTROL = "phRealtorAmount";
            const string PH_REALTORRATE_CONTROL = "phRealtorRate";

            var phRealtorAmount = e.Item.FindControl(PH_REALTORAMOUNT_CONTROL);
            var phRealtorRate = e.Item.FindControl(PH_REALTORRATE_CONTROL);
            var home = (Home)e.Item.DataItem;

            phRealtorAmount.Visible = !string.IsNullOrWhiteSpace(home.RealtorAmount);
            phRealtorRate.Visible = !string.IsNullOrWhiteSpace(home.RealtorRate);
        }

        private Home CreateHomeObject(ID homeID)
        {
            const string ONE = "1";
            const string SPACE = " ";
            const string STORY = "Story";
            const string STORIES = "Stories";
            const string ELEVATION_IMAGES_FIELD = "Elevation Images";
            const string REGEX_PLACEHOLDERNAME = @"^.*[a-zA-Z]+.*$";
            const string DOLLAR_FORMAT = "n0";
            const char PIPE_DELIMETER = '|';


            MediaUrlOptions urlOptions = new MediaUrlOptions();
            urlOptions.UseItemPath = false;

            var home = SCUtils.GetItem(homeID);
            var imageUrl = HOST + Constants.PageUrls.NoImagePath;
            if (!string.IsNullOrWhiteSpace(home[ELEVATION_IMAGES_FIELD]))
            {
                string[] imgIds = home[ELEVATION_IMAGES_FIELD].Split(PIPE_DELIMETER);
                Item image = SCContextDB.GetItem(new ID(imgIds[0]));
                if (image != null)
                    imageUrl = HOST + Sitecore.Resources.Media.MediaManager.GetMediaUrl(image, urlOptions);
            }
            else if (!string.IsNullOrWhiteSpace(home.Parent[ELEVATION_IMAGES_FIELD]))
            {

                string[] imgIds = home.Parent[ELEVATION_IMAGES_FIELD].Split(PIPE_DELIMETER);
                Item image = SCContextDB.GetItem(new ID(imgIds[0]));
                if (image != null)
                    imageUrl = HOST + Sitecore.Resources.Media.MediaManager.GetMediaUrl(image, urlOptions);
            }

            return new Home
            {
                Name = home.GetItemName(),
                SquareFeet = home.Fields[SCIDs.PlanBaseFields.SquareFootage].Value.CastAs<int>(0).ToString(DOLLAR_FORMAT),
                Address = home.Fields[SCIDs.HomesForSalesFields.StreetAddress1].Value,
                State = home.Parent.Parent.Parent.Parent.Parent.GetItemName(),
                City = home.Parent.Parent.Parent.GetItemName(),
                Bedrooms = home.Fields[SCIDs.PlanBaseFields.NumberOfBedrooms].Value,
                CommunityName = home.Parent.Parent.GetItemName(),
                PlanName = Regex.IsMatch(home.Parent.GetItemName(), REGEX_PLACEHOLDERNAME) ? home.Parent.GetItemName() : "Plan #" + home.Parent.GetItemName(),
                Garage = home.Fields[SCIDs.PlanBaseFields.NumderOfGarages].Value,
                CommunityURL = HOST + home.Parent.Parent.GetItemUrl().ShortenURL(SITENAME),
                HomeURL = HOST + home.GetItemUrl().ShortenURL(SITENAME),
                PlanURL = HOST + home.Parent.GetItemUrl().ShortenURL(SITENAME),
                RealtorAmount = home.Fields[SCIDs.HomesForSalesFields.RealtorIncentiveAmount].Value,
                RealtorRate = !string.IsNullOrWhiteSpace(home.Fields[SCIDs.HomesForSalesFields.RealtorBrokerCommissionRateOverride].Value)
                                                                ? home.Fields[SCIDs.HomesForSalesFields.RealtorBrokerCommissionRateOverride].Value
                                                                : !string.IsNullOrWhiteSpace(home.Fields[SCIDs.HomesForSalesFields.RealtorBrokerCommissionRate].Value)
                                                                ? home.Fields[SCIDs.HomesForSalesFields.RealtorBrokerCommissionRate].Value + PERCENT_SIGN
                                                                : string.Empty,
                MLS = home.Fields[SCIDs.HomesForSalesFields.MLS].Value,
                Stories = home.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value == ONE ? home.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value + SPACE + STORY : home.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value + SPACE + STORIES,
                LotNumber = home.Fields[SCIDs.HomesForSalesFields.LotNumber].Value,
                //Hacer brete con los halfbathrooms
                Baths = home.Fields[SCIDs.PlanBaseFields.NumberOfBathrooms].Value.ToString(),
                HalfBaths = home.Fields[SCIDs.PlanBaseFields.NumberOfHalfBathrooms].Value.ToString(),
                Price = home.Fields[SCIDs.HomesForSalesFields.Price].Value.CastAs<int>(0).ToString(DOLLAR_FORMAT),
                DivisionName = home.Parent.Parent.Parent.Parent.GetItemName(),
                ElevationImageURL = imageUrl + THUMBNAIL_WIDTH_HEIGHT,
                StatusForAvailability = AvalabilityText(home)
            };
        }

        private string AvalabilityText(Item inventory)
        {
            const string NAME_FIELD = "Name";
            string availabilityText;
            string statusForAvailability = inventory.Fields[SCIDs.HomesForSalesFields.StatusForAvailability].GetValue(string.Empty);

            if (Sitecore.Data.ID.IsID(statusForAvailability))
            {
                availabilityText = SCContextDB.GetItem(new ID(statusForAvailability)).Fields[NAME_FIELD].GetValue(string.Empty);
            }
            else
            {
                availabilityText = GetAvailabilityFromDate(inventory);
            }

            return availabilityText;
        }

        private string GetAvailabilityFromDate(Item inventory)
        {
            const string AVAILABLE_NOW = "Available Now!";
            const string AVAILABLE_IN = "Available in";
            const string SPACE = " ";
            const string MONTH_FULLNAME_FORMAT = "MMMM";

            var dateAvailable = DateUtil.ParseDateTime(inventory.Fields[SCIDs.HomesForSalesFields.AvaialabilityDate].Value,
                                                     default(DateTime));

            return dateAvailable == default(DateTime) ? string.Empty :
                dateAvailable.CompareTo(DateTime.Now) < 0 ? AVAILABLE_NOW : AVAILABLE_IN + SPACE + dateAvailable.ToString(MONTH_FULLNAME_FORMAT);
        }
    }

    public class Home
    {
        public string Name { get; set; }
        public string SquareFeet { get; set; }
        public string MLS { get; set; }
        public string RealtorAmount { get; set; }
        public string RealtorRate { get; set; }
        public string Bedrooms { get; set; }
        public string Garage { get; set; }
        public string Stories { get; set; }
        public string HomeURL { get; set; }
        public string PlanURL { get; set; }
        public string CommunityURL { get; set; }
        public string PlanName { get; set; }
        public string CommunityName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string LotNumber { get; set; }
        public string Baths { get; set; }
        public string HalfBaths { get; set; }
        public string Price { get; set; }
        public string DivisionName { get; set; }
        public string ElevationImageURL { get; set; }
        public string StatusForAvailability { get; set; }
    }


    #region Extensions
    public static class RWEExtensions
    {
        public static string ShortenURL(this string url, string siteName)
        {
            return url.Replace(string.Format("/sitecore/content/{0}/home", siteName), string.Empty);
        }
    }
    #endregion Extensions
}