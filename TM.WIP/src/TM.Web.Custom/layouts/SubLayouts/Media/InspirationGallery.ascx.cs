﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Media
{
    public partial class InspirationGallery : ControlBase
    {

        public string JSon = string.Empty;
        public string SpecialStyling = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentPage.Title = " - Inspiration Gallery";
            JSon = new MediaQueries().GetInspirationGalleryImages(CurrentPage.CurrentDivision.GetItemName());
            var company = Sitecore.Context.GetSiteName().ToLower();


            switch (company)
            {

                case CommonValues.DarlingHomesDomain:
                    CurrentPage.Title = "Darling Homes" + CurrentPage.Title;
                    break;
                case CommonValues.MonarchGroupDomain:
                    CurrentPage.Title = "Monarch Homes" + CurrentPage.Title;
                    SpecialStyling = "background:#DFDED8;";
                    break;
                    default:
                    CurrentPage.Title = "Taylor Morrison" + CurrentPage.Title;
                    break;
                    
            }

        }
    }
}