﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Sites;
using Sitecore.Web.UI.WebControls;
using TM.Domain;
using TM.Domain.Enums;
using TM.Utils.Web;
using TM.Web.Custom.Layouts;

namespace TM.Web.Custom.Layouts.SubLayouts
{
    public class ControlBase : UserControl
    {

        public Cache WebCache
        {
            get { return CurrentPage.WebCache; }

        }


        public BasePage CurrentPage
        {
            get
            {
#if (DEBUG)
                return (BasePage)Page;
#else
                return Page as BasePage;
#endif
            }
        }

        protected TMUser CurrentWebUser
        {
            get { return CurrentPage.CurrentWebUser; }
            set { CurrentPage.CurrentWebUser = value; }
        }

        protected  SiteContext SCContextSite
        {
            get { return Sitecore.Context.Site; }
        }
        protected  Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }

        protected Sitecore.Data.Items.Item SCContextItem
        {
            get { return CurrentPage.SCContextItem; }

        }

        protected string SCCurrentDomainName
        {
            get { return CurrentPage.SCCurrentDomainName; }
        }

		protected string SCCurrentDeviceName
		{
			get { return CurrentPage.SCCurrentDeviceName; }
		}


        protected DeviceType SCCurrentDeviceType
        {
            get { return CurrentPage.SCCurrentDeviceType; }
        }

        protected Sitecore.Data.Items.Item SCCurrentHomeItem
        {
            get
            {
                return
                    SCContextDB.GetItem(SCContextSite.StartPath);
            }
        }
        protected string SCCurrentSitePath
        {
            get { return SCContextSite.StartPath; }
        }

        protected string SCCurrentItemPath
        {
            get { return CurrentPage.SCCurrentItemPath; }
        }
        protected string SCCurrentHomePath
        {
            get { return SCCurrentHomeItem.Paths.FullPath; }
        }

        protected bool SCInPageEditorMode
        {
            get { return CurrentPage.SCInPageEditorMode; }
        }

        protected TMSession WebSession
        {
            get { return CurrentPage.WebSession; }
        }
        public string GoogleMapsApiKey
        {
            get 
            {
                if (Sitecore.Context.Site.Device.ToLower() == GlobalEnums.Domain.DarlingHomes.ToString().ToLower())
                    return Config.Settings.DHGoogleApiKey;

                return Config.Settings.TMGoogleApiKey; 
            }
        }
		public string FromEmail
		{
			get
			{
				if (Sitecore.Context.Site.Device.ToLower() == GlobalEnums.Domain.DarlingHomes.ToString().ToLower())
					return Config.Settings.DH_FromEmail;
				if (Sitecore.Context.Site.Device.ToLower() == GlobalEnums.Domain.MonarchGroup.ToString().ToLower())
					return Config.Settings.MG_FromEmail;

				return Config.Settings.TM_FromEmail;
			}
		}

        private Item _dataSource = null;
        public Item DataSource
        {
            get
            {
                if (_dataSource == null)
                    if (Parent is Sublayout)
                        _dataSource = Sitecore.Context.Database.GetItem(((Sublayout)Parent).DataSource);

                return _dataSource;
            }
        }
    }
}
