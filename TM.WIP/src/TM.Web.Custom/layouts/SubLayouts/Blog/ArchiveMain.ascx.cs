﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;

namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class ArchiveMain : ControlBase 
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Catergories));


            if (matchingTemplates != null)
            {

                if (matchingTemplates.Count() > 0)
                {
                    rptCategories.DataSource = matchingTemplates;
                    rptCategories.DataBind();

                    rptEntriesbyCategory.DataSource = matchingTemplates;
                    rptEntriesbyCategory.DataBind();
                }
            }

            Item[] matchingBlogTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Blog));
            string imageURL;

            if (matchingBlogTemplates != null)
            {

                 Sitecore.Data.Fields.ImageField imageField = matchingBlogTemplates[0].Fields["Image"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                        imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                        blogBanner.ImageUrl = imageURL;
                    }

            }

            //Item[] matchingEntries = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Entry));

            //if (matchingEntries != null)
            //{
            //    TotalPost.Text = matchingEntries.Count().ToString();
            //}

            //Item[] matchingComments = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Comments));

            //if (matchingComments != null)
            //{
            //    TotalCommetns.Text = matchingComments.Count().ToString();
            //}
        
        }


        private void Traverse(Item item, List<BlogEntry> entriesList,Item category)
        {
            Item itemCategory;
            BlogEntry blogEntry = new BlogEntry();

            if (item.TemplateID.ToString() == SCIDs.Blog.TM.Entry.ToString())
            {
                if (item.Fields["Category"].Value != "")
                {
                    List<string> categories = item.Fields["Category"].Value.Split('|').ToList();
                    
                    
                    foreach (string cat in categories)
                    {
                        if (cat == category.ID.ToString())
                        {
                            blogEntry.Date = item.Statistics.Created.ToShortDateString();
                            blogEntry.Name = item.Name;
                            blogEntry.Url = SCUtils.GetItemUrl(item).Replace(SCCurrentHomePath, string.Empty);
                            //Item[] matchingItems = item.Database.SelectItems(SCFastQueries.FindMatchingTemplates(item.Paths.FullPath, SCIDs.Blog.TM.Comments));

                            //if (matchingItems != null)
                            //{
                            //    blogEntry.CommentsSummary = matchingItems.Count();
                            //}

                            entriesList.Add(blogEntry);
                        }
                    }

                   
                }

                
            }

            Sitecore.Collections.ChildList childs = item.GetChildren();

            foreach (Item child in childs)
            {
                Traverse(child, entriesList, category);
            }

        }

        protected void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {


                        HyperLink categoryLink = (HyperLink)e.Item.FindControl("hlkCategory");
                        categoryLink.NavigateUrl = "~/blog/archive#" + currentItem.Fields["Title"].Value;
                        categoryLink.Text = currentItem.Fields["Title"].Value;

                       

                    }
            }

        }

        protected void rptEntriesbyCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;

            List<BlogEntry> entriesList = new List<BlogEntry>();

            //Assign the current item to HyperLink control define on repeater
            if (currentItem != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Blog));


                    if (matchingTemplates.Count() > 0)
                    {
                        Traverse(matchingTemplates[0], entriesList, currentItem);
                    }


                    //Label lblCategoryTitle = (Label)e.Item.FindControl("lblCategoryTitle");
                    //lblCategoryTitle.Text = currentItem.Fields["Title"].ToString();

                    System.Web.UI.HtmlControls.HtmlAnchor link = (System.Web.UI.HtmlControls.HtmlAnchor)e.Item.FindControl("catTitle");
                    //link.Attributes.Add("href", "#" + currentItem.Fields["Title"].ToString());
                    //link.Text = currentItem.Fields["Title"].ToString();
                    link.Name = currentItem.Fields["Title"].Value;

                    string entriesTotal = "";

                    if(entriesList != null)
                    {
                        entriesTotal = entriesList.Count().ToString();
                    }


                    link.InnerText = currentItem.Fields["Title"].Value + " (" + entriesTotal + ")";

                    Repeater rptEntries = (Repeater)e.Item.FindControl("rptEntries");
                    rptEntries.DataSource = entriesList;
                    rptEntries.DataBind();

                }
            }

           
        }
    }
}