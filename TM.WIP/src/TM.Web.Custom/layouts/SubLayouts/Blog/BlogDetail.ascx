﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogDetail.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.BlogDetail" %>
<sc:Placeholder ID="phBlogMenu" Key="phBlogMenu" runat="server" />

<asp:Repeater ID="rptBlogEntries" runat="server" OnItemDataBound="rptBlogEntries_ItemDataBound">
    <ItemTemplate>
        <div class="blo-ti">
            <div class="blogim">
                <asp:Image ID="tumbnail" runat="server" alt="" />
            </div>
            <asp:HyperLink ID="hlkTitle" runat="server"></asp:HyperLink>
            <div>
                <asp:Label ID="Owner" runat="server" Text=""></asp:Label></div>
        </div>
        <p>
            <asp:Label ID="Content" runat="server" Text=""></asp:Label></p>
        <asp:Panel ID="share" runat="server">
            <div class="share-blog">
                <p>
                    Share</p>
                <asp:HyperLink ID="lnkTwitter" runat="server" CssClass="blog-twitter" Target="_blank"></asp:HyperLink>
                <asp:HyperLink ID="lnkFacebook" runat="server" CssClass="blog-fb" Target="_blank"></asp:HyperLink>
                <asp:HyperLink ID="lnkG1" runat="server" CssClass="blog-g1" Target="_blank"></asp:HyperLink>
                <asp:HyperLink ID="lnkPinit" runat="server" CssClass="blog-pinit" Target="_blank"></asp:HyperLink>
            </div>
        </asp:Panel>
        <br />
    </ItemTemplate>
</asp:Repeater>
<asp:Panel ID="PagerPanel" runat="server">
    <div style="clear: both; display: block">
        <ul id="PostPager">
            <li>
                <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click">Next Posts</asp:LinkButton></li>
            <asp:Repeater ID="Pager" runat="server" OnItemDataBound="Pager_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <asp:HyperLink ID="lnkPage" runat="server"><%#Container.DataItem.ToString()%></asp:HyperLink></li>
                </ItemTemplate>
            </asp:Repeater>
            <li>
                <asp:LinkButton ID="lnkPrev" runat="server" OnClick="lnkPrev_Click">Previous Posts</asp:LinkButton></li>
        </ul>
    </div>
</asp:Panel>
