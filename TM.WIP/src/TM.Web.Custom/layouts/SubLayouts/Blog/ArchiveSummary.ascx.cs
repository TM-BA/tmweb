﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore.Data.Items;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;
using System.Linq.Expressions;
namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class ArchiveSummary : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var  entries = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Entry)).ToList();
            var orderedEntryYears = entries
                .Select(GetEntryDate)
                .Where(y => y != 1) //not min year
                .OrderByDescending(yr => yr)
                .Distinct();

            rptYears.DataSource = orderedEntryYears;
            rptYears.DataBind();

        }

        public void GetMonthlyEntries()
        {

            Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Blog));
        
            Item blog;
            Sitecore.Collections.ChildList childItems;

            if (matchingTemplates.Any())
            {
                blog = matchingTemplates[0];

                childItems = blog.GetChildren();

            }

        }

        public int GetEntryDate( Item entry)
        {
            var date = entry.Fields["DateCreated"].Value;
            var isDateTime = date.IsNotEmpty() && date!="$date" && Sitecore.DateUtil.IsIsoDate(date);
            return isDateTime ? SCUtils.SCDateTimeToMsDateTime(date).Year : entry.Statistics.Created.Year;
        }

        protected void rptYears_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            Item current = Sitecore.Context.Item;
            Sitecore.Collections.ChildList childItems;

            Item year;

            year = current.Database.GetItem((SCCurrentHomePath + "/Blog/" + e.Item.DataItem.ToString()));
	    if(year!=null)
	    {
	        childItems = year.GetChildren();
	        List<Archive> monthlyEntriesList = new List<Archive>();

	        if (childItems.Count > 0)
	        {
	            foreach (Item item in childItems)
	            {

	                Archive archive = new Archive();
	                archive.Month = item.DisplayName + " (" + item.GetChildren().Count.ToString() + ")";
	                archive.Url =  "/Blog?eid=" + year.DisplayName + "/" + item.DisplayName;

	                monthlyEntriesList.Add(archive);
	            }
            
	        }

	        Repeater rptMonthlyEntries = (Repeater)e.Item.FindControl("rptMonthlyEntries");
	        rptMonthlyEntries.DataSource = monthlyEntriesList;
	        rptMonthlyEntries.DataBind();
	    }

         
        }
    }
}
