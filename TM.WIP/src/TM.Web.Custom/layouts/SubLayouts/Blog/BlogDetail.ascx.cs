﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Express;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Domain.Entities;
using System.Linq.Expressions;
using Image = System.Web.UI.WebControls.Image;

namespace TM.Web.Custom.Layouts.SubLayouts.Blog
{
    public partial class BlogDetail : ControlBase 
    {
        protected object CurrentPageIndex
        {
            get
            {
                return ViewState["PageIndex"];
            }
            set
            {
                ViewState["PageIndex"] = value;
            }
        }

        protected object CurrentSearch
        {
            get
            {
                return ViewState["SearchKeyword"];
            }
            set
            {
                ViewState["SearchKeyword"] = value;
            }
        }

        protected object CurrentSearchTag
        {
            get
            {
                return ViewState["SearchTag"];
            }
            set
            {
                ViewState["SearchTag"] = value;
            }
        }

        PagedDataSource objPds = new PagedDataSource();
        int pageIndex=0;

        protected void Page_Load(object sender, EventArgs e)
        {
            
              PopulateBlogEntries();
            
        }

     

        protected void PopulateBlogEntries()
        {
            
            Item[] blogEntries = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath + "/blog", SCIDs.Blog.TM.Entry));
            List<Item> temp;

            List<Item> taggedEntries;

            if (blogEntries != null)
            {

                string tag = Request.QueryString["Tag"];
                if (!string.IsNullOrEmpty(tag))
                {
                    tag = tag.Replace('+', ' ');
                    CurrentSearchTag = tag;
                }

                string eid = Request.QueryString["eid"];
                CurrentSearch = eid;
                string currentUrl = Request.Url.AbsoluteUri;

                if (!string.IsNullOrEmpty(eid))
                {
                    if (currentUrl.ToLower().Contains("taylor"))
                    {
                        string[] words = eid.Split('/');
                        Page.Title = "Posts for " + words[1] + " " + words[0] + " - Taylor Morrison";
                        Page.MetaDescription = "See posts from " + words[1] + " " + words[0] +
                                               " on the Taylor Morrison blog.";
                    }
                    else
                    {
                        string[] words = eid.Split('/');
                        Page.Title = "Posts for " + words[1] + " " + words[0] + " - Darling Homes";
                        Page.MetaDescription = "See posts from " + words[1] + " " + words[0] +
                                               " on the Darling Homes blog.";                        
                    }
                    Item entryFolder = SCContextDB.GetItem(SCCurrentHomePath + "/Blog/" + eid);
                    if (entryFolder != null)
                    {

                        Sitecore.Collections.ChildList childItems = entryFolder.GetChildren();

                        objPds.DataSource = childItems;

                    }
                }
                else if (!string.IsNullOrEmpty(tag))
                {

                    taggedEntries = (from data in blogEntries where data.Fields["Tags"].Value.Contains(tag) select data).ToList();
                    objPds.DataSource = taggedEntries.OrderByDescending(x => x.Statistics.Created).ToList();

                }
                else
                { objPds.DataSource = blogEntries.OrderByDescending(x => x.Statistics.Created).ToList(); }

                
                objPds.AllowPaging = true;
                objPds.PageSize = 10;

                string page = Request.QueryString["page"];


                if (CurrentPageIndex != null)
                {
                    pageIndex = Convert.ToInt32(CurrentPageIndex);
                }

                if (!string.IsNullOrEmpty(page) )
                {
                    int.TryParse(page, out pageIndex);

                    if (pageIndex >= objPds.FirstIndexInPage && pageIndex <= objPds.PageCount)
                    {

                        objPds.CurrentPageIndex = (pageIndex-1);
                        CurrentPageIndex = pageIndex;

                        if (Convert.ToInt32(CurrentPageIndex) < objPds.PageCount)
                        {
                            lnkNext.Enabled = true;
                        }
                        else {
                            lnkNext.Enabled = false;
                        }

                        if (Convert.ToInt32(CurrentPageIndex) > 1)
                        {
                            lnkPrev.Enabled = true;
                        }
                        else
                        {
                            lnkPrev.Enabled = false;
                        }
                        
                    }
                }


                var blogs = new List<KeyValuePair<DateTime, Item>>();

                foreach (Item blog in objPds.DataSource)
                {
                    if (blog.Fields["datecreated"] != null)
                    {
                        var dateStr = blog["datecreated"];
                        if (!string.IsNullOrWhiteSpace(dateStr))
                        {
                            var date = SCHelpers.SCUtils.SCDateTimeToMsDateTime(dateStr);
                            blogs.Add(new KeyValuePair<DateTime, Item>(date, blog));
                        }
                    }
                    else
                    {
                        blogs.Add(new KeyValuePair<DateTime, Item>(blog.Statistics.Created, blog));
                    }

                }
                //key = datetime
                //value= blog item

                rptBlogEntries.DataSource = blogs.Where(x => x.Key <= DateTime.Now).OrderByDescending(x => x.Key).Select(x => x.Value).Take(10).ToList(); 
                rptBlogEntries.DataBind();

                List<string> pages = new List<string>();

                for(int i = 1; i <= objPds.PageCount; i++)
                {
                    pages.Add(i.ToString());
                }

                Pager.DataSource = pages;
                Pager.DataBind();

                if (pages.Count <= 1)
                {
                    PagerPanel.Visible = false;
              
                }

            }
            
        }

        //protected  void BindEntriesMonth(string month)
        //{

        //    Item blog = Sitecore.Context.Item;
        //    Item currentMonth = blog.Database.GetItem(blog.Paths.FullPath + "/" + month);

        //    if (currentMonth != null)
        //    {
        //        Sitecore.Collections.ChildList childItems = currentMonth.GetChildren();

        //        //EntryItem entry;



        //        //if (EntryList != null)
        //        //{
        //        //    EntryList.DataSource = childItems.ToList();
        //        //    EntryList.DataBind();
        //        //}

        //    }
        //}

        protected void rptBlogEntries_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            
            //get the current Item and cast it as Sitecore Item
            Item currentItem = (Item)e.Item.DataItem;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                HyperLink title = (HyperLink)e.Item.FindControl("hlkTitle");
                title.NavigateUrl = SCUtils.GetItemUrl(currentItem).Replace(SCCurrentHomePath, "");
                if (currentItem.Fields["Title"] != null)
                {
                    title.Text = currentItem.Fields["Title"].ToString();
                }

                Label locationlbl = (Label)e.Item.FindControl("Owner");

                if (!string.IsNullOrEmpty(currentItem.Fields["Author"].Value))
                {
                    DateTime date = currentItem.Fields["DateCreated"].Value != string.Empty ? SCUtils.SCDateTimeToMsDateTime(currentItem.Fields["DateCreated"].Value) : currentItem.Statistics.Created;
                    locationlbl.Text = "by " + currentItem.Fields["Author"].Value +", " + String.Format("{0:D}",date);
                }

                Label contentlbl = (Label)e.Item.FindControl("Content");

                if (!string.IsNullOrEmpty(currentItem.Fields["Content"].Value))
                {
                    contentlbl.Text = currentItem.Fields["Content"].Value;
                }


                string imageURL = string.Empty;
                Sitecore.Data.Fields.ImageField imageField = currentItem.Fields["Thumbnail Image"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                    imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                }

                if(string.IsNullOrEmpty(imageURL))
                {

                    Item[] matchingTemplates = SCContextDB.SelectItems(SCFastQueries.FindMatchingTemplates(SCCurrentHomePath, SCIDs.Blog.TM.Blog));
        
                    Item blog;

                    if (matchingTemplates.Any())
                    {
                        blog = matchingTemplates[0];

                        Sitecore.Data.Fields.ImageField blogEntryImage = blog.Fields["Thumbnail Entry Image"];
                        if (blogEntryImage != null && blogEntryImage.MediaItem != null)
                        {
                            Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(blogEntryImage.MediaItem);
                            imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                        }

                    }

                }

                Image tumbnail = (Image)e.Item.FindControl("tumbnail");

                tumbnail.ImageUrl = imageURL;


                //Social Links

                string facebookUrl = "";
                HyperLink lnkFacebook = (HyperLink)e.Item.FindControl("lnkFacebook");
                if (!string.IsNullOrEmpty(currentItem.Fields["Facebook URL"].Value))
                {
                    facebookUrl = ((Sitecore.Data.Fields.LinkField)currentItem.Fields["Facebook URL"]).Url;
                    lnkFacebook.NavigateUrl = facebookUrl;

                    if (facebookUrl == string.Empty)
                    {
                        lnkFacebook.Visible = false;
                    }
                }
                else
                    lnkFacebook.Visible = false;



                string twitterURL = "";
                HyperLink lnkTwitter = (HyperLink)e.Item.FindControl("lnkTwitter");
                if (!string.IsNullOrEmpty(currentItem.Fields["Twitter URL"].Value))
                {
                    twitterURL = ((Sitecore.Data.Fields.LinkField)currentItem.Fields["Twitter URL"]).Url;
                    lnkTwitter.NavigateUrl = twitterURL;

                    if (twitterURL == string.Empty)
                    {
                        lnkTwitter.Visible = false;
                    }
                }
                else
                    lnkTwitter.Visible = false;

                string pinitURL = "";
                HyperLink lnkPinit = (HyperLink)e.Item.FindControl("lnkPinit");
                if (!string.IsNullOrEmpty(currentItem.Fields["Pinterest URL"].Value))
                {
                    pinitURL = ((Sitecore.Data.Fields.LinkField)currentItem.Fields["Pinterest URL"]).Url;
                    lnkPinit.NavigateUrl = pinitURL;

                    if (pinitURL == string.Empty)
                    {
                        lnkPinit.Visible = false;
                    }
                }
                else
                    lnkPinit.Visible = false;

                string google1URL = "";
                HyperLink lnkG1 = (HyperLink)e.Item.FindControl("lnkG1");
                if (!string.IsNullOrEmpty(currentItem.Fields["Blog Rss URL"].Value))
                {
                    google1URL = ((Sitecore.Data.Fields.LinkField)currentItem.Fields["Blog Rss URL"]).Url;
                    lnkG1.NavigateUrl = google1URL;

                    if (google1URL == string.Empty)
                    {
                        lnkG1.Visible = false;
                    }
                }
                else
                    lnkG1.Visible = false;

                 Panel  share = (Panel)e.Item.FindControl("share");
                 if (lnkFacebook.Visible == false && lnkTwitter.Visible == false && lnkPinit.Visible == false && lnkG1.Visible == false)
                 {
                     share.Visible = false;
                 }
                


            }
        }


        

        protected void Pager_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HyperLink lnkPage = (HyperLink)e.Item.FindControl("lnkPage");

                string url = "";

                url = "/blog?Page=" + e.Item.DataItem;

                if(!string.IsNullOrEmpty((string)CurrentSearch))
                {

                    url = url + "&eid=" + CurrentSearch.ToString();
                    
                }else if (!string.IsNullOrEmpty((string)CurrentSearchTag))
                {

                    url = url + "&tag=" + CurrentSearchTag.ToString();
                    
                }



                lnkPage.NavigateUrl = url;

            }
        }

        protected void lnkPrev_Click(object sender, EventArgs e)
        {

            int prevIndex = Convert.ToInt32(CurrentPageIndex);
            if (Convert.ToInt32(CurrentPageIndex) >= 0)
            {
                prevIndex = prevIndex - 1;
                CurrentPageIndex = prevIndex;

                string url = "";

                url = "/blog?Page=" + prevIndex.ToString();

                if (!string.IsNullOrEmpty((string)CurrentSearch))
                {

                    url = url + "&eid=" + CurrentSearch.ToString();

                }else if (!string.IsNullOrEmpty((string)CurrentSearchTag))
                {

                    url = url + "&tag=" + CurrentSearchTag.ToString();

                }

                Response.Redirect(url);
                 
            }
            else
            {
                lnkPrev.Enabled = false;
            }
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {

            int nextIndex = Convert.ToInt32(CurrentPageIndex);
            if (Convert.ToInt32(CurrentPageIndex) <= objPds.PageCount)
            {
                nextIndex = pageIndex + 1;
                CurrentPageIndex = nextIndex;

                string url = "";

                url = "/blog?Page=" + nextIndex.ToString();

                if (!string.IsNullOrEmpty((string)CurrentSearch))
                {

                    url = url + "&eid=" + CurrentSearch.ToString();

                }else if (!string.IsNullOrEmpty((string)CurrentSearchTag))
                {

                    url = url + "&tag=" + CurrentSearchTag.ToString();

                }

                Response.Redirect(url);

            }
            else
            {
                lnkNext.Enabled = false;
            }
        }

        
    }
}
