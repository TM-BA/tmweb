﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using TM.Utils.Extensions;
using TM.Web.Custom.Layouts.SubLayouts;
using TM.Web.Custom.SCHelpers;
using TM.Utils.Web;


namespace TM.Web.Custom.Layouts.SubLayouts.HighTechHomes
{
  

    public partial class CommunityCard : ControlBase
    {

      

        public string Url
        {
            get { return BoundData("1"); }

        } 


        protected void Page_Load(object sender, EventArgs e)
        {

           
        }


  
        private string BoundData(string sortBy)
        {
           Sitecore.Data.Items.Item currentItem = Sitecore.Context.Item;
           string root =  currentItem.Fields["Root"].Value;
           string ppcurl = "";

           if (!string.IsNullOrEmpty(root))
           {
               try
               {
                   Sitecore.Data.Items.Item current = currentItem.Database.GetItem(root);

                   if (current != null)
                   {
                       ppcurl = current.Paths.FullPath;

                   }

               }
               catch (Exception)
               {

               }

            }

           return ppcurl;
            
        }

           

          
        
    }
}