﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Campaigns
{
    public partial class SpecificCampaign : ControlBase
    {
        public string CityName
        {
            get
            {
                if (CurrentPage.CurrentDivision != null)
                    return CurrentPage.CurrentDivision.GetItemName();
                return string.Empty;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SCContextItem["Background transparency RGBA color"]))
            {
                bannerLeftDiv.Attributes.Add("style", "background-color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Background transparency RGBA color"]))["Code"] + ");");
            }

            if (!string.IsNullOrWhiteSpace(SCContextItem["Campaign title box background RGBA color"]))
            {
                goDate.Attributes.Add("style", "background-color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Campaign title box background RGBA color"]))["Code"] + ");");
            }
            if (!string.IsNullOrWhiteSpace(SCContextItem["Left Section Link to Division Color"]))
            {
                litStyle.Text = "<style>.across-lt a,.across-lt .ltnavlnk li a {color:rgba(" + Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(SCContextItem["Left Section Link to Division Color"]))["Code"] + ");}</style>";
            }
            if (CurrentPage.CurrentDivision == null)
            {
                goAllCom.Visible = false;
            }
            else
            {
                hypAreaCommunities.NavigateUrl = SCUtils.GetItemUrl(CurrentPage.CurrentDivision).Replace(".aspx", string.Empty);
            }
        }
    }
}