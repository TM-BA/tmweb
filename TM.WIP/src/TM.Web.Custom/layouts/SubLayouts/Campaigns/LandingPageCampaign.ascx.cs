﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Sitecore.Data.Items;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts.SubLayouts.Campaigns
{
	public partial class LandingPageCampaign : ControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string values = SCContextItem.Fields["Landing Page Campaign"].Value;
			if (!string.IsNullOrEmpty(values))
			{
				string[] valuelist = values.Split('|');

				var searchHelper = new SearchHelper();
				foreach (var info in valuelist)
				{
					litLandingPageCampaign.Text = searchHelper.GetCampaignDetails(Sitecore.Context.Database.GetItem(info));
					break; 
				}
			}
			else
				litLandingPageCampaign.Text = string.Empty;
		}

		

	}
}