﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceVideos.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Homebuying101.ResourceVideos" %>
<div class="banner101">                 
    <a class="banner101-1" href="/home-buying/home-buying-101"></a>
    <a class="banner101-2" href="/home-buying/home-buying-201"></a>                 
</div>
<sc:placeholder runat="server" id="GlossaryListing" key="GlossaryListing">
    
</sc:placeholder>
<sc:fieldrenderer runat="server" id="ContentArea" fieldname="Content Area"></sc:fieldrenderer>
