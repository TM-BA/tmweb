﻿using System;
using System.Web;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Resources.Media;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.Layouts
{
	public partial class DH_Main : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        protected void Page_Init(object sender, EventArgs e)
        {
            var webform = (SitecoreSimpleForm)wfmJoinYourInterestList.FormInstance;
            webform.FailedSubmit += new EventHandler<EventArgs>(webform_FailedSubmit);


        }

        void webform_FailedSubmit(object sender, EventArgs e)
        {
            JoinYourInterestFormPosted = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                if (PostedWFFMForm == SCIDs.WebForms.JoinOurInterestList.ToString())
                    JoinYourInterestFormPosted = true;
            }
            Page.Title = PageTitle;
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccountDH) ?
                                            GoogleAnalytics.DarlingHomesAccountNumber :
                                             Config.Settings.GoogleAnalyticsAccountDH;

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            if (companyLogo != null && companyLogo.MediaItem != null)
            {
                imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
                imgSiteLogo.AlternateText = companyLogo.Alt;
            }

            var myTMLogo = CurrentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];

            if (myTMLogo != null && myTMLogo.HasValue)
            {
                imgMyCompanyLogo.ImageUrl = myTMLogo.GetMediaUrl();

            }

            LinkField youtubelink = CurrentHomeItem.Fields["YouTube URL"];

			litUserAuthentication.Text = WebUserExtensions.GetCredentialText(CurrentWebUser);

			//if url is new homes - redirect to home page
			var item = SCContextDB.GetItem(SCContextItem.Paths.FullPath);
			if (item.TemplateID == SCIDs.TemplateIds.ProductTypePage)
				Response.Redirect("/");
            var contextItem = SCContextItem;
            if (contextItem.Fields["Tracking Code"] != null)
            {
                yahooPixels.Text = contextItem.Fields["Tracking Code"].Value;
            }
            else { yahooPixels.Visible = false; }
        }

	    protected bool JoinYourInterestFormPosted { get; set; }

        public static bool IsMobile()
        {
            //GETS THE CURRENT USER CONTEXT
            var context = HttpContext.Current;
            //FIRST TRY BUILT IN ASP.NET CHECK
            if (context.Request.Browser.IsMobileDevice)
            {
                return true;
            }

            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }

            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
                context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }

            //AND FINALLY CHECK THE HTTP_USER_AGENT
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                var userAgent = context.Request.ServerVariables["HTTP_USER_AGENT"].ToLower();

                var tablets =
                  new[]
            {
              "ipad", "android 3", "xoom", "sch-i800", "tablet", "kindle", "playbook"
            };

                //Loop through each item in the list created above
                //and check if the header contains that text
                if (tablets.Any(userAgent.Contains) || (userAgent.Contains("android") && !userAgent.Contains("mobile")))
                {
                    return true;//if you want to include the scripts in tablets
                }

                //Create a list of all mobile types
                var mobiles =
                  new[]
            {
              "midp", "j2me", "avant", "docomo",
              "novarra", "palmos", "palmsource",
              "240x320", "opwv", "chtml",
              "pda", "windows ce", "mmp/",
              "blackberry", "mib/", "symbian",
              "wireless", "nokia", "hand", "mobi",
              "phone", "cdm", "up.b", "audio",
              "SIE-", "SEC-", "samsung", "HTC",
              "mot-", "mitsu", "sagem", "sony"
              , "alcatel", "lg", "eric", "vx",
              "NEC", "philips", "mmm", "xx",
              "panasonic", "sharp", "wap", "sch",
              "rover", "pocket", "benq", "java",
              "pt", "pg", "vox", "amoi",
              "bird", "compal", "kg", "voda",
              "sany", "kdd", "dbt", "sendo",
              "sgh", "gradi", "jb", "dddi",
              "moto", "iphone", "Opera Mini"
            };

                //Loop through each item in the list created above
                //and check if the header contains that text
                if (mobiles.Any(userAgent.Contains))
                {
                    return true;
                }
            }

            return false;
        }    
    }
}