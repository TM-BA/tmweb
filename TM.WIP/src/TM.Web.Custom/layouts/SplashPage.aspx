﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SplashPage.aspx.cs" Inherits="TM.Web.Custom.Layouts.SplashPage" %>
<%@ Import Namespace="TM.Utils.Web" %>
<%@ Import Namespace="TM.Web.Custom.Constants.GoogleEventing" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core"
	TagPrefix="wfm" %>
<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/USCanadaSVGMap.ascx" %>
<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head runat="server">

	<meta charset="utf-8" />
	<title>Taylor Morrison Homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<sc:Placeholder ID="phHeadSection" runat="server" Key="HeadSection" />
	<!--[if gte IE 9]>
	  <style type="text/css">
  	    .gradient {
  	        filter: none;
  	    }
  	</style>
	<![endif]-->
	<%= JsCssCombiner.GetSet("mainjs") %>
	 <sc:VisitorIdentification runat="server" />
    <asp:Literal runat="server" ID="litStyleSheet"></asp:Literal>
    <link rel="stylesheet" href="/Styles/main.css" />
    <link rel="stylesheet" href="/Styles/themes/dh_theme.css" runat="server" id="styleDH" Visible="False" />
    <link rel="stylesheet" href="/Styles/themes/mg_theme.css" runat="server" id="styleMG" Visible="False" />
    <link rel="stylesheet" href="/Styles/themes/tm_theme.css" runat="server" id="styleTM" Visible="False" />
    
    
	
</head>

<body id="tm" runat="server">
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MMZQFQ"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MMZQFQ');</script>
    <!-- End Google Tag Manager -->
 
	<%--<a href="<%= PrintFriendlyUrl %>" class="printLink">Oops, this page has not been formated
		for your printer, click here to print that instead.</a> <span id="slideImgExpander">
		</span>--%>
	<form id="form1" runat="server">
    
	<header runat="server" id="headerSection">
        <div class="header">
			<div class="topheader">
				<ul>
					<sc:Sublayout runat="server" ID="subnowtrending" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx">
					</sc:Sublayout>

					<li class="top4"><a href="/Account/My-Profile" onclick="TM.Common.GAlogevent('CreateAccount','Click','CreateAccountLink')">
						<asp:Image runat="server" ID="imgMyCompanyLogo" />
					</a></li>
					<li class="top5" id="logintext">
						<asp:Literal runat="server" ID="litUserAuthentication"></asp:Literal></li>
                    <li class="top3"><a id="lijoininterest" href="javascript:;" onclick="TM.Common.GAlogevent('JoinInterestList','Click','InHeader')">JOIN OUR INTEREST LIST</a>
						<div id="joinyourinterest" class="join-wrapper">
							<wfm:FormRender runat="server" ID="wfmJoinYourInterestList" FormID="{E059951C-1C5A-464E-A876-4C06FFD10F26}" />
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="hdline">
		</div> 
		<div class="logomenu">
			<div class="lt-box">
				<a href="/" class="logo">
					<asp:Image runat="server" ID="imgSiteLogo" /></a>
				<div class="searchbox">
					<input type="text" placeholder="Search Site, Enter myTM# or MLS#" id="siteSearchText" /><a
						href="#" onclick="sitesearch('#siteSearchText')"></a>
				</div>
			</div>
			<div class="rt-box">
				<div onclick='$j(".rt-box nav ul").toggle()' class="MobileMenuLink">
					Menu</div>
				<nav id="navtop-main">
					<ul>
						<li id="findyournewhome"><a href="#" onclick="TM.Common.GAlogevent('Search','Click','InHeaderFindYourNewHomes')"><span>FIND</span><span>YOUR HOME</span></a></li>
						<sc:Sublayout ID="Sublayout1" runat="server" Path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" />
					</ul>
					<div class="clearfix">
					</div>
				</nav>
			</div>
            <div class="mapWrapper">
                <uc:SVGMap ID="svgMap" runat="server" />
            </div>
			<div class="breadcrumb">
				<tm:Breadcrumb ID="bcBreadcrumb" runat="server" DividerText=">" />
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="clearfix">
		</div>
	</header>
    <div class="home-content  home-content-wrapper"  >
	    <sc:Placeholder runat="server" ID="phContent" Key="SplashPageLayoutContainer"/>
    </div>
	<footer runat="server" id="footerSection">
		<article>
			<sc:Sublayout ID="footer" runat="server" Path="/tmwebcustom/sublayouts/common/footer.ascx" />
			<sc:Placeholder ID="phFooter" runat="server" Key="FootSection" />
		</article>
	</footer>
   
	</form>
	<script type="text/javascript">
		$j(document).ready(function () {
			$j("li#findyournewhome").click(function () {
				$j("li#findyournewhome a").last().addClass("selected");
				$j('#svgmap').show();				 
			});
			$j("a#svgclose").click(function () {
				$j('#svgmap').hide();
				$j("li#findyournewhome a").removeClass("selected");
			});

			$j("a#lijoininterest").click(function () {
			    
				$j('#joinyourinterest').toggle();
			   <%=HomePageEvents.InHeader %>
			});
		    
		    <%= JoinYourInterestFormPosted?@"$j('#joinyourinterest').show();":string.Empty %>
		});

	    $j(document).ready(function () {
	        $j('#siteSearchText').on("keypress", function (e) {
	            if (e.keyCode == 13) {
	                sitesearch('#siteSearchText');
	                return false;
	            }
	        });
	    });		
	</script>

    <asp:literal runat="server" id="yahooPixels"></asp:literal>
     
</body>
</html>
