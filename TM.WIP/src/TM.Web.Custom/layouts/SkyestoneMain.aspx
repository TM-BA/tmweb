﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SkyestoneMain.aspx.cs"
    Inherits="TM.Web.Custom.Layouts.SkyestoneMain" %>

<%@ Import Namespace="TM.Utils.Web" %>
<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head>
    <meta charset="utf-8" />
    <title>Skyestone Homepage</title>
    <link rel="stylesheet" type="text/css" href="/Styles/Microsites/Skyestone.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <%=JsCssCombiner.GetSet("blanklayoutjs")%>
    <script type="text/javascript">

        var $j = jQuery.noConflict();

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
        _gaq.push(['_setLocalRemoteServerMode']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();


        TM.Common.GAlogevent = function (Category, Action, Label, OptValue) {
            
            _gaq.push(['_trackEvent', Category, Action, Label, OptValue]);
        };

        <sc:VisitorIdentification runat="server" />
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="header">
            <sc:Placeholder runat="server" ID="phHeader" Key="Header" />
        </div>
        <!--end header-->
        <form id="form1" runat="server">
            <sc:Placeholder runat="server" ID="phContent" Key="Content" />
        </form>
        <sc:Placeholder runat="server" ID="phFooter" Key="Footer" />
        <!--end footer-->
    </div>
    <!--end wrapper-->
</body>
</html>
