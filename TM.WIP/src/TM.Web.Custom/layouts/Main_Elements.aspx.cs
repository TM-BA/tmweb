﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Utils.Extensions;
namespace TM.Web.Custom.Layouts
{
    public partial class Main_Elements : BasePage
    {
        public string GoogleAnalyticsAccountNumber;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.IsPostBack)
            {

                if (PostedWFFMForm == SCIDs.WebForms.JoinOurInterestList.ToString())
                    JoinYourInterestFormPosted = true;
            }
            
            GoogleAnalyticsAccountNumber = String.IsNullOrEmpty(Config.Settings.GoogleAnalyticsAccount) ?
                                          "UA-37702556-1" :
                                           Config.Settings.GoogleAnalyticsAccount;

            ImageField companyLogo = CurrentHomeItem.Fields[SCIDs.Global.CompanyLogo];

            if (companyLogo != null && companyLogo.MediaItem != null)
            {
                imgSiteLogo.ImageUrl = MediaManager.GetMediaUrl(companyLogo.MediaItem);
                imgSiteLogo.AlternateText = companyLogo.Alt;
            }

            LinkField youtubelink = CurrentHomeItem.Fields["YouTube URL"];


        }

        protected bool JoinYourInterestFormPosted { get; set; }
    }
}