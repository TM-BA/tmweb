﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Sitecore.Sites;
using TM.Utils.Web;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using Sitecore.Security;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Domain.Enums;
using Sitecore.Data.Fields;

namespace TM.Web.Custom.Layouts
{
    public class BasePage : Page
    {
        private readonly Cache _webCache = new Cache();
        public TMSession WebSession;

        internal object PageTitleObject
        {
            get
            {
                var Company = CurrentHomeItem.GetItemName();
                var Product = CurrentProduct.GetItemName();
                var State = CurrentState.GetItemName();
                var Division = CurrentDivision.GetItemName();
                var City = CurrentCity.GetItemName();
                var Community = CurrentCommunity.GetItemName();
                var CurrentPage = SCContextItem.GetItemName();
                var Parent = SCContextItem.Parent.GetItemName();
                //FB Case 97: Add additional title tag tokens
                var Zip = CurrentCommunity.Fields[SCIDs.CommunityFields.ZipOrPostalCode].Value;
                var StateAbbreviated = CurrentState.GetItemName().GetStateAbbreviationByState(); 
                var PageName = SCContextItem.Name;

                return new
                {
                    Company = CurrentHomeItem.GetItemName(),
                    Product = CurrentProduct.GetItemName(),
                    State = CurrentState.GetItemName(),
                    Division = CurrentDivision.GetItemName(),
                    City = CurrentCity.GetItemName(),
                    Community = CurrentCommunity.GetItemName(),
                    CurrentPage = SCContextItem.GetItemName(),
                    Zip = CurrentCommunity.Fields[SCIDs.CommunityFields.ZipOrPostalCode].Value,
                    StateAbbreviated = CurrentState.GetItemName().GetStateAbbreviationByState(),
                    Parent = SCContextItem.Parent.GetItemName(),
                    PageName = SCContextItem.Name
                };
            }
        }
        private string _pageTitle;

        /* protected override PageStatePersister PageStatePersister
        {
            get
            {
                return new SessionPageStatePersister(this);
            }
        }*/


        protected override void OnPreInit(EventArgs e)
        {
            WebSession = new TMSession();
            base.OnPreInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {

            if (!Sitecore.Security.Accounts.User.Current.Identity.IsAuthenticated
                 && !HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant().Contains("/logout")
                 && !HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant().Contains("/notindexed/disclaimer.aspx"))
            {
                TMUser tmUser;
                if ((tmUser = WebUserExtensions.LoginUsingIfpssoCookieAndSetCurrentUser()) != null)
                {
                    CurrentWebUser = tmUser;
                    ClientScriptManager cs = Page.ClientScript;
                    cs.RegisterStartupScript(GetType(), "logintext", "ShowFaviLogoutText()", true);
                }
            }

            GetMetadataDescription();
            GetMetadataKeywords();

            base.OnLoad(e);
        }

        public string PageTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_pageTitle))
                {
                    if (System.Web.HttpContext.Current.Items.Contains("PageTitle"))
                    {
                        _pageTitle = System.Web.HttpContext.Current.Items["PageTitle"].CastAs<string>();
                    }
                    else
                    {
                        _pageTitle = string.IsNullOrWhiteSpace(SCContextItem["Title Template"])
                                       ? GetTagsReplaced(SCContextItem["Page Title"].NamedFormat(PageTitleObject), PageTitleObject)
                                       : GetTagsReplaced(SCContextItem["Title Template"].NamedFormat(PageTitleObject), PageTitleObject);
                        System.Web.HttpContext.Current.Items["PageTitle"] = _pageTitle;
                    }
                }
                return _pageTitle;
            }
            set
            {
                _pageTitle = value;
                System.Web.HttpContext.Current.Items["PageTitle"] = _pageTitle;
            }
        }

        private void GetMetadataDescription()
        {
            try
            {
                if (SCContextItem == null)
                    return;
                Page.MetaDescription = GetTagsReplaced(SCContextItem["Description"].NamedFormat(PageTitleObject), PageTitleObject);
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error While Setting Metadata Description", e);
            }

        }

        private void GetMetadataKeywords()
        {
            try
            {
                if (SCContextItem == null)
                    return;
                Page.MetaKeywords = GetTagsReplaced(SCContextItem["Keywords"].NamedFormat(PageTitleObject), PageTitleObject);
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error While Setting Metadata Keywords", e);
            }
        }

        internal string GetTagsReplaced(string tagvalue, object source)
        {
            string replacedValue = tagvalue;
            try
            {
                if (tagvalue.IndexOf("@name", System.StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    replacedValue = tagvalue.Replace("@name", DataBinder.Eval(source, "PageName").ToString());
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("Error While replacing Tags @name", e);
            }
            return replacedValue;
        }

        public Cache WebCache
        {
            get { return _webCache; }
        }


        //set when logged in
        public TMUser CurrentWebUser
        {
            get
            {
                return WebSession.GetUser<TMUser>();
            }
            set
            {

                WebSession.SetUser<TMUser>(value);
            }
        }

        public string PrintFriendlyUrl
        {
            get
            {
                return (Page.Request.RawUrl.Contains("?") ? (Page.Request.RawUrl + "&p=1") : (Page.Request.RawUrl + "?p=1"));
            }
        }

        private string _currentLiveChatSkill = string.Empty;
        public string CurrentLiveChatSkill
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_currentLiveChatSkill))
                {
                    Item currentNode = SCContextItem;
                    while (currentNode.ID != CurrentHomeItem.ID)
                    {
                        if (!string.IsNullOrWhiteSpace(currentNode[SCFieldNames.LiveChatSkillCode]))
                        {
                            return currentNode[SCFieldNames.LiveChatSkillCode];
                        }
                        currentNode = currentNode.Parent;
                    }
                }
                else
                    return _currentLiveChatSkill;
                return "";
            }
            set { _currentLiveChatSkill = value; }
        }

        private Item _currentCity;
        public Item CurrentCity
        {
            get
            {
                return _currentCity ??
                       (_currentCity =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CityPage, SCContextItem.ID,
                                                             CurrentHomeItem));
            }
        }
        private Item _currentCommunity;
        public Item CurrentCommunity
        {
            get
            {
                return _currentCommunity ??
                       (_currentCommunity =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, SCContextItem.ID,
                                                             CurrentHomeItem));
            }
        }
        private Item _currentState;
        public Item CurrentState
        {
            get
            {
                return _currentState ??
                       (_currentState =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.StatePage, SCContextItem.ID,
                                                             CurrentHomeItem));
            }
        }
        private Item _currentIHC;
        public Item CurrentIHC
        {
            get
            {
                if (_currentIHC == null)
                {
                    string ihcID;
                    System.Collections.Generic.List<string> possibleIHCs = new System.Collections.Generic.List<string>();

                    Sitecore.Data.Items.Item currentNode = SCContextItem;
                    while ((currentNode.ID != CurrentHomeItem.ID) && (possibleIHCs.Count == 0))
                    {
                        if (!string.IsNullOrWhiteSpace(currentNode[SCFieldNames.InternetHomeConsultant]))
                        {
                            possibleIHCs.AddRange(currentNode[SCFieldNames.InternetHomeConsultant].Split('|'));
                        }
                        currentNode = currentNode.Parent;
                    }
                    if (possibleIHCs.Count > 0)
                    {
                        var randomIndex = new Random();
                        while (_currentIHC == null && possibleIHCs.Count > 0)
                        {
                            int index = randomIndex.Next(possibleIHCs.Count);
                            ihcID = possibleIHCs[index];
                            var possibleIHC = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID(ihcID));
                            if (possibleIHC["Status"] != TM.Web.Custom.Constants.SCIDs.StatusIds.Status.Active)
                                possibleIHCs.RemoveAt(index);
                            else
                                _currentIHC = possibleIHC;
                        }
                    }
                }
                return _currentIHC;
            }
        }
        private Item _currentDivision;
        public Item CurrentDivision
        {
            get
            {
                return _currentDivision ??
                       (_currentDivision =
                        SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, SCContextItem.ID,
                                                             CurrentHomeItem));
            }
        }

        public bool SCInPageEditorMode
        {
            get { return Sitecore.Context.PageMode.IsPageEditor; }
        }
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            if (WebSession != null)
                WebSession.Save();
        }

        public Item SCContextItem
        {
            get { return Sitecore.Context.Item; }
        }

        protected string PostedWFFMForm
        {
            get
            {
                var postedForm = HttpContext.Current.Items["WFFMPostedFormID"];
                return postedForm.CastAs<string>(string.Empty);
            }
        }

        protected SiteContext SCCurrentSite
        {
            get { return Sitecore.Context.Site; }
        }
        public Sitecore.Data.Database SCContextDB
        {
            get { return Sitecore.Context.Database; }
        }


        public Item CurrentHomeItem
        {
            get
            {
                return SCContextDB.GetItem(SCCurrentSite.StartPath);
            }
        }
        public Item CurrentProduct
        {
            get
            {
                return SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.ProductTypePage, SCContextItem.ID, CurrentHomeItem);
            }
        }
        protected string SCCurrentSitePath
        {
            get { return SCCurrentSite.StartPath; }
        }

        protected string SCCurrentHomePath
        {
            get { return CurrentHomeItem.Paths.FullPath; }
        }

        public string SCCurrentDeviceName
        {
            get { return Sitecore.Context.GetDeviceName() == "Default" ? "TaylorMorrison" : Sitecore.Context.GetDeviceName(); }
        }

        public DeviceType SCCurrentDeviceType
        {
            get
            {

                switch (Sitecore.Context.GetDeviceName())
                {
                    case "TaylorMorrison":
                        return DeviceType.TaylorMorrison;

                    case "MonarchGroup":
                        return DeviceType.MonarchGroup;

                    case "DarlingHomes":
                        return DeviceType.DarlingHomes;

                    default:
                        return DeviceType.TaylorMorrison;


                }

            }
        }

        public string SCCurrentDomainName
        {
            get { return SCCurrentSite.Domain.Name; }
        }

        private bool _showCommunityHours = false;
        public bool ShowCommunityHours
        {
            get
            {
                return _showCommunityHours;
            }
            set
            {
                _showCommunityHours = value;
            }
        }

        public string SCCurrentItemPath
        {
            get { return SCContextItem.Paths.FullPath; }

        }
        private string _facebookUrl;
        public string FacebookUrl
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }

                while (string.IsNullOrWhiteSpace(_facebookUrl) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Facebook URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _facebookUrl = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _facebookUrl;
            }
        }

        private string _instagramURL;
        public string InstagramURL
        {
            get
            {
                _instagramURL = string.Empty;
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }

                while (string.IsNullOrWhiteSpace(_instagramURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Instagram URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _instagramURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _instagramURL;
            }
        }

        private string _linkedInURL;
        public string LinkedInURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }

                while (string.IsNullOrWhiteSpace(_linkedInURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["LinkedIn URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _linkedInURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _linkedInURL;
            }
        }



        private string _youTubeURL;
        public string YouTubeURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_youTubeURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["YouTube URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _youTubeURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _youTubeURL;
            }
        }
        private string _twitterURL;
        public string TwitterURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_twitterURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Twitter URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _twitterURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _twitterURL;
            }
        }
        private string _pinterestURL;
        public string PinterestURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_pinterestURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Pinterest URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _pinterestURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _pinterestURL;
            }
        }
        private string _googlePlusURL;
        public string GooglePlusURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_googlePlusURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["GooglePlus URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _googlePlusURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _googlePlusURL;
            }
        }
        private string _blogRssURL;
        public string BlogRssURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_blogRssURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Blog Rss URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _blogRssURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _blogRssURL;
            }
        }
        private string _blogURL;
        public string BlogURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_blogURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields["Blog URL"];
                    if (item != null && item.Value.IsNotEmpty())
                        _blogURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _blogURL;
            }
        }

        private string _pressReleaseURL;
        public string PressReleaseURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
               { 
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_pressReleaseURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields[SCIDs.HomePage.PressRoomURL];
                    if (item != null && item.Value.IsNotEmpty())
                        _blogURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _blogURL;
            }
        }

        private string _careersLinkURL;
        public string CareersLinkURL
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_careersLinkURL) && isBreakStatement(currentNode))
                {
                    var item = currentNode.Fields[SCIDs.HomePage.CareersLinkURL];
                    if (item != null && item.Value.IsNotEmpty())
                        _careersLinkURL = item.GetLinkFieldPath();
                    currentNode = currentNode.Parent;
                }
                return _careersLinkURL;
            }
        }

        private string _disclaimerText;
        public string DisclaimerText
        {
            get
            {
                Item currentNode;
                try
                {
                    currentNode = SCContextItem["Root"].IsNotEmpty() ? SCContextDB.GetItem(new Sitecore.Data.ID(SCContextItem["Root"])) : SCContextItem;
                }
                catch (Exception ex)
                {
                    currentNode = SCContextItem;
                }
                while (string.IsNullOrWhiteSpace(_disclaimerText) && isBreakStatement(currentNode))
                {
                    if (string.IsNullOrWhiteSpace(currentNode["Disclaimer"]))
                    {
                        currentNode = currentNode.Parent;
                    }
                    else
                    {
                        _disclaimerText = currentNode["Disclaimer"].Replace("\n", "<br />");
                    }
                }
                return _disclaimerText;
            }
        }

        public string SCCurrentItemUrl
        {
            get
            {
                var currentItemURL = SCContextItem.GetItemUrl().ToLowerInvariant();
                return currentItemURL;
            }

        }
        public string SCVirtualItemUrl
        {
            get
            {
                var virtualUrls = PageUrls.VirtualUrls;

                var currentItemURL = SCContextItem.GetItemUrl().ToLowerInvariant();
                var rawUrlAbsolutePath = Request.Url.AbsolutePath.ToLowerInvariant();
                var tabUrl = rawUrlAbsolutePath.Replace(currentItemURL, string.Empty).Replace("/", string.Empty);

                if (virtualUrls.Contains(tabUrl) || PageUrls.AdditionalCommunityPages.Contains(tabUrl))
                    return rawUrlAbsolutePath;

                return currentItemURL;
            }

        }

        public bool isBreakStatement(Item currentNode)
        {
            return ((currentNode.TemplateID.ToString() != SCIDs.TemplateIds.SwitchCompanyPage) &&
                    currentNode.TemplateID != Sitecore.ItemIDs.RootID && currentNode.TemplateID.ToString() != SCIDs.TemplateIds.MicrositeTemplate);
        }
    }
}
