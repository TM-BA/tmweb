﻿using Sitecore.Data.Fields;
using System;

namespace TM.Web.Custom.Layouts
{
    public partial class HighlandRanchMain : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            headerPanel.Visible = ((CheckboxField)Sitecore.Context.Item.Fields["ShowHeader"]).Checked;
            footerPanel.Visible = ((CheckboxField)Sitecore.Context.Item.Fields["ShowFooter"]).Checked;

            Page.Title = PageTitle;
        }

        protected string BodyClass
        {
            get
            {
                var cssClass = Sitecore.Context.Item["CssClass"];
                if (string.IsNullOrWhiteSpace(cssClass))
                    return Sitecore.Context.Item.Name.ToLowerInvariant().Replace(" ","-");
                return cssClass;
            }
        }
    }
}