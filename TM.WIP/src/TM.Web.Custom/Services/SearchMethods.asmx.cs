﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Form.Core.Web;
using Sitecore.Links;
using TM.Utils.Extensions;
using TM.Utils.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.SCHelpers.Search;
using TM.Web.Custom.Search.LuceneQueries;
using TM.Web.Custom.WebControls;
using SCID = Sitecore.Data.ID;

namespace TM.Web.Custom.Services
{
	/// <summary>
	/// Summary description for SearchMethods
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
       
	public class SearchMethods : System.Web.Services.WebService
	{
		#region Search Results

		[WebMethod]
        public string GetSearchResults(string searchLocation, string sortBy, string favorites, bool isRealtor, bool isClearSession, bool isHTH)
		{
		    if (!isClearSession)
		    {
		        var searchKey = searchLocation + sortBy + favorites + isRealtor;
		        return new Cache().Get(searchKey,
		                               () =>
		                               RefineSearchResultsbyFilter(searchLocation, sortBy, favorites, isRealtor,
                                                                   isClearSession, isHTH));
		    }
            return RefineSearchResultsbyFilter(searchLocation, sortBy, favorites, isRealtor, isClearSession, isHTH);
		}

        private string RefineSearchResultsbyFilter(string searchLocation, string sortBy, string favorites, bool isRealtor, bool isClearSession, bool isHTH)
        {
            var searchResultsbyFilter = RefineSearchResultsbyFilter(searchLocation, sortBy, favorites, isRealtor, new Dictionary<string, string>(), isClearSession, isHTH);
            return searchResultsbyFilter.ToJSON(true);
        }



        private SearchResults RefineSearchResultsbyFilter(string searchLocation, string sortBy, string favorites, bool isRealtor, Dictionary<string, string> filters, bool isClearSession, bool isHTH)
		{
			var searchHelper = new SearchHelper();

            if (isHTH)
            {
                return searchHelper.HydrateSearchResults(searchLocation, sortBy, favorites, isRealtor, filters, isClearSession, isHTH);
            }
            else {
                return searchHelper.HydrateSearchResults(searchLocation, sortBy, favorites, isRealtor, filters, isClearSession);
            }
		}

        

		[WebMethod]
		public string RefineSearchByFacet(string isRealtor, string searchPath, string favi, string sortby, string area, string city, string stEdPrices, string stEdSqft, string schooldt, string comStatus, string[] beds, string[] baths, string[] garages, string[] stories, string availability, string bonusDenSolar)
		{
			var filters = GetSelectedFacetValues(searchPath, area, city, stEdPrices, stEdSqft, schooldt, comStatus, beds, baths, garages,
												 stories, availability, bonusDenSolar);
			var refineSearchResults = RefineSearchResultsbyFilter(searchPath, sortby, favi, Convert.ToBoolean(isRealtor), filters, false,false);

			return refineSearchResults.ToJSON();
		}

		#endregion

		#region SVG Map Data

                [WebMethod]
		public string GetSvgData()
		{
	                Uri uri = HttpContext.Current.Request.Url;
			var key = uri.Host + "svgmap";
		        return new Cache().Get(key, GetData);
                       
		      
		}


	        public string GetData()
	        {
                var items = CompanySearch.GetCurrentCompanyStateMarketingList();

                var activeStateItems = items.Where(s => s.TemplateID == SCIDs.TemplateIds.StatePage).OrderBy(o => o.Name).ToList();
                var activedivisionItems = items.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name).ToList();

                var svgMapData = new SvgMapData();
                var stValues = new Dictionary<string, string>();
                var alldiviValues = new Dictionary<string, List<SvgDivisionData>>();
                var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };
                Uri uri = HttpContext.Current.Request.Url;

                foreach (Item info in activeStateItems)
                {
                    if (!stValues.ContainsKey(info.DisplayName.GetStateAbbreviationByState().ToString()))
                    {
                        //collect active state list
                        stValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), info.DisplayName);

                        //collect division and url details
                        var divisionValues = (from ct in activedivisionItems
                                              where ct.ParentID == info.ID
                                              let url = ct.GetCrossSiteSpecification(sitecoreUrlOptions, uri).SiteSpecificUrl
                                              select new SvgDivisionData()
                                              {
                                                  N = ct.DisplayName,
                                                  h = url,
                                                  tar = url.Contains(uri.Authority),
                                                  X = ct["X Value"].StringToInt32(),
                                                  Y = ct["Y Value"].StringToInt32()
                                              }).ToList();

                        if (!alldiviValues.ContainsKey(info.DisplayName.GetStateAbbreviationByState().ToString()))
                            alldiviValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), divisionValues);
                    }
                    else
                    {
                        //collect division and url details
                        var divisionValues = (from ct in activedivisionItems
                                              where ct.ParentID == info.ID
                                              let url = ct.GetCrossSiteSpecification(sitecoreUrlOptions, uri).SiteSpecificUrl
                                              select new SvgDivisionData()
                                              {
                                                  N = ct.DisplayName,
                                                  h = url,
                                                  tar = url.Contains(uri.Authority),
                                                  X = ct["X Value"].StringToInt32(),
                                                  Y = ct["Y Value"].StringToInt32()
                                              }).ToList();
                        alldiviValues[info.DisplayName.GetStateAbbreviationByState().ToString()].AddRange(divisionValues);

                        var divis = alldiviValues[info.DisplayName.GetStateAbbreviationByState().ToString()].DeepClone<List<SvgDivisionData>>().OrderByDescending(o => o.tar); ;
                        var newDivis = divis.Distinct(new SvgDivisionDataComparer()).ToList();
                        alldiviValues.Remove(info.DisplayName.GetStateAbbreviationByState().ToString());
                        alldiviValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), newDivis);
                    }
                }


                svgMapData.ActiveSvgMapStateProAndAbbreviation = stValues;
                svgMapData.SvgMapCityDetails = alldiviValues;
	            return svgMapData.ToJSON();
	        }

        [WebMethod(CacheDuration = 300)]
		public string GetTexasSvgData()
		{
			var items = CompanySearch.GetCurrentCompanyStateMarketingList();

			var activeStateItems = items.Where(s => s.TemplateID == SCIDs.TemplateIds.StatePage).OrderBy(o => o.Name).ToList();
			var activedivisionItems = items.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name).ToList();

			var svgMapData = new SvgMapData();
			var stValues = new Dictionary<string, string>();
			var alldiviValues = new Dictionary<string, List<SvgDivisionData>>();
			var sitecoreUrlOptions = new UrlOptions { LanguageEmbedding = LanguageEmbedding.Never };
			Uri uri = HttpContext.Current.Request.Url;

			foreach (Item info in activeStateItems.Where(c => c.DisplayName.ToString().ToLower() == "texas"))
			{
				if (!stValues.ContainsKey(info.DisplayName.GetStateAbbreviationByState().ToString()))
				{
					//collect active state list
					stValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), info.DisplayName);

					//collect division and url details
					var divisionValues = (from ct in activedivisionItems
										  where ct.ParentID == info.ID
										  let url = ct.GetCrossSiteSpecification(sitecoreUrlOptions, uri).SiteSpecificUrl
										  select new SvgDivisionData()
										  {
											  N = ct.DisplayName,
											  h = url,
											  tar = url.Contains(uri.Authority),
											  X = ct["X Value"].StringToInt32(),
											  Y = ct["Y Value"].StringToInt32()
										  }).ToList();

					if (!alldiviValues.ContainsKey(info.DisplayName.GetStateAbbreviationByState().ToString()))
						alldiviValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), divisionValues);
				}
				else
				{

					//collect division and url details
					var divisionValues = (from ct in activedivisionItems
										  where ct.ParentID == info.ID
										  let url = ct.GetCrossSiteSpecification(sitecoreUrlOptions, uri).SiteSpecificUrl
										  select new SvgDivisionData()
										  {
											  N = ct.DisplayName,
											  h = url,
											  tar = url.Contains(uri.Authority),
											  X = ct["X Value"].StringToInt32(),
											  Y = ct["Y Value"].StringToInt32()
										  }).ToList();

					alldiviValues[info.DisplayName.GetStateAbbreviationByState().ToString()].AddRange(divisionValues);

					var divis = alldiviValues[info.DisplayName.GetStateAbbreviationByState().ToString()].DeepClone<List<SvgDivisionData>>().OrderByDescending(o => o.tar);
					var newDivis = divis.Distinct(new SvgDivisionDataComparer()).ToList();
					alldiviValues.Remove(info.DisplayName.GetStateAbbreviationByState().ToString());
					alldiviValues.Add(info.DisplayName.GetStateAbbreviationByState().ToString(), newDivis);
				}
			}

			svgMapData.ActiveSvgMapStateProAndAbbreviation = stValues;
			svgMapData.SvgMapCityDetails = alldiviValues;
			return svgMapData.ToJSON();
		}

		#endregion

		#region Inventory
		[WebMethod]
		public string GetInventorySearchResults(string sitecorePath, string sortBy, bool isRealtor, bool isClearSession)
		{
			var homeforSaleSearchResults = RefineInventorySearchResultsByFilter(sitecorePath, sortBy, isRealtor, isClearSession);
			return homeforSaleSearchResults.ToJSON();
		}

		private HomeforSaleSearchResults RefineInventorySearchResultsByFilter(string sitecorePath, string sortBy, bool isRealtor, bool isClearSession)
		{
			return RefineInventorySearchResultsByFilter(sitecorePath, sortBy, null, isRealtor, isClearSession);
		}

		private HomeforSaleSearchResults RefineInventorySearchResultsByFilter(string searchLocation, string sortBy, Dictionary<string, string> filters, bool isRealtor, bool isClearSession)
		{
			var searchHelper = new HomeForSaleSearchHelper();
			return searchHelper.HydrateHomeforSaleSearchResults(searchLocation, sortBy, filters, isRealtor, isClearSession);
		}

		[WebMethod]
		public string RefineHfSSearchResults(string isRealtor, string searchPath, string sortby, string area, string city, string stEdPrices, string stEdSqft, string schooldt, string[] beds, string[] baths, string[] garages, string[] stories, string availability, string bonusDenSolar)
		{
			var filters = GetSelectedFacetValues(searchPath, area, city, stEdPrices, stEdSqft, schooldt, string.Empty, beds, baths, garages, stories, availability, bonusDenSolar);
			var refineSearchResults = RefineInventorySearchResultsByFilter(searchPath, sortby, filters, Convert.ToBoolean(isRealtor), false);
			return refineSearchResults.ToJSON();
		}
		#endregion

		#region Search Facet

		private Dictionary<string, string> GetSelectedFacetValues(string searchPath, string area, string city, string stEdPrices, string stEdSqft, string schooldt, string comStatus, string[] beds, string[] baths, string[] garages, string[] stories, string availability, string bonusDenSolar)
		{
			string price = stEdPrices.ParseString();
			string sqfts = stEdSqft.ParseString();

			var stPrice = !string.IsNullOrWhiteSpace(price) ? price.StringList()[0] : "0";
			var edPrice = !string.IsNullOrWhiteSpace(price) ? price.StringList()[1] : "0";

			var stSqft = !string.IsNullOrWhiteSpace(sqfts) ? sqfts.StringList()[0] : "0";
			var enSqft = !string.IsNullOrWhiteSpace(sqfts) ? sqfts.StringList()[1] : "0";

			var cty = string.IsNullOrEmpty(city) ? "0" : city.Trim();
			var ara = string.IsNullOrEmpty(area) ? "0" : area.Trim();


			var searchFacets = new Dictionary<string, string>
				{
					{GlobalEnums.SearchFilterKeys.SearchUrl.ToString(), searchPath},
					{GlobalEnums.SearchFilterKeys.DivisionArea.ToString(), ara},
					{GlobalEnums.SearchFilterKeys.City.ToString(), cty},
					{GlobalEnums.SearchFilterKeys.StartingPrice.ToString(), stPrice.Trim()},
					{GlobalEnums.SearchFilterKeys.EndingPrice.ToString(), edPrice.Trim()},
					{GlobalEnums.SearchFilterKeys.StartingSqFt.ToString(), stSqft.Trim()},
					{GlobalEnums.SearchFilterKeys.EndingSqFt.ToString(), enSqft.Trim()},
					{GlobalEnums.SearchFilterKeys.SchoolDistrict.ToString(), schooldt.Trim()},
					{GlobalEnums.SearchFilterKeys.CommunityStatus.ToString(), !string.IsNullOrWhiteSpace(comStatus) ? comStatus.Trim() : string.Empty},
					{GlobalEnums.SearchFilterKeys.NumberofBedRooms.ToString(), beds.StringFromArray()},
					{GlobalEnums.SearchFilterKeys.NumberofBathRooms.ToString(), baths.StringFromArray()},
					{GlobalEnums.SearchFilterKeys.NumberofGarage.ToString(), garages.StringFromArray()},
					{GlobalEnums.SearchFilterKeys.NumberofStories.ToString(), stories.StringFromArray()},
					{GlobalEnums.SearchFilterKeys.Availability.ToString(), availability.Trim()},
					{GlobalEnums.SearchFilterKeys.BonusDenSolar.ToString(), bonusDenSolar.Trim()}
				};

			return searchFacets;
		}

		//[WebMethod]		
		//public string BindSearchFacet(string sortby, string area, string city, string stEdPrices, string stEdSqft)
		//{				
		//    string price = stEdPrices.ParseString();
		//    string sqfts = stEdSqft.ParseString();

		//    var stPrice = price.StringList()[0];
		//    var edPrice = price.StringList()[1];

		//    var stSqft = sqfts.StringList()[0];
		//    var enSqft = sqfts.StringList()[1];

		//    var cty = string.IsNullOrEmpty(city) ? "0" : city.Trim();
		//    var ara = string.IsNullOrEmpty(area) ? "0" : area.Trim();

		//    var filters = new Dictionary<string, string>();

		//    filters.Add(GlobalEnums.SearchFilterKeys.DivisionArea.ToString(), ara);
		//    filters.Add(GlobalEnums.SearchFilterKeys.City.ToString(), cty);

		//    filters.Add(GlobalEnums.SearchFilterKeys.StartingPrice.ToString(), stPrice.Trim());
		//    filters.Add(GlobalEnums.SearchFilterKeys.EndingPrice.ToString(), edPrice.Trim());

		//    filters.Add(GlobalEnums.SearchFilterKeys.StartingSqFt.ToString(), stSqft.Trim());
		//    filters.Add(GlobalEnums.SearchFilterKeys.EndingSqFt.ToString(), enSqft.Trim());

		//    var homeForSaleInfos = RefineRealtorSearchResults(filters, sortby.Trim());

		//    return homeForSaleInfos.ToJSON();
		//}

		[WebMethod]
		public string GetCitiesByDivision(string areaId)
		{
			if (areaId != "0")
			{
				var cities = CitySearch.GetCitiesByDivision(areaId);
				var cityList = cities.Select(s => new NameandId { Name = s.Name, ID = s.ID }).ToList().OrderBy(o => o.Name);
				return cityList.ToJSON();
			}
			return null;
		}
		#endregion

		#region Save Items

		[WebMethod]
		public string GetSaveItemResults(string comfav, string planfav, string hfsfav, string aoifav)
		{
			var favoritesHelper = new FavoritesHelper();
            var searchResultsbyFilter = favoritesHelper.GetAllFavoriteItems(comfav, planfav, hfsfav, aoifav);
			return searchResultsbyFilter.ToJSON();
		}

		[WebMethod]
		public string SortSaveItemResults(string comfav, string planfav, string hfsfav, string aoifav, string sortbydomain, string type)
		{
			var favoritesHelper = new FavoritesHelper();
			var searchResultsbyFilter = favoritesHelper.SortFavoriteItems(comfav, planfav, hfsfav, aoifav, sortbydomain, type);
			return searchResultsbyFilter.ToJSON();
		}

		#endregion

		#region Campain IHC LIve chat
		[WebMethod]
		public string GetLandingPageInfos(string areaId, string campaignType)
		{
			if (areaId != "0")
			{
				var landingPageInfoscs = new LandingPageInfos();
				var searchHelper = new SearchHelper();
				var favHelper = new FavoritesHelper();
				var item = SearchHelper.GetSelectedArea(areaId);

				//ihc card
				var ihcItem = searchHelper.GetIhcCardValues(item);
				if (ihcItem != null)
					landingPageInfoscs.IhcCard = searchHelper.GetIHCCardDetails(ihcItem);

				//campaign
				var realtorCamp = searchHelper.GetCampaignValues(item, campaignType);
				if (realtorCamp != null)
					landingPageInfoscs.CampaignDetails = searchHelper.GetCampaignDetails(realtorCamp);

				Uri uri = HttpContext.Current.Request.Url;
				var regInfo = string.Format("{0}://{1}/{2}", uri.Scheme, uri.Authority, searchHelper.GetStateDivisionUrlById(areaId.ToString()));

				landingPageInfoscs.LiveChat = favHelper.GetLiveChat(item[SCFieldNames.LiveChatSkillCode], areaId, regInfo);

				return landingPageInfoscs.ToJSON();
			}
			return null;
		}

		#endregion

        #region ContactUs

        [WebMethod]
        public string GetCommunitiesByDivision(string areaId)
        {
            if (areaId != "0")
            {

                areaId = "{" + areaId.Trim() + "}";
                SCID scid = new SCID(areaId);
                
                var activeCitiesIDs = new CityQueries().GetAllActiveCommunitiesUnderDivision(scid);
                return activeCitiesIDs.ToJSON();

            }
            return null;
        }

        #endregion

    }
}
