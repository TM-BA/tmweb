﻿using Sitecore.Data.Items;
using Sitecore.sitecore.admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.WebControls;

namespace TM.Web.Custom.Services
{
    public partial class GetSFForms : AdminPage
    {
        
        private Item _System;
        private Item _Templates;
        private List<Item> _mappingList;

        protected override void OnInit(EventArgs e)
        {
            CheckForUser(true); //Required!
            base.OnInit(e);
        }

        protected void CheckForUser(bool isDeveloperAllowed)
        {
            if (Context.User.Identity.IsAuthenticated)
                return;

            this.Response.Redirect(string.Format("/sitecore/login?returnUrl={0}",
                (object)HttpUtility.UrlEncode(this.Request.Url.PathAndQuery)));
        }

        private List<Item> Forms
        {
            get
            {
                var _Forms = new List<Item>();
                if (_System == null) {
                    _System = Sitecore.Context.Database.GetItem(SCIDs.TemplateIds.System);
                }

                if (_Templates == null) {
                    _Templates = Sitecore.Context.Database.GetItem(SCIDs.TemplateIds.Templates);
                }
                _Forms = _System.Axes.GetDescendants()
                    .Where(x=> x.TemplateID == SCIDs.TemplateIds.Form)
                    .ToList();

                _Forms = _Forms.Union(_Templates.Axes.GetDescendants()
                    .Where(x => x.TemplateID == SCIDs.TemplateIds.Form)
                    .ToList()).ToList();

                return _Forms;
            }
        }

        public List<Item> MappingList
        {
            get
            {
                if (_mappingList == null ||
                    _mappingList.Count() == 0)
                {
                    var mappingNode = Sitecore.Context.Database.GetItem(SCIDs.SFIntegration.Mapping);
                    _mappingList = mappingNode.Axes
                        .GetDescendants()
                        .Where(x => x.TemplateID == SCIDs.TemplateIds.MappingItem)
                        .ToList();
                }
                return _mappingList;
            }
        }

        private List<SalesForceMapping> _SFItems;
        public List<SalesForceMapping> SFItems
        {
            get
            {
                if (_SFItems == null 
                    || _SFItems.Count() == 0) {
                        _SFItems = MappingList.Select(x => new SalesForceMapping()
                        {
                            DisplayName = x.DisplayName,
                            Id = x[SCFieldNames.SFIntegration.Mapping.FieldId] ?? null,
                            Name = x[SCFieldNames.SFIntegration.Mapping.FieldName] ?? null,
                            Mapps = String.IsNullOrEmpty(x[SCFieldNames.SFIntegration.Mapping.WFFMFields])
                                ? null : x[SCFieldNames.SFIntegration.Mapping.WFFMFields]
                                    .Split('|')
                                    .Select(y => new ItemMapping()
                                    {
                                        Id = y
                                    }).ToList()
                        }).ToList();
                }


                return _SFItems;
            }
        }

        public SalesForceInformation GetSFInfo(Item field)
        {
            var info = new SalesForceInformation();

            SFItems.ForEach(x => {
                x.Mapps.ForEach(y => { 
                    if (field.ID.ToString() == y.Id) {
                        info.SFDisplayName = x.DisplayName;
                        info.SFID = x.Id;
                        info.SFName = x.Name;
                    }
                });
            });
            return info;
        }

        public string GetSFID(Item field)
        {
            return null;
        }
        public List<TM.Web.Custom.WebControls.Fields> GetFIelds(Item form)
        {
            var fields = form
                .Axes.GetDescendants()
                .Where(x=> x.TemplateID == SCIDs.TemplateIds.FIeld);

            return fields.Select(x => new TM.Web.Custom.WebControls.Fields()
            { 
                Name = x.Name,
                Id = x.ID.ToString(),
                SFInfo = GetSFInfo(x)
            }).ToList();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var se = Forms.Select(z => new Form()
            {
                Name = z.DisplayName,
                ID = z.ID.ToString(),
                Path = z.Paths.ContentPath,
                Fields = GetFIelds(z)
                //Fields = 
            }).ToList();

            FormsRepeater.DataSource = se;
            FormsRepeater.DataBind();
        }

        protected void FormsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var source = (Form)args.Item.DataItem;
            var childRepeater = (Repeater)args.Item.FindControl("FormFields");
                childRepeater.DataSource = source.Fields;
                childRepeater.DataBind();
        }

        protected void FieldsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var source = (Form)((RepeaterItem)((Repeater)args.Item.Parent).NamingContainer).DataItem;
            var form = (Label)args.Item.FindControl("FormID");
            var path = (Label)args.Item.FindControl("Path");

            form.Text = source.ID;
            path.Text = source.Path;
        } 
    }
}