﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.sitecore.admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using TM.Utils.Extensions;
using System.Web.UI;
using System.Web.UI.WebControls;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Queries;
using TM.Web.Custom.Search.LuceneQueries;

namespace TM.Web.Custom.Services
{
    public partial class ExportImagesCSV : AdminPage
    {
        private List<string> _Columns;
        public List<string> Columns
        {
            get
            {
                if (_Columns == null)
                {
                    _Columns = new List<string>()
                    {
                        "ID", 
                        "Name",
                        "Sitecore Path",
                        "Image URL",
                        "Extension",
                        "Alt"
                    };
                }

                return _Columns;
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        protected override void OnInit(EventArgs e)
        {
            CheckForUser(true); //Required!
            base.OnInit(e);
        }
        protected void CheckForUser(bool isDeveloperAllowed)
        {
            if (Context.User.Identity.IsAuthenticated)
                return;

            this.Response.Redirect(string.Format("/Sitecore/login?returnUrl={0}",
                (object)HttpUtility.UrlEncode(this.Request.Url.PathAndQuery)));
        }

        protected void Export_Click(object sender, EventArgs e)
        {
            var builder = new StringBuilder();
            try
            {
                var images = new ImageQueries()
                .GetImagesByTemplates(GetImageItems());

                var columnNames = new List<string>();
                var rows = new List<string>();

                Columns.ForEach(column =>
                {
                    columnNames.Add(column);
                });

                builder
                    .Append(string
                        .Join(",", columnNames.ToArray()))
                    .Append("\n");

                images.ForEach(image =>
                {
                    var currentRow = new List<string>();

                    currentRow.Add(image.ID
                        .ToString()
                        .ParseForCSV());
                    currentRow.Add(image
                        .Name
                        .ParseForCSV());
                    currentRow.Add(image.Paths
                        .ContentPath
                        .ParseForCSV());
                    currentRow.Add(GetImageUrl(image)
                        .ParseForCSV());
                    currentRow.Add(image.Fields[SCFieldNames.Images.Extension] != null ?
                        image.Fields[SCFieldNames.Images.Extension]
                            .Value.ParseForCSV() : "");
                    currentRow.Add(image.Fields[SCFieldNames.Images.Alt] != null ?
                        image.Fields[SCFieldNames.Images.Alt]
                            .Value.ParseForCSV() : "");

                    rows.Add(string.Join(",", currentRow.ToArray()));
                });

                builder
                    .Append(string.Join("\n", rows.ToArray()));


            }
            catch (Exception ex)
            {
                builder.Append(String.
                    Format("There was an error while generating the csv.\n Inner: {0}\n Message: {1}",
                        ex.InnerException,
                        ex.Message));
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment;filename=images.csv");
            Response.Write(builder.ToString());
            Response.End();
        }

        private string GetImageUrl(Item currentItem)
        {
            var imageUrl = string.Empty;
            currentItem.Fields.ReadAll();
            var mediaItem = new MediaItem(currentItem);

            if (mediaItem != null)
            {
                imageUrl = Request.Url.Host + "" + StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(mediaItem));
            }

            return imageUrl;
        }

        private List<ID> GetImageItems()
        {
            var IdsArray = new List<ID>();
            var result = Sitecore.
                 Context.
                 Database.
                 SelectItems(SCFastQueries.AllTemplatesIdsForImages());

            var ids = result.Select(x => new
            {
                x.ID
            }).ToList();

            ids.ForEach(id =>
            {
                IdsArray.Add(id.ID);
            });

            IdsArray.Add(SCIDs.Images.Unversioned);
            IdsArray.Add(SCIDs.Images.Versioned);

            return IdsArray;
        }
    }
}