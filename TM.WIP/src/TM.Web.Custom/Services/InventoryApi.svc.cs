﻿using Sitecore.ContentSearch.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SitecoreContentDomain;
using TM.Web.Custom.Search.LuceneQueries;
using Sitecore.Links;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using TM.Web.Custom.WebControls;
using System.Globalization;

namespace TM.Web.Custom.Services
{

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    [ServiceContract]
    public class InventoryApi
    {
        private static string _activeStatus = IdHelper.NormalizeGuid(SCIDs.StatusIds.Status.Active, true);

        #region API_V1
        #region GetDivisions
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v1/GetDivisions?divisionId={divisionId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiDivision> GetDivisions(String divisionId)
        {
            List<Item> divisionsResults = null;

            try
            {
                divisionsResults = new DivisionQueries().GetDivisions(divisionId, Sitecore.Context.Site.ContentStartPath);

                return divisionsResults
               .Where(r => r != null)
               .Select(r => new InventoryApiDivision
               {
                   Id = r[SCFieldNames.DivisionFields.LegacyDivisionID],
                   Active = (r[SCFieldNames.DivisionFields.StatusForSearch] == SCIDs.StatusIds.Status.Active) ? 1 : 0,
                   Name = r[SCFieldNames.DivisionFields.DivisionName],
                   Url = GetUrl(r)
               }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetDivisions, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (divisionsResults != null && divisionsResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }
        }
        #endregion

        #region GetCommunities
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v1/GetCommunities?divisionId={divisionId}&communityID={communityID}&communityStatus={communityStatus}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiCommunity> GetCommunities(string divisionID, string communityID, string communityStatus)
        {
            List<Item> communityResults = null;

            try
            {
                communityResults = new CommunityQueries().GetCommunities(divisionID, communityID, communityStatus, Sitecore.Context.Site.ContentStartPath);

                if (communityResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }

                List<InventoryApiCommunity> list = communityResults
                    .Where(r => r != null)
                    .Select(r => new
                    {
                        Item = r,
                        DivisionParent = GetParentItem(r, SCIDs.TemplateIds.DivisionPage),
                    })
                    .Select(r => new InventoryApiCommunity
                    {
                        Id = r.Item[SCFieldNames.CommunityFields.CommunityLegacyId],
                        URL = GetUrl(r.Item),
                        Name = r.Item[SCFieldNames.CommunityFields.CommunityName],
                        Status = GetCommunityStatus(r.Item),
                        Description = r.Item[SCFieldNames.CommunityFields.Description],
                        Logo = GetLogoURL(r.Item),
                        IsTownsend = 0,
                        Directions = r.Item[SCFieldNames.CommunityFields.Directions],
                        Latitude = r.Item[SCFieldNames.CommunityFields.Latitude],
                        Longitude = r.Item[SCFieldNames.CommunityFields.Longitude],
                        Address = r.Item[SCFieldNames.CommunityFields.StreetAddress1],
                        Zip = r.Item[SCFieldNames.CommunityFields.Zip],
                        Phone = r.Item[SCFieldNames.CommunityFields.Phone],
                        SalesTeam = GetSalesTeam(r.Item),
                        Images = GetCommunityImages(r.Item),
                        SquareFeet = r.Item[SCFieldNames.CommunityFields.SquareFeets],
                        Beds = r.Item[SCFieldNames.CommunityFields.Beds],
                        Baths = r.Item[SCFieldNames.CommunityFields.Baths],
                        HalfBaths = r.Item[SCFieldNames.CommunityFields.HalfBaths],
                        Stories = r.Item[SCFieldNames.CommunityFields.Stories],
                        Garage = r.Item[SCFieldNames.CommunityFields.Garage],
                        UseOfficeHoursText = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "1") ? 1 : 0,
                        OfficeHoursText = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "1") ? r.Item[SCFieldNames.CommunityFields.OfficeHoursText] : "",
                        SundayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Sunday, r.Item) : "",
                        MondayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Monday, r.Item) : "",
                        TuesdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Tuesday, r.Item) : "",
                        WednesdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Wednesday, r.Item) : "",
                        ThursdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Thursday, r.Item) : "",
                        FridayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Friday, r.Item) : "",
                        SaturdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Saturday, r.Item) : "",
                        SchoolDistrict = (!string.IsNullOrEmpty(r.Item[SCFieldNames.CommunityFields.SchoolDistrict])) ? Sitecore.Context.Database.GetItem(new ID(r.Item[SCFieldNames.CommunityFields.SchoolDistrict])).Name : null,
                        Schools = GetSchools(r.Item),
                        IsActiveAdult = (r.Item[SCFieldNames.CommunityFields.IsActiveAdult] == "1") ? 1 : 0,
                        IsMasterPlanned = (r.Item[SCFieldNames.CommunityFields.IsMasterPlanned] == "1") ? 1 : 0,
                        IsGated = (r.Item[SCFieldNames.CommunityFields.IsGated] == "1") ? 1 : 0,
                        IsCondoOnly = (r.Item[SCFieldNames.CommunityFields.IsCondoOnly] == "1") ? 1 : 0,
                        HasPool = (r.Item[SCFieldNames.CommunityFields.HasPool] == "1") ? 1 : 0,
                        HasPlayground = (r.Item[SCFieldNames.CommunityFields.HasPlayground] == "1") ? 1 : 0,
                        HasGolfCourse = (r.Item[SCFieldNames.CommunityFields.HasGolfCourse] == "1") ? 1 : 0,
                        HasTennis = (r.Item[SCFieldNames.CommunityFields.HasTennis] == "1") ? 1 : 0,
                        HasSoccer = (r.Item[SCFieldNames.CommunityFields.HasSoccer] == "1") ? 1 : 0,
                        HasVolleyball = (r.Item[SCFieldNames.CommunityFields.HasVolleyball] == "1") ? 1 : 0,
                        HasBasketball = (r.Item[SCFieldNames.CommunityFields.HasBasketball] == "1") ? 1 : 0,
                        HasBaseball = (r.Item[SCFieldNames.CommunityFields.HasBaseball] == "1") ? 1 : 0,
                        HasView = (r.Item[SCFieldNames.CommunityFields.HasView] == "1") ? 1 : 0,
                        HasLake = (r.Item[SCFieldNames.CommunityFields.HasLake] == "1") ? 1 : 0,
                        HasPond = (r.Item[SCFieldNames.CommunityFields.HasPond] == "1") ? 1 : 0,
                        HasMarina = (r.Item[SCFieldNames.CommunityFields.HasMarina] == "1") ? 1 : 0,
                        HasBeach = (r.Item[SCFieldNames.CommunityFields.HasBeach] == "1") ? 1 : 0,
                        IsWaterfront = (r.Item[SCFieldNames.CommunityFields.IsWaterfront] == "1") ? 1 : 0,
                        HasPark = (r.Item[SCFieldNames.CommunityFields.HasPark] == "1") ? 1 : 0,
                        HasTrails = (r.Item[SCFieldNames.CommunityFields.HasTrails] == "1") ? 1 : 0,
                        HasGreenbelt = (r.Item[SCFieldNames.CommunityFields.HasGreenbelt] == "1") ? 1 : 0,
                        HasClubhouse = (r.Item[SCFieldNames.CommunityFields.HasClubhouse] == "1") ? 1 : 0,
                        DivisionID = r.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID],
                        DivisionName = r.DivisionParent[SCFieldNames.DivisionFields.DivisionName],
                        DivisionUrl = GetUrl(r.DivisionParent)
                    }
                    ).ToList();

                return list;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetCommunities, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (communityResults != null && communityResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #region GetPlans
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/v1/GetPlans?divisionId={divisionId}&communityID={communityID}&planId={planId}&status={status}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiPlan> GetPlans(string divisionID, string communityID, string planID, string status)
        {
            List<Item> planResults = null;

            try
            {
                planResults = new PlanQueries().GetPlans(divisionID, communityID, planID, status, Sitecore.Context.Site.ContentStartPath);

                return planResults
                   .Where(r => r != null)
                   .Select(r => new
                   {
                       Item = r,
                       DivisionParent = GetParentItem(r, SCIDs.TemplateIds.DivisionPage),
                       CommunityParent = GetParentItem(r, SCIDs.TemplateIds.CommunityPage),
                       MasterPlan = GetMasterPlan(r)
                   })
                   .Select(r => new InventoryApiPlan
                   {
                       Id = r.Item[SCFieldNames.PlanBaseFields.PlanLegacyID],
                       URL = GetUrl(r.Item),
                       Name = r.Item[SCFieldNames.PlanBaseFields.PlanName],
                       Status = (r.Item[SCFieldNames.PlanBaseFields.PlanStatus] == SCIDs.StatusIds.Status.Active) ? "Active" : "Inactive",
                       Description = r.Item[SCFieldNames.PlanBaseFields.Description],
                       Images = GetAPIImages(r.Item, SCIDs.TemplateIds.PlanPage.ToString()),
                       Ifp = r.Item[SCFieldNames.PlanBaseFields.IFP],
                       Price = r.Item[SCFieldNames.PlanBaseFields.PricedFromValue],
                       Sqft = r.Item[SCFieldNames.PlanBaseFields.Sqft],
                       Beds = r.Item[SCFieldNames.PlanBaseFields.Bedrooms],
                       Baths = r.Item[SCFieldNames.PlanBaseFields.Bathrooms],
                       HalfBaths = r.Item[SCFieldNames.PlanBaseFields.HalfBathrooms],
                       Stories = r.Item[SCFieldNames.PlanBaseFields.Stories],
                       Garage = r.Item[SCFieldNames.PlanBaseFields.Garages],
                       HasLivingRoom = r.Item[SCFieldNames.PlanBaseFields.HasLivingRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasLivingRoom] : "0",
                       HasDiningRoom = r.Item[SCFieldNames.PlanBaseFields.HasDiningRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasDiningRoom] : "0",
                       HasSunRoom = r.Item[SCFieldNames.PlanBaseFields.HasSunRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasSunRoom] : "0",
                       HasStudy = r.Item[SCFieldNames.PlanBaseFields.HasStudy] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasStudy] : "0",
                       HasLoft = r.Item[SCFieldNames.PlanBaseFields.HasLoft] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasLoft] : "0",
                       HasOffice = r.Item[SCFieldNames.PlanBaseFields.HasOffice] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasOffice] : "0",
                       HasGameRoom = r.Item[SCFieldNames.PlanBaseFields.HasGameRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasGameRoom] : "0",
                       HasMediaRoom = r.Item[SCFieldNames.PlanBaseFields.HasMediaRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasMediaRoom] : "0",
                       HasGuestBedroom = r.Item[SCFieldNames.PlanBaseFields.HasGuestBedroom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasGuestBedroom] : "0",
                       HasBonusRoom = r.Item[SCFieldNames.PlanBaseFields.HasBonusRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasBonusRoom] : "0",
                       HasBasement = r.Item[SCFieldNames.PlanBaseFields.HasBasement] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasBasement] : "0",
                       HasFireplace = r.Item[SCFieldNames.PlanBaseFields.HasFireplace] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasFireplace] : "0",
                       HasMasterBedroom = r.Item[SCFieldNames.PlanBaseFields.HasMasterBedroom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasMasterBedroom] : "0",
                       HasFamilyRoom = r.Item[SCFieldNames.PlanBaseFields.HasFamilyRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasFamilyRoom] : "0",

                       CommunityId = (r.CommunityParent != null) ? r.CommunityParent[SCFieldNames.CommunityFields.CommunityLegacyId] : "",
                       CommunityUrl = (r.CommunityParent != null) ? GetUrl(r.CommunityParent) : "",
                       CommunityName = (r.CommunityParent != null) ? r.CommunityParent[SCFieldNames.CommunityFields.CommunityName] : "",

                       DivisionID = (r.DivisionParent != null) ? r.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID] : "",
                       DivisionName = (r.DivisionParent != null) ? r.DivisionParent[SCFieldNames.DivisionFields.DivisionName] : "",
                       DivisionUrl = (r.DivisionParent != null) ? GetUrl(r.DivisionParent) : "",

                       PlanWidth = (r.MasterPlan != null) ? r.MasterPlan[SCFieldNames.MasterPlan.PlanWidth] : "",
                       PlanDepth = (r.MasterPlan != null) ? r.MasterPlan[SCFieldNames.MasterPlan.PlanDepth] : "",
                       PlanType = (r.MasterPlan != null) ? GetMasterPlanType(r.MasterPlan) : ""


                   }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetPlans, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (planResults != null && planResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #region GetSpecs
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v1/GetSpecs?divisionId={divisionId}&communityID={communityID}&planId={planId}&specId={specId}&status={status}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiHome> GetSpecs(string DivisionID, string CommunityID, string PlanId, string SpecId, string status)
        {
            List<Item> specResults = null;

            try
            {
                specResults = new HomeQueries().GetHomesForSale(DivisionID, CommunityID, PlanId, SpecId, status, Sitecore.Context.Site.ContentStartPath);

                return specResults.Where(s => s != null)
                    .Select(s => new
                    {
                        Item = s,
                        DivisionParent = GetParentItem(s, SCIDs.TemplateIds.DivisionPage),
                        CommunityParent = GetParentItem(s, SCIDs.TemplateIds.CommunityPage),
                        PlanParent = GetParentItem(s, SCIDs.TemplateIds.PlanPage)
                    })
                   .Select(s => new InventoryApiHome
                   {
                       Id = s.Item[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyID],
                       URL = GetUrl(s.Item),
                       Address = s.Item[SCFieldNames.HomeForSaleFields.StreetAddress1],
                       Status = (s.Item[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == SCIDs.StatusIds.Status.Active) ? "Active" : "Inactive",
                       Description = s.Item[SCFieldNames.HomeForSaleFields.Description],
                       Availability = getSpecAvailability(s.Item),
                       AvailabilityDate = GetDate(s.Item),
                       Images = GetAPIImages(s.Item, SCIDs.TemplateIds.HomeForSalePage.ToString()),
                       Ifp = GetIfpUrl(s.Item),
                       Price = s.Item[SCFieldNames.HomeForSaleFields.Price],
                       Lot = s.Item[SCFieldNames.HomeForSaleFields.LotNumber],
                       Sqft = s.Item[SCFieldNames.HomeForSaleFields.Sqft],
                       Beds = s.Item[SCFieldNames.HomeForSaleFields.Beds],
                       Baths = s.Item[SCFieldNames.HomeForSaleFields.Bathrooms],
                       HalfBaths = s.Item[SCFieldNames.HomeForSaleFields.HalfBathrooms],
                       Stories = s.Item[SCFieldNames.HomeForSaleFields.Stories],
                       Garage = s.Item[SCFieldNames.HomeForSaleFields.Garages],
                       HasLivingRoom = s.Item[SCFieldNames.HomeForSaleFields.HasLivingRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasLivingRoom] : "0",
                       HasDiningRoom = s.Item[SCFieldNames.HomeForSaleFields.HasDiningRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasDiningRoom] : "0",
                       HasSunRoom = s.Item[SCFieldNames.HomeForSaleFields.HasSunRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasSunRoom] : "0",
                       HasStudy = s.Item[SCFieldNames.HomeForSaleFields.HasStudy] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasStudy] : "0",
                       HasLoft = s.Item[SCFieldNames.HomeForSaleFields.HasLoft] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasLoft] : "0",
                       HasOffice = s.Item[SCFieldNames.HomeForSaleFields.HasOffice] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasOffice] : "0",
                       HasGameRoom = s.Item[SCFieldNames.HomeForSaleFields.HasGameRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasGameRoom] : "0",
                       HasMediaRoom = s.Item[SCFieldNames.HomeForSaleFields.HasMediaRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasMediaRoom] : "0",
                       HasGuestBedroom = s.Item[SCFieldNames.HomeForSaleFields.HasGuestBedroom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasGuestBedroom] : "0",
                       HasBonusRoom = s.Item[SCFieldNames.HomeForSaleFields.HasBonusRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasBonusRoom] : "0",
                       HasBasement = s.Item[SCFieldNames.HomeForSaleFields.HasBasement] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasBasement] : "0",
                       HasFireplace = s.Item[SCFieldNames.HomeForSaleFields.HasFireplace] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasFireplace] : "0",
                       HasMasterBedroom = s.Item[SCFieldNames.HomeForSaleFields.HasMasterBedroom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasMasterBedroom] : "0",
                       HasFamilyRoom = s.Item[SCFieldNames.HomeForSaleFields.HasFamilyRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasFamilyRoom] : "0",
                       CommunityId = s.CommunityParent[SCFieldNames.CommunityFields.CommunityLegacyId],
                       CommunityUrl = GetUrl(s.CommunityParent),
                       CommunityName = s.CommunityParent[SCFieldNames.CommunityFields.CommunityName],
                       DivisionID = s.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID],
                       DivisionName = s.DivisionParent[SCFieldNames.DivisionFields.DivisionName],
                       DivisionUrl = GetUrl(s.DivisionParent),
                       PlanId = s.PlanParent[SCFieldNames.PlanBaseFields.PlanLegacyID],
                       PlanUrl = GetUrl(s.PlanParent),
                       PlanName = s.PlanParent[SCFieldNames.PlanBaseFields.PlanName]

                   }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetSpecs, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (specResults != null && specResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #region Private Methods

        private string GetUrl(Item r)
        {
            var urlOptions = LinkManager.GetDefaultUrlOptions();
            urlOptions.AlwaysIncludeServerUrl = true;
            return LinkManager.GetItemUrl(r, urlOptions);
        }

        private string GetLogoURL(Item item)
        {
            var mediaField = (ImageField)item.Fields[SCFieldNames.CommunityFields.CommunityLogo];

            return GetMediaUrlInventoryApi(mediaField.MediaItem);
        }

        private string GetIfpUrl(Item item)
        {
            var mediaField = item.Fields[SCFieldNames.HomeForSaleFields.Ifp];
            //mediaField.Ur
            return null; // GetMediaUrlInventoryApi(mediaField.MediaItem);
        }


        private string GetWorkingHours(DayOfWeek dayOfWeek, Item item)
        {
            int dayValue = (int)dayOfWeek;
            string description = string.Empty;

            string dayClose = null;
            string dayOpen = null;
            string day = null;
            try
            {
                switch (dayValue)
                {
                    case 0:
                        day = "Sunday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.SundayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.SundayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.SundayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.SundayOpen])).Name : null;
                        break;
                    case 1:
                        day = "Monday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.MondayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.MondayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.MondayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.MondayOpen])).Name : null;
                        break;
                    case 2:
                        day = "Tuesday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.TuesdayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.TuesdayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.TuesdayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.TuesdayOpen])).Name : null;
                        break;
                    case 3:
                        day = "Wednesday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.WednesdayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.WednesdayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.WednesdayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.WednesdayOpen])).Name : null;
                        break;
                    case 4:
                        day = "Thursday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.ThursdayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.ThursdayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.ThursdayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.ThursdayOpen])).Name : null;
                        break;
                    case 5:
                        day = "Friday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.FridayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.FridayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.FridayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.FridayOpen])).Name : null;
                        break;
                    case 6:
                        day = "Saturday";
                        dayClose = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.SaturdayClose])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.SaturdayClose])).Name : null;
                        dayOpen = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.SaturdayOpen])) ?
                            Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.SaturdayOpen])).Name : null;
                        break;
                    default:
                        return null;
                }
            }
            catch (FormatException fe)
            {
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
            if (!string.IsNullOrEmpty(dayOpen) && !string.IsNullOrEmpty(dayClose))
                description = day + ": " + dayOpen + " to " + dayClose;

            return description;

        }

        private string GetCommunityStatus(Item item)
        {

            string status = (!string.IsNullOrEmpty(item[SCFieldNames.CommunityFields.CommunityStatus])) ?
                Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.CommunityFields.CommunityStatus])).Name : null;

            return status;
        }

        private string getSpecAvailability(Item item)
        {
            return (!string.IsNullOrEmpty(item[SCFieldNames.HomeForSaleFields.StatusForAvailability])) ?
                Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.HomeForSaleFields.StatusForAvailability])).Name : null;
        }

        private School[] GetSchools(Item item)
        {
            var schools = item.Axes.GetDescendants().Where(s => s.TemplateID == SCIDs.TemplateIds.School);

            return schools
                .Select(s => new School
                {
                    Name = s[SCFieldNames.School.Name],
                    Type = (!string.IsNullOrEmpty(s[SCFieldNames.School.Type])) ? Sitecore.Context.Database.GetItem(new ID(s[SCFieldNames.School.Type])).Name : null,
                }

                  ).ToArray();
        }

        private Item GetParentItem(Item item, ID templateID)
        {
            Item i = item.Axes.GetAncestors().Where(a => a.TemplateID == templateID).FirstOrDefault();

            return i;
        }

        private InventoryApiImage[] GetCommunityImages(Item item)
        {
            var ids = item[SCFieldNames.CommunityFields.SlideShowImage].Split('|');

            return ids.Where(i => i != null && i != string.Empty)
                   .Select(i => new InventoryApiImage
                   {
                       URL = GetMediaUrlInventoryApi(item.Database.GetItem(i)),
                       Type = "SLD"
                   }).ToArray();
        }

        private InventoryApiImage[] GetAPIImages(Item item, string templateId)
        {
            string[] idsELE = null;
            string[] idsINT = null;
            string[] idsFLP = null;

            if (templateId == SCIDs.TemplateIds.PlanPage.ToString())
            {
                idsELE = item[SCFieldNames.PlanBaseFields.ElevationImages].Split('|');
                idsINT = item[SCFieldNames.PlanBaseFields.InteriorImages].Split('|');
                idsFLP = item[SCFieldNames.PlanBaseFields.FloorPlanImages].Split('|');
            }

            if (templateId == SCIDs.TemplateIds.HomeForSalePage.ToString())
            {
                idsELE = item[SCFieldNames.HomeForSaleFields.ElevationImages].Split('|');
                idsINT = item[SCFieldNames.HomeForSaleFields.InteriorImages].Split('|');
                idsFLP = item[SCFieldNames.HomeForSaleFields.FloorPlanImages].Split('|');
            }

            var imagesList = new List<InventoryApiImage>();

            imagesList.AddRange(idsELE
                                    .Where(i => i != null && i != string.Empty)
                                    .Select(i => new InventoryApiImage
                                    {
                                        URL = GetMediaUrlInventoryApi(item.Database.GetItem(i)),
                                        Type = "ELE"
                                    }).ToList());

            imagesList.AddRange(idsINT
                                    .Where(i => i != null && i != string.Empty)
                                    .Select(i => new InventoryApiImage
                                    {
                                        URL = GetMediaUrlInventoryApi(item.Database.GetItem(i)),
                                        Type = "INT"
                                    }).ToList());

            imagesList.AddRange(idsFLP
                                .Where(i => i != null && i != string.Empty)
                                .Select(i => new InventoryApiImage
                                {
                                    URL = GetMediaUrlInventoryApi(item.Database.GetItem(i)),
                                    Type = "FLP"
                                }).ToList());

            return imagesList.ToArray();
        }

        private SalesTeam[] GetSalesTeam(Item item)
        {

            if (item[SCFieldNames.CommunityFields.SalesTeamCard] == null)
                return null;

            var ids = item[SCFieldNames.CommunityFields.SalesTeamCard].Split('|');

            return ids
                .Select(s => new
                {
                    Item = s,
                    salesPerson = item.Database.GetItem(s)
                })
            .Where(s => s.salesPerson != null)
            .Select(s => new SalesTeam
            {
                Name = s.salesPerson[SCFieldNames.SalesTeam.Name],
                Phone = s.salesPerson[SCFieldNames.SalesTeam.Phone]
            }).ToArray();
        }

        private string GetMediaUrlInventoryApi(Item item)
        {
            if (item != null)
            {
                MediaUrlOptions muo = new MediaUrlOptions();
                muo.AlwaysIncludeServerUrl = true;
                var src = MediaManager.GetMediaUrl(item, muo);
                return src;
            }

            return string.Empty;
        }

        private string GetDate(Item item)
        {
            string txtAvailabilityDate = null;

            string dateTime = item[SCFieldNames.HomeForSaleFields.AvailabilityDate].ToString();

            if (!string.IsNullOrEmpty(dateTime) && dateTime.Length > 7)
                txtAvailabilityDate = dateTime.Substring(4, 2) + "/" + dateTime.Substring(6, 2) + "/" +
                                           dateTime.Substring(0, 4);
            return txtAvailabilityDate;
        }

        private Item GetMasterPlan(Item item)
        {
            Item masterPlan = null;
            if (item == null
                || string.IsNullOrEmpty(item[SCFieldNames.PlanPageFields.MasterPlanID]))
            {
                return null;
            }
            masterPlan = Sitecore.Context.Database.GetItem(new ID(item[SCFieldNames.PlanPageFields.MasterPlanID]));
            return masterPlan;
        }

        private Item GetVirtualMasterPlan(Item item)
        {
            Item masterPlan = null;
            var itemTour = item[SCFieldNames.MasterPlan.VirtualTourId];
            if (String.IsNullOrEmpty(itemTour))
            {
                return null;
            }
            masterPlan = Sitecore.Context.Database.GetItem(new ID(itemTour));
            return masterPlan;
        }

        private string GetMasterPlanType(Item r)
        {
            string planType = "";
            Item i = Sitecore.Context.Database.GetItem(new ID(r[SCFieldNames.MasterPlan.ProductDisplayType]));
            planType = i.Name;
            return planType;
        }


        #endregion
        #endregion

        #region API_V2
        #region GetDivisions
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v2/GetDivisions?divisionId={divisionId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiDivisionV2> GetDivisionsV2(String divisionId)
        {
            List<Item> divisionsResults = null;

            try
            {
                divisionsResults = new DivisionQueries().GetDivisions(divisionId, Sitecore.Context.Site.ContentStartPath);

                return divisionsResults
               .Where(r => r != null)
               .Select(r => new
               {
                   Item = r,
                   State = GetParentItem(r, SCIDs.TemplateIds.StatePage)
               })
               .Select(r => new InventoryApiDivisionV2
               {
                   Id = GetFieldOrEmpty(r.Item.Fields[SCFieldNames.DivisionFields.LegacyDivisionID]),
                   Active = (r.Item[SCFieldNames.DivisionFields.StatusForSearch] == SCIDs.StatusIds.Status.Active) ? 1 : 0,
                   Name = GetFieldOrEmpty(r.Item.Fields[SCFieldNames.DivisionFields.DivisionName]),
                   NSEOunit = GetFieldOrEmpty(r.Item.Fields[SCFieldNames.DivisionFields.NSEOunit]),
                   Url = GetUrl(r.Item),
                   SalesforceStateValue = GetSFState(r.Item),
                   IHCCard = GetIHCCardList(r.Item, r.State),
                   PrintDisclaimer = GetDisclaimerText(r.Item, true),
                   WebDisclaimer = GetDisclaimerText(r.Item)

               }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetDivisions, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (divisionsResults != null && divisionsResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }
        }
        #endregion

        #region GetPlans
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "/v2/GetPlans?divisionId={divisionId}&communityID={communityID}&planId={planId}&status={status}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiPlanV2> GetPlansV2(string divisionID, string communityID, string planID, string status)
        {
            List<Item> planResults = null;

            try
            {
                planResults = new PlanQueries().GetPlans(divisionID, communityID, planID, status, Sitecore.Context.Site.ContentStartPath);

                return planResults
                   .Where(r => r != null)
                   .Select(r => new
                   {
                       Item = r,
                       DivisionParent = GetParentItem(r, SCIDs.TemplateIds.DivisionPage),
                       CommunityParent = GetParentItem(r, SCIDs.TemplateIds.CommunityPage),
                       MasterPlan = GetMasterPlan(r),
                       State = GetParentItem(r, SCIDs.TemplateIds.StatePage)

                   })
                   .Select(r => new InventoryApiPlanV2
                   {
                       Id = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.PlanLegacyID]),
                       URL = GetTextOrEmpty(GetUrl(r.Item)),
                       Name = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.PlanName]),
                       Status = (r.Item[SCFieldNames.PlanBaseFields.PlanStatus] == SCIDs.StatusIds.Status.Active) ? "Active" : "Inactive",
                       Description = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Description]),
                       Images = GetAPIImages(r.Item, SCIDs.TemplateIds.PlanPage.ToString()),
                       Ifp = GetFieldURL(r.Item.Fields[SCFieldNames.PlanBaseFields.IFP]),
                       Price = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.PricedFromValue]),
                       Sqft = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Sqft]),
                       Beds = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Bedrooms]),
                       Baths = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Bathrooms]),
                       HalfBaths = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.HalfBathrooms]),
                       Stories = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Stories]),
                       Garage = GetTextOrEmpty(r.Item[SCFieldNames.PlanBaseFields.Garages]),
                       HasLivingRoom = r.Item[SCFieldNames.PlanBaseFields.HasLivingRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasLivingRoom] : "0",
                       HasDiningRoom = r.Item[SCFieldNames.PlanBaseFields.HasDiningRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasDiningRoom] : "0",
                       HasSunRoom = r.Item[SCFieldNames.PlanBaseFields.HasSunRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasSunRoom] : "0",
                       HasStudy = r.Item[SCFieldNames.PlanBaseFields.HasStudy] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasStudy] : "0",
                       HasLoft = r.Item[SCFieldNames.PlanBaseFields.HasLoft] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasLoft] : "0",
                       HasOffice = r.Item[SCFieldNames.PlanBaseFields.HasOffice] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasOffice] : "0",
                       HasGameRoom = r.Item[SCFieldNames.PlanBaseFields.HasGameRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasGameRoom] : "0",
                       HasMediaRoom = r.Item[SCFieldNames.PlanBaseFields.HasMediaRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasMediaRoom] : "0",
                       HasGuestBedroom = r.Item[SCFieldNames.PlanBaseFields.HasGuestBedroom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasGuestBedroom] : "0",
                       HasBonusRoom = r.Item[SCFieldNames.PlanBaseFields.HasBonusRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasBonusRoom] : "0",
                       HasBasement = r.Item[SCFieldNames.PlanBaseFields.HasBasement] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasBasement] : "0",
                       HasFireplace = r.Item[SCFieldNames.PlanBaseFields.HasFireplace] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasFireplace] : "0",
                       HasMasterBedroom = r.Item[SCFieldNames.PlanBaseFields.HasMasterBedroom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasMasterBedroom] : "0",
                       HasFamilyRoom = r.Item[SCFieldNames.PlanBaseFields.HasFamilyRoom] != string.Empty ? r.Item[SCFieldNames.PlanBaseFields.HasFamilyRoom] : "0",

                       CommunityId = (r.CommunityParent != null) ? r.CommunityParent[SCFieldNames.CommunityFields.CommunityLegacyId] : "",
                       CommunityUrl = (r.CommunityParent != null) ? GetUrl(r.CommunityParent) : "",
                       CommunityName = (r.CommunityParent != null) ? r.CommunityParent[SCFieldNames.CommunityFields.CommunityName] : "",

                       DivisionID = (r.DivisionParent != null) ? r.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID] : "",
                       DivisionName = (r.DivisionParent != null) ? r.DivisionParent[SCFieldNames.DivisionFields.DivisionName] : "",
                       DivisionUrl = (r.DivisionParent != null) ? GetUrl(r.DivisionParent) : "",

                       PlanWidth = (r.MasterPlan != null) ? r.MasterPlan[SCFieldNames.MasterPlan.PlanWidth] : "",
                       PlanDepth = (r.MasterPlan != null) ? r.MasterPlan[SCFieldNames.MasterPlan.PlanDepth] : "",
                       PlanType = (r.MasterPlan != null) ? GetMasterPlanType(r.MasterPlan) : "",
                       VirtualTourURL = GetVirtualTour(r.Item),
                       CommunityPhone = GetTextOrEmpty(r.CommunityParent[SCFieldNames.CommunityFields.Phone]),
                       IHCCard = GetIHCCardList(r.Item, r.State)
                   }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetPlans, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (planResults != null && planResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #region GetSpecs
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v2/GetSpecs?divisionId={divisionId}&communityID={communityID}&planId={planId}&specId={specId}&status={status}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiHomeV2> GetSpecsV2(string DivisionID, string CommunityID, string PlanId, string SpecId, string status)
        {
            List<Item> specResults = null;

            try
            {
                specResults = new HomeQueries().GetHomesForSale(DivisionID, CommunityID, PlanId, SpecId, status, Sitecore.Context.Site.ContentStartPath);

                return specResults.Where(s => s != null)
                    .Select(s => new
                    {
                        Item = s,
                        DivisionParent = GetParentItem(s, SCIDs.TemplateIds.DivisionPage),
                        CommunityParent = GetParentItem(s, SCIDs.TemplateIds.CommunityPage),
                        PlanParent = GetParentItem(s, SCIDs.TemplateIds.PlanPage),
                        City = GetParentItem(s, SCIDs.TemplateIds.CityPage),
                        State = GetParentItem(s, SCIDs.TemplateIds.StatePage)
                    })
                   .Select(s => new InventoryApiHomeV2
                   {
                       Id = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyID]),
                       URL = GetUrl(s.Item),
                       Address = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.StreetAddress1]),
                       Status = (s.Item[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] == SCIDs.StatusIds.Status.Active) ? "Active" : "Inactive",
                       Description = s.Item[SCFieldNames.HomeForSaleFields.Description],
                       Availability = getSpecAvailability(s.Item),
                       AvailabilityDate = GetDate(s.Item),
                       Images = GetAPIImages(s.Item, SCIDs.TemplateIds.HomeForSalePage.ToString()),
                       Ifp = GetFieldURL(s.Item.Fields[SCFieldNames.PlanBaseFields.IFP]),
                       Price = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Price]),
                       Lot = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.LotNumber]),
                       Sqft = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Sqft]),
                       Beds = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Beds]),
                       Baths = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Bathrooms]),
                       HalfBaths = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.HalfBathrooms]),
                       Stories = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Stories]),
                       Garage = GetTextOrEmpty(s.Item[SCFieldNames.HomeForSaleFields.Garages]),
                       HasLivingRoom = s.Item[SCFieldNames.HomeForSaleFields.HasLivingRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasLivingRoom] : "0",
                       HasDiningRoom = s.Item[SCFieldNames.HomeForSaleFields.HasDiningRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasDiningRoom] : "0",
                       HasSunRoom = s.Item[SCFieldNames.HomeForSaleFields.HasSunRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasSunRoom] : "0",
                       HasStudy = s.Item[SCFieldNames.HomeForSaleFields.HasStudy] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasStudy] : "0",
                       HasLoft = s.Item[SCFieldNames.HomeForSaleFields.HasLoft] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasLoft] : "0",
                       HasOffice = s.Item[SCFieldNames.HomeForSaleFields.HasOffice] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasOffice] : "0",
                       HasGameRoom = s.Item[SCFieldNames.HomeForSaleFields.HasGameRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasGameRoom] : "0",
                       HasMediaRoom = s.Item[SCFieldNames.HomeForSaleFields.HasMediaRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasMediaRoom] : "0",
                       HasGuestBedroom = s.Item[SCFieldNames.HomeForSaleFields.HasGuestBedroom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasGuestBedroom] : "0",
                       HasBonusRoom = s.Item[SCFieldNames.HomeForSaleFields.HasBonusRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasBonusRoom] : "0",
                       HasBasement = s.Item[SCFieldNames.HomeForSaleFields.HasBasement] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasBasement] : "0",
                       HasFireplace = s.Item[SCFieldNames.HomeForSaleFields.HasFireplace] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasFireplace] : "0",
                       HasMasterBedroom = s.Item[SCFieldNames.HomeForSaleFields.HasMasterBedroom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasMasterBedroom] : "0",
                       HasFamilyRoom = s.Item[SCFieldNames.HomeForSaleFields.HasFamilyRoom] != string.Empty ? s.Item[SCFieldNames.HomeForSaleFields.HasFamilyRoom] : "0",
                       CommunityId = GetTextOrEmpty(s.CommunityParent[SCFieldNames.CommunityFields.CommunityLegacyId]),
                       CommunityUrl = GetTextOrEmpty(GetUrl(s.CommunityParent)),
                       CommunityName = GetTextOrEmpty(s.CommunityParent[SCFieldNames.CommunityFields.CommunityName]),
                       DivisionID = GetTextOrEmpty(s.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID]),
                       DivisionName = GetTextOrEmpty(s.DivisionParent[SCFieldNames.DivisionFields.DivisionName]),
                       DivisionUrl = GetTextOrEmpty(GetUrl(s.DivisionParent)),
                       PlanId = GetTextOrEmpty(s.PlanParent[SCFieldNames.PlanBaseFields.PlanLegacyID]),
                       PlanUrl = GetTextOrEmpty(GetUrl(s.PlanParent)),
                       PlanName = GetTextOrEmpty(s.PlanParent[SCFieldNames.PlanBaseFields.PlanName]),
                       City = GetTextOrEmpty(s.City[SCFieldNames.City.Name]),
                       State = GetTextOrEmpty(GetStateValue(GetStateName(s.State))),
                       Zip = GetTextOrEmpty(s.CommunityParent[SCFieldNames.CommunityFields.Zip]),
                       VirtualTourURL = GetTextOrEmpty(GetVirtualTour(s.Item)),
                       CommunityPhone = GetTextOrEmpty(s.CommunityParent[SCFieldNames.CommunityFields.Phone]),
                       IHCCard = GetIHCCardList(s.Item, s.State)

                   }).ToList();
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetSpecs, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (specResults != null && specResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #region GetCommunities
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/v2/GetCommunities?divisionId={divisionId}&communityID={communityID}&communityStatus={communityStatus}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<InventoryApiCommunityV2> GetCommunitiesV2(string divisionID, string communityID, string communityStatus)
        {
            List<Item> communityResults = null;

            try
            {
                communityResults = new CommunityQueries().GetCommunities(divisionID, communityID, communityStatus, Sitecore.Context.Site.ContentStartPath);

                if (communityResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }

                List<InventoryApiCommunityV2> list = communityResults
                    .Where(r => r != null)
                    .Select(r => new
                    {
                        Item = r,
                        DivisionParent = GetParentItem(r, SCIDs.TemplateIds.DivisionPage),
                        City = GetParentItem(r, SCIDs.TemplateIds.CityPage),
                        State = GetParentItem(r, SCIDs.TemplateIds.StatePage),
                        CalculatedFields = GetCalculatedFields(r[SCFieldNames.CommunityFields.CommunityLegacyId])
                    })
                    .Select(r => new InventoryApiCommunityV2
                    {
                        Id =GetTextOrEmpty( r.Item[SCFieldNames.CommunityFields.CommunityLegacyId]),
                        URL = GetTextOrEmpty(GetUrl(r.Item)),
                        Name = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.CommunityName]),
                        Status = GetTextOrEmpty(GetCommunityStatus(r.Item)),
                        Description = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Description]),
                        Logo = GetLogoURL(r.Item),
                        IsTownsend = 0,
                        Directions = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Directions]),
                        Latitude = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Latitude]),
                        Longitude = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Longitude]),
                        Address = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.StreetAddress1]),
                        Zip = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Zip]),
                        Phone = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Phone]),
                        SalesTeam = GetSalesTeam(r.Item),
                        Images = GetCommunityImages(r.Item),
                        SquareFeet = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.SquareFeets]),
                        Beds = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Beds]),
                        Baths = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Baths]),
                        HalfBaths = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.HalfBaths]),
                        Stories = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Stories]),
                        Garage = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.Garage]),
                        UseOfficeHoursText = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "1") ? 1 : 0,
                        OfficeHoursText = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "1") ? r.Item[SCFieldNames.CommunityFields.OfficeHoursText] : "",
                        SundayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Sunday, r.Item) : "",
                        MondayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Monday, r.Item) : "",
                        TuesdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Tuesday, r.Item) : "",
                        WednesdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Wednesday, r.Item) : "",
                        ThursdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Thursday, r.Item) : "",
                        FridayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Friday, r.Item) : "",
                        SaturdayHours = (r.Item[SCFieldNames.CommunityFields.UseOfficeHoursText] == "") ? GetWorkingHours(DayOfWeek.Saturday, r.Item) : "",
                        SchoolDistrict = (!string.IsNullOrEmpty(r.Item[SCFieldNames.CommunityFields.SchoolDistrict])) ? Sitecore.Context.Database.GetItem(new ID(r.Item[SCFieldNames.CommunityFields.SchoolDistrict])).Name : null,
                        Schools = GetSchools(r.Item),
                        IsActiveAdult = (r.Item[SCFieldNames.CommunityFields.IsActiveAdult] == "1") ? 1 : 0,
                        IsMasterPlanned = (r.Item[SCFieldNames.CommunityFields.IsMasterPlanned] == "1") ? 1 : 0,
                        IsGated = (r.Item[SCFieldNames.CommunityFields.IsGated] == "1") ? 1 : 0,
                        IsCondoOnly = (r.Item[SCFieldNames.CommunityFields.IsCondoOnly] == "1") ? 1 : 0,
                        HasPool = (r.Item[SCFieldNames.CommunityFields.HasPool] == "1") ? 1 : 0,
                        HasPlayground = (r.Item[SCFieldNames.CommunityFields.HasPlayground] == "1") ? 1 : 0,
                        HasGolfCourse = (r.Item[SCFieldNames.CommunityFields.HasGolfCourse] == "1") ? 1 : 0,
                        HasTennis = (r.Item[SCFieldNames.CommunityFields.HasTennis] == "1") ? 1 : 0,
                        HasSoccer = (r.Item[SCFieldNames.CommunityFields.HasSoccer] == "1") ? 1 : 0,
                        HasVolleyball = (r.Item[SCFieldNames.CommunityFields.HasVolleyball] == "1") ? 1 : 0,
                        HasBasketball = (r.Item[SCFieldNames.CommunityFields.HasBasketball] == "1") ? 1 : 0,
                        HasBaseball = (r.Item[SCFieldNames.CommunityFields.HasBaseball] == "1") ? 1 : 0,
                        HasView = (r.Item[SCFieldNames.CommunityFields.HasView] == "1") ? 1 : 0,
                        HasLake = (r.Item[SCFieldNames.CommunityFields.HasLake] == "1") ? 1 : 0,
                        HasPond = (r.Item[SCFieldNames.CommunityFields.HasPond] == "1") ? 1 : 0,
                        HasMarina = (r.Item[SCFieldNames.CommunityFields.HasMarina] == "1") ? 1 : 0,
                        HasBeach = (r.Item[SCFieldNames.CommunityFields.HasBeach] == "1") ? 1 : 0,
                        IsWaterfront = (r.Item[SCFieldNames.CommunityFields.IsWaterfront] == "1") ? 1 : 0,
                        HasPark = (r.Item[SCFieldNames.CommunityFields.HasPark] == "1") ? 1 : 0,
                        HasTrails = (r.Item[SCFieldNames.CommunityFields.HasTrails] == "1") ? 1 : 0,
                        HasGreenbelt = (r.Item[SCFieldNames.CommunityFields.HasGreenbelt] == "1") ? 1 : 0,
                        HasClubhouse = (r.Item[SCFieldNames.CommunityFields.HasClubhouse] == "1") ? 1 : 0,
                        DivisionID = r.DivisionParent[SCFieldNames.DivisionFields.LegacyDivisionID],
                        DivisionName = r.DivisionParent[SCFieldNames.DivisionFields.DivisionName],
                        DivisionUrl = GetUrl(r.DivisionParent),
                        OfficeAddress = GetCommunityAddress(r.Item),
                        OfficeCity = (r.City != null) ? GetTextOrEmpty(r.City[SCFieldNames.City.Name]) : "",
                        OfficeState = GetTextOrEmpty(GetStateValue(GetStateName(r.State))),
                        CalculatedSquareFeet = r.CalculatedFields.CalculatedSquareFeet,
                        CalculatedBeds = r.CalculatedFields.CalculatedBeds,
                        CalculatedBaths = r.CalculatedFields.CalculatedBaths,
                        CalculatedHalfBaths = r.CalculatedFields.CalculatedHalfBaths,
                        CalculatedStories = r.CalculatedFields.CalculatedStories,
                        CalculatedGarage = r.CalculatedFields.CalculatedGarage,
                        IHCCard = GetIHCCardList(r.Item, r.State),
                        NSEProjectID = GetTextOrEmpty(r.Item[SCFieldNames.CommunityFields.NSEProjectID]),
                        NSEOunit = GetTextOrEmpty(r.Item[SCFieldNames.DivisionFields.NSEOunit]),
                        SalesforceStateValue = GetTextOrEmpty(r.State[SCFieldNames.StateFields.SalesForceState]),
                        StatusChangeDate = GetTextOrEmpty(GetUTCFormatDate(r.Item.Fields[SCFieldNames.CommunityFields.StatusChangeDate]))
                    }
                    ).ToList();

                return list;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("GetCommunities, An error occur: ", ex, this);
                throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.InternalServerError);
            }
            finally
            {
                if (communityResults != null && communityResults.Count == 0)
                {
                    throw new WebFaultException<string>(null,
                        System.Net.HttpStatusCode.NotFound);
                }
            }

        }
        #endregion

        #endregion

        private List<Item> _states;
        private List<Item> States
        {
            get
            {
                if (_states == null || _states.Count() == 0)
                {
                    _states = Sitecore.Context.Database.GetItem(SCIDs.GlobalSharedFieldValues.States).Axes
                        .GetDescendants()
                        .Where(x => x.TemplateID.ToString() == SCIDs.TemplateIds.SwitchLookupItem)
                        .ToList();
                }
                return _states;
            }
        }
        private OfficeAddress GetCommunityAddress(Item Community)
        {
            var officeAddress = new OfficeAddress();
            if (Community != null) {
                officeAddress.Address1 = GetTextOrEmpty(Community[SCFieldNames.CommunityFields.StreetAddress1]);
                officeAddress.Address2 = GetTextOrEmpty(Community[SCFieldNames.CommunityFields.StreetAddress2]);
            } 
            return officeAddress;
        }

        private string GetUTCFormatDate(DateField dateField)
        {
            if (dateField == null
                || string.IsNullOrEmpty(dateField.Value) ) {
                return "";
            }

            var dateFormat = dateField.DateTime;
            var culture = new CultureInfo("en-US");
            return dateFormat.ToString("u", culture);
        }

        private string GetStateName(Item i)
        {
            var state = (!string.IsNullOrEmpty(i["State Name"])) ?
                Sitecore.Context.Database.GetItem(new ID(i["State Name"])).Name : "";

            return state;
        }

        private string GetFieldURL(Field item)
        {
            var stringResponse = string.Empty;
            if (item != null
                && !string.IsNullOrEmpty(item.Value))
            {
                stringResponse = LinkUrl(item);
            }
            return GetTextOrEmpty(stringResponse); 
        }

        private string GetVirtualTour(Item Plan)
        {
            var Master = GetVirtualMasterPlan(Plan);
           
            var statusItemText = Plan[SCFieldNames.PlanPageFields.VirtualTourStatus];
            if (statusItemText == null
                || Master == null
                || string.IsNullOrEmpty(statusItemText))
            {
                return "";
            }

            var statusItem = Sitecore.Context.Database.GetItem(new ID(statusItemText));
            if (statusItem == null ||
                String.IsNullOrEmpty(statusItem["Name"]) ||
                statusItem["Name"] != "Active" ||
                Master.Fields[SCFieldNames.VirtualTour.Url] == null ||
                String.IsNullOrEmpty(Master[SCFieldNames.VirtualTour.Url]))
            {
                return "";
            }

            return GetTextOrEmpty(LinkUrl(Master.Fields[SCFieldNames.VirtualTour.Url]));

        }

        private string GetStateValue(string Area)
        {
            var abbr = string.Empty;

            States.ForEach(state =>
            {
                var name = state[SCFieldNames.StateFields.Name];
                if (!String.IsNullOrEmpty(name) &&
                    name == Area) {
                        abbr = state[SCFieldNames.StateFields.Code];
                }
            });
            return abbr;
        }

        private String LinkUrl(Sitecore.Data.Fields.LinkField lf)
        {
            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    return lf.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not emptyge
                    return lf.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lf.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return lf.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return lf.Url;
                case "javascript":
                    // Just return javascript
                    return lf.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return lf.Url;
            }
        }

        private string GetSFState(Item division)
        {
            if (division == null)
            {
                return null;
            }

            var state = division.Parent;
            var sf = state.Fields[SCFieldNames.StateFields.SalesForceState];

            if (sf == null)
            {
                return null;
            }

            return GetFieldOrEmpty(sf);
        }

        private string GetFieldOrEmpty(Field field)
        {
            return field != null ? field.Value : "";
        }

        private string GetTextOrEmpty(string text)
        {
            return !String.IsNullOrEmpty(text) ? text : "";
        }

        private List<IHCCard> GetIHCCardList(Item division, Item parent = null)
        {
            if (division == null)
            {
                return null;
            }
            var ihcCards = division[SCFieldNames.DivisionFields.IHCCard];
            var originalItem = division;


            while (originalItem.TemplateID != parent.TemplateID
                   && parent != null)
            {
                var parentIHCCard = division[SCFieldNames.DivisionFields.IHCCard];
                if (!String.IsNullOrEmpty(parentIHCCard))
                {
                    ihcCards = parentIHCCard;
                    originalItem = parent;
                }
                else
                {
                    if (originalItem.ID != division.ID)
                    {
                        originalItem = originalItem.Parent;
                    }
                    division = division.Parent;
                }

            }
            var response = new List<IHCCard>();

            if (!String.IsNullOrEmpty(ihcCards))
            {
                var splitted = ihcCards
                .Split('|').ToList();

                splitted.ForEach(id =>
                {
                    var item = Sitecore
                        .Context.Database.GetItem(id);

                    if (item != null)
                    {
                        var statusId = item[SCFieldNames.IHCCard.Status];
                        if (!String.IsNullOrEmpty(statusId))
                        {
                            var statusItem = Sitecore.Context.Database.GetItem(new ID(statusId));
                            if (statusItem.Name == SCFieldNames.Status.Active)
                            {
                                var IHC = new IHCCard()
                                {
                                    IHCFirstName = GetFieldOrEmpty(item.Fields[SCFieldNames.IHCCard.FirstName]),
                                    IHCLastName = GetFieldOrEmpty(item.Fields[SCFieldNames.IHCCard.LastName]),
                                    IHCEmail = GetFieldOrEmpty(item.Fields[SCFieldNames.IHCCard.EmailAddress]),
                                    IHCPhone = GetFieldOrEmpty(item.Fields[SCFieldNames.IHCCard.Phone])
                                };
                                response.Add(IHC);
                            }
                        }
                    }
                });
            }
            return response;
        }

        private string GetDisclaimerText(Item baseItem, bool getHSH = false)
        {
            var name = SCFieldNames.DivisionFields.Disclaimer;
            var count = 0;
            if (getHSH)
                name = SCFieldNames.DivisionFields.HotSheetDisclaimer;

            while (count <= 1)
            {
                if (baseItem.Fields != null && baseItem.Fields[name] != null)
                {
                    if (!String.IsNullOrEmpty(baseItem.Fields[name].Value))
                    {
                        return baseItem.Fields[name].Value;
                    }
                }
                if (getHSH)
                {
                    return "";
                }
                baseItem = baseItem.Parent;
                count++;
            }
            return "";
        }

        private class CommunityCalculatedField
        {
            public string CalculatedSquareFeet { get; set; }
            public string CalculatedBeds { get; set; }
            public string CalculatedBaths { get; set; }
            public string CalculatedHalfBaths { get; set; }
            public string CalculatedStories { get; set; }
            public string CalculatedGarage { get; set; }
        }
        private CommunityCalculatedField GetCalculatedFields(string communityId)
        {
            int tempNumber = 0;

            try
            {
                List<Item> planResults = new PlanQueries().GetPlans(null, communityId, null, null, Sitecore.Context.Site.ContentStartPath);

                List<PlanInfo> planInfo = planResults.Where(p => p != null)
                                            .Select(p => new PlanInfo
                                            {
                                                SquareFootage = Int32.TryParse(p[SCFieldNames.PlanBaseFields.Sqft], out tempNumber) ? tempNumber : 0,
                                                NumberofBedrooms = Int32.TryParse(p[SCFieldNames.PlanBaseFields.Bedrooms], out tempNumber) ? tempNumber : 0,
                                                NumberofBathrooms = Int32.TryParse(p[SCFieldNames.PlanBaseFields.Bathrooms], out tempNumber) ? tempNumber : 0,
                                                NumberofHalfBathrooms = Int32.TryParse(p[SCFieldNames.PlanBaseFields.HalfBathrooms], out tempNumber) ? tempNumber : 0,
                                                NumberofStories = Int32.TryParse(p[SCFieldNames.PlanBaseFields.Stories], out tempNumber) ? tempNumber : 0,
                                                NumberofGarages = Int32.TryParse(p[SCFieldNames.PlanBaseFields.Garages], out tempNumber) ? tempNumber : 0
                                            }).ToList();

                List<HomeForSaleInfo> homeInfo = new List<HomeForSaleInfo>();
                List<Item> specResults = new List<Item>();

                if (planInfo.Count == 0)
                {
                    specResults = new HomeQueries().GetHomesForSale(null, communityId, null, null, null, Sitecore.Context.Site.ContentStartPath);
                    homeInfo = specResults.Where(p => p != null)
                                    .Select(p => new HomeForSaleInfo
                                    {
                                        SquareFootage = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.Sqft], out tempNumber) ? tempNumber : 0,
                                        NumberofBedrooms = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.Beds], out tempNumber) ? tempNumber : 0,
                                        NumberofBathrooms = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.Bathrooms], out tempNumber) ? tempNumber : 0,
                                        NumberofHalfBathrooms = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.HalfBathrooms], out tempNumber) ? tempNumber : 0,
                                        NumberofStories = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.Stories], out tempNumber) ? tempNumber : 0,
                                        NumberofGarages = Int32.TryParse(p[SCFieldNames.HomeForSaleFields.Garages], out tempNumber) ? tempNumber : 0
                                    }).ToList();
                }

                TM.Web.Custom.SCHelpers.SearchHelper helper = new TM.Web.Custom.SCHelpers.SearchHelper();

                CommunityCalculatedField calculatedFields = new CommunityCalculatedField();
                calculatedFields.CalculatedSquareFeet = helper.GetSquareFoot(planInfo, homeInfo);
                calculatedFields.CalculatedBeds = SCHelpers.SearchHelper.GetBedrooms(planInfo, homeInfo);
                calculatedFields.CalculatedBaths = SCHelpers.SearchHelper.GetBathrooms(planInfo, homeInfo);
                calculatedFields.CalculatedHalfBaths = SCHelpers.SearchHelper.GetHalfBathrooms(planInfo, homeInfo);
                calculatedFields.CalculatedStories = SCHelpers.SearchHelper.GetStory(planInfo, homeInfo, helper.SCCurrentDeviceType);
                calculatedFields.CalculatedGarage = SCHelpers.SearchHelper.GetGarage(planInfo, homeInfo);

                return calculatedFields;
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("GetCommunities GetCalculatedFields , An error occur: ", e, this);
                return new CommunityCalculatedField();
            }
        }
    }//
}
