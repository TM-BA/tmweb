﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetSFForms.aspx.cs" Inherits="TM.Web.Custom.Services.GetSFForms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <style>
        .form {
            width: 90%;
            background: #c2c2c2;
            margin: 20px auto;
            padding: 10px 2%;
            /* margin: 20px 0; */
            border: 1px solid black;
        }

        .form table {
            width: 100%;
            text-align: left;
            margin: 10px 0;
            border: 1px solid black;
        }

        th, td {
            border: 1px solid cadetblue;
        }
    </style>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <th>Form Path</th>
                <th>Form Id</th>
                <th>Form Field Name</th>
                <th>Form Field Id</th>
                <th>SF Item Name</th>
                <th>SF Field Name</th>
                <th>SF Field ID</th>
            </tr>
            <asp:Repeater ID="FormsRepeater" runat="server" OnItemDataBound="FormsRepeater_ItemDataBound">
                <ItemTemplate>
                    <asp:Repeater ID="FormFields" runat="server" OnItemDataBound="FieldsRepeater_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="Path" runat="server"></asp:Label></td>
                                <td><asp:Label ID="FormID" runat="server"></asp:Label></td>
                                <td><asp:Label ID="Name" runat="server" Text='<%#Eval("Name") %>'></asp:Label></td>
                                <td><asp:Label ID="FieldId" runat="server" Text='<%#Eval("Id") %>'></asp:Label></asp:Label></td>
                                <td><asp:Label ID="SFDisplayName" runat="server" Text='<%#Eval("SFInfo.SFDisplayName") %>'></asp:Label></td>
                                <td><asp:Label ID="SFName" runat="server" Text='<%#Eval("SFInfo.SFName") %>'></asp:Label></td>
                                <td><asp:Label ID="SFID" runat="server" Text='<%#Eval("SFInfo.SFID") %>'></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    </form>
</body>
</html>
