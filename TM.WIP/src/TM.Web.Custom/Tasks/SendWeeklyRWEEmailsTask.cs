﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Modules.EmailCampaign;

namespace TM.Web.Custom.Tasks
{
    public class SendWeeklyRWEEmailsTask
    {
        public void Execute(
            Sitecore.Data.Items.Item[] items,
            Sitecore.Tasks.CommandItem command,
            Sitecore.Tasks.ScheduleItem schedule)
        {
            if (items != null)
            {
                foreach (var item in items)
                {
                    var mi = Factory.GetMessage(item);
                    new SendingManager(mi).SendMessage();
                }
            }

        }
    }
}