﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Sitecore;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using TM.Domain.Entities;

namespace TM.Web.Custom.admin
{
    public partial class LegacyIdCleanup : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnImport_OnClick(object sender, EventArgs e)
        {
            FindDuplicateIDAndFix();
        }

        private readonly Database _masterDB = Factory.GetDatabase("master");
        private StringBuilder _messages;
        private List<KeyValuePair<string, Item>> _communities = new List<KeyValuePair<string, Item>>();
        private List<KeyValuePair<string, Item>> _plans = new List<KeyValuePair<string, Item>>();
        private List<KeyValuePair<string, Item>> _hfs = new List<KeyValuePair<string, Item>>();

        private void FindDuplicateIDAndFix()
        {
            DataSource dataSource = _masterDB.DataManager.DataSource;

            var root = ItemIDs.ContentRoot;
            Process(root, dataSource);

            PrintMessages(_communities,"Community");
            PrintMessages(_plans,"Plans");
            PrintMessages(_hfs,"Inventory");
           
            CacheManager.ClearAllCaches();
        }

        private void PrintMessages(List<KeyValuePair<string, Item>> itemKeyValuePairs, string reportName)
        {
            _messages =
                 new StringBuilder("<br/><b> "+reportName+" Report:</b><br/>");
            _messages.Append("<hr/>");
            _messages.AppendFormat("<table><tr><th>Item Path</th><th>Legacy ID</th><th>Status</th><th>Count</th></tr>");
            var grouped = itemKeyValuePairs.GroupBy(x => x.Key);

            foreach (var grp in grouped)
            {
                var kvPairs = itemKeyValuePairs.Where(i => i.Key == grp.Key);
                int count = 0;
                var paths = string.Empty;
                var status = string.Empty;
                foreach (var keyValuePair in kvPairs)
                {
                    paths += keyValuePair.Value.Paths.FullPath + "<br/>";
                    var item = keyValuePair.Value;
		    if(item.TemplateID == new ID(HomeForSaleItem.TemplateId))
			status += item["home for sale status"].EndsWith("6D3}") ? "In Active<br/>" : "Active<br/>";

                    if (item.TemplateID == new ID(PlanItem.TemplateId))
                        status += item["plan status"].EndsWith("6D3}") ? "In Active<br/>" : "Active<br/>";

		    if(item.TemplateID == new ID(CommunityItem.TemplateId))
			 status += item["community status"].EndsWith("5330}") ? "In Active<br/>" : "Active<br/>";
    
                    count++;
                }
                if (count > 1)
                    _messages.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", grp.Key, paths, status,
                        count);
            }

            _messages.AppendFormat("</table><br/><br/>");
            litReport.Text += _messages.ToString();
        }

        public void Process(ID itemId, DataSource dataSource)
        {
            IDList childIds = dataSource.GetChildIDs(itemId);


            foreach (ID childId in childIds)
            {
                Process(childId, dataSource);
            }

            var item = _masterDB.GetItem(itemId);

            var templateID = item.TemplateID;

            if (templateID == new ID(CommunityItem.TemplateId) || templateID == new ID(HomeForSaleItem.TemplateId) || templateID == new ID(PlanItem.TemplateId))
            {
                var legacyid = string.Empty;

                if (templateID == new ID(CommunityItem.TemplateId))
                {
                    legacyid = item["community legacy id"]??string.Empty;
                    _communities.Add(new KeyValuePair<string, Item>(legacyid,item));
                    
                }
                if (templateID == new ID(HomeForSaleItem.TemplateId))
                {
                    legacyid = item["home for sale legacy id"] ?? string.Empty;
                    _hfs.Add(new KeyValuePair<string, Item>(legacyid,item));
                    
                }

                if (templateID == new ID(PlanItem.TemplateId))
                {
                    legacyid = item["plan legacy id"] ?? string.Empty;
                    _plans.Add(new KeyValuePair<string, Item>(legacyid,item));
                }

                  

            }
        }

       

        protected void btnPublish_OnClick(object sender, EventArgs e)
        {
           /* var itemIDs =hdnProcessedItems.Value.Trim('|').Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var itemID in itemIDs)
            {
                var item = masterDB.GetItem(new ID(itemID));
                Sitecore.Publishing.PublishManager.AddToPublishQueue(item, ItemUpdateType.Saved);
            }
            Database[] targetDBs = new Database[] { Sitecore.Configuration.Factory.GetDatabase("web") };
            Language[] languages = new Language[] { LanguageManager.GetLanguage("en") };
            Sitecore.Publishing.PublishManager.PublishIncremental(masterDB, targetDBs, languages);
*/


        }
    }
}