﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportData.aspx.cs" Inherits="TM.Web.Custom.admin.ImportData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Import Data</title>
    <style>
body {
	font: 10pt Tahoma;
	margin:20px;
}
div {
    margin-top: 10px;
}
    </style>
</head>
<body>
    <div>Current user:
        <asp:Literal ID="literalUser" runat="server" />
    </div>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
    <asp:Panel ID="panelContent" runat="server" Visible="false">
            <div>Available divisions
                <asp:DropDownList ID="dropDownDivisions" runat="server" />
            </div>
            <div>Xml import file
                <asp:FileUpload ID="fileUpload" runat="server" />
            </div>
            <div>
                <asp:CheckBox ID="checkboxShowWarnings" runat="server" Text="Show validation warnings" />
            </div>
            <div>
                <asp:Button type="submit" ID="buttonSubmit" Text="Upload" runat="server" />
            </div>
        </asp:Panel>
    </form>
    <asp:Panel ID="panelLog" runat="server">
        <asp:Literal ID="literalLog" runat="server" />
    </asp:Panel>
    <asp:Panel ID="panelError" runat="server">
        <asp:Literal ID="literalError" runat="server" />
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</body>
</html>
