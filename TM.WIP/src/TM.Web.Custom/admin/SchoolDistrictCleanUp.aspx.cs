﻿using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Security.AccessControl;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.Queries.LuceneQueries;

namespace TM.Web.Custom.admin
{
    public partial class SchoolDistrictCleanUp : BasePage
    {
        private readonly Database _masterDB = Factory.GetDatabase("master");

        protected void Page_Load(object sender, EventArgs e)
        {
            var error = string.Empty;
            var user = Sitecore.Context.User;
            literalUser.Text = user.DisplayName;

            if (!user.IsAuthenticated)
            {
                error = "Error, current user is not autenticated";
            }
            else if (!user.DisplayName.StartsWith("sitecore"))
            {
                error = "Error, user must be a sitecore user";
            }

            if (!string.IsNullOrEmpty(error))
            {
                literalError.Text = error;
                return;
            }

            lock (TaskHolder._lock)
            {
                if (TaskHolder._task != null)
                {
                    panelContent.Visible = false;
                    literalLog.Text = GetLog();
                    if (!TaskHolder._task.IsCompleted && !TaskHolder._task.IsFaulted)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 5000);</script>");
                    }
                    else
                    {
                        TaskHolder._task = null;
                        TaskHolder._log = null;
                    }
                    return;
                }

                panelContent.Visible = true;
                if (IsPostBack)
                {
                    TaskHolder._log = new StringBuilder();
                    panelContent.Visible = false;
                    var removeOldSchools = checkboxDeleteOldSchools.Checked;
                    var modifyCommunities = checkboxModifyCommunities.Checked;

                    Log("Log:");
                    Log("Starting job");
                    Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 5000);</script>");
                    
                    TaskHolder._task = Task.Run(() =>
                    {
                        Stopwatch watch = Stopwatch.StartNew();
                        try
                        {
                            var divisions = GetAllDivisions();
                            foreach (var division in divisions)
                            {
                                Log("Running on division: " + division.Paths.FullPath);
                                var cities = GetAllCities(division);
                                foreach (var city in cities)
                                {
                                    Log("Running on city: " + city.Name);
                                    CopySchoolDistricts(division, city);
                                    if (modifyCommunities)
                                    {
                                        var communities = GetAllCommunities(city);

                                        foreach (var community in communities)
                                        {
                                            Log("Running on community: " + community.Name);
                                            var schoolDistrict = GetOrCreateSchoolDistrict(division, community);
                                            if (schoolDistrict != null)
                                            {
                                                ModifyCommunitySchoolDistrict(community, schoolDistrict);
                                            }
                                        }
                                        if (removeOldSchools)
                                        {
                                            DeleteSchools(city);
                                        }
                                    }
                                }
                                ProtectSchoolDistrictsItem(division);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log("Error: {0}", ex.ToString());
                        }

                        watch.Stop();
                        Log("Total time elapsed: {0}", watch.Elapsed.ToString());
                    });
                }
            }

            literalLog.Text = GetLog();
        }

        #region Database Utils
        private Item GetItemByID(string ID)
        {
            return _masterDB.GetItem(new ID(ID));
        }

        private Item GetItemByPath(string path)
        {
            return _masterDB.GetItem(path);
        }

        private TemplateItem GetTemplateByID(ID templateId)
        {
            return _masterDB.GetTemplate(templateId);
        }
        #endregion

        #region Utils
        private void ProtectSchoolDistrictsItem(Item division)
        {
            var divisionDchoolDistricts = GetOrCreateSchoolDistricts(division);
            var myRole = Role.FromName("sitecore\\Everyone");
            var accessRules = divisionDchoolDistricts.Security.GetAccessRules();
            accessRules.Helper.AddAccessPermission(myRole,
                AccessRight.ItemWrite,PropagationType.Any,AccessPermission.Deny);
            accessRules.Helper.AddAccessPermission(myRole,
                AccessRight.ItemRename,PropagationType.Any,AccessPermission.Deny);
            accessRules.Helper.AddAccessPermission(myRole,
                AccessRight.ItemDelete,PropagationType.Any,AccessPermission.Deny);
            accessRules.Helper.AddAccessPermission(myRole,
                AccessRight.ItemCreate,PropagationType.Any,AccessPermission.Deny);

            divisionDchoolDistricts.Editing.BeginEdit();
            divisionDchoolDistricts.Security.SetAccessRules(accessRules);
            divisionDchoolDistricts.Editing.EndEdit(); 
        }

        private void DeleteSchools(Item city)
        {
            var schoolDistrictsItem = city.Children
               .Where(i => i.TemplateID == SCIDs.TemplateIds.SchoolDistricts)
               .FirstOrDefault();
            if (schoolDistrictsItem != null)
            {
                Log("Recycling school districts on city: {0}", city.Name);
                schoolDistrictsItem.Recycle();
            }
        }

        private void CopySchoolDistricts(Item division, Item city)
        {
            var citySchoolDistricts = city.Children
               .Where(i => i.TemplateID == SCIDs.TemplateIds.SchoolDistricts)
               .FirstOrDefault();
            if (citySchoolDistricts == null)
            {
                return;
            }

            var divisionDchoolDistricts = GetOrCreateSchoolDistricts(division);

            foreach (Item citySchoolDistrict in citySchoolDistricts.Children)
            {
                var schoolDistrictItem = divisionDchoolDistricts.Children
                    .Where(i => i.Name == citySchoolDistrict.Name)
                    .FirstOrDefault();
                if (schoolDistrictItem == null)
                {
                    CreateSchoolDistrict(divisionDchoolDistricts, citySchoolDistrict);
                }
            }
        }

        private void ModifyCommunitySchoolDistrict(Item community, Item schoolDistrict)
        {
            if (community[SCFieldNames.CommunityFields.SchoolDistrict] == schoolDistrict.ID.ToString())
            {
                return;
            }
            Log("Assigning new school to community '{0}'", community.Name);
            community.Editing.BeginEdit();
            community[SCFieldNames.CommunityFields.SchoolDistrict] = schoolDistrict.ID.ToString();
            community.Editing.EndEdit();
        }

        private Item GetOrCreateSchoolDistrict(Item division, Item community)
        {
            var schoolDistrictsItem = GetOrCreateSchoolDistricts(division);

            var oldSchoolDistrictID = community[SCFieldNames.CommunityFields.SchoolDistrict];
            if (string.IsNullOrEmpty(oldSchoolDistrictID))
            {
                return null;
            }
            var oldSchoolDistrict = GetItemByID(oldSchoolDistrictID);
            if (oldSchoolDistrict == null)
            {
                return null;
            }

            var schoolDistrictItem = schoolDistrictsItem.Children
                .Where(i => i.Name == oldSchoolDistrict.Name)
                .FirstOrDefault();

            if (schoolDistrictItem != null)
            {
                return schoolDistrictItem;
            }

            schoolDistrictItem = CreateSchoolDistrict(schoolDistrictsItem, oldSchoolDistrict);

            return schoolDistrictItem;
        }

        private Item CreateSchoolDistrict(Item schoolDistrictsItem, Item oldSchoolDistrict)
        {
            Log("Creating school district '{0}'", oldSchoolDistrict.Name);
            var schoolDistrictItem = schoolDistrictsItem.Add(oldSchoolDistrict.Name, new TemplateID(SCIDs.TemplateIds.SchoolDistrict));

            schoolDistrictItem.Editing.BeginEdit();
            schoolDistrictItem[SCFieldNames.SchoolDistrict.Name] = oldSchoolDistrict[SCFieldNames.SchoolDistrict.Name];
            schoolDistrictItem[SCFieldNames.SchoolDistrict.SchoolDistrictID] = oldSchoolDistrict[SCFieldNames.SchoolDistrict.SchoolDistrictID];
            //schoolDistrictItem.Appearance.ReadOnly = true;
            schoolDistrictItem.Editing.EndEdit();

            return schoolDistrictItem;
        }

        private Item GetOrCreateSchoolDistricts(Item division)
        {
            var schoolDistrictsItem = division.Children
               .Where(i => i.TemplateID == SCIDs.TemplateIds.SchoolDistricts)
               .FirstOrDefault();

            if (schoolDistrictsItem == null)
            {
                Log("Creating school district nodes for division '{0}'", division.Name);
                schoolDistrictsItem = division.Add("School Districts", new TemplateID(SCIDs.TemplateIds.SchoolDistricts));
                //schoolDistrictsItem.Editing.BeginEdit();
                //schoolDistrictsItem.Appearance.ReadOnly = true;
                //schoolDistrictsItem.Editing.EndEdit();
            }
            return schoolDistrictsItem;
        }

        private IEnumerable<Item> GetAllDivisions()
        {
            using (var context = ContentSearchManager.GetIndex(string.Format("sitecore_{0}_index", _masterDB.Name)).CreateSearchContext())
            {
                var divisions = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.DivisionPage
                        && i.IsLatestVersion && !i.IsTemplate
                        && i.Language == "en" && i.Name != "__Standard Values")
                    .Select(i => i.GetItem())
                    .ToList();

                return divisions
                    .OrderBy(i => i.Paths.Path);
            }
        }

        private IEnumerable<Item> GetAllCities(Item division)
        {
            return division.Children
                .Where(i => i.TemplateID == SCIDs.TemplateIds.CityPage);
        }

        private IEnumerable<Item> GetAllCommunities(Item city)
        {
            return city.Children
                .Where(i => i.TemplateID == SCIDs.TemplateIds.CommunityPage);
        }

        private void Log(string format, params object[] values)
        {
            if (TaskHolder._log == null)
                TaskHolder._log = new StringBuilder();
            TaskHolder._log.AppendFormat(format, values);
            TaskHolder._log.AppendLine();
        }

        private string GetLog()
        {
            if (TaskHolder._log == null)
                return string.Empty;
            return HttpUtility.HtmlEncode(TaskHolder._log.ToString()).Replace(Environment.NewLine, "<br/>");
        }
        #endregion

        protected void buttonSubmit_Click(object sender, EventArgs e)
        {

        }
    }
}