﻿using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers.Search;

namespace TM.Web.Custom.admin
{
    public partial class ImportData : BasePage
    {
        private readonly Database _masterDB = Factory.GetDatabase("master");

        protected void Page_Load(object sender, EventArgs e)
        {
            var error = string.Empty;
            var user = Sitecore.Context.User;
            literalUser.Text = user.DisplayName;

            if (!user.IsAuthenticated)
            {
                error = "Error, current user is not autenticated";
            }
            else if (!user.DisplayName.StartsWith("sitecore"))
            {
                error = "Error, user must be a sitecore user";
            }

            if (!string.IsNullOrEmpty(error))
            {
                literalError.Text = error;
                return;
            }

            lock (TaskHolder._lock)
            {
                if (TaskHolder._task != null)
                {
                    panelContent.Visible = false;
                    literalLog.Text = GetLog();
                    if (!TaskHolder._task.IsCompleted && !TaskHolder._task.IsFaulted)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 5000);</script>");
                    }
                    else
                    {
                        TaskHolder._task = null;
                        TaskHolder._log = null;
                    }
                    return;
                }

                if (!IsPostBack)
                {
                    var divisions = GetDivisions();
                    if (divisions.Count == 0)
                    {
                        error = "Error, there are no empty divisions available";
                    }
                    foreach (var division in divisions)
                    {
                        dropDownDivisions.Items.Add(new System.Web.UI.WebControls.ListItem(division.Paths.Path.Replace("/sitecore/content", ""), division.ID.ToString()));
                    }
                }

                if (!string.IsNullOrEmpty(error))
                {
                    literalError.Text = error;
                    return;
                }

                panelContent.Visible = true;
                if (IsPostBack)
                {
                    TaskHolder._log = new StringBuilder();
                    Log("Log:");
                    var file = fileUpload.PostedFile;
                    if (file != null && file.ContentLength > 0)
                    {
                        var extension = Path.GetExtension(file.FileName).ToLower();
                        if (extension != ".xml")
                        {
                            Log("Only xml files are allowed");
                        }
                        else
                        {
                            panelContent.Visible = false;
                            Log("Starting job");
                            Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 5000);</script>");
                            TaskHolder._task = Task.Run(() =>
                            {
                                var watch = Stopwatch.StartNew();
                                var data = ReadPostedFile(file);
                                var xsdPath = Path.Combine(Server.MapPath("/"), "admin", "import_data.xsd");
                                var valid = ValidateXml(data, xsdPath, checkboxShowWarnings.Checked);

                                if (valid)
                                {
                                    Log("Xml is valid");
                                    var divisionItem = GetItemByID(dropDownDivisions.SelectedValue);
                                    ProcessXml(data, divisionItem);
                                }
                                watch.Stop();
                                Log("Total time elapsed: {0}", watch.Elapsed.ToString());
                            });
                        }
                    }
                    else
                    {
                        Log("Please choose a file to import");
                    }
                }
            }

            literalLog.Text = GetLog();
        }

        private void ProcessXml(byte[] data, Item divisionItem)
        {
            try
            {
                var settings = new XmlReaderSettings();
                settings.CheckCharacters = true;
                using (var reader = XmlReader.Create(new MemoryStream(data), settings))
                {
                    foreach (var community in GetElements(reader, "Subdivision"))
                    {
                        var cityItem = CreateCity(community, divisionItem);
                        var schoolDistrictItem = CreateSchoolDistrict(community, divisionItem);
                        var communityItem = CreateCommunity(community, cityItem, schoolDistrictItem);

                        CreateCommunitySchools(community, communityItem);
                        
                        var plans = community.Elements("Plan");
                        foreach (var plan in plans)
                        {
                            var masterPlanItem = CreateMasterPlan(plan, divisionItem);
                            var planItem = CreatePlan(plan, masterPlanItem, communityItem);
                            
                            var homes = plan.Elements("Spec");
                            foreach (var home in homes)
                            {
                                CreateHome(home, planItem);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log("Error: {0}", e.ToString());
            }
        }

        #region Sitecore elements
        private Item CreateHome(XElement home, Item planItem)
        {
            var addressNode = home.Element("SpecAddress");
            var homeName = GetValidName(addressNode.Element("SpecStreet1").Value);
            var homeItem = planItem.Children
                .Where(i => i.Name.StartsWith("Home Available Now at " + homeName))
                .FirstOrDefault();
            if (homeItem != null)
            {
                Log("Home '{0}' on plan '{1}' already exists", homeName, planItem.Name);
            }
            else
            {
                Log("Creating home '{0}' on plan '{1}'", homeName, planItem.Name);
                homeItem = planItem.Add(homeName, new TemplateID(SCIDs.TemplateIds.HomeForSalePage));
            }

            homeItem.Editing.BeginEdit();

            homeItem[SCFieldNames.HomeForSaleFields.MasterPlanID] = planItem[SCFieldNames.PlanPageFields.MasterPlanID];
            homeItem[SCFieldNames.HomeForSaleFields.HomeForSaleLegacyID] = home.Element("SpecNumber").Value;
            homeItem[SCFieldNames.HomeForSaleFields.HomeForSaleStatus] = SCIDs.StatusIds.Status.Active;
            homeItem[SCFieldNames.HomeForSaleFields.StatusForSearch] = SCIDs.StatusIds.Status.Active;
            homeItem[SCFieldNames.HomeForSaleFields.StatusForAssistance] = SCIDs.StatusIds.Status.Active;
            homeItem[SCFieldNames.HomeForSaleFields.StatusForAvailability] = SCIDs.StatusIds.InventoryHomeAvailabilityStatus.ReadyNow;
            homeItem[SCFieldNames.HomeForSaleFields.StreetAddress1] = addressNode.Element("SpecStreet1").Value;
            homeItem[SCFieldNames.HomeForSaleFields.Price] = home.Element("SpecPrice").Value;
            homeItem[SCFieldNames.HomeForSaleFields.Sqft] = home.Element("SpecSqft").Value;

            if (home.Element("SpecIsModel") != null)
            {
                homeItem[SCFieldNames.HomeForSaleFields.IsModel] = home.Element("SpecIsModel").Value;
            }
            
            if (addressNode.Element("SpecStreet2") != null)
            {
                homeItem[SCFieldNames.HomeForSaleFields.StreetAddress2] = addressNode.Element("SpecStreet2").Value;
            }
            if (addressNode.Element("SpecLot") != null)
            {
                homeItem[SCFieldNames.HomeForSaleFields.LotNumber] = addressNode.Element("SpecLot").Value;
            }

            if (home.Element("SpecMoveInDate") != null)
            {
                homeItem[SCFieldNames.HomeForSaleFields.AvailabilityDate] = home.Element("SpecMoveInDate").Element("Day").Value.Replace("-", "") + "T000000";
            }

            if (home.Element("SpecDiningAreas") != null)
            {
                homeItem[SCFieldNames.PlanBaseFields.DiningAreas] = home.Element("SpecDiningAreas").Value;
            }

            if (home.Element("SpecBasement") != null)
            {
                homeItem[SCFieldNames.PlanBaseFields.HasBasement] = home.Element("SpecBasement").Value;
            }

            homeItem[SCFieldNames.PlanBaseFields.Description] = home.Element("SpecDescription").Value.Trim();
            homeItem[SCFieldNames.PlanBaseFields.Stories] = home.Element("SpecStories").Value;
            homeItem[SCFieldNames.PlanBaseFields.Bathrooms] = home.Element("SpecBaths").Value;
            homeItem[SCFieldNames.PlanBaseFields.HalfBathrooms] = home.Element("SpecHalfBaths").Value;
            homeItem[SCFieldNames.PlanBaseFields.Garages] = home.Element("SpecGarage").Value;
            homeItem[SCFieldNames.PlanBaseFields.Bedrooms] = home.Element("SpecBedrooms").Value;
            
            switch (home.Element("SpecBedrooms").Attribute("MasterBedLocation").Value)
            {
                case "Up":
                    homeItem[SCFieldNames.PlanBaseFields.MasterBedLocation] = SCIDs.StatusIds.MasterBedroomLocation.Upstairs;
                    break;
                case "Down":
                    homeItem[SCFieldNames.PlanBaseFields.MasterBedLocation] = SCIDs.StatusIds.MasterBedroomLocation.Downstairs;
                    break;
                default:
                    Log("Unrecognized master bed location {0}", home.Element("SpecBedrooms").Attribute("MasterBedLocation").Value);
                    throw new Exception("Unrecognized master bed location");
            }

            if (home.Element("SpecGarage").Attribute("Entry") != null)
            {
                switch (home.Element("SpecGarage").Attribute("Entry").Value)
                {
                    case "Side":
                        homeItem[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Side;
                        break;
                    case "Front":
                        homeItem[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Front;
                        break;
                    case "Rear":
                        homeItem[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Rear;
                        break;
                    default:
                        Log("Unrecognized garage entry type {0}", home.Element("SpecGarage").Attribute("Entry").Value);
                        throw new Exception("Unrecognized garage entry type");
                }
            }

            var livingAreas = home.Elements("SpecLivingArea");
            foreach (var livingArea in livingAreas)
            {
                switch (livingArea.Attribute("Type").Value)
                {
                    case "LivingRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasLivingRoom] = "1";
                        break;
                    case "DiningRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasDiningRoom] = "1";
                        break;
                    case "FamilyRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasFamilyRoom] = "1";
                        break;
                    case "SunRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasSunRoom] = "1";
                        break;
                    case "Study":
                        homeItem[SCFieldNames.PlanBaseFields.HasStudy] = "1";
                        break;
                    case "Loft":
                        homeItem[SCFieldNames.PlanBaseFields.HasLoft] = "1";
                        break;
                    case "Office":
                        homeItem[SCFieldNames.PlanBaseFields.HasOffice] = "1";
                        break;
                    case "GameRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasGameRoom] = "1";
                        break;
                    case "MediaRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasMediaRoom] = "1";
                        break;
                    case "GuestRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasGuestBedroom] = "1";
                        break;
                    case "BonusRoom":
                        homeItem[SCFieldNames.PlanBaseFields.HasBonusRoom] = "1";
                        break;
                    default:
                        Log("Unrecognized living area {0}", livingArea.Attribute("Type").Value);
                        throw new Exception("Unrecognized living area");
                }
            }

            homeItem[SCFieldNames.PlanBaseFields.LivingAreas] = livingAreas.Count().ToString();

            var planAmenity = home.Element("SpecAmenity");
            if (planAmenity != null && planAmenity.Attribute("Type").Value == "Fireplaces")
            {
                homeItem[SCFieldNames.PlanBaseFields.HasFireplace] = "1";
            }

            Log("Loading home '{0}' images", homeName);

            var homeImages = home.Element("SpecImages");
            if (homeImages != null)
            {
                var elevationImageNodes = homeImages.Elements("SpecElevationImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (elevationImageNodes.Count() > 0)
                {
                    var elevationImageFolder = CreateMediaFolder(planItem, homeName + "/Images/Elevation");
                    var elevationImages = CreateImages(elevationImageFolder, elevationImageNodes, null, SCIDs.TemplateIds.ElevationImage);
                    homeItem[SCFieldNames.PlanBaseFields.ElevationImages] = string.Join("|", elevationImages.Select(i => i.ID));
                }

                var floorPlanImageNodes = homeImages.Elements("SpecFloorPlanImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (floorPlanImageNodes.Count() > 0)
                {
                    var floorPlanImageFolder = CreateMediaFolder(planItem, homeName + "/Images/FloorPlan");
                    var floorPlanImages = CreateImages(floorPlanImageFolder, floorPlanImageNodes, null, SCIDs.TemplateIds.FloorPlanImage);
                    homeItem[SCFieldNames.PlanBaseFields.FloorPlanImages] = string.Join("|", floorPlanImages.Select(i => i.ID));
                }

                var interiorImageNodes = homeImages.Elements("SpecInteriorImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (interiorImageNodes.Count() > 0)
                {
                    var interiorImageFolder = CreateMediaFolder(planItem, homeName + "/Images/Interior");
                    var interiorImages = CreateImages(interiorImageFolder, interiorImageNodes, null, SCIDs.TemplateIds.InteriorImage);
                    homeItem[SCFieldNames.PlanBaseFields.InteriorImages] = string.Join("|", interiorImages.Select(i => i.ID));
                }
            }

            homeItem.Editing.EndEdit();

            return homeItem;
        }

        private Item CreatePlan(XElement plan, Item masterPlanItem, Item communityItem)
        {
            var planName = plan.Element("PlanName").Value;
            var planItem = communityItem.Children
                .Where(i => i.Name == planName)
                .FirstOrDefault();
            if (planItem != null)
            {
                Log("Plan '{0}' on community '{1}' already exists", planName, communityItem.Name);
            }
            else
            {
                Log("Creating plan '{0}' on community '{1}'", planName, communityItem.Name);
                planItem = communityItem.Add(planName, new TemplateID(SCIDs.TemplateIds.PlanPage));
            }

            planItem.Editing.BeginEdit();

            planItem[SCFieldNames.PlanPageFields.MasterPlanID] = masterPlanItem.ID.ToString();

            foreach (Field masterField in masterPlanItem.Fields)
            {
                var planField = planItem.Fields[masterField.Name];
                if (planField == null)
                    continue;

                planField.Value = masterField.Value;
            }


            planItem[SCFieldNames.PlanPageFields.PlanStatus] = planItem[SCFieldNames.PlanBaseFields.StatusForSearch];
            switch (planItem[SCFieldNames.PlanPageFields.PlanStatus])
            {
                case SCIDs.StatusIds.Status.Active:
                    planItem.Appearance.Icon = masterPlanItem.Appearance.Icon;
                    planItem[SCFieldNames.PlanPageFields.StatusForAdvertising] = SCIDs.StatusIds.PlanAdvertisingStatus.Available;
                    break;
                case SCIDs.StatusIds.Status.InActive:
                    planItem[SCFieldNames.PlanPageFields.StatusForAdvertising] = SCIDs.StatusIds.PlanAdvertisingStatus.SoldOut;
                    break;
            }

            planItem[SCFieldNames.PlanPageFields.PricedFromValue] = plan.Element("BasePrice").Value;

            planItem.Editing.EndEdit();

            return planItem;
        }

        private Item CreateMasterPlan(XElement plan, Item division)
        {
            var masterPlanFolder = division.Children
                .Where(i => i.TemplateID == SCIDs.TemplateIds.MasterPlanFolder)
                .FirstOrDefault();

            if (masterPlanFolder == null)
            {
                Log("Creating master plans folder on division '{0}'", division.Name);
                masterPlanFolder = division.Add("Master Plans", new TemplateID(SCIDs.TemplateIds.MasterPlanFolder));
            }

            var planName = plan.Element("PlanName").Value;
            var subFolderName = planName.First().ToString().ToUpperInvariant();

            var masterPlanSubFolder = masterPlanFolder.Children
                .Where(i => i.Name == subFolderName)
                .FirstOrDefault();

            if (masterPlanSubFolder == null)
            {
                Log("Creating master plans subfolder '{0}'", subFolderName);
                masterPlanSubFolder = masterPlanFolder.Add(subFolderName, new TemplateID(SCIDs.TemplateIds.MasterPlanFolder));
            }

            var masterPlan = masterPlanSubFolder.Children
                .Where(i => i.Name == planName)
                .FirstOrDefault();

            if (masterPlan == null)
            {
                Log("Creating master plan '{0}'", planName);
                masterPlan = masterPlanSubFolder.Add(planName, new TemplateID(SCIDs.TemplateIds.MasterPlan));
            }
            else
            {
                Log("Master plan '{0}' already exists", planName);
            }

            masterPlan.Editing.BeginEdit();

            switch (plan.Element("PlanNotAvailable").Value)
            {
                case "0":
                    masterPlan[SCFieldNames.MasterPlan.Status] = SCIDs.StatusIds.Status.Active;
                    masterPlan[SCFieldNames.PlanBaseFields.StatusForSearch] = SCIDs.StatusIds.Status.Active;
                    masterPlan[SCFieldNames.PlanBaseFields.StatusForAssistance] = SCIDs.StatusIds.Status.Active;
                    break;
                case "1":
                    masterPlan[SCFieldNames.MasterPlan.Status] = SCIDs.StatusIds.Status.InActive;
                    masterPlan[SCFieldNames.PlanBaseFields.StatusForSearch] = SCIDs.StatusIds.Status.InActive;
                    masterPlan[SCFieldNames.PlanBaseFields.StatusForAssistance] = SCIDs.StatusIds.Status.InActive;
                    break;
                default:
                    Log("Unrecognized plan status {0}", plan.Element("PlanNotAvailable").Value);
                    throw new Exception("Unrecognized plan status");
            }

            switch (plan.Attribute("Type").Value)
            {
                case "SingleFamily":
                    masterPlan[SCFieldNames.MasterPlan.ProductType] = SCIDs.StatusIds.BHIProductType.SingleFamily;
                    masterPlan[SCFieldNames.MasterPlan.ProductDisplayType] = SCIDs.StatusIds.BHIProductDisplayType.SingleFamily;
                    break;
                case "MultiFamily":
                    masterPlan[SCFieldNames.MasterPlan.ProductType] = SCIDs.StatusIds.BHIProductType.MultiFamily;
                    masterPlan[SCFieldNames.MasterPlan.ProductDisplayType] = SCIDs.StatusIds.BHIProductDisplayType.Multiplex;
                    break;
                default:
                    Log("Unrecognized product type {0}", plan.Attribute("Type").Value);
                    throw new Exception("Unrecognized product type");
            }

            if (plan.Element("HalfBaths") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.HalfBathrooms] = plan.Element("HalfBaths").Value;
            }

            if (plan.Element("Garage").Attribute("Entry") != null)
            {
                switch (plan.Element("Garage").Attribute("Entry").Value)
                {
                    case "Side":
                        masterPlan[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Side;
                        break;
                    case "Front":
                        masterPlan[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Front;
                        break;
                    case "Rear":
                        masterPlan[SCFieldNames.PlanBaseFields.GarageEntry] = SCIDs.StatusIds.GarageEntryType.Rear;
                        break;
                    default:
                        Log("Unrecognized garage entry type {0}", plan.Element("Garage").Attribute("Entry").Value);
                        throw new Exception("Unrecognized garage entry type");
                }
            }

            var bedrooms = plan.Element("Bedrooms");
            if (bedrooms.Attribute("MasterBedLocation") != null)
            {
                switch (bedrooms.Attribute("MasterBedLocation").Value)
                {
                    case "Up":
                        masterPlan[SCFieldNames.PlanBaseFields.MasterBedLocation] = SCIDs.StatusIds.MasterBedroomLocation.Upstairs;
                        break;
                    case "Down":
                        masterPlan[SCFieldNames.PlanBaseFields.MasterBedLocation] = SCIDs.StatusIds.MasterBedroomLocation.Downstairs;
                        break;
                    default:
                        Log("Unrecognized master bed location {0}", plan.Element("Bedrooms").Attribute("MasterBedLocation").Value);
                        throw new Exception("Unrecognized master bed location");
                }
            }
            else
            {
                masterPlan[SCFieldNames.PlanBaseFields.MasterBedLocation] = SCIDs.StatusIds.MasterBedroomLocation.Upstairs;
            }

            var livingAreas = plan.Elements("LivingArea");
            foreach (var livingArea in livingAreas)
            {
                switch (livingArea.Attribute("Type").Value)
                {
                    case "LivingRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasLivingRoom] = "1";
                        break;
                    case "DiningRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasDiningRoom] = "1";
                        break;
                    case "FamilyRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasFamilyRoom] = "1";
                        break;
                    case "SunRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasSunRoom] = "1";
                        break;
                    case "Study":
                        masterPlan[SCFieldNames.PlanBaseFields.HasStudy] = "1";
                        break;
                    case "Loft":
                        masterPlan[SCFieldNames.PlanBaseFields.HasLoft] = "1";
                        break;
                    case "Office":
                        masterPlan[SCFieldNames.PlanBaseFields.HasOffice] = "1";
                        break;
                    case "GameRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasGameRoom] = "1";
                        break;
                    case "MediaRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasMediaRoom] = "1";
                        break;
                    case "GuestRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasGuestBedroom] = "1";
                        break;
                    case "BonusRoom":
                        masterPlan[SCFieldNames.PlanBaseFields.HasBonusRoom] = "1";
                        break;
                    default:
                        Log("Unrecognized living area {0}", livingArea.Attribute("Type").Value);
                        throw new Exception("Unrecognized living area");
                }
            }

            var planAmenity = plan.Element("PlanAmenity");
            if (planAmenity != null && planAmenity.Attribute("Type").Value == "Fireplaces")
            {
                masterPlan[SCFieldNames.PlanBaseFields.HasFireplace] = "1";
            }

            var imageSourceFolder = CreateMediaFolder(masterPlanSubFolder, planName);
            masterPlan[SCFieldNames.MasterPlan.ImageSource] = imageSourceFolder.Paths.Path;

            masterPlan[SCFieldNames.PlanBaseFields.PlanName] = planName;
            masterPlan[SCFieldNames.PlanBaseFields.PlanLegacyID] = plan.Element("PlanNumber").Value;
            masterPlan[SCFieldNames.PlanBaseFields.Bedrooms] = plan.Element("Bedrooms").Value;
            masterPlan[SCFieldNames.PlanBaseFields.Bathrooms] = plan.Element("Baths").Value;
            masterPlan[SCFieldNames.PlanBaseFields.Garages] = plan.Element("Garage").Value;
            masterPlan[SCFieldNames.PlanBaseFields.LivingAreas] = livingAreas.Count().ToString();

            if (plan.Element("Description") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.Description] = plan.Element("Description").Value.Trim();
            }
            if (plan.Element("Stories") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.Stories] = plan.Element("Stories").Value;
            }
            if (plan.Element("BaseSqft") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.Sqft] = plan.Element("BaseSqft").Value;
            }
            if (plan.Element("Basement") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.HasBasement] = plan.Element("Basement").Value;
            }
            if (plan.Element("DiningAreas") != null)
            {
                masterPlan[SCFieldNames.PlanBaseFields.DiningAreas] = plan.Element("DiningAreas").Value;
            }

            Log("Loading master plan '{0}' images", planName);

            var planImages = plan.Element("PlanImages");
            if (planImages != null)
            {
                var elevationImageNodes = planImages.Elements("ElevationImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (elevationImageNodes.Count() > 0)
                {
                    var elevationImageFolder = CreateMediaFolder(masterPlan, "Images/Elevation");
                    var elevationImages = CreateImages(elevationImageFolder, elevationImageNodes, null, SCIDs.TemplateIds.ElevationImage);
                    masterPlan[SCFieldNames.PlanBaseFields.ElevationImages] = string.Join("|", elevationImages.Select(i => i.ID));
                }

                var floorPlanImageNodes = planImages.Elements("FloorPlanImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (floorPlanImageNodes.Count() > 0)
                {
                    var floorPlanImageFolder = CreateMediaFolder(masterPlan, "Images/FloorPlan");
                    var floorPlanImages = CreateImages(floorPlanImageFolder, floorPlanImageNodes, null, SCIDs.TemplateIds.FloorPlanImage);
                    masterPlan[SCFieldNames.PlanBaseFields.FloorPlanImages] = string.Join("|", floorPlanImages.Select(i => i.ID));
                }

                var interiorImageNodes = planImages.Elements("InteriorImage")
                    .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                    .ToList();
                if (interiorImageNodes.Count() > 0)
                {
                    var interiorImageFolder = CreateMediaFolder(masterPlan, "Images/Interior");
                    var interiorImages = CreateImages(interiorImageFolder, interiorImageNodes, null, SCIDs.TemplateIds.InteriorImage);
                    masterPlan[SCFieldNames.PlanBaseFields.InteriorImages] = string.Join("|", interiorImages.Select(i => i.ID));
                }
            }

            masterPlan.Editing.EndEdit();

            if (planImages != null)
            {
                var virtualTourNode = planImages.Element("VirtualTour");
                if (virtualTourNode != null)
                {
                    var virtualToursFolder = masterPlan.Add("Virtual Tours", new TemplateID(SCIDs.TemplateIds.VirtualToursFolder));
                    var virtualTourName = GetValidName(Path.GetFileNameWithoutExtension(virtualTourNode.Value.Split('/').Last()));
                    Log("Loading master plan '{0}' virtual tour '{1}'", planName, virtualTourName);
                    var virtualTour = virtualToursFolder.Add(virtualTourName, new TemplateID(SCIDs.TemplateIds.VirtualTour));

                    virtualTour.Editing.BeginEdit();

                    LinkField linkField = virtualTour.Fields[SCFieldNames.VirtualTour.Url];
                    linkField.Clear();
                    linkField.LinkType = "external";
                    linkField.Url = virtualTourNode.Value;

                    virtualTour[SCFieldNames.VirtualTour.InternalNote] = virtualTourName;

                    virtualTour.Editing.EndEdit();

                    masterPlan.Editing.BeginEdit();
                    masterPlan.Fields[SCFieldNames.PlanBaseFields.VirtualToursMasterPlanID].Value = virtualTour.ID.ToString();
                    masterPlan.Editing.EndEdit();
                }
            }

            return masterPlan;
        }

        private void CreateCommunitySchools(XElement community, Item communityItem)
        {
            var schoolsNode = community.Element("Schools");
            if (schoolsNode == null)
                return;

            var schoolNodes = schoolsNode.Elements().ToList();
            if (schoolNodes.Count <= 1)
                return;

            var schoolsItem = communityItem.Children
                .Where(i => i.Name == "Schools")
                .FirstOrDefault();
            if (schoolsItem != null)
            {
                Log("Schools for community '{0}' already exist", communityItem.Name);
            }
            else
            {
                Log("Creating schools for community '{0}'", communityItem.Name);
                schoolsItem = communityItem.Add("Schools", new TemplateID(SCIDs.TemplateIds.SchoolList));
            }

            foreach (var schoolNode in schoolNodes)
            {
                var schoolName = schoolNode.Value.Trim().Replace(".","");
                var type = SCIDs.SchoolType.Elementary;

                switch (schoolNode.Name.ToString())
                {
                    case "DistrictName":
                        continue;
                    case "Elementary":
                        type = SCIDs.SchoolType.Elementary;
                        break;
                    case "Middle":
                        type = SCIDs.SchoolType.Middle;
                        break;
                    case "High":
                        type = SCIDs.SchoolType.High;
                        break;
                    default:
                        Log("Unrecognized school type '{0}'", schoolNode.Name);
                        throw new Exception("Unrecognized school type");
                }

                var schoolItem = schoolsItem.Children
                    .Where(i => i.Name == schoolName)
                    .FirstOrDefault();
                if (schoolItem != null)
                {
                    Log("School '{0}' already exist", schoolName);
                }
                else
                {
                    Log("Creating school '{0}'", schoolName);
                    schoolItem = schoolsItem.Add(schoolName, new TemplateID(SCIDs.TemplateIds.School));
                }
                
                schoolItem.Editing.BeginEdit();

                schoolItem[SCFieldNames.School.Name] = schoolName;
                if (schoolNode.Attribute("NCESID") != null)
                {
                    schoolItem[SCFieldNames.School.SourceID] = schoolNode.Attribute("NCESID").Value;
                }

                schoolItem[SCFieldNames.School.Type] = type.ToString();
                schoolItem.Editing.EndEdit();
            }
        }

        private Item CreateCommunity(XElement community, Item city, Item schoolDistrict)
        {
            var name = community.Element("SubdivisionName").Value;
            var communityItem = city.Children
                .Where(i => i.Name == name + " Community")
                .FirstOrDefault();
            if (communityItem != null)
            {
                Log("Community '{0}' already exists", name);
            }
            else
            {
                Log("Creating community '{0}'", name);
                communityItem = city.Add(name, new TemplateID(SCIDs.TemplateIds.CommunityPage));
            }

            var statusText = "Active";
            if (community.Attribute("Status") != null)
            {
                statusText = community.Attribute("Status").Value;
            }
            
            var status = SCIDs.StatusIds.CommunityStatus.Open;
            var statusForSearch = SCIDs.StatusIds.Status.Active;
            var statusForAssistance = SCIDs.StatusIds.Status.Active;

            switch (statusText)
            {
                case "Closed":
                    status = SCIDs.StatusIds.CommunityStatus.Closed;
                    statusForSearch = SCIDs.StatusIds.Status.InActive;
                    break;
                case "Closeout":
                    status = SCIDs.StatusIds.CommunityStatus.Closeout;
                    break;
                case "ComingSoon":
                    status = SCIDs.StatusIds.CommunityStatus.ComingSoon;
                    break;
                case "GrandOpening":
                    status = SCIDs.StatusIds.CommunityStatus.GrandOpening;
                    break;
                case "Inactive":
                    status = SCIDs.StatusIds.CommunityStatus.InActive;
                    statusForSearch = SCIDs.StatusIds.Status.InActive;
                    statusForAssistance = SCIDs.StatusIds.Status.InActive;
                    break;
                case "Active":
                    status = SCIDs.StatusIds.CommunityStatus.Open;
                    break;
                case "PreSelling":
                    status = SCIDs.StatusIds.CommunityStatus.PreSelling;
                    break;
                default:
                    Log("Unrecognized community status {0}", statusText);
                    throw new Exception("Unrecognized community status");
            }

            communityItem.Editing.BeginEdit();

            communityItem[SCFieldNames.CommunityFields.CommunityStatus] = status.ToString();
            communityItem[SCFieldNames.CommunityFields.CommunityStatusChangeDate] = DateTime.Now.ToString("yyyyMMddTHHmmss");
            communityItem[SCFieldNames.CommunityFields.CommunityName] = name;
            communityItem[SCFieldNames.CommunityFields.StatusforSearch] = statusForSearch;
            communityItem[SCFieldNames.CommunityFields.StatusforAssistance] = statusForAssistance;

            var communityStyles = community.Elements("CommunityStyle");
            foreach (var communityStyle in communityStyles)
            {
                switch (communityStyle.Value)
                {
                    case "Gated":
                        communityItem[SCFieldNames.CommunityFields.IsGated] = "1";
                        break;
                    case "MasterPlanned":
                        communityItem[SCFieldNames.CommunityFields.IsMasterPlanned] = "1";
                        break;
                    case "ActiveAdult":
                        communityItem[SCFieldNames.CommunityFields.IsActiveAdult] = "1";
                        break;
                    case "CondoOnly":
                        communityItem[SCFieldNames.CommunityFields.IsCondoOnly] = "1";
                        break;
                    default:
                        Log("Unrecognized community style {0}", communityStyle.Value);
                        throw new Exception("Unrecognized community style");
                }
            }

            var ammenities = community.Elements("SubAmenity");
            foreach (var ammenity in ammenities)
            {
                switch (ammenity.Attribute("Type").Value)
                {
                    case "Playground":
                        communityItem[SCFieldNames.CommunityFields.HasPlayground] = "1";
                        break;
                    case "Pool":
                        communityItem[SCFieldNames.CommunityFields.HasPool] = "1";
                        break;
                    case "Trails":
                        communityItem[SCFieldNames.CommunityFields.HasTrails] = "1";
                        break;
                    case "Views":
                        communityItem[SCFieldNames.CommunityFields.HasView] = "1";
                        break;
                    case "Park":
                        communityItem[SCFieldNames.CommunityFields.HasPark] = "1";
                        break;
                    case "Greenbelt":
                        communityItem[SCFieldNames.CommunityFields.HasGreenbelt] = "1";
                        break;
                    case "Lake":
                        communityItem[SCFieldNames.CommunityFields.HasLake] = "1";
                        break;
                    case "Volleyball":
                        communityItem[SCFieldNames.CommunityFields.HasVolleyball] = "1";
                        break;
                    case "Pond":
                        communityItem[SCFieldNames.CommunityFields.HasPond] = "1";
                        break;
                    case "Basketball":
                        communityItem[SCFieldNames.CommunityFields.HasBasketball] = "1";
                        break;
                    case "Baseball":
                        communityItem[SCFieldNames.CommunityFields.HasBaseball] = "1";
                        break;
                    case "Clubhouse":
                        communityItem[SCFieldNames.CommunityFields.HasClubhouse] = "1";
                        break;
                    case "Tennis":
                        communityItem[SCFieldNames.CommunityFields.HasTennis] = "1";
                        break;
                    case "GolfCourse":
                        communityItem[SCFieldNames.CommunityFields.HasGolfCourse] = "1";
                        break;
                    case "Soccer":
                        communityItem[SCFieldNames.CommunityFields.HasSoccer] = "1";
                        break;
                    case "Marina":
                        communityItem[SCFieldNames.CommunityFields.HasMarina] = "1";
                        break;
                    case "Beach":
                        communityItem[SCFieldNames.CommunityFields.HasBeach] = "1";
                        break;
                    case "Waterfront":
                        communityItem[SCFieldNames.CommunityFields.IsWaterfront] = "1";
                        break;
                    default:
                        Log("Unrecognized ammenity {0}", ammenity.Attribute("Type").Value);
                        throw new Exception("Unrecognized ammenity");
                }
            }

            var salesOfficeNode = community.Element("SalesOffice");
            if (salesOfficeNode != null)
            {
                var addressNode = salesOfficeNode.Element("Address");
                if (addressNode.Element("Street1") != null)
                {
                    communityItem[SCFieldNames.CommunityFields.StreetAddress1] = addressNode.Element("Street1").Value;
                }
                communityItem[SCFieldNames.CommunityFields.Zip] = addressNode.Element("ZIP").Value;

                var geocodeNode = addressNode.Element("Geocode");
                if (geocodeNode != null)
                {
                    communityItem[SCFieldNames.CommunityFields.Longitude] = geocodeNode.Element("Longitude").Value;
                    communityItem[SCFieldNames.CommunityFields.Latitude] = geocodeNode.Element("Latitude").Value;
                }

                var phoneNode = salesOfficeNode.Element("Phone");
                if (phoneNode != null)
                {
                    communityItem[SCFieldNames.CommunityFields.Phone] =
                        phoneNode.Element("AreaCode").Value
                        + "-" + phoneNode.Element("Prefix").Value
                        + "-" + phoneNode.Element("Suffix").Value;
                }

                var faxNode = salesOfficeNode.Element("Fax");
                if (faxNode != null)
                {
                    communityItem[SCFieldNames.CommunityFields.Fax] =
                        faxNode.Element("F_AreaCode").Value
                        + "-" + faxNode.Element("F_Prefix").Value
                        + "-" + faxNode.Element("F_Suffix").Value;
                }

                var emailNode = salesOfficeNode.Element("Email");
                if (emailNode != null)
                {
                    communityItem[SCFieldNames.CommunityFields.Email] = emailNode.Value;
                }

                var hoursNode = salesOfficeNode.Element("Hours");
                if (hoursNode != null)
                {
                    communityItem[SCFieldNames.CommunityFields.UseOfficeHoursText] = "1";
                    communityItem[SCFieldNames.CommunityFields.OfficeHoursText] = hoursNode.Value;
                }
            }

            if (schoolDistrict != null)
            {
                communityItem[SCFieldNames.CommunityFields.SchoolDistrict] = schoolDistrict.ID.ToString();
            }

            communityItem[SCFieldNames.CommunityFields.Description] = community.Element("SubDescription").Value;

            var imageSourceFolder = CreateMediaFolder(city, communityItem.Name);
            communityItem[SCFieldNames.CommunityFields.ImageSource] = imageSourceFolder.Paths.Path;

            communityItem.Editing.EndEdit();

            Log("Community '{0}' created", name);

            var slideShowImageNodes = community.Elements("SubImage")
                .Where(i => i.Attribute("Type") == null 
                    || i.Attribute("Type").Value == "Standard")
                .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                .ToList();

            var siteplanImageNodes = community.Elements("SubImage")
                .Where(i => i.Attribute("Type") != null
                    && i.Attribute("Type").Value == "LotMap")
                .OrderBy(i => int.Parse(i.Attribute("SequencePosition").Value))
                .ToList();

            Log("Loading community '{0}' images", name);
            var slideShowImageFolder = CreateMediaFolder(city, communityItem.Name + "/Images/Slideshow");
            var assetsImageFolder = CreateMediaFolder(city, communityItem.Name + "/Assets");
            var slideshowImages = CreateImages(slideShowImageFolder, slideShowImageNodes);
            var siteplanImages = CreateImages(assetsImageFolder, siteplanImageNodes, "SitePlan.jpg");

            communityItem.Editing.BeginEdit();

            communityItem[SCFieldNames.CommunityFields.SlideShowImage] = string.Join("|", slideshowImages.Select(i => i.ID));

            if (siteplanImages.Count > 0)
            {
                AssignMediaItem(communityItem.Fields[SCFieldNames.CommunityFields.SiteplanImage], siteplanImages[0]);
            }
            communityItem.Editing.EndEdit();

            return communityItem;
        }

        private Item CreateSchoolDistrict(XElement community, Item divisionItem)
        {
            var schoolNode = community.Element("Schools");
            if (schoolNode == null)
                return null;

            var districtNode = schoolNode.Element("DistrictName");
            if (districtNode == null)
                return null;

            var schoolDistrictsItem = divisionItem.Children
                .Where(i => i.TemplateID == SCIDs.TemplateIds.SchoolDistricts)
                .FirstOrDefault();

            if (schoolDistrictsItem == null)
            {
                Log("Creating school district nodes for division '{0}'", divisionItem.Name);
                schoolDistrictsItem = divisionItem.Add("School Districts", new TemplateID(SCIDs.TemplateIds.SchoolDistricts));
            }

            var schoolDistrictName = districtNode.Value.Trim();
            var schoolDistrictItem = schoolDistrictsItem.Children
                .Where(i => i.Name == schoolDistrictName)
                .FirstOrDefault();

            if (schoolDistrictItem != null)
            {
                Log("School district '{0}' already exists", schoolDistrictName);
            }
            else
            {
                Log("Creating school district '{0}'", schoolDistrictName);
                schoolDistrictItem = schoolDistrictsItem.Add(schoolDistrictName, new TemplateID(SCIDs.TemplateIds.SchoolDistrict));
            }

            schoolDistrictItem.Editing.BeginEdit();

            schoolDistrictItem[SCFieldNames.SchoolDistrict.Name] = schoolDistrictName;
            if (districtNode.Attribute("LEAID") != null)
            {
                schoolDistrictItem[SCFieldNames.SchoolDistrict.SchoolDistrictID] = districtNode.Attribute("LEAID").Value;
            }
            schoolDistrictItem.Editing.EndEdit();
            return schoolDistrictItem;
        }

        private Item CreateCity(XElement community, Item division)
        {
            var name = GetCityNameFromCommunity(community);

            var city = division.Children
                .Where(i => i["City Name"] == name)
                .FirstOrDefault();

            if (city != null)
            {
                Log("City '{0}' already exists", name);
            }
            else
            {
                Log("Creating city: '{0}'", name);
                city = division.Add(name, new TemplateID(SCIDs.TemplateIds.CityPage));
            }

            city.Editing.BeginEdit();

            city[SCFieldNames.City.Name] = name;
            city[SCFieldNames.City.StatusForSearch] = SCIDs.StatusIds.Status.Active;
            city[SCFieldNames.City.StatusForAssistance] = SCIDs.StatusIds.Status.Active;

            city.Editing.EndEdit();
            return city;
        }
        #endregion

        #region Database Utils
        private Item GetItemByID(string ID)
        {
            return _masterDB.GetItem(new ID(ID));
        }

        private Item GetItemByPath(string path)
        {
            return _masterDB.GetItem(path);
        }

        private TemplateItem GetTemplateByID(ID templateId)
        {
            return _masterDB.GetTemplate(templateId);
        }
        #endregion

        #region Image Utils
        private Stream DownloadImage(string url)
        {
            try
            {
                byte[] imageData;
                using (var client = new WebClient())
                {
                    imageData = client.DownloadData(url);
                }

                return new MemoryStream(imageData);
            }
            catch (Exception e)
            {
                Log("Could not download image {0}, Error {1}", url, e.ToString());
            }
            return null;
        }

        private void AssignMediaItem(ImageField field, Item mediaItem)
        {
            field.MediaID = mediaItem.ID;
            field.SetAttribute("mediapath", mediaItem.Paths.MediaPath);
            field.SetAttribute("showineditor", "1");
        }

        private Item CreateMediaFolder(Item parent, string itemName)
        {
            var imageFolderPath = parent.Paths.Path
                .Replace("/content/", "/media library/")
                .Replace("/Home/", "/") + "/" + itemName;

            var paths = imageFolderPath.Split('/');
            var currentImagePath = string.Join("/", paths[0], paths[1], paths[2], paths[3]);
            var currentItemPath = string.Join("/", paths[0], paths[1], "content", paths[3], "Home");
            var parentMediaItem = GetItemByPath(currentImagePath);
            if (parentMediaItem == null)
            {
                Log("Could not create Media folder starting at {0}", currentImagePath);
                throw new Exception("Could not create Media folder");
            }

            for (int i = 4; i < paths.Length; ++i)
            {
                currentImagePath = string.Join("/", currentImagePath, paths[i]);
                currentItemPath = string.Join("/", currentItemPath, paths[i]);
                var folderItem = GetItemByPath(currentImagePath);
                if (folderItem == null)
                {
                    Log("Creating media folder: {0}", currentImagePath);
                    folderItem = parentMediaItem.Add(paths[i], new TemplateID(SCIDs.TemplateIds.CustomMediaFolder));

                    var item = GetItemByPath(currentItemPath);
                    if (item != null)
                    {
                        folderItem.Editing.BeginEdit();
                        folderItem.Appearance.Icon = item.Appearance.Icon;
                        folderItem.Editing.EndEdit();
                    }
                }

                parentMediaItem = folderItem;
            }
            return parentMediaItem;
        }

        private List<MediaItem> CreateImages(Item imageFolder, List<XElement> imageNodes, string imageName = null, ID templateId = null)
        {
            List<MediaItem> images = new List<MediaItem>();
            foreach (var imageNode in imageNodes)
            {
                var url = imageNode.Value;
                var name = imageName ?? url.Split('/').Last();
                if (name.Contains("?"))
                {
                    name = name.Split('?').First();
                }
                var nameWithoutExtension = Path.GetFileNameWithoutExtension(name);
                if (nameWithoutExtension.Contains('.'))
                {
                    var noPointName = nameWithoutExtension.Replace('.','-');
                    name = name.Replace(nameWithoutExtension, noPointName);
                }
                var destination = string.Format("{0}/{1}",
                    imageFolder.Paths.Path, Path.GetFileNameWithoutExtension(name));

                var item = GetItemByPath(destination);
                if (item != null)
                {
                    Log("Image '{0}' on '{1}' already exists", name, imageFolder.Name);
                    continue;
                }

                Log("Creating image '{0}' on '{1}'", name, imageFolder.Name);

                var title = "Default title";
                if (imageNode.Attribute("Title") != null)
                {
                    title = imageNode.Attribute("Title").Value;
                }

                string caption = null;
                if (imageNode.Attribute("Caption") != null)
                {
                    caption = imageNode.Attribute("Caption").Value;
                }

                var options = new MediaCreatorOptions();
                options.Database = _masterDB;
                options.Language = Sitecore.Context.Language;
                options.Versioned = false;
                options.Destination = destination;
                options.FileBased = Settings.Media.UploadAsFiles;

                var creator = new MediaCreator();
                MediaItem mediaItem;
                using (var stream = DownloadImage(url))
                {
                    if (stream == null)
                        continue;
                    mediaItem = creator.CreateFromStream(stream, name, options);
                }

                mediaItem.BeginEdit();
                mediaItem.Alt = caption ?? title;
                mediaItem.EndEdit();

                if (templateId != (ID)null)
                {
                    mediaItem.InnerItem.ChangeTemplate(GetTemplateByID(templateId));
                }

                images.Add(mediaItem);
            }
            return images;
        }
        #endregion

        #region Utils
        private string GetCityNameFromCommunity(XElement community)
        {
            var salesOffice = community.Element("SalesOffice");
            if (salesOffice != null)
            {
                var address = salesOffice.Element("Address");
                if (address != null)
                {
                    var city = address.Element("City");
                    if (city != null && !String.IsNullOrEmpty(city.Value))
                    {
                        return city.Value;
                    }
                }
            }
            return "Unknown City";
        }

        private byte[] ReadPostedFile(HttpPostedFile httpPostedFile)
        {
            var data = new byte[httpPostedFile.ContentLength];
            httpPostedFile.InputStream.Read(data, 0, data.Length);

            return data;
        }

        private bool ValidateXml(byte[] data, string xsdPath, bool showWarnings)
        {
            var isValid = true;
            var maxErrors = 100;
            var errorCount = 0;
            Log("Validating xml");
            try
            {
                XmlSchema schema;
                using (StreamReader sr = new StreamReader(xsdPath))
                {
                    schema = XmlSchema.Read(sr, null);
                }

                var readerSettings = new XmlReaderSettings();
                readerSettings.ValidationType = ValidationType.Schema;
                readerSettings.Schemas.Add(schema);
                readerSettings.ConformanceLevel = ConformanceLevel.Document;
                readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                readerSettings.ValidationFlags |= XmlSchemaValidationFlags.AllowXmlAttributes;

                readerSettings.ValidationEventHandler += new ValidationEventHandler((sender, args) =>
                {
                    if (args.Message.Contains("attribute is not declared")
                        || args.Severity == XmlSeverityType.Warning)
                    {
                        if (showWarnings)
                        {
                            Log("Warning: {0} (Line,Position): ({1},{2})",
                                args.Exception.Message,
                                args.Exception.LineNumber,
                                args.Exception.LinePosition);
                        }
                        return;
                    }
                    isValid = false;
                    ++errorCount;
                    if (errorCount >= maxErrors)
                    {
                        Log("Maximum amount of errors reached.");
                    }
                    else
                    {
                        Log("Error: {0} (Line,Position): ({1},{2})",
                            args.Exception.Message,
                            args.Exception.LineNumber,
                            args.Exception.LinePosition);
                    }
                });

                using (XmlReader reader = XmlReader.Create(new MemoryStream(data), readerSettings))
                {
                    while (reader.Read() && errorCount <= maxErrors) ;
                }
            }
            catch (Exception e)
            {
                isValid = false;
                Log("Error: {0}", e.ToString());
            }

            return isValid;
        }

        private List<Item> GetDivisions()
        {
            List<Item> divisions;
            using (var context = ContentSearchManager.GetIndex(string.Format("sitecore_{0}_index", _masterDB.Name)).CreateSearchContext())
            {
                var allDivisions = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.TemplateIds.DivisionPage
                        && i.IsLatestVersion && !i.IsTemplate
                        && i.Language == "en" && i.Name != "__Standard Values")
                    .Select(i => i.GetItem()).ToList();

                divisions = allDivisions
                    .OrderBy(i => i.Paths.Path).ToList();
            }

            return divisions;
        }

        private void Log(string format, params object[] values)
        {
            if (TaskHolder._log == null)
                TaskHolder._log = new StringBuilder();
            TaskHolder._log.AppendFormat(format, values);
            TaskHolder._log.AppendLine();
        }

        private string GetLog()
        {
            if (TaskHolder._log == null)
                return string.Empty;
            return HttpUtility.HtmlEncode(TaskHolder._log.ToString()).Replace(Environment.NewLine, "<br/>");
        }

        private string GetValidName(string itemName)
        {
            Regex r = new Regex("(?:[^a-z0-9- ]|)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

            itemName = itemName.Replace("&amp;", "and").Replace(".", " ")
                .Replace("/", " ").Replace(@"\", " ").Replace("&", " and ")
                .Replace("(", " ").Replace(")", " ").Replace("-", " ")
                .Replace(",", " ").Replace("[", " ").Replace("]", " ")
                .Replace(";", " ").Replace(@"""", " ").Replace("|", " ")
                .Replace("+", " plus ").Replace("~", "-").Replace("*", " ")
                .Replace(":", " ").Replace("%", " percent ")
                .Replace("!", " ").Replace("?", " ");
            itemName = r.Replace(itemName, String.Empty);
            itemName = itemName.Replace("  ", " ");
            itemName = itemName.Replace("  ", " ");
            itemName = itemName.Replace("  ", " ");
            itemName = itemName.Trim();
            return itemName;
        }

        private IEnumerable<XElement> GetElements(XmlReader reader, string nodeName)
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == nodeName)
                {
                    yield return XElement.ReadFrom(reader) as XElement;
                }
            }
        }
        #endregion
    }

    public static class TaskHolder
    {
        public static object _lock = new object();
        public static StringBuilder _log;
        public static Task _task;
    }
}