﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportSOSDIDs.aspx.cs" Inherits="TM.Web.Custom.admin.ImportSOSDIDs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        body {
            font-size: 12px;
            font-family: Calibri;
        }

.easy {
font-size: 18px;
border-radius: 100px;
width: 264px;
height: 234px;
background: url(http://retireforlessincostarica.com/wp-content/uploads/2013/10/easy-button.jpg) no-repeat;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <%--   Click button below to start importing SO and SD IDs from CRM_CommunityParams table to all communities.<br/><br/>

        Specify root path (e.g. /sitecore/content/TaylorMorrison/Home/New Homes/Arizona/Phoenix/Scottsdale) <b>OR</b> leave the field as blank for all communities:<br/><br/>  --%>
	 <asp:TextBox id="txtRootPath" runat="server" width="400px" visible="false"/>
       <asp:Button ID="btnImport" runat="server" Text="Easy" onclick="btnImport_OnClick" class="easy"/>
        <br/>
	
	
	

        <br/>
    <asp:Literal ID="litReport" runat="server"></asp:Literal>
    </div>
    
    <asp:HiddenField id="hdnProcessedItems" runat="server" />
    
      <asp:Button ID="btnPublish" runat="server" Text="Would you like to publish these homes?" onclick="btnPublish_OnClick" />

    </form>
</body>
</html>
