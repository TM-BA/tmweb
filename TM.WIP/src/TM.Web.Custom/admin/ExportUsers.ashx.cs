﻿using System.Web;
using System.Web.Profile;
using System.Web.Security;
using Sitecore.Data;
using TM.Domain;
using TM.Web.Custom.SCHelpers;


namespace TM.Web.Custom.admin
{
    /// <summary>
    /// Summary description for ExportUsers
    /// </summary>
    public class ExportUsers : IHttpHandler
    {
        private static Database _masterDatabase = Sitecore.Configuration.Factory.GetDatabase("master");

        public void ProcessRequest(HttpContext context)
        {
            context.Server.ScriptTimeout = 3000;
            context.Response.Buffer = true;
            context.Response.Clear();
            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.AddHeader("content-disposition", @"attachment;filename=""TMUserExport.csv""");
            context.Response.ContentType = "text/plain";
            MembershipUserCollection users = Membership.GetAllUsers();
            context.Response.Write(
                "First Name, Last Name, Email, Area of Interest, Site ,Last Activity,Signup Date");
            foreach (MembershipUser user in users)
            {
                string username = user.UserName;
                Sitecore.Security.Accounts.User scUser = Sitecore.Security.Accounts.User.FromName(username,false);

                if (scUser!=null && scUser.Domain!=null && scUser.Domain.Name == "extranet")
                {
                    var tmUser = scUser.HydrateToTMUser(TMUserDomain.TaylorMorrison);//all enum in TMUserDomain are extranet 

                    context.Response.Write("\r\n");
                    context.Response.Write(tmUser.FirstName + ",");
                    context.Response.Write(tmUser.LastName + ",");
                    context.Response.Write(tmUser.Email + ",");
                    var divisionID = tmUser.AreaOfInterest;
                    var divisionName = ID.IsID(divisionID)
                        ? GetDivisionName(new ID(divisionID))
                        : divisionID;

                    context.Response.Write(divisionName + ",");
                    context.Response.Write(tmUser.Device + ",");
                    context.Response.Write(user.LastActivityDate + ",");
                    context.Response.Write(user.CreationDate);
                }
            }
        }

        private string GetDivisionName(ID id)
        {
            var item = _masterDatabase.GetItem(id);
            if (item != null)
            {
                return item.GetItemName();
            }
            return id.ToString();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

   

 }