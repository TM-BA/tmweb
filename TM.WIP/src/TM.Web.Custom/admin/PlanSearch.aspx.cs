﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.Queries;
using TM.Web.Custom.SCHelpers;
using TM.Web.Custom.SCHelpers.Search;

namespace TM.Domain
{
	public class PlanSearchCriteria
	{
		private string _marketID;
		private int _bedMin;
		private int _bedMax;
		private int _bathMin;
		private int _bathMax;
		private int _halfBathMin;
		private int _halfBathMax;
		private int _story;
		private string _selectedPlantype;
		private int _sqFtMin;
		private int _sqFtMax;

		public PlanSearchCriteria(string marketID, int bedMin, int bedMax, int bathMin, int bathMax, int halfBathMin, int halfBathMax, int story, string selectedPlantype, int sqFtMin, int sqFtMax)
		{
			_marketID = marketID;
			_bedMin = bedMin;
			_bedMax = bedMax;
			_bathMin = bathMin;
			_bathMax = bathMax;
			_halfBathMin = halfBathMin;
			_halfBathMax = halfBathMax;
			_story = story;
			_selectedPlantype = selectedPlantype;
			_sqFtMin = sqFtMin;
			_sqFtMax = sqFtMax;
		}

		public int SqFtMax
		{
			get { return _sqFtMax; }
		}

		public int SqFtMin
		{
			get { return _sqFtMin; }
		}

		public string MarketID
		{
			get { return _marketID; }
		}

		public int BedMin
		{
			get { return _bedMin; }
		}

		public int BedMax
		{
			get { return _bedMax; }
		}

		public int BathMin
		{
			get { return _bathMin; }
		}

		public int BathMax
		{
			get { return _bathMax; }
		}

		public int HalfBathMin
		{
			get { return _halfBathMin; }
		}

		public int HalfBathMax
		{
			get { return _halfBathMax; }
		}

		public int Story
		{
			get { return _story; }
		}

		public string SelectedPlantype
		{
			get { return _selectedPlantype; }
		}
	}

	public class PlanSearchResults
	{
		public string Division { get; set; }
		public string Community { get; set; }
		public string Subcommunity { get; set; }
		public string PlanName { get; set; }
		public string PlanType { get; set; }
		public string SqFt { get; set; }
		public string Width { get; set; }
		public string Depth { get; set; }
		public string Stories { get; set; }
		public string Bed { get; set; }
		public string Bath { get; set; }
		public string HalfBath { get; set; }
		public string Garage { get; set; }

	}
}

namespace TM.Web.Custom.admin
{
	public partial class PlanSearch : BasePage
	{
		private ListItemCollection _bedsBathsHalfBaths = new ListItemCollection
	                                                     {
							     new ListItem("Any","0"),
							     new ListItem("1","1"),
							     new ListItem("2","2"),
							     new ListItem("3","3"),
							     new ListItem("4","4"),
							     new ListItem("5","5"),
							     new ListItem("6","6")
	                                                     };

		private ListItemCollection _stories = new ListItemCollection
                                                  {
						      new ListItem("Any","0"),
                                                      new ListItem("1", "1"),
                                                      new ListItem("2", "2"),
                                                      new ListItem("3", "3")
                                                  };


		private ListItemCollection _planType = new ListItemCollection()
                                                   {
                                                       new ListItem("Either", "0"),
                                                       new ListItem("Single Family", "1"),
                                                       new ListItem("Multi Family", "2")
                                                   };

	    private Database _DB = Sitecore.Context.Database;

		protected void Page_Init(object sender, EventArgs e)
		{
			btnSearch.Click += OnSearchButtonClicked;
		}


		protected void Page_Load(object sender, EventArgs e)
		{

	    // for running after initial import
		 /*   FindPlanAndRunInheritanceRules();

		    return;*/

			if (!Page.IsPostBack)
			{

				ddlBedMin.DataSource = _bedsBathsHalfBaths;
				ddlBedMax.DataSource = _bedsBathsHalfBaths;
				ddlBathMin.DataSource = _bedsBathsHalfBaths;
				ddlBathMax.DataSource = _bedsBathsHalfBaths;
				ddlHalfBathMin.DataSource = _bedsBathsHalfBaths;
				ddlHalfBathMax.DataSource = _bedsBathsHalfBaths;
				ddlPlanType.DataSource = _planType;
				ddlStories.DataSource = _stories;
				GetMarketList();
				this.DataBind();
			}
		}

        private readonly Sitecore.Data.Database masterDB = Sitecore.Configuration.Factory.GetDatabase("master");

	private void FindPlanAndRunInheritanceRules()
	    {
		   
	    	    DataSource dataSource = masterDB.DataManager.DataSource;

                RunInheritance(new ID("{0DE95AE4-41AB-4D01-9EB0-67441B7C2450}"), dataSource);
               
              


	          CacheManager.ClearAllCaches();
	    }

        public void RunInheritance(ID itemId, DataSource dataSource)
        {
            IDList childIds = dataSource.GetChildIDs(itemId);

            foreach (ID childId in childIds)
            {
              
                RunInheritance(childId, dataSource);
            }

            var item = masterDB.GetItem(itemId);

           // dataSource.DeleteItem(itemId);
            
            var templateID = item.TemplateID;

	    if(templateID == SCIDs.TemplateIds.HomeForSalePage || templateID == SCIDs.TemplateIds.PlanPage)
	    {

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                using (new EventDisabler())
                using (new EditContext(item))
                {
                        if (item["Virtual Tours Master Plan ID"]!=string.Empty)
                            item["Virtual Tour Status"] = "{5B3598C0-B87C-41E5-87A8-388CAA114715}";
                }
            }

		
	    }
        }


	private void RenameItem(Item item)
	{
	    var inventoryItem = item;
	    var prefix = "Home Available Now at";
	    var streetAddress = inventoryItem[SCIDs.HomesForSalesFields.StreetAddress1];
        var nonAlpha = new Regex(@"\W");
        streetAddress = nonAlpha.Replace(streetAddress, " ");
        var multiSpace = new Regex(@"\s{2,}");//multispace
        streetAddress = multiSpace.Replace(streetAddress, " ");
	    var zipCode = inventoryItem.Parent.Parent["Zip or Postal Code"];
	    string newName = string.Format("{0} {1} {2}", prefix, streetAddress, zipCode);

	    if (zipCode.IsNotEmpty() && streetAddress.IsNotEmpty() && inventoryItem.Name != newName)
	    {
	        if (item.Template.StandardValues != null
	            && item.ID == item.Template.StandardValues.ID)
	        {
	            return;
	        }

	        foreach (Sitecore.Web.SiteInfo site in Sitecore.Configuration.Factory.GetSiteInfoList())
	        {
	            if (String.Compare(site.RootPath + site.StartItem, item.Paths.FullPath, true) == 0)
	            {
	                return;
	            }
	        }

	        using (new Sitecore.SecurityModel.SecurityDisabler())
	        {
	            using (new Sitecore.Data.Items.EditContext(item))
	            {
	                using (new Sitecore.Data.Events.EventDisabler())
	                {
	                    item.Name = newName;
	                    item.Appearance.DisplayName = newName;
	                }
	            }
	        }
	    }
	}


	    private void GetMarketList()
		{

			var allStatesAndDivisions = CompanySearch.GetCrossCompaniesStateMarketingList(_DB);
			var allDivisions = allStatesAndDivisions.Where(d => d.TemplateID == SCIDs.TemplateIds.DivisionPage).OrderBy(o => o.Name);

			foreach (var division in allDivisions)
			{
				chklMarketFilters.Items.Add(new ListItem(division.Name, division.ID.ToString()));

			}


		}

		private void OnSearchButtonClicked(object sender, EventArgs e)
		{
			var selectedBedMin = ddlBedMin.SelectedValue;
			var selectedBedMax = ddlBedMax.SelectedValue;
			var selectedBathMin = ddlBathMin.SelectedValue;
			var selectedBathMax = ddlBathMax.SelectedValue;
			var selectedHalfBathMin = ddlHalfBathMin.SelectedValue;
			var selectedHalfBathMax = ddlHalfBathMax.SelectedValue;
			var selectedPlantype = ddlPlanType.SelectedValue;
			var selectedStory = ddlStories.SelectedValue;




			int bedMin = selectedBedMin.IsEmpty() ? 0 : selectedBedMin.CastAs(0);
			int bedMax = selectedBedMax.IsEmpty() ? 0 : selectedBedMax.CastAs(0);
			int bathMin = selectedBathMin.IsEmpty() ? 0 : selectedBathMin.CastAs(0);
			int bathMax = selectedBathMax.IsEmpty() ? 0 : selectedBathMax.CastAs(0);
			int halfBathMin = selectedHalfBathMin.IsEmpty() ? 0 : selectedHalfBathMin.CastAs(0);
			int halfBathMax = selectedHalfBathMax.IsEmpty() ? 0 : selectedHalfBathMax.CastAs(0);
			int sqFtMin = txtSqFtMin.Text.CastAs(0);
			int sqFtMax = txtSqFtMax.Text.CastAs(0);
			int story = selectedStory.IsEmpty() ? 0 : selectedStory.CastAs(0);

			var results = new List<PlanSearchResults>();
			foreach (ListItem market in chklMarketFilters.Items)
			{
				if (market.Selected)
				{
					var planSeachCriteria =
						new PlanSearchCriteria(market.Value, bedMin, bedMax, bathMin, bathMax, halfBathMin, halfBathMax,
											   story, selectedPlantype, sqFtMin, sqFtMax);

					results.AddRange(GetPlans(planSeachCriteria));
				}
			}

			rptSearchResults.DataSource = results;
			rptSearchResults.DataBind();
		}

		private IEnumerable<PlanSearchResults> GetPlans(PlanSearchCriteria planSearchCriteria)
		{

			var divisionID = _DB.GetItem(new ID(planSearchCriteria.MarketID));
			var query = SCFastQueries.GetAllPlansUnderDivison(divisionID.Paths.FullPath, planSearchCriteria);
			var plans = _DB.SelectItems(query);
			var planSearchResults = new List<PlanSearchResults>();
			foreach (var plan in plans)
			{
				planSearchResults.Add(new PlanSearchResults()
										  {
											  Division = GetDivisionName(plan),
											  Community = GetCommunityName(plan),
											  Subcommunity = GetSubCommunityName(plan),
											  PlanName = plan.Fields[SCIDs.PlanFields.PlanName].Value,
											  PlanType = GetPlanType(plan),
											  SqFt = plan.Fields[SCIDs.PlanFields.SquareFootage].Value,
											  Stories = plan.Fields[SCIDs.PlanBaseFields.NumberOfStories].Value,
											  Bed = plan.Fields[SCIDs.PlanBaseFields.NumberOfBedrooms].Value,
											  Bath = plan.Fields[SCIDs.PlanBaseFields.NumberOfBathrooms].Value,
											  HalfBath = plan.Fields[SCIDs.PlanBaseFields.NumberOfHalfBathrooms].Value,
											  Garage = plan.Fields[SCIDs.PlanBaseFields.NumderOfGarages].Value

										  });
			}

			return planSearchResults;
		}

		private string GetPlanType(Item plan)
		{
			var productType = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.ProductTypePage, plan.ID, DB: _DB);

			return productType.Name.Contains("New Homes") ? "Single Family" : "Multi Family";

		}

		private string GetSubCommunityName(Item plan)
		{
			var subCommunityID = plan.Fields[SCIDs.PlanFields.SubCommunityID].Value;
			var subCommunity = _DB.GetItem(subCommunityID);

			if (subCommunity == null) return string.Empty;

			var subCommunityName = subCommunity.Fields[SCIDs.SubCommunityFields.SubCommunityName].Value;
			return subCommunityName;
		}

		private string GetCommunityName(Item plan)
		{
			var community = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.CommunityPage, plan.ID, DB: _DB);
			if (community == null) return string.Empty;
			return community.Fields[SCIDs.CommunityFields.CommunityName].Value;
		}

		private string GetDivisionName(Item plan)
		{
			var division = SCUtils.GetNearestParentOfTemplateID(SCIDs.TemplateIds.DivisionPage, plan.ID, DB: _DB);
			if (division == null) return string.Empty;
			return division.Fields[SCIDs.DivisionFieldIDs.Divisionname].Value;
		}
	}
}
