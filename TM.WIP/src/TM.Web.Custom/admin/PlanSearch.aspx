﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlanSearch.aspx.cs" Inherits="TM.Web.Custom.admin.PlanSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <title>TaylorMorrison Plan Utiltiy Page</title>
    
        <link type="text/css" href="/Styles/lib/ui.jqgrid.css" rel="stylesheet"/>
        <link type="text/css" href="/Styles/lib/jquery-ui-1.10.2.custom.min.css" rel="stylesheet"/>
        <script language="javascript" src="/javascript/lib/jquery-1.9.0.min.js"> </script>
        <script language="javascript" src="/javascript/lib/grid.locale-en.js"> </script>
        <script language="javascript" src="/javascript/lib/jquery.jqGrid.min.js"></script>
        <script language="javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"> </script>
   
        <style>
        
            body {
                font-family: arial;
                font-size: 11px;
            }

            #planSearchHeader {
                background-color: #979d54;
                width: 100%;
                height: auto;
                margin-bottom: 20px;
                font-size: 12px;
                font-weight: bold;
                color: #fff;
                padding: 2px;
            }

            #planSearchFilters {
                border-color: #4c4c4c;
                border-width: 1px;
                border-radius: 10px;
                background-color: #eee;
                float: left;
                padding: 5px;
            }


            #planSearchFilters label, #planSearchFilters input {
                display: inline-block;
                margin-bottom: 5px;
            }

            #planSearchFilters label {
                width: 80px; /* or whatever size you want them */
                padding: 5px;
            }

            #planSearchFilters input { width: 100px; /* or whatever size you want them */ }

            #planSearchMarketFilter label { width: 200px; /* or whatever size you want them */ }

            #planSearchMarketFilter input { width: auto; /* or whatever size you want them */ }

            #plansearchResults {
                float: right;
                margin: auto;
            }

            .alternaterow { background-color: #f3f3e9; }

            input.search {
                font-family: arial;
                font-size: 12px;
                font-weight: 600;
                background: #D31245;
                color: #FFFFFF;
                border: 1px #ffffff solid;
                text-align: center;
                margin: 0px 50px 10px 50px;
            }
        </style>
  
    </head>

    <body>
        <script language="javascript">
            $(document).ready(function() {
                tableToGrid("#planSearchResultsTbl", {
                    width: 890,
                    height: 900,
                    loadonce: true,
                    rowNum: -1,
                    altRows: true,
                    altclass: "alternaterow",
                    margin: "10px",
                    caption: "Please enter your search criteria on the left and click 'Search'"
                });

            });

        </script>
  

        <div id="planSearchLogo" >
            <img src="/images/tm/tm_logo.jpg"/>
        </div>
				
        <div id="planSearchHeader">
            Taylor Morrison Plan Search Tool 
        </div>

        <form runat="server" style="margin-right: 20px">
            <div id="planSearchFilters">
                <div>
                    <label >Sq. ft:</label>
                    <asp:TextBox runat="server" ID="txtSqFtMin"></asp:TextBox> to 
                    <asp:TextBox runat="server" ID="txtSqFtMax"></asp:TextBox>
                </div>
		
                <div>
                    <label>Width:</label>
                    <asp:TextBox runat="server" ID="txtWidthMin"></asp:TextBox> to 
                    <asp:TextBox runat="server" ID="txtWidthMax"></asp:TextBox>
                </div>
		
                <div>
                    <label>Depth:</label>
                    <asp:TextBox runat="server" ID="txtDepthMin"></asp:TextBox> to 
                    <asp:TextBox runat="server" ID="txtDepthMax"></asp:TextBox>
                </div>
		
                <div>
                    <label># of Beds:</label>
                    <asp:DropDownList runat="server" ID="ddlBedMin"></asp:DropDownList> to 
                    <asp:DropDownList runat="server" ID="ddlBedMax"></asp:DropDownList>
                </div>
		
                <div>
                    <label># of Baths:</label>
                    <asp:DropDownList runat="server" ID="ddlBathMin"></asp:DropDownList> to 
                    <asp:DropDownList runat="server" ID="ddlBathMax"></asp:DropDownList>
                </div>
		
                <div>
                    <label># of H.Baths</label>
                    <asp:DropDownList runat="server" ID="ddlHalfBathMin"></asp:DropDownList> to 
                    <asp:DropDownList runat="server" ID="ddlHalfBathMax"></asp:DropDownList>
                </div>
		
                <div>
                    <label>Stories</label>
                    <asp:DropDownList runat="server" ID="ddlStories"></asp:DropDownList> 
                </div>
		
                <div>
                    <label>Plan Type:</label>
                    <asp:DropDownList runat="server" ID="ddlPlanType"></asp:DropDownList>
                </div>
	         
                <div>
                    <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="search" />
                </div>

                <div id="planSearchMarketFilter">
                    <strong>Market Filter:</strong>
                    <asp:CheckBoxList runat="server" ID="chklMarketFilters" DataTextField="Text" DataValueField="Value" ></asp:CheckBoxList>
                </div>
            </div>

        </form>    

        <div id="#plansearchResults" style="float:left;margin-left:20px">
       
            <table id="planSearchResultsTbl">
                <asp:Repeater runat="server" ID="rptSearchResults">
                    <HeaderTemplate>
                        <thead>
                            <tr>
                                <th><a href="#">Division</a></th>
                                <th><a href="#">Community</a></th>
                                <th><a href="#">Subcommunity</a></th>
                                <th><a href="#">Plan Name</a></th>
                                <th><a href="#">Plan Type</a></th>
                                <th><a href="#">SqFt</a></th>
                                <th><a href="#">Width</a></th>
                                <th><a href="#">Depth</a></th>
                                <th><a href="#">Stroies</a></th>
                                <th><a href="#">Bed</a></th>
                                <th><a href="#">Bath</a></th>
                                <th><a href="#">Half Bath</a></th>
                                <th><a href="#">Garage</a></th>
                            </tr>
                        </thead>
                        <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%#Eval("Division")%></td>
                            <td><%#Eval("Community")%></td>
                            <td><%#Eval("SubCommunity")%></td>
                            <td><%#Eval("PlanName")%></td>
                            <td><%#Eval("PlanType")%></td>
                            <td><%#Eval("SqFt")%></td>
                            <td><%#Eval("Width")%></td>
                            <td><%#Eval("Depth")%></td>
                            <td><%#Eval("Stories")%></td>
                            <td><%#Eval("Bed")%></td>
                            <td><%#Eval("Bath")%></td>
                            <td><%#Eval("HalfBath")%></td>
                            <td><%#Eval("Garage")%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
	        
                    </FooterTemplate>
                </asp:Repeater>
            </tbody>
            </table>
        </div>
    	


	
    
   
   
    </body>
</html>
