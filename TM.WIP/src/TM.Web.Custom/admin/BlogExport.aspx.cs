﻿using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.IO;
using Sitecore.Security.AccessControl;
using Sitecore.Security.Accounts;
using Sitecore.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using TM.Web.Custom.Constants;
using TM.Web.Custom.Layouts;
using TM.Web.Custom.Queries.LuceneQueries;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.admin
{
    public partial class BlogExport : BasePage
    {
        private readonly Database _masterDB = Factory.GetDatabase("master");
        //private readonly string _zipFile = "BlogExport.zip";
        private readonly string _xmlFile = "BlogExport.xml";

        protected void Page_Load(object sender, EventArgs e)
        {
            var error = string.Empty;
            var user = Sitecore.Context.User;
            literalUser.Text = user.DisplayName;

            if (!user.IsAuthenticated)
            {
                error = "Error, current user is not autenticated";
            }
            else if (!user.DisplayName.StartsWith("sitecore"))
            {
                error = "Error, user must be a sitecore user";
            }

            if (!string.IsNullOrEmpty(error))
            {
                literalError.Text = error;
                return;
            }

            var xmlFilePath = Server.MapPath(Path.Combine(TempFolder.Folder, _xmlFile));
            var baseUrl = HttpContext.Current.Request.Url;

            lock (TaskHolder._lock)
            {
                if (TaskHolder._task != null)
                {
                    panelContent.Visible = false;
                    literalLog.Text = GetLog();
                    if (!TaskHolder._task.IsCompleted && !TaskHolder._task.IsFaulted)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 1000);</script>");
                    }
                    else if (TaskHolder._task.IsCompleted)
                    {
                        if (!File.Exists(xmlFilePath))
                        {
                            Log("Could not find file {0}", xmlFilePath);
                            literalLog.Text = GetLog();
                            TaskHolder._task = null;
                            TaskHolder._log = null;
                            return;
                        }
                        if (TaskHolder._log != null)
                        {
                            Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 1000);</script>");
                            TaskHolder._log = null;
                            return;
                        }
                        TaskHolder._task = null;
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.ContentType = "application/octet-stream";
                        HttpContext.Current.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", _xmlFile));
                        HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.OK;
                        HttpContext.Current.Response.BufferOutput = true;
                        using (StreamReader sr = new StreamReader(xmlFilePath))
                        {
                            sr.BaseStream.CopyTo(HttpContext.Current.Response.OutputStream);
                            HttpContext.Current.Response.Flush();
                            HttpContext.Current.Response.End();
                            return;
                        }
                    }
                    else
                    {
                        TaskHolder._task = null;
                        TaskHolder._log = null;
                    }
                    return;
                }

                if (!IsPostBack)
                {
                    error = LoadAvailableBlogs();
                }

                if (!string.IsNullOrEmpty(error))
                {
                    literalError.Text = error;
                    return;
                }

                panelContent.Visible = true;
                if (IsPostBack)
                {
                    TaskHolder._log = new StringBuilder();
                    panelContent.Visible = false;

                    Log("Log:");
                    Log("Starting job");
                    Page.ClientScript.RegisterStartupScript(GetType(), "refresh", "<script>setTimeout(function () {window.location.reload()}, 1000);</script>");

                    TaskHolder._task = Task.Run(() =>
                    {
                        var watch = Stopwatch.StartNew();
                        try
                        {
                            if (File.Exists(xmlFilePath))
                            {
                                File.Delete(xmlFilePath);
                            }

                            var blog = GetItemByID(dropDownBlogs.SelectedValue);
                            var entries = blog.Axes
                                .GetDescendants()
                                .Where(i => i.TemplateID == SCIDs.Blog.TM.Entry)
                                .OrderBy(i => i.Paths.Path);

                            var root = new XElement("Blog");
                            foreach (var entry in entries)
                            {
                                var item = CreateEntry(entry, baseUrl);
                                root.Add(item);
                            }

                            var doc = new XDocument(root);
                            doc.Save(xmlFilePath);
                        }
                        catch (Exception ex)
                        {
                            Log("Error: {0}", ex.ToString());
                        }

                        watch.Stop();
                        Log("Total time elapsed: {0}", watch.Elapsed.ToString());
                    });
                }
            }

            literalLog.Text = GetLog();
        }

        private XElement CreateEntry(Item entry, Uri baseUrl)
        {
            Log("Reading blog entry '{0}' full path '{1}'", entry.Name, entry.Paths.ContentPath);
            var content = GetFullSrcImages(entry[SCFieldNames.BlogEntry.Content], baseUrl);
            var introduction = GetFullSrcImages(entry[SCFieldNames.BlogEntry.Introduction], baseUrl);
            var thumbnailField = (ImageField)entry.Fields[SCFieldNames.BlogEntry.ThumbnailImage];
            var thumbnailImage = string.Empty;
            if (thumbnailField != null && thumbnailField.MediaItem != null)
            {
                thumbnailImage = GetAbsoluteUrl(thumbnailField.MediaItem.GetMediaUrl(), baseUrl);
            }

            var item = new XElement("Entry");
            item.Add(new XElement("Path", entry.Paths.ContentPath));
            item.Add(new XElement("Author", entry[SCFieldNames.BlogEntry.Author]));
            item.Add(new XElement("BlogRssURL", entry[SCFieldNames.BlogEntry.BlogRssURL]));
            item.Add(new XElement("BlogTitle", entry[SCFieldNames.BlogEntry.BlogTitle]));
            item.Add(new XElement("Category", entry[SCFieldNames.BlogEntry.Category]));
            item.Add(new XElement("Content", new XCData(content)));
            item.Add(new XElement("DateCreated", entry[SCFieldNames.BlogEntry.DateCreated]));
            item.Add(new XElement("DateModified", entry[SCFieldNames.BlogEntry.DateModified]));
            item.Add(new XElement("DisableComments", entry[SCFieldNames.BlogEntry.DisableComments]));
            item.Add(new XElement("Introduction", new XCData(introduction)));
            item.Add(new XElement("Tags", entry[SCFieldNames.BlogEntry.Tags]));
            item.Add(new XElement("ThumbnailImage", thumbnailImage));
            item.Add(new XElement("Title", entry[SCFieldNames.BlogEntry.Title]));

            //ZipRichTextImages(entry, SCFieldNames.BlogEntry.Content, zipWriter);
            //ZipRichTextImages(entry, SCFieldNames.BlogEntry.Introduction, zipWriter);
            //ZipThumbnailImage(entry, zipWriter);
            return item;
        }

        #region Database Utils
        private Item GetItemByID(string ID)
        {
            return _masterDB.GetItem(new ID(ID));
        }

        private Item GetItemByPath(string path)
        {
            return _masterDB.GetItem(path);
        }

        private TemplateItem GetTemplateByID(ID templateId)
        {
            return _masterDB.GetTemplate(templateId);
        }
        #endregion

        #region Utils
        private string GetFullSrcImages(string value, Uri baseUrl)
        {
            return Regex.Replace(value, "<img.*src=\"([^\"]+)\".*>", delegate(Match match)
                {
                    var src = match.Groups[1].Value;
                    return match.Value.Replace(src, GetAbsoluteUrl(src, baseUrl));
                });
        }

        private string GetAbsoluteUrl(string url, Uri baseUrl)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }
            var uri = new Uri(url, UriKind.RelativeOrAbsolute);
            if (!uri.IsAbsoluteUri)
            {
                var builder = new UriBuilder(baseUrl.Scheme, baseUrl.Host, baseUrl.Port);
                builder.Path = url;
                uri = builder.Uri;
            }
            return uri.ToString();
        }
        /*
        private void ZipRichTextImages(Item entry, string field, ZipWriter zipWriter)
        {
            var content = entry[field];
            var match = Regex.Match(content, "<img.*src=\"([^\"]+)\".*>");
            var count = 1;
            while (match.Success)
            {
                var src = match.Groups[1].Value;
                var extension = ".jpg";
                if (src.Contains("."))
                {
                    extension = src.Substring(src.LastIndexOf('.'));
                    if (extension.Contains("?"))
                    {
                        extension = extension.Substring(0, extension.IndexOf('?'));
                    }
                }
                try
                {
                    Log("Downloading image {0} as {1}", src, field + "-" + count + extension);
                    using (var stream = DownloadImage(src))
                    {
                        zipWriter.AddEntry(entry.Paths.ContentPath.Substring(1) + "/" + field + "-" + count++ + extension, stream);
                    }
                }
                catch (Exception ex)
                {
                    Log("Could not download image {0}. Error: {1}", src, ex.ToString());
                }
                match = match.NextMatch();
            }
        }

        private static Stream DownloadImage(string url)
        {
            byte[] imageData;
            using (var client = new WebClient())
            {
                imageData = client.DownloadData(url);
            }

            return new MemoryStream(imageData);
        }

        private void ZipThumbnailImage(Item entry, ZipWriter zipWriter)
        {
            ImageField thumbnail = entry.Fields[SCFieldNames.BlogEntry.ThumbnailImage];
            if (thumbnail != null)
            {
                MediaItem media = thumbnail.MediaItem;
                if (media != null)
                {
                    var stream = media.GetMediaStream();
                    if (stream != null && stream.Length > 0)
                    {
                        zipWriter.AddEntry(entry.Paths.ContentPath.Substring(1) + "/Thumbnail." + media.Extension, stream);
                    }
                    else
                    {
                        Log("Thumbnail image is not present.");
                    }
                }
                else
                {
                    Log("Thumbnail image is not present.");
                }
            }
        }
        */
        private string LoadAvailableBlogs()
        {
            using (var context = ContentSearchManager.GetIndex(string.Format("sitecore_{0}_index", _masterDB.Name)).CreateSearchContext())
            {
                var blogs = context.GetQueryable<TMSearchResultItem>()
                    .Where(i => i.TemplateId == SCIDs.Blog.TM.Blog
                        && i.IsLatestVersion && !i.IsTemplate
                        && i.Language == "en" && i.Name != "__Standard Values")
                    .Select(i => i.GetItem())
                    .ToList();

                if (blogs.Count == 0)
                {
                    return "Error, there are no empty divisions available";
                }
                foreach (var blog in blogs
                                    .Where(i => !i.Paths.Path.Contains("/templates/"))
                                    .OrderBy(i => i.Paths.Path))
                {
                    dropDownBlogs.Items.Add(new System.Web.UI.WebControls.ListItem(blog.Paths.Path.Replace("/sitecore/content", ""), blog.ID.ToString()));
                }
                return string.Empty;
            }
        }

        private void Log(string format, params object[] values)
        {
            if (TaskHolder._log == null)
                TaskHolder._log = new StringBuilder();
            TaskHolder._log.AppendFormat(format, values);
            TaskHolder._log.AppendLine();
        }

        private string GetLog()
        {
            if (TaskHolder._log == null)
                return string.Empty;
            return HttpUtility.HtmlEncode(TaskHolder._log.ToString()).Replace(Environment.NewLine, "<br/>");
        }
        #endregion
    }
}