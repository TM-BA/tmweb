﻿using System;
using System.Web;
using System.Web.UI;
using Sitecore.Sites;
using TM.Utils.Web;
using Sitecore.Data.Items;
using TM.Web.Custom.Constants;
using TM.Web.Custom.SCHelpers;
using Sitecore.Security;
using TM.Domain;
using TM.Utils.Extensions;

namespace TM.Web.Custom.SCHelpers
{
	public class BaseHelper
	{
		public Sitecore.Data.Database SCContextDB
		{
			get { return Sitecore.Context.Database; }
		}

		public Item CurrentHomeItem
		{
			get
			{
				return SCContextDB.GetItem(SCCurrentSite.StartPath);
			}
		}

		protected SiteContext SCCurrentSite
		{
			get { return Sitecore.Context.Site; }
		}

		public string SCCurrentDeviceName
		{
			get { return Sitecore.Context.GetDeviceName() == "Default" ? "TaylorMorrison" : Sitecore.Context.GetDeviceName(); }
		}

		public string SCCurrentDeviceNameAbbreviation
		{
			get { return SCCurrentDeviceName.GetCurrentCompanyNameAbbreviation().ToLower();}
		}

		
	}
}