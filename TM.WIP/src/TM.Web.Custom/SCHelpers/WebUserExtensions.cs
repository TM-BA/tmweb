﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Sitecore;
using Sitecore.SecurityModel;
using TM.Domain;
using TM.Utils;
using TM.Utils.Extensions;
using SCAuthentication = Sitecore.Security.Authentication;
using SCAccounts = Sitecore.Security.Accounts;
using TM.Web.Custom.Constants;
using TM.Utils.Web;
namespace TM.Web.Custom.SCHelpers
{
    public static class WebUserExtensions
    {
        private static string _IFPPassphrase = "IFP_ROCKS!";

        /// <summary>
        /// Call this xtn method on TMUser object.
        /// It is the resposibility of the caller to ensure TMUser is valid
        /// </summary>
        /// <param name="webUser"></param>
        /// <param name="persistent"></param>
        /// <returns></returns>
        public static bool Login(this TMUser webUser, bool persistent = false)
        {
            //username used to login is their email
            //Guard.AgainstEmpty(webUser.Email);
            //Guard.AgainstEmpty(webUser.Password);

            //var domainUser = Membership.GetUserNameByEmail(webUser.Email);
            //if (domainUser == null) return false; 
            //Guard.AgainstEmpty(domainUser);
            //webUser.UserName = domainUser.Split('\\')[1];
            //return SCAuthentication.AuthenticationManager.Login(domainUser, webUser.Password, persistent);

            var loggedin = SCAuthentication.AuthenticationManager.Login(webUser.FullyQualifiedUserName, webUser.Password, persistent);
            SCAuthentication.AuthenticationManager.SetActiveUser(webUser.FullyQualifiedUserName);

            CreateIFPSSOCookieOnLogin(webUser.UserName,webUser.Password);

            return loggedin;
        }

        public static void Logout()
        {
            SCAuthentication.AuthenticationManager.Logout();

            RemoveIFPSSOCookie();


        }


        private static void CreateIFPSSOCookieOnLogin(string userName, string password)
        {
            var cookie = new HttpCookie("IFPSSOAUTH");
            cookie.Path = "/";
            var host = HttpContext.Current.Request.Url.Host;
            host = host.Remove(0, host.IndexOf(".", System.StringComparison.Ordinal));
            cookie.Domain = host;
            cookie.Expires = DateTime.Now.AddDays(1);
            cookie.Value = CryptographyHelpers.EncryptString(userName+":"+password, _IFPPassphrase);
	    HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private static void RemoveIFPSSOCookie()
        {
           
            if (HttpContext.Current.Response.Cookies["IFPSSOAUTH"] != null)
            {
                var cookie = new HttpCookie("IFPSSOAUTH");
                cookie.Path = "/";
                var host = HttpContext.Current.Request.Url.Host;
                host = host.Remove(0, host.IndexOf(".", System.StringComparison.Ordinal));
                cookie.Domain = host;
                cookie.Expires = DateTime.Now.AddDays(-1);
                cookie.Value = string.Empty;
                HttpContext.Current.Response.Cookies.Add(cookie);
                
            }
        }

        public static TMUser LoginUsingIfpssoCookieAndSetCurrentUser()
	{
	   
            if (HttpContext.Current != null && Sitecore.Context.Domain.Name == "extranet" && !HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant().Contains("/logout")
                && !HttpContext.Current.Request.Url.AbsolutePath.ToLowerInvariant().Contains("/notindexed/disclaimer.aspx")
                )
	    {
	        var cookieCollection = HttpContext.Current.Request.Cookies;
	        var ifpIntegrationCookie = cookieCollection["IFPSSOAUTH"];
            if (ifpIntegrationCookie != null && !string.IsNullOrWhiteSpace(ifpIntegrationCookie.Value))
	        {
	            var username_password = ifpIntegrationCookie.Value;

	            username_password = CryptographyHelpers.DecryptString(username_password, _IFPPassphrase);

	            var username = username_password.Split(':')[0];
		   if(!username.Contains("extranet\\"))
		   {
		       username = "extranet\\" + username;
		   }
	            if (Sitecore.Security.Authentication.AuthenticationManager.Login(username, true))
	            {
	                SCAuthentication.AuthenticationManager.SetActiveUser(username);
	                var tmUser =
	                    new TMUser(GetUsersCurrentDomain(),
	                               username.Replace("extranet\\", string.Empty)).Get(true);
	                return tmUser;
	            }
	        }
	    }
	    return null;
	}

        


        public static bool Create(this TMUser webUser, string emailBody)
        {
            if (SCAccounts.User.Exists(webUser.FullyQualifiedUserName))
                return false;

            //emails are unique across systems
            if (Membership.GetUserNameByEmail(webUser.Email).IsNotEmpty())
                return false;

            Guard.AgainstEmpty(webUser.Password);
            Guard.AgainstEmpty(webUser.Email);
            MembershipCreateStatus status;
            Membership.CreateUser(webUser.FullyQualifiedUserName,
                                  webUser.Password, webUser.Password, null, null, true, out status);


           
            if (!status.Equals(MembershipCreateStatus.Success))
            {
                /* throw new System.Web.Security.MembershipCreateUserException(
                       status.ToString());*/
                return false;
            }
            CreateIFPSSOCookieOnLogin(webUser.UserName, webUser.Password);
            var scuser = webUser.GetSCUser(true);
            scuser.HydrateFromTMUser(webUser);
            scuser.Profile.Save();
            SCAuthentication.AuthenticationManager.SetActiveUser(scuser);

            if (SCAuthentication.AuthenticationManager.Login(scuser))
            {
                WelcomeEmail(webUser, emailBody);
                return true;
            }
	  
            return false;
        }

        public static void WelcomeEmail(TMUser info, string emailString)
        {
            string mycompany;
            string mycompanyname;
            var currentHomeItem = Context.Database.GetItem(Context.Site.StartPath);
            var myTMLogo = currentHomeItem.Fields[SCIDs.CompanyFields.MyLogo];
            var mycompanylogo = string.Empty;
            string fromEmail;

            //This should be current domain name not user's domain
            var currentDeviceName = Context.GetDeviceName() == "Default" ? "TaylorMorrison" : Context.GetDeviceName();

            switch (currentDeviceName.ToLower())
            {
                case "monarchgroup":
                    mycompany = "myFavs";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.MonarchGroup);
                    fromEmail = Config.Settings.MG_FromEmail;
                    break;
                case "darlinghomes":
                    mycompany = "myDH";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.DarlingHomes);
                    fromEmail = Config.Settings.DH_FromEmail;
                    break;
                default:
                    mycompany = "myTM";
                    mycompanyname = string.Format("{0}, Inc.", SCFieldNames.TaylorMorrison);
                    fromEmail = Config.Settings.TM_FromEmail;
                    break;
            }

            if (myTMLogo != null && myTMLogo.HasValue)
            {
                mycompanylogo = myTMLogo.GetMediaUrl();
            }

            emailString = System.Net.WebUtility.HtmlDecode(emailString);

            Uri uri = HttpContext.Current.Request.Url;
            string authority = string.Format("{0}://{1}", uri.Scheme, uri.Authority);
            emailString = emailString.Replace("{0}", authority).Replace("{1}", mycompanylogo).Replace("{2}", mycompany).Replace("{3}", info.FirstName).Replace("{4}", info.Email).Replace("{5}", mycompanyname);

            EmailHelper.SendEmail(fromEmail, info.Email, SCFieldNames.UserTexts.NewAccount + mycompany + "!", emailString);
        }

        public static TMUser Get(this TMUser webUser, bool isAuthenticated = false)
        {
            var scUser = webUser.GetSCUser(isAuthenticated);

            return scUser.HydrateToTMUser(webUser.Domain);
        }

        public static bool Exists(this TMUser webUser)
        {
            return SCAccounts.User.Exists(webUser.FullyQualifiedUserName);
        }

        public static SCAccounts.User GetSCUser(this TMUser webUser, bool isAuthenticated = false)
        {
            var scUser = SCAccounts.User.FromName(webUser.FullyQualifiedUserName, isAuthenticated);
            return scUser;
        }

        public static void Update(this TMUser webUser)
        {
            SCAccounts.User scUser = SCAccounts.User.FromName(webUser.FullyQualifiedUserName, true);
            scUser.HydrateFromTMUser(webUser);
            using (new SecurityDisabler())
            {
                scUser.Profile.Save();
            }
        }

        public static bool UpdateProfile(this TMUser webUser)
        {
            SCAccounts.User scUser = SCAccounts.User.FromName(webUser.FullyQualifiedUserName, true);
            scUser.HydrateFromTMUser(webUser);
            scUser.Profile.Save();

            SCAuthentication.AuthenticationManager.SetActiveUser(scUser);
            if (SCAuthentication.AuthenticationManager.Login(scUser))
            {
                return true;
            }
            return false;
        }

        public static void UpdatePassword(this TMUser webUser, string newPassword)
        {
            var user = Membership.GetUser(webUser.FullyQualifiedUserName, true);
            if (user != null) user.ChangePassword(user.ResetPassword(), newPassword);
        }

        public static void Delete(this TMUser webUser)
        {
            SCAccounts.User scUser = SCAccounts.User.FromName(webUser.FullyQualifiedUserName, false);
            scUser.Delete();
        }


        public static dynamic Impersonate(this TMUser currentUser, TMUser userToImpersonate, Func<dynamic> codeToExecute)
        {

            using (new SCAccounts.UserSwitcher(userToImpersonate.GetSCUser()))
            {
                return codeToExecute();
            }

        }

        public static string GetPassword(this TMUser webUser)
        {
            //SCDefaultMembership provider is based on asp.net membership
            var membershipUser = Membership.GetUser(webUser.FullyQualifiedUserName);
            if (membershipUser != null)
                return membershipUser.GetPassword();
            return null;
        }

        public static TMUser HydrateToTMUser(this Sitecore.Security.Accounts.User scUser, TMUserDomain domain)
        {
            Sitecore.Caching.CacheManager.ClearSecurityCache(scUser.Identity.Name);
            var webUser = new TMUser(domain, scUser.LocalName)
                              {
                                  FirstName = scUser.Profile.GetCustomProperty("FirstName"),
                                  LastName = scUser.Profile.GetCustomProperty("LastName"),
                                  Email = scUser.Profile.Email,
                                  AddressLine1 = scUser.Profile.GetCustomProperty("AddressLine1"),
                                  AddressLine2 = scUser.Profile.GetCustomProperty("AddressLine2"),
                                  City = scUser.Profile.GetCustomProperty("City"),
                                  StateAbbreviation = scUser.Profile.GetCustomProperty("StateAbbreviation"),
                                  Zipcode = scUser.Profile.GetCustomProperty("ZipCode"),
                                  Phone = scUser.Profile.GetCustomProperty("Phone"),
                                  PhoneExtension = scUser.Profile.GetCustomProperty("PhoneExtension"),
                                  AreaOfInterest = scUser.Profile.GetCustomProperty("AreaOfIterest"),
                                  PasswordRecoveryId = scUser.Profile.GetCustomProperty("PasswordRecoveryId"),
                                  CommunityFavorites = scUser.Profile.GetCustomProperty("CommunityFavorites"),
                                  PlanFavorites = scUser.Profile.GetCustomProperty("PlanFavorites"),
                                  HomeforSaleFavorites = scUser.Profile.GetCustomProperty("HomeforSaleFavorites"),
				 Device = scUser.Profile.GetCustomProperty("Device")

                              };
           
            return webUser;
        }

        public static SCAccounts.User HydrateFromTMUser(this Sitecore.Security.Accounts.User scUser, TMUser webUser)
        {

            using (new SecurityDisabler())
            {
            
                scUser.Profile.SetCustomProperty("UserName", webUser.UserName);
                scUser.Profile.Email = webUser.Email;
             
                if (webUser.FirstName.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("FirstName", webUser.FirstName);
                else
                    scUser.Profile.RemoveCustomProperty("FirstName");
                if (webUser.LastName.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("LastName", webUser.LastName);
                else
                    scUser.Profile.RemoveCustomProperty("LastName");
                if (webUser.AddressLine1.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("AddressLine1", webUser.AddressLine1);
                else
                    scUser.Profile.RemoveCustomProperty("AddressLine1");
                if (webUser.AddressLine2.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("AddressLine2", webUser.AddressLine2);
                else
                    scUser.Profile.RemoveCustomProperty("AddressLine2");
                if (webUser.City.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("City", webUser.City);
                else
                    scUser.Profile.RemoveCustomProperty("City");
                if (webUser.StateAbbreviation.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("StateAbbreviation", webUser.StateAbbreviation);
                else
                    scUser.Profile.RemoveCustomProperty("StateAbbr");
                if (webUser.Zipcode.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("ZipCode", webUser.Zipcode);
                else
                    scUser.Profile.RemoveCustomProperty("ZipCode");
                if (webUser.Phone.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("Phone", webUser.Phone);
                else
                    scUser.Profile.RemoveCustomProperty("Phone");
                if (webUser.PhoneExtension.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("PhoneExtension", webUser.PhoneExtension);
                else
                    scUser.Profile.RemoveCustomProperty("PhoneExtension");
                if (webUser.AreaOfInterest.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("AreaOfIterest", webUser.AreaOfInterest);
                else
                    scUser.Profile.RemoveCustomProperty("AreaOfIterest");
                if (webUser.PasswordRecoveryId.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("PasswordRecoveryId", webUser.PasswordRecoveryId);
                else
                    scUser.Profile.RemoveCustomProperty("PasswordRecoveryId");
                if (webUser.CommunityFavorites.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("CommunityFavorites", webUser.CommunityFavorites);
                else
                    scUser.Profile.RemoveCustomProperty("CommunityFavorites");
                if (webUser.PlanFavorites.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("PlanFavorites", webUser.PlanFavorites);
                else
                    scUser.Profile.RemoveCustomProperty("PlanFavorites");
                if (webUser.HomeforSaleFavorites.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("HomeforSaleFavorites", webUser.HomeforSaleFavorites);
                else
                    scUser.Profile.RemoveCustomProperty("HomeforSaleFavorites");
                if (webUser.Device.IsNotEmpty())
                    scUser.Profile.SetCustomProperty("Device", webUser.Device);
                else
                    scUser.Profile.RemoveCustomProperty("Device");
		
            }


            return scUser;
        }

        public static string GetCredentialText(TMUser currentWebUser)
        {
            var auth = String.Format("<a href=\"/Account/Register\" onclick=\"TM.Common.GAlogevent('CreateAccount','Click','CreateAccountLink')\">Create Account</a>|<a href=\"/Account/Login\" onclick=\"TM.Common.GAlogevent('Login','Click','Login')\">Login</a>");
            if (currentWebUser != null)
            {
                if (!String.IsNullOrWhiteSpace(currentWebUser.UserName))
                    auth = String.Format("<a href=\"/Account/My-Favorites\"  class=\"fav-savitems\">Favorites</a>|<a href=\"/Account/logout\">Logout</a>");
                else
                    auth = String.Format("<a href=\"/Account/logout\" onclick=\"TM.Common.GAlogevent('Login','Click','Login')\">Logout</a>");

                //if (!String.IsNullOrWhiteSpace(currentWebUser.CommunityFavorites) ||
                //    !String.IsNullOrWhiteSpace(currentWebUser.PlanFavorites) ||
                //    !String.IsNullOrWhiteSpace(currentWebUser.HomeforSaleFavorites))
                //    auth =
                //        String.Format("<a href=\"/Account/My-Favorites\"  class=\"fav-savitems\">Favorites</a>|<a href=\"/Account/logout\">Logout</a>");
                //else
                //    auth = String.Format("<a href=\"/Account/logout\">Logout</a>");

            }

            return auth;
        }

        /// <summary>
        /// Get current domain for TMusers
        /// </summary>
        /// <returns></returns>
        public static TMUserDomain GetUsersCurrentDomain()
        {
            var domain = Context.GetDeviceName().GetDomainFromDeviceName();
            switch (domain)
            {
                case "monarchgroup":
                    return TMUserDomain.MonarchGroup;
                case "darlingHomes":
                    return TMUserDomain.DarlingHomes;
                default:  //taylorMorrison
                    return TMUserDomain.TaylorMorrison;
            }
        }

    }
}
