﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.SCHelpers
{
	public class ImageHelper
	{
		public string DrawText(String text, string domain, string type)
		{
			var fileName = string.Format("{0}_{1}_{2}.png", domain, type, text);
			const string webPath = PageUrls.MapIconPath;
			var imgPath = webPath + "/" + fileName;

			if (!File.Exists(HttpContext.Current.Server.MapPath(imgPath)))
			{
				var textAlign = (text.Length == 1 ? 6 : (text.Length == 2 ? 1 : -2));
				var fontSize = text.Length == 3 ? 11 : 12;
				Font font = new Font("Arial", fontSize, FontStyle.Bold, GraphicsUnit.Pixel);
				Color textColor = Color.White;

				//first, create a dummy bitmap just to get a graphics object
				Image img = new Bitmap(1, 1);
				Graphics drawing = Graphics.FromImage(img);
				drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

				//free up the dummy image and old graphics object
				img.Dispose();
				drawing.Dispose();

				img = new Bitmap(26, 35);

				drawing = Graphics.FromImage(img);

				//create a brush for the text
				Brush textBrush = new SolidBrush(textColor);

				var backImageFilePath =
					HttpContext.Current.Server.MapPath(string.Format("/Images/mappins/{0}/{1}.png", domain, type));
				try
				{
					drawing.DrawImage(new Bitmap(backImageFilePath), new Rectangle(-1, -1, 26, 35));
				}
				catch (Exception ex)
				{
				    Log.Error("error on creating image for text:" + text, ex, typeof (ImageHelper));
				}
				

				drawing.DrawString(text, font, textBrush, textAlign, 2);
				drawing.Save();

				textBrush.Dispose();
				drawing.Dispose();

				var path = HttpContext.Current.Server.MapPath(webPath);
				fileName = string.Format("{0}_{1}_{2}.png", domain, type, text);
				var fullPath = System.IO.Path.Combine(path, fileName);
				img.Save(fullPath, ImageFormat.Png);

				imgPath = webPath + "/" + fileName;
			}
			return imgPath;
		}
	}
}