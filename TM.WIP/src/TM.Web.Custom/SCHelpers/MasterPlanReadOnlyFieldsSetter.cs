﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Shell.Applications.ContentEditor.Pipelines.RenderContentEditor;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.SCHelpers
{
    public class MasterPlanReadOnlyFieldsSetter
    {
        private Item _fieldItem;
        private Item _fieldTemplate;
        private Item _currentItem;
        public MasterPlanReadOnlyFieldsSetter(Item fieldItem, Item fieldTemplate, RenderContentEditorArgs arguments)
	{
             _fieldItem = fieldItem;
            _fieldTemplate = fieldTemplate;
            _currentItem = arguments.Item;
	}


	public bool IsFieldReadOnly()
	{
	  /*  var isPlan = IsPlan();
	    if(isPlan)
	    {
	        return false;
              //if (!_currentItem.Fields["Master Plan ID"].HasValue)
              //  return false;

              // var fieldName = _fieldTemplate.Name;
              // return MatchesReadOnlyFields.Any(fieldName, isPlan);
	       
	    }
        else if(IsHomeForSale())
        {
            return

               // Ticket 72783 Comented below to make MasterPlan ID editable.

               // _fieldTemplate.Name == "Master Plan ID" || 
               // _fieldTemplate.Name == "Source" || 
               // _fieldTemplate.ID == SCIDs.PlanBaseFields.PlanBrochureMasterID;

        }*/
	    return false;
	}

       

        private bool IsHomeForSale()
        {
            return _currentItem.TemplateID == SCIDs.TemplateIds.HomeForSalePage;
        }

        private bool IsPlan()
        {
            return _currentItem.TemplateID == SCIDs.TemplateIds.PlanPage;
        }


    }

    public class MatchesReadOnlyFields
    {

        private static IEnumerable<string> _planReadOnlyFields;
        private static IEnumerable<string> _homeForSaleReadOnlyFields; 
        static MatchesReadOnlyFields()
        {
            _planReadOnlyFields = new List<string>
                                      {
                                          //"Number of Stories",
                                          //"Number of Bedrooms",
                                          //"Number of Bathrooms",
                                          //"Number of Half Bathrooms",	
                                          //"Number of Garages",
					  "Source"
		                     
                                      };
	    
        }

	public static bool Any(string fieldName, bool isPlan)
	{
	    if(isPlan)
	    {
	       return _planReadOnlyFields.Any(f => f ==fieldName);
	    }
	   
	    return false;
	}
    }
}
