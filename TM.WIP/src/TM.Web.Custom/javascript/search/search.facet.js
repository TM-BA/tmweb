﻿var parsedsfinfo = "";
var Beds = [];
var Baths = [];
var Garages = [];
var Stories = [];
var parsedMapResponse = "";
var parsedSearchFacets = "";
var parsedMapBounds = "";
var parsedCommunityinfo = "";
var parsedMapCommunityinfo = "";
var parsedComminfo = "";

var communityTmpl = "";
var nocommunityTmpl = "";
$j(document).ready(function () {

    nocommunityTmpl = $j('#nocommunitycard').template();
});




function GetCommunities(searchlocation, sortby, isRealtor, comfav, isClearSession,isHTH) {
	if (searchlocation === undefined)
		searchlocation = "";

	if (sortby === undefined)
		sortby = "1";


	var searchInfo = { "searchLocation": searchlocation, "sortBy": sortby, "favorites": comfav, "isRealtor": isRealtor, "isClearSession": isClearSession, "isHTH": isHTH };
	$j('.facetprogress').show();
	$j('.mapprogress').show();
	$j('#CommunitySearchResult').html("<div class=\"progress\"><div>Loading...</div></div>");
	$j.ajax({
	    type: "POST",
	    data: JSON.stringify(searchInfo),
	    url: "/services/SearchMethods.asmx/GetSearchResults",
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function (data) {
	        var parsedSearchResponse = JSON.parse(data.d);

	        parsedMapBounds = parsedSearchResponse.MapBounds;
	        parsedCommunityinfo = parsedSearchResponse.CommunityInfo;
	        parsedMapCommunityinfo = parsedCommunityinfo; //parsedSearchResponse.MapCommunityInfo;

	        parsedSearchFacets = parsedSearchResponse.SearchFacet;

	        var communities = parsedCommunityinfo;
	        var categories = {};

	        $j('#CommunitySearchResult').html("");

	        if (parsedMapBounds != null) {
	            if (isRealtor == false) {
	                $j.each(communities, function (i, c) {
	                    if (c.MapPinStatus != "DesignCenter") {
	                        if (!categories[c.CompanyName]) categories[c.CompanyName] = [c];
	                        else categories[c.CompanyName].push(c);
	                    }
	                });

	                $j.tmpl(communityTmpl, categories).appendTo('#CommunitySearchResult');
	            }

	            if (parsedMapBounds.MinLatitude !== undefined && parsedMapBounds.MaxLongitude !== undefined && parsedMapBounds.MaxLatitude !== undefined && parsedMapBounds.MinLongitude !== undefined)
	                initializeMap(parsedMapCommunityinfo);

	        }
	        else {
	            if (isRealtor == false) {
	                $j.each(communities, function (i, c) {
	                    if (c.MapPinStatus != "DesignCenter") {
	                        if (!categories[c.CompanyName]) categories[c.CompanyName] = [c];
	                        else categories[c.CompanyName].push(c);
	                    }
	                });

	                $j.tmpl(nocommunityTmpl, categories).appendTo('#CommunitySearchResult');
	            }
	            var defaultMap = parsedSearchResponse.DefaultMapPoint;
	            InitializeDefaultMap(defaultMap.Latitude, defaultMap.Longitude, 12);

	        }
	        BoundSearchFacet(parsedSearchFacets, isRealtor);
	        $j('.facetprogress').hide();
	        $j('.mapprogress').hide();
	    }
	});
}

function GetCommunity(searchlocation, sortby, isRealtor, comfav, isClearSession, isHTH, mycommunity) {
    if (searchlocation === undefined)
        searchlocation = "";

    if (sortby === undefined)
        sortby = "1";

    var searchInfo = { "searchLocation": searchlocation, "sortBy": sortby, "favorites": comfav, "isRealtor": isRealtor, "isClearSession": isClearSession, "isHTH": isHTH };
    $j('.facetprogress').show();
    $j('.mapprogress').show();
    $j('#CommunitySearchResult').html("<div class=\"progress\"><div>Loading...</div></div>");
    $j.ajax({
        type: "POST",
        data: JSON.stringify(searchInfo),
        url: "/services/SearchMethods.asmx/GetSearchResults",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var parsedSearchResponse = JSON.parse(data.d);

            parsedMapBounds = parsedSearchResponse.MapBounds;
            parsedCommunityinfo = parsedSearchResponse.CommunityInfo;

            var communities = parsedCommunityinfo;
            
            if (parsedMapBounds != null) {
                initializeCommunity(communities);
            }
        }
    });
}

function BindSearchFacet() {
	var searchInfo = GetFacetValues();
	var isRealtor = searchInfo.isRealtor == "true" ? true : false;
	$j('.facetprogress').show();
	$j('.mapprogress').show();
	$j('#CommunitySearchResult').html("");
	$j('#CommunitySearchResult').html("<div class=\"progress\"><div>Loading...</div></div>");
	$j.ajax({
	    type: "POST",
	    data: JSON.stringify(searchInfo),
	    url: "/services/SearchMethods.asmx/RefineSearchByFacet",
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function (data) {
	        var parsedSearchResponse = $j.parseJSON(data.d);
	        parsedMapBounds = parsedSearchResponse.MapBounds;
	        parsedCommunityinfo = parsedSearchResponse.CommunityInfo;
	        parsedMapCommunityinfo = parsedCommunityinfo; // parsedSearchResponse.MapCommunityInfo;
	        parsedSearchFacets = parsedSearchResponse.SearchFacet;

	        var communities = parsedCommunityinfo;
	        var categories = {};
	        $j('#CommunitySearchResult').html("");
	        if (parsedMapBounds != null) {
	            if (parsedMapBounds.MinLatitude !== undefined && parsedMapBounds.MaxLongitude !== undefined && parsedMapBounds.MaxLatitude !== undefined && parsedMapBounds.MinLongitude !== undefined)
	                initializeMap(parsedMapCommunityinfo, parsedMapBounds.MinLatitude, parsedMapBounds.MaxLongitude, parsedMapBounds.MaxLatitude, parsedMapBounds.MinLongitude, isRealtor);
	            $j('.mapprogress').hide();
	            if (isRealtor == false) {
	                $j.each(communities, function (i, c) {
	                    if (c.MapPinStatus != "DesignCenter") {
	                        if (!categories[c.CompanyName]) categories[c.CompanyName] = [c];
	                        else categories[c.CompanyName].push(c);
	                    }
	                });

	                $j.tmpl(communityTmpl, categories).appendTo('#CommunitySearchResult');
	            }
	        }
	        else {
	            var defaultMap = parsedSearchResponse.DefaultMapPoint;
	            InitializeDefaultMap(defaultMap.Latitude, defaultMap.Longitude, 12);
	            $j('.mapprogress').hide();
	            if (isRealtor == false) {
	                $j.each(communities, function (i, c) {
	                    if (c.MapPinStatus != "DesignCenter") {
	                        if (!categories[c.CompanyName]) categories[c.CompanyName] = [c];
	                        else categories[c.CompanyName].push(c);
	                    }
	                });

	                $j.tmpl(nocommunityTmpl, categories).appendTo('#CommunitySearchResult');
	            }
	        }
	        $j('.mapprogress').hide();
	        BoundSearchFacet(parsedSearchFacets, isRealtor);
	        $j('.facetprogress').hide();
	    }
	});
}

function SetSliderValues(minPr, maxPr, selminPr, selmaxPr, prTick, minSf, maxSf, selminSf, selmaxSf, sqftTick) {
	$j("#sldrrange-StartingPrice").slider({
		range: true,
		min: minPr,
		max: maxPr,
		step: prTick,
		animate: 'true',
		values: [selminPr, selmaxPr],
		slide: function (event, ui) {
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-StartingPrice', "[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
			BindSearchFacet();
		}
	});
	$j("#startingprice").html("[ $" + $j("#sldrrange-StartingPrice").slider("values", 0).formatMoney(0, ".", ",") + " - $" + $j("#sldrrange-StartingPrice").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minPr == 0 & maxPr == 0) {
		$j("div#sdrpriceRange").css("display", "none");
	} else {
		$j("div#sdrpriceRange").css("display", "block");
	}

	$j("#sldrrange-SquareFeet").slider({
		range: true,
		min: minSf,
		max: maxSf,
		animate: 'true',
		step: sqftTick,
		values: [selminSf, selmaxSf],
		slide: function (event, ui) {
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-SqFt', "[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
			BindSearchFacet();
		}
	});
	$j("#squarefeet").html("[ " + $j("#sldrrange-SquareFeet").slider("values", 0).formatMoney(0, ".", ",") + " - " + $j("#sldrrange-SquareFeet").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minSf == 0 & maxSf == 0) {
		$j("div#sdrSqftRange").css("display", "none");
	}
	else {
		$j("div#sdrSqftRange").css("display", "block");
	}
}

function BoundSearchFacet(parsedVal, isRealtor) {
	//Price and sq.ft slider
	var minPrice = parsedVal.MinPrice;
	var maxPrice = parsedVal.MaxPrice;
	var selminPrice = parsedVal.SelectedMinPrice;
	var selmaxPrice = parsedVal.SelectedMaxPrice;
	var priceTick = parsedVal.PriceTick;
	var minSqft = parsedVal.MinSqFt;
	var maxSqft = parsedVal.MaxSqFt;
	var selminSqft = parsedVal.SelectedMinSqFt;
	var selmaxSqft = parsedVal.SelectedMaxSqFt;
	var sqftTick = parsedVal.SqFtTick;

	SetSliderValues(minPrice, maxPrice, selminPrice, selmaxPrice, priceTick, minSqft, maxSqft, selminSqft, selmaxSqft, sqftTick);

	//cities
	if (isRealtor == false) {
		SetDropDownListByValue(parsedVal.Cities, "selectcity");
	}
	SetDropDownListById(parsedVal.SchoolDistricts, "selectSchoolDistrict");
	SetDropDownListById(parsedVal.CommunityStatus, "selectCommunityStatus");
	SetDropDownListById(parsedVal.Availability, "selectAvailability");
	SetDropDownListByNameValue(parsedVal.BonusRoomsDensSolarium, "selectBonusRoomDen"); 
	
	SetMultiSelectLabels(parsedVal.NumberofGarages, "lblGarages");
	SetMultiSelectLabels(parsedVal.NumberofBedrooms, "lblBeds");
	SetMultiSelectLabels(parsedVal.NumberofBathrooms, "lblBaths");
	SetMultiSelectLabels(parsedVal.NumberofStories, "lblStories");
}

function lblBeds(ctrl) {
	$j(ctrl).addClass("selected");
	Beds.push($j(ctrl).text());
	TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-Beds', $j(ctrl).text());
	BindSearchFacet();
}

function lblBaths(ctrl) {
	$j(ctrl).addClass("selected");
	Baths.push($j(ctrl).text());
	TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-Baths', $j(ctrl).text());
	BindSearchFacet();
}

function lblGarages(ctrl) {
	$j(ctrl).addClass("selected");
	Garages.push($j(ctrl).text());
	TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-Garages', $j(ctrl).text());
	BindSearchFacet();
}

function lblStories(ctrl) {
	$j(ctrl).addClass("selected");
	Stories.push($j(ctrl).text());
	TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-Stories', $j(ctrl).text());
	BindSearchFacet();
}

function ClearSearchFacet() {
	var hndsearchPath = $j("#hndsearchPath").val();
	var hndFavi = $j("#hndFavi").val();
	var isreal = $j("#hndRealtor").val();
	var searchsortby = $j("#searchsortby").val();

	Beds = [];
	Baths = [];
	Garages = [];
	Stories = [];
	
	GetCommunities(hndsearchPath, searchsortby, isreal == "false" ? false : true, hndFavi, true,false);
} 

function GetFacetValues() {
	var hndsearchPath = $j("#hndsearchPath").val();
	var hndFavi = $j("#hndFavi").val();
	var isreal = $j("#hndRealtor").val();

	var hnddivi = $j("#hnddivision").val();
	var area = $j("#" + hnddivi).val();
	if (area == null)
		area = "";
	
	var city = $j("#selectcity").val();
	var searchsortby = $j("#searchsortby").val();
	if (searchsortby === undefined)
		searchsortby = "1";

	
	var price = $j("#startingprice").text();
	var sqft = $j("#squarefeet").text();
	var schooldt = $j("#selectSchoolDistrict").val();
	var comStatus = $j("#selectCommunityStatus").val();
	var availability = $j("#selectAvailability").val();
	var bonusDenSolar = $j("#selectBonusRoomDen").val();

	return { "isRealtor": isreal, "searchPath": hndsearchPath, "favi": hndFavi, "sortby": searchsortby, "area": area, "city": city, "stEdPrices": price, "stEdSqft": sqft, "schooldt": schooldt, "comStatus": comStatus, "beds": Beds, "baths": Baths, "garages": Garages, "stories": Stories, "availability": availability, "bonusDenSolar": bonusDenSolar };
}


function GetCitiesByDivision(val) {
	var searchInfo = { "areaId": val };

	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetCitiesByDivision",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedval = $j.parseJSON(data.d);
			SetDropDownListById(parsedval, "selectcity");
			BindSearchFacet();
		}
	});
}





