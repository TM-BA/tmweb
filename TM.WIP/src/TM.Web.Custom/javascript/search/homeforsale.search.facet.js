﻿var parsedsfinfo = "";
var Beds = [];
var Baths = [];
var Garages = [];
var Stories = [];


function GetInventorySearchResults(path, sortby, isRealtor, isClearSession) {
	if (path === undefined)
		path = "";

	if (sortby === undefined)
		sortby = "2";
	
	$j('.facetprogress').show();

	var searchInfo = { "sitecorePath": path, "sortBy": sortby, "isRealtor": isRealtor, "isClearSession": isClearSession };
	$j('#homeforsaleSearchResult').html("<div class=\"progress\"><div>Loading...</div></div>");
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetInventorySearchResults",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: onSuccessInventorySearch
	});
}


function onSuccessInventorySearch(data, status) {
	var parsedRlrrchRns = JSON.parse(data.d);
	
	//var hfs = {};
	//hfs.value = parsedRlrrchRns.HomeForSaleInfo;

	var parsedinfo = parsedRlrrchRns.HomeForSaleInfo;
	
	BoundHomeforSaleSearchFacet(parsedRlrrchRns.SearchFacet);
	
	$j("#homecount").html("Found " + parsedRlrrchRns.HomeForSaleInfo.length + " homes");

	$j('#homeforsaleSearchResult').html("");

	var homes = parsedinfo;
	var categories = {};
	$j.each(homes, function (i, c) {
		if (!categories[c.CompanyName]) categories[c.CompanyName] = [c];
		else categories[c.CompanyName].push(c);
	});
	
	if (parsedRlrrchRns.HomeForSaleInfo.length > 0) {
		$j('#homeforsalecard').tmpl(categories).appendTo('#homeforsaleSearchResult');
		
		//$j('#homeforsalecard').tmpl(hfs).appendTo('#homeforsaleSearchResult');
	} else {
		$j('#nohomeforsalecard').tmpl(categories).appendTo('#homeforsaleSearchResult');
		//$j('#nohomeforsalecard').tmpl(hfs).appendTo('#homeforsaleSearchResult');
	}
	$j('.facetprogress').hide();
}

function GetHsfFacetValues() {
	var isreal = $j("#hndRealtor").val();
	var hnddivi = $j("#hnddivision").val();
	var hndsearchPath = $j("#hndsearchPath").val();
	var area = $j("#" + hnddivi).val();
	if (area == null) {
		area = "";
	} else {
		hndsearchPath = "";
	}


	var city = $j("#selectcity").val();
	var searchsortby = $j("#searchsortby").val();
	if (searchsortby === undefined)
		searchsortby = "1";

	var price = $j("#startingprice").text();
	var sqft = $j("#squarefeet").text();
	var schooldt = $j("#selectSchoolDistrict").val();
	
	var availability = $j("#selectAvailability").val();
	var bonusDenSolar = $j("#selectBonusRoomDen").val();


	return { "isRealtor": isreal, "searchPath": hndsearchPath, "sortby": searchsortby, "area": area, "city": city, "stEdPrices": price, "stEdSqft": sqft, "schooldt": schooldt, "beds": Beds, "baths": Baths, "garages": Garages, "stories": Stories, "availability": availability, "bonusDenSolar": bonusDenSolar };
}


function RefineInventorySearchResults() {
	var searchInfo = GetHsfFacetValues();
	
	$j('.facetprogress').show();
	$j('#homeforsaleSearchResult').html("");
	$j('#homeforsaleSearchResult').html("<div class=\"progress\"><div>Loading...</div></div>");

	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/RefineHfSSearchResults",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedRlrrchRns = JSON.parse(data.d);

			var hfs = {};
			hfs.value = parsedRlrrchRns.HomeForSaleInfo;

			$j("#homecount").html("Found " + parsedRlrrchRns.HomeForSaleInfo.length + " homes");

			$j('#homeforsaleSearchResult').html("");
			if (parsedRlrrchRns.HomeForSaleInfo.length > 0)
				$j('#homeforsalecard').tmpl(hfs).appendTo('#homeforsaleSearchResult');
			else
				$j('#nohomeforsalecard').tmpl(hfs).appendTo('#homeforsaleSearchResult');

			BoundHomeforSaleSearchFacet(parsedRlrrchRns.SearchFacet);
			$j('.facetprogress').hide();
		}
	});
}

function SetSliderValues(minPr, maxPr, selminPr, selmaxPr, prTick, minSf, maxSf, selminSf, selmaxSf, sqftTick) {
	$j("#sldrrange-StartingPrice").slider({
		range: true,
		min: minPr,
		max: maxPr,
		step: prTick,
		animate: 'true',
		values: [selminPr, selmaxPr],
		slide: function (event, ui) {
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			$j("#sldrrange-StartingPrice > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-StartingPrice > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#startingprice").html("[ $" + ui.values[0].formatMoney(0, ".", ",") + " - $" + ui.values[1].formatMoney(0, ".", ",") + " ]");
			RefineInventorySearchResults();
		}
	});
	$j("#startingprice").html("[ $" + $j("#sldrrange-StartingPrice").slider("values", 0).formatMoney(0, ".", ",") + " - $" + $j("#sldrrange-StartingPrice").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minPr == 0 & maxPr == 0) {
		$j("div#sdrpriceRange").css("display", "none");
	} else {
		$j("div#sdrpriceRange").css("display", "block");
	}

	$j("#sldrrange-SquareFeet").slider({
		range: true,
		min: minSf,
		max: maxSf,
		animate: 'true',
		step: sqftTick,
		values: [selminSf, selmaxSf],
		slide: function (event, ui) {
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
		},
		stop: function (event, ui) {
			$j("#sldrrange-SquareFeet > a:nth-child(2)").attr("title", "$" + ui.values[0].formatMoney(0, ".", ","));
			$j("#sldrrange-SquareFeet > a:nth-child(3)").attr("title", "$" + ui.values[1].formatMoney(0, ".", ","));
			$j("#squarefeet").html("[" + ui.values[0].formatMoney(0, ".", ",") + " - " + ui.values[1].formatMoney(0, ".", ",") + " ]");
			RefineInventorySearchResults();
		}
	});
	$j("#squarefeet").html("[ " + $j("#sldrrange-SquareFeet").slider("values", 0).formatMoney(0, ".", ",") + " - " + $j("#sldrrange-SquareFeet").slider("values", 1).formatMoney(0, ".", ",") + " ]");

	if (minSf == 0 & maxSf == 0) {
		$j("div#sdrSqftRange").css("display", "none");
	}
	else {
		$j("div#sdrSqftRange").css("display", "block");
	}
}

function BoundHomeforSaleSearchFacet(parsedVal) {
	//Price and sq.ft slider
	var minPrice = parsedVal.MinPrice;
	var maxPrice = parsedVal.MaxPrice;
	var selminPrice = parsedVal.SelectedMinPrice;
	var selmaxPrice = parsedVal.SelectedMaxPrice;
	var priceTick = parsedVal.PriceTick;
	var minSqft = parsedVal.MinSqFt;
	var maxSqft = parsedVal.MaxSqFt;
	var selminSqft = parsedVal.SelectedMinSqFt;
	var selmaxSqft = parsedVal.SelectedMaxSqFt;
	var sqftTick = parsedVal.SqFtTick;
	var division = parsedVal.Division;
	
	if (division != "") {
		var hnddivi = $j("#hnddivision").val();
		$j("#" + hnddivi).val(division);
	}

	SetSliderValues(minPrice, maxPrice, selminPrice, selmaxPrice, priceTick, minSqft, maxSqft, selminSqft, selmaxSqft, sqftTick);

	//cities
	SetDropDownListByValue(parsedVal.Cities, "selectcity");
	SetDropDownListById(parsedVal.SchoolDistricts, "selectSchoolDistrict");
	SetDropDownListById(parsedVal.CommunityStatus, "selectCommunityStatus");
	SetDropDownListById(parsedVal.Availability, "selectAvailability");
	SetDropDownListByNameValue(parsedVal.BonusRoomsDensSolarium, "selectBonusRoomDen"); 
	
	SetMultiSelectLabels(parsedVal.NumberofGarages, "lblGarages");
	SetMultiSelectLabels(parsedVal.NumberofBedrooms, "lblBeds");
	SetMultiSelectLabels(parsedVal.NumberofBathrooms, "lblBaths");
	SetMultiSelectLabels(parsedVal.NumberofStories, "lblStories");
}


function lblBeds(ctrl) {
	$j(ctrl).addClass("selected");
	Beds.push($j(ctrl).text());
	RefineInventorySearchResults();
}

function lblBaths(ctrl) {
	$j(ctrl).addClass("selected");
	Baths.push($j(ctrl).text());
	RefineInventorySearchResults();
}

function lblGarages(ctrl) {
	$j(ctrl).addClass("selected");
	Garages.push($j(ctrl).text());
	RefineInventorySearchResults();
}

function lblStories(ctrl) {
	$j(ctrl).addClass("selected");
	Stories.push($j(ctrl).text());
	RefineInventorySearchResults();
}

function ClearSearchFacet() {
	var hndsearchPath = $j("#hndsearchPath").val();
	var searchsortby = $j("#searchsortby").val();
	var isreal = $j("#hndRealtor").val();
	
	var hnddivi = $j("#hnddivision").val();
	$j("#" + hnddivi).val($j("#hdnSelectedDiv").val());
	
	Beds = [];
	Baths = [];
	Garages = [];
	Stories = [];

	GetInventorySearchResults(hndsearchPath, searchsortby, isreal, true);
} 


function GetCitiesByDivision(val) {
	var searchInfo = { "areaId": val };
	var searchsortby = $j("#searchsortby").val();
	var isreal = $j("#hndRealtor").val();
	
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetCitiesByDivision",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedval = JSON.parse(data.d);
			SetDropDownListById(parsedval, "selectcity");
			//RefineInventorySearchResults();
			GetInventorySearchResults(val, searchsortby, isreal, false);
		}
	});
}


function GetLandingPageInfos(val, campaignType) {
	var searchInfo = { "areaId": val, "campaignType": campaignType };

	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetLandingPageInfos",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedval = JSON.parse(data.d);
			$j("#ihccard").html("");
			$j("#ihccard").html(parsedval.IhcCard);
			$j("#campaigndtl").html("");
			$j("#campaigndtl").html(parsedval.CampaignDetails);
			$j("#liveChat").html("");
			$j("#liveChat").html(parsedval.LiveChat);
		}
	});
}