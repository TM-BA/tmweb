﻿
//setup accordion conifg
var AccordionConfig = {};

AccordionConfig.currentItem = '';
AccordionConfig.speed = 1600;
AccordionConfig.currentStateSelected = '';
AccordionConfig.mainTextFadeInSpeed = 1500;
AccordionConfig.currentContentSelected = '';


//setup animation monitor to detect when all animations are running
var AnimationMonitor = {};

AnimationMonitor.runningForEnvironment = false;
AnimationMonitor.runningForEnergy = false;
AnimationMonitor.runningForEfficiency = false;

AnimationMonitor.IsAnimationRunning = function() {
    if (!AnimationMonitor.runningForEnvironment && !AnimationMonitor.runningForEfficiency && !AnimationMonitor.runningForEnergy) {
        return false;
    }

    return true;
}
 
$j(document).ready(function() {

    InitAccordion();

    LoadMapShades();

    InitMap();

    InitFooterLinks();

});



/**********************************************
*
* ACCORDION HELPER FUNCTIONS
*
*********************************************/
//Accordion init
function InitAccordion() {
    $j("#item-btn-environment").click(function() {

        if (AnimationMonitor.IsAnimationRunning() || AccordionConfig.currentItem == "environment") {
            return;
        }

        Hide_Accordion_Content(AccordionConfig.currentItem);
        Hide_Accordion_Item_Text();

        AnimationMonitor.runningForEfficiency = true;
        AnimationMonitor.runningForEnvironment = true;
        AnimationMonitor.runningForEnergy = true;

        $j("#accordion-item-efficiency").animate({ left: "859px" }, { queue: false, duration: AccordionConfig.speed, complete: EfficiencyAnimationComplete }).add(
        $j("#accordion-item-energy").animate({ left: "800px" }, { queue: false, duration: AccordionConfig.speed, complete: EnergyAnimationComplete }).framerate()).add(
        $j("#accordion-item-environment").animate({ left: "0px" }, { queue: false, duration: AccordionConfig.speed,

            //Animation complete
            complete: function() {
                //Show content visibility
                AccordionConfig.currentItem = "environment";
                $j("#item-btn-environment-text").fadeIn(1000, EnvironmentAnimationComplete);

                //fade in environment content
                Show_Accordion_Content(AccordionConfig.currentItem);

            }
        }).framerate()).framerate();

    });


    $j("#accordion-item-energy").click(function() {

        if (AnimationMonitor.IsAnimationRunning() || AccordionConfig.currentItem == "energy") {
            return;
        }

        Hide_Accordion_Content(AccordionConfig.currentItem);
        Hide_Accordion_Item_Text();

        AnimationMonitor.runningForEfficiency = true;
        AnimationMonitor.runningForEnvironment = true;
        AnimationMonitor.runningForEnergy = true;

        $j("#accordion-item-efficiency").animate({ left: "859px" }, { queue: false, duration: AccordionConfig.speed, complete: EfficiencyAnimationComplete }).add(
        $j("#accordion-item-environment").animate({ left: "0px" }, { queue: false, duration: AccordionConfig.speed, complete: EnergyAnimationComplete }).framerate()).add(
        $j("#accordion-item-energy").animate({ left: "59px" }, { queue: false, duration: AccordionConfig.speed,

            //Animation complete
            complete: function() {
                //Show content visibility
                AccordionConfig.currentItem = "energy";
                $j("#item-btn-energy-text").fadeIn(1000, EnvironmentAnimationComplete);

                //fade in engergy content
                Show_Accordion_Content(AccordionConfig.currentItem);

            }

        }).framerate()).framerate();

    });


    $j("#accordion-item-efficiency").click(function() {

        if (AnimationMonitor.IsAnimationRunning() || AccordionConfig.currentItem == "efficiency") {
            return;
        }

        Hide_Accordion_Content(AccordionConfig.currentItem);
        Hide_Accordion_Item_Text();

        AnimationMonitor.runningForEfficiency = true;
        AnimationMonitor.runningForEnvironment = true;
        AnimationMonitor.runningForEnergy = true;

        $j("#accordion-item-environment").animate({ left: "0px" }, { queue: false, duration: AccordionConfig.speed, complete: EnvironmentAnimationComplete }).add(
        $j("#accordion-item-energy").animate({ left: "59px" }, { queue: false, duration: AccordionConfig.speed, complete: EnergyAnimationComplete }).framerate()).add(
        $j("#accordion-item-efficiency").animate({ left: "118px" }, { queue: false, duration: AccordionConfig.speed,

            //Animation complete
            complete: function() {
                //Show content visibility
                AccordionConfig.currentItem = "efficiency";
                $j("#item-btn-efficiency-text").fadeIn(1000, EfficiencyAnimationComplete);

                //fade in efficiency content
                Show_Accordion_Content(AccordionConfig.currentItem);
            }
        }).framerate()).framerate();
    });

}

var EnvironmentAnimationComplete = function() {
    AnimationMonitor.runningForEnvironment = false;
};


var EnergyAnimationComplete = function() {
    AnimationMonitor.runningForEnergy = false;
};

var EfficiencyAnimationComplete = function() {
    AnimationMonitor.runningForEfficiency = false;
};



//Toggle accordion item text visibility
function Hide_Accordion_Item_Text() {
    $j("#item-btn-environment-text").hide();
    $j("#item-btn-energy-text").hide();
    $j("#item-btn-efficiency-text").hide();
}

//Show accordion section content
function Show_Accordion_Content(cur_item_selected) {
    if (cur_item_selected == "environment") {

        $j("#item-content-environment").fadeIn(1500);

        $j("#item-content-environment > div.accordion-item-content").fadeIn(1000, function() {
            $j(this).css("opacity", "0.6");
        });
        
        $j("#item-content-environment > div.accordion-item-content-text").fadeIn(1000);
    }
    else if (cur_item_selected == "energy") {
        $j("#item-content-energy > div.accordion-item-content-text").css({ "margin-left": "470px", "width": "300px" });
        $j("#item-content-energy").fadeIn(1500);
        $j("#item-content-energy > div.accordion-item-content").fadeIn(1000, function() {
            $j(this).css("opacity", "0.6");
        });
        $j("#item-content-energy > div.accordion-item-content-text").fadeIn(1000);
    }
    else if (cur_item_selected == "efficiency") {
        $j("#item-content-effiency > div.accordion-item-content-text").css({ "margin-left": "422px", "width": "337px", "margin-top": "22px" });
        $j("#item-content-effiency").fadeIn(1500);
        $j("#item-content-effiency > div.accordion-item-content").fadeIn(1000, function() {
            $j(this).css("opacity", "0.6");
        });
        $j("#item-content-effiency > div.accordion-item-content-text").fadeIn(1000);
    }
}


//Hide accordion section content
function Hide_Accordion_Content(cur_item_selected) {
    if (cur_item_selected == "environment") {
        $j("#item-content-environment").fadeOut(1500);
        $j("#item-content-environment > div.accordion-item-content").fadeOut(1000);
        $j("#item-content-environment > div.accordion-item-content-text").fadeOut(1000);
    }
    else if (cur_item_selected == "energy") {
        $j("#item-content-energy").fadeOut(1500);
        $j("#item-content-energy > div.accordion-item-content").fadeOut(1000);
        $j("#item-content-energy > div.accordion-item-content-text").fadeOut(1000)
    }
    else if (cur_item_selected == "efficiency") {
        $j("#item-content-effiency").fadeOut(1500);
        $j("#item-content-effiency > div.accordion-item-content").fadeOut(1000);
        $j("#item-content-effiency > div.accordion-item-content-text").fadeOut(1000);
    }
}


/**********************************************
*
* MAP HELPER FUNCTIONS
*
*********************************************/

function InitMap() {

    $j("#map-texas").mouseover(function() {
        if (AccordionConfig.currentStateSelected != "texas") {
            Hide_State_Bubble();
            $j("#map-texas").attr({ "fill": "#3993b7" });
            $j("#bubble-texas").show();
            AccordionConfig.currentStateSelected = "texas";
        }
    });

    $j("#map-florida").mouseover(function() {
        if (AccordionConfig.currentStateSelected != "florida") {
            Hide_State_Bubble();
            $j("#map-florida").attr({ "fill": "#3993b7" });
            $j("#bubble-florida").show();
            AccordionConfig.currentStateSelected = "florida";
        }
    });


    $j("#map-colorado").mouseover(function() {
        if (AccordionConfig.currentStateSelected != "colorado") {
            Hide_State_Bubble();
            $j("#map-colorado").attr({ "fill": "#3993b7" });
            $j("#bubble-colorado").show();
            AccordionConfig.currentStateSelected = "colorado";
        }
    });

    $j("#map-arizona").mouseover(function() {
        if (AccordionConfig.currentStateSelected != "arizona") {
            Hide_State_Bubble();
            $j("#map-arizona").attr({ "fill": "#3993b7" });
            $j("#bubble-arizona").show();
            AccordionConfig.currentStateSelected = "arizona";
        }
    });

    $j("#map-california").mouseover(function() {
        if (AccordionConfig.currentStateSelected != "california") {
            Hide_State_Bubble();
            $j("#map-california").attr({ "fill": "#3993b7" });
            $j("#bubble-california").show();
            AccordionConfig.currentStateSelected = "california";
        }
    });
}

function Hide_State_Bubble() {
    $j("#bubble-texas").hide();
    $j("#bubble-florida").hide();
    $j("#bubble-colorado").hide();
    $j("#bubble-arizona").hide();
    $j("#bubble-california").hide();
    $j("#map-texas").attr({ "fill": "#a8c8ce", "fill-opacity": "0.7" });
    $j("#map-florida").attr({ "fill": "#a8c8ce", "fill-opacity": "0.7" });
    $j("#map-colorado").attr({ "fill": "#a8c8ce", "fill-opacity": "0.7" });
    $j("#map-arizona").attr({ "fill": "#a8c8ce", "fill-opacity": "0.7" });
    $j("#map-california").attr({ "fill": "#a8c8ce", "fill-opacity": "0.7" });
}


/**********************************************
*
* FOOTER HELPER FUNCTIONS
*
*********************************************/
function InitFooterLinks() {
    $j(".panel-link-button").click(function() {
        AccordionConfig.currentContentSelected ='default';
        Hide_Element_Text_Content();
        $j("#default").fadeIn(AccordionConfig.mainTextFadeInSpeed);
    });

    $j("#ft-energy-star-more-info").click(function() {
        if (AccordionConfig.currentContentSelected != 'energy-star') {
            AccordionConfig.currentContentSelected = 'energy-star';
            Hide_Element_Text_Content();
            $j("#energy-star").fadeIn(AccordionConfig.mainTextFadeInSpeed);
        }
    });


    $j("#ft-boomerang-more-info").click(function() {
        if (AccordionConfig.currentContentSelected != 'boomerang') {
            AccordionConfig.currentContentSelected = 'boomerang';
            Hide_Element_Text_Content();
            $j("#boomerang").fadeIn(AccordionConfig.mainTextFadeInSpeed);
        }
    });

    $j("#ft-innovative-bldg-more-info").click(function() {
        if (AccordionConfig.currentContentSelected != 'innov-bldg') {
            AccordionConfig.currentContentSelected = 'innov-bldg';
            Hide_Element_Text_Content();
            $j("#innovative-building").fadeIn(AccordionConfig.mainTextFadeInSpeed);
        }
    });

    $j("#ft-hers-more-info").click(function() {
        if (AccordionConfig.currentContentSelected != 'hers') {
            AccordionConfig.currentContentSelected = 'hers';
            Hide_Element_Text_Content();
            $j("#hers").fadeIn(AccordionConfig.mainTextFadeInSpeed);
        }
    });

    $j("#energy-star-more-disc").click(function() {
        Hide_Element_Text_Content();
        $j("#energy-star-disclaimer").fadeIn(AccordionConfig.mainTextFadeInSpeed);
    });

    $j("#energy-disclaimer-back").click(function() {
        Hide_Element_Text_Content();
        $j("#energy-star").fadeIn(AccordionConfig.mainTextFadeInSpeed);
    });
}

function Hide_Element_Text_Content() {
    $j("#default").hide();
    $j("#energy-star").hide();
    $j("#energy-star-disclaimer").hide();
    $j("#hers").hide();
    $j("#innovative-building").hide();
    $j("#boomerang").hide();
}


/**********************************************
*
* MAP VIA RAPHAEL
*
*********************************************/

function LoadMapShades() {
    var mapJSon = {
        "shades": [

			    { "id": "map-texas", "coor": "91,114,99,116,106,116,109,117,110,110,111,103,111,97,112,93,116,93,121,93,125,93,127,94,127,101,126,104,129,105,133,107,136,107,139,108,143,108,148,108,152,108,155,109,157,110,158,114,159,117,161,121,161,125,161,128,158,131,154,130,154,132,152,134,148,136,144,137,142,139,140,142,139,145,140,149,140,151,135,151,131,149,128,145,128,142,126,139,122,136,122,133,119,130,113,129,110,130,106,132,102,131,97,122,93,119,91,115" },
			    { "id": "map-california", "coor": "24,40,28,41,31,43,34,44,36,45,39,47,40,48,38,52,37,55,35,59,33,61,35,64,38,69,40,72,42,76,44,79,47,83,48,86,51,88,51,90,51,93,49,95,47,97,47,100,41,100,36,99,33,98,31,93,30,89,27,87,24,85,21,82,20,79,19,75,19,71,19,67,19,61,19,55,19,51,20,48,19,45,22,40" },
			    { "id": "map-arizona", "coor": "48,100,49,96,51,94,52,90,53,86,53,84,55,85,57,85,57,81,60,81,64,82,67,83,72,84,76,84,79,85,81,85,82,88,80,91,80,95,79,98,78,101,78,104,77,106,76,109,75,111,75,114,74,116,69,115,64,114,60,110,55,107,51,105,47,103" },
			    { "id": "map-colorado", "coor": "87,66,87,69,86,72,85,75,85,78,83,81,83,84,83,85,87,87,91,87,96,87,100,88,103,88,107,88,111,89,114,89,116,89,118,89,118,85,118,80,119,75,119,72,119,69,114,69,106,68,98,67,88,66" },
			    { "id": "map-florida", "coor": "196,123,197,121,197,119,200,119,202,119,205,118,209,118,212,119,215,118,218,118,220,117,223,117,225,117,227,117,228,115,230,115,232,118,234,120,235,124,236,126,238,129,240,132,242,135,245,138,245,141,244,143,240,142,236,139,235,137,231,136,228,135,227,131,227,128,226,125,224,124,221,124,218,122,217,121,214,122,212,124,210,124,208,123,205,122,202,121,199,123,196,124" }
			]
    }

    var paper = Raphael("main-map", 272, 163);


    paper.customAttributes.id = function(value) {
        return { id: value };
    };



    var p1;
    var cur_coor;
    var cur_id;


    for (i = 0; i <= 4; i++) {
        cur_coor = mapJSon.shades[i].coor;
        cur_id = mapJSon.shades[i].id;

        p1 = paper.polyline(cur_coor);
        p1.node.setAttribute("id", cur_id);
        p1.attr({ "fill": "#a8c8ce", "fill-opacity": "0.7", "cursor": "pointer","stroke":"#dedede" });
        

    }


}

