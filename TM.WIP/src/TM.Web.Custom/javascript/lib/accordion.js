﻿
var $j = jQuery.noConflict();
$j(document).ready(function () {

    //ACCORDION BUTTON ACTION (ON CLICK DO THE FOLLOWING)
    $j('.accordionButton').click(function () {

        //REMOVE THE ON CLASS FROM ALL BUTTONS
        $j('.accordionButton').removeClass('on');

        //NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
        $j('.accordionContent').slideUp('normal');

        //IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
        if ($j(this).next().is(':hidden') == true) {

            //ADD THE ON CLASS TO THE BUTTON
            $j(this).addClass('on');

            //OPEN THE SLIDE
            $j(this).next().slideDown('normal');
        }

    });


    /*** REMOVE IF MOUSEOVER IS NOT REQUIRED ***/

    //ADDS THE .OVER CLASS FROM THE STYLESHEET ON MOUSEOVER 
    $j('.accordionButton').mouseover(function () {
        $j(this).addClass('over');

        //ON MOUSEOUT REMOVE THE OVER CLASS
    }).mouseout(function () {
        $j(this).removeClass('over');
    });

    /*** END REMOVE IF MOUSEOVER IS NOT REQUIRED ***/


    /********************************************************************************************************************
    CLOSES ALL S ON PAGE LOAD
    ********************************************************************************************************************/
    $j('.accordionContent').hide();

});
