﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogPostListEntry.ascx.cs" Inherits="Sitecore.Modules.WeBlog.Layouts.WeBlog.BlogPostListEntry" %>
<%@ Import Namespace="Sitecore.Modules.WeBlog.Items.WeBlog" %>
   
   <div class="blo-ti"> 
        <sc:Image runat="server" ID="EntryImage" Item="<%# (((ListViewDataItem)Container).DataItem as EntryItem) %>" Field="Thumbnail Image" />
         <a href="<%#(((ListViewDataItem)Container).DataItem as EntryItem).Url%>"><%#(((ListViewDataItem)Container).DataItem as EntryItem).Title.Rendered%></a>
         <p><%# Sitecore.Modules.WeBlog.Globalization.Translator.Format("ENTRY_DETAILS", GetPublishDate(((ListViewDataItem)Container).DataItem as EntryItem), (((ListViewDataItem)Container).DataItem as EntryItem).CreatedBy.LocalName)%></p>
    </div>           
         <p><%# GetSummary(((ListViewDataItem)Container).DataItem as EntryItem)%></p>
         <asp:HyperLink ID="BlogPostLink" runat="server"  NavigateUrl='<%# Eval("Url") %>'><%#Sitecore.Modules.WeBlog.Globalization.Translator.Text("READ_MORE")%></asp:HyperLink>
         <br />
