﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using TM.Domain;
using TM.Domain.Entities;
using TM.Domain.Entities.DataTemplate.Common;
using TM.Utils.Extensions;
using TM.Web.Custom.Constants;

namespace TM.Web.Custom.CrmIntegration
{
    public class PlanIntegrater
    {
	  private readonly Database _sourceDB;
        private readonly Database _targetDB;

        public PlanIntegrater(Database sourceDB, Database targetDB)
        {
            _sourceDB = sourceDB;
            _targetDB = targetDB;
        }

        public string Integrate(Item plan)
        {
            var currentPlan = new PlanItem(plan);
            var planLegacyID = 0;
            using (var db = new CMSIntegrationEntities())
            {
                planLegacyID = currentPlan.PlanLegacyID.Text.CastAs<int>(0);
                var community = new CommunityItem(currentPlan.InnerItem.Parent);
                var masterPlan = new MasterPlanItem(_sourceDB.GetItem(new ID(currentPlan.MasterPlanID.Raw)));
                var planStatus = currentPlan.PlanStatus.Raw == SCIDs.StatusIds.Status.Active ? 1 : 2;
                var planExists = from p in db.TW_CommunityPlans where p.CommunityPlanID == planLegacyID select p;
                var existingPlan = planExists.FirstOrDefault();
                var twCommunityPlan = existingPlan ?? new TW_CommunityPlans();
                
                
                twCommunityPlan.CommunityID = community.CommunityLegacyID.Text.CastAs<int>();
                var planAdvStatus = currentPlan.StatusforAdvertising.Raw;
		if(planAdvStatus.IsNotEmpty() && ID.IsID(planAdvStatus))
		{
		    var planAdvStatusLookupItemID = _sourceDB.GetItem(new ID(planAdvStatus));
		    var planAdvStatusLookupItem = new LookupItem(planAdvStatusLookupItemID);
	            twCommunityPlan.HomeOfferingStatusID = planAdvStatusLookupItem.CMSId.Text.CastAs<int>(0);
		}

                var bhiProductTypeLookupItem = new LookupItem(_sourceDB.GetItem(new ID(masterPlan.BHIProductType.Raw)));

                twCommunityPlan.PlanBHITypeID = bhiProductTypeLookupItem.CMSId.Text.CastAs<int>();
                var bhiProductDisplayTypeLookupItem = new LookupItem(_sourceDB.GetItem(new ID(masterPlan.BHIProductDisplayType.Raw)));
                twCommunityPlan.PlanBHIDisplayTypeID = bhiProductDisplayTypeLookupItem.CMSId.Text.CastAs<int>();
                twCommunityPlan.Name = currentPlan.PlanName.Text.Left(100);
                twCommunityPlan.Price = currentPlan.PricedfromValue;
                twCommunityPlan.SqFt = currentPlan.PlanBase.SquareFootage.Text.CastAs<int>();
                twCommunityPlan.BedroomCount = currentPlan.PlanBase.NumberofBedrooms.Rendered.CastAs<int>();
                twCommunityPlan.BathroomCount = currentPlan.PlanBase.NumberofBathrooms.Rendered.CastAs<int>();
                twCommunityPlan.HalfBathroomCount = currentPlan.PlanBase.NumberofHalfBathrooms.Rendered.CastAs<int>();
                twCommunityPlan.NumberOfStories = currentPlan.PlanBase.NumberofStories.Rendered.CastAs<int>();
                twCommunityPlan.GarageCapacity = currentPlan.PlanBase.NumberofGarages.Rendered.CastAs<int>();
                if (currentPlan.PlanBase.GarageEntryLocation.Rendered.IsNotEmpty())
                {
                    twCommunityPlan.GarageEntryCode = currentPlan.PlanBase.GarageEntryLocation.Item.DisplayName.Left(1);
                }
                if (currentPlan.PlanBase.MasterBedroomLocation.Rendered.IsNotEmpty())
                {
                    twCommunityPlan.MasterbedroomLocation = currentPlan.PlanBase.MasterBedroomLocation.Item.DisplayName.Left(1);
                }

                //http://bits.builderhomesite.com/production/default.asp?63765
                var isMonarch = plan.Paths.FullPath.ToLowerInvariant().Contains("monarch");

                twCommunityPlan.LivingAreasCount = isMonarch? 1: currentPlan.PlanBase.NumberofLivingAreas.Rendered.CastAs<int>();
                twCommunityPlan.DiningAreasCount = currentPlan.PlanBase.NumberofDiningAreas.Rendered.CastAs<int>();
                twCommunityPlan.Width = masterPlan.PlanWidth;
                twCommunityPlan.Depth = masterPlan.PlanDepth;
                twCommunityPlan.Description = currentPlan.PlanBase.PlanDescription.Text.Left(1500);
                twCommunityPlan.IsActive = currentPlan.PlanBase.StatusforSearch.Raw==SCIDs.StatusIds.Status.Active;
                twCommunityPlan.NumberOfBonusRooms = currentPlan.PlanBase.NumberofDens.Rendered.CastAs<int>() +
                                                     currentPlan.PlanBase.NumberofSolariums.Rendered.CastAs<int>() +
                                                     currentPlan.PlanBase.NumberofBonusRooms.Rendered.CastAs<int>();
		if(existingPlan==null)
		{
		    db.AddToTW_CommunityPlans(twCommunityPlan);
		}

                db.SaveChanges();
                if (existingPlan == null)
                {
                    var newPlanLegacyID = twCommunityPlan.CommunityPlanID;
                    using (new EventDisabler())
                    using (new EditContext(plan))
                    {
                        var legacyID = currentPlan.PlanLegacyID.Field.InnerField.ID;
                        plan.Fields[legacyID].Value =
                            newPlanLegacyID.ToString(CultureInfo.InvariantCulture);
                        planLegacyID = newPlanLegacyID;
                    }
                }
            }
            return planLegacyID.ToString(CultureInfo.InvariantCulture);
        }
    }
}