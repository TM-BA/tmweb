﻿var infowindow;
var map;
NS("TM.SearchMap");

function ZoomMap(lat, lng) {
    var latLng = new google.maps.LatLng(lat, lng);
    map.setCenter(latLng);
  //  map.setZoom(15);

    $j('html, body').animate({
        scrollTop: $j("#searchmap").offset().top
    }, 'slow');
}


//Single Point

function InitializeDefaultMap(lat, lng, zoom) {
    var latLng = new google.maps.LatLng(lat, lng);
    var myOptions = {
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latLng,
        zoom: zoom
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
}

//Bounds
var InfoBox;
var myCommunity;
function initializeMap(communities) {
    InfoBox = IB();
    var tmmap = new TM.SearchMap(communities,true);
    tmmap.execute(true);
}

function initializeCommunity(community,communityname) {
    InfoBox = IB();
    myCommunity = communityname;
    var tmmap = new TM.SearchMap(community,false);
    tmmap.execute(false);
}

TM.SearchMap = function (communities, newmap) {

    this.reset();

    this._communities = communities;

    var myOptions = {
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: 9,
        scrollwheel: false

    };

    var mapElement = document.getElementById("map_canvas");
    if (newmap)
    map = new google.maps.Map(mapElement, myOptions);
    this.Defaultap = map;
    if (screen.width <= 579) {
        map.setOptions({ draggable: false });
    }
};

TM.SearchMap.prototype = {
    execute: function (isDivision) {

        if (isDivision)
        {
        this.createMarkers();
        var styles = [{
            url: "/images/mappins/" + this._communities[0].CompanyNameAbbreviation + ".png",
            height: 55,
            width: 55,
            anchor: [32, 0],
            textColor: (this._communities[0].CompanyNameAbbreviation != "dh") ? '#EE5F84' : '#915E48',
            textSize: 1
        }];
        var clusterOptions = {
            zoomOnClick: false,
            gridSize: (this._communities[0].CompanyNameAbbreviation != "dh") ? 35 : 35,
            styles: styles,
            title: "Multiple communities available. Click to see more",
            minimumClusterSize: (this._communities[0].CompanyNameAbbreviation != "dh") ? 4 : 4
        };
        /*see markercluster.js */
        TM.SearchMap._markerClusterer = new MarkerClusterer(map, TM.SearchMap._markers, clusterOptions);
        if (TM.SearchMap._designCenterMarkers && TM.SearchMap._designCenterMarkers.length > 0) {
            var designCenterMarker = TM.SearchMap._designCenterMarkers[0]
            TM.SearchMap._designCenterMarkerClusterer = new MarkerClusterer(map, TM.SearchMap._designCenterMarkers, {
                zoomOnClick: false,
                gridSize: 10,
                styles: [{
                    url: designCenterMarker.icon,
                    height: 32,
                    width: 32,
                    anchor: [32, 0],
                    textSize: 1
                }],
                minimumClusterSize: 1
            });
        }

        TM.SearchMap._markerClusterer.fitMapToMarkers(TM.SearchMap._designCenterMarkers);
        this.attachEvents();
        }
        else 
        {
            this.createMarkersCommunity(mycommunity);
        }
    },

    createMarkers: function () {
        var thisCache = this;
        var communities = this._communities;
        $j.each(communities, function (index, community) {
            thisCache.createMarker(community);
        });
    },

    createMarkersCommunity: function (mycommunity) {
        var thisCache = this;
        var communities = this._communities;
        $j.each(communities, function (index, community) {
            if (community.Name == mycommunity)
            thisCache.createMarker(community);
        });
    },

    createMarker: function (community) {
        var latlng = new google.maps.LatLng(community.CommunityLatitude, community.CommunityLongitude);
        var title = community.Name;
        var that = this;
        var newmarker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: (community.MapPinStatus != "DesignCenter") ? title : community.CommunityName,
            content: (community.MapPinStatus != "DesignCenter") ? that.makeFlyout(community.CommunityFlyouts, community.Name) : this.makeDesignCenterFlyout(community),
            icon: community.DynamicMapIcon,
            animation: google.maps.Animation.DROP,
            id: community.CommunityLegacyID
        });

        google.maps.event.addListener(newmarker, "click", function () {
            if (infowindow)
                infowindow.close();
            infowindow = new InfoBox({
                content: newmarker.content,
                pixelOffset: new google.maps.Size(0, -50),
                alignBottom: true,
            });
            infowindow.open(map, newmarker);
        });

        if (community.MapPinStatus != "DesignCenter" || (typeof(mobilecheck) === "function" && mobilecheck())) {
            TM.SearchMap._markers.push(newmarker);
        } else {
            TM.SearchMap._designCenterMarkers.push(newmarker);
        }
        
    },

    makeFlyout: function (flyout, name) {
        var obj = $j.parseJSON(flyout);
        var content = "<div id=\"content\">" +
            "<div>" +
                "<a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href='{0}' {3}><img src='{1}' alt='{2}'></a>" +
                    "</div>" +
                        "<a onclick=\"TM.Common.GAlogevent('Search', 'Click', 'MapSearch', '');\" href=\"{0}\" {3} class=\"hdr\">{2}</a>" +
                            "<div>{4}</div>" +
                                "</div>";
        content = String.format(content, obj.url, obj.imgsrc, name, obj.target, obj.facet);

        return content;

    },

    makeDesignCenterFlyout: function (community) {
        var content = "<div id=\"content\"><strong>" +
		     community.CommunityName+"</strong><br/>"+
		     community.StreetAddress1 + "<br/>" +
                     community.City + ", " + community.StateProvinceAbbreviation + " " + community.ZipPostalCode + "<br/>" +
                     community.Phone + "<br/><br/>" +
                     community.OfficeHoursDay
                         + "</div>" ;

        return content;
    },

    getInfoWindowContent: function (markers) {
        var contentDetails = '';
        for (var i = 0; i < markers.length; i++) {
            var marker = markers[i];
            if (typeof marker != "undefined") {
                var flyOut = $j(marker.content);
                var communityNumber = $j('<div>').append($j("<img>").attr("src", marker.icon).css({ "width": "19px", "height": "24px" }).clone()).html();
                // var a = (marker.MapPinStatus.toLowerCase() == "designcenter") ? "<b>" + marker.title + "</b>" : $j('<div>').append(flyOut.find("a.hdr").clone()).html();
                var a = "";
                a = $j('<div>').append(flyOut.find("a.hdr").css({ "vertical-align": "top" }).clone()).html();
                //design center;
                if (a === "") {
                    a = "<span style='vertical-align:top'>" + marker.content + "</span>";
                    communityNumber = "";
                }
                var li = "<li style='list-style-type:none'>" + communityNumber + a + "</li>";
                contentDetails += li;
            }

        }
        return contentDetails;
    },

    attachEvents: function () {
        var that = this;
        function showInfoWindow(cluster, spacing) {
            var content = "<div>" + that.getInfoWindowContent(cluster.getMarkers()) + "</div>";
            // Convert lat/long from cluster object to a usable MVCObject
            var info = new google.maps.MVCObject;
            info.set('position', cluster.center_);
            if (infowindow)
                infowindow.close();
            infowindow = new InfoBox({
                content: content,
                pixelOffset: new google.maps.Size(0, spacing),
                alignBottom: true,
            });
            infowindow.open(map, info);
        }
        // Listen for a cluster to be clicked
        google.maps.event.addListener(TM.SearchMap._markerClusterer, 'click', function (cluster) {
            showInfoWindow(cluster, -30);
        });
        if (TM.SearchMap._designCenterMarkerClusterer) {
            google.maps.event.addListener(TM.SearchMap._designCenterMarkerClusterer, 'click', function (cluster) {
                showInfoWindow(cluster, -50);
            });
        }
    },

    reset: function () {
        TM.SearchMap._markers = [];
        TM.SearchMap._designCenterMarkers = [];

        if (TM.SearchMap._markerClusterer) {
            TM.SearchMap._markerClusterer.clearMarkers();
        }
        if (TM.SearchMap._designCenterMarkerClusterer) {
            TM.SearchMap._designCenterMarkerClusterer.clearMarkers();
        }
    }
};

function GetCommunityURLs(isRealtor, communityDetailsUrl, communityId) {
    if (isRealtor)
        return "/Realtor Home for Sale Search/Realtor Home for Search Results?cid=" + communityId;
    else
        return communityDetailsUrl;
}

//http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js
var IB = function () {
    function InfoBox(opt_opts) {

        opt_opts = opt_opts || {};

        google.maps.OverlayView.apply(this, arguments);

        this.content_ = opt_opts.content || "";
        this.disableAutoPan_ = opt_opts.disableAutoPan || false;
        this.maxWidth_ = opt_opts.maxWidth || 0;
        this.pixelOffset_ = opt_opts.pixelOffset || new google.maps.Size(0, 0);
        this.position_ = opt_opts.position || new google.maps.LatLng(0, 0);
        this.zIndex_ = opt_opts.zIndex || null;

        this.boxClass_ = opt_opts.boxClass || "infoBox";
        this.boxStyle_ = opt_opts.boxStyle || {};
        this.closeBoxMargin_ = opt_opts.closeBoxMargin || "0 4px";
        this.closeBoxURL_ = opt_opts.closeBoxURL || "/images/map-close.png";
        if (opt_opts.closeBoxURL === "") {
            this.closeBoxURL_ = "";
        }
        this.infoBoxClearance_ = opt_opts.infoBoxClearance || new google.maps.Size(1, 1);
        this.isHidden_ = opt_opts.isHidden || false;
        this.alignBottom_ = opt_opts.alignBottom || false;
        this.pane_ = opt_opts.pane || "floatPane";
        this.enableEventPropagation_ = opt_opts.enableEventPropagation || false;

        this.div_ = null;
        this.closeListener_ = null;
        this.eventListener1_ = null;
        this.eventListener2_ = null;
        this.eventListener3_ = null;
        this.moveListener_ = null;
        this.contextListener_ = null;
        this.fixedWidthSet_ = null;
    }

    InfoBox.prototype = new google.maps.OverlayView();

    InfoBox.prototype.createInfoBoxDiv_ = function () {

        var bw;
        var me = this;

        // This handler prevents an event in the InfoBox from being passed on to the map.
        //
        var cancelHandler = function (e) {
            e.cancelBubble = true;

            if (e.stopPropagation) {

                e.stopPropagation();
            }
        };

        // This handler ignores the current event in the InfoBox and conditionally prevents
        // the event from being passed on to the map. It is used for the contextmenu event.
        //
        var ignoreHandler = function (e) {

            e.returnValue = false;

            if (e.preventDefault) {

                e.preventDefault();
            }

            if (!me.enableEventPropagation_) {

                cancelHandler(e);
            }
        };

        if (!this.div_) {

            this.div_ = document.createElement("div");
            this.div_.className = "infoBoxContent";
            this.setBoxStyle_();

            if (typeof this.content_.nodeType === "undefined") {
                this.div_.innerHTML = this.getCloseBoxImg_() + this.content_;
            } else {
                this.div_.innerHTML = this.getCloseBoxImg_();
                this.div_.appendChild(this.content_);
            }

            // Add the InfoBox DIV to the DOM
            this.getPanes()[this.pane_].appendChild(this.div_);

            this.addClickHandler_();

            if (this.div_.style.width) {

                this.fixedWidthSet_ = true;

            } else {

                if (this.maxWidth_ !== 0 && this.div_.offsetWidth > this.maxWidth_) {

                    this.div_.style.width = this.maxWidth_;
                    this.div_.style.overflow = "auto";
                    this.fixedWidthSet_ = true;

                } else { // The following code is needed to overcome problems with MSIE

                    bw = this.getBoxWidths_();

                    this.div_.style.width = (this.div_.offsetWidth - bw.left - bw.right + 8) + "px";
                    this.fixedWidthSet_ = false;
                }
            }

            this.panBox_(this.disableAutoPan_);

            if (!this.enableEventPropagation_) {

                // Cancel event propagation.
                //
                this.eventListener1_ = google.maps.event.addDomListener(this.div_, "mousedown", cancelHandler);
                this.eventListener2_ = google.maps.event.addDomListener(this.div_, "click", cancelHandler);
                this.eventListener3_ = google.maps.event.addDomListener(this.div_, "dblclick", cancelHandler);
                this.eventListener4_ = google.maps.event.addDomListener(this.div_, "mouseover", function (e) {
                    this.style.cursor = "default";
                });
            }

            this.contextListener_ = google.maps.event.addDomListener(this.div_, "contextmenu", ignoreHandler);

            /**
             * This event is fired when the DIV containing the InfoBox's content is attached to the DOM.
             * @name InfoBox#domready
             * @event
             */
            google.maps.event.trigger(this, "domready");
        }
    };

    InfoBox.prototype.getCloseBoxImg_ = function () {

        var img = "";

        if (this.closeBoxURL_ !== "") {

            img = "<img";
            img += " src='" + this.closeBoxURL_ + "'";
            img += " style='";
            img += " position: absolute;"; // Required by MSIE
            img += " cursor: pointer;right:0;";
            img += " margin: " + this.closeBoxMargin_ + ";";
            img += "'>";
        }

        return img;
    };

    InfoBox.prototype.addClickHandler_ = function () {

        var closeBox;

        if (this.closeBoxURL_ !== "") {

            closeBox = this.div_.firstChild;
            this.closeListener_ = google.maps.event.addDomListener(closeBox, 'click', this.getCloseClickHandler_());

        } else {

            this.closeListener_ = null;
        }
    };

    InfoBox.prototype.getCloseClickHandler_ = function () {

        var me = this;

        return function (e) {

            // 1.0.3 fix: Always prevent propagation of a close box click to the map:
            e.cancelBubble = true;

            if (e.stopPropagation) {

                e.stopPropagation();
            }

            me.close();

            /**
             * This event is fired when the InfoBox's close box is clicked.
             * @name InfoBox#closeclick
             * @event
             */
            google.maps.event.trigger(me, "closeclick");
        };
    };

    InfoBox.prototype.panBox_ = function (disablePan) {

        var map;
        var bounds;
        var xOffset = 0, yOffset = 0;

        if (!disablePan) {

            map = this.getMap();

            if (map instanceof google.maps.Map) { // Only pan if attached to map, not panorama

                if (!map.getBounds().contains(this.position_)) {
                    // Marker not in visible area of map, so set center
                    // of map to the marker position first.
                    map.setCenter(this.position_);
                }

                bounds = map.getBounds();

                var mapDiv = map.getDiv();
                var mapWidth = mapDiv.offsetWidth;
                var mapHeight = mapDiv.offsetHeight;
                var iwOffsetX = this.pixelOffset_.width;
                var iwOffsetY = this.pixelOffset_.height;
                var iwWidth = this.div_.offsetWidth;
                var iwHeight = this.div_.offsetHeight;
                var padX = this.infoBoxClearance_.width;
                var padY = this.infoBoxClearance_.height;
                var pixPosition = this.getProjection().fromLatLngToContainerPixel(this.position_);

                if (pixPosition.x < (-iwOffsetX + padX)) {
                    xOffset = pixPosition.x + iwOffsetX - padX;
                } else if ((pixPosition.x + iwWidth + iwOffsetX + padX) > mapWidth) {
                    xOffset = pixPosition.x + iwWidth + iwOffsetX + padX - mapWidth;
                }
                if (this.alignBottom_) {
                    if (pixPosition.y < (-iwOffsetY + padY + iwHeight)) {
                        yOffset = pixPosition.y + iwOffsetY - padY - iwHeight;
                    } else if ((pixPosition.y + iwOffsetY + padY) > mapHeight) {
                        yOffset = pixPosition.y + iwOffsetY + padY - mapHeight;
                    }
                } else {
                    if (pixPosition.y < (-iwOffsetY + padY)) {
                        yOffset = pixPosition.y + iwOffsetY - padY;
                    } else if ((pixPosition.y + iwHeight + iwOffsetY + padY) > mapHeight) {
                        yOffset = pixPosition.y + iwHeight + iwOffsetY + padY - mapHeight;
                    }
                }

                if (!(xOffset === 0 && yOffset === 0)) {

                    // Move the map to the shifted center.
                    //
                    var c = map.getCenter();
                    map.panBy(xOffset, yOffset);
                }
            }
        }
    };

    InfoBox.prototype.setBoxStyle_ = function () {

        var i, boxStyle;

        if (this.div_) {

            // Apply style values from the style sheet defined in the boxClass parameter:
            this.div_.className = this.boxClass_;

            // Clear existing inline style values:
            this.div_.style.cssText = "";

            // Apply style values defined in the boxStyle parameter:
            boxStyle = this.boxStyle_;
            for (i in boxStyle) {

                if (boxStyle.hasOwnProperty(i)) {

                    this.div_.style[i] = boxStyle[i];
                }
            }

            // Fix up opacity style for benefit of MSIE:
            //
            if (typeof this.div_.style.opacity !== "undefined" && this.div_.style.opacity !== "") {

                this.div_.style.filter = "alpha(opacity=" + (this.div_.style.opacity * 100) + ")";
            }

            // Apply required styles:
            //
            this.div_.style.position = "absolute";
            this.div_.style.visibility = 'hidden';
            if (this.zIndex_ !== null) {

                this.div_.style.zIndex = this.zIndex_;
            }
        }
    };

    InfoBox.prototype.getBoxWidths_ = function () {

        var computedStyle;
        var bw = { top: 0, bottom: 0, left: 0, right: 0 };
        var box = this.div_;

        if (document.defaultView && document.defaultView.getComputedStyle) {

            computedStyle = box.ownerDocument.defaultView.getComputedStyle(box, "");

            if (computedStyle) {

                // The computed styles are always in pixel units (good!)
                bw.top = parseInt(computedStyle.borderTopWidth, 10) || 0;
                bw.bottom = parseInt(computedStyle.borderBottomWidth, 10) || 0;
                bw.left = parseInt(computedStyle.borderLeftWidth, 10) || 0;
                bw.right = parseInt(computedStyle.borderRightWidth, 10) || 0;
            }

        } else if (document.documentElement.currentStyle) { // MSIE

            if (box.currentStyle) {

                // The current styles may not be in pixel units, but assume they are (bad!)
                bw.top = parseInt(box.currentStyle.borderTopWidth, 10) || 0;
                bw.bottom = parseInt(box.currentStyle.borderBottomWidth, 10) || 0;
                bw.left = parseInt(box.currentStyle.borderLeftWidth, 10) || 0;
                bw.right = parseInt(box.currentStyle.borderRightWidth, 10) || 0;
            }
        }

        return bw;
    };

    InfoBox.prototype.onRemove = function () {

        if (this.div_) {

            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        }
    };

    InfoBox.prototype.draw = function () {

        this.createInfoBoxDiv_();
        var pixPosition = this.getProjection().fromLatLngToDivPixel(this.position_);

        this.div_.style.left = (pixPosition.x + this.pixelOffset_.width - this.div_.clientWidth / 2) + "px";

        if (this.alignBottom_) {
            this.div_.style.bottom = -(pixPosition.y + this.pixelOffset_.height) + "px";
        } else {
            this.div_.style.top = (pixPosition.y + this.pixelOffset_.height) + "px";
        }

        if (this.isHidden_) {

            this.div_.style.visibility = 'hidden';

        } else {

            this.div_.style.visibility = "visible";
        }
    };

    InfoBox.prototype.setOptions = function (opt_opts) {
        if (typeof opt_opts.boxClass !== "undefined") { // Must be first

            this.boxClass_ = opt_opts.boxClass;
            this.setBoxStyle_();
        }
        if (typeof opt_opts.boxStyle !== "undefined") { // Must be second

            this.boxStyle_ = opt_opts.boxStyle;
            this.setBoxStyle_();
        }
        if (typeof opt_opts.content !== "undefined") {

            this.setContent(opt_opts.content);
        }
        if (typeof opt_opts.disableAutoPan !== "undefined") {

            this.disableAutoPan_ = opt_opts.disableAutoPan;
        }
        if (typeof opt_opts.maxWidth !== "undefined") {

            this.maxWidth_ = opt_opts.maxWidth;
        }
        if (typeof opt_opts.pixelOffset !== "undefined") {

            this.pixelOffset_ = opt_opts.pixelOffset;
        }
        if (typeof opt_opts.alignBottom !== "undefined") {

            this.alignBottom_ = opt_opts.alignBottom;
        }
        if (typeof opt_opts.position !== "undefined") {

            this.setPosition(opt_opts.position);
        }
        if (typeof opt_opts.zIndex !== "undefined") {

            this.setZIndex(opt_opts.zIndex);
        }
        if (typeof opt_opts.closeBoxMargin !== "undefined") {

            this.closeBoxMargin_ = opt_opts.closeBoxMargin;
        }
        if (typeof opt_opts.closeBoxURL !== "undefined") {

            this.closeBoxURL_ = opt_opts.closeBoxURL;
        }
        if (typeof opt_opts.infoBoxClearance !== "undefined") {

            this.infoBoxClearance_ = opt_opts.infoBoxClearance;
        }
        if (typeof opt_opts.isHidden !== "undefined") {

            this.isHidden_ = opt_opts.isHidden;
        }
        if (typeof opt_opts.enableEventPropagation !== "undefined") {

            this.enableEventPropagation_ = opt_opts.enableEventPropagation;
        }

        if (this.div_) {

            this.draw();
        }
    };

    InfoBox.prototype.setContent = function (content) {
        this.content_ = content;

        if (this.div_) {

            if (this.closeListener_) {

                google.maps.event.removeListener(this.closeListener_);
                this.closeListener_ = null;
            }

            // Odd code required to make things work with MSIE.
            //
            if (!this.fixedWidthSet_) {

                this.div_.style.width = "";
            }

            if (typeof content.nodeType === "undefined") {
                this.div_.innerHTML = this.getCloseBoxImg_() + content;
            } else {
                this.div_.innerHTML = this.getCloseBoxImg_();
                this.div_.appendChild(content);
            }

            // Perverse code required to make things work with MSIE.
            // (Ensures the close box does, in fact, float to the right.)
            //
            if (!this.fixedWidthSet_) {
                this.div_.style.width = this.div_.offsetWidth + "px";
                if (typeof content.nodeType === "undefined") {
                    this.div_.innerHTML = this.getCloseBoxImg_() + content;
                } else {
                    this.div_.innerHTML = this.getCloseBoxImg_();
                    this.div_.appendChild(content);
                }
            }

            this.addClickHandler_();
        }

        /**
         * This event is fired when the content of the InfoBox changes.
         * @name InfoBox#content_changed
         * @event
         */
        google.maps.event.trigger(this, "content_changed");
    };

    InfoBox.prototype.setPosition = function (latlng) {

        this.position_ = latlng;

        if (this.div_) {

            this.draw();
        }

        /**
         * This event is fired when the position of the InfoBox changes.
         * @name InfoBox#position_changed
         * @event
         */
        google.maps.event.trigger(this, "position_changed");
    };

    InfoBox.prototype.setZIndex = function (index) {

        this.zIndex_ = index;

        if (this.div_) {

            this.div_.style.zIndex = index;
        }

        /**
         * This event is fired when the zIndex of the InfoBox changes.
         * @name InfoBox#zindex_changed
         * @event
         */
        google.maps.event.trigger(this, "zindex_changed");
    };

    InfoBox.prototype.getContent = function () {

        return this.content_;
    };

    InfoBox.prototype.getPosition = function () {

        return this.position_;
    };

    InfoBox.prototype.getZIndex = function () {

        return this.zIndex_;
    };

    InfoBox.prototype.show = function () {

        this.isHidden_ = false;
        if (this.div_) {
            this.div_.style.visibility = "visible";
        }
    };

    InfoBox.prototype.hide = function () {

        this.isHidden_ = true;
        if (this.div_) {
            this.div_.style.visibility = "hidden";
        }
    };

    InfoBox.prototype.open = function (map, anchor) {

        var me = this;

        if (anchor) {

            this.position_ = anchor.position;
            this.moveListener_ = google.maps.event.addListener(anchor, "position_changed", function () {
                me.setPosition(this.getPosition());
            });
        }

        this.setMap(map);

        if (this.div_) {

            this.panBox_();
        }
    };

    InfoBox.prototype.close = function () {

        if (this.closeListener_) {

            google.maps.event.removeListener(this.closeListener_);
            this.closeListener_ = null;
        }

        if (this.eventListener1_) {

            google.maps.event.removeListener(this.eventListener1_);
            google.maps.event.removeListener(this.eventListener2_);
            google.maps.event.removeListener(this.eventListener3_);
            google.maps.event.removeListener(this.eventListener4_);
            this.eventListener1_ = null;
            this.eventListener2_ = null;
            this.eventListener3_ = null;
            this.eventListener4_ = null;
        }

        if (this.moveListener_) {

            google.maps.event.removeListener(this.moveListener_);
            this.moveListener_ = null;
        }

        if (this.contextListener_) {

            google.maps.event.removeListener(this.contextListener_);
            this.contextListener_ = null;
        }

        this.setMap(null);
    };

    return InfoBox;
};