﻿var parsedMapBounds = "";
var parsedCommunityinfo = "";
var parsedPlaninfo = "";
var parsedHomeforSaleinfo = "";
var parsedMapCommunityinfo = "";

function SearchModel() {
	$j('html, body').animate({
		scrollTop: $j("#svgmap").offset().top
	}, 'slow');
}


function SortSaveItemResults(comfav, planfav, hfsfav, sortbydomain, type) {
	var searchInfo = {"comfav": comfav, "planfav": planfav, "hfsfav": hfsfav, "sortbydomain": sortbydomain, "type": type };
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/SortSaveItemResults",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedSearchResponse = JSON.parse(data.d);

			parsedCommunityinfo = parsedSearchResponse.CommunityInfo;
			parsedPlaninfo = parsedSearchResponse.PlanInfo;
			parsedHomeforSaleinfo = parsedSearchResponse.HomeForSaleInfo;

			var domain = sortbydomain.substring(2);

			if (type == "Community") {
				$j("#favCommunitycard_" + domain).html("");
				$j('#tmplCommunity').tmpl(parsedCommunityinfo).appendTo($j("#favCommunitycard_" + domain));
			}
			if (type == "Plan") {
				$j("#favPlancard_" + domain).html("");
				$j('#tmplPlan').tmpl(parsedPlaninfo).appendTo($j("#favPlancard_" + domain));
			}
			if (type == "HomeForSale") {
				$j("#favHfsCard_" + domain).html("");
				$j('#tmplHfs').tmpl(parsedHomeforSaleinfo).appendTo($j("#favHfsCard_" + domain));
			}
		}
	});
}

function GetSaveItemResults(comfav, planfav, hfsfav, aoifav) {

    var searchInfo = { "comfav": comfav, "planfav": planfav, "hfsfav": hfsfav, "aoifav": aoifav};
	$j('.mapprogress').show();
	$j.ajax({
		type: "POST",
		data: JSON.stringify(searchInfo),
		url: "/services/SearchMethods.asmx/GetSaveItemResults",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
			var parsedSearchResponse = JSON.parse(data.d);
			parsedMapBounds = parsedSearchResponse.MapBounds;
			parsedMapCommunityinfo = parsedSearchResponse.MapCommunityInfo;
			parsedCommunityinfo = parsedSearchResponse.CommunityInfo;
			parsedPlaninfo = parsedSearchResponse.PlanInfo;
			parsedHomeforSaleinfo = parsedSearchResponse.HomeForSaleInfo;

			if (parsedMapBounds != null) {
				if (parsedMapBounds.MinLatitude !== undefined && parsedMapBounds.MaxLongitude !== undefined && parsedMapBounds.MaxLatitude !== undefined && parsedMapBounds.MinLongitude !== undefined)
					initializeMap(parsedMapCommunityinfo, parsedMapBounds.MinLatitude, parsedMapBounds.MaxLongitude, parsedMapBounds.MaxLatitude, parsedMapBounds.MinLongitude, false);
				$j('.mapprogress').hide();
			}
			else {
				var defaultMap = parsedSearchResponse.DefaultMapPoint;
				InitializeDefaultMap(defaultMap.Latitude, defaultMap.Longitude, 12);
			}
			$j('.mapprogress').hide();
		}
	});
}


function ScorllItem(target) {
	$j('html, body').animate({
		scrollTop: $j("#" + target).offset().top
	}, 'slow');
}


function removeIFP(id) {
    var searchInfo = { "id": id, "action": "remove" };

    $j.ajax({
        type: "GET",
        data: searchInfo,
        url: "/admin/favorites.aspx",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });

    $j("#span_" + id).hide();
}

