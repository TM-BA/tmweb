(function(n){n.easytabs=function(t,i){var u=this,f=n(t),v={animate:!0,panelActiveClass:"active",tabActiveClass:"active",defaultTab:"li:first-child",animationSpeed:"normal",tabs:"> ul > li",updateHash:!0,cycle:!1,collapsible:!1,collapsedClass:"collapsed",collapsedByDefault:!0,uiTabs:!1,transitionIn:"fadeIn",transitionOut:"fadeOut",transitionInEasing:"swing",transitionOutEasing:"swing",transitionCollapse:"slideUp",transitionUncollapse:"slideDown",transitionCollapseEasing:"swing",transitionUncollapseEasing:"swing",containerClass:"",tabsClass:"",tabClass:"",panelClass:"",cache:!1,panelContext:f},e,h,o,c,s,y={fast:200,normal:400,slow:600},r;u.init=function(){u.settings=r=n.extend({},v,i),r.uiTabs&&(r.tabActiveClass="ui-tabs-selected",r.containerClass="ui-tabs ui-widget ui-widget-content ui-corner-all",r.tabsClass="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all",r.tabClass="ui-state-default ui-corner-top",r.panelClass="ui-tabs-panel ui-widget-content ui-corner-bottom"),r.collapsible&&i.defaultTab!==undefined&&i.collpasedByDefault===undefined&&(r.collapsedByDefault=!1),typeof r.animationSpeed=="string"&&(r.animationSpeed=y[r.animationSpeed]),n("a.anchor").remove().prependTo("body"),f.data("easytabs",{}),u.setTransitions(),u.getTabs(),p(),w(),k(),nt(),tt(),f.attr("data-easytabs",!0)},u.setTransitions=function(){o=r.animate?{show:r.transitionIn,hide:r.transitionOut,speed:r.animationSpeed,collapse:r.transitionCollapse,uncollapse:r.transitionUncollapse,halfSpeed:r.animationSpeed/2}:{show:"show",hide:"hide",speed:0,collapse:"hide",uncollapse:"show",halfSpeed:0}},u.getTabs=function(){var t;u.tabs=f.find(r.tabs),u.panels=n(),u.tabs.each(function(){var i=n(this),e=i.children("a"),f=i.children("a").data("target");i.data("easytabs",{}),f!==undefined&&f!==null?i.data("easytabs").ajax=e.attr("href"):f=e.attr("href"),f=f.match(/#([^\?]+)/)[0].substr(1),t=r.panelContext.find("#"+f),t.length?(t.data("easytabs",{position:t.css("position"),visibility:t.css("visibility")}),t.not(r.panelActiveClass).hide(),u.panels=u.panels.add(t),i.data("easytabs").panel=t):u.tabs=u.tabs.not(i)})},u.selectTab=function(n,t){var e=window.location,o=e.hash.match(/^[^\?]*/)[0],i=n.parent().data("easytabs").panel,f=n.parent().data("easytabs").ajax;r.collapsible&&!s&&(n.hasClass(r.tabActiveClass)||n.hasClass(r.collapsedClass))?u.toggleTabCollapse(n,i,f,t):n.hasClass(r.tabActiveClass)&&i.hasClass(r.panelActiveClass)?r.cache||a(n,i,f,t):a(n,i,f,t)},u.toggleTabCollapse=function(n,t,i,e){u.panels.stop(!0,!0),l(f,"easytabs:before",[n,t,r])&&(u.tabs.filter("."+r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass),n.hasClass(r.collapsedClass)?(!i||r.cache&&n.parent().data("easytabs").cached||(f.trigger("easytabs:ajax:beforeSend",[n,t]),t.load(i,function(i,r,u){n.parent().data("easytabs").cached=!0,f.trigger("easytabs:ajax:complete",[n,t,i,r,u])})),n.parent().removeClass(r.collapsedClass).addClass(r.tabActiveClass).children().removeClass(r.collapsedClass).addClass(r.tabActiveClass),t.addClass(r.panelActiveClass)[o.uncollapse](o.speed,r.transitionUncollapseEasing,function(){f.trigger("easytabs:midTransition",[n,t,r]),typeof e=="function"&&e()})):(n.addClass(r.collapsedClass).parent().addClass(r.collapsedClass),t.removeClass(r.panelActiveClass)[o.collapse](o.speed,r.transitionCollapseEasing,function(){f.trigger("easytabs:midTransition",[n,t,r]),typeof e=="function"&&e()})))},u.matchTab=function(n){return u.tabs.find("[href='"+n+"'],[data-target='"+n+"']").first()},u.matchInPanel=function(n){return n?u.panels.filter(":has("+n+")").first():[]},u.selectTabFromHashChange=function(){var n=window.location.hash.match(/^[^\?]*/)[0],t=u.matchTab(n),i;r.updateHash&&(t.length?(s=!0,u.selectTab(t)):(i=u.matchInPanel(n),i.length?(n="#"+i.attr("id"),t=u.matchTab(n),s=!0,u.selectTab(t)):e.hasClass(r.tabActiveClass)||r.cycle||(n===""||u.matchTab(c).length||f.closest(n).length)&&(s=!0,u.selectTab(h))))},u.cycleTabs=function(t){r.cycle&&(t=t%u.tabs.length,$tab=n(u.tabs[t]).children("a").first(),s=!0,u.selectTab($tab,function(){setTimeout(function(){u.cycleTabs(t+1)},r.cycle)}))},u.publicMethods={select:function(t){var i;(i=u.tabs.filter(t)).length===0?(i=u.tabs.find("a[href='"+t+"']")).length===0&&(i=u.tabs.find("a"+t)).length===0&&(i=u.tabs.find("[data-target='"+t+"']")).length===0&&(i=u.tabs.find("a[href$='"+t+"']")).length===0&&n.error("Tab '"+t+"' does not exist in tab set"):i=i.children("a").first(),u.selectTab(i)}};var l=function(t,i,r){var u=n.Event(i);return t.trigger(u,r),u.result!==!1},p=function(){f.addClass(r.containerClass),u.tabs.parent().addClass(r.tabsClass),u.tabs.addClass(r.tabClass),u.panels.addClass(r.panelClass)},w=function(){var t=window.location.hash.match(/^[^\?]*/)[0],i=u.matchTab(t).parent(),f;i.length===1?(e=i,r.cycle=!1):(f=u.matchInPanel(t),f.length?(t="#"+f.attr("id"),e=u.matchTab(t).parent()):(e=u.tabs.parent().find(r.defaultTab),e.length===0&&n.error("The specified default tab ('"+r.defaultTab+"') could not be found in the tab set."))),h=e.children("a").first(),b(i)},b=function(t){var i,u;r.collapsible&&t.length===0&&r.collapsedByDefault?e.addClass(r.collapsedClass).children().addClass(r.collapsedClass):(i=n(e.data("easytabs").panel),u=e.data("easytabs").ajax,!u||r.cache&&e.data("easytabs").cached||(f.trigger("easytabs:ajax:beforeSend",[h,i]),i.load(u,function(n,t,r){e.data("easytabs").cached=!0,f.trigger("easytabs:ajax:complete",[h,i,n,t,r])})),e.data("easytabs").panel.show().addClass(r.panelActiveClass),e.addClass(r.tabActiveClass).children().addClass(r.tabActiveClass))},k=function(){u.tabs.children("a").bind("click.easytabs",function(t){r.cycle=!1,s=!1,u.selectTab(n(this)),t.preventDefault()})},a=function(n,t,i,e){if(u.panels.stop(!0,!0),l(f,"easytabs:before",[n,t,r])){var v=u.panels.filter(":visible"),h=t.parent(),p,w,a,y,b=window.location.hash.match(/^[^\?]*/)[0];r.animate&&(p=d(t),w=v.length?g(v):0,a=p-w),c=b,y=function(){f.trigger("easytabs:midTransition",[n,t,r]),r.animate&&r.transitionIn=="fadeIn"&&a<0&&h.animate({height:h.height()+a},o.halfSpeed).css({"min-height":""}),r.updateHash&&!s?window.location.hash="#"+t.attr("id"):s=!1,t[o.show](o.speed,r.transitionInEasing,function(){h.css({height:"","min-height":""}),f.trigger("easytabs:after",[n,t,r]),typeof e=="function"&&e()})},!i||r.cache&&n.parent().data("easytabs").cached||(f.trigger("easytabs:ajax:beforeSend",[n,t]),t.load(i,function(i,r,u){n.parent().data("easytabs").cached=!0,f.trigger("easytabs:ajax:complete",[n,t,i,r,u])})),r.animate&&r.transitionOut=="fadeOut"&&(a>0?h.animate({height:h.height()+a},o.halfSpeed):h.css({"min-height":h.height()})),u.tabs.filter("."+r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass),u.tabs.filter("."+r.collapsedClass).removeClass(r.collapsedClass).children().removeClass(r.collapsedClass),n.parent().addClass(r.tabActiveClass).children().addClass(r.tabActiveClass),u.panels.filter("."+r.panelActiveClass).removeClass(r.panelActiveClass),t.addClass(r.panelActiveClass),v.length?v[o.hide](o.speed,r.transitionOutEasing,y):t[o.uncollapse](o.speed,r.transitionUncollapseEasing,y)}},d=function(t){if(t.data("easytabs")&&t.data("easytabs").lastHeight)return t.data("easytabs").lastHeight;var r=t.css("display"),i=t.wrap(n("<div>",{position:"absolute",visibility:"hidden",overflow:"hidden"})).css({position:"relative",visibility:"hidden",display:"block"}).outerHeight();return t.unwrap(),t.css({position:t.data("easytabs").position,visibility:t.data("easytabs").visibility,display:r}),t.data("easytabs").lastHeight=i,i},g=function(n){var t=n.outerHeight();return n.data("easytabs")?n.data("easytabs").lastHeight=t:n.data("easytabs",{lastHeight:t}),t},nt=function(){typeof n(window).hashchange=="function"?n(window).hashchange(function(){u.selectTabFromHashChange()}):n.address&&typeof n.address.change=="function"&&n.address.change(function(){u.selectTabFromHashChange()})},tt=function(){var n;r.cycle&&(n=u.tabs.index(e),setTimeout(function(){u.cycleTabs(n+1)},r.cycle))};u.init()},n.fn.easytabs=function(t){var i=arguments;return this.each(function(){var u=n(this),r=u.data("easytabs");return undefined===r&&(r=new n.easytabs(this,t),u.data("easytabs",r)),r.publicMethods[t]?r.publicMethods[t](Array.prototype.slice.call(i,1)):void 0})}})(jQuery);/*!
 * @name        jQuery Slideshow
 * @author      Matt Hinchliffe <https://github.com/i-like-robots/jQuery-Slideshow>
 * @modified    25/01/2013
 * @version     1.6.1
 */
 (function (jQuery, undefined) {

     'use strict';

     var defaults = {

         // Setup
         carousel: '.carousel',      // Selector for the carousel element.
         items: '.slide',            // Selector for carousel items.
         slideWidth: false,          // Set a fixed width for each slide.
         jumpQueue: true,            // Allow .to() method while animations are queued.
         offset: 1,                  // Starting slide.

         // Controls
         skip: true,                 // Render next/previous skip buttons.
         pagination: true,           // Render pagination.
         gestures: true,            // Allow touch swipe events to control previous/next.
         auto: 3000,                 // Autoplay timeout in milliseconds. Set to false for no autoplay.
         autostop: true,             // Stop autoplay when user manually changes slide.
         hoverPause: false,          // Pause autoplay on hover.
         loop: false,                // Allow slideshow to loop.
         nextText: '',           // Text to display on next skip button
         previousText: '',   // Text to display on previous skip button
         skipCount: 1,       //number of images to skip when you click next

         // Transitions
         transition: 'scroll',       // Specify transition.
         speed: 0,                   // Animation speed between slides in milliseconds.
         easing: 'swing',            // Animation easing between slides.
         visible: 1,                 // Number of slides visible when scrolling.

         // Callbacks
         onupdate: false,            // A callback function to execute on slide change.
         oncomplete: false,          // A callback function to execute on slide transition complete.
         //wrapper
         wrapper: '<div style="position:relative;overflow:hidden;">'
     };

     function Slides(target, options) {

         this.target = target;
         this.$target = jQuery(target);
         this.opts = jQuery.extend({}, defaults, options, this.$target.data());
         this.$carousel = this.$target.children(this.opts.carousel);
         this.$items = this.$carousel.children(this.opts.items);
         this.count = this.$items.length;

         if (this.count > 1) {
             this._init();
         }

        

         return this;
     }

     /**
     * Init
     * @private
     */
     Slides.prototype._init = function () {

         var self = this;

         // $wrapper is a document fragment, not the new DOM reference
         this.$wrapper = this.$carousel.wrap(this.opts.wrapper).parent();
         // Create pagination
         if (this.opts.pagination) {
             this.$pagination = jQuery('<ul class="slides-pagination">');

             for (var i = 0, len = this.count; i < len; i++) {
                 this.$pagination.append('<li><a href="#" data-slides="' + i + '"></a></li>');
                 //this.$pagination.append('<li><a href="#" data-slides="' + i + '">' + (i+1) + '</a></li>');
             }

             this.$target.append(this.$pagination);
         }

         // Create skip links
         if (this.opts.skip) {
             if (this.opts.previousText == null) this.opts.previousText = '';
             if (this.opts.nextText == null) this.opts.nextText = '';
             this.$prev = jQuery('<a href="#" class="slides-prev" data-slides="previous">' + this.opts.previousText + '</a>');
             this.$next = jQuery('<a href="#" class="slides-next" data-slides="next">' + this.opts.nextText + '</a>');
             this.$target.append(this.$next, this.$prev);
         }

         // Controls
         if (this.opts.pagination || this.opts.skip) {

             this.$target.on('click.slides', '[data-slides]', function (e) {

                 e.preventDefault();

                 var $this = jQuery(this);

                 if (!$this.hasClass('disabled')) {
                     self.to($this.data('slides'), true);
                 }
             });
         }

         this.redraw();

         //
         // Gestures modified from Zepto.js <https://github.com/madrobby/zepto/blob/master/src/touch.js>
         // This will be buggy on iOS6 but you can try and work around it: <https://gist.github.com/3755461>
         //
         if (this.opts.gestures && 'ontouchstart' in document.documentElement) {
             this.target.addEventListener('touchstart', function (e) {
                 self.t = {
                     x1: e.touches[0].pageX,
                     el: e.touches[0].target,
                     dif: 0
                 };
             }, false);

             this.target.addEventListener('touchmove', function (e) {
                 self.t.x2 = e.touches[0].pageX;
                 self.t.dif = Math.abs(self.t.x1 - self.t.x2);
                 e.preventDefault();
             }, false);

             this.target.addEventListener('touchend', function () {
                 if (self.t.x2 > 0 && self.t.dif > 30) {
                     self.to(self.t.x1 - self.t.x2 > 0 ? 'next' : 'previous', true);
                 }
             }, false);
         }

         // Autoplay
         if (this.opts.auto) {
             if (this.opts.hoverPause) {
                 this.$target.hover(function () {
                     if (!self.stopped) {
                         self.pause();
                     }
                 }, function () {
                     if (self.paused) {
                         self.play();
                     }
                 });
             }

             this.play();
         }
     };

     /**
     * On Complete
     * @description Update controls and perform callbacks on transition complete.
     * @private
     */
     Slides.prototype._oncomplete = function () {

         this.current = this.future;

         // Highlight current item within pagination
         if (this.opts.pagination) {
             this.$pagination.children()
                .removeClass('selected')
                .eq(this.current)
                .addClass('selected');
         }

         // Disable skip buttons when not looping
         if (this.opts.skip) {
             if (!this.hasNext() && !this.opts.loop) {
                 this.$next.addClass('disabled');
             }
             else {
                 this.$next.removeClass('disabled');
             }

             if (!this.hasPrevious() && !this.opts.loop) {
                 this.$prev.addClass('disabled');
             }
             else {
                 this.$prev.removeClass('disabled');
             }
         }

         if (this.opts.oncomplete) {
             this.opts.oncomplete.call(this, this.current);
         }
     };

     /**
     * Has next
     * @description Are there any slides after current item (ignores loop).
     * @returns {boolean}
     */
     Slides.prototype.hasNext = function () {
         return this.current < (this.count - 1);
     };

     /**
     * Has previous
     * @description Are there any slides previous to current item (ignores loop).
     * @returns {boolean}
     */
     Slides.prototype.hasPrevious = function () {
         return this.current > 0;
     };

     /**
     * Next
     */
     Slides.prototype.next = function () {
         this.to(this.current + this.opts.skipCount);
     };

     /**
     * Previous
     */
     Slides.prototype.previous = function () {
         this.to(this.current - this.opts.skipCount);
     };

     /**
     * Go to slide
     * @param {integer} x
     * @param {boolean} user
     */
     Slides.prototype.to = function (x, user) {

         // Allow while animating?
         if (this.opts.jumpQueue) {
             this.$items.stop(true, true);
         }
         else if (this.$items.queue('fx').length) { // <http://jsperf.com/animated-pseudo-selector/3>
             return;
         }

         // Shortcuts
         if (x === 'next') {
             x = ((this.current + this.opts.skipCount) < this.count) || (this.current == this.count - 1) ? (this.current + this.opts.skipCount) : this.count - 1;
         }
         else if (x === 'previous') {
             x = ((this.current - this.opts.skipCount) < 0) && (this.current != 0) ? 0 : (this.current - this.opts.skipCount);
         }

         if (typeof x !== 'number') {
             x = parseInt(x, 10);
         }

         // Loop
         if (x >= this.$items.length) {
             x = this.opts.loop ? 0 : this.count - 1;
         }
         else if (x < 0) {
             x = this.opts.loop ? this.count - 1 : 0;
         }

         // Stop or reset autoplay
         if (user && !this.stopped) {
             if (this.opts.autostop) {
                 this.stop();
             }
             else if (!this.paused) {
                 this.play();
             }
         }

         // Change slide if different or not yet run
         if (x !== this.current) {
             this.future = x;
             this.transition.execute.call(this);

             if (this.opts.onupdate) {
                 this.opts.onupdate.call(this, x);
             }
         }
     };

     /**
     * Redraw the carousel
     * @param {string} transition
     */
     Slides.prototype.redraw = function (transition) {

         if (this.transition) {
             this.transition.teardown.call(this);
         }

         if (transition) {
             this.opts.transition = transition;
         }

         this.current = undefined;
         this.transition = Transitions[this.opts.transition].call(this);
         this.to(this.opts.offset - 1);
     };

     /**
     * Start autoplay
     */
     Slides.prototype.play = function () {
         var self = this;

         clearInterval(this.timeout);
         this.paused = this.stopped = false;

         this.timeout = setInterval(function () {
             self.to('next');
         }, this.opts.auto);
     };

     /**
     * Pause autoplay
     */
     Slides.prototype.pause = function () {
         this.paused = true;
         clearInterval(this.timeout);
     };

     /**
     * Stop (and clear) autoplay
     */
     Slides.prototype.stop = function () {
         this.stopped = true;
         this.paused = false;
         clearInterval(this.timeout);
     };

     /**
     * Transitions
     * @description Methods are called from a Slides instance. In theory this could be extended or replaced without
     * affecting the slideshow core.
     */
     var Transitions = {

         crossfade: function () {
             var self = this;

             this.$items
                .filter(function (i) {
                    return i !== (self.opts.offset - 1);
                })
                .css('display', 'none');

             this.execute = function () {
                 var $next = this.$items.eq(this.future),
                    $current = this.$items.eq(this.current).css({
                        position: 'absolute',
                        left: 0,
                        top: 0
                    });

                 $next.fadeIn(this.opts.speed, this.opts.easing, function () {
                     self._oncomplete.call(self);
                 });

                 $current.fadeOut(this.opts.speed, this.opts.easing, function () {
                     $current.css('position', '');
                 });
             };

             this.teardown = function () {
                 this.$items.stop(true, true).removeAttr('style');
             };

             return this;
         },

         // Scroll
         scroll: function () {
             var self = this;

             this.$items.css({
                 'float': 'left',
                 'width': this.opts.slideWidth
             });

             this.$carousel.css({
                 'minWidth': (Math.max(this.$items.outerWidth(true),150) + 16) * this.count // setting width property does not work on iOS 4
             });

             this.realcount = this.count;
            // this.count -= this.opts.visible - 1;

             this.execute = function () {
                 var futurePosition = this.$items.eq(this.future).position();
                 if (typeof futurePosition != 'undefined') {
                     var left = this.$items.eq(this.future).position().left + this.$wrapper.scrollLeft();

                     this.$wrapper.animate({
                         scrollLeft: left // Scroll prevents redraws
                     }, this.opts.speed, this.opts.easing, function () {
                         self._oncomplete.call(self);
                     });
                 }
             };

             this.teardown = function () {
                 this.count = this.realcount;
                 this.$carousel.stop(true, true).removeAttr('style');
                
                 this.$items.removeAttr('style');
             };

             return this;
         }
     };

     // jQuery plugin wrapper
     jQuery.fn.slides = function (options) {
         return this.each(function () {
             if (!jQuery.data(this, 'slides')) {
                 jQuery.data(this, 'slides', new Slides(this, options));
             }
         });
     };

 })(jQuery);;(function(n){var t={url:!1,callback:!1,target:!1,duration:120,on:"mouseover"};n.zoom=function(t,i,r){var u,f,o,s,e,h=n(t).css("position");return n(t).css({position:/(absolute|fixed)/.test()?h:"relative",overflow:"hidden"}),n(r).addClass("zoomImg").css({position:"absolute",top:0,left:0,opacity:0,width:r.width,height:r.height,border:"none",maxWidth:"none"}).appendTo(t),{init:function(){u=n(t).outerWidth(),f=n(t).outerHeight(),o=(r.width-u)/n(i).outerWidth(),s=(r.height-f)/n(i).outerHeight(),e=n(i).offset()},move:function(n){var t=n.pageX-e.left,i=n.pageY-e.top;i=Math.max(Math.min(i,f),0),t=Math.max(Math.min(t,u),0),r.style.left=t*-o+"px",r.style.top=i*-s+"px"}}},n.fn.zoom=function(i){return this.each(function(){var r=n.extend({},t,i||{}),h=r.target||this,u=this,e=new Image,s=n(e),o="mousemove",f=!1;(r.url||(r.url=n(u).find("img").attr("src"),r.url))&&(e.onload=function(){function i(i){t.init(),t.move(i),s.stop().fadeTo(n.support.opacity?r.duration:0,1)}function c(){s.stop().fadeTo(r.duration,0)}var t=n.zoom(h,u,e);"grab"===r.on?n(u).mousedown(function(r){n(document).one("mouseup",function(){c(),n(document).unbind(o,t.move)}),i(r),n(document)[o](t.move),r.preventDefault()}):"click"===r.on?n(u).click(function(r){if(!f)return(f=!0,i(r),n(document)[o](t.move),n(document).one("click",function(){c(),f=!1,n(document).unbind(o,t.move)}),!1)}):"toggle"===r.on?n(u).click(function(n){f?c():i(n),f=!f}):(t.init(),n(u).hover(i,c)[o](t.move)),n.isFunction(r.callback)&&r.callback.call(e)},e.src=r.url)})},n.fn.zoom.defaults=t})(window.jQuery);var stlib,_all_services,tpcCookiesEnableCheckingDone,tpcCookiesEnabledStatus,customProduct,stWidgetVersion,stButtons,stWidget;(function(){window.ShareThisEvent={},ShareThisEvent.listen=function(n,t){document.addEventListener?document.addEventListener(n,t,!1):document.documentElement.attachEvent("onpropertychange",function(i){i.propertyName==n&&t()})},ShareThisEvent.trigger=function(n){if(document.createEvent){var t=document.createEvent("Event");t.initEvent(n,!0,!0),document.dispatchEvent(t)}else document.documentElement[n]++}})(),function(n){var t=document.createElement(n),r="async-buttons",i;document.getElementById(r)||(t.type="text/javascript",t.id=r,t.src=("https:"==document.location.protocol?"https://ws.":"http://w.")+"sharethis.com/button/async-buttons.js",i=document.getElementsByTagName("script")[0],i.parentNode.insertBefore(t,i))}("script"),stlib=stlib||{functions:[],functionCount:0,util:{prop:function(n,t){return t?t[n]:function(t){return t[n]}}},dynamicOn:!0,setPublisher:function(n){stlib.publisher=n},setProduct:function(n){stlib.product=n},parseQuery:function(n){var u={},f,i,t,e,r;if(!n)return u;for(f=n.split(/[;&]/),i=0;i<f.length;i++)(t=f[i].split("="),t&&t.length==2)&&(e=unescape(t[0]),r=unescape(t[1]),r=r.replace(/\+/g," "),u[e]=r);return u},getQueryParams:function(){var n=document.getElementById("st_insights_js"),i,t;n&&n.src&&(i=n.src.replace(/^[^\?]+\??/,""),t=stlib.parseQuery(i),stlib.setPublisher(t.publisher),stlib.setProduct(t.product))}},stlib.global={hash:stlib.util.prop("hash",document.location).substr(1)},stlib.getQueryParams(),stlib.debugOn=!1,stlib.debug={count:0,messages:[],debug:function(n,t){t&&typeof console!="undefined"&&console.log(n),stlib.debug.messages.push(n)},show:function(n){for(message in stlib.debug.messages)typeof console!="undefined"&&(n?/ERROR/.test(stlib.debug.messages[message])?console.log(stlib.debug.messages[message]):null:console.log(stlib.debug.messages[message]))},showError:function(){stlib.debug.show(!0)}};var _$d=function(n){stlib.debug.debug(n,stlib.debugOn)},_$d0=function(){_$d(" ")},_$d_=function(){_$d("___________________________________________")},_$d1=function(n){_$d(_$dt()+"| "+n)},_$d2=function(n){_$d(_$dt()+"|  * "+n)},_$de=function(n){_$d(_$dt()+"ERROR: "+n)},_$dt=function(){var n=new Date,t=n.getHours(),i=n.getMinutes(),r=n.getSeconds();return t+":"+i+":"+r+" > "};stlib.allServices={adfty:{title:"Adfty"},allvoices:{title:"Allvoices"},amazon_wishlist:{title:"Amazon Wishlist"},arto:{title:"Arto"},att:{title:"AT&T"},baidu:{title:"Baidu"},blinklist:{title:"Blinklist"},blip:{title:"Blip"},blogmarks:{title:"Blogmarks"},blogger:{title:"Blogger",type:"post"},buddymarks:{title:"BuddyMarks"},buffer:{title:"Buffer"},care2:{title:"Care2"},chiq:{title:"chiq"},citeulike:{title:"CiteULike"},chiq:{title:"chiq"},corkboard:{title:"Corkboard"},dealsplus:{title:"Dealspl.us"},delicious:{title:"Delicious"},digg:{title:"Digg"},diigo:{title:"Diigo"},dzone:{title:"DZone"},edmodo:{title:"Edmodo"},email:{title:"Email"},embed_ly:{title:"Embed.ly"},evernote:{title:"Evernote"},facebook:{title:"Facebook"},fark:{title:"Fark"},fashiolista:{title:"Fashiolista"},flipboard:{title:"Flipboard"},folkd:{title:"folkd.com"},foodlve:{title:"FoodLve"},fresqui:{title:"Fresqui"},friendfeed:{title:"FriendFeed"},funp:{title:"Funp"},fwisp:{title:"fwisp"},google:{title:"Google"},googleplus:{title:"Google +"},google_bmarks:{title:"Bookmarks"},google_reader:{title:"Google Reader"},google_translate:{title:"Google Translate"},hatena:{title:"Hatena"},instapaper:{title:"Instapaper"},jumptags:{title:"Jumptags"},kaboodle:{title:"Kaboodle"},kik:{title:"Kik"},linkagogo:{title:"linkaGoGo"},linkedin:{title:"LinkedIn"},livejournal:{title:"LiveJournal",type:"post"},mail_ru:{title:"mail.ru"},meneame:{title:"Meneame"},messenger:{title:"Messenger"},mister_wong:{title:"Mr Wong"},moshare:{title:"moShare"},myspace:{title:"MySpace"},n4g:{title:"N4G"},netlog:{title:"Netlog"},netvouz:{title:"Netvouz"},newsvine:{title:"Newsvine"},nujij:{title:"NUjij"},odnoklassniki:{title:"Odnoklassniki"},oknotizie:{title:"Oknotizie"},pinterest:{title:"Pinterest"},pocket:{title:"Pocket"},print:{title:"Print"},raise_your_voice:{title:"Raise Your Voice"},reddit:{title:"Reddit"},segnalo:{title:"Segnalo"},sharethis:{title:"ShareThis"},sina:{title:"Sina"},sonico:{title:"Sonico"},startaid:{title:"Startaid"},startlap:{title:"Startlap"},stumbleupon:{title:"StumbleUpon"},stumpedia:{title:"Stumpedia"},typepad:{title:"TypePad",type:"post"},tumblr:{title:"Tumblr"},twitter:{title:"Twitter"},viadeo:{title:"Viadeo"},virb:{title:"Virb"},vkontakte:{title:"Vkontakte"},voxopolis:{title:"VOXopolis"},whatsapp:{title:"WhatsApp"},weheartit:{title:"We Heart It"},wordpress:{title:"WordPress",type:"post"},xerpi:{title:"Xerpi"},xing:{title:"Xing"},yammer:{title:"Yammer"}},stlib.allOauthServices={twitter:{title:"Twitter"},linkedIn:{title:"LinkedIn"},facebook:{title:"Facebook"}},stlib.allNativeServices={fblike:{title:"Facebook Like"},fbrec:{title:"Facebook Recommend"},fbsend:{title:"Facebook Send"},fbsub:{title:"Facebook Subscribe"},foursquaresave:{title:"Foursquare Save"},foursquarefollow:{title:"Foursquare Follow"},instagram:{title:"Instagram Badge"},plusone:{title:"Google +1"},pinterestfollow:{title:"Pinterest Follow"},twitterfollow:{title:"Twitter Follow"},youtube:{title:"Youtube Subscribe"}},stlib.allDeprecatedServices={google_bmarks:{title:"Google Bookmarks"},yahoo_bmarks:{title:"Yahoo Bookmarks"}},stlib.allOtherServices={copy:{title:"Copy Paste"},sharenow:{title:"ShareNow"},sharenow_auto:{title:"Frictionless Sharing"},fbunlike:{title:"Facebook Unlike"}},_all_services=stlib.allServices,stlib.buttonInfo={buttonList:[],addButton:function(n){stlib.buttonInfo.buttonList.push(n)},getButton:function(n){if(isNaN(n))for(c=0;c<stlib.buttonInfo.buttonList.length;c++)stlib.buttonInfo.buttonList[c].service==n&&debug(stlib.buttonInfo.buttonList[c]);else return n>=stlib.buttonInfo.buttonList.length?!1:stlib.buttonInfo.buttonList[n]},clickButton:function(n){if(isNaN(n)){for(c=0;c<stlib.buttonInfo.buttonList.length;c++)if(stlib.buttonInfo.buttonList[c].service==n){if(stlib.buttonInfo.getButton(c).service=="sharethis"||stlib.buttonInfo.getButton(c).service=="email"||stlib.buttonInfo.getButton(c).service=="wordpress")return stlib.buttonInfo.getButton(c).popup(),!0;stlib.buttonInfo.getButton(c).element.childNodes[0].onclick()}}else{if(n>=stlib.buttonInfo.buttonList.length)return!1;stlib.buttonInfo.getButton(n).service=="sharethis"||stlib.buttonInfo.getButton(n).service=="email"||stlib.buttonInfo.getButton(n).service=="wordpress"?stlib.buttonInfo.getButton(n).popup():stlib.buttonInfo.getButton(n).element.childNodes[0].onclick()}},resetButton:function(){stlib.buttonInfo.buttonList=[]},listButton:function(){for(c=0;c<stlib.buttonInfo.buttonList.length;c++)debug(stlib.buttonInfo.buttonList[c])}},stlib.buttonInfo.resetButton(),stlib.messageQueue=function(){var n=this;this.pumpInstance=null,this.queue=[],this.dependencies=["data"],this.sending=!0,this.setPumpInstance=function(n){this.pumpInstance=n},this.send=function(t,i){var r,u;if(typeof t=="string"&&typeof i=="string"&&(_$d_(),_$d1("Queueing message: "+i+": "+t)),typeof t=="string"&&typeof i=="string"?this.queue.push([i,t]):null,this.sending==!1||stlib.browser.ieFallback)if(this.pumpInstance!=null)if(this.dependencies.length>0)for(messageSet in this.queue)this.queue.hasOwnProperty(messageSet)&&this.queue[messageSet][0]==this.dependencies[0]&&this.queue.length>0&&(_$d1("Current Queue Length: "+this.queue.length),r=this.queue.shift(),this.pumpInstance.broadcastSendMessage(r[1]),this.dependencies.shift(),this.sending=!0);else this.queue.length>0&&(_$d1("Current Queue Length: "+this.queue.length),r=this.queue.shift(),this.pumpInstance.broadcastSendMessage(r[1]),this.sending=!0);else _$d_(),_$d1("Pump is null");stlib.browser.ieFallback&&this.queue.length>0&&(u="process"+stlib.functionCount,stlib.functionCount++,stlib.functions[u]=n.process,setTimeout("stlib.functions['"+u+"']()",500))},this.process=function(){_$d1("Processing MessageQueue"),n.sending=!1,_$d(this.queue),n.send()}},stlib.sharer={sharerUrl:("https:"==document.location.protocol?"https://ws.":"http://wd.")+"sharethis.com/api/sharer.php",regAuto:new RegExp(/(.*?)_auto$/),constructParamString:function(){stlib.data.validate(),stlib.hash.checkURL();var t=stlib.data.pageInfo,i="?",n;for(n in t)i+=n+"="+encodeURIComponent(t[n])+"&",_$d1("constructParamStringPageInfo: "+n+": "+t[n]);t=stlib.data.shareInfo;for(n in t)i+=n+"="+encodeURIComponent(t[n])+"&",_$d1("constructParamStringShareInfo: "+n+": "+t[n]);return i+="ts="+(new Date).getTime()+"&",i.substring(0,i.length-1)},stPrint:function(){window.print()},incrementShare:function(){var n=stlib.data.get("url","shareInfo"),t=stlib.data.get("destination","shareInfo"),u="https:"==document.location.protocol?"https://":"http://",i,r;n=n.split("#sthash")[0],i="&service="+encodeURIComponent(t)+"&url="+encodeURIComponent(n),r=u+"count-server.sharethis.com/increment_shares?countType=share&output=false"+i,t!="copy"&&stlib.scriptLoader.loadJavascript(r,function(){})},sharePinterest:function(){if(stlib.sharer.incrementShare(),stlib.data.get("image","shareInfo")==!1||stlib.data.get("image","shareInfo")==null||stlib.data.get("pinterest_native","shareInfo")=="true"){typeof stWidget!="undefined"&&typeof stWidget.closeWidget=="function"&&stWidget.closeWidget(),typeof stcloseWidget=="function"&&stcloseWidget(),typeof stToolbar!="undefined"&&typeof stToolbar.closeWidget=="function"&&stToolbar.closeWidget();var n=document.createElement("script");n.setAttribute("type","text/javascript"),n.setAttribute("charset","UTF-8"),n.setAttribute("src","//assets.pinterest.com/js/pinmarklet.js?r="+Math.random()*99999999),document.body.appendChild(n)}},share:function(n,t){var i=stlib.sharer.constructParamString(),r;_$d_(),_$d1("Initiating a Share with the following url:"),_$d2(stlib.sharer.sharerUrl+i),stlib.sharer.incrementShare(),stlib.data.get("destination","shareInfo")=="print"||stlib.data.get("destination","shareInfo")=="email"||stlib.data.get("destination","shareInfo")=="pinterest"&&stlib.data.get("source","shareInfo").match(/share4xmobile/)==null&&stlib.data.get("source","shareInfo").match(/share4xpage/)==null&&stlib.data.get("source","shareInfo").match(/5xpage/)==null&&(stlib.data.get("image","shareInfo")==!1||stlib.data.get("image","shareInfo")==null)||stlib.data.get("destination","shareInfo")=="snapsets"||stlib.data.get("destination","shareInfo")=="copy"||stlib.data.get("destination","shareInfo")=="plusone"||stlib.data.get("destination","shareInfo").match(stlib.sharer.regAuto)||typeof stlib.nativeButtons!="undefined"&&stlib.nativeButtons.checkNativeButtonSupport(stlib.data.get("destination","shareInfo"))||stlib.data.get("pinterest_native","shareInfo")!=!1&&stlib.data.get("pinterest_native","shareInfo")!=null?(r=new Image(1,1),r.src=stlib.sharer.sharerUrl+i,r.onload=function(){return}):typeof t!="undefined"&&t==!0?window.open(stlib.sharer.sharerUrl+i,(new Date).valueOf(),"scrollbars=1, status=1, height=480, width=640, resizable=1"):window.open(stlib.sharer.sharerUrl+i),n?n():null}},stlib.scriptLoader={loadJavascript:function(n,t){var i=stlib.scriptLoader;i.head=document.getElementsByTagName("head")[0],i.scriptSrc=n,i.script=document.createElement("script"),i.script.setAttribute("type","text/javascript"),i.script.setAttribute("src",i.scriptSrc),i.script.async=!0,window.attachEvent&&document.all?i.script.onreadystatechange=function(){(this.readyState=="complete"||this.readyState=="loaded")&&t()}:i.script.onload=t,i.s=document.getElementsByTagName("script")[0],i.s.parentNode.insertBefore(i.script,i.s)},loadCSS:function(n,t){_$d_(),_$d1("Loading CSS: "+n);var i=stlib.scriptLoader,r;i.head=document.getElementsByTagName("head")[0],i.cssSrc=n,i.css=document.createElement("link"),i.css.setAttribute("rel","stylesheet"),i.css.setAttribute("type","text/css"),i.css.setAttribute("href",n),i.css.setAttribute("id",n),setTimeout(function(){t(),document.getElementById(n)||(r=setInterval(function(){document.getElementById(n)&&(clearInterval(r),t())},100))},100),i.head.appendChild(i.css)}},stlib.browser={iemode:null,firefox:null,firefoxVersion:null,safari:null,chrome:null,opera:null,windows:null,mac:null,ieFallback:/MSIE [6789]/.test(navigator.userAgent),init:function(){var n=navigator.userAgent.toString().toLowerCase();/msie|trident/i.test(n)&&(document.documentMode?stlib.browser.iemode=document.documentMode:(stlib.browser.iemode=5,document.compatMode&&document.compatMode=="CSS1Compat"&&(stlib.browser.iemode=7))),stlib.browser.firefox=n.indexOf("firefox")!=-1&&typeof InstallTrigger!="undefined"?!0:!1,stlib.browser.firefoxVersion=n.indexOf("firefox/5.0")!=-1||n.indexOf("firefox/9.0")!=-1?!1:!0,stlib.browser.safari=n.indexOf("safari")!=-1&&n.indexOf("chrome")==-1?!0:!1,stlib.browser.chrome=n.indexOf("safari")!=-1&&n.indexOf("chrome")!=-1?!0:!1,stlib.browser.opera=window.opera||n.indexOf(" opr/")>=0?!0:!1,stlib.browser.windows=n.indexOf("windows")!=-1?!0:!1,stlib.browser.mac=n.indexOf("macintosh")!=-1?!0:!1},getIEVersion:function(){return stlib.browser.iemode},isFirefox:function(){return stlib.browser.firefox},firefox8Version:function(){return stlib.browser.firefoxVersion},isSafari:function(){return stlib.browser.safari},isWindows:function(){return stlib.browser.windows},isChrome:function(){return stlib.browser.chrome},isOpera:function(){return stlib.browser.opera},isMac:function(){return stlib.browser.mac},isSafariBrowser:function(n,t){var r=n&&n.indexOf("Apple Computer, Inc.")>-1&&t&&!t.match("CriOS"),i=/^((?!chrome|android).)*safari/i.test(t),u=/^((?!firefox|linux))/i.test(t),f=t.indexOf("Mac OS X")>-1||/iPad|iPhone|iPod/.test(t)&&!window.MSStream,e=t.indexOf("Windows NT")>-1&&i;return r&&i&&u&&(f||e)}},stlib.browser.init(),function(n){var t=document.createElement(n),r="async-mobileid",i;stlib.browser.isSafariBrowser(navigator.vendor,navigator.userAgent)&&(document.getElementById(r)||(t.type="text/javascript",t.id=r,t.src=("https:"==document.location.protocol?"https://ws.":"http://w.")+"sharethis.com/button/mobileid.js",i=document.getElementsByTagName("script")[0],i.parentNode.insertBefore(t,i)))}("script"),stlib.browser.mobile={mobile:!1,uagent:null,android:null,iOs:null,silk:null,windows:null,kindle:null,url:null,sharCreated:!1,sharUrl:null,isExcerptImplementation:!1,iOsVer:0,init:function(){this.uagent=navigator.userAgent.toLowerCase(),this.isAndroid()?this.mobile=!0:this.isIOs()?this.mobile=!0:this.isSilk()?this.mobile=!0:this.isWindowsPhone()?this.mobile=!0:this.isKindle()&&(this.mobile=!0)},isMobile:function(){return this.mobile},isAndroid:function(){return this.android===null&&(this.android=this.uagent.indexOf("android")>-1),this.android},isKindle:function(){return this.kindle===null&&(this.kindle=this.uagent.indexOf("kindle")>-1),this.kindle},isIOs:function(){return this.iOs===null&&(this.iOs=this.uagent.indexOf("ipad")>-1||this.uagent.indexOf("ipod")>-1||this.uagent.indexOf("iphone")>-1),this.iOs},isSilk:function(){return this.silk===null&&(this.silk=this.uagent.indexOf("silk")>-1),this.silk},getIOSVersion:function(){return this.isIOs()&&(this.iOsVer=this.uagent.substr(this.uagent.indexOf("os ")+3,5).replace(/\_/g,".")),this.iOsVer},isWindowsPhone:function(){return this.windows===null&&(this.windows=this.uagent.indexOf("windows phone")>-1),this.windows}},stlib.browser.mobile.init(),stlib=stlib||{},stlib.browser=stlib.browser||{},stlib.browser.mobile=stlib.browser.mobile||{},stlib.browser.mobile.handleForMobileFriendly=function(n,t,i){var u,r,o,f,e,y,s,p,w,c,h;if(!this.isMobile())return!1;typeof stLight=="undefined"&&(stLight={},stLight.publisher=t.publisher,stLight.sessionID=t.sessionID,stLight.fpc="");var l=typeof n.title!="undefined"?n.title:encodeURIComponent(document.title),a=typeof n.url!="undefined"?n.url:document.URL,v=t.short_url!=""&&t.short_url!=null?t.short_url:"";if(t.service=="sharethis"){var l=typeof n.title!="undefined"?n.title:encodeURIComponent(document.title),a=typeof n.url!="undefined"?n.url:document.URL,b="";typeof n.summary!="undefined"&&n.summary!=null&&(b=n.summary),u=document.createElement("form"),u.setAttribute("method","GET"),u.setAttribute("action","http://edge.sharethis.com/share4x/mobile.html"),u.setAttribute("target","_blank"),r={url:a,title:l,summary:b,destination:t.service,publisher:stLight.publisher,fpc:stLight.fpc,sessionID:stLight.sessionID,short_url:v},typeof n.image!="undefined"&&n.image!=null&&(r.image=n.image),typeof n.summary!="undefined"&&n.summary!=null&&(r.desc=n.summary),typeof i!="undefined"&&typeof i.exclusive_services!="undefined"&&i.exclusive_services!=null&&(r.exclusive_services=i.exclusive_services),typeof t.exclusive_services!="undefined"&&t.exclusive_services!=null&&(r.exclusive_services=t.exclusive_services),typeof i!="undefined"&&typeof i.services!="undefined"&&i.services!=null&&(r.services=i.services),typeof t.services!="undefined"&&t.services!=null&&(r.services=t.services),o=t,typeof i!="undefined"&&(o=i),typeof o.doNotHash!="undefined"&&o.doNotHash!=null&&(r.doNotHash=o.doNotHash),typeof n.via!="undefined"&&n.via!=null&&(r.via=n.via),r.service=t.service,r.type=t.type,stlib.data&&(f=stlib.json.encode(stlib.data.pageInfo),e=stlib.json.encode(stlib.data.shareInfo),stlib.browser.isFirefox()&&!stlib.browser.firefox8Version()?(f=encodeURIComponent(encodeURIComponent(f)),e=encodeURIComponent(encodeURIComponent(e))):(f=encodeURIComponent(f),e=encodeURIComponent(e)),r.pageInfo=f,r.shareInfo=e);for(y in r)s=document.createElement("input"),s.setAttribute("type","hidden"),s.setAttribute("name",y),s.setAttribute("value",r[y]),u.appendChild(s);return document.body.appendChild(u),u.submit(),!0}return t.service=="email"&&(w=0,stlib.browser.mobile.url=a,stlib.browser.mobile.sharUrl==null&&stlib.browser.mobile.createSharOnPage(),c=v!=""?v+"%0A%0a":"{sharURLValue}%0A%0a",typeof n.summary!="undefined"&&n.summary!=null&&(c+=n.summary+"%0A%0a"),c+="Sent using ShareThis",h="mailto:?",h+="subject="+l,h+="&body="+c,p=setInterval(function(){stlib.browser.mobile.sharUrl!=null&&(clearInterval(p),window.location.href=h.replace("{sharURLValue}",stlib.browser.mobile.sharUrl)),w>500&&(clearInterval(p),window.location.href=h.replace("{sharURLValue}",stlib.browser.mobile.sharUrl)),w++},100)),!0},stlib.browser.mobile.createSharOnPage=function(){if(stlib.browser.mobile.url!==""&&stlib.browser.mobile.url!==" "&&stlib.browser.mobile.url!==null&&!stlib.browser.mobile.sharCreated){var n=["return=json","cb=stlib.browser.mobile.createSharOnPage_onSuccess","service=createSharURL","url="+encodeURIComponent(stlib.browser.mobile.url)];n=n.join("&"),stlib.scriptLoader.loadJavascript(("https:"==document.location.protocol?"https://ws.":"http://wd.")+"sharethis.com/api/getApi.php?"+n,function(){})}},stlib.browser.mobile.createSharOnPage_onSuccess=function(n){n.status=="SUCCESS"?(stlib.browser.mobile.sharCreated=!0,stlib.browser.mobile.sharUrl=n.data.sharURL):stlib.browser.mobile.sharUrl=stlib.browser.mobile.url},tpcCookiesEnableCheckingDone=!1,tpcCookiesEnabledStatus=!0,stlib.cookie={setCookie:function(n,t,i){var y=navigator.userAgent.indexOf("Safari")!=-1&&navigator.userAgent.indexOf("Chrome")==-1,p=navigator.userAgent.indexOf("MSIE")!=-1,v,s,l,r,u,f,e,o,h,c,a;if(y||p){v=i?i*86400:0,s=document.createElement("div"),s.setAttribute("id",n),s.setAttribute("type","hidden"),document.body.appendChild(s),l=document.getElementById(n),r=document.createElement("form");try{u=document.createElement('<iframe name="'+n+'" ><\/iframe>')}catch(w){u=document.createElement("iframe")}u.name=n,u.src="javascript:false",u.style.display="none",l.appendChild(u),r.action=("https:"==document.location.protocol?"https://sharethis.com/":"http://sharethis.com/")+"account/setCookie.php",r.method="POST",f=document.createElement("input"),f.setAttribute("type","hidden"),f.setAttribute("name","name"),f.setAttribute("value",n),r.appendChild(f),e=document.createElement("input"),e.setAttribute("type","hidden"),e.setAttribute("name","value"),e.setAttribute("value",t),r.appendChild(e),o=document.createElement("input"),o.setAttribute("type","hidden"),o.setAttribute("name","time"),o.setAttribute("value",v),r.appendChild(o),r.target=n,l.appendChild(r),r.submit()}else i?(h=new Date,h.setTime(h.getTime()+i*864e5),c="; expires="+h.toGMTString()):c="",a=n+"="+escape(t)+c,a+="; domain="+escape(".sharethis.com")+";path=/",document.cookie=a},setTempCookie:function(n,t,i){var r,u,f;i?(r=new Date,r.setTime(r.getTime()+i*864e5),u="; expires="+r.toGMTString()):u="",f=n+"="+escape(t)+u,f+="; domain="+escape(".sharethis.com")+";path=/",document.cookie=f},getCookie:function(n){var t=document.cookie.match("(^|;) ?"+n+"=([^;]*)(;|$)");return t?unescape(t[2]):!1},deleteCookie:function(n){var e="/",o=".sharethis.com",s,h,u,f,t,i,r;if(document.cookie=n.replace(/^\s+|\s+$/g,"")+"="+(e?";path="+e:"")+(o?";domain="+o:"")+";expires=Thu, 01-Jan-1970 00:00:01 GMT",s=navigator.userAgent.indexOf("Safari")!=-1&&navigator.userAgent.indexOf("Chrome")==-1,h=navigator.userAgent.indexOf("MSIE")!=-1,s||h){u=document.createElement("div"),u.setAttribute("id",n),u.setAttribute("type","hidden"),document.body.appendChild(u),f=document.getElementById(n),t=document.createElement("form");try{i=document.createElement('<iframe name="'+n+'" ><\/iframe>')}catch(c){i=document.createElement("iframe")}i.name=n,i.src="javascript:false",i.style.display="none",f.appendChild(i),t.action=("https:"==document.location.protocol?"https://sharethis.com/":"http://sharethis.com/")+"account/deleteCookie.php",t.method="POST",r=document.createElement("input"),r.setAttribute("type","hidden"),r.setAttribute("name","name"),r.setAttribute("value",n),t.appendChild(r),t.target=n,f.appendChild(t),t.submit()}},deleteAllSTCookie:function(){for(var t=document.cookie,n,t=t.split(";"),i=0;i<t.length;i++)if(n=t[i],n=n.split("="),!/st_optout/.test(n[0])){var r=n[0];document.cookie=r+"=;path=/;domain=.edge.sharethis.com;expires=Thu, 01-Jan-1970 00:00:01 GMT"}},setFpcCookie:function(n,t){var i=new Date,u=i.getFullYear(),o=i.getMonth()+9,s=i.getDate(),r=n+"="+escape(t),f,e;u&&(f=new Date(u,o,s),r+="; expires="+f.toGMTString()),e=stlib.cookie.getDomain(),r+="; domain="+escape(e)+";path=/",document.cookie=r},getFpcCookie:function(n){var t=document.cookie.match("(^|;) ?"+n+"=([^;]*)(;|$)");return t?unescape(t[2]):!1},getDomain:function(){var n=document.domain.split(/\./),t="";return n.length>1&&(t="."+n[n.length-2]+"."+n[n.length-1]),t},checkCookiesEnabled:function(){return tpcCookiesEnableCheckingDone?tpcCookiesEnabledStatus:(stlib.cookie.setTempCookie("STPC","yes",1),tpcCookiesEnabledStatus=stlib.cookie.getCookie("STPC")=="yes"?!0:!1,tpcCookiesEnableCheckingDone=!0,tpcCookiesEnabledStatus)},hasLocalStorage:function(){try{return localStorage.setItem("stStorage","yes"),localStorage.removeItem("stStorage"),!0}catch(n){return!1}}},stlib.fpc={cookieName:"__unam",cookieValue:"",createFpc:function(){var n,i,r,u,f,o,t,e;if(stlib.fpc.setOptout(),!document.domain||document.domain.search(/\.gov/)>0)return!1;if(n=stlib.cookie.getFpcCookie(stlib.fpc.cookieName),n==!1){if(i=Math.round(Math.random()*2147483647),i=i.toString(16),r=(new Date).getTime(),r=r.toString(16),u=window.location.hostname.split(/\./)[1],!u)return!1;f="",f=stlib.fpc.determineHash(u)+"-"+r+"-"+i+"-1",n=f}else o=n,t=o.split(/\-/),t.length==4&&(e=Number(t[3]),e++,n=t[0]+"-"+t[1]+"-"+t[2]+"-"+e);return stlib.cookie.setFpcCookie(stlib.fpc.cookieName,n),stlib.fpc.cookieValue=n,n},setOptout:function(){opt_out=stlib.cookie.getCookie("st_optout"),stlib.data.set("st_optout",opt_out,"pageInfo")},determineHash:function(n){for(var t=0,u=0,r,i=n.length-1;i>=0;i--)r=parseInt(n.charCodeAt(i)),t=(t<<8&268435455)+r+(r<<12),(u=t&161119850)!=0&&(t=t^u>>20);return t.toString(16)}},stlib.validate={regexes:{notEncoded:/(%[^0-7])|(%[0-7][^0-9a-f])|["{}\[\]\<\>\\\^`\|]/gi,tooEncoded:/%25([0-7][0-9a-f])/gi,publisher:/^(([a-z]{2}(-|\.))|)[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i,url:/^(http|https):\/\/([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*)/i,fpc:/^[0-9a-f]{7}-[0-9a-f]{11}-[0-9a-f]{7,8}-[0-9]*$/i,sessionID:/^[0-9]*\.[0-9a-f]*$/i,title:/.*/,description:/.*/,buttonType:/^(chicklet|vcount|hcount|large|custom|button|)$/,comment:/.*/,destination:/.*/,source:/.*/,image:/(^(http|https):\/\/([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*))|^$/i,sourceURL:/^(http|https):\/\/([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*)/i,sharURL:/(^(http|https):\/\/([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*))|^$/i}},stlib.html={encode:function(n){return stlib.html.startsWith(n,"http")?String(n).replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;"):String(n).replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#39;").replace(/</g,"&lt;").replace(/>/g,"&gt;")},startsWith:function(n,t){return n.match("^"+t)==t}},typeof stlib.data=="undefined"&&(stlib.data={bInit:!1,publisherKeySet:!1,pageInfo:{},shareInfo:{},resetPageData:function(){stlib.data.pageInfo.fpc="ERROR",stlib.data.pageInfo.sessionID="ERROR",stlib.data.pageInfo.hostname="ERROR",stlib.data.pageInfo.location="ERROR",stlib.data.pageInfo.product="widget",stlib.data.pageInfo.stid=""},resetShareData:function(){stlib.data.shareInfo={},stlib.data.shareInfo.url="ERROR",stlib.data.shareInfo.sharURL="",stlib.data.shareInfo.buttonType="ERROR",stlib.data.shareInfo.destination="ERROR",stlib.data.shareInfo.source="ERROR"},resetData:function(){stlib.data.resetPageData(),stlib.data.resetShareData()},validate:function(){function r(n,t){t!=encodeURIComponent(t)&&(i.notEncoded.test(t)?_$de(n+" not encoded"):null,i.tooEncoded.test(t)?_$de(n+" has too much encoding"):null);var r=i[n]?i[n].test(decodeURIComponent(t)):!0;r||_$de(n+" failed validation")}var i=stlib.validate.regexes,t=stlib.data.pageInfo,n;for(n in t)r(n,t[n]);t=stlib.data.shareInfo;for(n in t)r(n,t[n])},init:function(){if(!stlib.data.bInit){stlib.data.bInit=!0,stlib.data.resetData(),stlib.publisher&&stlib.data.setPublisher(stlib.publisher),stlib.data.set("product",stlib.product,"pageInfo");var t=document.location.href,i="",r="",n=[],f="",u="",e="",o="";n=stlib.data.getRefDataFromUrl(t),n.length>0?(i=typeof n[0]!="undefined"?n[0]:"",r=typeof n[1]!="undefined"?n[1]:"",u=stlib.data.removeRefDataFromUrl(t),stlib.data.showModifiedUrl(u),stlib.data.set("url",u,"shareInfo")):(f=document.referrer,n=f.replace("http://","").replace("https://","").split("/"),i=n.shift(),r=n.join("/"),stlib.data.set("url",t,"shareInfo")),stlib.data.set("title",document.title,"shareInfo"),stlib.data.publisherKeySet!=!0&&stlib.data.set("publisher","ur.00000000-0000-0000-0000-000000000000","pageInfo"),stlib.fpc.createFpc(),stlib.data.set("fpc",stlib.fpc.cookieValue,"pageInfo"),e=(new Date).getTime().toString(),o=Number(Math.random().toPrecision(5).toString().substr(2)).toString(),stlib.data.set("sessionID",e+"."+o,"pageInfo"),stlib.data.set("hostname",document.location.hostname,"pageInfo"),stlib.data.set("location",document.location.pathname,"pageInfo"),stlib.data.set("refDomain",i,"pageInfo"),stlib.data.set("refQuery",r,"pageInfo")}},showModifiedUrl:function(n){if(window.history&&history.replaceState)history.replaceState(null,document.title,n);else if(/MSIE/.test(navigator.userAgent)){var t=0,i=window.location.hash,u=new RegExp("(&st_refDomain=?)[^&|]+"),f=new RegExp("(#st_refDomain=?)[^&|]+"),r=document.location.href;u.test(r)?(t=i.indexOf("&st_refDomain"),window.location.hash=i.substr(0,t)):f.test(r)&&window.location.replace("#")}else document.location.replace(n)},getRefDataFromUrl:function(n){var u=new RegExp("st_refDomain="),i="",r="",t=[];return u.test(n)&&(i=n.match(/(st_refDomain=?)[^\&|]+/g),t.push(i[0].split("=")[1]),r=n.match(/(st_refQuery=?)[^\&|]+/g),t.push(r[0].replace("st_refQuery=",""))),t},removeRefDataFromUrl:function(n){var t=new RegExp("(&st_refDomain=?)[^&|]+"),i=new RegExp("(#st_refDomain=?)[^&|]+");return t.test(n)?n.replace(/\&st_refDomain=(.*)/g,""):i.test(n)?n.replace(/\#st_refDomain=(.*)/g,""):n},setPublisher:function(n){stlib.data.set("publisher",n,"pageInfo"),stlib.data.publisherKeySet=!0},setSource:function(n,t){var i="";i=t?t.toolbar?"toolbar"+n:t.page&&t.page!="home"&&t.page!=""?"chicklet"+n:"button"+n:n,stlib.data.set("source",i,"shareInfo")},set:function(n,t,i){if(typeof t=="number"||typeof t=="boolean")stlib.data[i][n]=t;else if(typeof t!="undefined"&&t!=null&&(stlib.data[i][n]=encodeURIComponent(decodeURIComponent(unescape(t.replace(/<[^<>]*>/gi," ")).replace(/%/gi,"%25"))),n=="url"||n=="location"||n=="image"))try{stlib.data[i][n]=encodeURIComponent(decodeURIComponent(decodeURI(t.replace(/<[^<>]*>/gi," ")).replace(/%/gi,"%25")))}catch(r){stlib.data[i][n]=encodeURIComponent(decodeURIComponent(unescape(t.replace(/<[^<>]*>/gi," ")).replace(/%/gi,"%25")))}},get:function(n,t){try{return stlib.data[t]&&stlib.data[t][n]?decodeURIComponent(stlib.data[t][n]):!1}catch(i){return!1}},unset:function(n,t){stlib.data[t]&&typeof stlib.data[t][n]!="undefined"&&delete stlib.data[t][n]}},stlib.data.resetData()),stlib.comscore={load:function(){var i=document.referrer,t=("https:"==document.location.protocol?"https://sb.":"http://b.")+"scorecardresearch.com/",n;t+="b?c1=7&c2=8097938&rn="+Math.round(Math.random()*2147483647)+"&c7="+encodeURIComponent(document.location.href)+"&c3=8097938&c8="+encodeURIComponent(document.title)+(i?"&c9="+encodeURIComponent(document.referrer):"")+"&cv=2.2&cs=js",n=new Image(1,1),n.src=t,n.onload=function(){return}}},stlib.hash={doNotHash:!0,hashAddressBar:!1,doNotCopy:!0,prefix:"sthash",shareHash:"",incomingHash:"",validChars:["1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],servicePreferences:{linkedin:"param",stumbleupon:"param",bebo:"param"},hashDestination:function(n){if(n=="copy")return"dpuf";var t=n.substring(0,2)+n.substring(n.length-2,n.length),i=function(n,t){return n.charCodeAt(t)==122?"a":String.fromCharCode(n.charCodeAt(t)+1)};return i(t,0)+i(t,1)+i(t,2)+i(t,3)},getHash:function(){var i=!1,r="",t=document.location.href,n;if(t=t.split("#").shift(),n=t.split("?"),n.length>1){n=n[1].split("&");for(arg in n)try{n[arg].substring(0,6)=="sthash"&&(i=!0,r=n[arg])}catch(u){}return i?r:document.location.hash.substring(1)}return document.location.hash.substring(1)},stripHash:function(n){var t=n;return t=t.split("#"),t.length>1?t[1]:""},clearHash:function(){if(stlib.hash.validateHash(document.location.hash)){var n=document.location.href.split("#").shift();window.history&&history.replaceState?history.replaceState(null,document.title,n):/MSIE/.test(navigator.userAgent)?window.location.replace("#"):document.location.hash=""}},init:function(){for(var n="",u=stlib.hash.validChars.length,i,r,t=0;t<8;t++)n+=stlib.hash.validChars[Math.random()*u|0];stlib.hash.getHash()==""?stlib.hash.shareHash=stlib.hash.prefix+"."+n:(i=stlib.hash.getHash().split("."),r=i.shift(),r==stlib.hash.prefix||r==stlib.hash.prefix?(stlib.hash.incomingHash=stlib.hash.getHash(),stlib.hash.shareHash=stlib.hash.prefix+"."+i.shift()+"."+n):stlib.hash.shareHash=stlib.hash.prefix+"."+n),!stlib.hash.doNotHash&&stlib.hash.hashAddressBar?(document.location.hash==""||stlib.hash.validateHash(document.location.hash))&&(window.history&&history.replaceState?history.replaceState(null,"ShareThis","#"+stlib.hash.shareHash+".dpbs"):/MSIE/.test(navigator.userAgent)?window.location.replace("#"+stlib.hash.shareHash+".dpbs"):document.location.hash=stlib.hash.shareHash+".dpbs"):stlib.hash.clearHash(),stlib.hash.doNotHash||stlib.hash.doNotCopy||stlib.hash.copyPasteInit(),stlib.hash.copyPasteLog()},checkURL:function(){var t=stlib.data.get("destination","shareInfo"),n=stlib.hash.updateParams(t),o="."+stlib.hash.hashDestination(t),e,r,u,f,i;if(stlib.hash.updateDestination(o),!stlib.hash.doNotHash&&typeof stlib.data.pageInfo.shareHash!="undefined"&&(e=stlib.data.get("url","shareInfo"),r=stlib.hash.stripHash(e),stlib.hash.validateHash(r)||r==""))if(typeof stlib.hash.servicePreferences[t]!="undefined")if(stlib.hash.servicePreferences[t]=="param"){if(_$d1("Don't use hash, use params"),_$d2(n),n.split("?").length>1){for(u=n.split("?")[1].split("&"),f=!1,i=0;i<u.length;i++)u[i].split(".")[0]=="sthash"&&(f=!0);f?stlib.data.set("url",n,"shareInfo"):stlib.data.set("url",n+"&"+stlib.data.pageInfo.shareHash,"shareInfo")}else stlib.data.set("url",n+"?"+stlib.data.pageInfo.shareHash,"shareInfo");t=="linkedin"&&stlib.data.get("sharURL","shareInfo")!=""&&stlib.data.set("sharURL",stlib.data.get("url","shareInfo"),"shareInfo")}else _$d1("Using Hash"),stlib.data.set("url",n+"#"+stlib.data.pageInfo.shareHash,"shareInfo");else _$d1("Not using custom destination hash type"),stlib.data.set("url",n+"#"+stlib.data.pageInfo.shareHash,"shareInfo")},updateParams:function(){var n=stlib.data.get("url","shareInfo").split("#").shift(),t=/(\?)sthash\.[a-zA-z0-9]{8}\.[a-zA-z0-9]{8}/,i=/(&)sthash\.[a-zA-z0-9]{8}\.[a-zA-z0-9]{8}/,r=/(\?)sthash\.[a-zA-z0-9]{8}/,u=/(&)sthash\.[a-zA-z0-9]{8}/;return t.test(n)?n=n.replace(t,"?"+stlib.data.pageInfo.shareHash):i.test(n)?n=n.replace(i,"&"+stlib.data.pageInfo.shareHash):r.test(n)?n=n.replace(r,"?"+stlib.data.pageInfo.shareHash):u.test(n)&&(n=n.replace(u,"&"+stlib.data.pageInfo.shareHash)),n},updateDestination:function(n){_$d_(),_$d1("Updating Destination"),/sthash\.[a-zA-z0-9]{8}\.[a-zA-z0-9]{8}\.[a-z]{4}/.test(stlib.data.pageInfo.shareHash)?(_$d2(stlib.data.pageInfo.shareHash.substring(0,24)),stlib.data.pageInfo.shareHash=stlib.data.pageInfo.shareHash.substring(0,24)+n):/sthash\.[a-zA-z0-9]{8}\.[a-z]{4}/.test(stlib.data.pageInfo.shareHash)?(_$d2(stlib.data.pageInfo.shareHash.substring(0,15)),stlib.data.pageInfo.shareHash=stlib.data.pageInfo.shareHash.substring(0,15)+n):stlib.data.pageInfo.shareHash+=n},validateHash:function(n){return/[\?#&]?sthash\.[a-zA-z0-9]{8}$/.test(n)||/[\?#&]?sthash\.[a-zA-z0-9]{8}\.[a-z]{4}$/.test(n)||/[\?#&]?sthash\.[a-zA-z0-9]{8}\.[a-zA-z0-9]{8}\.[a-z]{4}$/.test(n)||/[\?#&]?sthash\.[a-zA-z0-9]{8}\.[a-zA-z0-9]{8}$/.test(n)},appendHash:function(n){var t=stlib.hash.stripHash(n);return stlib.data.pageInfo.shareHash&&(stlib.hash.validateHash(t)||t=="")&&(n=n.replace("#"+t,"")+"#"+stlib.data.pageInfo.shareHash),n},copyPasteInit:function(){var t=document.getElementsByTagName("body")[0],n=document.createElement("div"),i,r;n.id="stcpDiv",n.style.position="absolute",n.style.top="-1999px",n.style.left="-1988px",t.appendChild(n),n.innerHTML="ShareThis Copy and Paste",i=document.location.href.split("#").shift(),r="#"+stlib.hash.shareHash,document.addEventListener?t.addEventListener("copy",function(){var t,r,i,u;typeof Tynt=="undefined"&&((t=document.getSelection(),t.isCollapsed)||(r=t.getRangeAt(0).cloneContents(),n.innerHTML="",n.appendChild(r),n.textContent.trim().length!=0)&&((t+"").trim().length==0||(n.innerHTML==t+""||n.textContent==t+""?n.innerHTML=stlib.html.encode(stlib.hash.selectionModify(t)):n.innerHTML+=stlib.html.encode(stlib.hash.selectionModify(t,!0))),i=document.createRange(),i.selectNodeContents(n),u=t.getRangeAt(0),t.removeAllRanges(),t.addRange(i),setTimeout(function(){t.removeAllRanges(),t.addRange(u)},0)))},!1):document.attachEvent},copyPasteLog:function(){var n=window.addEventListener?"addEventListener":"attachEvent",i=n=="attachEvent"?"oncopy":"copy",t=document.getElementsByTagName("body")[0];t&&t[n](i,function(){var n=!0;stlib.data.resetShareData(),stlib.data.set("url",document.location.href,"shareInfo"),stlib.data.setSource("copy"),stlib.data.set("destination","copy","shareInfo"),stlib.data.set("buttonType","custom","shareInfo"),typeof Tynt!="undefined"&&(stlib.data.set("result","tynt","shareInfo"),stlib.logger.log("debug"),n=!1),typeof addthis_config!="undefined"&&(stlib.data.set("result","addThis","shareInfo"),typeof addthis_config.data_track_textcopy=="undefined"||addthis_config.data_track_textcopy?(stlib.data.set("enabled","true","shareInfo"),n=!1):stlib.data.set("enabled","false","shareInfo"),stlib.logger.log("debug")),n&&(stlib.data.set("result","pass","shareInfo"),stlib.logger.log("debug"))},!1)},logCopy:function(n,t){stlib.data.resetShareData(),stlib.data.set("url",n,"shareInfo"),stlib.data.setSource("copy"),stlib.data.set("destination","copy","shareInfo"),stlib.data.set("buttonType","custom","shareInfo"),t&&stlib.data.set("description",t,"shareInfo"),stlib.sharer.share()},selectionModify:function(n,t){n=""+n,_$d_(),_$d1("Copy Paste");var e=document.location.href.split("#").shift(),f="#"+stlib.hash.shareHash,u="",i="",r="";return typeof t=="undefined"&&(/^((http|https):\/\/([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*))/i.test(n)||/^([a-z0-9!'\(\)\*\.\-\+:]*(\.)[a-z0-9!'\(\)\*\.\-\+:]*)((\/[a-z0-9!'\(\)\*\.\-\+:]*)*)/i.test(n))&&!/[\s@]/.test(n.trim())?(_$d2("is Url"),n.match(/#/)==null||stlib.hash.validateHash(n)?(i=n.split("#")[0]+f+".dpuf",r=i):(i=n,r=i)):(_$d2("is Not Url"),i=document.location.hash==""||/^#$/.test(document.location.hash)||stlib.hash.validateHash(document.location.hash)?e+f+".dpuf":document.location.href,r=n,n.length>50&&(u=" - See more at: "+i+"",/^\+?1?[\.\-\\)_\s]?[\\(]?[0-9]{3}[\.\-\\)_\s]?[0-9]{3}[\.\-_\s]?[0-9]{4}$|^[0-9]{3}[\.\-_\s]?[0-9]{4}$/.test(n)||/^[0-9]{3}[\.\-_\s]?[0-9]{8}$/.test(n)||/^[0-9]{2}[\.\-_\s]?[0-9]{4}[\.\-_\s]?[0-9]{4}$/.test(n)||/[\-_\.a-z0-9]+@[\-_\.a-z0-9]+\.[\-_\.a-z0-9]+/i.test(n)||(r+=u))),n.length>140&&(n=n.substring(0,137)+"..."),stlib.hash.logCopy(i,n),t&&t==!0?u:r}},stlib.pump=function(n,t,i){var r=this;this.isIframeReady=!1,this.isIframeSending=!1,this.getHash=function(n){var t=n.split("#");return t.shift(),t.join("#")},this.broadcastInit=function(n){this.destination=n,_$d_("---------------------"),_$d1("Initiating broadcaster:"),_$d(this.destination)},this.broadcastSendMessage=function(n){if(_$d_("---------------------"),_$d1("Initiating Send:"),this.destination===window){if(stlib.browser.ieFallback){window.location.replace(window.location.href.split("#")[0]+"#"+n),_$d2("child can't communicate with parent");return}_$d2("Iframe to publisher: "+n),parent.postMessage("#"+n,document.referrer)}else{if(_$d2("Publisher to Iframe: "+n),stlib.browser.ieFallback){this.destination.contentWindow&&(this.destination.contentWindow.location.replace(this.destination.src+"#"+n),this.isIframeSending=!0);return}this.destination.contentWindow.postMessage("#"+n,this.destination.src)}},this.receiverInit=function(n,t){var u,f,i,r;if(_$d_("---------------------"),_$d1("Initiating Receiver:"),_$d(n),stlib.browser.ieFallback){this.callback=t,this.source=n,n===window&&(window.location.replace(window.location.href.split("#")[0]+"#"),this.currentIframe=window.location.hash,u="receiver"+stlib.functionCount,stlib.functions[u]=function(n){if(""!=window.location.hash&&"#"!=window.location.hash){var t=window.location.hash;n(t),window.location.replace(window.location.href.split("#")[0]+"#")}},stlib.functionCount++,f="callback"+stlib.functionCount,stlib.functions[f]=t,stlib.functionCount++,setInterval("stlib.functions['"+u+"'](stlib.functions['"+f+"'])",200)),i=window.addEventListener?"addEventListener":"attachEvent",r=i=="attachEvent"?"onmessage":"message",window[i](r,function(t){n==window||t.origin.indexOf("sharethis.com")!=-1&&(t.data.match(/#Pinterest Click/)&&stlib.sharer.sharePinterest(),t.data.match(/#Print Click/)&&stlib.sharer.stPrint())},!1);return}i=window.addEventListener?"addEventListener":"attachEvent",r=i=="attachEvent"?"onmessage":"message",window[i](r,function(i){n==window?(_$d1("arrived in iframe from:"),_$d(i.origin),(i.data.match(/#fragmentPump/)||i.data.match(/#Buttons Ready/)||i.data.match(/#Widget Ready/)||i.data.indexOf("#light")==0||i.data.indexOf("#widget")==0||i.data.indexOf("#popup")==0||i.data.indexOf("#show")==0||i.data.indexOf("#init")==0||i.data.indexOf("#test")==0||i.data.indexOf("#data")==0)&&t(i.data)):i.origin.indexOf("sharethis.com")!=-1?(_$d1("arrived in parent from:"),_$d(i.origin),i.data.match(/#fragmentPump/)||i.data.match(/#Buttons Ready/)||i.data.match(/#Widget Ready/)||i.data.indexOf("#light")==0||i.data.indexOf("#widget")==0||i.data.indexOf("#popup")==0||i.data.indexOf("#show")==0||i.data.indexOf("#init")==0||i.data.indexOf("#test")==0||i.data.indexOf("#data")==0?t(i.data):i.data.match(/#Pinterest Click/)?stlib.sharer.sharePinterest():i.data.match(/#Print Click/)&&stlib.sharer.stPrint()):(_$d1("discarded event from:"),_$d(i.origin))},!1)},this.broadcastInit(n),this.receiverInit(t,i)},stlib.json={c:{"\b":"b","\t":"t","\n":"n","\f":"f","\r":"r",'"':'"',"\\":"\\","/":"/"},d:function(n){return n<10?"0".concat(n):n},e:function(n,t,i){return i=eval,delete eval,typeof eval=="undefined"&&(eval=i),t=eval(""+n),eval=i,t},i:function(n,t,i){return 1*n.substr(t,i)},p:["","000","00","0",""],rc:null,rd:/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$/,rs:/(\x5c|\x2F|\x22|[\x0c-\x0d]|[\x08-\x0a])/g,rt:/^([0-9]+|[0-9]+[,\.][0-9]{1,3})$/,ru:/([\x00-\x07]|\x0b|[\x0e-\x1f])/g,s:function(n,t){return"\\".concat(stlib.json.c[t])},u:function(n,t){var i=t.charCodeAt(0).toString(16);return"\\u".concat(stlib.json.p[i.length],i)},v:function(n,t){return stlib.json.types[typeof result](result)!==Function&&(t.hasOwnProperty?t.hasOwnProperty(n):t.constructor.prototype[n]!==t[n])},types:{boolean:function(){return Boolean},"function":function(){return Function},number:function(){return Number},object:function(n){return n instanceof n.constructor?n.constructor:null},string:function(){return String},undefined:function(){return null}},$$:function(n){function t(t,i){i=t[n],delete t[n];try{stlib.json.e(t)}catch(r){return t[n]=i,1}}return t(Array)&&t(Object)},encode:function(){var n=arguments.length?arguments[0]:this,t,i,r,f;if(n===null)t="null";else if(n!==undefined&&(i=stlib.json.types[typeof n](n)))switch(i){case Array:t=[];for(var r=0,u=0,e=n.length;u<e;u++)n[u]!==undefined&&(i=stlib.json.encode(n[u]))&&(t[r++]=i);t="[".concat(t.join(","),"]");break;case Boolean:t=String(n);break;case Date:t='"'.concat(n.getFullYear(),"-",stlib.json.d(n.getMonth()+1),"-",stlib.json.d(n.getDate()),"T",stlib.json.d(n.getHours()),":",stlib.json.d(n.getMinutes()),":",stlib.json.d(n.getSeconds()),'"');break;case Function:break;case Number:t=isFinite(n)?String(n):"null";break;case String:t='"'.concat(n.replace(stlib.json.rs,stlib.json.s).replace(stlib.json.ru,stlib.json.u),'"');break;default:r=0,t=[];for(f in n)n[f]!==undefined&&(i=stlib.json.encode(n[f]))&&(t[r++]='"'.concat(f.replace(stlib.json.rs,stlib.json.s).replace(stlib.json.ru,stlib.json.u),'":',i));t="{".concat(t.join(","),"}")}return t},decode:function(n){if(typeof n=="string")try{return/^[\],:{}\s]*$/.test(n.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))?window.JSON&&window.JSON.parse?window.JSON.parse(n):new Function("return "+n)():null}catch(t){}}};try{stlib.json.rc=new RegExp('^("(\\\\.|[^"\\\\\\n\\r])*?"|[,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t])+?$')}catch(z){stlib.json.rc=/^(true|false|null|\[.*\]|\{.*\}|".*"|\d+|\d+\.\d+)$/}stlib.logger={loggerUrl:("https:"==document.location.protocol?"https://":"http://")+"l.sharethis.com/",l2LoggerUrl:("https:"==document.location.protocol?"https://":"http://")+"l2.sharethis.com/",productArray:[],version:"",lang:"en",isFpEvent:!1,constructParamString:function(){var t=stlib.data.pageInfo,i="",n;for(n in t)i+=n+"="+t[n]+"&";t=stlib.data.shareInfo;for(n in t)i+=n+"="+t[n]+"&";return i.substring(0,i.length-1)},loadPixelsAsync:function(n){if(typeof n!="undefined"&&(stlib.data.set("stid",n.stid,"pageInfo"),n.status=="success")){var t=("https:"==document.location.protocol?"https://ws.":"http://w.")+"sharethis.com/button/p.js";stlib.scriptLoader.loadJavascript(t,function(){})}},log:function(n,t,i){var f,h,e,u,r,o,s;typeof stlib.data.get("counter","shareInfo")!="undefined"?(f=0,stlib.data.get("counter","shareInfo")&&(f=stlib.data.get("counter","shareInfo")),stlib.data.set("ts"+(new Date).getTime()+"."+f,"","shareInfo"),stlib.data.unset("counter","shareInfo")):stlib.data.set("ts"+(new Date).getTime(),"","shareInfo"),n=="widget"&&(h="."+stlib.hash.hashDestination(stlib.data.shareInfo.destination),stlib.hash.updateDestination(h)),t&&(t==stlib.logger.loggerUrl||t==stlib.logger.l2LoggerUrl)||(t=stlib.logger.loggerUrl),e=n=="pview"?n:n=="debug"?"cns":"log",n=="pview"?(stlib.comscore.load(),u=t+e+"?event="+n+"&version="+stlib.logger.version+"&lang="+stlib.logger.lang+"&"+stlib.logger.constructParamString()):u=t+e+"?event="+n+"&"+stlib.logger.constructParamString();try{r=new XMLHttpRequest,r.open("GET",u,!0),r.withCredentials=!0,r.onreadystatechange=function(){if(this.readyState==this.DONE){try{o=JSON.parse(r.responseText)}catch(t){}n=="pview"?stlib.logger.loadPixelsAsync(o):i?i():null}},r.send()}catch(c){s=new Image(1,1),s.src=u+"&img_pview=true",s.onload=function(){return},n=="pview"?stlib.logger.loadPixelsAsync(o):i?i():null}}},stlib.logger.version="buttons.js",customProduct="widget",typeof stLight=="undefined"&&typeof SHARETHIS=="undefined"&&(stWidgetVersion=!1,typeof switchTo5x=="undefined"?stWidgetVersion="4x":(switchTo5x==!1&&(stWidgetVersion="4x"),switchTo5x==!0&&(stWidgetVersion="5xa")),stLight=new function(){this.version=!1,this.publisher=null,this.sessionID_time=(new Date).getTime().toString(),this.sessionID_rand=Number(Math.random().toPrecision(5).toString().substr(2)).toString(),this.sessionID=this.sessionID_time+"."+this.sessionID_rand,this.fpc=null,this.counter=0,this.readyRun=!1,this.meta={hostname:document.location.host,location:document.location.pathname},this.loadedFromBar=!1,this.clickCallBack=!1},stLight.loadDefault=function(){this.product=typeof customProduct=="undefined"?"DOS2":customProduct,this.source="DOS2",this.version="st_insights.js"},stLight.options=function(n){this.loadDefault(),n&&n.publisher&&stLight.setPublisher(n.publisher),n&&n.refDomain&&stLight.setRefDomain(n.refDomain),stlib.logger.productArray=[],n&&n.product?stLight.setProduct(n.product):stLight.setProduct(stLight.product),n&&typeof n.hashAddressBar!="undefined"&&(stlib.hash.hashAddressBar=n.hashAddressBar),n&&typeof n.doNotHash!="undefined"&&(stlib.hash.doNotHash=n.doNotHash),n&&typeof n.doNotCopy!="undefined"&&(stlib.hash.doNotCopy=n.doNotCopy),stlib.stLightOptionsObj=n},stLight.onReady=function(){if(stLight.readyRun==!0)return!1;if(stLight.loadFromScript(),stLight.readyRun=!0,stlib.data.init(),stLight.fpc=stlib.data.get("fpc","pageInfo"),stLight.publisher==null&&typeof window.console!="undefined")try{}catch(n){}stLight.setProduct(stLight.product),stlib.logger.lang="en"},stLight.log=function(n){stlib.data.resetShareData(),stlib.data.setSource(stLight.getSource()),stlib.data.set("url",document.location.href,"shareInfo"),stlib.data.set("title",document.title,"shareInfo"),stlib.data.set("counter",stLight.counter++,"shareInfo"),stlib.logger.log(n)},window.document.readyState=="completed"?stLight.onReady():typeof window.addEventListener!="undefined"?window.addEventListener("load",stLight.onReady,!1):typeof document.addEventListener!="undefined"?document.addEventListener("load",stLight.onReady,!1):typeof window.attachEvent!="undefined"&&window.attachEvent("onload",stLight.onReady),stLight.setPublisher=function(n){stlib.data.setPublisher(n),stLight.publisher=n},stLight.setRefDomain=function(n){stlib.data.setRefDomain(n)},stLight.setProduct=function(n){this.product=n,stlib.data.set("product",n,"pageInfo")},stLight.getProduct=function(){return this.product},stLight.getSource=function(){var n="share4x";return stWidgetVersion=="5xa"&&(n="share5x"),n}),stLight.getUrlSearchParam=function(){var n=window.location.search.substring(1);return n.split("&")},stLight.getUrlQueryParams=function(n){var t={},i=n.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(n,i,r){t[i]=r});return t},stLight.getScriptSrcParams=function(n){var t=document.getElementById(n);if(t)return stLight.getUrlQueryParams(t.src)},stLight.setParams=function(n){n&&(n.refdomain&&stLight.setRefDomain(n.refdomain),n.publisher&&stLight.setPublisher(n.publisher),n.product&&stLight.setProduct(n.product))},stLight.loadFromScript=function(){var n=stLight.getScriptSrcParams("st_insights_js");stLight.setParams(n)},stLight.loadFromWindowLocation=function(){var n=stLight.getUrlSearchParam();stLight.setParams(n)},stLight.onDomContentLoaded=function(){stLight.onReady()},stLight.domReady=function(){stLight.onReady()},st_showing=!1,stLight.clickSubscribers=[],stLight.nonClickSubscribers=[],window.document.readyState=="completed"?stLight.domReady():typeof window.addEventListener!="undefined"?window.addEventListener("load",stLight.domReady,!1):typeof document.addEventListener!="undefined"?document.addEventListener("load",stLight.domReady,!1):typeof window.attachEvent!="undefined"&&window.attachEvent("onload",stLight.domReady),typeof __st_loadLate=="undefined"?typeof window.addEventListener!="undefined"?window.addEventListener("DOMContentLoaded",stLight.onDomContentLoaded,!1):typeof document.addEventListener!="undefined"&&document.addEventListener("DOMContentLoaded",stLight.onDomContentLoaded,!1):typeof window.addEventListener!="undefined"?window.addEventListener("DOMContentLoaded",stLight.onDomContentLoadedLazy,!1):typeof document.addEventListener!="undefined"&&document.addEventListener("DOMContentLoaded",stLight.onDomContentLoadedLazy,!1),document.readyState=="complete"&&stLight.readyRun==!1&&stLight.domReady(),stButtons=stButtons||{},stButtons.getCount=function(n,t,i){if(i&&i!==null)while(i.childNodes.length>=1)try{i.removeChild(i.firstChild)}catch(r){}ShareThisEvent.listen("on_async_buttons_load",function(){stButtons=async_buttons.stButtons,stButtons.cbQueue.push({url:n,service:t,element:i}),stButtons.getCountsFromService(n,t,i)},!1)},stButtons.locateElements=function(n){ShareThisEvent.listen("on_async_buttons_load",function(){async_buttons.stButtons.locateElements(n)},!1)},stWidget=stWidget||{},typeof stWidget.readyRun=="undefined"&&(stWidget.addEntry=function(n){ShareThisEvent.listen("on_async_buttons_load",function(){stWidget=async_buttons.stWidget,stWidget.addEntry(n)},!1)}),stLight.subscribe=function(n,t){ShareThisEvent.listen("on_async_buttons_load",function(){stButtonsLib.subscribe(n,t)},!1)},window.__sharethis__&&(stlib.setProduct(window.__sharethis__.product),stlib.setPublisher(window.__sharethis__.property)),stlib.onscriptload||document.URL.indexOf("edge.sharethis.com")!=-1||(stlib.data.init(),stlib.onscriptload=!0,stlib.logger.log("pview",null)),stlib.scriptLoader={loadJavascript:function(n,t){var i=stlib.scriptLoader;i.head=document.getElementsByTagName("head")[0],i.scriptSrc=n,i.script=document.createElement("script"),i.script.setAttribute("type","text/javascript"),i.script.setAttribute("src",i.scriptSrc),i.script.async=!0,window.attachEvent&&document.all?i.script.onreadystatechange=function(){(this.readyState=="complete"||this.readyState=="loaded")&&t()}:i.script.onload=t,i.s=document.getElementsByTagName("script")[0],i.s.parentNode.insertBefore(i.script,i.s)},loadCSS:function(n,t){_$d_(),_$d1("Loading CSS: "+n);var i=stlib.scriptLoader,r;i.head=document.getElementsByTagName("head")[0],i.cssSrc=n,i.css=document.createElement("link"),i.css.setAttribute("rel","stylesheet"),i.css.setAttribute("type","text/css"),i.css.setAttribute("href",n),i.css.setAttribute("id",n),setTimeout(function(){t(),document.getElementById(n)||(r=setInterval(function(){document.getElementById(n)&&(clearInterval(r),t())},100))},100),i.head.appendChild(i.css)}};NS("TM.Scripts.PlanPages"),TM.Scripts.PlanPages=function(){},TM.Scripts.PlanPages.prototype={loadImage:function(n,t){var f=$j(n),r=f.attr("src"),e=f.parent(),h,s,i;r=r.substr(0,r.indexOf("?"));var u=$j("#main-img"),c=$j("#imgTitle"),o=f.attr("alt");u.attr("src",r),u.attr("alt",o),u.attr("title",o),c.html(o),u.fadeIn(800),h=e.parent().children(),$j.each(h,function(n,t){$j(t).removeClass("on")}),e.addClass("on"),s=e.index(),this.$carousel.hasNext()?(i=$j(".next-arrow-disable"),i.length==0&&(i=$j(".next-arrow")),i.unbind("click"),i.bind("click",function(){return $j(".slides-next").click(),!1}),i.removeClass("next-arrow-disable").addClass("next-arrow")):(i=$j(".next-arrow"),i.length==0&&(i=$j(".next-arrow-disable")),i.unbind("click"),i.removeClass("next-arrow").addClass("next-arrow-disable")),this.$carousel.hasPrevious()?(i=$j(".prev-arrow-disable"),i.length==0&&(i=$j(".prev-arrow")),i.unbind("click"),i.bind("click",function(){return $j(".slides-prev").click(),!1}),i.removeClass("prev-arrow-disable").addClass("prev-arrow")):(i=$j(".prev-arrow"),i.length==0&&(i=$j(".prev-arrow-disable")),i.unbind("click"),i.removeClass("prev-arrow").addClass("prev-arrow-disable")),s!=null&&t&&($j(".prev-arrow").length>0||$j(".prev-arrow-disable").length>0)&&this.$carousel.to(s),t&&this.$carousel.stop()},onSlideTransitionComplete:function(n){var t=n.$carousel,i,r;t!=null&&t.$items.length>0&&(i=typeof t.current!="undefined"&&t.current!=null?t.current:0,r=$j(t.$items[i]).children()[0],n.loadImage(r))},$carousel:null,loadSlides:function(){var n,t,i;$j("li.slide").length==1?($j("li.slide").hide(),this.loadImage($j("li.slide").children()[0])):(n=TM.Rotator.get_slideOptions(),t=this,n.oncomplete=function(){t.onSlideTransitionComplete(t)},n.animate=!0,i=$j(".slideshow").slides(n),$j("lide.slide").length!=1&&$j("li.slide").show(),this.$carousel=i.data("slides"))},init:function(){var n=this;$j(document).ready(function(){var t=window.document.location.href,i=t.substr(t.lastIndexOf("/")+1);switch(i){case"floorplans":$j("li.pd1").addClass("on");break;case"photos":$j("li.pd2").addClass("on");break;case"locations":$j("li.pd3").addClass("on");break;case"options":$j("li.pd4").addClass("on");break;default:$j("li.pd1").addClass("on")}n.loadSlides()})}};var planScripts=new TM.Scripts.PlanPages;planScripts.init()