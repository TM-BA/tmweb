﻿function turnOn(sender){
    $j(".mapNav").removeClass("on");
    $j(sender).addClass("on");
}
// Set up the map and the local searcher.
function initialize() {

    // Initialize the map with default UI.
    map = new google.maps.Map(document.getElementById("map"), {
        center: communityLL,
        zoom: 11,
        mapTypeId: 'roadmap'
        ,scaleControl:true
    });
    // Create one InfoWindow to open when a marker is clicked.
    infoWindow = new google.maps.InfoWindow;
    google.maps.event.addListener(infoWindow, 'closeclick', function () {
        unselectMarkers();
    });

            

    // Initialize the local searcher
    service = new google.maps.places.PlacesService(map);
            
    requestNewPOIs("0");
}

function requestNewPOIs(typeIndex){
    var request = {
        location:communityLL,
        rankBy :google.maps.places.RankBy.DISTANCE,
        //radius:16000,
        types:categoryJson[typeIndex].fields.split('|')
    };
    service.nearbySearch(request, function(results, status){OnLocalSearch(results, status,typeIndex,true);});
}

function showAll() {
    reinitializeMap();
    $j(".mapNav").removeClass("on");
    $j(".mapNav").addClass("on");
    for (var category in categoryJson) {

        var request = {
            location: communityLL,
            rankBy: google.maps.places.RankBy.DISTANCE,
            //radius:16000,
            types: categoryJson[category].fields.split('|')
        };

        doNearbySearch(service, request, category);
    }
}

function doNearbySearch(service, request, typeIndex) {
    service.nearbySearch(request, function(results, status) {
        OnLocalSearch(results, status, typeIndex);
    });
}


function reinitializeMap(){
    // Clear the map and the old search well
    var searchWell = document.getElementById("searchwell");
    searchWell.innerHTML = "";
    for (var i = 0; i < gCurrentResults.length; i++) {
        gCurrentResults[i].marker().setMap(null);
    }
    //delete previous bounds
    latlngbounds =  new google.maps.LatLngBounds( );
    latlngbounds.extend(communityLL);
    // Close the infowindow
    gCurrentResults = [];
}

function unselectMarkers() {
    for (var i = 0; i < gCurrentResults.length; i++) {
        gCurrentResults[i].unselect();
    }
}

// Called when Local Search results are returned, we clear the old
// results and load the new ones.
function OnLocalSearch(results, status, typeIndex, clearMap) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var searchWell = document.getElementById("searchwell");
        if (clearMap) {
            reinitializeMap();
        }
        infoWindow.close();
        gOffMarker = categoryJson[typeIndex].marker;
        gOnMarker = categoryJson[typeIndex].markerOn;
        var resultsLen = results.length;
        var includedResultLength = gCurrentResults.length;
        if (resultsLen > 0) {
        var showHeader = true;
            for (var i = 0; i < resultsLen; i++) {
                var thisDistance = getDistance(communityLL, new google.maps.LatLng(parseFloat(results[i].geometry.location.lat()), parseFloat(results[i].geometry.location.lng()))) * .621371 ;
                if (thisDistance > maxDistance) {
                // if the distance over max distance stop processing future results they all will be
                    i = resultsLen;
                }
                else {
                    var category = categoryJson[typeIndex].name;
                    if (showHeader) {
                        $j("#searchwell").append('<span class="t-b">' + category + '</span><ul class="head"><li>Name</li><li>Address</li><li>Distance</li></ul><hr class="listDivider" />');
                        showHeader = false;
                    }
                    gCurrentResults.push(new LocalResult(results[i], category));
                }
            }
            if (includedResultLength == gCurrentResults.length) {
                $j("#searchwell").append('<div style="float:left;clear:both;width:100%;">Sorry, no "' + categoryJson[typeIndex].name + '" found within ' + maxDistance + ' miles.</div>');
            }
            else 
            {
                map.fitBounds(latlngbounds);
            }
        }
        else {
            $j("#searchwell").append('<div style="float:left;clear:both;width:100%;">Sorry, no "' + categoryJson[typeIndex].name + '" found within ' + maxDistance + ' miles.</div>');
        }
    }
}

// A class representing a single Local Search result returned by the
// Google AJAX Search API.
function LocalResult(result, categoryName) {
    var me = this;
    me.result_ = result;
    me.category = categoryName;
    me.resultNode_ = me.node();
    me.marker_ = me.marker();
    latlngbounds.extend(me.LatLng());

    google.maps.event.addDomListener(me.resultNode_, 'mouseover', function () {
        // Highlight the marker and result icon when the result is
        // mouseovered.  Do not remove any other highlighting at this time.
        me.highlight(true);
    });

    google.maps.event.addDomListener(me.resultNode_, 'mouseout', function () {
        // Remove highlighting unless this marker is selected (the info
        // window is open).
        if (!me.selected_) me.highlight(false);
    });

    google.maps.event.addDomListener(me.resultNode_, 'click', function () {
        me.select();
    });

    document.getElementById("searchwell").appendChild(me.resultNode_);
}

LocalResult.prototype.node = function () {
    if (this.resultNode_) return this.resultNode_;
    return this.html();
};

LocalResult.prototype.LatLng = function(){
    var me = this;
    if (me.LatLng_) return me.LatLng_;
    var LatLng = me.LatLng_ = new google.maps.LatLng( parseFloat( me.result_.geometry.location.lat() ), parseFloat( me.result_.geometry.location.lng() ) );
    return LatLng;
}
        
// Returns the GMap marker for this result, creating it with the given
// icon if it has not already been created.
LocalResult.prototype.marker = function () {
    var me = this;
    if (me.marker_) return me.marker_;
    var marker = me.marker_ = new google.maps.Marker({
        position: me.LatLng()
        ,icon: gOffMarker, shadow: gSmallShadow, map: map
    });
    google.maps.event.addListener(marker, "mouseover", function () {
        me.select();
    });
    return marker;
};

// Unselect any selected markers and then highlight this result and
// display the info window on it.
LocalResult.prototype.select = function () {
    unselectMarkers();
    var me = this;
    me.selected_ = true;
    me.highlight(true);
    //call to get extra place detail info
    this.PlaceDetails(function(){
        infoWindow.setContent(me.html(true));
        infoWindow.open(map, me.marker());
    });
};

LocalResult.prototype.PlaceDetails = function(callback){
    var me = this;
    if(me.placeDetails_){
    if(callback){
                callback();
            } else {
        return me.placeDetails_;
        }
    }

    service.getDetails({ reference: me.result_.reference }, function(place, status){
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            me.placeDetails_ = place;
            if(callback){
                callback();
            }
        }
    });

}

LocalResult.prototype.Distance = function () {
    var me = this;
    if (true) {//convertToMiles) {
        return (getDistance(communityLL, me.LatLng()) * .621371).toPrecision(2);
    }
    else {
        return (getDistance(communityLL, me.LatLng())).toPrecision(2);
    }
}

LocalResult.prototype.Phone = function(){
    var me = this;
    return (me.PlaceDetails()) ? me.PlaceDetails().formatted_phone_number : "nothing";
}

LocalResult.prototype.isSelected = function () {
    return this.selected_;
};

// Remove any highlighting on this result.
LocalResult.prototype.unselect = function () {
    this.selected_ = false;
    this.highlight(false);
};

// Returns the HTML we display for a result before it has been "saved"
LocalResult.prototype.html = function (showPhone) {
    var me = this;
    var container = document.createElement("div");
    container.className = "unselected";
    var UL = document.createElement("ul");
    var liTitle = document.createElement("li");
    liTitle.textContent = me.result_.name;
    var liAddress = document.createElement("li");
    liAddress.textContent = me.result_.vicinity;

    var liDistance = document.createElement("li");
    var computedDistance = me.Distance();
    //if (computedDistance < maxDistance) {
        liDistance.textContent = computedDistance + "mi.";
        UL.appendChild(liTitle);
        UL.appendChild(liAddress);
        if (showPhone) {
            var liPhone = document.createElement("li");
            liPhone.textContent = me.Phone();
            UL.appendChild(liPhone);

            var liCategory = document.createElement("li");
            liCategory.textContent = me.category;
            UL.appendChild(liCategory);
        }
        UL.appendChild(liDistance);

        container.appendChild(UL);
        return container;
    //} 
}

LocalResult.prototype.highlight = function (highlight) {
    this.marker().setOptions({ icon: highlight ? gOnMarker : gOffMarker });
    this.node().className = "unselected" + (highlight ? " red" : "");
}

Number.prototype.toRad = function(){return this*(3.1415/180);}

function getDistance(latLng1, latLng2) {
    var R = 6371; // km
    var dLat = (latLng2.lat()-latLng1.lat()).toRad();
    var dLon = (latLng2.lng()-latLng1.lng()).toRad();
    var lat1 = latLng1.lat().toRad();
    var lat2 = latLng2.lat().toRad();

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    return d;
}

$j(document).ready(initialize());
