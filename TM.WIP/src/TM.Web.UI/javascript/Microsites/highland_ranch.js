﻿try { Typekit.load(); } catch (e) { }

var app = {
    ready: function () {
        var count = 0,
        interval = setInterval(function () {
            count++;
            app.resize();
            if (count > 8) {
                clearInterval(interval);
            }
        }, 250);
    },
    resize: function () {
        var width = $j(window).width(),
        height = $j(window).height();

        $j('#body').find('.col.left').height('');
        $j('#body').find('.col.right').height('');
        $j('#body').find('.col.left p.contact').css({ paddingTop: '' });

        var rHeight = $j('#body').find('.col.right').height(),
        lHeight = $j('#body').find('.col.left').height();

        if (rHeight > lHeight) {
            lHeight = $j('#body').find('.col.left').height(rHeight);
            $j('#body').find('.col.left p.contact').css({ paddingTop: rHeight - 529 });
        } else if (lHeight >= rHeight) {
            rHeight = $j('#body').find('.col.right').height(lHeight);

        }
    },
    thankyou: function (url) {
        $j(document).ready(function () {
            iBox.showURL(url, '', { width: 600, height: 500 });
        });
    },
    error: function () {
        alert('Could not submit form. Make sure all the fields are filled out and try again.');
    }
}

$j(document).ready(function () { app.ready(); app.resize(); });
$j(window).resize(function () { app.resize(); });