<report date='20160811T162530'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='18' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='1621557' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='73047' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='798097' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='26095' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='4' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='4' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='61503' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='172' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='39137' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='9650' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='1' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='55' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='3' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='3' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='642203' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='915691' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='669' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='36' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='54014' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='47' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='6' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='3670' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='161' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='9769' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='50561' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='242817' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='16' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='162' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='14392' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='2' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='297' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='42' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='6' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='9' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='257024' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='257024' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='1198' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='10' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='8' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='933' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='38' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='6480' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='51512' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='366' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='5564' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='169' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='18' category='Sitecore.System'/>
  </category>
</report>

