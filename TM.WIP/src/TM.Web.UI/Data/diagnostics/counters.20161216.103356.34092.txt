<report date='20161216T103356'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='2898' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='163689' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='19909' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='65336' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='16481' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='2896' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='12552' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='35' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='130' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='8' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='6' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='2' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='7' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='19' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='25' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='24290' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='1000002' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='1054' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='0' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='11064' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='76' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='3627' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='2507' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='1723' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='25245' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='111192' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='38' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='3' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='2508' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='615' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='356' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='28' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='7' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='20007' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='20007' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='10417' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='617' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='49' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='8001' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='232' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='4296' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='6601' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='357' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='2912' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='2515' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='126' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='36' category='Sitecore.System'/>
  </category>
</report>

