<report date='20161215T122324'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='107' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='515231' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='56761' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='238566' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='45445' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='14' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='2' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='44995' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='196' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='8604' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='2266' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='84' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='47' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='49' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='7' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='3' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='0' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='173099' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='840706' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='878' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='18' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='40782' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='86' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='2836' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='168' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='20069' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='49214' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='192554' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='54' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='170' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='10203' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='7' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='277' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='108' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='6' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='32' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='56' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='101158' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='101158' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='1238' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='1' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='22' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='984' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='41' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='5790' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='28803' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='408' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='4728' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='177' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='960' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='35' category='Sitecore.System'/>
  </category>
</report>

