<report date='20160519T142124'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='66' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='2253796' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='112166' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='1116251' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='36450' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='8' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='6' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='114100' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='204' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='54933' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='9236' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='14' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='162' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='5' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='4' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='824421' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='891199' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='3667' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='34' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='100218' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='156' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='69' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='4524' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='402' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='11669' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='91783' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='453308' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='186' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='403' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='28644' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='4' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='598' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='63' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='5' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='13' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='457101' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='457107' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='4196' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='40' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='8' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='852' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='8' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='11861' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='107470' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='511' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='7533' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='410' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='39' category='Sitecore.System'/>
  </category>
</report>

