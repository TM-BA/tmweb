<report date='20160522T231635'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='4866' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='4950735' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='152792' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='2763121' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='44323' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='4832' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='6' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='18' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='130448' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='253' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='94963' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='22159' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='22' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='182' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='5' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='8' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='2047678' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='966091' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='11101' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='36' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='106682' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='217' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='277' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='10265' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='1065' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='21954' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='131654' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='625655' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='697' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='1066' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='30651' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='3' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='1243' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='116' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='6' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='11' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='1176170' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='1176173' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='10813' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='987' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='67' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='14' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='1898' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='16' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='15620' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='152702' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='1069' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='9003' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='1073' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='58' category='Sitecore.System'/>
  </category>
</report>

