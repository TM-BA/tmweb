<report date='20170224T135426'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='2' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='4109023' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='453375' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='1846963' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='221191' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='10' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='4' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='26266' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='182' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='6657' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='6544' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='52' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='5' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='32' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='41' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='899944' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='3014122' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='1558' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='56' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='21106' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='79' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='4861' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='2314' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='120111' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='200618' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='671720' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='55' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='3' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='2315' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='4128' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='5' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='328' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='242' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='6' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='22' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='614938' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='614942' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='10073' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='4' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='150' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='7085' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='227' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='5685' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='14772' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='385' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='4710' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='2322' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='30' category='Sitecore.System'/>
  </category>
</report>

