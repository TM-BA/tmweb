<report date='20170331T115521'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='2' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='10829236' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='734797' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='4847161' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='251411' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='26' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='14' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='442199' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='379' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='296386' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='36752' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='30' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='627' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='26' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='24' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='3779169' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='3096600' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='16457' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='160' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='375388' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='266' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='499' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='30462' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='8' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='2147' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='124643' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='431706' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='1859050' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='711' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='1' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='2148' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='103046' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='13' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='1992' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='251' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='6' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='15' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='2415561' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='2415570' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='18496' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='143' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='33' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='4464' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='297' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='51074' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='501858' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='1169' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='19252' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='2155' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='124' category='Sitecore.System'/>
  </category>
</report>

