<report date='20161021T153254'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='12722' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='789870' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='51176' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='356671' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='24540' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='12720' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='6' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='12' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='21203' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='149' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='12861' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='10766' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='1' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='16' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='0' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='12' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='4' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='245111' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='838406' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='263' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='54' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='16208' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='12' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='1786' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='137' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='7766' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='23323' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='99657' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='6' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='138' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='4582' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='3' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='142' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='139' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='5' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='7' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='0' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='136489' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='136495' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='769' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='3075' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='5' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='6' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='1723' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='35' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='3191' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='15774' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='220' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='4415' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='145' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='0' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='14' category='Sitecore.System'/>
  </category>
</report>

