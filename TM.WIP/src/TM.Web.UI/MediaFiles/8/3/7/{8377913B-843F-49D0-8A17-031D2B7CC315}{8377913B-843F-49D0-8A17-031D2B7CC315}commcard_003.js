{{each(companyname, communities) $data}}
	{{each communities}}
	<div class="commCard" id="${CommunityLegacyID}">
		<div class="commName">
			<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}">${Name}</a>
		</div>
		<div class="commImg">
			<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}">
				<img class="lazy" data-original="${CommunityCardImage}?w=640&bc=white" alt="${CommunityName}">
			</a>
		    <div class="commStatus">
		    	<div class="ribbon">
			    	{{if MapPinStatus == "nowselling"}}Available Now
			    	{{else MapPinStatus == "preselling"}}Available Now
			    	{{else MapPinStatus == "comingsoon"}}Coming Soon
			    	{{else MapPinStatus == "closeout"}}Closeout
			    	{{else}}Available Now
			    	{{/if}}
			    </div>
		    </div>	
			<div class="priceFromSpace">
				<span class="priceFrom">${CommunityPrice}</span>
			</div>
		</div>
		<div class="commLocation">
			<a href="https://www.google.com/maps?q=${AddressLink}">${StreetAddress1} ${StreetAddress2}, ${City}, ${StateProvinceAbbreviation} ${ZipPostalCode}</a>
		</div>
		<div class="commDetails">
			<h5>Features</h5>
			{{if SquareFootage == "" && (Bedrooms == "" || Bedrooms == "0 Bed") && (Bathrooms == "" || Bathrooms == "0 Bath") && (Stories == "" || Stories == "0 Story Home") && (Garage == "" || Garage =="0 Car Garage" )}}
				<p>For more information <a href="${CommunityDetailsURL}/request-information/">Contact us</a>.</p>
			{{else}}
				<ul>
					{{if SquareFootage != ""}}<li>${SquareFootage}</li>{{/if}}
					{{if (Bedrooms == "" || Bedrooms == "0 Bed")}}{{else}}<li>${Bedrooms}</li>{{/if}}
					{{if (Bathrooms == "" || Bathrooms == "0 Bath")}}{{else}}<li>${Bathrooms}</li>{{/if}}
					{{if (Stories == "" || Stories == "0 Story Home")}}{{else}}<li>${Stories}</li>{{/if}}
					{{if (Garage == "" || Garage =="0 Car Garage")}}{{else}}<li>${Garage}</li>{{/if}}
				</ul>
			{{/if}}
		</div>
		<ul class="commCardGrayBar">
			<li>{{if HomeForSaleList.length > 0}}<a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/homes-ready-now"><i class="icon icon-tag"></i> Homes for Sale</a>{{/if}}</li>
			<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/floor-plans"><i class="icon icon-home"></i> Available Plans</a></li>
			<li><a {{if IsCurrentDomain == false }}target="_blank"{{/if}} href="${CommunityDetailsURL}/driving-directions"><i class="icon icon-direction"></i> Driving Directions</a></li>
		</ul>
		<div class="commFooter">
			<ul>
				<li class="directions">
					<a href="https://maps.google.com/maps?t=m&saddr=Current+Location&daddr=${CommunityLatitude},${CommunityLongitude}" target="_blank"><i class="icon icon-cab"></i><span>Get Directions<br> <strong>&nbsp;</strong></span></a>
				</li>
				<li class="call">
					<a href="tel:+1${Phone}"><i class="icon icon-phone"></i><span>Questions?<br> <strong>${Phone}</strong></span></a>
				</li>
			</ul>
		</div>
	</div>
   {{/each}}
{{/each}}
