﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintLayout.aspx.cs" Inherits="TM.Web.Custom.Layouts.PrintLayout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insert the page title here.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
    <script src="/javascript/common/TMCommon.js" type="text/javascript"></script>
    <script type="text/javascript">

        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='/Javascript/lib/jquery.min.js' type='text/javascript'%3E%3C/script%3E"));
        }

        var $j = jQuery.noConflict();
    </script>
    <link rel="stylesheet" type="text/css" href="/styles/print.css" />
    <%= PrintColors %>
</head>
<body>
    <form method="post" runat="server" id="mainform">
    <div class="print<%=IsPlanorHFSPage?"-plan":""%>">
        <sc:Placeholder ID="phMain" runat="server" Key="Content" />
    </div>
    </form>
    <script type="text/javascript">
        // BEGIN Google Analiytics Tracking code
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
        _gaq.push(['_setLocalRemoteServerMode']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        //END Google Analiytics Tracking code

        $j(document).ready(function () {
            $j('#siteSearchText').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    sitesearch('#siteSearchText');
                    return false;
                }
            });
        });
    </script>
</body>
</html>