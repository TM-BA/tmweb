﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HighlandRanchMain.aspx.cs" Inherits="TM.Web.Custom.Layouts.HighlandRanchMain" %>

<%@ Import Namespace="TM.Utils.Web" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core" TagPrefix="wfm" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="wf-museoslab-n3-active wf-active">
<head runat="server">
    <style type="text/css">
        #ibox {
            z-index: 1000000;
        }

        #ibox_overlay {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            z-index: 1000000;
        }

        #ibox_loading{ 
            position: absolute;
            z-index: 1000001;
        }

        #ibox_wrapper {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 1000001;
            padding: 30px 0px 0px 0px;
        }

        #ibox_content {
            z-index: 1000002;
            overflow: auto;
            height: 100%;
            position: relative;
            padding: 2px;
            text-align: left;
        }

            #ibox_content object {
                display: block;
            }

            #ibox_content .ibox_image {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                border: 0;
                display: block;
            }

        #ibox_footer_wrapper a {
            float: right;
            display: block;
            outline: 0;
            margin: 0;
            padding: 0;
        }

        #ibox_footer_wrapper {
            text-align: left;
            position: absolute;
            top: 5px;
            right: 10px;
            left: 10px;
            white-space: nowrap;
            overflow: hidden;
        }
    </style>
    <style type="text/css">
        #ibox_footer_wrapper {
            font-weight: bold;
        }

            #ibox_footer_wrapper a {
                text-decoration: none;
                color: #4C3329;
                text-transform: uppercase;
                font-weight: bold;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 16px;
            }

        #ibox_footer_wrapper {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: normal;
        }

        #ibox_wrapper {
            border: 1px solid #4C3329;
            box-shadow: 3px 3px 10px rgba(0,0,0,0.5);
        }

        #ibox_wrapper, #ibox_footer_wrapper a {
            background: #fff;
            color: #CCC;
        }

        #ibox_content {
            background: #fff;
            color: #fff;
            padding: 0;
        }

        #ibox_loading {
            padding: 50px;
            background: #FFF;
            color: #4C3329;
            font-size: 16px;
            font-weight: bold;
        }
    </style>
    <meta http-equiv="robots" content="all"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Language" content="en-US"/>
    <link href="/styles/microsites/highland_ranch.css" media="screen" rel="stylesheet" type="text/css">
    <%=JsCssCombiner.GetSet("blanklayoutjs")%>
    <script type="text/javascript" src="/javascript/microsites/ibox.js"></script>
    <%--<script src="//use.typekit.net/miy1rnc.js"></script>--%>
    <%--<script src="//use.typekit.net/chg1wge.js"></script>--%>
    <script type="text/javascript" src="/javascript/microsites/highland_ranch.js"></script>
</head>
<body class="<%: BodyClass %>">
    <sc:Text Field="CommonScripts" runat="server" />
    <div id="wrapper">
        <asp:Panel runat="server" ID="headerPanel">
            <sc:Placeholder Key="header" runat="server" />
        </asp:Panel>
        <div id="body">
            <div class="col left" id="leftColumn">
                <div class="pad">
                    <form id="form1" runat="server">
                         <sc:Text Field="LeftContent" runat="server" />
                    </form>
                    <p class="contact">Old Highway 50 and Blackstill Lake Road Clermont, Florida 34711<span class="phone">877.249.6168</span></p>
                </div>
            </div>
            <div class="col right" id="rightColumn">
                <div class="pad">
                    <sc:Text Field="Content" runat="server" />
                    <div class="clear">&nbsp;</div>
                </div>
            </div>
        </div>
        <asp:Panel ID="footerPanel" runat="server">
            <sc:Placeholder Key="footer" runat="server" />
        </asp:Panel>
    </div>
    <script type="text/javascript">
        $j(window).load(function () {
            var height = Math.max($j("#leftColumn").height(), $j("#rightColumn").height(), 600);
            $j("#body").css("min-height", height);
            $j("#leftColumn").css("min-height", height);
        });
    </script>
</body>
</html>
