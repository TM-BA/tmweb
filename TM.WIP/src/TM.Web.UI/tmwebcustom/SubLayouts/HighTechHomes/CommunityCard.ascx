﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityCard.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.HighTechHomes.CommunityCard" %>

<%@ Import Namespace="TM.Utils.Web" %>
<%@ Register src="../Search/CommunitySearchCard.ascx" tagname="CommunitySearchCard" tagprefix="uc1" %>

<%=JsCssCombiner.GetSet("searchfacetjs") %>
<script type="text/javascript">
    $j(document).ready(function () {

        var searchpath = '<%= Url %>';
        var comFav = "";

        
            $j(window).load(function () { GetCommunities(searchpath, 1, false, comFav, false,true); });


            $j("#searchsortby").change(function () {
                GetCommunities(searchpath, $j("#searchsortby").val(), false, comFav, false,true);
            });
        
    });
   
</script>
<uc1:CommunitySearchCard ID="CommunitySearchCard" runat="server" />

