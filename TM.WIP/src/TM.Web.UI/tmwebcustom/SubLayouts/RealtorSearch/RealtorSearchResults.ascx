﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealtorSearchResults.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.RealtorSearch.RealtorSearchResults" %>
<%@ Register TagPrefix="uc" TagName="SearchFacet" Src="HomeForSaleSearchFacet.ascx" %>
<%@ Register TagPrefix="uc" TagName="LiveChat" Src="~/tmwebcustom/SubLayouts/LiveChatButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="HomeForSaleSearchCard" Src="HomeForSaleSearchCard.ascx" %>
<script type="text/javascript">
    $j(document).ready(function () {
        var searchpath = "<asp:Literal ID='liturl' runat='server' />";
        $j(window).load(function () { GetInventorySearchResults(searchpath, 1, true, false); });

        $j("#searchsortby").change(function () {
            GetInventorySearchResults(searchpath, $j("#searchsortby").val(), true, false);
        });

        $j("a#msignupleadform").click(function () {
            $j('.UpdatesBoxModel').toggle();
        });
    });
</script>
<style>
    .searchfact
    {
        display: block;
    }
</style>
<div class="lt-col">
    <div class="blue-bar">
        <a href="#">Home for Sale Search</a>
    </div>
    <input type="hidden" id="hndsearchPath" value="<%= liturl.Text%>" />
    <input type="hidden" id="hndRealtor" value="true" />
    <uc:SearchFacet ID="ucSearchFact" runat="server" />
</div>
<div class="rt-col">
    <div id="campaigndtl" class="ppc-bnr">
        <asp:Literal runat="server" ID="litLandingPageCampaign"></asp:Literal>
    </div>
    <div class="ppc-bottom">
        <div class="ihc">
            <ul class="mapbut">
                <li><a id="msignupleadform" href="<%=CurrentPage.SCVirtualItemUrl%>/make-an-appointment" onclick="TM.Common.GAlogevent('Realtor', 'Click', 'MakeAnAppointment')">
                    Make an Appointment </a>
                </li>
                <li id="liveChat" class="chat-btn">
                    <uc:LiveChat runat="server" id="LiveChat">
                    </uc:LiveChat>
                </li>
            </ul>
            <div id="ihccard" class="ihc-info">
                <asp:Literal runat="server" ID="litihccard"></asp:Literal>
            </div>
        </div>
        <uc:HomeForSaleSearchCard ID="ucHomeForSaleSearchCard" runat="server" />
    </div>
</div>