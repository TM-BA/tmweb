﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RealtorSearchFacet.ascx.cs"
	Inherits="TM.Web.Custom.layouts.SubLayouts.RealtorSearch.RealtorSearchFacet" %>
<%@ Register TagPrefix="tm" Namespace="TM.Web.Custom.WebControls" Assembly="TM.Web.Custom" %>
<script src="/javascript/util/jquery-ui/jquery-ui.js" type="text/javascript"></script>
<link href="/Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="/javascript/search/realtor.search.facet.js" type="text/javascript"></script>
<script src="/javascript/search/facet.common.js" type="text/javascript"></script>
<script src="/javascript/lib/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<style>
	.lihdr
	{
		display: none;
	}
	.facetprogress
	{
		top: 23px;
	}
	.searchfact{margin: 0 0 17px;height:auto !important; }
</style>
<script>

	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		$j(window).load(function () { SetSearchFacet(searchpath); });

		var hnddiv = $j("#hnddivision").val();

		$j("#" + hnddiv).change(function () {
			if ($j(this).val() != "0")
				GetCitiesByDivision($j(this).val());
		});

		$j("#selectcity").change(function () { BindRealtorSearchFacet(); });
		$j("#selectBonusRoomDen").change(function () { BindRealtorSearchFacet(); });
		$j("#selectSchoolDistrict").change(function () { BindRealtorSearchFacet(); });
		$j("#selectCommunityStatus").change(function () { BindRealtorSearchFacet(); });
		$j("#selectAvailability").change(function () { BindRealtorSearchFacet(); });

//		$j("#sldrrange-StartingPrice").draggable();
//		$j("#sldrrange-SquareFeet").draggable();

	});

//	function RealtorResult() {
//		var hnddiv = $j("#hnddivision").val();
//		var selText = $j("#" + hnddiv + " option:selected").text();
//		window.location.href = window.location.href + "/" + selText;
//	}

	function Clear() {
		var hnddiv = $j("#hnddivision").val();
		ClearSearchFacet($j('#' + hnddiv).val());
	}

</script>
<div id="searchfacet" class="searchfact">
	<div class="facetprogress">
		<div>
			Loading...</div>
	</div>
	<input type="hidden" id="hnddivision" value="<%= ddldivision.ClientID%>" />
	<div id="selectAreaBox">
		<p>
			Select Area:</p>
		<div>
			<tm:ExtendedDropDownList runat="server" ID="ddldivision" />
		</div>
	</div>
	<section id="realtorfacet">
		<div id="sdrpriceRange" class="range">
			<p>
				Starting Price:<span id="startingprice"></span>
			</p>
			<div id="sldrrange-StartingPrice">
			</div>
		</div>
		<div id="selectcityBox">
			<p>
				City:</p>
			<div>
				<select id="selectcity">
				</select>
			</div>
		</div>
		<div id="lblBedsBox">
			<p>
				Bed:</p>
			<div>
				<ul id="lblBeds">
				</ul>
			</div>
		</div>
		<div id="lblBathsBox">
			<p>
				Bath:</p>
			<div>
				<ul id="lblBaths">
				</ul>
			</div>
		</div>
		<div id="lblGaragesBox">
			<p>
				Garage:</p>
			<div>
				<ul id="lblGarages">
				</ul>
			</div>
		</div>
		<div id="storyBox">
			
            <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
          { %> <p>Stories:</p> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
          {  %>  <p>Storeys:</p> <% } 
            else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
          {  %>  <p>Stories:</p> <% } %>

			<div>
				<ul id="lblStories">
				</ul>
			</div>
		</div>
		<div id="selectBonusRoomDenBox">
			<p>
				Bonus room/Den:</p>
			<div>
				<select id="selectBonusRoomDen" class="bonden">
				</select></div>
		</div>
		<div id="sdrSqftRange" class="range">
			<p>
				Square Feet:<span id="squarefeet"></span>
			</p>
			<div id="sldrrange-SquareFeet">
			</div>
		</div>
		<div id="selectSchoolDistrictBox">
			<p>
				School District:</p>
			<div>
				<select id="selectSchoolDistrict">
				</select></div>
		</div>
		<div id="selectCommunityStatusBox">
			<p>
				Community Status:</p>
			<div>
				<select id="selectCommunityStatus">
				</select></div>
		</div>
		<div id="selectAvailabilityBox">
			<p>
				Availability:</p>
			<div>
				<select id="selectAvailability">
				</select></div>
		</div>
		<div>
			<%--<a class="rlrsearch" href="javascript:BindRealtorSearchFacet();"></a>--%>
			<%--<a class="rlrsearch" href="javascript:RealtorResult();"></a>--%>
			<asp:LinkButton runat="server" class="button" Text="Search for Homes"  OnClick="OnClick_HomeSearch" ></asp:LinkButton>
			<a class="facetclear" href="javascript:Clear();">clear selections</a>
		</div>
	</section>
</div>
