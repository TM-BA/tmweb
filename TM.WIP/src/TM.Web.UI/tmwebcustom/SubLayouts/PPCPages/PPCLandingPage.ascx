﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PPCLandingPage.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.PPCPages.PPCLandingPage" %>
<%@ Register Src="PPCCommunityCard.ascx" TagPrefix="uc" TagName="PPCCommunityCard" %>
<%@ Register TagPrefix="uc" TagName="LiveChat" Src="~/tmwebcustom/SubLayouts/LiveChatButton.ascx" %>
<style>
	.ihc-info{margin: 10px 0 5px 10px;}
	@media only screen and (max-width : 960px)
	{	
		.ihc-info{margin: 10px 0 5px -10px;}
	}
		
	ul.mapbut
	{
		clear: both;
		float: left;
		height: 19px;
		margin: 10px 0 15px;
		padding: 0;
		width: 100%;
	}
	.mapbut li
	{
		float: left;
		margin: 0 auto;
		padding: 0;
		width: 100%;
	}
</style>
<div class="lt-col">
	<div class="blue-bar-ppc">
		<span>All Communities in <asp:Literal runat="server" ID="ltldivi" ></asp:Literal> </span>
	</div>
	<div class="ppc-lt">
		<div class="UpdatesBox">
			<p>
				Sign Up for Updates,<br>
				Special Deals & Invites
			</p>
			<sc:Placeholder runat="server" ID="phPPCDivisionLeadForm" Key="PPCDivisionLeadForm"></sc:Placeholder>
		</div>
		<div id="ihccard"  class="ihc-info">
			<asp:Literal runat="server" ID="litihccard"></asp:Literal>
		</div>
		<ul class="mapbut">
			<li id="liveChat" class="chat-btn">
					<uc:LiveChat runat="server" id="LiveChat"></uc:LiveChat> 
				</li>
		</ul>
		<p>
			<asp:Literal runat="server" id="litseocontent"></asp:Literal>
		</p>
	</div>
	<!--end ppc-lt-->
</div>
<div class="rt-col">
	<div id="campaigndtl" class="ppc-bnr">
		<asp:Literal runat="server" ID="litLandingPageCampaign"></asp:Literal>
	</div>
	<uc:PPCCommunityCard runat="server" ID="ucPPCCommunityCard" />
</div>
