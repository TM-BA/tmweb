﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PasswordReset.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.PasswordReset" %>
<div class="gr-bk">
	<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
			<img src="<%=MycompanyLogo %>"/>
		</div>
	<p>
		Account
	</p>
</div>
<div class="create-acc">
	<h1>
		Reset Your <%=MycompanySmall%> Account Password</h1>
	<p>
		Enter your new password. Already have your password?
		<a href="/Account/Login">Login</a></p>
	<asp:ValidationSummary runat="server" CssClass="errbx" ID="vlsErrorMessage" EnableClientScript="True"
		ShowSummary="True" DisplayMode="SingleParagraph" HeaderText="Please provide a valid information."  Enabled="True" ShowMessageBox="False" />
	<asp:PlaceHolder runat="server" ID="phPasswordReset">
		<span>Password *</span>
		<asp:TextBox runat="server" TextMode="Password"  ID="txtPassword" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqPassword" ControlToValidate="txtPassword"
			EnableClientScript="True"></asp:RequiredFieldValidator>
		<span>Confirm Password *</span>
		<asp:TextBox runat="server" TextMode="Password" ID="txtConfirmPassword" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqConfirmPassword" ControlToValidate="txtConfirmPassword"
			EnableClientScript="True"></asp:RequiredFieldValidator>
		<asp:LinkButton runat="server" CssClass="button" Text="Save password<span></span>"  ID="butLogin" OnClick="OnClick"></asp:LinkButton>
	</asp:PlaceHolder>
</div>
