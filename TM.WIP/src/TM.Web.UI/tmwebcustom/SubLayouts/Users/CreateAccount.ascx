﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.Users.CreateAccount" %>
	<%@ Register TagPrefix="tm" Namespace="TM.Web.Custom.WebControls" Assembly="TM.Web.Custom" %>
<asp:PlaceHolder runat="server" ID="phcreateaccount">
	<div class="gr-bk">
		<div class="mytm-logo" style="background:none; margin:4px 5px 0 14px;" >
			<img src="<%=MycompanyLogo %>"/>
		</div>
		<p>
			Account
		</p>
	</div>
	<div class="create-acc">
		<h1>
			Create your <%=MycompanySmall %> account</h1>
		<p>
			A <%=MycompanySmall %> account allows you to save your favorite homes and communities.
		</p>
		<asp:ValidationSummary runat="server" ID="vlsErrorMessage" CssClass="errbx" EnableClientScript="True"
			ShowSummary="True" DisplayMode="SingleParagraph" Enabled="False" HeaderText=""
			ShowMessageBox="False" />
		<p>
			*= required information</p>
		<span>First Name *</span>
		<asp:TextBox runat="server" ID="txtFirstName" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqFirstName" CssClass="errtext" ControlToValidate="txtFirstName"
			ErrorMessage="First name is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<span>Last Name *</span>
		<asp:TextBox runat="server" ID="txtLastName" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqLastName" CssClass="errtext" ControlToValidate="txtLastName"
			ErrorMessage="Last name is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<span>Email Address *</span>
		<asp:TextBox runat="server" ID="txtEmailAddress" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqEmailAddress" CssClass="errtext"
			ControlToValidate="txtEmailAddress" ErrorMessage="Email is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<asp:RegularExpressionValidator runat="server" ID="regemail" ErrorMessage="Invalid email address format"
			ControlToValidate="txtEmailAddress" CssClass="errtext" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
		<span>Password *</span>
		<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqPassword" CssClass="errtext" ControlToValidate="txtPassword"
			ErrorMessage="Password is required" EnableClientScript="True"></asp:RequiredFieldValidator>
		<span>Confirm Password *</span>
		<asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" CssClass="tm-account"></asp:TextBox>
		<asp:RequiredFieldValidator runat="server" ID="reqConfirmPassword" CssClass="errtext"
			ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required"
			EnableClientScript="True"></asp:RequiredFieldValidator>
		<asp:CompareValidator runat="server" ID="comconfpwd" CssClass="errtext" ControlToValidate="txtPassword"
			ErrorMessage="Non-matching passwords" ControlToCompare="txtConfirmPassword"></asp:CompareValidator>
		<span>Area of Interest *</span>
		<tm:ExtendedDropDownList runat="server" ID="ddlAreaofinterest" class="tm-account" />
		<asp:RequiredFieldValidator runat="server" ID="reqAreaofinterest" CssClass="errtext"
			ControlToValidate="ddlAreaofinterest" ErrorMessage="Area of Interest is required"
			InitialValue="0" EnableClientScript="True"></asp:RequiredFieldValidator>
		<div class="clearfix"></div>
		<asp:LinkButton runat="server" ID="butCreate" CssClass="button" Text="Create Account<span></span>" OnClick="OnClick"></asp:LinkButton>
	</div>
	<!--end create-acc-->
	<div class="disc">
		We take your privacy seriously and assure that the information you supply will not
		be shared with anyone outside our company. 
        
        <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
               { %>
                <a href="/Privacy-Policy">Please read our Privacy Policy.</a>
              <% } else { %>
               <a href="/Privacy-Policies">Please read our Privacy Policy.</a>
               <% } %> 
        
     </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phthankyou">
	<div class="plan-details-wrapper">
		<div class="gr-bk">
			<p>
				Thank You
			</p>
		</div>
		<div class="create-acc">
			<p>
				Thank you for creating a <%=Mycompany %> account. We will keep you posted on the latest news,
				offers, events, home buying tips and more. You may now save your favorite communities,
				plans and homes for sale to your favorites by clicking <a>
					<img src="/images/tm/redStar2.png" alt="">Add to Favorites</a>.</p>
			<div class="next-steps">
				<h1>
					Next Steps</h1>
				<p>
					Explore our beautiful community in the <asp:Literal runat="server" ID="litSelectedArea"></asp:Literal></p>
				<p>
					Want to make sure our emails get to your inbox?
					<br>
					Be sure to include <%=MycompanyEmail%> in your email address book.</p>
				<%if (MycompanySmall != "myFavs") {%>
				<p>
					<a href="/Home-Buying">Visit Homebuying 101</a> to help decode the processing of buying a home.
				</p>
				<%} %>
				<asp:Literal ID="litPreQual" runat="server"></asp:Literal>
			</div>
			<div class="ways-connect">
<h1>More Ways To Connect</h1>
<asp:Literal ID="litSocialMedia" runat="server"></asp:Literal>
<asp:Literal ID="litIhc" runat="server"></asp:Literal>
<asp:Literal ID="litBlogLink" runat="server"></asp:Literal>
</div>
		</div>
		<!--end create-acc-->
	</div>
</asp:PlaceHolder>
