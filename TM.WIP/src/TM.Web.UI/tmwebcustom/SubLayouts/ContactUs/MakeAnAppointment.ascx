﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MakeAnAppointment.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.MakeAnAppointment" %>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
<link href="/~/media/taylormorrison/contact-us/css/main" rel="stylesheet">

<div class="container contact-us">
    <div class="row hero">
        <img src="/~/media/taylormorrison/contact-us/images/50720_TMCorp_Schedule_Appointment" class="img-responsive" />        
    </div>
    <div class="row">
        <div class="col-xs-12 bg-gray no-padding">
            <div class="content">
                <br />
                <p><strong>Homebuyers are our inspiration and we cannot wait to hear from you. Ask us anything you want and we&rsquo;ll get back to you as quickly as possible with an answer.</strong></p>
                <div class="clickdform mainDiv"
                    <sc:Placeholder ID="scForm" Key="MaaForm" runat="server" /> 
                </div>
            </div>
        </div>
    </div>
    

</div>
<style>
    .img-responsive {
        width: 100%;
        height: auto;
    }
    .scfRequired {
        display: none;
    }
    .scfDatePickerGeneralPanel
    {
        width:100px;
    }
    .scfSectionBorderAsFieldSet {
        border-top:0px;
        margin:0px;
        padding:0px;
    }
    .bg-gray {
        padding-bottom:7.5%;
    }
    .content .scfSubmitButtonBorder input {
        font-family: Arial, sans-serif !important;
        font-size: 14px !important;
        font-weight: bold !important;
        color: #fff !important;
        text-transform: uppercase;
        padding: 10px 30px;
        margin-bottom: 20px;
        border: none !important;
        background:url() !important;
        text-indent:0px !important;
        width: 120px !important;
        height: 35px !important;
        background-color: #808184 !important;
        box-shadow:none;
        margin-right: 26px;
        margin-top: 8px;
    }
    .contact-us .content {
        padding: 0 100px !important;
    }
    .halfAvailableWidth {
        margin:10px 0px;
    }
    .content select {
        background:white;
    }
    .halfAvailableWidth .scfDropListGeneralPanel, .halfAvailableWidth .scfSingleLineGeneralPanel {
        width:93% !important;
    }
    .content .scfMultipleLineGeneralPanel {
        width: 96.5% !important;
    }
    .thirdAvailableWidth label {
        float:none;
        width:100%;
        font-weight:bold;
        margin-top:-13px;
        margin-bottom:0px;
    }
    .content .scfTelephoneGeneralPanel {
        margin-top:9px;
        width: 93%;
    }
    .phoneNum {
        width: 50%;
        clear: left;
    }
    .dateVisit, .timeVisit {
        width: 25%;
    }
    .scfDropList, .scfSectionContent input, .scfSectionContent textarea {
        border:1px solid #999;
    }
    @media screen and (max-width:767px) {
        .contact-us h1 {
            font-size: 32px;
            top: 30px;
            left: 50px;
        }
        .contact-us .side {
            min-height: 0;
        }
        .contact-us .main {
            border-left: none;
        }
        .contact-us .divisions.left {
	        border-right: none;
        }
        .contact-us .content {
            padding: 0 20px !important;
        }
        .halfAvailableWidth, .phoneNum, .scfMultipleLineTextBorder {
            width: 100% !important;
        }
        .scfMultipleLineTextBorder {
            width: 96% !important;
        }
        .dateVisit, .timeVisit {
            width: 50%;
            margin-top: 20px;
        }
        .home-content-wrapper, .footer-content-wrapper, header, .logomenu, .searchbox, .logomenu .lt-box {
            width: 100% !important;
        }
        .logo {
            display: block;
        }
        .searchbox {
            padding-top: 0px;
        }
    }
</style>
<script>
    $j(document).ready(function () {
        $j("#globalRFI input").filter('[type="submit"]').click(TM.Common.GAlogevent('LeadForm', 'Click', 'LeadForm'))
    });
</script>