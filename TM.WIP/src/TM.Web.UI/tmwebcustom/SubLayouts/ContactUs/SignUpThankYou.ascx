﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignUpThankYou.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.SignUpThankYou" %>
<div class="gr-bk">
    <p><asp:Literal ID="litTitle" runat="server"></asp:Literal></p>        
</div>


<div class="create-acc">
    <div class="leftContent">
        <asp:Literal ID="litContent" runat="server"></asp:Literal>
    </div>

        <div id="nextSteps" class="rightContent" runat="server">
            <asp:Literal ID="litNextStep" runat="server"></asp:Literal>
        </div>
        <div id="createAccount" class="rightContent" runat="server">
            <asp:Panel ID="pnlCreateAccount" runat="server">
            <p>Provide a password to save communities and homes to your favorites.<br>
                * required information</p>

                <span>Password:</span>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="tm-account"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="newAccount" ControlToValidate="txtPassword" ErrorMessage="You need to provide a password for your account" runat="server" CssClass="error" display="Dynamic"></asp:RequiredFieldValidator>
                <span>Confirm Password:</span>
                <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" CssClass="tm-account"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="newAccount" ControlToValidate="txtPassword2" ErrorMessage="You need to provide a confirmation password for your account" runat="server" CssClass="error" display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" ValidationGroup="newAccount" ControlToValidate="txtPassword2" ControlToCompare="txtPassword" ErrorMessage="Please confirm you password to continue" runat="server" CssClass="error" Display="Dynamic"></asp:CompareValidator>

                <asp:LinkButton ValidationGroup="newAccount" ToolTip="Create Account" runat="server" ID="lnkBtnCreateAccount" CssClass="create-acc-btn" />
            </asp:Panel>
            <div class="th-bt">
                <asp:Literal ID="litAccountCreated" runat="server"></asp:Literal>    
            </div>    
        </div>
        <br/>
        <br/>
        <div id="waystoconnect" class="rightContent" runat="server">
            <h1>More Ways To Connect</h1>
            <asp:Literal ID="litSocialMedia" runat="server"></asp:Literal>
            <asp:Literal ID="litIhc" runat="server"></asp:Literal>
            <asp:Literal ID="litBlogLink" runat="server"></asp:Literal>
        </div>
        <div id="waystoconnectContent" class="rightContent" runat="server">
            <asp:Literal ID="litWaysConnectContent" runat="server"></asp:Literal>
        </div>

</div>

<script>
    $j(document).ready(function () {
        $j("a.InterestItem").click(TM.Common.GAlogevent('Details', 'Click', '<%= InterestItemGALabel %>'));
        $j("a.create-acc-btn").click(TM.Common.GAlogevent('CreateAccount', 'Click', 'CreateAccount'));
        $j(".fb").click(TM.Common.GAlogevent('Social Media', 'Click', 'Facebook'));
        $j(".tw").click(TM.Common.GAlogevent('Social Media', 'Click', 'Twitter'));
    });
</script>