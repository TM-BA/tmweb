﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UniversalRFI.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.ContactUs.UniversalRFI" %>

<div id="globalRFI">
    <h1>Find out more about <%= LongTitle %></h1>
    <p><asp:Literal ID="litIntro" runat="server"></asp:Literal></p>
    <p><asp:Literal ID="litIhcText" runat="server"></asp:Literal></p>

        
    <%--
        Removed per ticket 62671
    <asp:Image ID="imgMain" runat="server" CssClass="rfiImg"/>--%>
    <sc:Placeholder ID="scForm" Key="RfiForm" runat="server" />
    <asp:HyperLink id="lnkCancel" runat="server" class="cancelLink">Cancel & go back</asp:HyperLink>

</div>
<style>
.scfRequired {
display: none;
}
.scfDatePickerGeneralPanel
{
    width:100px;
}

</style>
<script>
    $j(document).ready(function () {
        $j("#globalRFI input").filter('[type="submit"]').click(TM.Common.GAlogevent('LeadForm', 'Click', 'LeadForm'))
    });
</script>