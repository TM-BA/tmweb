﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailablePositions.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Carrer.AvailablePositions" %>
    <div class="lt-col">
        <sc:placeholder runat="server" id="LeftNavigation" key="LeftNavigation">
        </sc:placeholder>
    </div>
    <div class="rt-col">
        <div class="content-pg">
            <sc:placeholder runat="server" id="ContentArea" key="ContentArea">
            </sc:placeholder>
        </div>

         <div class="filters">

        <div class="filter-left">
            
            <p>Filter by City, State</p>

            <asp:DropDownList ID="ddlCityState" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddlCityState_SelectedIndexChanged">
            </asp:DropDownList>
        
        </div>

        <div class="filter-right">

            <p>Filter by Department</p>

            <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddlDepartment_SelectedIndexChanged">
            </asp:DropDownList>


        
        </div>
    
    
    
    </div>

            <asp:Repeater ID="rptAvailablePositions" runat="server" 
                    onitemdatabound="rptAvailablePositions_ItemDataBound">
                <ItemTemplate>
                  <div class="career-box">
                        <asp:HyperLink ID="hlkTitle" runat="server" Target="_blank" CssClass="career-box-link"></asp:HyperLink>
                         <asp:Label ID="PostingStartDate" runat="server"  CssClass="career-date"></asp:Label>
                        <asp:Label ID="locationlbl" runat="server"   CssClass="career-box-label"></asp:Label>
                               
                  </div>
                </ItemTemplate>
            </asp:Repeater>
            <div class="results-msg"><asp:Label ID="lblNoResults" runat="server" Text=""></asp:Label></div>
    </div>



