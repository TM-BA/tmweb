﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Careers.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Career.Careers" %>
 <div class="lt-col">
        <sc:placeholder runat="server" id="LeftNavigation" key="LeftNavigation">
        </sc:placeholder>
    </div>

    <div class="rt-col">
        <div class="content-pg">
            <sc:placeholder runat="server" id="ContentArea" key="ContentArea">
    
            </sc:placeholder>
        </div>
    </div>