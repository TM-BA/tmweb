﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="TM.Web.Custom.layouts.SubLayouts.Search.Search" %>
<%@ Register TagPrefix="uc" TagName="SearchFacet" Src="SearchFacet.ascx" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="SearchMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="IHCCard" Src="~/tmwebcustom/SubLayouts/Common/IHCCard.ascx" %>
<%@ Register TagPrefix="uc" TagName="CommunitySearchCard" Src="CommunitySearchCard.ascx" %>
<script type="text/javascript">
	$j(document).ready(function () {
		var searchpath = "<asp:Literal ID='liturl' runat='server' />";
		var comFav = "<asp:Literal ID='litcomFav' runat='server' />";
		$j(window).load(function () { GetCommunities(searchpath, 1, false, comFav, false,false); });

		$j("#searchsortby").change(function () {
			GetCommunities(searchpath, $j("#searchsortby").val(), false, comFav, false,false);
		});

		var container = $j("div#searchfacet");
		$j("a#searchfacet").click(
				function (event) {
					event.preventDefault();
					FacetBox(container);
				}
		);

		$j("div.advSearBox").click(
				function (event) {
					event.preventDefault();
					FacetBox(container);
				}
		);

		$j("a#msignupleadform").click(function () {
			$j('.UpdatesBoxModel').toggle();
		});
	});

	function FacetBox(container) {
		if (container.is(":visible")) {
			TM.Common.GAlogevent('Search', 'Click', 'AdvSearchClose', '');
			$j("a#searchfacet").css('background-image', 'url(/images/tm/arrowUp.png)');
			$j("a#searchfacet").html("see search options");
			container.slideUp(500);
			$j("div.magglass").css("z-index", "9999");
			if ($j(".home-content-wrapper").css('width') == '334px') {
				$j("#srhfactbx").css('bottom', '0');
			}

		} else {
			TM.Common.GAlogevent('Search', 'Click', 'AdvSearch-Open', '');
			$j("a#searchfacet").css('background-image', 'url(/images/tm/arrowDown.png)');
			$j("a#searchfacet").html("hide search options");
			$j("div.magglass").css("z-index", "999");
			container.slideDown(500);
			if ($j(".home-content-wrapper").css('width') == '334px') {
				$j("#srhfactbx").css('bottom', '-450px');
			}
		}
	}

</script>
<div class="lt-col">
	<section class="hm-lt_sr">
		<input type="hidden" id="hndsearchPath" value="<%= liturl.Text%>" />
		<input type="hidden" id="hndFavi" value="<%= litcomFav.Text%>" />
		<input type="hidden" id="hndRealtor" value="false" />
		<sc:Placeholder runat="server" ID="phLeftAreaPromo" Key="LeftAreaPromo" />
		
		<sc:Placeholder ID="phContent" runat="server" Key="LeftContent" />
	
		<div class="advSearBox">
			<div class="magglass">
			</div>
			<div class="advSearBox_t1">
				Advanced Search
			</div>
			<p class="advSearBox_t2">
				by price, bedrooms and more.
			</p>
		</div>
		<div id="srhfactbx">
			<div class="s-opt">
				<a id="searchfacet" href="#" onclick="TM.Common.GAlogevent('Search','Click','AdvSearch-Open')">see search options</a>
			</div>
			<uc:SearchFacet ID="ucSearchFact" runat="server" />
		</div>
	</section>
</div>
<div class="rt-col">
	<uc:SearchMap ID="ucSearchMap" runat="server" />
	<div class="legend-map">
		<ul>
			<li class="marker-1">Now Selling</li>
			<li class="marker-2">Coming Soon</li>
			<li class="marker-3">Closeout</li>
			<li class="marker-4">Design Studio</li>
		</ul>
	</div>
	<!--end of legend-map-->
	<div class="ihc">
		<ul class="mapbut" style="position: relative">
			<li><a id="msignupleadform" href="<%=CurrentPage.SCVirtualItemUrl%>/make-an-appointment" onclick="TM.Common.GAlogevent('Updates', 'Click', 'MakeAnAppointment')">
                    Make an Appointment </a>
			</li>
			<li id="liveChat" class="chat-btn">
				<sc:Placeholder ID="Placeholder1" runat="server" Key="LiveChat" />
			</li>
		</ul>
		<uc:IHCCard id="ucIHCCard" runat="server" />
	</div>
</div>
<uc:CommunitySearchCard ID="ucCommunitySearchCard" runat="server" />
