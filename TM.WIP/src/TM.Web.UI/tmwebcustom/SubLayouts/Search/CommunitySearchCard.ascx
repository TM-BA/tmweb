﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunitySearchCard.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Search.CommunitySearchCard" %>
<div class="search-results-wrapper">
	<div class="h-sear">
		<div class="h-sear1">
			Sort by:</div>
		<div class="h-sear2">
			<select id="searchsortby" name="sortBy" class="sb1" onchange="CommunitySort()">
				<option value="1" selected="selected">Community name</option>
				<option value="2">Price low to high</option>
				<option value="3">Price high to low</option>
				<option value="4">Location</option>
			</select></div>
		<div class="h-sear3">
			COMMUNITY</div>
		<div class="h-sear4">
			LOCATION</div>
		<div class="h-sear5">
			DETAILS</div>
		<div class="h-sear6">
			PROMOTIONS</div> 
	</div>
	<div id="CommunitySearchResult">
	</div>
	<% if(SCCurrentDeviceName == "DarlingHomes") { 
         if(TM.Web.Custom.Layouts.DH_Main.IsMobile()) { %> 
            <script id="communitycard" type="text/x-jquery-tmpl" src="/~/media/darlinghomes/mobile/js/commcard.js"></script>   
        <% } else { %>
            <script id="communitycard" type="text/x-jquery-tmpl" src="/~/media/darlinghomes/desktop/js/commcard.js"></script>  
        <% }
    } else if(TM.Web.Custom.Layouts.DH_Main.IsMobile()) { %> 
        <script id="communitycard" type="text/x-jquery-tmpl" src="/~/media/taylormorrison/mobile/js/commcard.js?20160922"></script>   
    <% } else { %>
        <script id="communitycard" type="text/x-jquery-tmpl" src="/~/media/taylormorrison/desktop/js/commcard.js"></script>  
    <% } %>
	<script id="nocommunitycard" type="text/x-jquery-tmpl">    		
		<div class="commCard">					
			<p>No records found</p>
			</div>
	</script>
    <script>
        $j(function () {
            <% if(SCCurrentDeviceName == "DarlingHomes") { 
                 if(TM.Web.Custom.Layouts.DH_Main.IsMobile()) { %> 
                    var src = "/~/media/darlinghomes/mobile/js/commcard.js";
                <% } else { %>
                    var src = "/~/media/darlinghomes/desktop/js/commcard.js";
                <% }
            }  else if(TM.Web.Custom.Layouts.DH_Main.IsMobile()) { %>
                var src = "/~/media/taylormorrison/mobile/js/commcard.js?20160922";
            <% } else { %>
                var src = "/~/media/taylormorrison/desktop/js/commcard.js";
            <% } %>
            $j.get(src, null, function (data) {
                communityTmpl = data;
            }, "text");
            /*loadExternalKnockoutTemplates(function () {
                communityTmpl = $j('#communitycard').template();
            });*/
        });
			
        function loadExternalKnockoutTemplates(callback) {
            var sel = 'script[src][type="text/x-jquery-tmpl"]:not([loaded])';
            $toload = $j(sel);
            function oncomplete() {
                this.attr('loaded', true);
                var $not_loaded = $j(sel);
                if (!$not_loaded.length) {
                    callback();
                }
            }
            $toload.each(function (index, elem) {
                var $elem = $j(elem);
                $elem.load($elem.attr('src'), oncomplete.bind($elem));

            });
        }
            	
        var userIsLoggedIn = '<%=CurrentWebUser != null %>';
	</script>
</div>
