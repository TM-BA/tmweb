﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Content.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.AdoraTrails.Content" %>
<div id="site-content">
    <div class="nav-shadow-top"></div>
    <div id="content-columnLeft">
        <div class="nav-shadow-left"></div>
        <sc:Text runat="server" Field="LeftContent" />
    </div>
    <div id="content-columnRight">	
        <div class="nav-shadow-right"></div>
        <sc:Text runat="server" Field="RightContent" />
    </div>
</div>

