﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.AdoraTrails.Header" %>
<div id="top-green-bar">
    <a href="/"><div id="adora-logo"></div></a>
</div>
<div id="header">
    <div class="nav-shadow-top"></div>
    <div class="header" id="header-<%=HeaderClass%>">
        <sc:Image runat="server" Field="HeaderImage"/>
        <div id="header-float-<%=HeaderClass%>"></div>
    </div>
    <div class="nav-shadow-bottom"></div>
    <div id="shade-overlay" class="home">
        <div id="header-text" class="<%=HeaderClass%>">
            <sc:Text runat="server" Field="HeaderText" />
        </div>
    </div>
</div>
<div class="clear"></div>
<div id="main-menu">
    <ul class="menu">
        <li><a id="menu-home" href="/">Home</a></li>
        <li><a id="menu-community-amenities" href="/community-amenities/overview">Community Amenities</a></li>
        <li><a id="menu-area-information" href="/area-information">Area Information</a></li>
        <li><a id="menu-find-your-home" href="/find-your-home">Find Your Home</a></li>
        <li><a id="menu-location-and-directions" href="/location-and-directions">Location &amp; Directions</a></li>
        <li><a id="menu-contact" href="/contact">Contact</a></li>
    </ul>
</div>
<div id="sub-menu">
    <div class="subMenu-wrapper">
        <div class="subMenu-holder" id="community-amenities" style="display: none;">
            <div class="subMenu-filler">
                <div class="subMenu-column">
                    <div class="subMenu-column-top">
                        <div class="subMenu-column-top-left"></div>
                        <div class="subMenu-column-top-right"></div>
                        <div class="clear"></div>

                    </div>
                    <div class="subMenu-column-left"></div>
                    <div class="subMenu-column-right"></div>
                    <div class="subMenu-column-bottom">
                        <div class="subMenu-column-bottom-left"></div>
                        <div class="subMenu-column-bottom-right"></div>
                    </div>								
                    <div class="subMenu-column-content">
                        <a href="/community-amenities/overview" class="Asub">Overview &amp; Site Map</a>
                        <a href="/community-amenities/neighborhood-clubhouse" class="Asub">Neighborhood Clubhouse</a>
                        <a href="/community-amenities/outdoor-recreation" class="Asub">Outdoor Recreation</a>
                        <a href="/community-amenities/on-site-elementary" class="Asub">On-Site Elementary</a>
                        <a href="/community-amenities/event-gallery" class="Asub">Event Gallery</a>
                        <a href="/community-amenities/video-tour" id="video-tour-button">Take the Video Tour</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subMenu-holder" id="area-information" style="display: none;">
            <div class="subMenu-filler">
                <div class="subMenu-column">
                    <div class="subMenu-column-top">
                        <div class="subMenu-column-top-left"></div>
                        <div class="subMenu-column-top-right"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="subMenu-column-left"></div>
                    <div class="subMenu-column-right"></div>
                    <div class="subMenu-column-bottom">
                        <div class="subMenu-column-bottom-left"></div>
                        <div class="subMenu-column-bottom-right"></div>
                    </div>
                    <div class="subMenu-column-content">
                        <a href="/area-information/town-of-gilbert" class="Asub">Town of Gilbert</a>
                        <a href="/area-information/shopping-and-dining" class="Asub">Shopping &amp; Dining</a>
                        <a href="/area-information/golf" class="Asub">Golf</a>
                        <a href="/area-information/entertainment" class="Asub">Entertainment</a>
                        <a href="/area-information/schools" class="Asub">Schools</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="subMenu-holder" id="find-your-home" style="display: none;">
            <div class="subMenu-filler">
                <div class="subMenu-column">
                    <div class="subMenu-column-top">
                        <div class="subMenu-column-top-left"></div>
                        <div class="subMenu-column-top-right"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="subMenu-column-left"></div>
                    <div class="subMenu-column-right"></div>
                    <div class="subMenu-column-bottom">
                        <div class="subMenu-column-bottom-left"></div>
                        <div class="subMenu-column-bottom-right"></div>
                    </div>
                    <div class="subMenu-column-content">
                        <a href="/find-your-home/about-taylor-morrison" class="Asub">About Taylor Morrison</a>
                        <a href="/find-your-home/encoreii-collection" class="Asub">Encore II Collection</a>
                        <a href="/find-your-home/discoveryii-collection" class="Asub">Discovery II Collection</a>
                        <a href="/find-your-home/summit-collection" class="Asub">Summit Collection</a>
                        <a href="/find-your-home/passage-collection" class="Asub">Passage Collection</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
