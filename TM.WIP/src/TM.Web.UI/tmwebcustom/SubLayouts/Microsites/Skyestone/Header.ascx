﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone.Header" %>
<div class="topbar">
    <ul>
        <li>
            <asp:HyperLink ID="PhoneLink" runat="server">HyperLink</asp:HyperLink> </li>
        <li><asp:HyperLink onclick="TM.Common.GAlogevent('SkyestoneBlog', 'Click','SkyestoneBloglink');" ID="BlogLink" runat="server">BLOG</asp:HyperLink></li>
        <li><asp:HyperLink onclick="TM.Common.GAlogevent('SkyestoneLead', 'Click','SkyestoneGetUpdates');" ID="GetUpdatesLink" runat="server">GET UPDATES</asp:HyperLink></li>
    </ul>
</div>

<!--end topbar-->
<div class="logo-bg">
</div>
<div class="logo">
    <a href="/" onclick="TM.Common.GAlogevent('SkyestoneNavigation', 'Click','SkyestoneGoHome');">
        <asp:Literal ID="Logo" runat="server"></asp:Literal>
        </a></div>
<div class="bnner-hder">
     <asp:Literal ID="HeaderBanner" runat="server"></asp:Literal></div>
