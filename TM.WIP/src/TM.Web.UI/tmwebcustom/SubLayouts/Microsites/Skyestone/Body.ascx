﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Body.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.Skyestone.Body" %>
<link rel="stylesheet" type="text/css" href="/Styles/Microsites/bjqs.css" />
<script type="text/javascript" src="/javascript/lib/jquery1.9.0.min.js"></script>
<script type="text/javascript" src="/javascript/Microsites/bjqs-1.3.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        $('.slider').bjqs({
            height: 310,
            width: 256,
            responsive: true,
            showmarkers: false,
            usecaptions: false,
            animtype: 'fade'
        });

    });

</script>
<div class="idea">
    <div class="title1">
        <asp:Literal ID="TheIdeaTitle" runat="server"></asp:Literal>
    </div>
    <!--end title1-->
    <div class="text1">
        <p>
            <asp:Literal ID="TheIdeaText" runat="server"></asp:Literal>
        </p>
    </div>
    <!--end text1-->
</div>
<!--end idea-->
<div class="center-img">
  <sc:Placeholder runat="server" ID="phCenterImage" Key="Center" /></div>
<div class="community">
  <sc:Placeholder runat="server" ID="phCommunity" Key="Community" /></div>
<div class="collections">
  <sc:Placeholder runat="server" ID="phCollections" Key="Collections" /></div>
<div class="slider">
    <ul class="bjqs">
        <asp:Literal ID="Slider" runat="server"></asp:Literal>
    </ul>
</div>
<div class="sm-right">
    <sc:Placeholder runat="server" ID="phSmRight" Key="SMRight" />
</div>
<div class="location">
    <sc:Placeholder runat="server" ID="phLocation" Key="Location" />
</div>
<div class="box-mob">
</div>
<div class="box-bt">
    <div class="sm-left">
          <sc:Placeholder runat="server" ID="phSMLeft" Key="SMLeft" /></div>
    <div class="title2">
     <asp:Literal ID="TheExperienceTitle" runat="server"></asp:Literal>
      
    </div>
    <div class="text2">
        <asp:Literal ID="TheExperience" runat="server"></asp:Literal>
    </div>
</div>
<!--end box-bt-->
