﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.HighlandRanch.Header" %>
<div id="header">
    <div class="content">
        <div class="banner">
            <sc:Image runat="server" Field="HeaderImage" CssClass="fullHeight" />
            <div class="head">
                <a href="/" class="logo">
                    <img src="/images/microsites/highlandranch/logo.png" alt="Highland Ranch"></a><ul class="menu">
                        <li><a id="menu-neighborhoods" href="/neighborhoods">Neighborhoods</a></li>
                        <li><a id="menu-information" href="/area-information">Area Information</a></li>
                        <li><a id="menu-homes" href="/directions">Directions</a></li>
                    </ul>
            </div>
            <div class="main">
                <sc:Text runat="server" Field="HeaderMain" />
            </div>
            <div class="sub">
                <sc:Text runat="server" Field="HeaderSub" />
            </div>
            <div class="foot">&nbsp;</div>
        </div>
    </div>
</div>
