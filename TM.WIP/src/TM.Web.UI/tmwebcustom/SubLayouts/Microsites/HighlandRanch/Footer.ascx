﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Microsites.HighlandRanch.Footer" %>
<div id="footer">
    <div class="content">
        <div class="col left">&nbsp;</div>
        <div class="pad">
            <ul class="menu">
                <li><a id="menu-neighborhoods" href="/neighborhoods">Neighborhoods</a></li>
                <li><a id="menu-information" href="/area-information">Area Information</a></li>
                <li><a id="menu-homes" href="/directions">Directions</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="legal">
    <div class="col left">&nbsp;</div>
    <div class="col right">
        <ul>
            <li><a href="/disclaimer" target="_blank">Full Disclaimer</a></li>
            <li><a href="http://www.taylormorrison.com/Privacy-Policies" target="_blank">Privacy Policy</a></li>
            <li><a href="http://www.taylormorrison.com/terms-of-use" target="_blank">Terms &amp; Conditions</a></li>
        </ul>
    </div>
    <div class="clear">&nbsp;</div>
</div>
