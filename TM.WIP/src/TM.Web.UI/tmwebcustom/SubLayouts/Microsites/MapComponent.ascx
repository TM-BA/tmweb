﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MapComponent.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.MapComponent" %>
<%@ Register TagPrefix="uc" TagName="SearchMap" Src="../Search/SearchMap.ascx" %>
<script type="text/javascript" src="/javascript/search/searchMap.js"></script>
<script type="text/javascript" src="/javascript/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/javascript/lib/markerclusterer.js"></script>
<div id="mapContainer">
    <uc:SearchMap ID="ucSearchMap" runat="server" />
</div>
<span style="display: none" id="spanNoDataSource">No datasource or neighborhoods are associated with this map control</span>
<script type="text/javascript">
    $j(function () {
        //InitializeDefaultMap("40.111689", "-95.888672", 4); //center of USA        
        
        var communityJson = <%= CommunityJSON %>;
        if (communityJson.length > 0) {
            $j('#mapContainer').show();
            $j('#spanNoDataSource').hide();
            initializeMicroSiteMap(communityJson);
        } else {
            $j('#mapContainer').hide();
            $j('#spanNoDataSource').show();
        }

    });

    function initializeMicroSiteMap(communities) {
        var tmmap = new TM.SearchMap(communities);

        tmmap.createMarker = function(community) {

            var latlng = new google.maps.LatLng(community.CommunityLatitude, community.CommunityLongitude);
            var title = community.Name;
            var that = this;
            var newmarker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: "/images/icon-house.png",
                title: (community.MapPinStatus != "DesignCenter") ? title : community.CommunityName,
                content: that.makeFlyout(community),
                animation: google.maps.Animation.DROP
                // zIndex: Math.round(latlng.lat() * -100000) << 5
            });

            google.maps.event.addListener(newmarker, "click", function() {
                if (infowindow)
                    infowindow.close();
                infowindow = new google.maps.InfoWindow(
                {
                    content: newmarker.content
                });
                infowindow.open(map, newmarker);
            });

            TM.SearchMap._markers.push(newmarker);


        };

        tmmap.makeFlyout = function (community) {
            var content = "<div id=\"content\"><strong>" +
                            community.CommunityName+"</strong><br/>"+
                            community.StreetAddress1 + "<br/>" +
                            community.City + ", " + community.StateProvinceAbbreviation + " " + community.ZipPostalCode + "<br/></div>" ;
            content += "<br/><br/><br/><div><a onclick=\"selectCommunityData(\'" + community.CommunityID + "\')\" href='javascript:void(0)'>Get Directions</a></div>";

            return content;
        };

        tmmap.execute();
    }

    function selectCommunityData(itemID) {
        console.log(itemID);
        $j('#hiddenItemId').val(itemID);
        $j('#btnGetDirections').click();
    }
</script>
<style>
.mapWindow {
height: 432px;
width: 100%;
margin: 0;
border: 1px solid #dcdcdc;
}

.srchmap {
float: left;
margin: 0;
padding: 0;
width: 99.9%;
position: relative;
}
</style>
<asp:Button runat="server" ID="btnGetDirections" ClientIDMode="Static" onclick="btnGetDirections_Click" style="display: none"/>
<input type="hidden" id="hiddenItemId" runat="server" ClientIDMode="Static" style="display: none"/>