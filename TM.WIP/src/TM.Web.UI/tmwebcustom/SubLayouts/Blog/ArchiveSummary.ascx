﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchiveSummary.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.ArchiveSummary" %>

<p class="rhs-2-title">Archive</p>
<asp:Repeater ID="rptYears" runat="server" 
        onitemdatabound="rptYears_ItemDataBound" >
     <ItemTemplate>
        <ul class="rhs-2">
        <li><%# Container.DataItem %></li>
            <asp:Repeater ID="rptMonthlyEntries" runat="server">
                <ItemTemplate>
                    <li>
                        <asp:HyperLink ID="hlkMonthlyEntries" runat="server" NavigateUrl='<%#Eval("Url") %>'><%#Eval("Month") %></asp:HyperLink>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
     </ItemTemplate>
</asp:Repeater>




