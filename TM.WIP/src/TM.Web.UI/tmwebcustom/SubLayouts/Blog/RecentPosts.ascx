﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentPosts.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Blog.RecentPosts" %>

<ul class="rhs">
    <li>Recent Posts</li>
        <asp:Repeater ID="rptRecentPosts" runat="server" 
        onitemdatabound="rptRecentPosts_ItemDataBound">
            <ItemTemplate>
  
                <li><asp:HyperLink ID="postLink" runat="server"></asp:HyperLink></li>

<%--            This functionality has been disabled.
                <li class="rhs-c"> Comments: 0</li>
                <li class="rhs-c"> Not rated yet</li>--%>

            </ItemTemplate>
        </asp:Repeater>
</ul>