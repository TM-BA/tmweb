﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashPage.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.SplashPage" %>

<%#Sitecore.Context.PageMode.IsPageEditor? "<br/><div style=\"border:1px solid gray\">" : string.Empty %>
<sc:fieldrenderer runat="server" id="Html" fieldname="Html"></sc:fieldrenderer>
<%# Sitecore.Context.PageMode.IsPageEditor? "</div>" : string.Empty %>

<%#Sitecore.Context.PageMode.IsPageEditor? "<br/><div style=\"border:1px solid gray\">" : string.Empty %>
<sc:fieldrenderer runat="server" id="Meta" fieldname="Meta"></sc:fieldrenderer>
<%# Sitecore.Context.PageMode.IsPageEditor? "</div>" : string.Empty %>

<body>
<%#Sitecore.Context.PageMode.IsPageEditor? "<br/><div style=\"border:1px solid gray\">" : string.Empty %>
<sc:text runat="server" id="Body" field="Body"></sc:text>
<%# Sitecore.Context.PageMode.IsPageEditor? "</div>" : string.Empty %>


 <div id="form">	
  <form method="post" runat="server" id="mainform">
     <%#Sitecore.Context.PageMode.IsPageEditor? "<br/>": string.Empty %>
        <sc:Placeholder ID="phMain" runat="server" Key="Form" />
  </form>
  </div>
  </body>
</html>