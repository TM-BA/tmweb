﻿<%@ Control Language="c#" AutoEventWireup="True" CodeBehind="ContentAreaTwoCollumn.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.ContentAreaTwoCollumn" %>
<div class="rightCol">
<sc:Placeholder ID="RightCol" Key="RightCollumn" runat="server" />
<sc:Text ID="RightColContent" runat="server" Field="Collumn 2 Content" />
</div>
<asp:Panel id="pnlContentWrapper" runat="server" class="leftCol"> 
      <sc:fieldrenderer runat="server" id="StyleBlock" fieldname="Style Block"></sc:fieldrenderer>
      <sc:fieldrenderer runat="server" id="MainContentArea" fieldname="Main Content"></sc:fieldrenderer>
</asp:Panel>
<style>
.rightCol{width:200px;float:right;}
.leftCol{width:448px;float:left;}
</style>
<sc:Image Field="Bottom Image" runat="server" MaxWidth="668" />
