﻿<%@ Control Language="c#" AutoEventWireup="True" CodeBehind="Accordion.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Accordion" %>
<div id="horAcordNav">
    <asp:Literal ID="navBar" runat="server"></asp:Literal>
    <div id="navTabs">
        <div id="tabs-1">
            <span class="mobileIndicator">
                <sc:Text ID="Text7" Field="Section 1 Title" runat="server" />
                <span class="arrow"></span>
            </span>
            <sc:Text ID="Text1" Field="Section 1 Content" runat="server" />    
        </div>
        <div id="tabs-2">
            <span class="mobileIndicator">
                <sc:Text ID="Text8" Field="Section 2 Title" runat="server" />
                <span class="arrow"></span></span>
            <sc:Text ID="Text2" Field="Section 2 Content" runat="server" />
        </div>
        <div id="tabs-3">
            <span class="mobileIndicator">
                <sc:Text ID="Text9" Field="Section 3 Title" runat="server" />
                <span class="arrow"></span></span>
            <sc:Text ID="Text3" Field="Section 3 Content" runat="server" />
        </div>
        <div id="tabs-4">
            <span class="mobileIndicator">
                <sc:Text ID="Text10" Field="Section 4 Title" runat="server" />
                <span class="arrow"></span></span>
            <sc:Text ID="Text4" Field="Section 4 Content" runat="server" />
        </div>
        <div id="tabs-5">
            <span class="mobileIndicator">
                <sc:Text ID="Text11" Field="Section 5 Title" runat="server" />
                <span class="arrow"></span></span>
            <sc:Text ID="Text5" Field="Section 5 Content" runat="server" />
        </div>
        <div id="tabs-6">
            <span class="mobileIndicator">
                <sc:Text ID="Text12" Field="Section 6 Title" runat="server" />
                <span class="arrow"></span></span>
            <sc:Text ID="Text6" Field="Section 6 Content" runat="server" />
        </div>
    </div>
</div>
<script type="text/javascript">
    $j(document).ready(function () {
        $j(".child").click(function () {
            
            if ($j(this).hasClass("active")) {
                $j(this).removeClass("active"); 
                $j($j(this).children("a")[0].hash).hide();
            } else {

                $j('.container .child').each(function (index) {
                    $j(this).removeClass("active");
                    $j($j(this).children("a")[0].hash).hide();
                });
                
                $j(this).addClass("active");
                $j($j(this).children("a")[0].hash).show();
                if (!$j($j(this).children("a")[0].hash).hasClass('accordionContentBlock')) {
                    $j($j(this).children("a")[0].hash).addClass('accordionContentBlock');
                }
            }

            //console.log();
            
            return false;
        });
    });
   
</script>