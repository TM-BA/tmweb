﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MortgageConsultantSearch.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.MortgageConsultantSearch" %>

<div class="rt-col">
<sc:placeholder runat="server" id="ContentArea" key="ContentArea"/>
</div>
    
<div class="req-info">

    <div class="ri-l">
        
        <p>Area of Interest </p>

        <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="True" 
        ontextchanged="ddlArea_TextChanged" CssClass="rip-1" 
            onselectedindexchanged="ddlArea_SelectedIndexChanged">
        </asp:DropDownList>

        <p>Community of Interest </p>

        <asp:DropDownList ID="ddlComunity" runat="server" CssClass="rip-1" 
            onselectedindexchanged="ddlComunity_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>

        <asp:Button ID="btnContactInformation" runat="server" 
            Text="Search" CssClass="send-rbtn" 
            onclick="btnContactInformation_Click" />

         <p><asp:Label ID="hmc" runat="server" Text="" CssClass="no-mc1"> </asp:Label></p>
         <div class="err-Area">
            <asp:Label ID="errorMessageArea" runat="server" Text=""> </asp:Label>
          </div>
          <div class="err-Comm">
           <asp:Label ID="errorMessageCommunity" runat="server" Text=""> </asp:Label>
          </div>

        <asp:Repeater ID="rptContactInformation" runat="server" 
        onitemdatabound="rptContactInformation_ItemDataBound">
        <ItemTemplate>
            <div class="morg-1">
                 <div class="morg-ins">
                 <asp:Label ID="name" runat="server" Text="" Font-Bold="true" CssClass="no-mc1"> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Phone: </p><asp:Label ID="phone" runat="server" Text="" CssClass="pho-red"> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Mobile: </p><asp:Label ID="mobile" runat="server" Text=""> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Fax: </p><asp:Label ID="fax" runat="server" Text=""> </asp:Label>
                </div>
                <div class="morg-ins">
                    <p> Email: </p> <asp:HyperLink ID="email" runat="server"></asp:HyperLink>
                </div> 
            </div>               
        
            
        </ItemTemplate>
        </asp:Repeater>

     <div class="loan-app">
       <asp:HyperLink ID="hlkStarLoanApp" runat="server" target="_blank" Visible="false">Start the pre-qualification process</asp:HyperLink>
     </div>    
    
    </div>
    
</div>