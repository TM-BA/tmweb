﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlossaryItemDisplay.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.GlossaryItemDisplay" %>

<div class="glos-list-wrapper"> 
				
					<ul class="glos-list">
                        <asp:Repeater ID="rptTerms" runat="server">
                            <ItemTemplate>
                                <div>
                                    <li> 
                        	            <span><%# Eval("Term")%></span>
                        	            <span><%# Eval("Definition")%></span>
                                    </li>
                                </div>                 
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
</div>

