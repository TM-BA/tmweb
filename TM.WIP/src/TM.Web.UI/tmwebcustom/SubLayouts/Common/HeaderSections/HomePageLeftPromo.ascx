﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageLeftPromo.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.HeaderSections.HomePageLeftPromo" %>
<section class="hm-lt">
    <h1 class="lttle">
        <sc:Text runat="server" ID="scTxtPromoTitle" Field="Promo Title" />
    </h1>
    <div class="navln"></div>
    <p>
        <sc:Text runat="server" ID="scTxtPromoContent" Field="Content" />
    </p>
</section>
<div class="lihdr">
    <h2 class="ltstle">
        <sc:Text runat="server" ID="sctxtFindYourNewHomeTxt" Field="Find Your New Home Text" />
    </h2>
</div>
<script type="text/javascript">
    $j(document).ready(function () {
        $j('h1.lttle').append("<sup style='vertical-align: super;'>&reg;</sup>");
        $j('h1.lttle:contains("Building Dreams With Pride")').replaceWith("<h1 class='lttle'> Building Dreams With Pride<sup style='vertical-align: super;font-size:16px;'>&trade;</sup></h1>");
    });
</script>
