﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StandardContent.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.StandardContent" %>


<asp:Panel ID="pnl_Navigation" runat="server">
<div class="lt-col">
<sc:placeholder runat="server" id="LeftNavigation" key="LeftNavigation">
    
</sc:placeholder>
</div>


<div class="rt-col">
   
   <div class="content-pg"> 
    <sc:placeholder runat="server" id="ContentArea" key="ContentArea"></sc:placeholder>
   </div>

</div>
</asp:Panel>

<asp:Panel ID="pnl_ContentArea" runat="server">
    <sc:placeholder runat="server" id="ContentArea2" key="ContentArea2"></sc:placeholder>
</asp:Panel>