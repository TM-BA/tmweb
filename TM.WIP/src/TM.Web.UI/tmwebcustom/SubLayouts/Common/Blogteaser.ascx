﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Blogteaser.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Common.Blogteaser" %>
<article id="news">
<asp:Repeater runat="server" ID="rptBlog">
    <HeaderTemplate>
         <div class="hmnews">RECENT BLOG POSTS</div>
    </HeaderTemplate>
    <ItemTemplate>
        <div class="newsbx">
					<div class="clbl">
							<div><%# Eval("Month") %></div>
							<div><%# Eval("Date") %></div>
					</div>
					
                    <div class="ncont">
                        <a href=" <%# Eval("Link") %>" onclick="TM.Common.GAlogevent('Blog','Click','BlogLink')"><%# Eval("Title") %></a>
						<p>
							<%# Eval("Description") %>
						</p>
						<a href="<%# Eval("Link") %>" onclick="TM.Common.GAlogevent('Blog','Click','BlogLink')">READ MORE</a>								
					</div>
	   </div>
    </ItemTemplate>
</asp:Repeater>	

       

</article>