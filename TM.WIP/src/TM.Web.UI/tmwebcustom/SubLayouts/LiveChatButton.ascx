﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LiveChatButton.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.LiveChatButton" %>
<a id="chat-Button" href="<%= RFILocation %>" onclick="TM.Common.GAlogevent('IHC', 'Click', 'StartLiveChat-IHCOffline');">Request Information</a>
<asp:Panel ID="pnLiveChat" runat="server">
	<!-- BEGIN LivePerson Button Code -->
    <!-- Code Changed on 9/22/2014  -71065  -->
    <div id='lpChat' > </div>
	<!-- END LivePerson Button code -->
	<!-- BEGIN Custom LivePerson Button code -->
</asp:Panel>
<script>
    function heightTest(image) { //offline
        if ($j(image).height() < 10) {
            $j("#<%=pnLiveChat.ClientID %>").hide();
		} else {
		    var $liveChatButton = $j("#_lpChatBtn");
		    var href = $liveChatButton.attr("href");
		    var target = $liveChatButton.attr("target");
		    $j("#<%=pnLiveChat.ClientID %>").hide();

			$j("#chat-Button").attr({ "href": href, "target": target }).text("Chat Now");
        }
    }
</script>
