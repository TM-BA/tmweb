﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Homebuying101Glossary.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Homebuying101.Homebuying101Glossary" %>
<div class="banner101">  
    
    <% if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.TaylorMorrison)
       { %>

         <a class="banner101-1" href="/home-buying/home-buying-101"></a>
         <a class="banner101-2" href="/home-buying/home-buying-201"></a> 

    <% }
       else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.MonarchGroup)
       {  %>

         <a class="banner101-1" href="/home-buying"></a> 

    <% }
       else if (SCCurrentDeviceType == TM.Domain.Enums.DeviceType.DarlingHomes)
       { %>

         <a class="banner101-1" href="/home-buying/home-buying-101"></a>
         <a class="banner101-2" href="/home-buying/home-buying-201"></a> 

    <% } %>
               
                  
</div>
<sc:placeholder runat="server" id="GlossaryListing" key="GlossaryListing">
    
</sc:placeholder>
<sc:placeholder runat="server" id="GlossaryItemDisplay" key="GlossaryItemDisplay">
    
</sc:placeholder>
