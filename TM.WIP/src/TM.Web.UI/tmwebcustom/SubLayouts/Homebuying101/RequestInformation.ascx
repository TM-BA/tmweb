﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestInformation.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Homebuying101.RequestInformation" %>

<div class="checklist">
    <asp:HyperLink ID="AssociatedDocument" runat="server" Target="_blank"></asp:HyperLink>
</div>
<div class="req-info">
    <sc:placeholder runat="server" id="ContentArea" key="ContentArea">
    
     </sc:placeholder>
     <div class="req-info-form-hb">
       
     <sc:placeholder runat="server" id="RequestInfoForm" key="RequestInfoForm">
     </sc:placeholder>
                
    </div><!--end of req-info-form-->
</div><!--end of req-info-->
                
