﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlanHomeForSaleQuickNavigation.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Plan.PlanHomeForSaleQuickNavigation" %>
<div class="plan-middle2" id="divQuickNav" runat="server">
    <hr/>			
						<div class="fl-plan-n" >
							<p>
							<%=IsCurrentItemAPlan?"Floor Plan ":"Home " %> <span><%=DisplayCurrentIndex %> of <%=TotalNumberOfPlans %> </span><a onclick="TM.Common.GAlogevent('Details', 'Click', 'OtherPlansScroller');" href="<%=NextLink %>" class="rt-arr"></a>
								<a onclick="TM.Common.GAlogevent('Details', 'Click', 'OtherPlansScroller');" href="<%=PrevLink %>" class="lt-arr">
								</a>
							</p>
						</div>
						<div class="p-image" >
						     <span  style="top:41px;left:186px;position:relative" class="st_pinterest_large" displayText="Pinterest"></span>
						      <img src="<%=ImageURL%>" width="236" height="133">
						</div>
					</div>
                    <!--end plan-middle2-->
