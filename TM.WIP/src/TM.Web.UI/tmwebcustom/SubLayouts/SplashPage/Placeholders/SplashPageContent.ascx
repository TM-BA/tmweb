﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashPageContent.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.SplashPage.Placeholders.SplashPageContent" %>
<div><sc:Text ID="txtContent" runat="server" Field="Content"/></div>
<asp:Literal runat="server" ID="litMessage" Visible="False">There is no datasource for this Content Block</asp:Literal>