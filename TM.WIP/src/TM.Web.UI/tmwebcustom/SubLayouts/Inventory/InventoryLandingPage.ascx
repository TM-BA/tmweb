﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InventoryLandingPage.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Inventory.InventoryLandingPage" %>
<%@ Register TagPrefix="uc" TagName="SearchFacet" Src="~/tmwebcustom/SubLayouts/RealtorSearch/HomeForSaleSearchFacet.ascx" %>
<%@ Register TagPrefix="uc" TagName="LiveChat" Src="~/tmwebcustom/SubLayouts/LiveChatButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="HomeForSaleSearchCard" Src="~/tmwebcustom/SubLayouts/RealtorSearch/HomeForSaleSearchCard.ascx" %>
<script type="text/javascript">
    $j(document).ready(function () {
        var searchpath = "<asp:Literal ID='liturl' runat='server' />";
        $j(window).load(function () { GetInventorySearchResults(searchpath, 1, false, false); });

        $j("#searchsortby").change(function () {
            GetInventorySearchResults(searchpath, $j("#searchsortby").val(), false, false);
        });

        $j("a#msignupleadform").click(function () {
            $j('.UpdatesBoxModel').toggle();
        });
    });
</script>
<style>
    .searchfact {
        display: block;
    }

    .mapbut {
        margin-left: 200px;
    }

    .ihc-info {
        float: right;
    }
</style>
<div class="lt-col">
    <div class="blue-bar">
        <a href="#">Home for Sale Search</a>
    </div>
    <input type="hidden" id="hndsearchPath" value="<%= liturl.Text%>" />
    <input type="hidden" id="hndRealtor" value="false" />
    <uc:SearchFacet ID="ucSearchFact" runat="server" />
    <div class="hfsbb">
        <div class="navln">
        </div>
        <asp:Literal ID="litdiviurl" runat="server"></asp:Literal>
    </div>
    <div class="clearfix">
    </div>
</div>
<div class="rt-col">
    <div id="campaigndtl" class="ppc-bnr">
        <asp:Literal runat="server" ID="litLandingPageCampaign"></asp:Literal>
    </div>
    <div class="ppc-bottom">
        <div class="ihc">
            <ul class="mapbut">
                <%-- <li><a id="msignupleadform">Sign Up For Updates </a>
                    <div class="UpdatesBoxModel">
                        <p>
                            Sign Up for Updates,<br>
                            Special Deals & Invites
                        </p>
                        <sc:Placeholder runat="server" ID="phInventoryLeadForm" Key="InventoryLeadForm">
                        </sc:Placeholder>
                    </div>
                </li>--%>
                <li id="liveChat" class="chat-btn">
                    <uc:LiveChat runat="server" id="LiveChat">
                    </uc:LiveChat>
                </li>
            </ul>
            <div id="ihccard" class="ihc-info">
                <asp:Literal runat="server" ID="litihccard"></asp:Literal>
            </div>
        </div>
        <uc:HomeForSaleSearchCard id="ucHomeForSaleSearchCard" runat="server" />
    </div>
</div>
