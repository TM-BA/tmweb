﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityOpen.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityOpen" %>
    
<div class="lt-col-cd">
    <section class="hm-lt_sr">
        <div class="go-logo" runat="server" id="logo" visible="false">
            <sc:Image ID="Image1" Field="Community Logo" runat="server" />
        </div>
        <h1 class="leftTitle_CD" runat="server" id="communityTitle" visible="false">
            <sc:Text ID="scName" runat="server" Field="Community Name" />
        </h1>
        <div class="navln">
        </div>
        <p class="leftsubTitle2_CD">
            <asp:Literal ID="litProductType" runat="server"></asp:Literal>
            <br>
            <%= CommunityStartingPrice %>
        </p>
        <div id="callsToAction" class="callsToAction" runat="server">
            <ul class="ltnavlnk">
                <li class="fav"><a href="#" id="fav_<%=SCContextItem.ID.ToString().Substring(1,36) %>"
                    onclick="toggleCommunityFavorties(this,'<%=SCContextItem.ID %>');TM.Common.GAlogevent('Favorites','Click','AddToFavorites');">
                    <%=ComFavText%></a></li>
                <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                    rel="nofollow">Print</a></li>
            </ul>
            <ul class="ltnavbut">
                <li><a href="<%=CurrentPage.SCVirtualItemUrl%>/make-an-appointment" onclick="TM.Common.GAlogevent('Updates', 'Click', 'LeftRail-SignUpForUpdates')">
                    Make an Appointment </a></li>
                <li id="liveChat" class="chat-btn">
                    <sc:Placeholder ID="Placeholder1" runat="server" Key="LiveChat" />
                </li>
            </ul>
        </div>
    </section>
    <div id="mobileHours">
        <div class="s-center">
            <h2>Sales Center:</h2>
            <p class="address">
                <a onclick="TM.Common.GAlogevent('Directions', 'Click', 'AddressLink');" href="https://www.google.com/maps?q=<%=AddressLink %>">
                    <span class="clearRightText">
                        <%=Address1 %>
                    </span> 
                    <span class="clearRightText">
                        <%=Address2 %>
                    </span> 
                    <span>
                        <%= CityName %>,
                        <%= State %>
                        <%=Zip %>
                    </span>
                </a>
            </p>
            <% if (!Request.Browser.IsMobileDevice) { %>
                <asp:HyperLink CssClass="announ" ID="HyperLink1" runat="server" onclick="TM.Common.GAlogevent('Directions', 'Click', 'DrivingDirections');">Driving Directions</asp:HyperLink>
            <% } %>
        </div>
		<div class="s-associate">
			<h2>Sales Contact:</h2>
			<p>
                <span class="clearRightText ihc-phone"><a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a></span>
                <span class="clearRightText ihc-phone"><a href="tel:<%=IHCPhone%>"><%=IHCPhone%></a></span>
            </p>
		</div>
    </div>
    <div class="announc1" id="areaCommunities" runat="server">
        <asp:HyperLink ID="hypAreaCommunities" runat="server" onclick="TM.Common.GAlogevent('Search', 'Click', 'ViewAllAreas');">View all <%= Division%> Area Communities</asp:HyperLink>
    </div>
</div>
<div class="rt-col">
    <div class="banner">
        <sc:Placeholder ID="scImageRotator" runat="server" Key="Slides" />
    </div>
</div>
<div class="det-wrapper-bt">
    <div class="cd_left">
        <div class="geneInfo">
            <div>
                GENERAL INFORMATION</div>
        </div>
        <div class="geneInfoBox">
            <h2>
                Sales Center:
            </h2>
            <p class="address">
                <a onclick="TM.Common.GAlogevent('Directions', 'Click', 'AddressLink');" href="https://www.google.com/maps?q=<%= AddressLink %>">
                    <span class="clearRightText">
                        <%=Address1 %>
                    </span> 
                    <span class="clearRightText">
                        <%=Address2 %>
                    </span> 
                    <span>
                        <%= CityName %>,
                        <%= State %>
                        <%=Zip %>
                    </span>
                </a>
                <br />
                <span>
                    <a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a>
                </span>
            </p>
            <% if (!Request.Browser.IsMobileDevice) { %>
                <asp:HyperLink CssClass="announ" ID="drivingDirections" runat="server" onclick="TM.Common.GAlogevent('Directions', 'Click', 'DrivingDirections');">Driving Directions</asp:HyperLink>
            <% } %>
            <div class="grayLineS">
            </div>
            <h2>
                Sales Team:
            </h2>
            <asp:Repeater ID="rptContactInfo" runat="server" OnItemDataBound="rptContactInfo_ItemDataBound">
                <ItemTemplate>
                    <p>
                        <asp:Label ID="Name" runat="server" Text='<%#Eval("Name") %>'></asp:Label><br />
                        <asp:Label ID="Phone" runat="server" Text='<%#Eval("Phone") %>'></asp:Label><br />
                        <asp:Label ID="Registration" runat="server" Text='<%#Eval("Registration") %>'></asp:Label>
                    </p>
                </ItemTemplate>
            </asp:Repeater>
            <div class="grayLineS">
            </div>
            <sc:Placeholder ID="leadForm" Key="RFIWidget" runat="server" />
            <sc:Placeholder ID="phSecondaryNavWidgets" Key="secNavWidgets" runat="server" />
        </div>
    </div>
    <!--end of cd_left-->
    <sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />
    <!--end cd_right-->
</div>