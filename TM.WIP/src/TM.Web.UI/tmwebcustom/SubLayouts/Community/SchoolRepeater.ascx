﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SchoolRepeater.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.SchoolRepeater" %>
<script type="text/javascript">
	//Data for home cards
    NS("TM.CommunityScripts");
	<%= SchoolRepeaterData %>
	$j(document).ready(function() {
	    var SchoolsModel = function(Schools) {
	        var self = this;
	        self.Schools = ko.observableArray(ko.utils.arrayMap(Schools,
	            function(school) {
	                return {
	                    img: school.img,
	                    url: school.url,
	                    name: school.name,
	                    phone: school.phone,
	                    grades: school.grades,
	                    prin: school.prin,
	                    dis: school.dis,
	                    nces: school.nces
	                };
	            }
	        ));
	    };
	
	    ko.applyBindings(new SchoolsModel(SchoolInitialData), document.getElementById('SchoolRepeater'));
	});
</script>
<asp:Literal ID="litPrintHeader" runat="server"></asp:Literal>

<div class="schoolsInfo">
    <div class="districtDesc" style="padding-top: 3em;">
        <sc:Text field="Schools Description" runat="server" />
    </div>
    <div id="SchoolRepeater" data-bind="foreach: Schools">
    <div class="schoolBlock">
    <div class="schoolImg">
    <img data-bind="attr:{src:img}" width="172" height="115"> 
    </div>

    <div class="schoolText">
    <span data-bind="if: url">
    <a class="sc" data-bind="text:name, attr:{href:url}" target="_blank" ></a>
    </span>
    <span data-bind="if: !url">
    <a class="sc" data-bind="text:name"></a>
    </span> <br>
    <span data-bind="text:phone"></span> <br>
    Grades: <span data-bind="text:grades"></span> <br>
    Principal: <span data-bind="text:prin"></span> <br>
    Distance from community: <span data-bind="text:dis"></span> <br>
    <a target="_blank" data-bind="attr:{href:'<%= SchoolReportUrl %>' + nces}, style: { display: (nces!='' ? 'inline' : 'none')}" class="sc">NCES Report</a></div>
    </div>
    </div>
</div>

<script>
    $j(document).ready(function () {
        $j(".schoolsInfo a").click(function () { TM.Common.GAlogevent('Details', 'Click', 'SchoolURL') });
    });
</script>