﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocalInterest.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.LocalInterest" %>
<%@ Import Namespace="TM.Utils.Extensions" %>
<sc:Placeholder ID="header" Key="CompressedCommunityHeader" runat="server" />
<div class="dd-bottom">
    <div class="local-interest-out" id="LocalInterestHeader" runat="server">
        <ul class="loc-int">
            <li>
                <asp:LinkButton ID="lBtnShowList" runat="server" CssClass="loc-int on">LOCAL INTERESTS</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lBtnShowMap" runat="server" CssClass="loc-int">AREA MAP</asp:LinkButton></li>
        </ul>
    </div>
    <asp:Panel ID="pnlMap" runat="server">
        <span class="map-more">Select from the following categories to see what’s near
            <%=CommunityName%>
            or <a href="#" onclick="turnOn(this); showAll()">see all categories.</a></span>
        <ul class="icon-local" id="ulCategories" runat="server">
        </ul>
        <div class="loc-int-map">
            <div id="map" class="localInterstMap">
            </div>
        </div>
        <div class="box-local">
            <div class="list-local">
                <div id="searchwell">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="local-interest">
            <h1>
                Local Interests</h1>
            <asp:Literal ID="litContent" runat="server"></asp:Literal>
        </div>
        <div class="home-feat">
            <asp:Literal ID="litImages" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <div id="lastUpdated" class="clear">
        Last Updated:
        <%= LastUpdate.ToShortDateString() %>
    </div>
</div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=<%=GoogleMapsApiKey %>"></script>
<script src="/javascript/search/searchMap.js" type="text/javascript"></script>
<script src="/javascript/search/search.facet.js" type="text/javascript"></script>
<script type="text/javascript">
    //<![CDATA[

        var gSelectedResults = [];
        var gCurrentResults = [];
        var gSearchForm;

        var latlngbounds;
        var map;
        var service;
        var infowindow;
        var communityLL = new google.maps.LatLng(<%=CommunityLattitude %>, <%=CommunityLongitude %>);
        var maxDistance = <%=scMaxDistance %> ;

        <%=CategoryJson %>

        var gOffMarker;
        var gOnMarker;
        var gSmallShadow = new google.maps.MarkerImage("http://labs.google.com/ridefinder/images/mm_20_shadow.png",new google.maps.Size(22, 20),new google.maps.Point(0, 0),new google.maps.Point(6, 20));

        var mycommunity = <%=myCommunity %>;
        var searchpath = "<asp:Literal ID='liturl' runat='server' />";
        var comFav = "<asp:Literal ID='litcomFav' runat='server' />";
        GetCommunity(searchpath, 1, false, comFav, false, false, mycommunity);
        
    //]]>
</script>
<script src="/javascript/community/localInterest.js" type="text/javascript"></script>