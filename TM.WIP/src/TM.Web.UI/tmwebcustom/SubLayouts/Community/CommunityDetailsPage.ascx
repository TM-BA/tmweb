﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityDetailsPage.ascx.cs"
    Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.CommunityDetailsPage" %>
    <asp:Literal ID="litHeader" runat="server"></asp:Literal>
<sc:Placeholder runat="server" ID="phFullCommunity" />

<script>

    var userIsLoggedIn = <%=UserIsLoggedIn %>;
    function toggleCommunityFavorties(sender, comid) {
        if(userIsLoggedIn) {
	        //var comidval = comid.replace("{", "").replace("}", "");
            if ($j(sender).text().indexOf("Add to Favorites") < 0)
            {
                $j(sender).text("Add to Favorites");
                AddRemoveFavorites(comid, 'Community', 'Remove');
	            //$j("a#fav_" + comidval).removeClass("selected");
            } 
            else {
                TM.Common.GAlogevent('Favorites', 'Click', 'AddToFavorites');
                $j(sender).text("Remove from Favorites");
                AddRemoveFavorites(comid, 'Community', 'Add');
	            //$j("a#fav_" + comidval).addClass("selected");
            }
        }
        else 
        {
            //TM.Common.ShowLoginModal(document.location.href);
	        TM.Common.ShowLoginModal("?id=" + comid + "&type=Community&action=Add");
        }
    }
</script>