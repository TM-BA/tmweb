﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HSH.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Community.HSH" %>
<div class="left-container" runat="server" id="leftSide" visible="false">
    <div class="lt-col-cd">
    <section class="hm-lt_sr">
        <div class="go-logo" runat="server" id="logo" visible="false">
            <sc:Image ID="Image1" Field="Community Logo" runat="server" />
        </div>
        <h2 class="leftTitle_CD" runat="server" id="communityTitle">
            <sc:Text ID="scName" runat="server" Field="Community Name" />
        </h2>
        <div class="navln">
        </div>
        <p class="leftsubTitle2_CD">
            <asp:Literal ID="litProductType" runat="server"></asp:Literal>
            <br>
            <%= CommunityStartingPrice %>
        </p>
        <div id="callsToAction" class="callsToAction" runat="server">
            <ul class="ltnavlnk">
                <li class="fav"><a href="#" id="fav_<%=SCContextItem.ID.ToString().Substring(1,36) %>"
                    onclick="toggleCommunityFavorties(this,'<%=SCContextItem.ID %>');TM.Common.GAlogevent('Favorites','Click','AddToFavorites');">
                    <%=ComFavText%></a></li>
                <li class="print"><a target="_blank" href="<%= Request.Url.GetLeftPart(UriPartial.Path) %>?sc_device=print"
                    rel="nofollow">Print</a></li>
            </ul>
            <ul class="ltnavbut">
                <li><a href="<%=CurrentPage.SCVirtualItemUrl%>/make-an-appointment" onclick="TM.Common.GAlogevent('Updates', 'Click', 'LeftRail-SignUpForUpdates')">
                    Make an Appointment </a></li>
                <li id="liveChat" class="chat-btn">
                    <sc:Placeholder ID="Placeholder1" runat="server" Key="LiveChat" />
                </li>
            </ul>
        </div>
    </section>
    <div id="mobileHours">
        <div class="s-center">
            <h2>Sales Center:</h2>
            <p class="address">
                <a onclick="TM.Common.GAlogevent('Directions', 'Click', 'AddressLink');" href="https://www.google.com/maps?q=<%=AddressLink %>">
                    <span class="clearRightText">
                        <%=Address1 %>
                    </span> 
                    <span class="clearRightText">
                        <%=Address2 %>
                    </span> 
                    <span>
                        <%= CityName %>,
                        <%= State %>
                        <%=Zip %>
                    </span>
                </a>
            </p>
            <% if (!Request.Browser.IsMobileDevice) { %>
                <asp:HyperLink CssClass="announ" ID="HyperLink1" runat="server" onclick="TM.Common.GAlogevent('Directions', 'Click', 'DrivingDirections');">Driving Directions</asp:HyperLink>
            <% } %>
        </div>
		<div class="s-associate">
			<h2>Sales Contact:</h2>
			<p>
                <span class="clearRightText ihc-phone"><a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a></span>
                <span class="clearRightText ihc-phone"><a href="tel:<%=IHCPhone%>"><%=IHCPhone%></a></span>
            </p>
		</div>
    </div>
    <div class="announc1" id="areaCommunities" runat="server">
        <asp:HyperLink ID="hypAreaCommunities" runat="server" onclick="TM.Common.GAlogevent('Search', 'Click', 'ViewAllAreas');">View all <%= Division%> Area Communities</asp:HyperLink>
    </div>
</div>
    <div class="cd_left">
        <div class="geneInfo">
            <div>
                GENERAL INFORMATION</div>
        </div>
        <div class="geneInfoBox">
            <h2>
                Sales Center:
            </h2>
            <p class="address">
                <a onclick="TM.Common.GAlogevent('Directions', 'Click', 'AddressLink');" href="https://www.google.com/maps?q=<%= AddressLink %>">
                    <span class="clearRightText">
                        <%=Address1 %>
                    </span> 
                    <span class="clearRightText">
                        <%=Address2 %>
                    </span> 
                    <span>
                        <%= CityName %>,
                        <%= State %>
                        <%=Zip %>
                    </span>
                </a>
                <br />
                <span>
                    <a href="tel:<%=LocationPhone%>"><%=LocationPhone%></a>
                </span>
            </p>
            <% if (!Request.Browser.IsMobileDevice) { %>
                <asp:HyperLink CssClass="announ" ID="drivingDirections" runat="server" onclick="TM.Common.GAlogevent('Directions', 'Click', 'DrivingDirections');">Driving Directions</asp:HyperLink>
            <% } %>
            <div class="grayLineS">
            </div>
            <h2>
                Sales Team:
            </h2>
            <asp:Repeater ID="rptContactInfo" runat="server" OnItemDataBound="rptContactInfo_ItemDataBound">
                <ItemTemplate>
                    <p>
                        <asp:Label ID="Name" runat="server" Text='<%#Eval("Name") %>'></asp:Label><br />
                        <asp:Label ID="Phone" runat="server" Text='<%#Eval("Phone") %>'></asp:Label><br />
                        <asp:Label ID="Registration" runat="server" Text='<%#Eval("Registration") %>'></asp:Label>
                    </p>
                </ItemTemplate>
            </asp:Repeater>
            <div class="grayLineS">
            </div>
            <sc:Placeholder ID="leadForm" Key="RFIWidget" runat="server" />
            <sc:Placeholder ID="phSecondaryNavWidgets" Key="secNavWidgets" runat="server" />
        </div>
    </div>
</div>

<div class="rt-col hsh-page <%= IsCommunityLevel ? "community-hsh" : "home-hsh" %>">
    <h2>Step 1: <span>Select a Hot Sheet Helper to Print</span></h2>
    <%= templateSection %>
    <div class="step-2 step-section">
        <h2>Step 2: <span>Select Contacts to Appear on Hot Sheet</span></h2>
        <ul class="template-selection sales-team-ul">
            <p class="allowed-data"></p>
            <asp:Repeater id="salesteam_repeater" runat="server">
                <ItemTemplate>
                    <li class="template-item choose-sales">
                        <asp:CheckBox id="salesCheckbox" AutoPostBack="False" Text="" Checked="False" runat="server"/>
                        <div class="template-item sales-team-members">
                            <i></i>
                            <h2><%#Eval("Name") %></h2>
                            <asp:TextBox ID="name" runat="server" Text='<%#Eval("Name") %>' placeholder="Name" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="title" runat="server" Text="" placeholder="Title" CssClass="title-input"></asp:TextBox>
                            <label class="error-label title-input">Please add a title</label>
                            <asp:TextBox ID="email" runat="server" Text="" placeholder="Email Address" CssClass="email-field"></asp:TextBox>
                            <label class="error-label">Please add a valid email</label>
                            <asp:TextBox ID="phone" runat="server" CssClass="phone-field" Text='<%#Eval("Phone") %>' placeholder="Phone"></asp:TextBox>
                            <label class="error-label">Please add a phone number</label>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            <li class="template-item choose-sales last-member">
                <asp:CheckBox id="ExtraMember" AutoPostBack="False" Text="" Checked="False" runat="server"/>
                <div class="template-item sales-team-members">
                        <i></i>
                        <asp:TextBox ID="extra_name" runat="server" Text="" placeholder="Name"></asp:TextBox>
                        <label class="error-label">Please add a name</label>
                        <asp:TextBox ID="extra_title" runat="server" Text="" placeholder="Title" CssClass="title-input"></asp:TextBox>
                        <label class="error-label title-input">Please add a title</label>
                        <asp:TextBox ID="extra_email" runat="server" Text="" placeholder="Email" CssClass="email-field"></asp:TextBox>
                        <label class="error-label">Please add a valid email</label>
                        <asp:TextBox ID="extra_phone" runat="server" Text="" placeholder="Phone"  CssClass="phone-field"></asp:TextBox>
                        <label class="error-label">Please add a phone</label>
                    </div>
            </li>
        </ul>
    </div>

    <div class="new-step-3 hsh-page">
        <h2>Step 3: <span>Add Content</span></h2>
        <textarea runat="server" ID="custom_text"></textarea>
    </div>
    <div class="new-step-4 step-section hsh-page-community">
        <h2>Step 4: <span>You're ready to go!</span></h2>
        <div class="create-hotsheet">
            <asp:Button ID="Button1" runat="server" Text="CREATE HOT SHEET" OnClick="CreateHotSheet_Click" />
            <span class="arrow"></span>
        </div>
        
    </div>

    <div class="step-3 step-section hsh-page-community">
        <h2>Step 3: <span>You're ready to go!</span></h2>
        <div class="create-hotsheet">
            <asp:Button ID="CreateHotSheet" runat="server" Text="CREATE HOT SHEET" OnClick="CreateHotSheet_Click" />
            <span class="arrow"></span>
        </div>
        
    </div>
</div>
<div class="det-wrapper-bt">
    
    <!--end of cd_left-->
    <%--<sc:Placeholder ID="phSecondaryNav" runat="server" Key="secNav" />--%>
    <!--end cd_right-->
</div>

<script type="text/javascript">
    $j(window).load(function () {
        $j('.choose-template').on('click', function (e) {
            var target = $j(this);
            if (target.find('input').prop('checked')) {
                return;
            }
            target.find('input').prop('checked', true);
            $j('.allowed-data').html(target.attr('data-allowed') + " allowed on selected hot sheet");
            $j('.allowed-data').attr('data-allowed', target.attr('data-allowed'));

            if (target.attr('data-ihc')) {
                $j('.step-3').slideUp(500, function () {
                    $j('.new-step-3,.new-step-4,.step-2').slideDown(500);
                });
            } else {
                $j('.new-step-3,.new-step-4').slideUp(500, function () {
                    $j('.step-3,.step-2').slideDown(500);
                });
            }
            
            $j('.sales-team-ul input[type="checkbox"]').prop('checked', false);
            $j('.choose-sales').each(function (i, e) {
                $j(e).attr('data-right', false);
            });
            HideMessages();
            //CheckIfReadyToStep3();
      
        });
        $j('.choose-sales').on('click', function (e) {
            if ($j(e.target).context.tagName == 'INPUT') {
                return;
            }
            var target = $j(this);
            var quantity = $j('.allowed-data').attr('data-allowed');
            var amount = 0;
            var e = target.find('input').prop('checked');
            if (e) {
                target.find('input').prop('checked', false);
                HideMessages(target);
            } else {
                $j('.sales-team-ul input[type="checkbox"]').each(function (i,el) {
                    if ($j(el).prop('checked')) {
                        amount++;
                    }
                });

                if (amount < quantity) {
                    target.find('input').prop('checked', "checked");
                    DisplayMessages(target);
                }
            }

            //CheckIfReadyToStep3();
        });

        $j('.sales-team-members input').on('input propertychange', function (e) {
            var right = true;
            var $input = $j(e.target);
            /*if ($input.val() == '') {
                right = false;
                $input.next().show();
            } else {
                if ($input.hasClass('email-field')) {
                    if (!isValidEmailAddress($input.val())) {
                        $input.next().show();
                        right = false;
                    } else {
                        $input.next().hide();
                    }
                } else {
                    $input.next().hide();
                }
            }*/

            if ($input.parents(':eq(1)').find('> input').prop('checked')) {
                DisplayMessages($input.parents(':eq(1)'));
                //$input.parents(':eq(1)').attr('data-right', right);
            }
            //CheckIfReadyToStep3();
        });

        function HideMessages(target) {
            var $el = $j(target);

            $el.find('.sales-team-members input').each(function (i, e) {
                $j(e).next().hide();
                $el.attr('data-right', false);
            });
        }

        function DisplayMessages(target) {
            var $el = $j(target);
            var right = true;
            $el.find('.sales-team-members input').each(function (i, e) {
                var $input = $j(e);
                if (!e.value) {
                    $j(e).next().show();
                    right = false;
                } else {
                    if ($input.hasClass('email-field')) {
                        if (!isValidEmailAddress($input.val())) {
                            $input.next().show();
                            right = false;
                        } else {
                            $input.next().hide();
                        }
                    } else {
                        $input.next().hide();
                    }
                }
            });
            $el.attr('data-right', right);
        }
        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        };

        function CheckIfReadyToStep3() {
            var allowed = $j('.allowed-data').attr('data-allowed');
            var rightPicked = 0;
            if (allowed != 0) {
                $j('.choose-sales').each(function (i, e) {
                    if ($j(e).attr('data-right') == "true") {
                        rightPicked++;
                    }
                });
            } else {
                rightPicked = allowed; 
            }
            
            
            if (rightPicked == allowed) {
                $j('.step-3').slideDown(500);
            } else {
                $j('.step-3').SlideUp(500);
            }
        }
    });
</script>
