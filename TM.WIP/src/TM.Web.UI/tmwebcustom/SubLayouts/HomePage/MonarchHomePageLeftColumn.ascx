﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MonarchHomePageLeftColumn.ascx.cs"
	Inherits="TM.Web.Custom.Layouts.SubLayouts.HomePage.MonarchHomePageLeftColumn" %>
<div class="lt-col hmbg" id="leftColumn">
	<section>
		<div class="lt-col-sp">
			<p class="leftTitle_M">
				Single Family Homes</p>
			<div class="redLine">
				&nbsp;
			</div>
			<p class="leftText_M">
				Front-porch friendly. Fireplace cozy. Homes that tug your heartstrings</p>
			<br>
			<ul class="SearHomes">
				<asp:repeater runat="server" ID="rptSingleFamilyHomes">
				    <itemtemplate><li><a onclick="TM.Common.GAlogevent('Search', 'Click', '<%#Eval("Value") %>');" href="<%#Eval("Key")%>">Search <%#Eval("Value") %> </a></li></itemtemplate>
				 </asp:repeater>
			</ul>
			<p class="leftTitle_M">
				Condos</p>
			<div class="redLine">
				&nbsp;
			</div>
			<p class="leftText_M">
				Connected. Creative. Culturally vibrant. Homes with an urban edge.</p>
			<ul class="SearHomes">
				<asp:repeater runat="server" ID="rptCondoHomes">
				    <itemtemplate><li><a onclick="TM.Common.GAlogevent('Search', 'Click', '<%#Eval("Value") %>');" href="<%#Eval("Key")%>">Search <%#Eval("Value") %> </a></li></itemtemplate>
				 </asp:repeater>
			</ul>
		</div>
		<ul class="RecBlogBox">
			<li>
				<sc:Link ID="lnkFooterBlog" runat="server" Field="Blog URL">
					Recent Blog Posts</sc:Link></li>
		</ul>
	</section>
</div>
