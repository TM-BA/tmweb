﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignListing.ascx.cs" Inherits="TM.Web.Custom.Layouts.SubLayouts.Campaigns.CampaignListing" %>
 
 <div class="plan-details-wrapper">

        <div class="gr-bk">
            <p><sc:fieldrenderer runat="server" id="Header" fieldname="Header"></sc:fieldrenderer></p>        
            
        </div>

         <asp:Repeater ID="rptCampaignListing" runat="server" 
            onitemdatabound="rptCampaignListing_ItemDataBound">
            <ItemTemplate>
                 <div class="cam-list">
                    <div class="schoolImg">
                        <asp:Image ID="Image" runat="server" width="172" height="115" />
                    </div>

                    <div class="cam-text">
                        <asp:Literal ID="Headline" runat="server"></asp:Literal>
                        <br>
                       <asp:Label ID="ContentText" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
            </ItemTemplate>
 </asp:Repeater>

       
 </div>
 
 
