﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NoHtmlLayout.aspx.cs" Inherits="TM.Web.Custom.Layouts.NoHtmlLayout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <sc:Placeholder runat="server" ID="phContent" Key="content"/>
</html>
