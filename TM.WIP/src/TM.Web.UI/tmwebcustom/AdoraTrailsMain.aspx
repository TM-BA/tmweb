﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdoraTrailsMain.aspx.cs" Inherits="TM.Web.Custom.Layouts.AdoraTrailsMain" %>
<%@ Register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="robots" content="all" />
    <meta http-equiv="Cache-Control" content="public" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="en-US" />
    <link href="/~/media/microsites/adoratrails/images/favicon.ico" rel="favicon" />
    <link href="/~/media/microsites/adoratrails/scripts/css/thickbox.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/~/media/microsites/adoratrails/scripts/css/standard.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/~/media/microsites/adoratrails/scripts/css/main.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/~/media/microsites/adoratrails/scripts/css/images.css" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/jquery142min.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/preloadCssImagesjQueryv5.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/ui.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/ibox.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/thickbox-compressed.js"></script>
    <script type="text/javascript" src="/~/media/microsites/adoratrails/scripts/js/jquerycookie.js"></script>
</head>
<body>
    <div id='site-container'>
        <div id='site-center'>
            <div id='left-gradient'></div>
            <div id='site-content-container'>
                <sc:Placeholder key="header" runat="server" />
                <sc:Placeholder key="mainContent" runat="server" />
                <sc:Placeholder key="footer" runat="server" />
            </div>
            <div id='right-gradient'></div>
        </div>
    </div>
    <sc:Text field="PopUpContent" runat="server" />
</body>
</html>
