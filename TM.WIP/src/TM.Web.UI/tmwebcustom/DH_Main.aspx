﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DH_Main.aspx.cs" Inherits="TM.Web.Custom.Layouts.DH_Main" %>

<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="TM.Utils.Web" %>

<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/TexasSVGMap.ascx" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core"
    TagPrefix="wfm" %>

<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <title>Darling Homes Homepage</title>
    <link rel="stylesheet" type="text/css" href="/Styles/dh_style.css" />
    <link href="/Styles/lib/colorbox.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/Styles/animate-custom.css" />
    <meta name="viewport" content="width=device-width">

    <% if (SCContextItem.TemplateID.ToString() == "{CFF47DD0-6A86-4F25-887D-8F959278A2E0}")
       { %>
    <meta name="msvalidate.01" content="A2936F841880FE1F007F0C5F498236A3" />
    <meta name="google-site-verification" content="8jucB4LVd00yEk-xQtbpRx_lMcxZo7rQmBrzARRR4Qc" />

    <%} %>

    <sc:placeholder id="phHeadSection" runat="server" key="HeadSection" />
    <sc:placeholder id="modalHeadSection" runat="server" key="ModalHead" />
    <!--[if gte IE 9]>
	  <style type="text/css">
		.gradient {
		   filter: none;
		}
	  </style>
	<![endif]-->
    <style>
        /*join interest list*/
        .join-wrapper {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #CECECE;
            display: none;
            height: auto;
            padding: 20px;
            position: absolute;
            width: 335px;
            z-index: 9999;
            left: 610px;
            top: 28px;
        }

        @-moz-document url-prefix() {
            .join-wrapper {
                left: 580px;
            }
        }

        .join-wrapper h1 {
            font-size: 19px;
            line-height: 21px;
            margin: 0 0 12px;
        }

        .scfSingleLineTextBorder {
            margin: 0 0 7px;
        }

        .scfDropList {
            width: 169px;
            background-color: #DFDED8;
            margin: 0 0 7px;
            border: 1px solid #b6b5ac;
        }

        .jil {
            font-size: 10px;
            line-height: 12px;
        }

            .jil a {
                font-size: 10px;
                color: #2da5e0;
                text-decoration: underline;
                line-height: 12px;
            }

        .cl-bt {
            background-color: #acb5bb;
            width: 15px;
            height: 15px;
            float: right;
        }

            .cl-bt a {
                color: #fff;
                text-decoration: none;
                margin: 0 0 0 4px;
            }

        .scfTitleBorder {
            color: #513528 !important;
            padding: 0 0 10px 10px;
        }
    </style>
    <%=JsCssCombiner.GetSet("dhmainjs")%>
    <script type="text/javascript">

        var $j = jQuery.noConflict();
        $j(window).load(function () {
            $j("#leftColumn").css("min-height", $j("#rightColumn").height());
        });

        $j(document).ready(function () {
            var imgZip = $j('#WhyZip img');
            if (imgZip.length > 0) {
                imgZip.attr("src", "/Images/dh/question-dh.gif");
            }

            $j("li#findyournewhome").click(function () {
                $j("li#findyournewhome").last().addClass("selected");
                $j('#svgmap').show();
            });
            $j("a#svgclose").click(function () {
                $j('#svgmap').hide();
                $j("li#findyournewhome").removeClass("selected");
            });

            $j("a#lijoininterest").click(function () {
                $j('#joinyourinterest').toggle();
            });
            $j("a#joininterest").click(function () {
                $j('#joinyourinterest').hide();
            });

            <%= JoinYourInterestFormPosted?@"$j('#joinyourinterest').show();":string.Empty %>

            $j('#joinyourinterest').prepend('<div class="scfTitleBorder">Get Updates, Invites & Special Deals</div>');
        });

        $j(document).ready(function () {
            $j('#siteSearchText').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    sitesearch('#siteSearchText');
                    return false;
                }
            });
        });
    </script>


    <% if (IsMobile()) {
        if (SCContextItem.TemplateID.ToString() == "{CFF47DD0-6A86-4F25-887D-8F959278A2E0}" ||
            SCContextItem.TemplateID.ToString() == "{C70FF0BA-F242-45A3-AE34-1BD9B700B06E}" ||
            SCContextItem.TemplateID.ToString() == "{DE2DA9D0-C765-46B9-820D-28325FA1B227}" ||
            SCContextItem.TemplateID.ToString() == "{2B7CD906-758A-4E29-BAAC-453349506CE1}")
        { %>

    <script src="//use.typekit.net/nqo1ucy.js"></script>
    <script>try { Typekit.load(); } catch (e) { }</script>

    <link rel="stylesheet" href="/~/media/DarlingHomes/Mobile/css/dh_mobile.css?20161125">

    <%}} %>
</head>
<body>
    <sc:placeholder runat="server" id="ISplash" key="Splash" />
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-P47BG2"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-P47BG2');</script>
    <!-- End Google Tag Manager -->
    <%--<a href="<%= PrintFriendlyUrl %>" class="printLink">Oops, this page has not been formated
		for your printer, click here to print that instead.</a>--%>
    <form id="form1" runat="server">
        <header>
            <div class="header">
                <div class="topheader">
                    <ul>
                        <sc:sublayout runat="server" id="subnowtrending" path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx"></sc:sublayout>
                        <li class="top4"><a href="/Account/My-Profile" onclick="TM.Common.GAlogevent('CreateAccount','Click','CreateAccountLink')">
                            <asp:Image runat="server" ID="imgMyCompanyLogo" />
                        </a></li>
                        <li class="top5" id="logintext">
                            <asp:Literal runat="server" ID="litUserAuthentication"></asp:Literal></li>
                        <li class="top3"><a id="lijoininterest" href="javascript:;" onclick="TM.Common.GAlogevent('JoinInterestList','Click','InHeader')">JOIN OUR INTEREST LIST</a>
                            <div id="joinyourinterest" class="join-wrapper">
                                <wfm:formrender runat="server" id="wfmJoinYourInterestList" formid="{E059951C-1C5A-464E-A876-4C06FFD10F26}" />
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="hdline">
            </div>
            <div class="logomenu">
                <div class="lt-box">
                    <a href="/" class="logo">
                        <asp:Image runat="server" ID="imgSiteLogo" /></a>
                    <div class="searchbox">
                        <input type="text" placeholder="Search Site, Enter myTM# or MLS#" id="siteSearchText" /><a
                            href="#" onclick="sitesearch('#siteSearchText')" onclick="TM.Common.GAlogevent('Search','Click','SearchBox')"></a>
                    </div>
                </div>
                <div class="rt-box">
                    <div onclick='$j(".rt-box nav ul").toggle()' class="MobileMenuLink">Menu</div>
                    <nav id="navtop-main">
                        <ul>
                            <li id="findyournewhome"><a href="#"><span>FIND</span><span>YOUR NEW HOME</span></a></li>
                            <uc:svgmap id="svgMap" runat="server" />
                            <sc:sublayout id="Sublayout1" runat="server" path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx" />
                        </ul>
                        <div class="clearfix">
                        </div>
                    </nav>
                </div>
                <div class="breadcrumb">
                    <tm:breadcrumb id="bcBreadcrumb" runat="server" dividertext=">" />
                </div>
            </div>
            <div class="clearfix">
            </div>
        </header>
        <div class="home-content">
            <article>
                <div class="home-content-wrapper">
                    <sc:placeholder runat="server" id="phContent" key="Content" />
                    <div class="clearfix">
                    </div>
                </div>
            </article>
        </div>
        <footer>
            <article>
                <sc:sublayout id="footer" runat="server" path="/tmwebcustom/sublayouts/common/footer.ascx" />
                <sc:placeholder id="phFooter" runat="server" key="FootSection" />
            </article>
        </footer>
    </form>

    <asp:Literal runat="server" ID="yahooPixels"></asp:Literal>

    <sc:visitoridentification runat="server" />

    <% if (IsMobile())
       {
           if (SCContextItem.TemplateID.ToString() == "{CFF47DD0-6A86-4F25-887D-8F959278A2E0}" ||
               SCContextItem.TemplateID.ToString() == "{C70FF0BA-F242-45A3-AE34-1BD9B700B06E}" ||
               SCContextItem.TemplateID.ToString() == "{DE2DA9D0-C765-46B9-820D-28325FA1B227}" ||
               SCContextItem.TemplateID.ToString() == "{2B7CD906-758A-4E29-BAAC-453349506CE1}")

           { %>
    <script type="text/javascript" src="/~/media/DarlingHomes/Mobile/js/dh_mobile.js?20161125"></script>

    <%}} %>
</body>
</html>
