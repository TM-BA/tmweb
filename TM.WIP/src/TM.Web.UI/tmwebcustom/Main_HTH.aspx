﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="TM.Web.Custom.Layouts.Main" %>

<%@ Import Namespace="TM.Utils.Web" %>
<%@ Import Namespace="TM.Web.Custom.Constants.GoogleEventing" %>
<%@ Register Namespace="Sitecore.Form.Core.Renderings" Assembly="Sitecore.Forms.Core"
	TagPrefix="wfm" %>
<%@ Register TagPrefix="uc" TagName="SVGMap" Src="~/tmwebcustom/SubLayouts/Common/USCanadaSVGMap.ascx" %>
<!DOCTYPE html>
<html class="js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths wf-museoslab-n5-active wf-museoslab-i5-active wf-museosans-n3-active wf-museosans-i3-active wf-museosans-n5-active wf-museosans-i5-active wf-museosans-n7-active wf-museosans-i7-active wf-active">
<head runat="server">
	<meta charset="utf-8" />
	<title>Taylor Morrison Homepage</title>
	<link rel="stylesheet" type="text/css" href="/Styles/tm_style.css" media="handheld,screen" />
	<link rel="stylesheet" type="text/css" href="/Styles/tm_hth_style.css" media="handheld,screen" />
	<link rel="stylesheet" type="text/css" href="/Styles/print.css" media="print" />
	<link href="/Styles/lib/colorbox.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/Styles/animate-custom.css" />
	<meta name="viewport" content="width=device-width">
	<sc:Placeholder ID="phHeadSection" runat="server" Key="HeadSection" />
	<!--[if gte IE 9]>
	  <style type="text/css">
  	    .gradient {
  	        filter: none;
  	    }
  	</style>
	<![endif]-->
	<%= JsCssCombiner.GetSet("mainjs") %>
	
	<style>
		/*join interest list*/
		.join-wrapper
		{
			background: none repeat scroll 0 0 #FFFFFF;
			border: 1px solid #CECECE;
			display: none;
			height: auto;
			padding: 20px;
			position: absolute;
			width:335px;
			z-index: 9999;
			top: 29px;
			left: 610px;
		}
		
@-moz-document url-prefix() {       
.join-wrapper {left:583px;}
}
		
		
		.join-wrapper h1
		{
			font-size: 19px;
			color: #d61042;
			line-height: 21px;
			margin: 0 0 12px;
		}
		.scfSingleLineTextBorder
		{
			margin: 0 0 7px;
		}
		.scfDropList
		{
			width: 169px;
			background-color: #DFDED8;
			margin: 0 0 7px;
			border: 1px solid #b6b5ac;
		}
		.jil
		{
			font-size: 10px;
			line-height: 12px;
		}
		.jil a
		{
			font-size: 10px;
			color: #2da5e0;
			text-decoration: underline;
			line-height: 12px;
		}
		.cl-bt
		{
			background-color: #acb5bb;
			width: 15px;
			height: 15px;
			float: right;
		}
		.cl-bt a
		{
			color: #fff;
			text-decoration: none;
			margin: 0 0 0 4px;
		}
	</style>
</head>

<body>
<%--        <a href="<%= PrintFriendlyUrl %>" class="printLink">Oops, this page has not been formated
		for your printer, click here to print that instead.</a> --%>
        <span id="slideImgExpander">
		</span>
	<form id="form1" runat="server">
    
	<header>
		<div class="header">
			<div class="topheader">
				<ul>
					<sc:sublayout runat="server" id="subnowtrending" path="/tmwebcustom/SubLayouts/Common/HeaderSections/NowTrending.ascx">
					</sc:sublayout>

					<li class="top4"><a href="/Account/My-Profile" onclick="TM.Common.GAlogevent('CreateAccount','Click','CreateAccountLink')">
						<asp:image runat="server" id="imgMyCompanyLogo"></asp:image>
					</a></li>
					<li class="top5" id="logintext">
						<asp:literal runat="server" id="litUserAuthentication"></asp:literal></li>
                    <li class="top3"><a id="lijoininterest" href="javascript:;" onclick="TM.Common.GAlogevent('JoinInterestList','Click','InHeader')">JOIN OUR INTEREST LIST</a>
						<div id="joinyourinterest" class="join-wrapper">
							<wfm:formrender runat="server" id="wfmJoinYourInterestList" formid="{E059951C-1C5A-464E-A876-4C06FFD10F26}" xmlns:wfm="http://www.sitecore.net/xhtml"></wfm:formrender>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="hdline">
		</div>
		<div class="logomenu">
			<div class="lt-box">
				<a href="/" class="logo">
					<asp:image runat="server" id="imgSiteLogo"></asp:image></a>
				<div class="searchbox">
					<input type="text" placeholder="Search Site, Enter myTM# or MLS#" id="siteSearchText" /><a href="#" onclick="sitesearch('#siteSearchText')"></a>
				</div>
			</div>
			<div class="rt-box">
				<div onclick="$j(&quot;.rt-box nav ul&quot;).toggle()" class="MobileMenuLink">
					Menu</div>
				<nav id="navtop-main">
					<ul>
						<li id="findyournewhome"><a href="#" onclick="<%=HomePageEvents.InHeaderFindYourNewHomes %>"><span>FIND</span><span>YOUR HOME</span></a></li>
						<uc:svgmap runat="server" id="svgMap" xmlns:uc="http://www.sitecore.net/xhtml"></uc:svgmap>
						<sc:sublayout runat="server" id="Sublayout1" path="/tmwebcustom/SubLayouts/Common/HeaderSections/MainNavigation.ascx"></sc:sublayout>
					</ul>
					<div class="clearfix">
					</div>
				</nav>
			</div>
			<div class="breadcrumb">
				<tm:breadcrumb runat="server" id="bcBreadcrumb" xmlns:tm="http://www.sitecore.net/xhtml"></tm:breadcrumb>
			</div>
			<div class="clearfix">
			</div>
		</div>
		<div class="clearfix">
		</div>
	</header>
	<div class="home-content">
		<article>
			<div class="home-content-wrapper">
				<sc:placeholder runat="server" id="phContent" key="Content"></sc:placeholder>
				<div class="clearfix">
				</div>
			</div>
		</article>
	</div>
	<footer>
		<article>
			<sc:sublayout runat="server" id="footer" path="/tmwebcustom/sublayouts/common/footer.ascx"></sc:sublayout>
			<sc:placeholder runat="server" id="phFooter" key="FootSection"></sc:placeholder>
		</article>
	</footer>
   
	</form>
	<script type="text/javascript">
		$j(document).ready(function () {
			$j("li#findyournewhome").click(function () {
				$j("li#findyournewhome").last().addClass("selected");
				$j('#svgmap').show();				 
			});
			$j("a#svgclose").click(function () {
				$j('#svgmap').hide();
				$j("li#findyournewhome").removeClass("selected");
			});

			$j("a#lijoininterest").click(function () {
			    
				$j('#joinyourinterest').toggle();
			   <%=HomePageEvents.InHeader %>
			});
		    
		    <%= JoinYourInterestFormPosted?@"$j('#joinyourinterest').show();":string.Empty %>
		});
		
	</script>
    <sc:sublayout runat="server" id="clickdimensionscript" path="/tmwebcustom/sublayouts/common/CDAndLivePersonScripts.ascx"></sc:sublayout>

    <!-- Google Code Global Remarketing -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1030693709;
    var google_conversion_label = "G2QsCNPngAIQzca86wM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>


    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1030693709/?value=0&amp;label=G2QsCNPngAIQzca86wM&amp;guid=ON&amp;script=0&amp;w=1&amp;h=1&amp;as=1" />
    </div>
    </noscript>

    <script type="text/javascript">
        // BEGIN Google Analiytics Tracking code
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '<%= GoogleAnalyticsAccountNumber %>']);
        _gaq.push(['_setLocalRemoteServerMode']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        //END Google Analiytics Tracking code

        $j(document).ready(function () {
            $j('#siteSearchText').on("keypress", function (e) {
                if (e.keyCode == 13) {
                    sitesearch('#siteSearchText');
                    return false;
                }
            });
        });
	</script>

    <script type="text/javascript">
        adroll_adv_id = "NEJ47QLMS5CI3EP5D5Q2YF";
        adroll_pix_id = "JUA2LYWVABBBLGG64ADBUR";
        (function () {
            var oldonload = window.onload;
            window.onload = function () {
                __adroll_loaded = true;
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                if (oldonload) { oldonload() }
            };
        } ());
    </script>
     <asp:literal runat="server" id="yahooPixels"></asp:literal></body>
</html>
