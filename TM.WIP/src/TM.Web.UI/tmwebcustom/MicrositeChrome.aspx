﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true" %><%@ OutputCache Location="None" VaryByParam="none" %><!DOCTYPE html>
<html lang="en">
  <head>
    <sc:placeholder runat="server" id="HeadRegionPH" key="HeadRegion" />
    <title><sc:placeholder runat="server" id="TitlePH" key="PageTitle" /></title>
    <sc:placeholder runat="server" id="PageHeadRegionPH" key="PageHeadRegion" />
  </head>
  <body>
    <sc:placeholder runat="server" id="HeaderRegionPH" key="HeaderRegion" />
    <sc:placeholder runat="server" id="ContentRegionPH" key="ContentRegion" />
    <sc:placeholder runat="server" id="FooterRegionPH" key="FooterRegion" />
    <sc:placeholder runat="server" id="ScriptsPH" key="Scripts" />
    <div id="form"> 
      <form method="post" runat="server" id="mainform">
      <%#Sitecore.Context.PageMode.IsPageEditor? "<br/>": string.Empty %>
      <sc:Placeholder ID="phMain" runat="server" Key="Form" />
      </form>
    </div>
  </body>
</html>
