﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<html>
<head>
    <title>
        <sc:XslFile ID="Xslfile1" runat="server" RenderingID="{F88585C9-A6F7-49C8-B803-885518CC6CCB}"
            Path="/xsl/starter kit/html head title.xslt"></sc:XslFile>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link href="/starterkit_popup.css" rel="stylesheet" />
    <sc:Placeholder runat="server" Key="html-head-meta-data"></sc:Placeholder>
</head>
<body>
    <div id="entire-page">
        <form method="post" id="mainform" runat="server">
        <div style="padding: 10px;">
            <sc:Placeholder Key="content" ID="Placeholder1" runat="server"></sc:Placeholder>
        </div>
        </form>
    </div>
</body>
</html>
