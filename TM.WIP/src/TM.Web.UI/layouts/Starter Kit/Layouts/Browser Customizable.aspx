﻿<%@ Page Language="c#" CodePage="65001" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <sc:XslFile ID="Xslfile1" runat="server" RenderingID="{F88585C9-A6F7-49C8-B803-885518CC6CCB}"
            Path="/xsl/starter kit/html head title.xslt"></sc:XslFile>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link href="/starterkit.css" rel="stylesheet" />
    <sc:XslFile ID="Xslfile2" runat="server" RenderingID="{0D37377B-D3C1-435A-8CCC-238C938C44C8}"
        Path="/xsl/starter kit/html head include.xslt"></sc:XslFile>
    <sc:Placeholder runat="server" Key="html-head-meta-data"></sc:Placeholder>
</head>
<body>
    <div id="page-shadow">
        <div id="entire-page">
            <form method="post" id="mainform" runat="server">
                <sc:Placeholder Key="page-header" ID="pageheader" runat="server"></sc:Placeholder>
                <sc:Placeholder Key="page-columns" ID="pagecolumns" runat="server"></sc:Placeholder>
                <sc:Placeholder Key="page-footer" ID="pagefooter" runat="server"></sc:Placeholder>
                <sc:XslFile Path="/xsl/Starter Kit/Powered By.xslt" RenderingID="{2D63242D-51B2-4212-9CC1-C596B965808A}"
                    ID="XslFile7" runat="server"></sc:XslFile>
            </form>
            <a style="float: right; color: #ffffff; text-decoration: none"  href="http://www.sitecore.net/">
                powered by sitecore</a>
            <div id="bottom-spacing">
            </div>
        </div>
    </div>
</body>
</html>
