﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true" %>
<%@ register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>
    <sc:xslfile ID="Xslfile1" runat="server" renderingid="{F88585C9-A6F7-49C8-B803-885518CC6CCB}" path="/xsl/starter kit/html head title.xslt">
    </sc:xslfile>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="CODE_LANGUAGE" content="C#"/>
    <meta name="vs_defaultClientScript" content="JavaScript"/>
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
    <link href="/starterkit.css" rel="stylesheet"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon" /> 
    <sc:xslfile ID="Xslfile2" runat="server" renderingid="{0D37377B-D3C1-435A-8CCC-238C938C44C8}" path="/xsl/starter kit/html head include.xslt">
    </sc:xslfile>
    <sc:placeholder runat="server" key="html-head-meta-data"></sc:placeholder>
  </head>
  <body onload="if (window && window.print) { window.print(); }"><div id="entire-page"><form method="post" id="mainform" runat="server"><sc:xslfile placeholder="content" path="/xsl/starter kit/Print Header.xslt" renderingid="{E2496EAE-E2F5-44C4-83C6-CD3924A98F62}" runat="server"></sc:xslfile><sc:placeholder key="page-columns" runat="server"></sc:placeholder><sc:xslfile placeholder="content" path="/xsl/starter kit/Footer.xslt" renderingid="{356A5561-94FF-4B6C-8E63-2A26E66E691E}" runat="server"></sc:xslfile></form></div></body>
</html>
