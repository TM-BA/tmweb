﻿<%@ Register TagPrefix="sk" Namespace="Sitecore.Starterkit" Assembly="Sitecore.Starterkit" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Page Language="c#" CodePage="65001" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <title>
        <sc:XslFile ID="Xslfile1" runat="server" RenderingID="{F88585C9-A6F7-49C8-B803-885518CC6CCB}"
            Path="/xsl/starter kit/html head title.xslt"></sc:XslFile>
    </title>
    <sc:XslFile ID="Xslfile2" runat="server" RenderingID="{0D37377B-D3C1-435A-8CCC-238C938C44C8}"
        Path="/xsl/starter kit/html head include.xslt"></sc:XslFile>
    <sc:Placeholder runat="server" Key="html-head-meta-data"></sc:Placeholder>
    <link href="/starterkit.css" rel="stylesheet" />
</head>
<body>
    <div id="page-shadow">
        <div id="entire-page">
            <form id="mainform" method="post" runat="server">
            <sc:Sublayout runat="server" RenderingID="{6D366A65-54FF-49B5-A57F-2EBB9F426433}"
                placeholder="content" Path="/layouts/Starter Kit/Sublayouts/Header Fixed.ascx"
                ID="HeaderFixed"></sc:Sublayout>
            <sc:XslFile runat="server" RenderingID="{2B74B7E9-40B6-405D-844F-F4850F7B194D}" placeholder="content"
                Path="/xsl/Starter Kit/Top Menu.xslt" ID="TopMenu" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
            </sc:XslFile>
            <sc:XslFile runat="server" RenderingID="{3F3302BE-59DF-4F23-A378-179537B4B1FD}" placeholder="content"
                Path="/xsl/Starter Kit/Banner.xslt" ID="Banner" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
            </sc:XslFile>
            <sc:Placeholder runat="server" Key="page-columns" ID="pagecolumns"></sc:Placeholder>
            <sk:FooterControl runat="server" RenderingID="{B58E35CF-EC5B-4C57-9FF0-2DF646D79C6B}"
                ID="FooterControl1"></sk:FooterControl>
            <sc:XslFile runat="server" RenderingID="{469FAB68-A0EC-4599-B1AB-31C5AB1459AA}" placeholder="content"
                Path="/xsl/Starter Kit/Bottom Menu.xslt" ID="BottomMenu" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem">
            </sc:XslFile>
            <sc:XslFile runat="server" RenderingID="{2D63242D-51B2-4212-9CC1-C596B965808A}" Path="/xsl/Starter Kit/Powered By.xslt"
                ID="PoweredBy" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem"></sc:XslFile>
            </form>
            <a style="float: right; color: #ffffff; text-decoration: none" href="http://www.sitecore.net/"
                >powered by sitecore</a>
            <div id="bottom-spacing">
            </div>
        </div>
    </div>
</body>
</html>
