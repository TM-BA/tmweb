﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<!-- 
COLUMNS CONTENT AND RIGHT 
   -->
<div id="columns-C-and-R">
    <!-- 
CONTENT COLUMN 
   -->
    <div id="content-col">

        <div class="content-spacing">
            <sc:Placeholder runat="server" Key="content-top-row-front-page" ID="ContentTopRowFrontPage" />
        </div>
                <div class="content-spacing">
            <sc:Placeholder runat="server" Key="content" ID="content" />
        </div>
        <div class="content-spacing">
            <sc:Placeholder runat="server" Key="content-bottom-row-front-page" ID="ContentBottomRowFrontPage" />
        </div>
    </div>
    <!--
 RIGHT COLUMN 
   -->
    <div id="right-col">
        <sc:Placeholder runat="server" Key="column-right" ID="columnright" />
        <sc:XslFile runat="server" RenderingID="DD24FB5-CFAC-4223-AC3D-72F3E851DFE9%7d" placeholder="content"
            Path="/xsl/Starter Kit/Sidebar Text.xslt" ID="SidebarText" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem" />
        <sc:XslFile runat="server" RenderingID="81C8544-7FAB-410E-8563-8E9403DDAD67%7d" Path="/xsl/Starter Kit/Recent News.xslt"
            ID="RecentNews" Parameters="lang&amp;id&amp;sc_item&amp;sc_currentitem" /> 
        <br />
        <sc:Placeholder runat="server" Key="column-right-bottom" ID="columnrightbottom" />
    </div>
</div>
