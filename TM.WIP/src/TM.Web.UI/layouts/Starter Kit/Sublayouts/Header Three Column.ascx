﻿<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table width="100%">
    <tbody>
        <tr>
            <td align="left" id="header-left">
                <sc:Placeholder Key="header-left" ID="headerleft" runat="server"></sc:Placeholder>
            </td>
            <td align="center" id="header-middle">
                <sc:Placeholder Key="header-middle" ID="headermiddle" runat="server"></sc:Placeholder>
            </td>
            <td align="right" id="header-right">
                <sc:Placeholder Key="header-right" ID="headerright" runat="server"></sc:Placeholder>
            </td>
        </tr>
    </tbody>
</table>
