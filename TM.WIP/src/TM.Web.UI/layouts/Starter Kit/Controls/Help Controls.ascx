<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Help Controls.ascx.cs" Inherits="Sitecore.Starterkit.Help_Controls" %>
<asp:Panel ID="pnButtons" runat="server" DefaultButton="btnLeftHelpImage">
        <div class="help-controls">
          <table>
          <tr>
          <td>
          <asp:ImageButton ID="btnLeftHelpImage" runat="server" ImageUrl="~/images/ShowHelp.gif" OnClick="btnHelpImage_Click" />
          </td>
          <td valign="middle">
          <asp:LinkButton ID="btnHelpLink" runat="server" OnClick="btnHelpLink_Click">Show Help</asp:LinkButton>
          </td>
          </tr>
          </table>
        </div>
</asp:Panel>
