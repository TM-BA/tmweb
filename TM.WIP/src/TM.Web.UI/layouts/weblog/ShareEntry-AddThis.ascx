﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShareEntry-AddThis.ascx.cs" Inherits="Sitecore.Modules.WeBlog.layouts.Blog.ShareEntry_AddThis" %>
<div class="wb-entry-share wb-panel">
    <h1><%=Sitecore.Modules.WeBlog.Globalization.Translator.Render("SHARE")%></h1>

    <ul class="blog-icons">
        <li><a class="blog-y" href="#"></a></li>
        <li><a class="blog-twitter" href="#"></a></li>
        <li><a class="blog-fb" href="#"></a></li>
    </ul>

    
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js<%#AddThisAccountName%>"></script>
        <!-- AddThis Button END -->
    
</div>