﻿
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core.Analytics;
using Sitecore.Modules.EmailCampaign.Messages;
using Sitecore.Web;
using System;
using System.Web.UI;

namespace Sitecore.Modules.EmailCampaign.UI
{
  public class Uall : Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
	   Response.Write("Hello");
      string str = this.Context.Request.QueryString[GlobalSettings.AutomationStateQueryKey];
	  
	  
      if (string.IsNullOrEmpty(str) || !ShortID.IsShortID(str))
        return;
      
	    Guid recipientId = new Guid(str);
        string messageId;
        Guid stateId;
        string recipient = AnalyticsHelper.GetRecipient(recipientId, out messageId, out stateId);
        
		if (string.IsNullOrEmpty(recipient) || !ID.IsID(messageId))
          return;
		  
        Contact contactFromName = Factory.GetContactFromName(recipient);
        
		MessageItem message = Factory.GetMessage(messageId);
        
		if (contactFromName == null || message == null)
          return;
	  
	  Response.Write(contactFromName.InnerUser.LocalName);
	  WebUtil.Redirect(ClientApi.UnsubscribeFromAll(str) ?? Settings.ItemNotFoundUrl);
    }
  }
}
