﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Xsl;
using System.Xml.XPath;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using TM.Web.Custom.SCHelpers;

namespace TM.Web.Custom.XSL
{
	public class XslExtensions : Sitecore.Xml.Xsl.XslHelper 
	{
		public string GetUrl(XPathNodeIterator iterator)
		{
			Item item = this.GetRequiredItem(iterator);
            return SCUtils.GetItemUrl(item).Replace("/en/", "/");			

			//Sitecore.Links.UrlOptions URLOptions = new Sitecore.Links.UrlOptions();
			//URLOptions.Site = Sitecore.Configuration.Factory.GetSite("dev.tm.com");
			//return Sitecore.Links.LinkManager.GetItemUrl(item, URLOptions);
		}

		public Item GetRequiredItem(XPathNodeIterator iterator)
		{
			Assert.IsNotNull(iterator, "iterator");

			if (!iterator.MoveNext())
			{
				XsltException ex = new XsltException("No iterator.");
				Log.Error(ex.Message, ex, this);
				throw ex;
			}

			Item item = this.GetItem(iterator);

			if (item == null)
			{
				XsltException ex = new XsltException("No item.");
				Log.Error(ex.Message, ex, this);
				throw ex;
			}

			return item;
		}
	}
}