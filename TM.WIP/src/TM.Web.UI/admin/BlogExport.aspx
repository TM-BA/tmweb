﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlogExport.aspx.cs" Inherits="TM.Web.Custom.admin.BlogExport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Blog Export</title>
    <style>
body {
	font: 10pt Tahoma;
	margin:20px;
}
div {
    margin-top: 10px;
}
    </style>
</head>
<body>
    <div>Current user:
        <asp:Literal ID="literalUser" runat="server" />
    </div>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
    <asp:Panel ID="panelContent" runat="server" Visible="false">
        <div>Available blogs
            <asp:DropDownList ID="dropDownBlogs" runat="server" />
        </div>
        <div>
            <asp:Button type="submit" ID="buttonSubmit" Text="Run" runat="server" />
        </div>
        </asp:Panel>
    </form>
    <asp:Panel ID="panelLog" runat="server">
        <asp:Literal ID="literalLog" runat="server" />
    </asp:Panel>
    <asp:Panel ID="panelError" runat="server">
        <asp:Literal ID="literalError" runat="server" />
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</body>
</html>
