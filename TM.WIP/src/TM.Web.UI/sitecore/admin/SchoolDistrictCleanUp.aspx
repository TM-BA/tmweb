﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SchoolDistrictCleanUp.aspx.cs" Inherits="TM.Web.Custom.admin.SchoolDistrictCleanUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>School District Clean Up</title>
    <style>
body {
	font: 10pt Tahoma;
	margin:20px;
}
div {
    margin-top: 10px;
}
    </style>
</head>
<body>
    <div>Current user:
        <asp:Literal ID="literalUser" runat="server" />
    </div>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
    <asp:Panel ID="panelContent" runat="server" Visible="false">
        <div>Start school district clean up</div>
        <div>
            <asp:CheckBox ID="checkboxModifyCommunities" runat="server" Text="Modify communities" />
        </div>
        <div>
            <asp:CheckBox ID="checkboxDeleteOldSchools" runat="server" Text="Delete old schools" />
        </div>
        <div>
            <asp:Button type="submit" ID="buttonSubmit" Text="Run" runat="server" />
        </div>
        </asp:Panel>
    </form>
    <asp:Panel ID="panelLog" runat="server">
        <asp:Literal ID="literalLog" runat="server" />
    </asp:Panel>
    <asp:Panel ID="panelError" runat="server">
        <asp:Literal ID="literalError" runat="server" />
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</body>
</html>
