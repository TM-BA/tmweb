﻿if (typeof ($scw) === "undefined") {
    window.$scw = jQuery.noConflict(true);
}

$scw.widget("wffm.eventTracking", {
    options: {
        formId: null,
        pageId: null,
        fieldId: null,
        fieldTitle: null,
        fieldValue: null,
        eventCount: null,
        rules: null
    },

    _create: function () {
        var self = this,
            options = this.options;
        options.eventCount = 0;
        var id = $scw(this.element).attr("id");
        options.formId = $scw(this.element).children("input[name='" + id + ".FormId']").val();
        this.element.find("input[type!='submit'], select, textarea").bind('focus', function (e) {
            self.onFocusField(e, this);
        }).bind('blur change', function (e) { self.onBlurField(e, this); });
    },


    _getElementName: function (element) {
        var fieldName = element.name;
        if (!this.endsWith(fieldName.toLowerCase(),"value")) {
            var searchPattern = "fields[";
            var index = fieldName.toLowerCase().indexOf(searchPattern);
            return fieldName.substring(0, index + searchPattern.length + 3) + "Value";
        }

        return fieldName;
    },

    _getElementValue: function (element) {
        var value = null;
        var checkboxListValue = [];
        if (element.type == "checkbox") {
            var fieldName = element.name;            

            var form = $scw(element).closest("form");
            var selected = "selected";
            if (this.endsWith(fieldName.toLowerCase(), selected)) {
                var searchPattern = "items[";
                var index = fieldName.toLowerCase().indexOf(searchPattern);

                var pattern = fieldName.substring(0, index + searchPattern.length);
                var checkboxListSelected = form.find("input[name^='" + pattern + "']").not(":not(:checked)");
                $scw.each(checkboxListSelected, function () {
                    var valueId = this.name.slice(0, -selected.length) + "Value";
                    var valueElement = $scw(this).parent().find("input[name='" + valueId + "']");
                    if (valueElement) {
                        var v = valueElement.val();
                        checkboxListValue.push(v);
                    }
                });

                value = checkboxListValue;

            } else {
                var checkboxList = form.find("input[name='" + fieldName + "']");

                if (checkboxList.length > 1) {
                    checkboxList = checkboxList.not(":not(:checked)");
                    $scw.each(checkboxList, function () {
                        checkboxListValue.push($scw(this).val());
                    });

                    value = checkboxListValue;
                } else {
                    value = element.checked ? "1" : "0";
                }
            }

        } else {
            value = $scw(element).val();
        }



        return value;
    },

    onFocusField: function (e, element) {

        var fieldId = this._getElementName(element);
        var value = this._getElementValue(element);

        if (this.options.fieldId != fieldId) {
            this.options.fieldId = fieldId;
            this.options.fieldValue = value;
        }
    },

    onBlurField: function (e, element) {
        var form = $scw(element).closest("form");
        var validator = form.data("validator");

        var fieldId = this._getElementName(element);

        if (!this.endsWith(fieldId, "value")) {
            var owner = this._getOwner(form, fieldId);
            if (!owner) {
                return;
            }

            element = owner;
        }
        var value = this._getElementValue(element);
        if (this.options.fieldId != fieldId || (this.options.fieldId == fieldId && this.options.fieldValue != value)) {

            this.options.fieldId = fieldId;
            this.options.fieldValue = value;

            var fieldTitle = $scw(element).closest(".field-title")[0];
            if (!fieldTitle) {
                fieldTitle = this.closestElement(element, "field-title");
            }

            var title = fieldTitle[0].innerHTML.replace(/(\r\n|\n|\r)/gm, " ").trim();
            if ($scw.isArray(value)) {
                value = value.join(',');
            }

            if (element.type == "password") {
                value = "schidden";
            }

            var clientEvent = this._getEvent(fieldId, title, "Field Completed", value.replace(/<schidden>.*<\/schidden>/, "schidden"));

            var validationevent = [];
            var valid = validator.element(element);
            if (validator && !valid) {
                validationevent = this._checkClientValidation(element, title, validator);
            }
            this._trackEvents($scw.merge([clientEvent], validationevent));
        }
    },

    endsWith: function (str, suffix) {
        return str.toLowerCase().indexOf(suffix.toLowerCase(), str.length - suffix.length) !== -1;
    },

    closestElement: function (element, cssClass) {
        var parent = $scw(element).parent();
        var search = parent.find("." + cssClass);
        if (!search[0] && !parent.hasClass("field-border")) {
            return this.closestElement(parent, cssClass);
        }
        return search;
    },

    _getOwner: function (form, elementId) {

        var targetId = elementId.slice(0, -(elementId.length - elementId.lastIndexOf('.') - 1)) + "Value";
        return form.find("input[name=\"" + targetId + "\"]")[0];
    },

    _checkClientValidation: function (element, title, validator) {
        var tracker = this;
        var events = [];

        var rules = $scw.parseJSON(tracker.options.rules.replace(/&quot;/g, '"'));
        if (!rules) {
            rules = { "*": "{844BBD40-91F6-42CE-8823-5EA4D089ECA2}" };
        }

        $scw.each(validator.errorMap, function (key, value) {
            if (key == element.name) {

                var ruleName = tracker._selectKey(validator.settings.messages[key], value);

                var eventId = rules[ruleName];
                if (!eventId) {
                    eventId = rules["*"];
                }
                var clientEvent = tracker._getEvent(key, title, eventId, value);
                events.push(clientEvent);
            }
        });

        return events;
    },

    _selectKey: function (list, selectedValue) {
        var selectedKey;
        $scw.each(list, function (key, value) {
            if (selectedValue == value) {
                selectedKey = key;
                return false;
            }
        });
        return selectedKey;
    },

    _trackEvents: function (events) {
        $scw.ajax({
            type: 'POST',
            url: "/clientevent/process",
            data: JSON.stringify(events),
            dataType: 'json',
            contentType: 'application/json'
        });
    },

    _getEvent: function (fieldid, title, type, value) {
        var options = this.options;
        ++options.eventCount;

        var fieldIdHidden = fieldid.slice(0, -5) + "FieldId";
        fieldid = $scw("input[name=\"" + fieldIdHidden + "\"]").val();

        return {
            'FieldID': fieldid,
            'Type': type,
            'Value': value,
            'FieldTitle': title,
            'FormID': options.formId,
            'PageID': options.pageId,
            'Ticks': options.eventCount
        };
    },

});


