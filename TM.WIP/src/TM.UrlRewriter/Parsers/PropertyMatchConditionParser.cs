// UrlRewriter - A .NET URL Rewriter module
// Version 2.0
//
// Copyright 2011 Intelligencia
// Copyright 2011 Seth Yates
// 

using System;
using System.Xml;
using System.Configuration;
using TM.UrlRewriter.Conditions;
using TM.UrlRewriter.Utilities;
using TM.UrlRewriter.Utilities;

namespace TM.UrlRewriter.Parsers
{
    /// <summary>
    /// Parser for property match conditions.
    /// </summary>
    public sealed class PropertyMatchConditionParser : IRewriteConditionParser
    {
        /// <summary>
        /// Parses the condition.
        /// </summary>
        /// <param name="node">The node to parse.</param>
        /// <returns>The condition parsed, or null if nothing parsed.</returns>
        public IRewriteCondition Parse(XmlNode node)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            XmlNode propertyAttr = node.Attributes.GetNamedItem(Constants.AttrProperty);
            if (propertyAttr == null)
            {
                return null;
            }

            string match = node.GetRequiredAttribute(Constants.AttrMatch, true);

            return new PropertyMatchCondition(propertyAttr.Value, match);
        }
    }
}
