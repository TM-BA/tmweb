// UrlRewriter - A .NET URL Rewriter module
// Version 2.0
//
// Copyright 2011 Intelligencia
// Copyright 2011 Seth Yates
// 

using System;
using System.Net;
using System.Text.RegularExpressions;
using TM.UrlRewriter.Utilities;
using TM.UrlRewriter.Utilities;

namespace TM.UrlRewriter.Conditions
{
    /// <summary>
    /// Matches on the current remote IP address.
    /// </summary>
    public sealed class HostNameCondition : IRewriteCondition
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pattern"></param>
        public HostNameCondition(string pattern)
        {
            if (pattern == null)
            {
                throw new ArgumentNullException("pattern");
            }
            _pattern = pattern;
        }

        /// <summary>
        /// Determines if the condition is matched.
        /// </summary>
        /// <param name="context">The rewriting context.</param>
        /// <returns>True if the condition is met.</returns>
        public bool IsMatch(RewriteContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            // Use double-checked locking pattern to synchronise access to the regex.
            if (_regex == null)
            {
                lock (this)
                {
                    if (_regex == null)
                    {
                        _regex = new Regex(Pattern, RegexOptions.IgnoreCase);
                    }
                }
            }
            System.Diagnostics.Debug.Print("Pattern: " + Pattern);

            Match match = _regex.Match(context.Uri.Host);

            System.Diagnostics.Debug.Print("Location: " + context.Location);

            if (match.Success)
            {
                context.LastMatch = match;
            }

            return match.Success;
        }

        /// <summary>
        /// The pattern to match.
        /// </summary>
        public string Pattern
        {
            get { return _pattern; }
        }

        private Regex _regex;
        private string _pattern;
    }
}
