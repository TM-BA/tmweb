﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain
{
    //type-safe-enum pattern
	[Serializable]
    public sealed class TMUserDomain
    {
        private readonly String _name;
        private readonly int _value;

        public static readonly TMUserDomain TaylorMorrison = new TMUserDomain(1, "extranet");
        public static readonly TMUserDomain MonarchGroup = new TMUserDomain(2, "extranet");
        public static readonly TMUserDomain DarlingHomes = new TMUserDomain(3, "extranet");
        
        //talk to raj if you need to change the access level of this .ctor
        private TMUserDomain(int value, String name)
        {
            this._name = name;
            this._value = value;
        }

        public override String ToString()
        {
            return _name;
        }


    }


}
