using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class CompanyItem : CustomItem
{

public static readonly string TemplateId = "{733D370D-DE96-44CE-BC45-B8A0D5A0EE41}";

#region Inherited Base Templates

private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }


#endregion

#region Boilerplate CustomItem Code

public CompanyItem(Item innerItem) : base(innerItem)
{
	_TMBaseItem = new TMBaseItem(innerItem);
	

}

public static implicit operator CompanyItem(Item innerItem)
{
	return innerItem != null ? new CompanyItem(innerItem) : null;
}

public static implicit operator Item(CompanyItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField CompanyName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{DD448FCD-B608-403A-92F7-8AAA2281A3F2}"]);
	}
}


public CustomLookupField CompanyStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{FD8A8BCE-E990-4548-9BB8-B48DBBC99BE2}"]);
	}
}


public CustomTextField CompanyWebsiteURL
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{97C20545-7CF2-4853-8AA1-4E9D579F23C4}"]);
	}
}


public CustomLookupField CorporateBrandsStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{CC23D925-7088-4CCF-AB82-CC2F7CF631A6}"]);
	}
}


public CustomTextField LegacyCompanyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{6E0923E1-EA26-4E18-B860-DC69A96E2A2F}"]);
	}
}


public CustomTextField StreetAddress1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2A8AD443-8C17-47AB-BDB3-4FD8300DA3A4}"]);
	}
}


public CustomTextField StreetAddress2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{1B72D33B-D2E0-451F-850F-C0443C58E8E1}"]);
	}
}


public CustomMultiListField City
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{04A1D527-98C8-445B-ABAA-E624FEE17C0B}"]);
	}
}


public CustomMultiListField StateorProvinceAbbreviation
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{23F075BD-C408-4F8E-8FE9-0D48A3973AEC}"]);
	}
}


public CustomTextField ZiporPostalCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{56B90ED1-2890-44AF-BDC8-A6A499CC515D}"]);
	}
}


public CustomTextField Phone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{5BCA943C-D7B0-41A6-8BA1-87F24DE1D105}"]);
	}
}


public CustomImageField CompanyLogo
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{6DCD57EE-E230-490D-AEC0-6544CDACCD32}"]);
	}
}


public CustomImageField MyLogo
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{176779E3-72F7-41A3-BD34-C0E17466B79D}"]);
	}
}


public CustomTextField MyLabel
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{D7D046FE-92F1-4D72-BC83-AE77E4DC013D}"]);
	}
}


public CustomImageField PolicyImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{292FC148-F37A-4FF7-BB3A-D7CCCBB305FC}"]);
	}
}


public CustomLookupField PolicyDisplayText
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{B1E34A32-8B04-4299-9C07-A4D1F0010AEC}"]);
	}
}


public CustomTextField PolicyAltText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2FE7DE4D-B872-44A5-BFD4-1ECE86405FD1}"]);
	}
}


public CustomTextField CorporateDisclaimerImage
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{10466886-C2CB-4BED-8014-D3B03D227F5A}"]);
	}
}


public CustomTextField FinancingCalltoAction
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{63C07DA0-87C7-4B7B-9387-E2D1AFAEA9F6}"]);
	}
}


public CustomTextField SignUpforUpdatesButtonCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{139F8DF0-1803-4392-BB36-84DAA3F10F56}"]);
	}
}


public CustomImageField SignUpforUpdatesImage
{
	get
	{
		return new CustomImageField(InnerItem, InnerItem.Fields["{9A6394F9-29E5-40EE-B379-5881DC0B55A8}"]);
	}
}


public CustomTextField AskaQuestionButtonCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{00005AF5-2733-42CC-A9A0-F2CEB3B6E007}"]);
	}
}


public CustomTextField StartLiveChatButtonCopy
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{8860E5D9-9A97-482D-BFDC-9CA16CC9B696}"]);
	}
}


public CustomTextField SchoolReportURL
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{1ADAC784-DCFF-4519-A29E-1F1D1450B299}"]);
	}
}


#endregion //Field Instance Methods
}
}