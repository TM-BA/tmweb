using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class PlanBaseItem : CustomItem
{

public static readonly string TemplateId = "{4983D85E-EF26-44EA-8648-DF15BF7A3E61}";

#region Inherited Base Templates

private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public PlanBaseItem(Item innerItem) : base(innerItem)
{
	_TMBaseItem = new TMBaseItem(innerItem);

}

public static implicit operator PlanBaseItem(Item innerItem)
{
	return innerItem != null ? new PlanBaseItem(innerItem) : null;
}

public static implicit operator Item(PlanBaseItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField StatusforSearch
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{112B8C71-E575-4496-A0EE-E1074882705B}"]);
	}
}


public CustomLookupField StatusforAssistance
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{6705530C-3204-4F79-BF9E-810853E52102}"]);
	}
}


public CustomTextField SquareFootage
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{4CDA4A5E-9B60-441B-B12E-A222733A0B38}"]);
	}
}


public CustomTextField PlanDescription
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{531E4F58-67EF-4116-A47C-FCB01816B0EA}"]);
	}
}


public CustomCheckboxField FlashIFPAvailable
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{4432784A-F704-4AF2-847E-56677259A8EA}"]);
	}
}


//Could not find Field Type for Elevation Images


//Could not find Field Type for Interior


//Could not find Field Type for Floor Plan


public CustomLookupField NumberofStories
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{ADD15C2B-F7DF-4FE7-AF3A-078D70787FD9}"]);
	}
}


public CustomMultiListField NumberofBedrooms
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{FE5ABCE3-78BB-40EB-98BE-97F401519922}"]);
	}
}


public CustomLookupField MasterBedroomLocation
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{8BD9B993-CCCC-4056-9414-183AB3B1227F}"]);
	}
}


public CustomMultiListField NumberofBathrooms
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{E141D5EB-2EF0-45FB-874D-91E1A886D2F9}"]);
	}
}


public CustomMultiListField NumberofHalfBathrooms
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{49C7CF48-C394-4E92-88F6-10A4883AC609}"]);
	}
}


public CustomMultiListField NumberofGarages
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{24CFB438-D9C5-4863-97A0-8A17F6C5C4BE}"]);
	}
}


public CustomLookupField GarageEntryLocation
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{03EF2C53-7631-46CF-B666-467627FDEA44}"]);
	}
}


public CustomMultiListField NumberofDens
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{811228B3-BD23-48E4-9739-C706A3607A47}"]);
	}
}


public CustomMultiListField NumberofSolariums
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{5FD4B618-CBD4-4F0A-A1B3-307BFF06A3EC}"]);
	}
}


public CustomMultiListField NumberofBonusRooms
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{92A23EE5-D29F-4047-B588-0DFCFDAF3BD4}"]);
	}
}


public CustomMultiListField NumberofFamilyRooms
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{3E5EBDE3-D9A8-4B04-854E-B3BFC581BE35}"]);
	}
}


public CustomMultiListField NumberofLivingAreas
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{A495BC45-C238-49B9-895D-3741C4880559}"]);
	}
}


public CustomMultiListField NumberofDiningAreas
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{2A77522C-41D5-4144-A3E9-FEB89C28A603}"]);
	}
}


public CustomCheckboxField HasaLivingRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{F6C50A71-5928-4091-AC6E-7B21259A3965}"]);
	}
}


public CustomCheckboxField HasaDiningRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{2A07EE2D-7BCF-4198-B075-E198E20FEF93}"]);
	}
}


public CustomCheckboxField HasaSunRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{02541046-7551-46B5-B50C-3845D1DD4A75}"]);
	}
}


public CustomCheckboxField HasaStudy
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{2DCA9C5E-0700-4A34-81FC-13BE8F454FFE}"]);
	}
}


public CustomCheckboxField HasaLoft
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{55F886EC-C44B-4B0C-A3EE-BA96B4BCB608}"]);
	}
}


public CustomCheckboxField HasanOffice
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{BFB61A1F-6350-41EB-8319-CB0A76EE31FC}"]);
	}
}


public CustomCheckboxField HasaGameRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{D06814B1-ECB8-4463-BBD1-E80AF705030C}"]);
	}
}


public CustomCheckboxField HasaMediaRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{0EF3157D-DECD-4BDC-81FE-E794E7D7BEFB}"]);
	}
}


public CustomCheckboxField HasaGuestBedroom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{E857B938-D3B0-4678-AEC8-FC641A658B89}"]);
	}
}


public CustomCheckboxField HasaBonusRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{A2161332-BA6C-424F-B6B4-209E062B030C}"]);
	}
}


public CustomCheckboxField HasaBasement
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{8DEF55B1-5BC9-40B5-9DBB-D5BEAE5191BF}"]);
	}
}


public CustomCheckboxField HasaFireplace
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{BB465FE8-E2AF-44BA-8214-3E0A25EF5244}"]);
	}
}


public CustomCheckboxField HasaMasterBedroom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{514DDF10-2D3C-40F5-BD5D-2AEB07635144}"]);
	}
}


public CustomCheckboxField HasaFamilyRoom
{
	get
	{
		return new CustomCheckboxField(InnerItem, InnerItem.Fields["{D4DA35E5-EAAF-4279-92AD-925D9210D7BA}"]);
	}
}


public CustomTextField ImageSource
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{E75C4E10-CCCE-4954-9144-08EB6901DEB6}"]);
	}
}


#endregion //Field Instance Methods


public CustomMultiListField ElevationImages
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{55A14BEF-969E-4B00-8A03-37E67F069E3D}"]);
    }
}


public CustomMultiListField InteriorImages
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{81BB094D-8C55-4723-A602-C7E7F1E1C5DF}"]);
    }
}

public CustomMultiListField FloorPlanImages
{
    get
    {
        return new CustomMultiListField(InnerItem, InnerItem.Fields["{ED4733D5-EFD7-4C18-A8C3-D418E85910A5}"]);
    }
}
}
}