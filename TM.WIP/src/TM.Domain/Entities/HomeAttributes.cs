namespace TM.Domain.Entities
{
    public class HomeAttributes
    {
        //see:HomeAttributeExtension
        public bool IsCurrentItemAPlan { get; set; }
               
        public int Bathrooms { get; set; }

        public int HalfBaths { get; set; }

        public int Garage { get; set; }

        public int Stories { get; set; }

        public int Bedrooms { get; set; }

        public int SqFt { get; set; }

        public int Price { get; set; }

        public string Description { get; set; }

        public string PriceText { get; set; }
    }
}