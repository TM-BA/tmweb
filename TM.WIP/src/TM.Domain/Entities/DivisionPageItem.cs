using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;


namespace TM.Domain.Entities
{
public partial class DivisionPageItem : CustomItem
{

public static readonly string TemplateId = "{C70FF0BA-F242-45A3-AE34-1BD9B700B06E}";

#region Inherited Base Templates



#endregion

#region Boilerplate CustomItem Code

public DivisionPageItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator DivisionPageItem(Item innerItem)
{
	return innerItem != null ? new DivisionPageItem(innerItem) : null;
}

public static implicit operator Item(DivisionPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomIntegerField XValue
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{B6BE745B-1943-4AD4-ADA6-3A2652A965F3}"]);
	}
}

public CustomTextField CRMDivisionName
{
    get
    {
        return new CustomTextField(InnerItem, InnerItem.Fields["{EC6FE63A-1E5A-446B-8DEF-717039C7DA3B}"]);
    }
}

public CustomTextField DivisionName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{470D848B-6386-42AC-AFC6-B2A3D6F8FA15}"]);
	}
}


public CustomIntegerField YValue
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{200DFD1B-F43D-43DF-9029-2E0F34CB3DD2}"]);
	}
}


public CustomTextField LegacyDivisionID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{32FB9529-23B1-4FB8-893E-30F444BA1A18}"]);
	}
}


public CustomLookupField StatusforSearch
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{BB7FBC93-55B7-438C-96A3-70D88740E625}"]);
	}
}


public CustomLookupField StatusforAssistance
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{E2C9FB18-8549-4906-BE83-99082C3FA9E4}"]);
	}
}


public CustomIntegerField SearchCentroidLatitude
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{683BFADE-E34B-4D67-8C07-6C662670EA1C}"]);
	}
}


public CustomIntegerField SearchCentroidLongitude
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{5952D916-96D9-4BA8-AE74-EF2B7C1FAC31}"]);
	}
}


public CustomMultiListField IHCAssignment
{
	get
	{
		return new CustomMultiListField(InnerItem, InnerItem.Fields["{13779C44-EBC9-4347-B2BB-79776B59785C}"]);
	}
}


public CustomTextField StreetAddress1
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{47047D22-BDF8-4713-8A20-7F61698F112B}"]);
	}
}


public CustomTextField StreetAddress2
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{AF3D4CFB-787C-45C6-A791-2CEEA7D00426}"]);
	}
}


public CustomTextField City
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{A32727C2-5632-4DD7-AD7B-FC13E251C09F}"]);
	}
}


public CustomTextField ZiporPostalCode
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{9EC10919-B4AD-4DFA-BBB8-ED0F9C8E2901}"]);
	}
}


public CustomTextField Phone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2D5ECA74-9F57-46E0-878D-AA0F655D71E6}"]);
	}
}


public CustomTextField AlternatePhone
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{91F16461-F39E-4EBA-BEEC-2B38B897FADA}"]);
	}
}


public CustomTextField Fax
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{01A19931-7036-48EC-B04F-9A37174A44A0}"]);
	}
}


public CustomTextField DivisionDisclaimerText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{EB0B52B0-7529-4811-9F27-9A1EC161BDE2}"]);
	}
}


//Could not find Field Type for Grid Homes for Sale


//Could not find Field Type for Grid Plans


public CustomGeneralLinkField FacebookURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{3B52CF0F-47F1-4F17-842E-F0D0E38C8F80}"]);
	}
}


public CustomGeneralLinkField TwitterURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{A6BA7A70-AB35-4259-835C-C17547A5DFFB}"]);
	}
}


public CustomGeneralLinkField YouTubeURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{B3ECE7F2-5BBB-42D7-9A54-F4C9417BECF3}"]);
	}
}


public CustomGeneralLinkField PinterestURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{0C9FFB51-F106-49A9-921E-D7FA966EA89C}"]);
	}
}


public CustomGeneralLinkField GooglePlusURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{E6D1CB75-BC81-486B-B651-E3CE12984547}"]);
	}
}


public CustomGeneralLinkField BlogURL
{
	get
	{
		return new CustomGeneralLinkField(InnerItem, InnerItem.Fields["{A15E87EA-C5CD-444B-A63C-E58749EB2D77}"]);
	}
}


//Could not find Field Type for Warranty Email Address


//Could not find Field Type for Miscellaneous Contact Email Address


//Could not find Field Type for Trade Partner Contact Email Address


//Could not find Field Type for Land Partner Contact Email Address


public CustomTextField CompanyContractorNumber
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{09BBABE5-F535-417A-85BA-FED2DCE2DE29}"]);
	}
}


public CustomIntegerField RealtorCommissionRate
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{BB265EC6-A384-48B5-A34D-EFA86601C31C}"]);
	}
}

public TextField NSS_DB
{
    get
    {
        return new TextField(InnerItem.Fields["{778638D9-6D8D-4B91-B11C-A53DBD9920A9}"]);
    }
}

public CustomTextField SEOContent
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{29B5966D-4692-43C2-9757-56481A2A347B}"]);
	}
}


#endregion //Field Instance Methods
}
}