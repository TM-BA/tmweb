﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class SlideShowOptions
    {
        private string _carousel=".carousel";
        public string carousel
        {
            get { return _carousel; }
            set { _carousel = value; }
        }

        public bool slideWidth { get; set; }

        private bool _jumpQueue = true;
        public bool jumpQueue
        {
            get { return _jumpQueue; }
            set { _jumpQueue = value; }
        }

        private int _offset = 1;
        public int offset
        {
            get { return _offset; }
            set { _offset = value; }
        }

        private bool _skip = true;
        public bool skip
        {
            get { return _skip; }
            set { _skip = value; }
        }

        private bool _pagination = true;
        public bool pagination
        {
            get { return _pagination; }
            set { _pagination = value; }
        }

        private bool _gestures= true;
        public bool gestures
        {
            get { return _gestures; }
            set { _gestures = value; }
        }

        private int _auto= 3000;
        public int auto
        {
            get { return _auto; }
            set { _auto = value; }
        }

        private bool _autostop= true;
        public bool autostop
        {
            get { return _autostop; }
            set { _autostop = value; }
        }

        private bool _hoverPause;
        public bool hoverPause
        {
            get { return _hoverPause; }
            set { _hoverPause = value; }
        }

        private bool _loop;
        public bool loop
        {
            get { return _loop; }
            set { _loop = value; }
        }

        private string _nextText;
        public string nextText
        {
            get { return _nextText; }
            set { _nextText = value; }
        }

        private string _previousText;
        public string previousText
        {
            get { return _previousText; }
            set { _previousText = value; }
        }

        private int _skipCount=1;
        public int skipCount
        {
            get { return _skipCount; }
            set { _skipCount = value; }
        }

        private string _transition="scroll";
        public string transition
        {
            get { return _transition; }
            set { _transition = value; }
        }

        private int _speed;
        public int speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        private string _easing="swing";
        public string easing
        {
            get { return _easing; }
            set { _easing = value; }
        }

        private int _visible=1;
        public int visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public bool onupdate { get; set; }

        public bool oncomplete { get; set; }

        private string _wrapper = "<div style=\"position:relative;overflow:hidden;\">";
        public string wrapper
        {
            get { return _wrapper; }
            set { _wrapper = value; }
        }
    }
}

