using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class PlanBaseAdditionalItem : CustomItem
{

public static readonly string TemplateId = "{53100E20-4582-4C37-9AA6-3B92E6165ED4}";


#region Boilerplate CustomItem Code

public PlanBaseAdditionalItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator PlanBaseAdditionalItem(Item innerItem)
{
	return innerItem != null ? new PlanBaseAdditionalItem(innerItem) : null;
}

public static implicit operator Item(PlanBaseAdditionalItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField SubCommunityID
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{DC810098-A7EC-4FDF-B21F-A5F71336B83F}"]);
	}
}


public CustomTextField VirtualToursMasterPlanID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2E565CB5-378D-434D-BE51-AB94748E72EE}"]);
	}
}


public CustomLookupField VirtualTourStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{D4047B64-6ED0-4511-86E2-066AFFF167F7}"]);
	}
}


public CustomTextField PlanBrochureMasterID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{52090ED2-0A28-4498-9580-F2E0D6A9ED72}"]);
	}
}


public CustomLookupField PlanBrochureStatus
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{FDF4B419-721C-4524-9224-3540000FB67B}"]);
	}
}


#endregion //Field Instance Methods
}
}