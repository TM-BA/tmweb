﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class PlanFacets
    {
        public string MaxBedroom;
        public string MinBedroom;
        public string MaxBathroom;
        public string MinBathroom;
        public string MaxPrice;
        public string MinPrice;
        public string MaxGarage;
        public string MinGarage;
        public string MaxStory;
        public string MinStory;
    }
}
