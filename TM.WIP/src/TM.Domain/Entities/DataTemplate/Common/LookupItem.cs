using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities.DataTemplate.Common
{
public partial class LookupItem : CustomItem
{

public static readonly string TemplateId = "{A9E74B92-8790-4AD7-8114-BD8F4CB83991}";


#region Boilerplate CustomItem Code

public LookupItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator LookupItem(Item innerItem)
{
	return innerItem != null ? new LookupItem(innerItem) : null;
}

public static implicit operator Item(LookupItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{126B89AC-85F4-4223-BADC-88037D78A799}"]);
	}
}


public CustomTextField Code
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{3C3DA81A-08AE-48DE-AE3B-0D5B3D1556FF}"]);
	}
}


public CustomTextField CMSId
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{2C7323AA-D803-4672-80E3-651AC28B25F0}"]);
	}
}


#endregion //Field Instance Methods
}
}