using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class PlanAndHomeForSaleOnlyBaseItem : CustomItem
{

public static readonly string TemplateId = "{92FE3ABD-E0D3-40E2-B33F-0DF28D3707A2}";


#region Boilerplate CustomItem Code

public PlanAndHomeForSaleOnlyBaseItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator PlanAndHomeForSaleOnlyBaseItem(Item innerItem)
{
	return innerItem != null ? new PlanAndHomeForSaleOnlyBaseItem(innerItem) : null;
}

public static implicit operator Item(PlanAndHomeForSaleOnlyBaseItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomIntegerField SlideshowImageRotationSpeed
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{7E987E16-B4E3-4DB8-853D-974633C42851}"]);
	}
}


#endregion //Field Instance Methods
}
}