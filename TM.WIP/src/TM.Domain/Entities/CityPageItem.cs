using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;



namespace TM.Domain.Entities
{
public partial class CityPageItem : CustomItem
{

public static readonly string TemplateId = "{DE2DA9D0-C765-46B9-820D-28325FA1B227}";

#region Inherited Base Templates

private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public CityPageItem(Item innerItem) : base(innerItem)
{
	
	_TMBaseItem = new TMBaseItem(innerItem);

}

public static implicit operator CityPageItem(Item innerItem)
{
	return innerItem != null ? new CityPageItem(innerItem) : null;
}

public static implicit operator Item(CityPageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField CityName
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{C960CEC6-DB17-42AB-A281-747C010C298D}"]);
	}
}


public CustomLookupField StatusforSearch
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{FB011121-0EBD-4746-9C41-6B2C14EB35C5}"]);
	}
}


public CustomLookupField StatusforAssistance
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{B892ADC4-518B-475A-8808-FB601D1B0D0B}"]);
	}
}


public CustomTextField SearchCentroidLatitude
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{C9C5B5F5-BBB2-464E-BFD6-F462724D053A}"]);
	}
}


public CustomTextField SearchCentroidLongitude
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{74F6AE34-4799-437B-BE91-B9BBA8DBF266}"]);
	}
}


public CustomTextField CityDisclaimerText
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{B928BCF4-7453-4684-A714-4BBD4B62245F}"]);
	}
}


public CustomTextField GridHomesforSale
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{43835830-CEEC-40C2-B865-F5B986602261}"]);
	}
}


public CustomTextField GridPlans
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{7AFDDDCD-F7A8-4F0B-9DA8-BBE096CAC9C0}"]);
	}
}


public CustomTextField LegacyID
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{0511C4C0-A45B-4695-A42B-3D4E635A7BE7}"]);
	}
}


public CustomTextField CMSId
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{4267D895-C9E5-4EF2-969D-F2BAED157C48}"]);
	}
}


#endregion //Field Instance Methods
}
}