using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class StatePageItem : CustomItem
{

public static readonly string TemplateId = "{2B7CD906-758A-4E29-BAAC-453349506CE1}";

#region Inherited Base Templates

private readonly TMBaseItem _TMBaseItem;
public TMBaseItem TMBase { get { return _TMBaseItem; } }

#endregion

#region Boilerplate CustomItem Code

public StatePageItem(Item innerItem) : base(innerItem)
{
	
	_TMBaseItem = new TMBaseItem(innerItem);

}

public static implicit operator StatePageItem(Item innerItem)
{
	return innerItem != null ? new StatePageItem(innerItem) : null;
}

public static implicit operator Item(StatePageItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomLookupField StateName
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{7CFCBB2C-E477-440B-8D22-895D9E793DF2}"]);
	}
}


public CustomLookupField StatusforSearch
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{87EA051C-50F3-4F8B-90F3-1B8FBAAC4C74}"]);
	}
}


public CustomLookupField StatusforAssistance
{
	get
	{
		return new CustomLookupField(InnerItem, InnerItem.Fields["{78328961-D3B3-4EFA-A3C0-D1E9430BDCDC}"]);
	}
}


public CustomIntegerField SearchCentroidLatitude
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{A9988C35-D3B1-4CD7-BF3F-F75D18BA2631}"]);
	}
}


public CustomIntegerField SearchCentroidLongitude
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{781A117F-782B-48A6-B193-FD9C33C67EE7}"]);
	}
}


public CustomIntegerField SearchCentroidZoom
{
	get
	{
		return new CustomIntegerField(InnerItem, InnerItem.Fields["{3E4C848D-14D4-484B-ACBD-A281CA04C5B9}"]);
	}
}


public CustomTextField StateDisclaimerImage
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{F9C40FBF-5093-434E-8E46-7A8661FB97BA}"]);
	}
}


#endregion //Field Instance Methods
}
}