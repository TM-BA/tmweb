using System;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using Sitecore.Web.UI.WebControls;
using CustomItemGenerator.Fields.LinkTypes;
using CustomItemGenerator.Fields.ListTypes;
using CustomItemGenerator.Fields.SimpleTypes;

namespace TM.Domain.Entities
{
public partial class LocalInterestCategoryItem : CustomItem
{

public static readonly string TemplateId = "{93E6D17A-86DE-40E5-832A-6DA7E93D23BE}";


#region Boilerplate CustomItem Code

public LocalInterestCategoryItem(Item innerItem) : base(innerItem)
{

}

public static implicit operator LocalInterestCategoryItem(Item innerItem)
{
	return innerItem != null ? new LocalInterestCategoryItem(innerItem) : null;
}

public static implicit operator Item(LocalInterestCategoryItem customItem)
{
	return customItem != null ? customItem.InnerItem : null;
}

#endregion //Boilerplate CustomItem Code


#region Field Instance Methods


public CustomTextField Name
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{8C14179B-BC9E-456B-B241-D7D36D375204}"]);
	}
}


public CustomTextField Code
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{31C978D0-035D-4998-8EFE-6ECE3734D815}"]);
	}
}


public CustomTextField CMSId
{
	get
	{
		return new CustomTextField(InnerItem, InnerItem.Fields["{6BA8CFAF-C855-40E3-A102-BBA7ECB3931A}"]);
	}
}


#endregion //Field Instance Methods
}
}