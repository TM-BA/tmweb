﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Entities
{
    public class PlansAlsoAvailableCommunity
    {
        public string CommunityName { get; set; }
        public string CommunityAddress { get; set; }
        public string CommunityPriceRange { get; set; }
        public string CommunityPriceText { get; set; }
        public string CommunityBedroomRange { get; set; }
        public string CommunityBathroomRange { get; set; }
        public string CommunityLink { get; set; }
    }
}
