﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain
{
	[Serializable]
	public class TMUser
	{

		public TMUser(TMUserDomain domain, string userName)
		{
			this.Domain = domain;
			this.UserName = userName;
			this.Email = userName;
		}
	 
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Email { get; set; }

		public string Password { get; set; }

		public string AddressLine1 { get; set; }

		public string AddressLine2 { get; set; }

		public string City { get; set; }

		public string StateAbbreviation { get; set; }
		public string Zipcode { get; set; }
		public string Phone { get; set; }
		public string PhoneExtension { get; set; }

		public string AreaOfInterest { get; set; }

		public string UserName { get; set; }
		public string PasswordRecoveryId { get; set; }

		public TMUserDomain Domain { get; private set; }
		public string Device { get; set; }

		public string CommunityFavorites { get; set; }
		public string PlanFavorites { get; set; }
		public string HomeforSaleFavorites { get; set; }
		public string IFPS { get; set; }

		public string FullyQualifiedUserName
		{
			get { return Domain + @"\" + UserName; }
		} 
	}
}
