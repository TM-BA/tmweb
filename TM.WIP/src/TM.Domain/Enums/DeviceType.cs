﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Enums
{
    public enum DeviceType
    {
        TaylorMorrison = 1,
        MonarchGroup = 2,
        DarlingHomes = 3

    }

}