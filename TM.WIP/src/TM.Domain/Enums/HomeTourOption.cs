﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Domain.Enums
{
    public enum HomeTourOption
    {
        None,
        AvailableAsModelHome,
        AvailableAsModelHomeInNearByCommunity,
        AvailableAsInventory,
    }
}
