﻿using System;

namespace Sitecore.Modules.SitemapXML
{
    public class SitemapHandler
    {
        public void RefreshSitemap(object sender, EventArgs args)
        {
            SitemapManager sitemapManager = new SitemapManager();
            sitemapManager.SubmitSitemapToSearchenginesByHttp();
            sitemapManager.RegisterSitemapToRobotsFile();
        }
    }
}
