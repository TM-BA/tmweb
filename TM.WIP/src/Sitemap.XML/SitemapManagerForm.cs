﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

namespace Sitecore.Modules.SitemapXML
{
    public class SitemapManagerForm : BaseForm
    {
        protected Button RefreshButton;
        protected Literal Message;

        protected override void OnLoad(EventArgs args)
        {
            base.OnLoad(args);
            if (Context.ClientPage.IsEvent)
                return;
            this.RefreshButton.Click = "RefreshButtonClick";
        }

        protected void RefreshButtonClick()
        {
            new SitemapHandler().RefreshSitemap(this, new EventArgs());

            StringDictionary sites = SitemapManagerConfiguration.GetSites();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string str in (IEnumerable)sites.Values)
            {
                if (stringBuilder.Length > 0)
                    stringBuilder.Append(", ");
                stringBuilder.Append(str);
            }

            this.Message.Text = string.Format(" - The sitemap file <b>\"{0}\"</b> has been refreshed<br /> - <b>\"{0}\"</b> has been registered to \"robots.txt\"", stringBuilder.ToString());
            SitemapManagerForm.RefreshPanel("MainPanel");
        }

        private static void RefreshPanel(string panelName)
        {
            Panel panel = Context.ClientPage.FindControl(panelName) as Panel;
            Assert.IsNotNull((object)panel, "can't find panel");
            Context.ClientPage.ClientResponse.Refresh((System.Web.UI.Control)panel);
        }
    }
}
