﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Xml;
using System.Collections.Specialized;
using System.Xml;

namespace Sitecore.Modules.SitemapXML
{
    public class SitemapManagerConfiguration
    {
        public static string XmlnsTpl
        {
            get
            {
                return SitemapManagerConfiguration.GetValueByName("xmlnsTpl");
            }
        }

        public static string WorkingDatabase
        {
            get
            {
                return SitemapManagerConfiguration.GetValueByName("database");
            }
        }

        public static string SitemapConfigurationItemPath
        {
            get
            {
                return SitemapManagerConfiguration.GetValueByName("sitemapConfigurationItemPath");
            }
        }

        public static string EnabledTemplates
        {
            get
            {
                return SitemapManagerConfiguration.GetValueByNameFromDatabase("Enabled templates");
            }
        }

        public static string ExcludeItems
        {
            get
            {
                return SitemapManagerConfiguration.GetValueByNameFromDatabase("Exclude items");
            }
        }

        public static bool IsProductionEnvironment
        {
            get
            {
                string valueByName = SitemapManagerConfiguration.GetValueByName("productionEnvironment");
                return !string.IsNullOrEmpty(valueByName) && (valueByName.ToLower() == "true" || valueByName == "1");
            }
        }

        private static string GetValueByName(string name)
        {
            string str = string.Empty;
            foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sitemapVariable"))
            {
                if (XmlUtil.GetAttribute("name", node) == name)
                {
                    str = XmlUtil.GetAttribute("value", node);
                    break;
                }
            }
            return str;
        }

        private static string GetValueByNameFromDatabase(string name)
        {
            string str = string.Empty;
            Database database = Factory.GetDatabase(SitemapManagerConfiguration.WorkingDatabase);
            if (database != null)
            {
                Item obj = database.Items[SitemapManagerConfiguration.SitemapConfigurationItemPath];
                if (obj != null)
                    str = obj[name];
            }
            return str;
        }

        public static StringDictionary GetSites()
        {
            StringDictionary stringDictionary = new StringDictionary();
            foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sites/site"))
            {
                if (!string.IsNullOrEmpty(XmlUtil.GetAttribute("name", node)) && !string.IsNullOrEmpty(XmlUtil.GetAttribute("filename", node)))
                    stringDictionary.Add(XmlUtil.GetAttribute("name", node), XmlUtil.GetAttribute("filename", node));
            }
            return stringDictionary;
        }

        public static string GetServerUrlBySite(string name)
        {
            string str = string.Empty;
            foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sites/site"))
            {
                if (XmlUtil.GetAttribute("name", node) == name)
                {
                    str = XmlUtil.GetAttribute("serverUrl", node);
                    break;
                }
            }
            return str;
        }
    }
}
