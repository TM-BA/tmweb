﻿using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.Sites;
using Sitecore.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;

namespace Sitecore.Modules.SitemapXML
{
    public class SitemapManager
    {
        private static StringDictionary m_Sites;

        public Database Db
        {
            get
            {
                return Factory.GetDatabase(SitemapManagerConfiguration.WorkingDatabase);
            }
        }

        public SitemapManager()
        {
            m_Sites = SitemapManagerConfiguration.GetSites();
            foreach (DictionaryEntry dictionaryEntry in m_Sites)
                BuildSiteMap(dictionaryEntry.Key.ToString(), dictionaryEntry.Value.ToString());
        }

        private void BuildSiteMap(string sitename, string sitemapUrlNew)
        {
            Site site = SiteManager.GetSite(sitename);
            List<Item> sitemapItems = GetSitemapItems(Factory.GetSite(sitename).StartPath);
            string path = MainUtil.MapPath("/" + sitemapUrlNew);

            string str = BuildSitemapXML(sitemapItems, site);
            StreamWriter streamWriter = new StreamWriter(path, false);
            streamWriter.Write(str);
            streamWriter.Close();
        }

        public bool SubmitSitemapToSearchenginesByHttp()
        {
            if (!SitemapManagerConfiguration.IsProductionEnvironment)
                return false;

            bool result = false;
            Item sitemapConfig = Db.Items[SitemapManagerConfiguration.SitemapConfigurationItemPath];

            if (sitemapConfig != null)
            {
                string engines = sitemapConfig.Fields["Search engines"].Value;
                foreach (string index in engines.Split('|'))
                {
                    Item engine = this.Db.Items[index];
                    if (engine != null)
                    {
                        string engineHttpRequestString = engine.Fields["HttpRequestString"].Value;
                        foreach (string sitemapUrl in (IEnumerable)SitemapManager.m_Sites.Values)
                            this.SubmitEngine(engineHttpRequestString, sitemapUrl);
                    }
                }
                result = true;
            }
            return result;
        }

        public void RegisterSitemapToRobotsFile()
        {
            string path = MainUtil.MapPath("/" + "robots.txt");
            StringBuilder sitemapContent = new StringBuilder(string.Empty);
            if (File.Exists(path))
            {
                StreamReader streamReader = new StreamReader(path);
                sitemapContent.Append(streamReader.ReadToEnd());
                streamReader.Close();
            }

            StreamWriter streamWriter = new StreamWriter(path, false);
            foreach (string sitemapUrl in m_Sites.Values)
            {
                string sitemapLine = "Sitemap: " + sitemapUrl;
                if (!sitemapContent.ToString().Contains(sitemapLine))
                    sitemapContent.AppendLine(sitemapLine);
            }
            streamWriter.Write(sitemapContent.ToString());
            streamWriter.Close();
        }

        private string BuildSitemapXML(List<Item> items, Site site)
        {
            XmlDocument doc = new XmlDocument();

            XmlNode declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            XmlNode urlsetNode = doc.CreateElement("urlset");
            XmlAttribute xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            urlsetNode.Attributes.Append(xmlnsAttr);

            doc.AppendChild(urlsetNode);

            foreach (Item itm in items)
            {
                doc = this.BuildSitemapItem(doc, itm, site);
            }

            return doc.OuterXml;
        }

        private XmlDocument BuildSitemapItem(XmlDocument doc, Sitecore.Data.Items.Item item, Site site)
        {
            string url = HtmlEncode(this.GetItemUrl(item, site));
            string lastMod = HtmlEncode(item.Statistics.Updated.ToString("yyyy-MM-ddTHH:mm:sszzz"));

            XmlNode urlsetNode = doc.LastChild;

            XmlNode urlNode = doc.CreateElement("url");
            urlsetNode.AppendChild(urlNode);

            XmlNode locNode = doc.CreateElement("loc");
            urlNode.AppendChild(locNode);
            locNode.AppendChild(doc.CreateTextNode(url));

            XmlNode lastmodNode = doc.CreateElement("lastmod");
            urlNode.AppendChild(lastmodNode);
            lastmodNode.AppendChild(doc.CreateTextNode(lastMod));

            return doc;
        }

        private string GetItemUrl(Sitecore.Data.Items.Item item, Site site)
        {
            var options = UrlOptions.DefaultOptions;

            options.SiteResolving = Settings.Rendering.SiteResolving;
            options.Site = SiteContext.GetSite(site.Name);
            options.AlwaysIncludeServerUrl = false;

            var url = LinkManager.GetItemUrl(item, options);
            var serverUrl = SitemapManagerConfiguration.GetServerUrlBySite(site.Name);
            if (serverUrl.Contains("http://"))
            {
                serverUrl = serverUrl.Substring("http://".Length);
            }

            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(serverUrl))
            {
                if (url.Contains("://") && !url.Contains("http"))
                {
                    sb.Append("http://");
                    sb.Append(serverUrl);
                    if (url.IndexOf("/", 3) > 0)
                        sb.Append(url.Substring(url.IndexOf("/", 3)));
                }
                else
                {
                    sb.Append("http://");
                    sb.Append(serverUrl);
                    sb.Append(url);
                }
            }
            else if (!string.IsNullOrEmpty(site.Properties["hostname"]))
            {
                sb.Append("http://");
                sb.Append(site.Properties["hostname"]);
                sb.Append(url);
            }
            else
            {
                if (url.Contains("://") && !url.Contains("http"))
                {
                    sb.Append("http://");
                    sb.Append(url);
                }
                else
                {
                    sb.Append(Sitecore.Web.WebUtil.GetFullUrl(url));
                }
            }

            return sb.ToString();
        }

        private static string HtmlEncode(string text)
        {
            return HttpUtility.HtmlEncode(text);
        }

        private void SubmitEngine(string engine, string sitemapUrl)
        {
            if (sitemapUrl.Contains("http://localhost"))
                return;

            var requestUriString = engine + SitemapManager.HtmlEncode(sitemapUrl);
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUriString);
            try
            {
                if (((HttpWebResponse)httpWebRequest.GetResponse()).StatusCode != HttpStatusCode.OK)
                    Log.Error(string.Format("Cannot submit sitemap to \"{0}\"", (object)engine), (object)this);
            }
            catch
            {
                Log.Warn(string.Format("The serachengine \"{0}\" returns an 404 error", (object)requestUriString), (object)this);
            }
        }

        private List<Item> GetSitemapItems(string rootPath)
        {
            var contentRoot = Db.Items[rootPath];
            var user = User.FromName(@"extranet\Anonymous", true);

            Item[] descendants;
            using (new UserSwitcher(user))
            {
                descendants = contentRoot.Axes.GetDescendants();
            }

            var homeTemplateID = new ID("{7E05E34B-1262-4059-877D-335F1F7246FC}");
            var inactiveHomeStatus = "{F790311A-F688-47F1-8E80-F51BBF14B6D3}";

            var sitemapItems = descendants.ToList();
            var results = new List<Item>();

            foreach (Item item in sitemapItems)
            {
                if (item.TemplateID.Equals(homeTemplateID) && item.Fields["Home for Sale Status"].Value == inactiveHomeStatus)
                {
                    continue;
                }

                if (item.Fields["Include In Sitemap XML"] != null && item.Fields["Include In Sitemap XML"].Value == "1")
                {
                    results.Add(item);
                }
            }

            return results;
        }
    }
}
