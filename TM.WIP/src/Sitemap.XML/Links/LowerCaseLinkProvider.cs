﻿using Sitecore.Data.Items;
using Sitecore.Links;

namespace Sitemap.XML.Links
{
    public class LowerCaseLinkProvider : LinkProvider
    {
        public override string GetItemUrl(Item item, UrlOptions options)
        {
            string itemUrl = base.GetItemUrl(item, options);
            if (!string.IsNullOrEmpty(itemUrl))
                return itemUrl.ToLower();
            return itemUrl;
        }
    }
}
