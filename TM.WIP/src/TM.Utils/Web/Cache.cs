﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using TM.Utils.Extensions;
using System.Web.Caching;
namespace TM.Utils.Web
{
  public  class Cache
    {

        public T Get<T>(string cacheKey, Func<T> callback) where T : class
        {
            var item = HttpContext.Current.Cache[cacheKey];
            if (RefereshCache || item == null)
            {
                item = callback();
                Set(new CacheItem {Key = cacheKey, Value = item});
            }

            return item.DeepClone<T>();
        }

    

      public IEnumerable<T> Get<T>(string cacheKey, Func<IEnumerable<T>> callback) where T : class
        {
           
            var items = HttpContext.Current.Cache.Get(cacheKey) as IEnumerable<T>;
            if (RefereshCache || items == null)
            {
                items = callback();
                Set(new CacheItem {Key = cacheKey,Value = items});
            }

            return items.DeepCloneEnum<T>();
        }

      public void Set(CacheItem cacheItem)
      {
          
          HttpContext.Current.Cache.Insert(cacheItem.Key, cacheItem.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration,TimeSpan.FromMinutes(10));
      }

      private static bool RefereshCache
      {
          get
          {
              var refereshCache = HttpContext.Current.Request.QueryString["clearcache"] != null;

              //handle ajax
              var uriReferrer = HttpContext.Current.Request.UrlReferrer;
              if (uriReferrer != null && uriReferrer.Query.Contains("clearcache"))
              {
                  refereshCache = true;
              }
              return refereshCache;
          }
      }


    }
}
