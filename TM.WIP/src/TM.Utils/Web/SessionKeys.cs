﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TM.Utils.Web
{
    public class SessionKeys
    {
        public static string LastVisitedCommunity(string sitename)
        {
          return sitename+"_LastVisitedCommunity";  
        } 
        public static string LastRandomCommunityIndex(string sitename)
        {
          return sitename+   "_LastRandomCommunityIndex";
        }

        public static string CurrentUser(string sitename)
        {
            return sitename + "_CurrentUser";
        }


		public static string SpecificRfiLocationKey = "SpecificRfiLocation";
		public static string RealtorSearchDiviKey = "RealtorSearchDiviId";
		public static string SendtoUrlKey = "SendtoUrlfromLogin";
		
		public static string InventorySearchFacetsKey = "InventorySearchFacetsKey_";
		public static string SearchFacetsKey(bool isRealtorSearch)
		{
			return "SearchFacetsKey_RealtorSearchis-" + isRealtorSearch.ToString();
		}

		public static string SearchPathKey(bool isRealtorSearch)
		{
			return "SearchPathKey_RealtorSearchis-" + isRealtorSearch.ToString();
		}
		
    }
}
