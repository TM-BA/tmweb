
namespace TM.Utils.Extensions
{
	using System;
	using System.Globalization;


	public static class ExtensionsToInt32
	{
		public static int FromHexToInt32(this string text)
		{
			int value;
			if (int.TryParse(text, NumberStyles.HexNumber, NumberFormatInfo.CurrentInfo, out value))
				return value;

			throw new ArgumentException("'{0}' is not a valid hexidecimal value".FormatWith(text));
		}

		public static int StringToInt32(this string text)
		{
			if (!string.IsNullOrEmpty(text))
			{
				int value;
				if (int.TryParse(text, out value))
					return value;
			}
			
				return 0;
										  
			//throw new ArgumentException("'{0}' is not a valid integer value".FormatWith(text));
		}
	}
}