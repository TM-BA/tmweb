﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace TM.Utils.Extensions
{
    public static class JavaScriptSerializerHelper
    {
        public static string ToJSON(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static dynamic FromJSON(this string obj)
        {
            return JsonConvert.DeserializeObject(obj);
        }

        public static T FromJSON<T>(this string obj)
        {
            return JsonConvert.DeserializeObject<T>(obj);
        }

       
        public static string ToJSON(this object obj,bool ignoreMemberWithDefaultValue)
        {
            return ignoreMemberWithDefaultValue 
                ?JsonConvert.SerializeObject(obj,Formatting.None ,new JsonSerializerSettings(){DefaultValueHandling = DefaultValueHandling.Populate}) 
                : obj.ToJSON();
        }
    }
}