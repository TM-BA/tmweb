﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using TM.Utils.Web;

namespace TM.Utils.Extensions
{
	public static class ExtensionsToEnum
	{
		public static T ToEnum<T>(this string enumString)
		{
			return (T)Enum.Parse(typeof(T), enumString);
		}

		public static string GetAbbreviation(this Enum value)
		{
			Type type = value.GetType();
			string name = Enum.GetName(type, value);
			if (name != null)
			{
				FieldInfo field = type.GetField(name);
				if (field != null)
				{
					DescriptionAttribute attr =
						   Attribute.GetCustomAttribute(field,
							 typeof(DescriptionAttribute)) as DescriptionAttribute;
					if (attr != null)
					{
						return attr.Description;
					}
				}
			}
			return null;
		}

		public static string GetState(this GlobalEnums.State state)
		{
			switch (state)
			{
				case GlobalEnums.State.AL:
					return "ALABAMA";

				case GlobalEnums.State.AK:
					return "ALASKA";

				case GlobalEnums.State.AS:
					return "AMERICAN SAMOA";

				case GlobalEnums.State.AZ:
					return "ARIZONA";

				case GlobalEnums.State.AR:
					return "ARKANSAS";

				case GlobalEnums.State.CA:
					return "CALIFORNIA";

				case GlobalEnums.State.CO:
					return "COLORADO";

				case GlobalEnums.State.CT:
					return "CONNECTICUT";

				case GlobalEnums.State.DE:
					return "DELAWARE";

				case GlobalEnums.State.DC:
					return "DISTRICT OF COLUMBIA";

				case GlobalEnums.State.FM:
					return "FEDERATED STATES OF MICRONESIA";

				case GlobalEnums.State.FL:
					return "FLORIDA";

				case GlobalEnums.State.GA:
					return "GEORGIA";

				case GlobalEnums.State.GU:
					return "GUAM";

				case GlobalEnums.State.HI:
					return "HAWAII";

				case GlobalEnums.State.ID:
					return "IDAHO";

				case GlobalEnums.State.IL:
					return "ILLINOIS";

				case GlobalEnums.State.IN:
					return "INDIANA";

				case GlobalEnums.State.IA:
					return "IOWA";

				case GlobalEnums.State.KS:
					return "KANSAS";

				case GlobalEnums.State.KY:
					return "KENTUCKY";

				case GlobalEnums.State.LA:
					return "LOUISIANA";

				case GlobalEnums.State.ME:
					return "MAINE";

				case GlobalEnums.State.MH:
					return "MARSHALL ISLANDS";

				case GlobalEnums.State.MD:
					return "MARYLAND";

				case GlobalEnums.State.MA:
					return "MASSACHUSETTS";

				case GlobalEnums.State.MI:
					return "MICHIGAN";

				case GlobalEnums.State.MN:
					return "MINNESOTA";

				case GlobalEnums.State.MS:
					return "MISSISSIPPI";

				case GlobalEnums.State.MO:
					return "MISSOURI";

				case GlobalEnums.State.MT:
					return "MONTANA";

				case GlobalEnums.State.NE:
					return "NEBRASKA";

				case GlobalEnums.State.NV:
					return "NEVADA";

				case GlobalEnums.State.NH:
					return "NEW HAMPSHIRE";

				case GlobalEnums.State.NJ:
					return "NEW JERSEY";

				case GlobalEnums.State.NM:
					return "NEW MEXICO";

				case GlobalEnums.State.NY:
					return "NEW YORK";

				case GlobalEnums.State.NC:
					return "NORTH CAROLINA";

				case GlobalEnums.State.ND:
					return "NORTH DAKOTA";

				case GlobalEnums.State.MP:
					return "NORTHERN MARIANA ISLANDS";

				case GlobalEnums.State.OH:
					return "OHIO";

				case GlobalEnums.State.OK:
					return "OKLAHOMA";

				case GlobalEnums.State.OR:
					return "OREGON";

				case GlobalEnums.State.PW:
					return "PALAU";

				case GlobalEnums.State.PA:
					return "PENNSYLVANIA";

				case GlobalEnums.State.PR:
					return "PUERTO RICO";

				case GlobalEnums.State.RI:
					return "RHODE ISLAND";

				case GlobalEnums.State.SC:
					return "SOUTH CAROLINA";

				case GlobalEnums.State.SD:
					return "SOUTH DAKOTA";

				case GlobalEnums.State.TN:
					return "TENNESSEE";

				case GlobalEnums.State.TX:
					return "TEXAS";

				case GlobalEnums.State.UT:
					return "UTAH";

				case GlobalEnums.State.VT:
					return "VERMONT";

				case GlobalEnums.State.VI:
					return "VIRGIN ISLANDS";

				case GlobalEnums.State.VA:
					return "VIRGINIA";

				case GlobalEnums.State.WA:
					return "WASHINGTON";

				case GlobalEnums.State.WV:
					return "WEST VIRGINIA";

				case GlobalEnums.State.WI:
					return "WISCONSIN";

				case GlobalEnums.State.WY:
					return "WYOMING";
			}

			throw new Exception("Not Available");
		}


		public static GlobalEnums.State GetStateAbbreviationByState(this string name)
        {
			switch (name.ToUpper())
			{
				case "ALABAMA":return GlobalEnums.State.AL;
				case "ALASKA":return GlobalEnums.State.AK; 
				case "AMERICAN SAMOA":return GlobalEnums.State.AS;
				case "ARIZONA":return GlobalEnums.State.AZ;
				case "ARKANSAS":return GlobalEnums.State.AR;
				case "CALIFORNIA":return GlobalEnums.State.CA;
				case "COLORADO":return GlobalEnums.State.CO;
				case "CONNECTICUT":return GlobalEnums.State.CT;
				case "DELAWARE":return GlobalEnums.State.DE;
				case "DISTRICT OF COLUMBIA":return GlobalEnums.State.DC;
				case "FEDERATED STATES OF MICRONESIA":return GlobalEnums.State.FM;
				case "FLORIDA":return GlobalEnums.State.FL;
				case "GEORGIA":return GlobalEnums.State.GA;
				case "GUAM":return GlobalEnums.State.GU;
				case "HAWAII":return GlobalEnums.State.HI;
				case "IDAHO":return GlobalEnums.State.ID;
				case "ILLINOIS":return GlobalEnums.State.IL;
				case "INDIANA":return GlobalEnums.State.IN;
				case "IOWA":return GlobalEnums.State.IA;
				case "KANSAS":return GlobalEnums.State.KS;
				case "KENTUCKY":return GlobalEnums.State.KY;
				case "LOUISIANA":return GlobalEnums.State.LA;
				case "MAINE":return GlobalEnums.State.ME;
				case "MARSHALL ISLANDS":return GlobalEnums.State.MH;
				case "MARYLAND":return GlobalEnums.State.MD;
				case "MASSACHUSETTS":return GlobalEnums.State.MA;
				case "MICHIGAN":return GlobalEnums.State.MI;
				case "MINNESOTA":return GlobalEnums.State.MN;
				case "MISSISSIPPI":return GlobalEnums.State.MS;
				case "MISSOURI":return GlobalEnums.State.MO;
				case "MONTANA":return GlobalEnums.State.MT;
				case "NEBRASKA":return GlobalEnums.State.NE;
				case "NEVADA":return GlobalEnums.State.NV;
				case "NEW HAMPSHIRE":return GlobalEnums.State.NH;
				case "NEW JERSEY":return GlobalEnums.State.NJ;
				case "NEW MEXICO":return GlobalEnums.State.NM;
				case "NEW YORK":return GlobalEnums.State.NY;
				case "NORTH CAROLINA":return GlobalEnums.State.NC;
				case "NORTH DAKOTA":return GlobalEnums.State.ND;
				case "NORTHERN MARIANA ISLANDS":return GlobalEnums.State.MP;
				case "OHIO":return GlobalEnums.State.OH;
				case "OKLAHOMA":return GlobalEnums.State.OK;
				case "OREGON":return GlobalEnums.State.OR;
				case "PALAU":return GlobalEnums.State.PW;
				case "PENNSYLVANIA":return GlobalEnums.State.PA;
				case "PUERTO RICO":return GlobalEnums.State.PR;
				case "RHODE ISLAND":return GlobalEnums.State.RI;
				case "SOUTH CAROLINA":return GlobalEnums.State.SC;
				case "SOUTH DAKOTA":return GlobalEnums.State.SD;
				case "TENNESSEE":return GlobalEnums.State.TN;
				case "TEXAS":return GlobalEnums.State.TX;
				case "UTAH":return GlobalEnums.State.UT;
				case "VERMONT":return GlobalEnums.State.VT;
				case "VIRGIN ISLANDS":return GlobalEnums.State.VI;
				case "VIRGINIA":return GlobalEnums.State.VA;
				case "WASHINGTON":return GlobalEnums.State.WA;
				case "WEST VIRGINIA":return GlobalEnums.State.WV;
				case "WISCONSIN":return GlobalEnums.State.WI;
				case "WYOMING":return GlobalEnums.State.WY;

				case "ALBERTA": return GlobalEnums.State.AB;
				case "BRITISH COLUMBIA": return GlobalEnums.State.BC;
				case "MANITOBA": return GlobalEnums.State.MB;
				case "NEW BRUNSWICK": return GlobalEnums.State.NB;
				case "NEWFOUNDLAND AND LABRADOR": return GlobalEnums.State.NL;
				case "NOVA SCOTIA": return GlobalEnums.State.NS;
				case "NORTHWEST TERRITORIES": return GlobalEnums.State.NT;
				case "NUNAVUT": return GlobalEnums.State.NU;
				case "ONTARIO": return GlobalEnums.State.ON;
				case "PRINCE EDWARD ISLAND": return GlobalEnums.State.PE;
				case "QUEBEC": return GlobalEnums.State.QC;
				case "SASKATCHEWAN": return GlobalEnums.State.SK;
				case "YUKON": return GlobalEnums.State.YT;
				default: return GlobalEnums.State.XX;
 
			}

            throw new Exception("Not Available");
        }

		public static GlobalEnums.SortBy GetSearchSortByValue(this string value)
		{
			switch (value)
			{
				case "2": return GlobalEnums.SortBy.PriceLowtoHigh;
				case "3": return GlobalEnums.SortBy.PriceHightoLow;
				case "4": return GlobalEnums.SortBy.Location;
				case "5": return GlobalEnums.SortBy.PlanName;
				case "6": return GlobalEnums.SortBy.SquareFootage;
				case "7": return GlobalEnums.SortBy.Numberofbedrooms;
				case "8": return GlobalEnums.SortBy.AvailabilityDate;
				case "9": return GlobalEnums.SortBy.Stories;
				case "1": 
				default:
					return GlobalEnums.SortBy.CommunityName;
			}

			throw new Exception("Not Available");
		}   

	}
}
