
namespace TM.Utils.Extensions
{
	using System.IO;
	using System.Text;
	using Newtonsoft.Json;


	public static class ExtensionsToStream
	{
		public static byte[] ReadToEnd(this Stream stream)
		{
			using (var content = new MemoryStream())
			{
				var buffer = new byte[4096];

				int read = stream.Read(buffer, 0, 4096);
				while (read > 0)
				{
					content.Write(buffer, 0, read);

					read = stream.Read(buffer, 0, 4096);
				}

				return content.ToArray();
			}
		}

		public static string ReadToEndAsText(this Stream stream)
		{
			return Encoding.UTF8.GetString(stream.ReadToEnd());
		}


		public static T ReadJson<T>(this Stream context)
		{
			using (var streamReader = new StreamReader(context))
			using (var jsonReader = new JsonTextReader(streamReader))
				return (T)new JsonSerializer().Deserialize(jsonReader, typeof(T));
		}

		public static object ReadJsonObject(this Stream context)
		{
			using (var streamReader = new StreamReader(context))
			using (var jsonReader = new JsonTextReader(streamReader))
				return new JsonSerializer().Deserialize(jsonReader);
		}
	}
}