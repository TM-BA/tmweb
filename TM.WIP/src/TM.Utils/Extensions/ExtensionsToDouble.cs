
namespace TM.Utils.Extensions
{
	using System;
	using System.Globalization;


	public static class ExtensionsToDouble
	{
		public static double StringToDouble(this string text)
		{
			if (!string.IsNullOrEmpty(text))
			{
				double value;
				if (double.TryParse(text, out value))
					return value;
			}
	
			return 0;
			
			
//			throw new ArgumentException("'{0}' is not a valid integer value".FormatWith(text));
		}
	}
}