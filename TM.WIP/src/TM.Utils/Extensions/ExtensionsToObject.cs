
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace TM.Utils.Extensions
{
	using System;
	using System.Collections;
	using System.Linq;
	using System.Reflection;


	public static class ExtensionsToObject
	{
		const int RecursionLimit = 10;

		public static string Stringify(this object value)
		{
			try
			{
				return StringifyInternal(value, RecursionLimit);
			}
			catch (InvalidOperationException ex)
			{
				return value.ToString();
			}
		}

		static string StringifyInternal(object value, int recursionLevel)
		{
			if (value == null)
				return "null";

			if (recursionLevel < 0)
				throw new InvalidOperationException();

			if (value is string || value is char)
				return "\"" + value + "\"";

			var collection = value as IEnumerable;
			if (collection != null)
				return StringifyCollection(collection, recursionLevel);

			if (value.GetType().IsValueType)
				return value.ToString();

			return StringifyObject(value, recursionLevel);
		}


		static string StringifyCollection(IEnumerable collection, int recursionLevel)
		{
			string[] elements = collection.Cast<object>()
				.Select(x => StringifyInternal(x, recursionLevel - 1))
				.ToArray();

			return "[" + String.Join(", ", elements) + "]";
		}

		static string StringifyObject(object value, int recursionLevel)
		{
			string[] elements = value
				.GetType()
				.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
				.Select(x => "{0} = {1}".FormatWith(x.Name, StringifyInternal(x.GetValue(value, null), recursionLevel - 1)))
				.ToArray();

			return "{" + String.Join(", ", elements) + "}";
		}

		public static T CastAs<T>(this object input)
		{

            if (input is T)
                return (T)input;

            var destinationType = typeof(T);
            if (destinationType.IsGenericType && destinationType.GetGenericTypeDefinition() == typeof(Nullable<>))
                destinationType = new NullableConverter(destinationType).UnderlyingType;

            return (T)Convert.ChangeType(input, destinationType);
        }

	

        public static T CastAs<T>(this object source, T returnValueIfException)
        {
            try
            {
                return source.CastAs<T>();
            }
            catch
            {
                return returnValueIfException;
            }
        }
  
        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T DeepClone<T>(this object source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static IEnumerable<T> DeepCloneEnum<T>(this IEnumerable<T> source)
        {
            List<T> items = new List<T>();

            foreach (var t in source)
            {
                items.Add(t.DeepClone<T>());
            }

            return items.AsEnumerable();
        }

		/// <summary>
		/// Returns the value of the instance member, or the default value if the instance is null
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="instance"></param>
		/// <param name="accessor"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static TValue ValueOrDefault<T, TValue>(this T instance, Func<T, TValue> accessor, TValue defaultValue)
			where T : class
		{
			if(null == instance)
				return defaultValue;

			return accessor(instance);
		}

	}
}